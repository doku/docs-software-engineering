#number of files with umlaute
count="0"

#setting to enable for loop to iterate over files with spaces in their path
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

for file in $(find . -name '*.*'); do

  case "$file" in
     *ä* )
          #space in filename
          echo "FILE: $file"
          echo "WARNING: This filepath contains ä"
          echo

          count=$((count+1))
          ;;
      *Ä* )
          #space in filename
          echo "FILE: $file"
          echo "WARNING: This filepath contains Ä"
          echo

          count=$((count+1))
          ;;
      *ü* )
          #space in filename
          echo "FILE: $file"
          echo "WARNING: This filepath contains ü"
          echo

          count=$((count+1))
          ;;
      *Ü* )
          #space in filename
          echo "FILE: $file"
          echo "WARNING: This filepath contains Ü"
          echo

          count=$((count+1))
          ;;
      *ö* )
          #space in filename
          echo "FILE: $file"
          echo "WARNING: This filepath contains ö"
          echo

          count=$((count+1))
          ;;
      *Ö* )
          #space in filename
          echo "FILE: $file"
          echo "WARNING: This filepath contains Ö"
          echo

          count=$((count+1))
          ;;
  esac
done

#setting for the for loop
IFS=$SAVEIFS

if [ $count -gt "0" ]; then
  echo "FAILING - filenames and foldernames with umlauts are not allowed"
  exit 1
fi
