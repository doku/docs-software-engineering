# build latex pdf if not found in cache
if [ ! -e pdf/software-engineering-doku.pdf ]
  then ./build-latex-pdf.sh
fi
