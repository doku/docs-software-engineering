cd source/sphinx-custom-theme/
git fetch
SUBMODULEHEAD=$(git rev-parse HEAD)
SUBMODULEORIGINHEAD=$(git rev-parse origin/HEAD)
cd - > /dev/null
if [ ${SUBMODULEHEAD} != ${SUBMODULEORIGINHEAD} ]
then
	echo "The sphinx-custom-theme submodule is not uptodate. Please update it using <git submodule update --remote>."
	exit 1
fi
