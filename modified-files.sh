git log --since="90 days ago" --pretty=format:"" --name-only | \
grep "[^\s]" | \
sort | uniq -c | \
sort -nr | head -10
