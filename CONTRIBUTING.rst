============
Contributing
============

Everything you need to contribute is summarized in the ``section`` of the documentation.

https://doku.gitlab.io/docs-software-engineering/doc/about/index.html

Konventions
===========

https://doku.gitlab.io/docs-software-engineering/doc/about/conventions/conventions.html

RST Cheatsheet
==============
https://doku.gitlab.io/docs-software-engineering/doc/about/cheatsheet/cheatsheet.html

Make a Change
=============
Just click edit on Gitlab and commit your change