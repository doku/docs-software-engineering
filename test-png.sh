# check for .PNG files and makes pipline fail. All images should be .png

count="0"

for file in $(find . -name '*.PNG'); do
  echo "$file$"
  count=$((count+1))
done

echo "Capital PNG Ending:     $count"

if [ $count -gt "0" ]; then
  echo "FAILING - no images with capital PNG are allowed"
  exit 1
fi
