#number of files with spaces
count="0"

#setting to enable for loop to iterate over files with spaces in their path
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

for file in $(find . -name '*.*'); do

  case "$file" in
     *\ * )
          #space in filename
          echo "FILE: $file"
          echo "WARNING: This filepath contains spaces"
          echo

          count=$((count+1))
          ;;
       *)
           # No Space in filename
           ;;
esac
done

#setting for the for loop
IFS=$SAVEIFS

if [ $count -gt "0" ]; then
  echo "FAILING - filenames and foldernames with spaces are not allowed"
  exit 1
fi
