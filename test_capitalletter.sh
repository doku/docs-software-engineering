#number of files with umlaute
count="0"

#setting to enable for loop to iterate over files with spaces in their path
#SAVEIFS=$IFS
#IFS=$(echo -en "\n\b")

regex="/\w*[A-Z]\w*/g"

for file in $(find . -name '*.*'); do

  if [[ $file =~  $regex ]]; then
    count=$((count+1))

    echo "$file"
  fi

done

#setting for the for loop
#IFS=$SAVEIFS

if [ $count -gt "0" ]; then
  echo "FAILING - filenames and foldernames with capital are not allowed"
  exit 1
fi
