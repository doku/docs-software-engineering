========================================
Software-Engineering Documentation
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   doc/software_engineering/index
   doc/datastructures_algorithms/index
   doc/hpi/index
   doc/sjsu/index
   doc/programming/index
   doc/theoretische_informatik/index
   doc/technische_informatik/index
   doc/math/index
   doc/design/index
   doc/guides/index
   doc/sport/index
   doc/business/index
   doc/about/index
