======
Sphinx
======

Fix Syntax Errors locally
=========================

#. Delete the build folder (sphinx usually only generates the files that
   have changed and therefor won't show all warnings unless you delete the
   previously generated build folder)
#. open a command line tool of your choice in full screen mode (to avoid unnecessary line
   breaks in log)
#. run ``sphinx-build source build/html`` (takes files form source folder and builds
   html version of docs in the build/html folder)
#. scroll to the top of the log and fix the warnings one by one
#. this command does not rebuild the docs automatically, to check if all errors are
   fixed, it is recommended to delete the build folder and rerun the command

.. hint::
  Frequently deleting and rebuilding the html version can increase the disk space
  the Trash (*Papierkorb*) requires significantly. Make sure too delete all
  build folders from your trash to save disk space.


Templates
=========

sphinx uses the templating engine Jinja.


HTML File templates:

https://twig.sensiolabs.org/

Generate PDF from Sphinx
========================

rst2pdf does not support python3

.. code-block:: bash

  sphinx-build -b latex source output/pdf

This command produces a ``.tex`` file which can be converted to pdf using latex.


Sphinx Customization
====================

Theme Variables
---------------

Example:

.. code-block:: python

  html_context = {'url_to_doku_pdf': "pdf/doku.pdf", 'doku_pdf_titel': "doku"}

.. code-block:: js

    $("nav.wy-nav-side").append("<a' href='{{ base_url }}/{{url_to_doku_pdf}}' download='{{doku_pdf_titel}}'>Download</a>");


Problem mit BaseURL: Wenn die Doku selbst nur als sub url existiert, dann wird als
base nicht die richtige URL verwendet.

Beispiel: Die Start URL ist:

.. code-block:: bash

    https://doku.gitlab.io/docs-software-engineering/

    //Die {{ base_url }} wäre
    https://doku.gitlab.io
