=====================
Adobe Premiere Basics
=====================

Keyboard Shortcuts
==================

* Ripple Delete ``Shift Entf``
* Cursor Cut ``Strg K``


Basics
======

Audio aus Video entfernen:

.. figure:: unlinkvideoaudio.png
	 :alt: unlinkVideoAudio
