==========
Acrobat DC
==========

PDF Scaling Rollup
==================

- Gegebenes PDF soll auf beliebige Größe Hoch oder Runter skaliert werden.

#. Öffnen der PDF in Acrobat DC
#. PDF Drucken mit Adobe PDF
#. Beim Drucker auf ``Properties``
#. unter ``Adobe PDF Settings`` bei ``Adobe PDF Page Size`` auf ``add`` gehen
#. Hier kann man ein eigenes Seitenformat erstellen und die gewünschte Größe angeben
#. Das neu erstellte Format kann dann unter ``Page Setup`` ausgewählt werden
#. Unter ``Page Sizing`` die Option ``fit`` wählen
#. Seite "Drucken" und gewünschten Speicherort wählen

.. figure:: drucker_view.png
	 :alt: drucker_view
