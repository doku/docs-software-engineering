======================
Software Konfiguration
======================

Display Fusion
==============

Shortcuts
---------

- ``STRG + WIN + X`` to move an application to the next monitor

- ``WIN F9`` to switch to **2 Monitor Split**
- ``WIN F10`` to switch to **2 Monitor default** (no Split Setup)
