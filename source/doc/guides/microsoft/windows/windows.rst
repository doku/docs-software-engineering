=======
Windows
=======

Shortcuts
=========

``CTRL + N`` to open new  Explorer Window at same location

``STRG + Shift + N`` um neuen Ordner zu erzeugen

``F2`` um Datei oder Verzeichnis umzubenennen

Move Window to different Desktop: ``WIN + TAB`` then focus the window using arrow
keys and hit ``Shift + F11``

Chrome
------

``CTRL + TAB`` Tab vorwärts

``CTRL + SHIFT + TAB`` Tab zurück

Snipping Tool
-------------

``ALT + N`` Neuer Screenshot

Konfiguration
=============

Remove File Path limit
----------------------

In diesem Guide wird im Registry die 255 Character Sperre für Dateipfade entfernt.
Damit sollten Anwendungen auch längere Dateipfade verwenden können.

https://www.howtogeek.com/266621/how-to-make-windows-10-accept-file-paths-over-260-characters/

Allerdings gibt es immer noch einen ``filepath too long`` Error wenn man versucht
einen ``.zip`` Ordner mit sehr langen Pfaden mit dem standard Windows Entpacken zu
extrahieren. Verwendet man ``7zip`` tritt der Fehler nicht auf.

Nachdem die ``long paths`` auf ``enabled`` gesetzt wurde lassen sich nun Ordner mit
langen Pfaden löschen, ohne dass Fehlermeldungen kommen.

Werbung entfernen
-----------------

Werbung im Startmenü entfernen: Unter ``Einstellungen -> Personalisieren -> Start``

.. figure:: removestartadds.png
	 :alt: removestartadds

Ausführen-Dialog (Windows + R)
==============================

Registry: ``regedit``

Commands
========

Datenträgerverwaltung ``diskmgmt`` (cmd als admin öffnen um rechte für Datenträgerverwaltung zu haben)
