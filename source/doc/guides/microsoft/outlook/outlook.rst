Outlook
=======

Outlook Postfach leeren
-----------------------

Outlook öffnen
~~~~~~~~~~~~~~~

Zuerst muss Outlook geöffnet werden.

.. figure:: outlookopen.png
	 :alt:   outlookOpen

Emails nach Größe sortieren
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Um die Mails nach der Größe zu sortieren müssen folgende Schritte ausgeführt werden:

Die Dateien werden jetzt der Größe nach geordnet angezeigt.

.. figure:: emailsize.png
	 :alt: emailSize

Emails zum Löschen auswählen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Nun muss man die Emails aussuchen die gelöscht werden sollen. Dazu klickt man die
Email an und entscheidet, ob die Anhänge gespeichert werden sollen und die Mail dann
gelöscht werden kann.

Speichern der Anhänge
~~~~~~~~~~~~~~~~~~~~~

Mit einem Klick auf den kleine Pfeil der Anhänge kann man das Optionsmenü öffnen.
Jetzt kann man ``Speichern unter`` auswählen.

Diesen Vorgang muss man für jeden Anhang wiederholen.

Löschen der Mail
~~~~~~~~~~~~~~~~

Hat man alle Anhänge der Mail gespeichert, kann diese gelöscht werden.

Dazu geht man mit der Maus über die Mail. Es erscheint ein rotes Kreuz und klickt
man darauf wird die Mail gelöscht.
