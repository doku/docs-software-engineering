=======
Windows
=======

.. toctree::
  :maxdepth: 2
  :caption: Contents
  :glob:

  windows/*
  outlook/*
