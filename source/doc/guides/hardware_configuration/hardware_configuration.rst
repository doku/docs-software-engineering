======================
Hardware Konfiguration
======================

Drucker
=======

Samsung Laser Farbdrucker
-------------------------

Ändern des Druckertreibers um Farbdruck zu ermöglichen (Win10):

In der Systemsteuerung ``Geräte und Drucker`` öffnen und Rechtsklick auf Samsung Drucker. Dann
``Druckereigenschaften -> Eigenschaften ändern -> Geräteoptionen -> Druckermodell``
auswählen und das richtige Druckermodell auswählen.

.. figure:: select_printer.png
	 :alt: select_printer
