=====
TOEFL
=====

Speaking
========

Integrated Task
---------------

Question 3

  Reading: pay attention to the description of the proposal and the reasons that
  are given for or against the proposal

  You are not asked to give your own opinion but to state the opinion of one of
  the speakers and summarize the reasons for having that opinion.

  :preparation time:
  :speaking time:

Quiestion 4

  :preparation time: 30s
  :speaking time: 60s

Question 5

  describe the problem, very short (1-2 Sentences)

  state which solution you would prefer
  explain why you prefer that solution


Writing Section
===============

Integrated Task
---------------

Summarize the points made in the lecture, and explain how reading passage supports/contradicts
the lecture.

You are **not** being asked for your opinion.

:time: 20min

Phrases
~~~~~~~


- But the lecture completely contradicts this claim by stating that...
- The lecture brings up the idea that...
- This casts doubt on the claim in the reading that...


Independent Task
----------------

Essay

:time: 30 min

clear structure (3 Points with examples and explanation)

- clear outline for essay structure
- Rhetorical Questions
