==========
Vocabulary
==========

.. csv-table::
  :header: "English", "German", "Sentence"

  "ingenuity", "Einfallsreichtum"
  "pebble", "Kieselstein"
  "disturbance", "Störung"
  "ooze"
  "esteem", "Wertschätzung"
  "Ambiguity", "Mehrdeutigkeit"
