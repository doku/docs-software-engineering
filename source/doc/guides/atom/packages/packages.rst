===================
Handy Atom Packages
===================

RST-Preview
===========
With the `rst-preview-pandoc <https://atom.io/packages/rst-preview-pandoc>`_
Atom package you can preview your rst(and many other filed) using the
keykombination``strg`` + ``shift`` + ``e``. This may look like this:

.. figure:: rst_preview.png
	 :alt: rst_preview

Code, Math, Images and other Formating will be displayed correctly.
There are problems with unformatted tocs(tree of content) though.

Installation
------------
Besides installing the package itself you will need to download `pandoc <http://pandoc.org/installing.html>`_ (`download link <https://github.com/jgm/pandoc/releases/tag/2.1>`_).
It is easiest to download the zip and unzip it to a folder of choice.
Now you need to add the ``Path`` to the ``pandoc.exe`` in the package.


.. figure:: pandoc_path.png
	 :alt: pandoc_path

By pressing the mentioned keykombination ``strg`` + ``shift`` + ``e`` a preview should open now.
