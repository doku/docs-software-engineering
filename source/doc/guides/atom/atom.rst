====
Atom
====

Shortcuts
=========

https://shortcutworld.com/en/Atom-(text-editor)/1.0/win/all

Tree View
---------

- ``CTRL + K , CTRL + B`` Toggle Tree View
- ``a`` While folder in Tree View selected, adds a new from-file

- ``CRTL + SHIFT + 9`` Toggle Git View



Editor
------

- ``CTRL + ArrowUp/ ArrowDown`` Move line up/down
- ``CTRL + W`` Close currently active Tab
- ``CTRL + p`` Search for file and open it.
- ``SHIFT + TAB`` Remove Indentation (in one tab increments)

Shortcuts Customizing
=====================

Open ``File -> Keymapp..`` and add your custom shortcuts here.

.. code::

  '.platform-win32': 'ctrl-alt-l': 'tree-view:toggle-focus'

Für custom Shortcuts muss die ``keymap.cson`` angepasst werden:

.. code::

  'atom-text-editor':
    'ctrl-m': 'custom:insert-rst-inline-math'
    'alt-shift-o': 'custom:insert-landau-O'

Im ``init.coffee`` kann dann die Funktion definiert werden:

.. code-block:: CoffeeScript

  atom.commands.add 'atom-text-editor',
    'custom:insert-rst-inline-math': ->
      editor = @getModel()
      editor.insertText(':math:``')

  'custom:insert-landau-O': ->
    editor = @getModel()
    editor.insertText('\\\mathcal{O}')

.. danger:: Die Shortcuts treten erst nach Neustarten des Editors in Kraft.

Mit ``\\`` können special characters wie ``\ { }`` escaped werden.

Spell Check
===========

Die Atom Rechtschreibkontrolle ist nicht standardmäßig für ``restructuredText``
aktiviert.

Man muss unter ``Settings -> Packages`` nach ``Spell Check`` suchen und die ``settings``
der Erweiterung öffnen. Hier muss ``text.restructuredtext`` unter ``Grammars`` hinzugefügt
werden.

.. figure:: spellcheck_rst.png
	 :alt: spellcheck_rst

Ist der Curser auf einem rot unterstrichenen Wort mit Rechtschreibfehler, so können
mit der Tastenkombination ``STRG + SHIFT + .`` Korrekturvorschläge angezeigt werden.

Um auch für weitere Dateitypen die Rechtschreibkontrolle zu aktivieren, muss man
ihren ``scope name`` auch unter ``grammars`` eintragen.

Um den ``scope name`` einer Datei herauszufinden, muss man das Commands
``Editor: Log Curser Scope`` in der ``Command Palette`` ausführen.
Die ``Command Palette`` kann man mit ``STRG + Shift + P`` öffnen.

Packages
========

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   packages/*
