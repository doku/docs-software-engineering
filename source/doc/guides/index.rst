Guides
======

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   linux/index
   sphinx/*
   latex/index
   adobe/index
   microsoft/index
   hardware_configuration/*
   software_configuration/*
   atom/*
   neo/*
   vmware/*
   english/index
