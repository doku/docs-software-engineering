=============
Latex Symbole
=============

Mathe Symbole
=============

:math:`\Biggl(\biggl(\Bigl(\bigl((x)\bigr)\Bigr)\biggr)\Biggr)`

- Unendlich Symbol :math:`\infty`: ``\infty``
- Rundung :math:`\approx`: ``\approx``

Landau Symbole
--------------

Groß O: :math:`\mathcal{O}` : ``\mathcal{O}``

Integral
--------

Integral mit Grenzen:

.. math::

  \int \limits_{-a}^a f(x) dx = \int \limits_0^a f(x) + f(-x)

.. code-block:: latex

  \int \limits_{-a}^a f(x) dx = \int \limits_0^a f(x) + f(-x)
