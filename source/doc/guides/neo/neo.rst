====================
How to switch to NEO
====================

Install NEO
===========

Use the Neo Installer given in the Guide from their website
https://wiki.neo-layout.org/wiki/Neo%20unter%20Windows%20einrichten/kbdneo#WieaktivierenunterWindows10
and install neo. Then you need to add the keyboard layout to your system.
To do this, you have to visit ``Systemsteuerung\Zeit, Sprache und Region\Sprache``.

.. figure:: change_keyboard_layout1.png
	 :alt: change_keyboard_layout1

Here you klick on the ``option`` button and form there enter ``Eingabemethode hinzufügen``

.. figure:: change_keyboard_layout2.png
	 :alt: change_keyboard_layout2

Neo should be listed as a ``Tastatur``

.. figure:: change_keyboard_layout3.png
	 :alt: change_keyboard_layout3

Now you should be able to switch between Neo and the other keyboard layouts using
the button next to date field.

.. figure:: switch_active_keyboard_layout.png
	 :alt: switch_active_keyboard_layout

You can also switch the active keyboard layout using the hotkeys: ``Windows + Space``.
If you press ``Windows + Space`` you can switch though the list by pressing the space bar

.. todo:: Installation of Font, UNICODE Symbols

Linux
=====

.. code-block:: bash

	. /home/user/neo/starte_neo
