=========================
Raspberry PI Kiosk Screen
=========================

To create a raspi kiosk use [Grafana Kiosk](https://github.com/grafana/grafana-kiosk) and add 
the following content to the file `/etc/xdg/lxsession/LXDE-pi/autostart`

```
/usr/bin/grafana-kiosk --URL https://grafana.voize.de/playlists/play/1 --login-method local --username <username> --password <pw> --kiosk-mode full --lxde

```