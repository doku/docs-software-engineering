Terminal
========

Overview for important terminal commands
----------------------------------------

The Backslash ``\`` can be used to escape special characters.

change to super user:

.. code-block:: bash

  #change to super user
  su -

  #logout super user
  exit

Add user to sudo file: http://sharadchhetri.com/2013/08/07/sudo-command-not-found-debian-7/

Festplatten wechseln in der Konsole:

.. code-block:: bash

  cd D:

vi Editor
----------

vim-game: https://vim-adventures.com/

``vi`` (bzw ``vim``) ist der Standart-Editor vieler Unix/Linux Umgebungen.

Nachdem etwas geschrieben wurde kann mit ``esc`` wieder in den normalen Modus gewechselt
werden. Hier kann man durch ``:`` Befehle ausführen:

.. code-block:: bash

  #save and exit
  :x

  #save and exit (override read-only)
  :x!

  #quit
  :q

Löschen von Zeilen mit ``dd`` in der zu löschenden Zeile tippen.
Wurde eine Zeile mit ``dd`` gelöscht, so ist diese noch zwischengespeichert. Durch
drücken von ``p`` an einer anderen Stelle kann die Zeile wieder eingefügt werden.

Möchte man eine Ziele kopieren aber nicht ausschneiden kann man das mit ``yy`` machen.
Durch voranstellen von Zahlen an die Commands kann man die Anzahl der Zeilen erhöhen.
``5dd`` löscht zum Beispiel 5 Zeilen.

Wenn man im normalen Mode ist kann man über ``p`` Elemente aus der Zwischenablage einfügen.

Einen guten Guide zu allen Befehlen findet man hier:
https://getintodevops.com/blog/how-the-hell-do-i-exit-a-beginners-guide-to-vim

Cheatsheet zu vim

https://vim.rtorr.com/


``vimdiff``
~~~~~~~~~~~

Mit dem Kommando ``vimdiff`` können zwei bis vier Dateien mit vi nebeneinander
verglichen werden. Mit ``Strg + w + w``  kann zwischen den Dateien gewechselt
werden.

.. code-block:: bash

  vimdiff file1 file2 file3






Basics
======

Verzeichnisse mit / für absoluten Pfad /mnt

falls kein / vorne dann relativer Pfad
