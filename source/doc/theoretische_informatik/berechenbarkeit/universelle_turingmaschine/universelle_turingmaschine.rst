==========================
Universelle Turingmaschine
==========================

Bislang wurde immer eine neue TM entworfen, um ein spezifisches Problem zu lösen.
Reale Computer können jedoch "programmiert werden" und somit verschiedene Probleme
lösen.

Die **universelle Turingmaschine** :math:`U` simuliert eine beliebige andere
TM :math:`M` auf beliebiger Eingabe :math:`w`. Als Eingabe bekommt :math:`U`
einen String ``<M>w``, hier ist ``<M>`` die Kodierung der zu simulierenden
TM :math:`M`. Die universelle TM simuliert das Verhalten der TM :math:`M` auf
der Eingabe :math:`w` und übernimmt deren Akzeptanzverhalten.

.. _goedelnummer:

Gödelnummer
===========

``<M>`` bezeichnet man als **Gödelnummer**.

.. glossary::

  Gödelnummerierung
    Als Gödelnummerierung bezeichnet man eine :term:`injektiv` Abbildung aus der Menge
    aller TMs in :math:`\{0,1\}^*`

Wir zeigen im Folgenden eine Kodierung für TMs, die eine :term:`Gödelnummerierung` darstellt.

Die zu Kodierende TM habe:

- Zustandsmenge :math:`Q =\{q_1, q_2, ... q_3\}`
- Anfangszustand ist immer :math:`q_1`
- Stopzustand ist immer :math:`q_2`
- Bandalphabet ist :math:`\Gamma = \{\underbrace{0}_{1. \text{Zeichen}}, \underbrace{1}_{2.}, \underbrace{B}_{3.}\}`
- Kopfbewegungen sind ebenfalls nummeriert: L = 1. Bewegung, N = 2. Bewegung, R = 3. Bewegung

Das ist keine Einschränkung, da jede TM in diese Form gebracht werden kann.

Defacto müssen wir also nur die Übergangsfunktion :math:`\delta` kodieren.

Der Übergang

.. math:: \delta(q_i, \underbrace{X_j}_{j \text{-tes Zeichen im Alphabet}}) =
          (q_k, X_l, \underbrace{D_m}_{m \text{-te Bewegung}})

wird kodiert als:

.. math:: 0^i10^j10^k10^l10^m


Sei :math:`code(t)` die Kodierung des t-ten Übergangs. Die Kodierung der gesamten TM ist
dann. ``<M> = code(1)11code(2)11...11 code(s)111``

Die Universelle TM :math:`U` erhält als Eingabe ein Wort :math:`<M>w` und simuliert :math:`M` auf
Eingabe :math:`M` und übernimmt auch das Akzeptanzverhalten von :math:`M`.


Umsetzung der Simulation mittels einer 3-Band TM
=================================================

#. Auf Band :math:`1` von :math:`U` steht das, was :math:`M` auf seinem Band stehen hätte.
#. Auf Band :math:`2` von :math:`U` enthält die Gödelnummen ``<M>`` von :math:`M`.
#. Auf Band :math:`3` speichert :math:`U` den jeweils aktuellen Zustand von :math:`M`.

- Initialisierung.

  - überprüfe, ob ``<M>`` gültige Kodierung einer TM darstellt.
  - Verschiebe Kodierung von ``<M>`` auf Band 2
  - Schreibe Kodierung des Anfangszustands :math:`q_1` auf Band 3

- Rechenschritt: Die Simulation beginnt mit SLK auf erstem Zeichen von :math:`w` auf Band 1

  - Man sucht auf Band 2 den Übergang der zu Zeichen unter dem Kopf auf Band 1 und Zustand auf Band 3 passt
  - aktualisiere entsprechend Bandinhalt auf Band 1
  - bewege Kopf auf Band 1
  - ändere Zustand auf Blatt 3

Unter Annahme, dass ``<M>`` konstant groß ist, kann ein Schritt von M in konstanter
Zeit von :math:`U` simuliert werden.

Wenn wir diese universelle 3 Band TM auf einer 1 Band Tm simulieren, hätte das
zur Folge, dass man durch quadratischen Overhead einen Schritt nicht mehr in
konstanter Zeit durchführen könnten.
Dieser kann jedoch auch vermieden werden.

.. hint::

   Wir könne jede TM :math:`M` auf :math:`U` simulieren. :math:`<M>` entspricht einem
   "Programm."
