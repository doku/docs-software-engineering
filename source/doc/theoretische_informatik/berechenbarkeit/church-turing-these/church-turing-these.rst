===================
Church-Turing-These
===================

.. hint::

  Die Klasse der Probleme, welche *intuitiv berechenbar* sind, ist äquivalent mit der Klasse
  der Probleme, die TM berechenbar bzw. rekursiv sind.

Die Klasse der *intuitiv berechenbaren* Funktionen interpretieren wir als die
Menge aller Funktionen, die durch einen Algorithmus berechnet werden können.

Jeder Algorithmus lässt sich durch eine Turingmaschine realisieren, jede
berechenbare Funktion lässt sich programmieren.

Berechenbarkeit
===============

Berechenbar oder rekursiv heißt eine Funktion :math:`f: M \rightarrow N` dann,
wenn es einen Algorithmus gibt, der für jeden Eingabewert :math:`m \in M`
für den :math:`f(m)` definiert ist, nach endlich vielen
Schritten anhält und das Ergebnis :math:`f(m)` liefert.
