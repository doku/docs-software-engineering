========================
Unentscheidbare Probleme
========================

Postsches  Korrespondenzproblem (PKP)
=====================================

Das PKP (im englischen post's correspondence problem PCP) stellt folgende
Aufgabenstellung:

Gegeben ist eine endliche Folge von Wortpaaren, wobei :math:`x_i, y_i \in \Sigma^+`

.. math:: K = \left\{ \left[\frac{x_1}{y_1}\right], ..., \left[\frac{x_k}{y_k}\right]\right\}

Es soll entschieden werden, ob es eine korrespondierende Folge von Indizes
:math:`i_1, ..., i_n \in \{1, ..., k\}, n \geq 1` gibt, sodass gilt:

.. math:: x_{i_1}x_{i_2}...y_{i_n} = y_{i_1}y_{i_2}...y_{i_n}

Das PKP ist nicht entscheidbar, aber semi-entscheidbar. Das Komplement des
PKP ist nicht semi-entscheidbar.

Das PKP ist bereits unentscheidbar, wenn man sich auf das Alphabet :math:`\{0,1\}`
beschränkt.

Das unäre PKP ist entscheidbar.

Hilberts zehntes Problem
========================
Gegeben sei ein multivariates Polynom :math:`p` (also ein Polynom, mit mehreren
Variablen). Es wird nun die Frage gestellt, ob :math:`p` eine ganzzahlige
Nullstelle hat.

.. math::

  N = \{ p|p \text{ ist ein Polynom mit ganzzahligen Koeffizienten und
      ganzzahliger Nullstelle} \}

Die Sprache :math:`N` ist unentscheidbar.

Unentscheidbare Grammatik Probleme
==================================
