==============
Turingmaschine
==============

Die Turingmaschine ist ein universelles Rechenmodell, welches 1936 von
Alan Turing (1912-1954) eingeführt wurde. Alle Funktionen die von einer
Turingmaschine berechnet werden können, werden Turing-berechenbar (oder
berechenbar) genannt.

Eine Turingmaschine besteht aus einen **unendlichen Arbeitsband** mit Schreib-
und Lesekopf und einer endlichen Zustandskontrolle.

.. figure:: turingmaschine_leseband.png
	 :alt: turingmaschine_leseband

Aktionen in einem Schritt:

    #. Schreiben eines (neuen) Symbols
    #. Ändern des inneren Zustands
    #. schieben des Schreib/Lesekopfs um ein Feld nach links/rechts oder stehen lassen
       :math:`\rightarrow \{L, R, N\}`

Bei einer Turingmaschine gibt es drei verschiedene Berechnungsarten:

-  deterministisch
-  nicht-deterministisch
-  stochastisch

Die "Programmierung" einer TM ist relativ mühsam, dennoch können alle
Sprachkonstrukte aus "normalen" Programmiersprachen dargestellt werden.
z.B.

- eine boolsche Variable im Programm kann in den Zustandsraum kodiert werden.
  Zustände alt: :math:`Q` und Zustände neu :math:`Q \times \{0,1\}`
- Eine Variable :math:`X` mit konstatn vilene Zuständen :math:`\{0,1,...,k\}`
  kann ebenfalls in den Zustandsraum kodiert werden.
  :math:`Q_{neu} = Q \times \{0,1,2,...,k\}`
- man kann auch so tun, als ob die TM Mehrere Spuren hat. :math:`k` Spurige TM
  kann simultan durch 1-Spurige TM mit jeweils einem Bandalphabet
  Nützlich um 2 Binärzahlen zu addieren.
- Konstant viele Variable mit nicht konstanten Wertebereich -> jeweils eine Spur pro
  Variable.
- Unterprogramme und Rekusion duch Reservierung einer Spur für Prozedurstack
- Schleifen wie im Beispiel

Beispiele für TMs
=================

:math:`L=\{w0 | w \in \{0,1\}* \}`

Eine TM, welche :math:`L` entscheidet:

Formale Definition
==================

.. math:: M=(Z, \Sigma, \Gamma, \delta, z_0, \square, E)

- :math:`Z`: endliche Menge von Zuständen, :math:`Z \neq \emptyset`
- :math:`\Sigma`: endliches Eingabealphabet, :math:`\Sigma \cap Z = \emptyset`
- :math:`\Gamma`: endliches Bandalphabet :math:`\Gamma \cap \Sigma = \emptyset \; \Sigma \subseteq \Gamma`
- :math:`\delta`:

  - deterministische Übergangsfunktion:
    :math:`\delta: Z \times \Gamma \rightarrow Z \times \Gamma \times \{L,R,N\}`
  - nicht deterministische Übergangsfunktion:
    :math:`\delta: Z \times \Gamma \rightarrow \mathcal{P}(Z \times \Gamma \times \{L,R,N\})`

- :math:`z_0 \in Z`: Startzustand
- :math:`\square \in \Gamma \backslash \Sigma`: Leersymbol
- :math:`E \subseteq Z`: Menge der akzeptierenden Endzustände.

Interpretation der Übergänge
----------------------------

.. math::
  \delta(z, a) = (z', a', X) \; mit \; z,z' \in Z; \; a, a' \in \Gamma; \; X \in \{L,R,N\}

#. Die Maschine ist im Zustand :math:`z` und liest das Zeichen :math:`a` auf dem Band.
#. Sie ersetzt das Zeichen :math:`a` durch :math:`a'` und wechselt in den Zustand :math:`z'`.
#. Der Schreib/Lesekopf bewegt sich:

  - bei :math:`x=R` um ein Feld nach **rechts**
  - bei :math:`x=N` Kopf bleibt **unverändert**
  - bei :math:`x=L` um ein Feld nach **links**

.. _nondeterministic-turingmaschine:

Nichtdeterministische Turingmaschinen
=====================================

Eine nichtdeterministische Turingmaschine (NTM) ist definiert wie eine deterministische
TM, nur die Zustandsüberführungsfunktion wird zu einer Relation:

.. math:: \delta \subseteq ((Z \backslash E )\times \Gamma) \times (Z \times \Gamma \times \{L,R,N\})

Akzeptanzverhalten einer NTM (Nichtdeterministische TM)
--------------------------------------------------------
Eine NTM :math:`M` akzeptiert eine Eingabe :math:`x \in \Sigma^*`, falls es
**mindestens eine** Sequenz von gültigen Rechenschritten (gemäße der Übergangsrelation)
gibt, die in einer akzeptierenden Konfiguration endet. Die von :math:`M` erkannte
Sprache :math:`L(M)` besteht aus allen von :math:`M` akzeptierten Wörtern.

Laufzeit einer NTM
------------------
Sei :math:`M` eine NTM. Die Laufzeit :math:`T_M(x)` von :math:`M` auf der
Eingabe :math:`x \in L(M)` ist definiert als

.. math::

  T_M(x):= \text{Länge des kürzesten akzeptierenden Rechenwegs von } M \text{ auf } x

Für ein :math:`x \notin L(M)` ist :math:`T_M(x)=0`. Die Worst-Case-Laufzeit
:math:`t_M(n)` für :math:`M` auf Eingabe der Länge :math:`n` ist

.. math::

  t_M(n):= \max{\{ T_M(x) | x \in \Sigma^n}

Konfiguration
=============
Innerhalb einer speziellen, von einer TM durchgeführten Berechnung
beschreiben drei Angaben eindeutig eine Konfiguration:

-  der aktuelle Zustand :math:`q`
-  der Bandinhalt :math:`T: \mathbb{Z} \rightarrow B` wobei :math:`T(i)` dem Zeichen in der Zelle :math:`i`
   entspricht
-  die aktuelle Bandposition :math:`C \in \mathbb{Z}`

Formal: Eine Konfiguration ist ein String :math:`\alpha q \beta` mit :math:`q \in Z`
und :math:`\alpha, \beta \in \Gamma^*`.
Das bedeutet, auf dem Band befindet sich :math:`\alpha`.
Der aktuelle Zustand ist :math:`q` und der SLK befindet sich auf dem ersten Zeichen von :math:`\beta`.

Man nennt :math:`\alpha'q'\beta'` **direkte Nachfolgekonfiguration** von :math:`\alpha q\beta`,
falls :math:`\alpha'q'\beta'` in einem Rechenschritt aus :math:`\alpha q\beta` entsteht.
Man schreibt :math:`\alpha q\beta \vdash \alpha'q'\beta'`.

Man nennt :math:`\alpha''q''\beta''` **Nachfolgekonfiguration** von :math:`\alpha q\beta`,
falls :math:`\alpha''q''\beta''` in endlich vielen Rechenschritten aus :math:`\alpha q\beta` entsteht.
Man schreibt :math:`\alpha q\beta \vdash^* \alpha''q''\beta''`.

Eine Rechnung der TM terminiert, sobald der Endzustand :math:`\bar{q}` erreicht wird.
Die Laufzeit ist definiert als die Anzahl der Rechenschritte, die die TM bis zur Terminierung ausführt.
Falls die Rechnung nicht terminiert, ist die Laufzeit unbeschränkt.
Der Platzbedarf einer TM-Rechnung ist die Anzahl der besuchten Bandzellen (kann auch unbeschränkt sein).


TM-berechenbare Funktionen und Sprachen
=======================================

Eine TM :math:`\mathcal{M}`, die für jede Eingabe terminiert, berechnet eine
totale Funktion :math:`f_\mathcal{M} : \Sigma^* \rightarrow \Sigma^*`.
Wenn der Endzustand :math:`\bar{q}` erreicht ist, kann das Ergebnis
:math:`f_\mathcal{M}(x)` rechts vom SLK vom Band gelesen werden.
Das Ergebnis kann auch :math:`\epsilon` sein, falls der SLK nach der Beendigung
auf einem Zeichen :math:`\notin \Sigma` steht.

Einige TMs terminieren auf manchen Eingaben nicht, man erweitert deshalb :math:`f_\mathcal{M}`
auf:

.. math::
        & f_\mathcal{M} : \Sigma^* \rightarrow \Sigma^* \cup \{ \bot \} \\
        & \bot \rightarrow \textit{nicht definiert}

.. note::

  Eine Funktion :math:`f_\mathcal{M} : \Sigma^* \rightarrow \Sigma^* \cup \{  \bot \}`
  heißt TM-berechenbar bzw. rekursiv, wenn es eine TM :math:`\mathcal{M}` gibt, mit :math:`f_\mathcal{M} = f`.

Diese Definition schließt auch Funktionen ein, die für manche Eingaben keine
wohldefinierte Ausgaben haben.

Im Spezialfall, dass :math:`f : \Sigma^* \rightarrow \{0, 1\}` (das bedeutet,
man hat ein Entscheidungsproblem), soll eine TM eine Eingabe :math:`w \in \Sigma^*`
akzeptieren, wenn sie terminiert und die Ausgabe mit einer :math:`1` beginnt.
Die TM verwirft :math:`w \in \Sigma^*`, falls sie terminiert, aber die Ausgabe
nicht mit einer :math:`1` beginnt
(Die TM wird nun nicht auf jeder Eingabe terminieren).

.. note::

  Eine Sprache :math:`L \subseteq \Sigma^*` heißt TM-entscheidbar bzw rekursiv, wenn es eine TM
  gibt, die auf allen Eingaben hält, die Eingabe :math:`w` akzeptiert falls
  :math:`w \in L`, und die Eingabe verwirft falls :math:`w \notin L`.



Akzeptierte Sprache einer TM
============================


Mehrspurige TM
==============
Bei einer :math:`k`-Spurigen TM handelt es sich um eine TM, bei der das Band in
:math:`k` sogenannten Spuren aufgeteilt ist, d.h. in jeder Bandzelle stehen
:math:`k` Zeichen, die der Kopf gleichzeitig einlesen kann. Das können wir
erreichen, indem wir das Bandalphabet um :math:`k`-dimensionale Vektoren erweitern:

.. math:: \Gamma' := \Sigma \cup \Gamma^k

Konstant viele Variablen mit jeweils konstant vielen möglichen Werten können
durch eine Erweiterung des Zustandsraums in die Zustände kodiert werden.

- Für Booleanvariable: :math:`Q' = Q \times \{0, 1\}`
- Für Zähler mit :math:`k` konstant: :math:`Q' = Q \times \{0,1, ... k\}`

Mehrband TM
===========

Eine :math:`k`-Band-TM ist eine Verallgemeinerung der TM und verfügt über :math:`k`
Speicherbänder mit jeweils einem unabhängigen Kopf. Die Zustandsübergangsfunktion
ist von der Form:

.. math:: \delta: (Q \backslash \{\bar{q}\}) \times \Gamma^k \rightarrow Q \times \Gamma^k \times \{ L,R,N \}^k

Nach Konvention enthält Band 1 die Eingabe/Ausgabe, die anderen Bänder sind
initial mit :math:`B^*` gefüllt.

.. hint:: k-Spur TM hat auf allen Spuren gleichen SLK, Band TM hat indidividulelle SKL

Satz: Eine :math:`k`-Band TM die mit Rechenzeit :math:`t(n)` und Platz :math:`S(n)` auskommt kann
mit einer 1-Band TM :math:`M'` mit Zeitbedarf :math:`O(t^2(n))` und Platzbedarf
:math:`O(s(n))` simuliert werden.


.. todo:: Beispiel

Für einen Rechenschritt der k-Band-TM laufe über den beschiebenen Teil der 2k Spur TM,
sammle die Zeichen unter den SLKs der k Bänder ein (speicher den Zustand)
update der Zeichen und Kopfposition in einem zweiten durchlauf über die Spur.

ANzahl der Beschriebenen Zellten :math:`\leq t(n)`, 1 Schritt der K-Band TM kostet
:math:`O(t(n))` Schritte in der 2k-Spur TM -> O(t^2 (n)) insgesamt
