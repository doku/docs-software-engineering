========================
Rekursive Aufzählbarkeit
========================

.. glossary::

  Aufzähler
    Ein Aufzähler für eine Sprache :math:`L` ist eine TM mit Drucker (Ausgabeband,
    auf dem der Kopf nur nach rechts bewegt werden darf). Ein Aufzähler für :math:`L`
    gibt alle Wörter aus :math:`L` auf dem Drucker aus:

    - Es werden nur Wörter aus :math:`L` ausgedruckt
    - Jedes Wort aus :math:`L` wird irgendwann gedruckt

  rekursiv aufzählbar
    Eine Sprache :math:`L` heißt rekursiv aufzählbar, wenn es einen Aufzähler
    für :math:`L` gibt.

Eine Sprache :math:`L` ist genau dann :term:`semi-entscheidbar`, wenn :math:`L`
:term:`rekursiv aufzählbar` ist.
