=========
Reduktion
=========

Reduktion bildet die Eingabe eines Problems auf ein anderes Problem ab
("Eingabe-Eingabe-Reduktion")

Definition
==========

Es seien :math:`L_1` und :math:`L_2` Sprachen über :math:`\Sigma`. Dann
heißt :math:`L_1` auf :math:`L_2` reduzierbar, Notation :math:`L_1 \leq L_2`,
wenn es einen berechenbare Funktion :math:`f: \Sigma^* \rightarrow \Sigma^*`
gibt, so dass für alle :math:`x \in \Sigma^*` gilt:

.. math:: x \in L_1 \Leftrightarrow f(x) \in L_2


.. figure:: reduction.png
	 :alt: reduction

Die Notation :math:`L_1 \leq L_2` bedeutet, dass :math:`L_1` unter
dem Gesichtspunkt der Berechenbarkeit nicht schwieriger als :math:`L_2`
ist.

Folgerungen
===========

Falls :math:`L_1 \leq L_2` und :math:`L_2` entscheidbar, so ist auch :math:`L_1`
entscheidbar.

Falls :math:`L_1 \leq L_2` und :math:`L_1` unentscheidbar, so ist auch :math:`L_2`
unentscheidbar.

- :math:`A \leq B \Leftrightarrow \bar{A} \leq \bar{B}`
- :math:`A \leq B \wedge B \leq C \Rightarrow A \leq C`
- :math:`A \leq B \wedge A \text{ unentscheidbar } \Rightarrow B \text{ unentscheidbar}`
- :math:`A \leq \bar{B} \wedge A \text{ unentscheidbar aber semi-entscheidbar } \Rightarrow B \nleq A`

Polynomielle Reduktion
======================
:math:`L_1` und :math:`L_2` seien zwei Sprachen über :math:`\Sigma_1` und
:math:`\Sigma_2`. :math:`L_1` ist polynomiell reduzierbar auf :math:`L_2`,
wenn es ein :math:`f: \Sigma_1^* \rightarrow \Sigma_2^*` gibt, dass in
polynomieller Zeit berechnet werden kann mit :math:`x \in L_1 \Leftrightarrow f(x) \in L_2`

Geschrieben: :math:`L_1 \leq_p L_2`

Es gilt: :math:`L_1 \leq_p L_2 \wedge L_2 \in P \Rightarrow L_1 \in P`
