=============
Satz von Rice
=============

TMs halten nicht auf jeder Eingabe, dass bedeutet, sie berechnen partielle
Funktionen, die die folgende Form aufweisen:

.. math::

  f: \{0,1 \}^* \rightarrow \{0,1 \} ^* \cup \{\perp \}


Sei nun die Menge aller Funktionen, die von irgendeiner TM erkannt werden,
definiert als:

.. math:: R = \{ f_M : \{0,1\}^* \rightarrow \{0,1 \} ^* \cup \{\perp \} | M \text{ ist TM} \}

Dann besagt der **Satz von Rice**:

Sei :math:`S` eine Teilmenge von :math:`R` mit :math:`\emptyset \neq S \neq R`
Dann ist die Sprache

.. math:: L(S) = \{ <M> | M \text{ berechnet eine Funktion aus } S \}

unentscheidbar.

.. note::
  Nicht-triviale Eigenschaften der von einer TM berechneten Funktion sind
  unentscheidbar.

Folgerungen
===========

Aus dem Satz von Rice folgt direkt, dass man nicht entscheiden kann, ob ein
Programm auf jeder Eingabe hält:

.. math:: 0 \neq S = \{f:\{0,1\}^* \rightarrow \{0,1\}^*\} \neq R
