=========
Überblick
=========

Eine Sprache ist :term:`rekursiv aufzählbar` genau dann, wenn sie :term:`semi-entscheidbar`
ist.

Eine Sprache ist genau dann :term:`rekursiv`, wenn sie :term:`entscheidbar` ist.



Das spezielle Halteproblem ist nicht :term:`entscheidbar`.

Abschlusseigenschaften
======================

Abschluss unter Schnitt
-----------------------

Wenn die Sprachen :math:`L_1` und :math:`L_2` entscheidbar/rekursiv sind, so
ist auch die Sprache :math:`L_1 \cap L_2` entscheidbar/rekursiv.

Wenn die Sprachen :math:`L_1` und :math:`L_2` semi-entscheidbar/rekursiv aufzählbar
sind, so ist auch die Sprache :math:`L_1 \cap L_2` semi-entscheidbar/rekursiv
aufzählbar.

Abschluss unter Union
---------------------
Wenn die Sprachen :math:`L_1` und :math:`L_2` entscheidbar/rekursiv sind, so
ist auch die Sprache :math:`L_1 \cup L_2` entscheidbar/rekursiv.

Wenn die Sprachen :math:`L_1` und :math:`L_2` semi-entscheidbar/rekursiv aufzählbar
sind, so ist auch die Sprache :math:`L_1 \cup L_2` semi-entscheidbar/rekursiv
aufzählbar.

.. hint::
  Da :math:`M_1` und :math:`M_2` die Sprachen :math:`L_1` und :math:`L_2` nur
  :term:`erkennen`, aber nicht :term:`entscheiden`, kann beim Sequentiellen Ausführen der
  Fall auftreten, das :math:`M_2` die Eingabe akzeptieren würde, aber :math:`M`
  das nie feststellt, da :math:`M_1` nicht terminiert.

  Daher können :math:`M_1` und :math:`M_2` nicht sequentiell simuliert werden,
  sondern müssen parallel auf einer 2-Band TM :math:`M` parallel simuliert werden.

Abschluss unter Komplement
--------------------------

Sei :math:`L` ein unentscheidbare Sprache. Dann ist auch :math:`\bar{L} = \Sigma* -L`,
das Komplement von :math:`L` auch unentscheidbar.

Im Gegensatz zur Menge der entscheidbaren Sprachen ist die Menge der
semientscheidbaren/rekursiv aufzählbaren Sprachen nicht gegen Komplementbildung
abgeschlossen. Aus :math:`L` semi-entscheidbar kann nicht gefolgert werden
:math:`\bar{L}` semi-entscheidbar.

Eine Sprache ist :term:`entscheidbar` genau dann, wenn sowohl :math:`A`
als auch :math:`\bar{A}` :term:`semi-entscheidbar` sind.

Sprachen
========

Das Wortproblem für Typ-1 Sprachen ist :term:`NP-hart`. Genauer gesagt ist es
sogar DSPACE-vollständig.
