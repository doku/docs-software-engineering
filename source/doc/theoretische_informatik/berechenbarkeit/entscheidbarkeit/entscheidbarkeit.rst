================
Entscheidbarkeit
================

.. glossary::

  entscheidbar
    vergleiche :term:`Entscheidbarkeit`

  entscheiden
    vergleiche :term:`Entscheidbarkeit`

  Entscheidbarkeit
    Eine Menge :math:`A \subseteq \Sigma^*` heißt **entscheidbar**, falls die
    charakteristische Funktion von :math:`A`, nämlich :math:`\chi_A: \Sigma^* \rightarrow \{0,1\}`,
    berechenbar ist. Hierbei ist für alle :math:`w \in \Sigma^*`:

    .. math::

      \chi_A(w)=
        \begin{cases}
        1, w \in A \\
        0, w \notin A
        \end{cases}

  erkennen
    Eine Sprache :math:`L` wird von einer TM :math:`M` erkannt, wenn :math:`M`
    jedes Wort aus :math:`L` akzeptiert und :math:`M` kein Wort akzeptiert, das
    nicht in :math:`L` ist. Auf Eingaben, die nicht in :math:`L` sind, muss :math:`M`
    nicht halten.

    :math:`L(M)` ist die von :math:`M` erkannte Sprache, also :math:`L(M) := \{w \in \Sigma^{*} | M\text{ akzeptiert }w\}`.

  semi-entscheidbar
    Eine Menge :math:`A \subseteq \Sigma^*` heißt **semi-entscheidbar**, falls die halbe
    charakteristische Funktion von :math:`A`, nämlich :math:`\chi'_A: \Sigma^* \rightarrow \{0,1\}`,
    berechenbar ist. Hierbei ist für alle :math:`w \in \Sigma^*`:

    .. math::

      \chi_A(w)=
        \begin{cases}
        1, w \in A \\
        \text{undefiniert }, w \notin A
        \end{cases}

    Eine Sprache ist rekursiv aufzählbar, genau dann, wenn sie semi-entscheidbar
    ist.

.. figure:: unentscheidbarkeit_entscheidbarkeit_semi.png
	 :alt: unentscheidbarkeit_entscheidbarkeit_semi

- wenn entscheidbar, dann auch semi-entscheidbar und cosemi-entscheidbar
- wenn semi-entscheidbar, dann nicht zwingend entscheidbar
- wenn unentscheidbar, dann nicht entscheidbar
- wenn nicht semi-entscheidbar, dann unentscheidbar
