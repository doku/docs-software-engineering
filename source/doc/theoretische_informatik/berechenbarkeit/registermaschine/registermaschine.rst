.. _registermaschine:

======================
Registermaschine (RAM)
======================

Die  Registermaschine (RAM) ist ein Maschinenmodell, das der Assemblersprache
eines modernen Rechners recht nahe kommt. *RAM* steht hier für *Random Access Machine*

Der Speicher der RAM ist unbeschränkt und besteht aus Registern :math:`c(i)`.
Der Register :math:`c(0)` heißt Akkumulator. Rechenbefehle nutzen den Inhalt
des Akkumulators als implizites Argument. Das Ergebnis von Rechenoperationen wird
im stets Akkumulator gespeichert. Die Inhalte der Register sind beliebig große
ganze Zahlen.

Der Befehlszähler steht initial auf :math:`1`.

.. figure:: ram_grafik.png
	 :alt: ram_grafik

Eine RAM arbeitet ein endliches Programm mit durchnummerierten Befehlen
ab. In jedem Schritt wird der Befehl in Programmzeile :math:`b` abgearbeitet.

Beim Befehl ``GOTO i`` wird der Befehlszähler auf :math:`i` gesetzt. Beim Befehl
``END`` stoppt die Rechnung.

.. figure:: ram_befehle_syntax_semantik.png
	 :alt: ram_befehle_syntax_semantik

Die Eingabe steht am Anfang der Rechnung in den Registern :math:`c(1), ..., c(k)`
fr ein :math:`k \in \mathbb{N}`. Die Ausgabe befindet sich nach dem Stoppen in den Registern
:math:`c(1), ...,  c(k)` für ein :math:`l \in \mathbb{N}`.
Der Einfachheit halber nehmen wir an, dass :math:`k` und :math:`l`
zu Beginn der Rechnung festliegen. Die RAM berechnet somit eine Funktion der
Form :math:`f : \mathbb{Z}^k \rightarrow \mathbb{Z}^l \cup \{ \perp\}`,
wobei das Zeichen :math:`\perp` wie bei der TM für eine nicht
terminierende Rechnung steht.

Laufzeitmodelle
===============

.. glossary::

  Uniformes Kostenmaß
    Jeder Schritt  zählt eine Zeiteinheit

  Logarithmisches Kostenmaß
    Die Laufzeitkosten eines Schrittes sind proportional zur binären Länge der
    Zahlen in den angesprochenen Registern.


Äquivalenz TM und Registermaschine
==================================

Jede im logarithmischen Kostenmaß :math:`t(n)`-zeitbeschrärkte RAM kann für
ein Polynom :math:`q` durch eine :math:`O(q(n+t(n)))`-zeitbeschränkte TM
simuliert werden.

Jede :math:`t(n)`-zeitbeschränkte TM kann durch eine RAM simuliert werden, die
uniform :math:`O(t(n)+n)` und logarithmisch :math:`O((t(n)+n) \log(t(n)+n))`
zeitbeschränkt ist.

.. hint::

  Die Mehrband-TM kann mit quadratischem Zeitverlust durch eine (1-Band)
  TM simuliert werden. TMs und RAMs (mit logarithmischen Laufzeitkosten)
  können sich gegenseitig mit polynomiell beschränktem Zeitverlust simulieren.
