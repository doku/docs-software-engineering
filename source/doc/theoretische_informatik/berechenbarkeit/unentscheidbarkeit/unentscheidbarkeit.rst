====================
Unentscheidbarkeiten
====================

Eine TM M entscheidet eine Sprache :math:`L \subseteq \Sigma^*`, falls m auf einer
Eingabe :math:`w \in \Sigma^*` hält und genau dann akzeptiert wenn :math:`w \in L`
und sonst verwirft.

.. glossary::

  abzählbare Menge
    Eine Menge :math:`M` heißt abzählbar, falls es eine surjektive Funtion
    :math:`C: \mathbb{N} \rightarrow M` gibt. Nichtabzählbare Mengen heißten
    überabzählbar.

Im Fall einer abzählbar unendlichen Menge :math:`M` gibt es immer auch eine bijektive
Abbildung :math:`C: \mathbb{N} \rightarrow M`, da Wiederholungen übersprungen werden
können. So eine Abbildung kann auch als Nummerierung der Menge :math:`M` interpretiert werden.

Folgende Mengen sind abzählbar:

- Menge der ganzen Zahlen :math:`Z`
- Menge der Wörter über :math:`\{0,1\}^*`

.. note::

  Die Menge der TMs ist **abzählbar**, weil jede TM durch eine :ref:`goedelnummer`
  beschrieben wird, und die Menge der Gödelnummern eine Teilmenge der Wörter über
  :math:`\{0,1\}^*` ist.

Satz: Die Menge :math:`\mathcal{P}(\mathbb{N})` ist überabzählbar.

Die Menge der Sprachen über dem Alphabet :math:`\{0,1\}` ist überabzählbar.
Da Menge Sprachen entsprechen der Potenzmenge der Menge aller Wörter über {0,1}.

.. hint::
  Es gibt nur abzählbarviele TMs, aber überabzählbare verschiedene Sprachen.
  Es gibt echt Mehr Sprachen als TMs. Damit existieren Sprachen, die
  unentscheidbar sind.

Explizite Konstruktion einer Sprache, die von keiner Turingmaschine
entschieden werden kann.

Unentscheidbarkeit der Diagonal Sprache :math:`D`
=================================================
Wir definieren die sog. Diagonalsprache :math:`D` wie folgt:

.. math::

  D := \{w \in \{0,1\}* | w = w_i \wedge M_i \text{ akzeptiert w nicht }\}

Oder in anderen Worten: das :math:`i`-te Wort ist in :math:`D`, genau dann wenn die :math:`i`-te
TM es nicht akzeptiert.

.. figure:: diagonalsprache.png
	 :alt: diagonalsprache

.. math:: D = \{w_i | A_{i,i} = 0\}

**Theorem:** die Diagonalsprache :math:`D` ist nicht entscheidbar.

**Beweis:** (durch Widerspruch)

Sei :math:`M_j` eine TM, welche :math:`D` entscheidet. Wir lassen :math:`M_j` auf
Eingabe :math:`w_j` laufen.

1. Fall:
   :math:`M_j` aktzeptiert :math:`w_j \Rightarrow w_j \in D`
   aber dadurch auch :math:`A_{j,j} = 1 \Rightarrow w_j \notin D` (Widerspurch)

2. Fall:
   :math:`M_j` aktzeptiert nicht :math:`w_j \Rightarrow w_j \notin D`
   aber dadurch auch :math:`A_{j,j}  = 0 \Rightarrow w_j \in D` (Widerspruch)

Eine solche TM kann es also nicht geben, damit ist :math:`D` unentscheidbar.

Sei :math:`L` ein unentscheidbare Sprache. Dann ist auch :math:`\bar{L} = \Sigma^* -L`,
das Komplement von :math:`L` auch unentscheidbar.

Beweis: Nutze Maschine, die :math:`\bar{L}` entscheidet, um :math:`L`
zu entscheinen, in dem man ``Ja`` und ``Nein`` vertauscht. Falls :math:`\bar{L}`
entscheidbar, ist auch :math:`L` entscheidbar.
