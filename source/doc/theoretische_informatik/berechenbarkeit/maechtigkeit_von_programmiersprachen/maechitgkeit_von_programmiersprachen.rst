===================================
Mächtigkeit von Programmiersprachen
===================================

.. glossary::

  Turing-mächtig
    Eine Programmiersprache wird als **Turing-mächtig** bezeichnet, wenn jede berechenbare
    Funktion auch durch ei Programm in dieser Programmiersprache berechnet werden
    kann.

``WHILE``-Programme
===================

Die Programmiersprache ``WHILE`` ist :term:`Turing-mächtig`.

``LOOP``-Programme
==================

Die Ackermannfunktion ist nicht ``LOOP``-berechenbar.

Es gibt totale, ``WHILE``-Berechenbare Funktionen, die nicht ``LOOP``-Berechenbar
sind.
