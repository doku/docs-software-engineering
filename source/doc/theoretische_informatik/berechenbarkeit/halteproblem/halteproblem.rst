============
Halteproblem
============

Unentscheidbarkeit des Halteproblems
====================================

Halteproblem: Gegeben ein Programm und eine Eingabe für das Programm, entscheide,
ob Programm auf dieser Eingabe terminiert.

.. math::

  H = \{<M>w | M \text{ hält auf }w\}

Anmerkung:
  Universelle TM :math:`U` kann :math:`H` nicht entscheiden. Zwar könnte für
  alle :math:`<M>w \in H` die TM :math:`U` irgendwann ``Ja`` sagen, aber nie mit Sicherheit
  ``NEIN``.

.. note:: Das Halteproblem/die Sprache :math:`H` ist unentschiedbar.

Beweis: Annahme :math:`H` ist entscheidbar mittels einer TM :math:`M_H`

Beweisidee mittels Reduktion: Konstruiere aus :math:`M_H` eine TM :math:`M_{\bar{D}}`
welche :math:`\bar{D}` (und damit auch :math:`D`) entscheidet. So eine TM
:math:`M_{\bar{D}}` kann es aber nicht geben, damit kann es :math:`M_H` nicht geben

Beschreibung von :math:`M_{\bar{D}}`

1. Auf der Eingabe :math:`w` berechne i mit :math:`w = w_i`
2. Berechne Kodierung :math:`<M_i>` der i-ten TM
3. Starte :math:`M_H` als Unterprogramm auf die Eingabe :math:`<M_i>w`
4. Falls :math:`M_H` akzeptiert, simuliere :math:`M_i` auf :math:`w` (wie
   die universelle TM :math:`U`) übernehme Ausgabe
5. Falls :math:`M_H` verwirft, verwerfe Eingabe.

Korrektheit von :math:`M_{\bar{D}}`
-----------------------------------
:math:`w \in M_{\bar{D}}\Rightarrow M_H` akzeptiert :math:`<M_i>w` (3.) (da :math:`M_i`)
das w= w_i akzeptiert und dann auch stoppt) und M_i akzeptiert w (4.)
:math:` \Rightarrow M_{\bar{D}}` akzeptiert :math:`w`

Das Spezielle Halteproblem
==========================
Das Spezielle Halteproblem ist definiert als

.. math:: H_{\epsilon} = \{ <M> | M \text{ hält auf der Eingabe } \epsilon\}

.. note::
  Das Spezielle Halteproblem :math:`H_{\epsilon}` ist unentscheidbar und
  semi-entscheidbar.

Beweis:
  Man nimmt an, es gibt eine TM :math:`M_{\epsilon}` die :math:`H_{\epsilon}`
  entscheidet. Dann kann man :math:`M_{\epsilon}` als Unterprogramm einer TM
  genutzt werden, die :math:`H` entscheidet. So eine :math:`M_{\epsilon}` kann
  es aber nicht geben. Damit muss :math:`H_{\epsilon}` unentscheidbar sein.

Das Allgemeine Halteproblem
===========================
Die Sprache :math:`H_{all}` ist definiert als:

.. math:: H_{all} = \{<M> | M \text{ hält auf alle Eingaben}\}

:math:`\bar{H_{all}}` ist nicht semi-entscheibar.

:math:`H_{all}` ist nicht semi-entscheibar,
