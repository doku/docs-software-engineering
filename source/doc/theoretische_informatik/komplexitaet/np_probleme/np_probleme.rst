===========
NP Probleme
===========

.. glossary::

  Cliquenproblem
    Sei Graph :math:`G=(V,E)` und :math:`k \in \{1,..., |V|\}`. Gibt es eine
    :math:`k`- :term:`Clique`.

    :term:`Cliquenproblem` ist :math:`\in NP`

  Coloring
    Sei :math:`G=(V,E)` (ungerichtet) gegeben und :math:`k \in \{1,...,|V|\}`.
    Gibt es eine Färbung :math:`c: V \rightarrow \{1, ..., k\}` der Knoten in
    :math:`G` mit :math:`k`-Farben, sodass benachbarte Knoten nie dieselbe
    Farbe haben?

  SAT
    Bei SAT (Satisfiability) sei eine aussagenlogische Formel :math:`\delta` in
    KNF gegeben. Ist :math:`\delta` erfüllbar.

    :term:`SAT` :math:`\leq_p` :term:`DHC`

  3SAT
    Bei 3SAT oder 3KNF-Sat Ist eine Boolesche Formel :math:`F` in konjunktiver
    Normalform (:term:`KNF` Klauselform) mit höchstens :math:`3` Literalen
    pro Klausel gegeben. Ist :math:`F` :term:`erfüllbar`?

  Subset-Sum
    Auch bekannt als :term:`Rucksack`

  Rucksack
    Gegeben: :math:`a_1, a_2, ..., a_k \in \mathbb{N}` und :math:`b \in \mathbb{N}`.
    Gefragt: gibt es eine Teilmenge :math:`K \subseteq \{1,...,k\}` mit
    :math:`\sum_{i\in K} a_i=b`

  Partition
    Eingabe: :math:`a_1, ..., a_N \in \mathbb{N}`
    Frage: Gibt es eine Teilmenge :math:`K \subseteq \{1,2,...,k\}` mit
    :math:`\sum_{i\in K} a_i = \sum_{i \notin K} a_i`

    Partition ist ein Spezialfall von :term:`Rucksack`, da die gestellte Frage
    äquivalent zur Frage ist, ob eine Teilmenge :math:`K` mit Summenwert
    :math:`\frac{1}{2} \sum_{i=1}^{N} a_i` gibt.

  Bin Backing
    Bin Packing oder BPP

    **Entscheidungsvariante**:
    Gegeben ist die "Behältergröße" :math:`b \in \mathbb{N}` und
    die Anzahl der Behälter :math:`k \in \mathbb{N}`, "Objekte"
    :math:`a_1, ...,a_n \leq b`. Gefragt ist, ob die Objekte so auf die
    :math:`k` Behälter verteilt werden können, dass kein Behälter
    überläuft. D.h. gefragt ist, ob eine Abbildung
    :math:`f: \{1,...,n\} \rightarrow \{1, ...,k\}` existiert, so dass für alle
    :math:`j = 1,...,k` gilt :math:`\sum_{f(i)=j} a_i \leq b`

    **Optimierungsvariante**:
    Gegeben :math:`b \in \mathbb{N}` und :math:`a_1, ..., a_n \leq b`. Gesucht
    ist eine Funktion :math:`f: \{1,...,n\} \rightarrow \{1, ...,k\}`, sodass
    für alle :math:`i \in \{1,...k\}` gilt:

    .. math:: \sum_{i \in f^{-1}(j)} w_i \leq b

    und :math:`k` minimal.

  TSP
    TSP oder Travelling Salesman

    Gegeben sei ein vollständiger gerichteter Graph mit :math:`N`-Knoten. Es
    soll eine Permutation :math:`\pi` der Knoten gefunden werden, sodass die
    die Rundreise zwischen den Knoten möglichst billig ist. Also ein :math:`\pi`
    sodass folgende Gleichung minimal ist:

    .. math:: \sum_{i=0}^{n-1} c(\pi(i), \pi(i+1)) + c(\pi(n),\pi(1))

    :term:`TSP` ist :term:`NP-hart`

  HC
    Hamiltonian Circuit oder Hamiltonkreis

    Gegeben ein Graph :math:`G=(V,E)`. Gibt es einen :ref:`hamilton-kreis` in :math:`G`?

  DHC
    Directed :term:`HC`, Gerichteter :ref:`hamilton-kreis`.

    Gegeben einen gerichteten Graph :math:`G=(V, E)`. Gibt es einen Hamiltonkreis
    in :math:`G`.

Komplexitätsklassen
===================

- :term:`HC` :math:`\leq_p` :term:`DHC` und :term:`DHC` :math:`\leq_p` :term:`HC`
- :term:`HC` und :term:`DHC` sind :term:`NP-vollständig`
- :term:`HC` ist :math:`\leq_p` :term:`TSP`

Für jedes Entscheidungsproblem :math:`L \in NP` gib
