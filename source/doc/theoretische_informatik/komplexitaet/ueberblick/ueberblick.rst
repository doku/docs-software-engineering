=========
Überblick
=========

Sprachen
========

Das Wortproblem für Typ-1 Sprachen ist :term:`NP-hart`. Genauer gesagt ist es
sogar DSPACE-vollständig.
