===================
Komplexitätsklassen
===================

Komlexitätsklasse :math:`P`
===========================

Die **worst case Laufzeit** :math:`t_A(n), n \in \mathbb{N}` eines Algorithmus :math:`A`
entspricht den maximalen Laufzeitkosten auf Eingaben der Länge :math:`n` bezüglich
des logarithmischen Kostenmaßes (:term:`logarithmisches Kostenmaß`)
der :ref:`registermaschine`.

Wir sagen, die worst case Laufzeit :math:`t_A(n)` eines Algorithmus :math:`A`
ist **polynomiell beschränkt**, falls gilt:

.. math:: \exists \alpha \in \mathbb{N}: t_A(n) = O(n^\alpha)

Einen Algorithmus mit polynomiell beschränkter worst case Laufzeit bezeichnen wir
als **Polynomialzeitalgorithmus**.

Ein Problem :math:`X` ist in der Komplexitätsklasse :math:`\mathcal{P}`, wenn
es einen Polynomzeitalgorithmus für :math:`X` gibt.

Ein Problem :math:`X` ist in der Komplexitätsklasse :math:`\mathcal{P}`, wenn
es eine TM :math:`M` gibt, die :math:`X` in einer polynomiellen Anzahl an
Schritten löst.

Sortieren, Minimaler Spannbaum, kürzeste Wege etc. sind Probleme :math:`\in P`.


Komlexitätsklasse :math:`NP`
============================

:math:`\mathcal{NP}` (**nichtdeterministisch Polynomiell**) ist die Klasse
der Entscheidungsprobleme, die durch einen
NTM (:ref:`nondeterministic-turingmaschine`) :math:`M` erkannt werden, wobei die
Worst-Case-Laufzeit :math:`t_M(n)` polynomiell in :math:`n` beschränkt ist.

Eine Sprache :math:`L \subseteq \Sigma^*` ist genau dann in :math:`NP`, wenn es
einen Polynomialzeitalgorithmus :math:`V` (einen sogenannten Verifizierer) und
ein Polynom :math:`p` mat der folgenden Eigenschaft gibt:

.. math:: x \in L \Leftrightarrow \exists y \in \{0,1\}^*, |y| \leq p(|x|): V \text{ akzeptiert } y\#x

Alle Sprachen in :math:`NP` können in exponential Zeit deterministisch entschieden
werden.

.. figure:: komplexitaets_klassen.png
	 :alt: komplexitaets_klassen

.. figure:: komplexitaetsklassen_ueberblick.png
	 :alt: komplexitaetsklassen_ueberblick

.. glossary::

  NP-hart
    Ein Problem :math:`L` heißt :math:`NP`-hart (oder :math:`NP`-schwer), falls
    gilt:

    .. math:: \forall L' \in NP: L' \leq_p L

  NP-vollständig
    Ein Problem heißt :math:`NP`-Vollständig, falls :math:`L \in NP` und
    :math:`L` ist :term:`NP-hart`

    NPC ist die Klasse der :math:`NP`-vollständigen Probleme.

.. note:: Ist :math:`L` :term:`NP-hart` und :math:`L \in P`, dann gilt: :math:`P = NP`
