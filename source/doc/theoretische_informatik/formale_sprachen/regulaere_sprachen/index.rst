=================
Reguläre Sprachen
=================

.. toctree::
  :maxdepth: 2
  :glob:

  regulaere_sprachen/*
  endliche_automaten/*
  myhill_nerode/*
