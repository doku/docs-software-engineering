.. _endlicher-automat:

==================
Endliche Automaten
==================

Deterministischer endlicher Automat (DFA)
=========================================

.. math:: M = (Z, \Sigma, \delta, z_0, E)

- :math:`Z`: endliche Zustandsmenge
- :math:`\Sigma`: Alphabet mit :math:`Z \cap \Sigma = \emptyset`
- :math:`\delta`: Übergangsfunktion :math:`\delta : Z \times \Sigma \rightarrow Z : \delta(z,a)=z'`
- :math:`z_0`: Startzustand :math:`z_0 \in Z`
- :math:`E`: Akzeptierende Endzustände :math:`E \subseteq Z`

.. math::
  \hat{\delta}: Z \times \Sigma^* \rightarrow Z

  \hat{\delta}(z,ax) = \hat{\delta}(\delta(z,a), x)

Die vom Automaten :math:`M = (Z, \Sigma, \delta, z_0, E)` akzeptierte Sprache
:math:`T(M)` ist gegeben durch:

.. math:: T(M) = \{w \in \Sigma^* | \hat{\delta}(z_0, w) \in E\}

Nicht Deterministischer endlicher Automat (NFA)
===============================================

.. math:: M = (Z, \Sigma, \delta, S, E)

- :math:`S`: Menge der Startzustände :math:`S \subseteq Z`
- :math:`\delta: Z \times \Sigma \rightarrow \mathcal{P}(Z)`

.. math::

  \hat{\delta}: \mathcal{P}(Z) \times \Sigma^* \rightarrow \mathcal{P} \\
  \hat{\delta}(Z', \varepsilon) = Z' \forall Z' \subseteq Z \\
  \hat{\delta}(Z', ax) = \bigcup_{z \in Z'} \hat{\delta} (\delta(z,a),x)

Akzeptierte Sprache des NEA :math:` M = (Z, \Sigma, \delta, S, E)`:

.. math:: T(M) = \{w \in \Sigma^* | \hat{\delta}(S, w) \cap E \neq \emptyset \}

Satz von Rabin und Scott
========================
