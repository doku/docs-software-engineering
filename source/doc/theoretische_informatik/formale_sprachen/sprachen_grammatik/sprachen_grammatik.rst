========================
Sprachen und Grammatiken
========================

Künstliche Sprachen haben ein festes, endliches Grundvokabular und einen
festen Syntax und Semantik.

.. glossary::

  Syntax
    Die **Syntax** einer Sprache bestimmt, welche Aussagen zulässig sind.
    The **sytnax** of a program is the structure and the form if its text.

  Semantik
    Die **Semantik** ist die Bedeutung der zulässigen Aussagen.
    The **semantics** of a program is the set of properties of its potential execution.
    (:ref:`ref-semantik-types`)

  Pragmatik
    Die Bedeutung des Programms über ihren formalen Sinn hinaus ist die **Pragmatik**

.. figure:: Sprachen.png
   :alt: Sprachen

Formale Sprachen
================

Eine linear geordnete Zeichenmenge ist ein **Alphabet**.

Sei :math:`\Sigma` ein Alphabet. Eine **formale Sprache** :math:`L` über :math:`\Sigma`
ist eine beliebige Teilmenge von :math:`\Sigma^*`:

.. math:: L \subseteq \Sigma^*

Eine Grammatik :math:`G` erzeugt eine Sprache :math:`L`. Eine Maschine :math:`M` erkennt
eine Sprache :math:`L`. Grammatiken sind daher Erzeugendensysteme bezeichnet
und Maschinen werden häufig als Erkennungssystem verwendet.

Eine Grammatik legt den Syntax einer Sprache fest.

.. note::

  Jede Sprache (über einem endlichen Alphabet) ist abzählbar, es gibt aber
  überabzählbar viele verschiedene Sprachen.

Grammatik
=========

Die Grammatik :math:`G`  definiert man durch das Viertupel:

.. math::  G = (V, \Sigma, S, P)

Chomsky Grammatiken
-------------------

Ein Grammatik :math:`G = (V, \Sigma, S, P)` heißt Chomsky-Grammatik, wenn gilt:

-  :math:`V` ist eine endliche, nichtleere Menge, die **Variable**
-  :math:`\Sigma` ist eine endliche, nichtleere Menge, die **Terminalsymbole**
   mit :math:`V \cap \Sigma = \emptyset`
-  :math:`S` ist das Startsymbol mit :math:`S \in V`
-  :math:`P` ist die Menge der Produktionsregeln mit
   :math:`P \subseteq (V \cup \Sigma)^+ \times (V \cup \Sigma)^*`

Die von einer Grammatik erzeugte Sprache ist

.. math:: L(G)= \{ w \in \Sigma^* \; | \; S \rightarrow^*_G \}

Chomsky Hierarchie
------------------

.. figure:: ChomskyHirarchie.png
   :alt: Chomsky Hirarchie

   Chomsky Hirarchie

Typ 0 - Phrasenstruktur-Grammatiken
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Alle Grammatiken sind Typ 0. Es gibt Einschränkungen ihrer Porduktionsregeln.

Typ 0 Sprachen sind die Klasse der semi-entscheidbaren Sprachen und könne durch
Turingmaschinen erkannt werden.

Entscheidbare Sprachen
~~~~~~~~~~~~~~~~~~~~~~
Menge aller Entscheidbaren Sprachen. Diese Sprachklasse ist nicht teil
der ursprünglichen Chomsky Hierarchie.

Typ 1 - Kontextsensitive Grammatiken
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Typ 1 Sprachen können durch linear beschränkte, nichtdeterministische
Turingmaschinen erkannt werden (LBA).

.. math::
  \alpha\beta\gamma \rightarrow \alpha\lambda\gamma \; \; \; \beta \in V,
  \alpha,\gamma \in (T \cup V)^*, \lambda \in (T \cup V)

Kontextsensitiven Grammatiken sind äquivalent mit den nicht-verkürzenden
Grammatiken.

.. math::
  \forall (u, v) \in P : |u| \leq |v| \longrightarrow  P  \subseteq V \times (V \cup \Sigma)^*

Typ 1 Sprachen: :math:`L= \{ a^n b^n c^n | n \in \mathbb{N}_0 \}`

Soll :math:`\epsilon` in der Sprache enthalten sein, muss die
:math:`\epsilon`-Sonderregel (neue Regel: :math:`S' \rightarrow \epsilon`) angewendet werden.

Typ 2 - Kontextfreie Grammatiken
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Typ 2 Sprachen könne durch Kellerautomaten erkannt werden. Die Grammatik
Regeln von kontextfreien Grammatiken können durch BNF, EBNF und CNF
dargestellt werden.

.. math::  \forall (u,v) \in P : \; p \in V, \lambda \in (T \cup V)^+

In den Produktionsregeln steht links nur ein Nichtterminal.

Typ 2 Sprachen: :math:`L=\{ ww^R \}`

Deterministisch kontextfreie Sprachen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Diese Sprachklasse wird durch deterministische Kellerautomaten erkannt.
Sie kann durch die sogenannten (LR(k))-Grammatiken dargestellt werden.

.. math::
  L = \{ a^n b^n | n \in \mathbb{N} \}
  L = \{ w\$w^R \}

Typ 3 - Reguläre Grammatiken
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Die Sprachklasse der regulären Sprachen ist äquivalent mit der Menge der
durch **reguläre Ausdrücke** beschreibbaren Sprachen. Typ 3 wird durch
NEAs und DEAs erkannt.

.. math::
  \forall (u,v) \in P : v \in  \Sigma \cup \Sigma V
  \longrightarrow P \subseteq V \times (\Sigma \cup \Sigma V)


rechtsreguläre Grammatiken: :math:`p \rightarrow \lambda \; p \in V,
\lambda \in T \cup TV`

Linksreguläre Grammatiken: :math:`p \rightarrow \lambda \; p \in V,
\lambda \in T \cup VT`

Entscheidbarkeiten
==================

-  Das **Wortproblem** ist für kontextsensitive Sprachen (Typ-1)
   entscheidbar
-  Das **Leerheitsproblem** ist für kontextfreie Sprachen (Typ-2)
   entscheidbar
-  Das **Endlichkeitsproblem** ist für kontextfreie Sprachen (Typ-2)
   entscheidbar
-  Das **Äquivalenzproblem** ist für reguläre Sprachen (Typ-3)
   entscheidbar

.. figure:: entscheidbarkeiten.png
	 :alt: entscheidbarkeiten

Wortproblem
===========

.. figure:: komplexitaet_wortproblem.png
	 :alt: komplexitaet_wortproblem

Abschlusseigenschaften
======================

.. figure:: abschlusseigenschaften.png
	 :alt: abschlusseigenschaften


Mehrdeutigkeit
==============
Eine Grammatik, die für mindestens ein Wort zwei Syntaxbäume hat, heißt
**mehrdeutige Grammatik**. Wenn es für jedes Wort eine einzige eindeutig erzeugte
Linksableitung gibt (nur einen Syntaxbaum), ist die Grammatik **eindeutig**.

Eine Typ-2 Sprache heißt **inhärent mehrdeutig**, wenn jede Grammatik, die die
Sprach erzeugt mehrdeutig ist.

Typ-3 Sprachen sind immer eindeutig, da man jeden DEA in eine eindeutige Grammatik
wandeln kann.
