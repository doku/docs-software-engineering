========================
EBNF und Syntaxdiagramme
========================

EBNF und BNF sind kompakte formale Metasprachen zur Darstellung von
kontextfreien Grammatiken (Typ 2).

.. figure:: NotationsuebersichtProduktionen.png
   :alt: Notationsübersicht der Produktionsregeln

   Notationsübersicht der Produktionsregeln

BNF
~~~

-  ist definiert als (:math:`::=`)
-  oder :math:`|`
-  Nichtterminalsymbole :math:`<Summe>`
-  Terminalsymbole :math:`a`

.. math:: <Summe> ::= <Summand> | <Summand> + <Summe>

EBNF
~~~~

-  ist definiert als ::math:`=`
-  genau eine Alternative :math:`(...|...)`
-  Inhalt ist optional :math:`[...]`
-  Inhalt n-fach :math:`n \ge 0 ) ...`
-  Ende der Produktion :math:`;`
-  Nichtterminalsymbole :math:`Summe`
-  Terminalsymbole :math:`("a")`

::

    methodDeclaration = {modifier} type identifier "(" [parameterList] ")" (statementBlock | ";");

Syntaxdiagramme
~~~~~~~~~~~~~~~

-  Nichtterminale werden in Eckigen Kästen gezeichnet
-  Terminalsymbole haben abgerundete Kästen
-  Alternativen und Wiederhohlungen werden mit Verzweigungen gezeichnet.

.. figure:: Syntaxdiagramme1.png
   :alt: Syntaxdiagramme

   Syntaxdiagramme

.. figure:: Syntaxdiagramme2.png
   :alt: Syntaxdiagramme

   Syntaxdiagramme
