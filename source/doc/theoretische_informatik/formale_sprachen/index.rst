================
Formale Sprachen
================

.. toctree::
  :maxdepth: 2
  :glob:

  sprachen_grammatik/*
  ebnf_syntaxdiagramme/*
  regulaere_sprachen/index
  semantik/*
