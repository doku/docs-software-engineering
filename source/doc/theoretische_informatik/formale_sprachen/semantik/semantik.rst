.. _ref-semantik-types:

Semantik
========

.. note::
      - Syntax = Lehre des Satzbau
      - Semantik = Bedeutungslehre

Programme müssen unmissverständlich interpretiert werden können. Anhand
definierter Semantik kann untersucht werden, ob ein Programm der
Spezifikation entspricht

Die wichtigsten Techniken sind:

-  Übersetzersemantik
-  Operationale Semantik
-  Denotationelle Semantik
-  Axiomatische Semantik

.. figure:: SemantiktechnikenUebersicht.png
   :alt: Übersicht der Semantiktechniken

   Übersicht der Semantiktechniken

Übersetzersemantik
------------------

.. note::
  Idee
    Die Bedeutung von Anweisungen einer neuen
    Programmiersprache wird einer bereits bekannten Programmiersprache
    zugewiesen

Grundprinzip ist die formale Zurückführung jeder Anweisung in die
bekannte Programmiersprache.

Operationale Semantik
---------------------

.. note::
  Idee
    Für die Programmiersprache wird eine mathematische
    Maschine definiert

Die Maschine erzeugt aus Eingabedaten durch schrittweise Abarbeitung die
Ausgabedaten. In dem Verfahren wir angegeben, wie der Ausgabewert durch
Operationen erzeugt wird. Die Maschine kann durch verschiedene Modelle
definiert sein.

Beispiel
~~~~~~~~

.. code::

    read x, y;
    z := y + 1;
    z := z - 1;

    while z != 0 do
        x := x + 1;
        z := z - 1;
    end;

    write x;

Operationale Semantik mit der Eingabe **x=3** und **y=2**:

Es werden folgende Abkürzungen verwendet:

-  **w** für die ``while``-Schleife
-  **c** für den Inhalt der ``while``-Schleife


+-------+-------------+-------------+-------------+----------------------------------------------------------+
| i     | (z_i(x))    | (z_i(y))    | (z_i(z))    | Anweisungen                                              |
+=======+=============+=============+=============+==========================================================+
| 1     | 3           | 2           | 0           | ``z:=y+1; z:=z-1; w;`` (:math:`\epsilon, z_1`)           |
+-------+-------------+-------------+-------------+----------------------------------------------------------+
| 2     | 3           | 2           | 3           | ``z:=z-1; w;`` (:math:`\epsilon, z_2`)                   |
+-------+-------------+-------------+-------------+----------------------------------------------------------+
| 3     | 3           | 2           | 2           | ``while z!=0 do c end;`` (:math:`\epsilon, z_3`)         |
+-------+-------------+-------------+-------------+----------------------------------------------------------+
| 4     | 3           | 2           | 2           | ``x:=x+1; z:=z-1; w;`` (:math:`\epsilon, z_4`)           |
+-------+-------------+-------------+-------------+----------------------------------------------------------+
| 5     | 4           | 2           | 2           | ``z:=z-1; w;`` (:math:`\epsilon, z_5`)                   |
+-------+-------------+-------------+-------------+----------------------------------------------------------+
| 6     | 4           | 2           | 1           | ``while z!=0 do c end;`` (:math:`\epsilon, z_6`)         |
+-------+-------------+-------------+-------------+----------------------------------------------------------+
| 7     | 4           | 2           | 1           | ``x:=x+1; z:=z-1; w;`` (:math:`\epsilon, z_7`)           |
+-------+-------------+-------------+-------------+----------------------------------------------------------+
| 8     | 5           | 2           | 1           | ``z:=z-1; w;`` (:math:`\epsilon, z_8`)                   |
+-------+-------------+-------------+-------------+----------------------------------------------------------+
| 9     | 5           | 2           | 0           | ``while z!=0 do c end;`` (:math:`\epsilon, z_9`)         |
+-------+-------------+-------------+-------------+----------------------------------------------------------+
| 10    | 5           | 2           | 0           | (:math:`\epsilon, z_{10}`)                               |
+-------+-------------+-------------+-------------+----------------------------------------------------------+



Denotationelle Semantik
-----------------------

.. note::
  Idee
    Für die Anweisungen einer Programmiersprache wird die
    semantische Funktion festgelegt.

Dadurch kann man vom konkreten Maschinenmodell abstrahieren. Zudem sind
nicht alle Zwischenzustände von Interesse. Es wird nur die Wirkung von
Anweisungen in Form von veränderter Variablenbelegung untersucht.

Axiomatische Semantik
---------------------

.. note::
  Idee
    Wir betrachten nur noch die Eigenschaft der Zustände und
    nicht mehr die konkreten Zustände.

Die Wirkung einer Anweisung wird durch ein Tupel (:math:`(P, {S}, Q)`). Dabei
ist :math:`P` die Vorbedingung und :math:`Q` ist die Nachbedingung. :math:`S` ist das
untersuchte Programm.

Wenn :math:`P`  vor der Ausführung gilt und :math:`S`  terminiert ist, dann gilt :math:`Q` .
Falls :math:`S`  nicht terminiert gibt es keine Aussage über die Nachbedingung.

weakest Precondition
~~~~~~~~~~~~~~~~~~~~

Ist bekannt welche Nachbedingung nach der Ausführung eines Programms
gelten soll, so kann man ein Prädikat :math:`P`  angeben, das mindestens
vor der Ausführung gegolten haben muss. Hier spricht man von der
**schwächsten Vorbedingung** (weakest precondition). Analog hierzu lässt
sich die **stärkste Nachbedingung** (strongest postcondition) definieren.
Beide werden in der **semantischen Verifikation** eingesetzt.

1. Wertzuweisungsregel

   -  :math:`wp(x:=E, Q(x)) \equiv E \in Typ(x) \wedge Q(E)`

2. Sequenzregel

   -  :math:`wp(S_1; S_2, Q) \equiv wp(S_1, wp(S_2, Q))`

3. Alternativenregel

   -  :math:`wp(if : B : then : S_1 : else : S_2, Q) \equiv (B \wedge wp(S_1, Q)) \vee (\neg B \wedge wp(S_2, Q))`

Beispiele
^^^^^^^^^

Beispiel 1
''''''''''

Code Beispiele in ``mini``.

.. code::

    S_1:
    x = 2 * y + 1;      (l_1)
    x = x + 1;          (l_2)
    Q_1: x > 0

.. math::

   \boldsymbol{wp}((l_1; l_2 ), Q_1)
   &\equiv \boldsymbol{wp}((l_1) \boldsymbol{wp}(l_2 ), Q_1)\\
   &\equiv \boldsymbol{wp}((x = 2 * y + 1;) \boldsymbol{wp}(x = x + 1;), x > 0) \\
   &\equiv \boldsymbol{wp}((x = 2 * y + 1;) , ( x + 1 > 0)) \\
   &\equiv \boldsymbol{wp}((x = 2 * y + 1;) , ( x > -1)) \\
   &\equiv ( 2 * y + 1 > -1) \\
   &\equiv ( 2 * y > -2) \\
   &\equiv ( y > -1)

Beispiel 2
''''''''''

.. code::

    S_2:
    if x = 0 then
        y = 42;          (l_1)
    else
        z = x - 5;       (l_2)
        y = z * z;       (l_3)
    Q_2: y = 9

.. math::

   \textbf{wp} & ((if (x = 0) \; then \; l_1 \; else \; (l_2; l_3)), Q_2) \\
   &\equiv \textbf{wp}((if (x = 0) then (y=42) else (z=x-5; y=z*z)), y=9)\\
   &\equiv ((x = 0) \wedge \textbf{wp}((y=42), y=9)) \vee ((x \neq 0) \wedge \textbf{wp}((z=x-5; y=z*z)), y=9) \\
   &\equiv ((x = 0) \wedge 42=9) \vee ((x \neq 0) \wedge \textbf{wp}((z=x-5; y=z*z)), y=9) \\
   &\equiv ((x = 0) \wedge FALSE) \vee ((x \neq 0) \wedge \textbf{wp}((z=x-5; y=z*z)), y=9) \\
   &\equiv FALSE \vee ((x \neq 0) \wedge \textbf{wp}((z=x-5; y=z*z)), y=9) \\
   &\equiv ((x \neq 0) \wedge \textbf{wp}((z=x-5; y=z*z)), y=9) \\
   &\equiv ((x \neq 0) \wedge \textbf{wp}((z=x-5); \textbf{wp}((y=z*z), y=9))) \\
   &\equiv ((x \neq 0) \wedge \textbf{wp}((z=x-5); (z*z=9))) \\
   &\equiv ((x \neq 0) \wedge ((x-5)^2=9)) \\
   &\equiv ((x \neq 0) \wedge (x^2-10x+25=9)) \\
   &\equiv ((x \neq 0) \wedge (x^2-10x+16=0)) \\
   &\equiv ((x \neq 0) \wedge (x_{1,2} = \frac{10\pm \sqrt{100-4*16}}{2})) \\
   &\equiv ((x \neq 0) \wedge (x_{1,2} = \frac{10\pm \sqrt{36}}{2})) \\
   &\equiv ((x \neq 0) \wedge (x_{1,2} = 5\pm 3)) \\
   &\equiv (x = 2) \vee (x=8) \\
