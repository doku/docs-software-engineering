==================
Entscheidbarkeiten
==================

Das Gültigkeitsproblem der Prädikantenlogik ist unentscheidbar. (Church)

Das Erfüllbarkeitsproblem (gegeben: Formel :math:`F`, gefragt: Ist :math:`F` erfüllbar)
der Prädikatenlogik ist unentscheidbar.
