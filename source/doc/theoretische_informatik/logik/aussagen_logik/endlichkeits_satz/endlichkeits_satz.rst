================
Endlichkeitssatz
================

.. attention::
  :math:`M` sei eine (unendliche) Menge von Formeln :math:`F_1, F_2, ...`. Dann
  gilt:

  :math:`M` ist genau dann erfüllbar, wenn jede endliche Teilmenge von :math:`M`
  erfüllbar ist.

:math:`M = \{ F_1, F_2, ...\}` sei eine Menge von Formeln. :math:`M` heißt
:term:`erfüllbar`, wenn eine Belegung :math:`A` existiert, die zu allen Formeln
aus :math:`M` passend ist und für die gilt :math:`A \models F \; \forall F \in M`

Wenn eine unendliche Menge unerfüllbar ist, dann gibt es mindestens eine endliche
Teilmenge, die unerfüllbar ist. Die Unerfüllbarkeit kann nicht durch die Unendlichkeit
kommen.
