===========
Hornformeln
===========

.. glossary::

  Klausel
    Die Disjunktion einer KNF nennt man auch Klausel.

Eine Hornformel ist eine Formel in KNF, bei der in jeder Klausel, höchstens ein
positives Literal vorkommt.

Jede Hornklausel lässt sich in **prozeduraler Form** schreiben.

- Axiom: :math:`A \equiv 1 \rightarrow A`
- :math:`A \vee \neg B_1 \vee \neg B_2 \vee ... \neg B_n \equiv (B_1 \wedge B_2 \wedge ... \wedge B_n)
  \rightarrow A`
- Zielklausel: :math:`\neg B_1 \vee \neg B_2 \vee ... \vee \neg B_n \equiv (B_1 \wedge
  B_2 \wedge ... \wedge B_n) \rightarrow 0`

Markierungsalgorithmus
======================
Diese Algorithmus testet die Erfüllbarkeit von Hornformeln.

#. Für alle in :math:`F` vorkommenden Atomaren Formeln :math:`A`:
   Falls :math:`1 \rightarrow A` in :math:`F` vorkommt, markiere :math:`A`.
#. Wenn :math:`A_1 \wedge ... \wedge A_m \rightarrow 0` in :math:`F` vorkommt und
   alle :math:`A_i` markiert sind: **Stop**, die Formel ist unerfüllbar.
#. Wenn :math:`A_1 \wedge ... \wedge A_n \rightarrow B` in :math:`F` vorkommt und
   alle :math:`A_i` markiert sind, aber :math:`B` nicht: markiere :math:`B`.
#. Wenn Schritt 3. nicht zutraf **Stop**, die Formel ist erfüllbar; sonst weiter mit 2.

Setzt man alle markierten Atomaren Formeln auf :math:`1` und die anderen auf :math:`0`,
so erhält man die **minimale erfüllende Belegung**. Da man nur denjenigen atomaren
Formeln den Wert :math:`1` zuordnet, die in jeder erfüllenden Belegung den
Wert :math:`1` haben, ist die so konstruierte Belegung, die
**eindeutig definierte** erfüllende Belegung.

Daher kann es auch nicht zu jeder Formel eine äquivalente Hornformel geben.
:math:`A \vee B`: hier gibt es keine eindeutige minimale Belegung.

.. hint::

  Hornformeln ohne Zielklauseln sind immer erfüllbar. (Setze alle atomaren Formeln
  auf :math:`1`.)

  Hornformeln ohne Axiome sind immer erfüllbar. Für eine erfüllende Belegung setzt
  man alle atomaren Formeln auf :math:`0`.

Falls :math:`F` eine Hornformel mit höchstens :math:`n` atomaren Formeln ist,
dann terminiert der Algorithmus nach höchstens :math:`n` Schleifendurchläufen,
da in jedem Durchlauf eine atomare Formel markiert wird.
