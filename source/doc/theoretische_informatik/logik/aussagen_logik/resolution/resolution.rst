==========
Resolution
==========

Resolution ist eine syntaktische Umformung (Kalkül), um die Unerfüllbarkeit von
gegebenen Formeln zu testen.

Für die Anwendung der Resolution muss die Formel in KNF vorliegen.

.. math:: F = (L_{1,1} \vee ... \vee L_{1,n_1}) \wedge ... \wedge (L_{k,1} \vee ... \vee L_{k,n_k})

Wobei :math:`L_{i,j}` Literale (:term:`Literal`) sind. Die KNF lässt sich dann als Menge von
Klauseln darstellen. Die Klauseln sind Mengen von Literalen, welche die einzelnen
Disjunktionsglieder darstellen.

.. math:: F = \{\{L_{1,1}, ..., L_{1,n_1}\}, ... , \{L_{k,1}, ..., L_{k,n_k}\}\}

Die Elemente haben keine Reihenfolge und doppeltvorkommende verschmelzen.

.. figure:: resolution_prinzipal.png
	 :alt: resolution_prinzipal

Es kann immer nur eine atomare Formel in einem Resolutionsschritt wegresolviert
werden. Ziel ist es zwei Klauseln der Form :math:`\{\alpha_1\}`, :math:`\{\neg\alpha_1\}`
zu erhalten. Diese können dann zur leeren Klausel :math:`\{\}` oder :math:`\square`
resolviert werden.

Die leere Klausel :math:`\square` ist die einzige unerfüllbare Klausel, da eine
Klausel nur dann erfüllbar ist, wenn sie ein Literal enthält, dass unter der gegebene
Belegung den Wert :math:`1` hat.

Die Klauselmenge, die :math:`\square` enthält ist immer unerfüllbar, da sie einer
Konjunktion ihrer Klauseln entspricht und daher nur erfüllt ist, wenn alle
Klauseln erfüllt sind.

Resolutionslemma
================

Wenn in der Klauselmenge :math:`F` der Resolvent :math:`R` gebildet werden kann,
dann sind :math:`F` und :math:`F \cup \{R\}` äquivalent.

Zu jeder Klauselmenge definieren wir :math:`Res(F)`:

.. math:: Res(F) =_{def} F \cup \{R | \text{R ist Resolvent zweier Klauseln in F} \}

.. math:: Res^0(F) = F

.. math:: Res^n(F) = Res(Res^{n-1} (F))

Bei :math:`n` atomaren Formeln kann es maximal :math:`4^n` verschiedene Klauseln
geben, da eine atomare Formel in jeder Klausel garnicht, positiv, negativ und beides
vorkommen kann.

Als Resolutionsabschluss der Klauselmenge :math:`F` bezeichnet man die Klauselmenge
:math:`Res^*(F)`, die wie folgt definiert wird:

.. math:: Res^*(F =_{def} \bigcup_{n \geq 0} Res^n(F))

Sobald zum ersten Mal :math:`Res^n(F) = Res^{n-1}(F)` gilt für dieses :math:`n`
:math:`Res^n(F) = Res^*(F)`

Resolutionssatz der Aussagenlogik
=================================
Eine Formel in KNF (dargestellt als Klauselmenge) ist genau dann erfüllbar,
wenn :math:`\square \notin Res^*(F)`.
