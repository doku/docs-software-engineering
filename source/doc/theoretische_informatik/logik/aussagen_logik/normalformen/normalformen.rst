============
Normalformen
============

.. glossary::

  Literal
    Ein positives Literal ist eine atomare Formel.

    Ein negatives Literal ist die Negation einer atomaren Formel.

  DNF
    Eine Formel ist in disjunktiver Normalform (DNF), wenn sie eine Disjunktion von
    Konjunktionen von Literalen ist.

  KNF
    Eine Formel ist in konjunktiver Normalform (KNF), wenn sie eine Konjunktion von
    Disjunktionen von Literalen ist.

.. attention:: Zu jeder Formel existieren äquivalente Formeln in DNF und KNF.

Normalformen Erzeugen
=====================

KNF Umformung
-------------

#. Negationen nach innen schieben mit deMorgan und dabei Doppelnegationen eliminieren
#. Zweites Distributivgesetz nutzen, um :math:`\vee`-Operationen an :math:`\wedge`
   vorbei nach innen zu schieben.

Ablesen aus Wahrheitstafel
--------------------------
Man kann KNF und DNF auch direkt aus der Wahrheitstafel generieren.

Um eine KNF für die Formel zu erhalten, schließen wir mit einer Konjunktion
alle Fälle aus, in denen F den Wert :math:`0` annimmt. Hierbei werden die atomaren
Formeln einer Nullzeile negiert.

Um eine DNF zu erhalten, bilden wir aus den Zeilen mit dem Wert :math:`1`
Konjunktionsglieder der Disjunktion.

Bei :math:`n` atomaren Formeln, sind die so erzeugte DNF und KNF zusammen immer
von der Größenordnung :math:`n*2^n`
