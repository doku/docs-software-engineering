========================
Einführung Aussagenlogik
========================

Aussagenlogische Formeln sind entweder **wahr** oder **falsch**.

Syntax
======

#. Atomare Formeln :math:`A_i \; i=1,2,3, ...` sind **Formeln**
#. Falls :math:`F` und :math:`G` Formeln sind, dann auch :math:`(F \wedge G)` und
   :math:`(F \vee G)`
#. Wenn :math:`F` eine Formel ist, dann auch :math:`\neg F`

Abkürzungen
-----------

- Namen ohne Index: :math:`A, B, C`
- Implikation: :math:`F_1 \rightarrow F_2 = ( \neg F_2 \vee F_2)`
- Äquivalenz: :math:`F_1 \Leftrightarrow F_2 = ((F_1 \wedge F_2) \vee (F_1 \wedge F_2))`
- n-faches Oder: :math:`\bigvee_{i=1}^{n} F_i = (...((F_1 \vee F_2) \vee F_3) ... \vee F_n)`
- n-faches Und: :math:`\bigwedge_{i=1}^{n} F_i = (...((F_1 \wedge F_2) \wedge F_3) ... \wedge F_n)`

Semantik
========
Die Elemente der Menge :math:`\{1,0\}` heißen Wahrheitswerte. Eine Belegung ist eine
Funktion :math:`\mathcal{A} : D \rightarrow \{1,0\}`, wobei :math:`D` eine Teilmenge
der atomaren Formeln ist. Durch eine Belegung erhält jede atomare Formel einen Wert.

Der Wert einer atomaren Formel ist genau dann definert, wenn :math:`A_1 \in D` gilt,
und er ist dann :math:`\mathcal{A}(A_i)`

- :math:`\mathcal{A}(F \wedge G) = min \{ \mathcal{A}(F), \mathcal{A}(G)\}`
- :math:`\mathcal{A}(F \vee G) = max \{ \mathcal{A}(F), \mathcal{A}(G)\}`
- :math:`\mathcal{A}(\neg F) = 1 - \mathcal{A}(F)`

.. glossary::

  passend
    Falls eine Belegung :math:`\mathcal{A}` für alle in :math:`F` vorkommenden
    atomaren Formeln definiert ist, so heißt :math:`\mathcal{A}` zu :math:`F` passend.

  Modell
    Falls :math:`\mathcal{A}` zu :math:`F` passend ist und :math:`\mathcal{A}(F)=1`
    gilt, ist :math:`\mathcal{A}` ein Model für :math:`F`. Man schreibt
    :math:`\mathcal{A} \models F`. Falls  :math:`\mathcal{A}(F)=0` ist :math:`\mathcal{A}`
    kein Modell für :math:`F`. Man schreibt :math:`\mathcal{A} \nvDash F`

  erfüllbar
    Eine Formel :math:`F` ist erfüllbar, falls :math:`F` mindestens ein :term:`Modell`
    besitzt, andernfalls heißt :math:`F` unerfüllbar.

  unerfüllbar
    Die Formel :math:`F` ist nicht erfüllbar.

  Tautologie
    Eine Formel :math:`F` heißt gültig oder Tautologie, falls jede zu :math:`F`
    passende Belegung ein Modell für :math:`F` ist. Wir schreiben :math:`\models F`,
    falls :math:`F` eine Tautologie ist und :math:`\nvDash F`, falls F keine Tautologie
    ist.

  gültig
    Die Formel :math:`F` ist eine :term:`Tautologie`

Wahrheitstafeln
===============

.. figure:: wahrheitstafeln.png
	 :alt: wahrheitstafeln

Spiegelungsprinzip
==================

.. attention::

  Eine Formel :math:`F` ist eine :term:`Tautologie` :math:`\Leftrightarrow \neg F` ist
  :term:`unerfüllbar`

Ist :math:`F` eine erfüllbare aber nicht gültige Formel, dann ist auch :math:`\neq F`
erfüllbar aber nicht gültig.

Äquivalenz
==========

Definition
----------
:math:`F` und :math:`G` heißen **semantisch äquivalent**, wenn für alle zu beiden
passenden Belegungen :math:`\mathcal{A}` gilt :math:`\mathcal{A}(F) = \mathcal{A}(G)`.
Wir schreiben :math:`F \equiv G`.

Ersetzbarkeitstheorem
---------------------
Ersetzt man eine äquivalente Teilformel, ändert sich der Wahrheitswert der
Formel nicht.

.. attention::

  Sei :math:`H` eine Formel, in der :math:`F` als Teilformel vorkommt. Sei :math:`G`
  eine zu :math:`F` äquivalente Formel und sei :math:`H'` die Formel, die aus :math:`H`
  entsteht, wenn :math:`F` durch :math:`G` ersetzt wird. Dann sind :math:`H` und
  :math:`H'` äquivalent.

Äquivalenzen
------------

.. figure:: aequivalenzen.png
	 :alt: aequivalenzen

.. csv-table::

   "Idempotenz", ":math:`F \equiv (F \wedge F) \equiv (F \vee F)`"
   "Kommutativität", ":math:`(F \wedge G) \equiv (G \wedge F)`, :math:`(F)`"
