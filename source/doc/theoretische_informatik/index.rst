=======================
Theoretische Informatik
=======================

.. toctree::
  :maxdepth: 2
  :glob:

  formale_sprachen/index
  berechenbarkeit/index
  komplexitaet/index
  logik/index
  diskrete_strukturen/index
