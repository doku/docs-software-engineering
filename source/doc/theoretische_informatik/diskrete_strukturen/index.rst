===================
Diskrete Strukturen
===================

.. toctree::
  :maxdepth: 2
  :glob:

  strukturen/*
  ggt/*
  primzahlen/*
  modulare_arithmetik/*
  rsa/*
  euler_phi_funktion/*
  fibonacci_zahlen/*
  goldener_schnitt/*
