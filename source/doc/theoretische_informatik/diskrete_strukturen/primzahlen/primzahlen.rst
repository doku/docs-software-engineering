==========
Primzahlen
==========

Eindeutige Primfaktorzerlegung
==============================

Sei :math:`n \in \mathbb{N} n>0`, dann lässt sich :math:`n` eindeutig darstellen als

.. math:: n = \prod_{p \; Primzahl} p^{n_p}

Dabei ist :math:`n_p \neq 0 \Leftrightarrow p` Teiler von :math:`n` ist.


Satz von Wilson
===============

Für alle natürlichen Zahlen :math:`n \geq 2` gilt:

.. math:: (n-1)! \equiv 1  \mod n \Longleftrightarrow n \; ist \; Primzahl
