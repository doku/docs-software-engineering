===================
Modulare Arithmetik
===================


Primzahltest nach Fermat
------------------------

.. figure:: primzahltest_example.png
	 :alt: primzahltest_example

Schnelle Exponentiation
-----------------------

.. figure:: schnelle_exponentation_algorithmus.png
	 :alt: schnelle_exponentation_algorithmus

Die Laufzeit des Algorithmus ist logarithmisch.
