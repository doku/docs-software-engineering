=========
gcd (ggT)
=========

:math:`\gcd` oder ggT

.. math:: \gcd(n \mod m, m) = \gcd (m,n)

Jeder Teiler von *m* und *n* teilt auch *n mod m*, und jeder Teiler von *m* und
*n mod m* teilt auch *n*.

.. math:: \forall n : \gcd(0,n) = n

.. hint:: :math:`0` und :math:`1` sind teilerfremd.

Um den :math:`\gcd(k,l)` mit dem Euklidischen Algorithmus zu berechnen, braucht man
höchstens :math:`\lceil \log_{\phi}k \rceil < \frac{3}{2} \log_2 k` Aufrufe.

Lemma von Bézout
================

Für alle :math:`m, n \in \mathbb{Z}` existieren :math:`a, b \in \mathbb{Z}`, so dass
:math:`\gcd(m,n) = am+bn`
