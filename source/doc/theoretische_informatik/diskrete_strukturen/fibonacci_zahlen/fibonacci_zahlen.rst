=========
Fibonacci
=========

Definition
==========

.. math::

  F_0 = 0 \\
  F_1 = 1 \\
  F_{n+2} = F_{n+1} + F_{n}

Dominostein-Interpretation
==========================
:math:`F_n+1` entspricht der Anzahl der Möglichkeiten eine Sequenz der Länge :math:`n`
aus Steinen der Länge :math:`1` und :math:`2` zu legen.


Fibonacci und der Goldene Schnitt
=================================

:term:`goldener Schnitt`

.. math::

  F_n (a,b) = a * \phi - b * \hat{\phi} \\

  F_n  = \frac{1}{\sqrt{5}} ((\frac{1+\sqrt{5}}{2})^n - (\frac{1-\sqrt{5}}{2})^n)

:math:`F_n` ist die nächstgelegene natürliche Zahl bei
:math:`\frac{1}{\sqrt{5}} (\frac{1+\sqrt{5}}{2})^n`

Der Quotient folgender Fibonacci-Zahlen strebt gegen :term:`goldener Schnitt`:

.. math:: \frac{f_{n+1}}{f_n} = \frac{f_n+f_{n-1}}{f_n} = 1 + \frac{f_{n-1}}{f_n}

Für :math:`n \rightarrow \infty` konvergiert die Folge gegen den Grenzwert :math:`\phi`

Damit folgt: :math:`\phi = 1 + \frac{1}{\phi}`

.. figure:: fibonacci_konvergenz_goldener_schnitt.png
	 :alt: fibonacci_konvergenz_goldener_schnitt

:math:`\gcd` und :math:`F_n`
============================

.. math:: \gcd(F_m, F_n) = F_{\gcd(m,n)}

:math:`\gcd` und :math:`F_n` sind vertauschbar, sie vertragen sich.

.. math::

  \gcd(F_8, F_{12}) = F_4 = 3

  F_8 = 21 \\
  F_12 = 144 \\
  \gcd(21, 144) = 3
