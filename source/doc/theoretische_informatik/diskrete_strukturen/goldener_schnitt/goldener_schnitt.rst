================
Goldener Schnitt
================

.. glossary::

  Goldener Schnitt
    Teilungsverhältnis einer Strecke in Teilstrecken :math:`a, b`, sodass
    gilt:

    .. math:: \frac{a}{b} = \frac{a+b}{a}

.. figure:: goldener_schnitt_calculation.png
	 :alt: goldener_schnitt_calculation
