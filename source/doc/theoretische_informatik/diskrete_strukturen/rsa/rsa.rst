===
RSA
===

RSA (benannt nach seinen Autoren Rivest, Shamir und Adleman) ist ein asymmetrisches
Verschlüsslungsverfahren.

Verfahren
=========

Schlüssel berechnen
-------------------

#. Wähle große Primzahlen :math:`p` und :math:`q` mit :math:`p < q`
#. :math:`n = p*q`
#. :math:`\varphi (n) = (p-1)(q-1)`
#. Wähle :math:`e>1` mit :math:`e*s \equiv 1 \mod \varphi (n)`
#. Veröffentliche :math:`(n,e)`, :math:`p,q,s` müssen geheim bleiben

Verschlüsseln
-------------

#. Der öffentliche Schlüssel :math:`(n,e)` ist bekannt
#. Wähle eine Nachricht :math:`x \in \{0, ..., n-1\}`
#. Berechne :math:`y = x^e \mod n` (mittels schneller Exponentiation)
#. Sende die verschlüsselte Nachricht :math:`y`

Entschlüsseln
-------------

#. Die verschlüsselte Nachricht sei :math:`y`
#. berechne :math:`x' = y^s \mod n`
#. Es gilt :math:`x' = x`. Damit ist die Nachricht entschlüsselt.

Es gilt: :math:`(x^e)^s \mod n = x`

Sicherheit
==========
Es gibt drei Probleme, deren Lösbarkeit, dazu führen würde, dass das RSA-Verfahren
nicht mehr sicher wäre:

#. Faktorisiere :math:`n=p*q`
#. Berechne :math:`\varphi (n)`
#. Finde :math:`s` mit :math:`e*s \equiv 1 \mod \varphi (n)`

Die Komplexität der Probleme ist nicht bekannt.

Löst man eine der drei Probleme, so kann man die anderen leicht bestimmen.
