==============================================
Einführung in die Softwaretechnik - Grundlagen
==============================================

Software - Eigenschaften
========================

Software ist immateriell. Was wir als *natürlich* empfinden, ist an materielle
Eigenschaften geknüpft. **An Software ist nichts natürlich**.

Software wird nicht gefertigt, sondern **nur entwickelt**. Kopie und Original sind daher
nicht unterscheidbar.

Software verschleißt nicht. Die "**Wartung**" stellt nicht den alten Zustand wieder
her, sondern einen neuen. **Fehler** entstehen nicht durch Abnutzung, sondern sind
eingebaut.

.. danger:: Software Maintenance is done in order not to maintain the current state of a system.

**Wiederverwendung** ist bei Software extrem lukrativ, wenn der Aufwand für
Anpassungen relativ gering ist (Goldmine des Software engineering's).

In der Software arbeitet man sehr gerne in optimal Szenarien und blendet die
Realität aus.

Wir müssen davon ausgehen das wir bei weitem nicht so schnell und zuverlässig
arbeiten wie wir uns einbilden oder wünschen.

In der Software gibt es keine örtliche Beschränkung der Auswirkung von Problemen.
Es gibt keine natürliche Lokalität. In vielen Fällen müssen wir daher aktiv
Distanz herstellen und ungewollte Fernwirkungen verhindern.

Software-Systeme sind **sehr komplex**. Die Entwicklungskosten sind unvermeidlich
hoch.

Software-Systeme müssen **autonom funktionieren**. Die meisten komplexen Artefakte
funktionieren nur, wenn und solange Menschen immer wieder eingreifen und Mängel,
Defekte und Widersprüche beseitigen oder kompensieren.

.. note::
    Whatever can go wrong, will go wrong.

Auch **außergewöhnliche Situationen**, die nur im Abstand von Milliarde Operationen
auftreten, müssen als **Normalfälle** behandelt werden.

Die **Werkstoffe** der Software sind **amorph und universell**. Werkstoffe sind die
eingesetzten Sprachen, meist nur die *natürliche* Sprache und die Programmiersprache,
evtl. noch andere, z.B. graphische Notation.

Modularisierung (Teile überschaubar machen) führt zu besserer Wartbarkeit.

.. hint::
  Ein Programm wird in seinem Leben **einmal** geschrieben, aber **siebenmal** gelesen.

Hardware wird immer schneller und Software wird immer langsamer. Aber Hardware kann
nicht so viel schneller werden wie die Software langsamer wird.

Leitbild des Ingenieurs
=======================

Als Softwareentwickler orientiert man sich an den Denkprozess eines Ingenieurs.

Im Vordergrund stehen hier folgende Merkmale:

- **Rationalität** und Anwendung aller wissenschaftlichen Erkenntnisse als Grundprinzip
- **Problemlösen** als eigentliche Aufgabe
- **Kostenbewusstsein statt Perfektionismus**: Ziel ist ein gutes Kosten/Nuten Verhältnis
  zu erreichen. An vielen Stellen muss zur Senkung der Gesamtkosten der Aufwand erhöht werden.
- **Universeller Anspruch**, der nicht vor fachlichen Grenzen Halt macht.
- Praktischer Erfolg als einziger Beweis
- **Qualitätsbewusstsein** als Denkprinzip, streben nach möglichst hoher Qualität
- Einführung und Betrachten von **Normen**, diese schaffen auf allen Ebenen einheitliche
  Schnittstellen.
- Denken in Baugruppen: Normen ermöglichen es Baugruppen zu verwenden, die Teilprobleme
  lösen und es ermöglichen auf einer höheren Abstraktionsebene zu arbeiten.

Grundbegriffe
=============

.. glossary::

  Taxonomie
    - Begriff der Biologie,
    - Aufteilung einer Gruppe in Untergruppen
    - ein Modell und damit Menschen gemacht, und damit ist jede Klassifikation willkürlich
      also nicht *objektiv* richtig

    A scheme that partitions a body of knowledge and defines the relationship
    among the pieces. It is used for classifying and understanding the body of knowledge


  Werkzeuge
    Dienen zur Ausführung einer Arbeit, die wenigstens prinzipiell auch ohne das Werkzeug geleistet
    werden könnte. Ein Werkzeug erweitert die Möglichkeiten die wir haben

    Im Software Engineering dienen Werkzeuge zur automatischen oder vom Benutzer gesteuerten Transformation
    oder Speicherung von Informationen

  Werkstoffe
    sind von den Werkzeugen deutlich unterschieden, weil aus ihnen das Produkt
    geformt wird. Werkstoffe bleiben im Produkt. :term:`Werkzeuge` bleiben beim Hersteller.
    Wir benutzen als Werkstoffe unserer Produkte **Sprachen**. Eine Sprache (**Notation**)
    legt die möglichen Aussagen fest (:term:`Syntax`) und gibt diesen eine Bedeutung
    (:term:`Semantik`)

  Methoden
    sind Handlungsanweisungen, also Regeln, die den Menschen bei der Wahr seiner Aktionen
    führen.

  Systemdreieck
    Das Systemdreieck: Methoden, Sprachen und Werkzeuge sind im Idealfall durch
    **gemeinsame Konzepte** verbunden.

  Effektiv und effizient
    Eine Lösung, die ihre geforderte Funktionalität erfüllt, also im üblichen Sinne
    **korrekt** ist, ist **effektiv**. Wir verwenden das Wort allgemeiner auch bei
    Werkzeugen und Verfahren; ein effektives Werkzeug erfüllt seine Zweck,
    ein effektives Verfahren **führt zum Ziel**

    Effiziente Lösungen nehmen sparsam Gebrauch von den Betriebsmitteln, (wenig
    Rechenzeit oder wenig Speicher).

    Wir streben nicht nach maximaler Effizienz sondern, nur nach ausreichender
    Effizienz. In vielen Fällen spielt Effizienz kaum eine Rolle. Effizienz nur dann
    optimieren wenn Probleme auftreten oder Teile **sehr** oft ausgeführt werden.
