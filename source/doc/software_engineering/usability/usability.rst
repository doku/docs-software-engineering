=========
Usability
=========

.. glossary::

  usability
    Usability umfasst alle Merkmale eines Systems, die Einfluss darauf haben, wie
    mühsam oder mühelos die Benutzer mit dem System zurechtkommen.

.. figure:: usability_aspekte.png
	 :alt: usability_aspekte

UX (User Experience)
====================

.. figure:: user_research.png
	 :alt: user_research

UX Patterns
===========

user form
---------
When asking for user data  ideally the steps should be able to be interchangeably
completed. Each step should be marked whether it is not started, started or finished.
The progress should be indicated

Functionality Updates
---------------------
Notifications when updates change functionality

.. figure:: functionality_notifications.png
	 :alt: functionality_notifications
