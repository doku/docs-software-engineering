.. _meaningful-names:

================
Meaningful Names
================

Searchability
=============
Especially constants require a very searchable name.

The length of a name should correspond with the size of its scope.

Method names
============
A method name should clearly state **what** the method does, and should generally
not give any implementation detail.

When constructors are overloaded, use static factory methods with names that
describe the arguments. You can enforce the use of the factory methods by making
the corresponding constructor private.

.. code-block:: java

  //static factory method
  Complex fulcrumPoint = Complex.FromRealNumber(23.0);

  //overloaded constructor
  Complex fulcrumPoint = new Complex(23.0);

.. hint:: Say what you mean, mean what you say.

Consistency
===========
Pick one word for one abstract concept and stick with it. It is very confusing if
you have ``fetch``, ``retrieve`` and ``get`` as equivalent methods of different
classes.

Avoid using the same word for two purposes.

Domain Names
============
When using patterns or known algorithms make sure to use their well known names.
Other programmers will be immediately familiar with the concept. But only use
patter names like (factory, controller etc.) if you are actually using these
patterns. Otherwise you will confuse everyone.
