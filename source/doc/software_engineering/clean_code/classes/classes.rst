=======
Classes
=======

Structure
=========
A class should begin with a list of variables, with ``public static`` constants
before ``private static`` before ``private`` variables. There is rarely a good
reason to have ``public`` variables.

``public`` functions come after the variables and ``private`` utilities come
after that.

Encapsulation
=============
We like to keep our variables and utility functions private. Sometimes they have
to be set to ``protected`` so that they can be accessed by tests.

Classes should be small
=======================
Classes should be small. Rather than counting methods or lines of code, we are
counting the responsibilities of a class to determine the size.

The name of a class should describe what responsibilities it fulfills. If we
cannot derive a concise name for the class, its probably to large.

We should be able to write a brief description of the class in about 25 words,
without using the words ``if``, ``and``, ``or``, or ``but``.

A class should have one responsibility (:ref:`single-responsibility-principle`)
and one reason to change.

Trying to identify responsibilities (reasons to change) often helps us recognize
and create better abstractions in our code.

Every sizable system will contain a large amount of logic and complexity. The
primary goal in managing such complexity is to **organize** it so that a developer
knows where to look to find things and need only understand the directly affected
complexity at any given time.

.. note::
  We want our systems to be composed of many small classes, not a few large ones.
  Each small class encapsulates a single responsibility, has a single reason to
  change, and collaborates with a few others to achieve the desired system
  behaviors.

Cohesion
========
Classes should have a small number of instance variables. Each of the methods
of a class should manipulate one ore more of those variables. In general the more
variables a method manipulates the more cohesive that method is to its class.
When cohesion is high, it means that that the methods and variables of the class
are co-dependent and hang together as a logical whole. If some instance variables
are only used by a subset of methods you should try to separate the variables and
methods into two or more classes such that the classes are more cohesive.

Organizing for Change
=====================
Every change introduces the risk that the remainder of the system no longer works
as intended. In a clean system we organize our classes to reduce the risk of
change.

In an ideal system, we incorporate new features by extending the system, not by
making modifications to existing code.
