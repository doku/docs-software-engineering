===============
Emergent Design
===============

Rules of Simple Design by Kent Beck
===================================

A design is *simple* if it follows the following rules:

#. Runs all the tests
#. Contains no duplication
#. Expresses the intent of the programmer
#. Minimize the number of classes and methods

Run all the tests
-----------------
A design must produce a system that acts as intended. A system that is comprehensively
tested and passes all of its tests all of the time is a testable system. Systems
that aren't testable aren't verifiable. A system that cannot be verified should
never be deployed.

Tight coupling and violations if the SRP (:ref:`single-responsibility-principle`)
make it difficult to test. Making our system testable pushes us toward a good design.

Once we have tests we are empowered to keep our code and classes clean.

No Duplication
--------------
Duplication is the primary enemy of a well-designed system. It represents
additional work, risk and unnecessary complexity.

Expressive
----------
It's easy to write code that we understand, because at the time of writing it
we are deep in an understanding of the problem we're trying to solve. Other
maintainers of the code aren't going to have so deep an understanding.

The majority of the cost of an software project is in long-term maintenance. As
systems become more complex, they take more and more time for a developer
to understand and there is an ever greater opportunity for a misunderstanding.

The clearer the author can make the code the less time others will have to spend
understanding it. This will reduce defects and shrink the cost of maintenance.

Use :ref:`meaningful-names` and keep functions and classes small.

But the most important way to be expressive is to **try**. All too often we get
our code working and then move on to the next problem without giving sufficient
thought to making that code easy for the next person to read.

Minimal Classes and Methods
---------------------------
Even concepts as fundamental as elimination of duplication, code expressiveness,
and the SRP can be taken too far. In an effort to make our classes and methods
small, we might create too many tiny classes and methods. Our goal is to keep our
overall system small while also keeping our functions and classes small.
