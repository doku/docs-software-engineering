==========
Formatting
==========

Code formatting is important. You should choose a set of simple rules that govern
the format of your code, and then you should consistently apply those rules. It
helps if you have an automated tool that can apply those formatting rules.

A team of developers should agree upon a single formatting style. We want the software
to have a consistent style.

Vertical Formatting
===================
File sizes should be small (under 300 lines). Small files are usually easier to
understand than large files.

Variables should be declared as close to their usage as possible. Instance
Variables should be declared at the top of the class.

If one function calls another, they should be vertically close, and the caller
should be above the callee, if at all possible. This gives the program a natural flow.
Functions that perform a similar operation should be grouped together (overloaded
functions).

The Newspaper Metaphor
----------------------
The topmost parts of the source file should provide the high-level concepts
and algorithms. Detail should increase as we move downward, until at the end
we find the lowest level functions and details in the source file.

We expect the most important concepts to come first.
