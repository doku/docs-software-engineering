===========
Concurrency
===========

Concurrency is a decoupling strategy. It helps us decouple **what** gets done from
**when** it gets done. In a single-threaded application what and when are so
strongly coupled that the state of the entire application can often be determined
by looking at the stack backtrace. Decoupling *what* from *when* can dramatically
improve both the throughput and the structure of an application. From a structural
point of view the application looks like many little collaborating programs
rather than one big loop. This can make the system easier to understand and offers
some powerful ways to separate concerns.

.. note:: Writing clean concurrent programs is hard.

Concurrency can sometimes improve performance, but only when there is a lot of
wait time that can be shared between multiple threads or multiple processors.

The design of a concurrent algorithm can be remarkably different form the design
of a single-threaded system. The decoupling of what from when usually has a huge
effect on the structure of the system.

Concurrency incurs some overhead, both in performance as well as in writing additional
code.

Correct concurrency is complex, even for simple problems.

Concurrency bugs aren't usually repeatable, so they are often ignored as some
one-offs instead of the true defects they are.

Concurrency often requires a fundamental change in design strategy.

Concurrency Defense Principles
==============================

Concurrency design is complex enough to be a reason to change in it's own right
and therefore deserves to be separated from the rest of the code
(:ref:`single-responsibility-principle`).

.. hint:: Keep your concurrency-related code separate form other code.

It is important to restrict the number of critical sections in code that uses
shared objects. Take data encapsulation to heart, severely limit the access of
any data that may be shared.

A good away to avoid shared data are copy objects. If using copies of objects
allows the code to avoid synchronizing, the savings in avoiding the intrinsic
lock will likely make up for the additional creation and garbage collection
overhead.

Consider writing your threaded code such that each thread exists in its own
world, sharing no data with any other thread. This makes each thread behave as
if it is the only thread and there are no synchronization requirements.
Attempt to partition data into independent subsets that can be operated
on by independent threads.

Concurrency Problems
====================

.. glossary::

  Bound Resources
    Resources of a fixed size or number used in a concurrent environment. Examples
    include database connections and fixed-size read/write buffers.

  Mutual Exclusion
    Only one thread can access shared data or a shared resource at a time

  Starvation
    One thread or  group of threads is prohibited from proceeding for an excessively
    long time or forever. For example. always letting fast-running threads through
    first could starve out longer running threads if there is no end to the
    fast-running threads.

  Deadlock
    Two or more threads waiting for each other to finish. Each thread has a resource
    that the other thread requires and neither can finish until it gets the other
    resource.

  Livelock
    Threads in lockstep, each trying to work but finding another "in the way".
    Due to resonance, treads continuing to make progress but are unable for an excessively
    long time-or for ever.

Producer-Consumer
-----------------

Readers-Writers
---------------

Dining Philosophers
-------------------
