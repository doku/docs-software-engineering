=======
Systems
=======

Separate Constructing a System from using it
============================================
Software systems should separate the startup process, when the application objects
are constructed and the dependencies are wired together, form the runtime logic
that takes over after startup.

The startup process is a *concern* that any application must address. Unfortunately,
most applications don't separate this concern (:ref:`separation-of-concerns`).
The code for the startup process is ad hoc and it is mixed in with the runtime
logic.

.. code-block:: java

  public Service getService() {
    if (this.service == null) {
      service = new MyServiceImpl(defaultValues); //Good enough default for most cases?
    }
    return service;
  }

While **lazy instantiation** (:ref:`lazy-instantiation`) has many benefits such as
faster startup time and prevention of a ``null`` return, it also introduces a
hard-coded dependency on the object and everything its constructor requires.
We can't compile without resolving these dependencies, even if we never actually
use an object of this type at runtime. Additionally, we don't know whether
specific implementation is the right object in all cases. The class with this
method should not know the global context which object is the right.

One occurrence of lazy instantiation isn't a serious problem, however there are
normally many instances of little setup idioms like this in a application. The
global setup strategy is scattered across the application, with little modularity
and often significant duplication.

We should modularize the startup process separately form the normal runtime logic
and we should make sure that we have a global, consistent strategy for resolving
major dependencies.

Separation of Main
------------------
To separate construction from use you can simply move all aspects of construction
to ``main`` or modules called by ``main``. The rest of the system is designed to
assume that all objects have been constructed and wired up appropriately.

The application itself has no knowledge of ``main`` or the construction process.
It simply expects that everything has been build properly.

Factories
---------
With the use of an **Abstract Factory** (:ref:`abstract-factory-pattern`)
we can give the application control of **when** to build an object, but keep the
details of that construction separate form the application code.

The application is decoupled form the details of how to build that object. That
capability is held in the factory implementation and yet the application is in complete
control of when instances get build and can even provide application-specific
constructor arguments.

Dependency Injection
--------------------
A powerful mechanism for separating construction from use is **Dependency Injection**,
the application of **Inversion of Control** to dependency management. An object
should not take responsibility for instantiating dependencies itself, instead it
should pass this responsibility to another "authoritative" mechanism, thereby
inverting the control.

The class takes no direct steps to resolve its dependencies, instead it provides
setter methods or constructor arguments (or both) that are used to **inject** the
dependencies. During the construction process, the dependency injection container
instantiates the required objects and uses the constructor arguments or setter
methods provided to wire together the dependencies. Which dependent objects
are actually used is specified trough a configuration file or programmatically in
a special-purpose construction module.

The Spring framework provides a well known Dependency Injection Container for
Java.

Scaling Up
==========
It is a myth that we can get systems "**right the first time**". Instead we should
implement only today's stories, then refactor and expand the system to implement
new stories tomorrow. This is the essence of iterative and incremental agility.

.. hint::
  Software systems are unique compared to physical systems. Their architectures
  can grow incrementally, if we maintain the proper separation of concerns.
  (:ref:`separation-of-concerns`)

Aspect Oriented Programming
===========================

Aspect-oriented programming (AOP) is a general-purpose approach to restoring
modularity for cross-cutting concerns.

In AOP, modular constructs called **aspects** specify which points in the system
should have their behavior modified in some consistent way to support a
particular concern.

Test driven system architecture
===============================
If you are able to write your application's domain logic using Plain Old Java Objects
(POJOs), decoupled from any architecture concerns at the code level, then it is
possible to truly test drive your architecture. You can evolve it from simple
to sophisticated, as needed, by adopting new technologies on demand. It is not
necessary to do a **Big Design Up Front**

.. hint::
  Building architects have to do Big Design Up Front because it is not feasible
  to make radical architectural changes to a large physical structure once construction
  us under way. Software has its own physics, it is economically feasible to make
  radical change, if the structure of the software separates its concerns effectively.

This means that we can start a software project with a naively simple but nicely
decoupled architecture, delivering working user stories quickly, then adding more
infrastructure as we scale up.

.. note::
  An optimal system architecture consists of modularized domains of concerns, each
  of which is implemented with Plain Old Java (or other) Objects. The different
  domains are integrated together wit minimally invasive Aspects or Aspect-like
  tools. This architecture can be test driven, just like the code.

Modularity and separation of concerns make decentralized management and decision
making possible. In a sufficiently large system, no one person can make all
the decisions.

It is best to **postpone decisions until the last possible moment**. This lets us
make informed choices wit the best possible information. A premature decision is
a decision made with suboptimale knowledge. We have much less customer feedback,
mental reflection on the project, and experience with our implementation
choices if we decide too soon.

.. note::
  The agility provided by a POJO system with modularized concerns allows us to
  make optimal, just-in-time decisions, based on the most recent knowledge. The
  complexity of these decisions is also reduced.

.. hint::
  Whether you are designing systems or individual modules, never forget to use
  the simplest thing that can possibly work.
