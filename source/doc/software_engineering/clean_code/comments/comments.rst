========
Comments
========

.. hint:: Don't comment bad code - rewrite it.

The proper use of comments is to compensate for our failure to express ourself
in code. The older a comment is, and the farther away it is form the code it
describes, the more likely it is to be just plain wrong. Programmers can't realistically
maintain them.

Inaccurate comments are far worse than no comments at all. They delude and mislead.
Truth can only be found in the code itself.

Comments don't make up for bad code
===================================
One of the more common motivations for writing comments is bad code.
We write a module and know it is confusing and disorganized. So we say to ourself,
"Oh, I'd better comment that".

Clear and expressive code with few comments is far superior to cluttered and
complex code with lots of comments.

.. code-block:: java

  //Check to see if the employee is eligible for full benefits
  if((employee.flags & HOURLY_FLAG) && (employee.age > 65))

  if(employee.isEligibleForFullBenigits())

In many cases you can just create a function that says the same thing as the
comment you want to write.

Comments that are redundant and don't give any additional information just
clutter and obscure the code.

Javadocs
========
It is just plain silly to have a rule that says that every function must have
a javadoc, or every variable must have a comment. Comments like this just clutter
up the code, propagate lies, and lead to general confusion and disorganization.

Short functions don't need much description. A well-chosen name for a small
function that does one thing is usually better than a comment header.

AS useful as javadocs are for public APIs, they are not necessary for code that
is not intended for public consumption. Generating javadoc pages for the functions
and classes inside a system is not generally useful, and the extra formality of
the javadoc comments amounts to little more that cruft and distraction.

Commented-Out Code
==================
Few practices are as odious as commenting-out code. Source code control systems
remember code for us.

Never commit commented out code! No other one will have the courage to delete it because
they will think it is important.


Nonlocal Information
====================
If you must write a comment make sure it describes the code it appears near.
Don't offer systemwide information in the context of a local comment.

Good Comments
=============
Some comments are necessary or beneficial.

- Copyright comments
- Informative Comments (return value of abstract methods, Regex)
- Explanation of intent (states why a specific implementation was chosen)
- Clarification (obscure argument or return value)
- Warning for consequences
- ToDos
- Javadoc (for public API)

Comment for Regular Expression in code are ok, because humans can't read regular
expressions.

.. hint::
  Before writing a comment, take care that there is no better way, and
  then take even more care that they are accurate.
