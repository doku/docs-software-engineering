==========
Clean Code
==========

.. toctree::
  :maxdepth: 2
  :glob:

  introduction/*
  the_programmers_oath/*
  meaningful_names/*
  functions/*
  comments/*
  formatting/*
  objects_datastructures/*
  error_handling/*
  boundaries/*
  unit_tests/*
  classes/*
  systems/*
  emergent_design/*
  concurrency/*
  smells_and_heuristics/*
  notes

Source:  Clean Code (copyright Robert Martin)
