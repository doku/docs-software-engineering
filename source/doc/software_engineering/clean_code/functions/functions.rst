=========
Functions
=========

Size
====
Functions should be small.

Each Function should be transparently obvious.

Blocks within ``if``, ``else``, ``while`` statements etc. should be one line long.
Therefore, the indent level of a function should not be greater than one or two.

.. hint::
  The purpose of methods is to comprise a complex functionality out of many
  trivial steps arranged in a way that is easy to understand.

.. note::
  Functions should do one thing. They should do it well. They should do it only.

A function is small enough and does only one thing if it is impossible to extract
another function from it with a name that is not merely a restatement of its
implementation.


One Level of Abstraction per Function
=====================================
All statements on a function should be at the same level of abstraction.

Mixing levels of abstraction within a function is always confusing. Readers may
not be able to tell whether a particular expression is an essential concept or
a detail.

In a class methods with a high level of abstraction should come first, followed by
methods with a low level of abstraction.

Switch Statements
=================
``switch`` can only be tolerated if they appear only once, are used to create
polymorphic objects, and are hidden behind an inheritance relationship so that
the rest of the system can't see them.

Functions with ``switch`` statements are to long and by their nature they always
do :math:`n` things. ``switch`` statements should only be used with an
**Abstract Factory**.

Descriptive Names
=================

.. note::
  You know you are working on clean code when each routine turns out to be
  pretty much what you expected. (Ward)

The smaller and more focused a function is, the easier it is to choose a descriptive
name. The name of a method should say **what** it does. Your function should not
have any **side effects**. It should do only what the method name suggests and
nothing more (no hidden initialization or class variable changes).

Don't be afraid to make a name long. A long descriptive name is better than a short
enigmatic name.

Function Arguments
==================
The ideal number of arguments for a function is zero.

A function should never have more than three arguments.

With many arguments, testing every combination of appropriate values can be daunting.

When a function seems to need more than two or three arguments, it is likely that
some of those arguments ought to be wrapped in a class of their own.

.. code-block:: java

  Circle makeCircle(double x, double y, double radius);
  Circle makeCircle(Point center, double radius);

Flag Arguments
--------------
Flag arguments are ugly. Passing a boolean into a function is a truly terrible
practice. It immediately complicates the signature of the method, loudly proclaiming
that this function does more than one thing. It does one thing if the flag is
``true`` and another if the flag is ``false``.

Output Arguments
----------------
Using an output argument instead of a return value for a transformation is confusing.

Arguments are most naturally interpreted as **inputs** to a function.

In general output arguments should be avoided. If your function must change the
state of something, have it be the state of its owning object.

.. _command-query-separation:

Command Query Separation
========================
Functions should either do something or answer something, but not both.
Either your function should change the state of an object, or it should return
some information about that object.

Don't Repeat Yourself
=====================
Duplication is a problem because it bloats the code and requires multiple
modifications should the algorithm ever change or if a bug is found.
