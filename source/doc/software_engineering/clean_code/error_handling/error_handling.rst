==============
Error Handling
==============

.. hint:: Error handling is important, but if it obscures logic, it's wrong.

Exceptions are better than Error Codes
======================================
Returning error codes from command functions violates the command query separation
(:ref:`command-query-separation`). You should use exceptions rather tan return codes.

``Try-catch`` blocks are ugly in their own right. They confuse the structure of
the code and mix error processing with normal processing. So it is better to extract
the bodies of the ``try`` and ``catch`` blocks out into functions of their own.

.. hint::
  Functions should do one thing. Error handling is one thing. Thus, a function
  that handles errors should do nothing else (there should be nothing before or
  after the ``try/catch/finally`` blocks)

Error handling and the actual business logic are two different concerns which
should be separated.

Your ``catch`` has to leave your system in consistent state, no matter what
happens in the ``try``. For this reason it is good practice to start with a
``try-catch-finally`` statement when you are writing code that could throw
exceptions. This helps you define what the user or that code should expect, no
matter what goes wrong with the code that is expected in the ``try``.

Try to write tests that force exceptions, and then add behavior to your handler
to satisfy your tests. This will cause you to build the transaction scope of the
``try`` block first and will help you maintain the transaction nature of that
scope.

Use Unchecked Exceptions
========================
When using checked exceptions, all exceptions that could be thrown by a method
are added to the methods signature.

Checked exceptions violate the Open-Closed Principle (:ref:`open-close-principle`).
If you throw a checked exception from a method in your code and the ``catch``
is three levels above, you **must** declare that exception in the signature of
each method between you and the ``catch``. This means that a change at low level
of the software can force signature changes on many higher levels.
Encapsulation is broken because all functions in the path of a throw must know
about details of that low-level exception.

Checked exceptions can sometimes be useful if you are writing a critical library:
you must catch them. But in general application development the dependency cost
outweigh the benefits.

Provide Context with Exceptions
===============================
Each exception that you throw should provide enough context to determine the
source and location of an error. Create informative error messages and
pass them along with your exceptions. Mention the operation that failed and
the type of failure. If you are logging in your application, pass along enough
information to be able to log the error in your ``catch``.

When we define exception classes in an application, our most important concern
is **how they are caught**.

Having to catch many different exception types can clutter up your error handling
code. Often a single exception class is finde for a particular area of code.
The information sent with the exception can distinguish the errors. Use different
exception classes only if there are times when you want to catch one exception and
allow the other one to pass through.

Don't return Null
=================
When we return ``null``, we are essentially creating work for ourselves and
foisting problems upon our callers. All it takes is one missing ``null`` check
to send an application spinning out of control.

Many ``null`` checks reduce the readability and distract from the important
logic. If you are tempted to return ``null`` from a method, consider throwing
an exception or returning a **special case** object (:ref:`special-case-pattern`)
instead.

Don't pass Null
===============
Returning ``null`` from methods is bad, but passing ``null`` into methods is
worse. Unless you are working with an API which expects you to pass ``null``,
you should avoid passing ``null`` in your code whenever possible.

.. code-block:: java

  public class MetricsCalculator {
    public double xProjection (Point p1, Point p2) {
      assert p1 != null : "p1 should not be null";
      assert p2 != null : "p2 should not be null";
      return (p2.x - p1.x) *1,5;
    }
  }

This is good documentation, but is doesn't solve the problem. If someone passes
``null``, we'll still have a runtime error.

In most programming languages there is no good way to deal with a ``null`` that
is passes by a caller accidentally. Therefore, the rational approach is to forbid
passing ``null`` by default. When you do so, you can code with the knowledge that
a ``null`` in a argument list is an indication of a problem, and end up with
far fewer careless mistakes.
