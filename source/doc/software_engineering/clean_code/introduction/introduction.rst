============
Introduction
============

.. note::
  Nobody writes clean code right from the start. You start by writing your (messy)
  functions and refactor and refine them until they are clean and well structured.
  Unit-tests make sure you don't break anything while cleaning up.

  Code without tests is bad code. It doesn't matter how well written it is; it
  doesn't matter how pretty or object-oriented or well-encapsulated it is. With
  tests, we can change the behavior of our code quickly and verifiably. Without
  them, we really don't know if our code is getting better or worse.

Getting software to work and making software clean are two very different activities.
The problem is that too many of us think that we are done once the program works.

It is not enough for code to work. Code that works is often badly broken. Nothing
has a more profound and long-term degrading effect upon a development project than
bad code. As code rots, the modules insinuate into each other, creating lots of
hidden and tangled dependencies. Finding and breaking old dependencies is a long
and expensive task. On the other hand, keeping code clean is relatively easy.
If you made a mess in a module five minutes ago, it is very easy to clean it up
right now.

.. hint:: “ Code is like humor. When you have to explain it, it’s bad.” – Cory House

So the solution is to continuously keep your code as clean and simple as it can
be. Never let the rot get started.

Clean code is not written by following a set of rules. You don't become a software
craftsman by learning a list of heuristics. Professionalism and craftsmanship come
from values that drive disciplines.

The only way to keep your productivity high and go fast, is to keep the code as
clean as possible at all times.

Wrap particular implementations which are likely to change in a more abstract
method or class. This allows you to go forward quickly (because you can use the
easiest implementation that works right now) while preserving your ability to change
later.

.. hint::

  As our processes become more agile, our coding must be more resilient. Because
  they will never be finished. They will be at constant state of improvement.
  We need to code our programs so that they are more easily improved over time.

.. note::

  It seems that perfection is attained not when there is nothing more to add,
  but when there is nothing more to subtract. (Antoine de Saint-Exupery)
