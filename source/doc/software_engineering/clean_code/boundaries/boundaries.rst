==========
Boundaries
==========

Wrappers
========
Wrapping third-party APIs is a best practice. When you wrap a third-party API,
you minimize your dependencies upon it: you can choose to mode to a different
library in the future without much penalty. Wrapping also makes it easier to
mock out third-party calls when you are testing your own code.

You can define an API you feel comfortable with. You can tailor and constrain
the interface to meet the needs of your application. It results in code that
is easier to understand and harder to misuse.

Learning Boundaries
===================
Third-party code helps us get more functionality delivered in less time.

Learning and integrating the third-party code is hard. Instead of experimenting
and trying out now stuff in you production code, we could write some tests to
explore our understanding of the third-party code. Such tests are called
learning tests (Jim Newkirk). In learning tests we call the third-party API, as
we expect to use it in our application. We're essentially doing controlled experiments
that check our understanding of that API.

Learning Tests are better than free. The learning tests where precise experiments
that helped increase our understanding. When there are new releases of the
third-party, we can run the learning tests and check whether the library is
still compatible with our requirements. This allows us to migrate safely to new
versions.

Using Code that does not yet exist
==================================

When the API of code you are supposed to use is yet to be defined you can define
the API the way you want to consume it and use the Adapter Pattern (:ref:`adapter-pattern`)
to bridge the gap. The adapter encapsulates the interaction with the API and
provides a single place to change when the API evolves.

One good thing about writing the interface we wish we had is that it's under our
control. This helps keep client code more readable and focused on what it is trying
to accomplish.

Clean Boundaries
================

Good software designs accommodate change without huge investments and rework.
When we use code that is out of our control, special care must be taken to protect
our investment and make sure future changes are not to costly.

Code at the boundaries needs clear separation and tests that define expectations.
As little of our code as possible should know about third-party code. It is better
to depend on something you control tan on something that you don't control and
could end up controlling you.
