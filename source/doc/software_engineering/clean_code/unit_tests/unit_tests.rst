==========
Unit Tests
==========

Clean Tests
===========

Tests must change as the production code evolves. The dirtier the tests, the
harder they are to change. As you start modifying the production code, old tests
start to fail, and the mess in the test code makes it hard to get those tests
to pass again.

.. hint::
  Test code is just as important as production code.

  It requires thought, design, and care. It must be kept as clean as production
  code.

If you don't keep your tests clean, you will lose them. **Unit tests** keep your
code flexible, maintainable, and reusable. Without tests, you will be reluctant
to make changes because of the fear that you will introduce undetected bugs.

So having an automated suite of unit tests that cover the production code is the
key to keeping your design and architecture as clean as possible. Tests enable
change.

Readability of tests is extremely important. Clarity, simplicity, and density of
expression make tests readable. In a test you want to say a lot with as few
expressions as possible.

Annoying detail in tests should be eliminated. Anyone who reads a test should be
able to work out what they do very quickly, without being misled or overwhelmed
by details.

.. note::
  If you let the tests rot, then your code will rot too. Keep your tests clean.

Single Concept per Test
=======================
We want to test a single concept in each test function.

The number of asserts in a test should be minimized.

FIRST
=====

Fast
  Test should be fast and run quickly. When tests run slow, you won't want to run
  them frequently.

Independent
  Tests should not depend on each other. One test should not set up the conditions
  for the next test. You should be able to run each test independently and run
  the tests in any order you like.

Repeatable
  Tests should be repeatable in any environment. You should be able to run the tests
  in the production environment, in the QA environment and on your laptop without
  a network.

Self-Validating
  Tests should have a boolean output. Either they pass or fail. You should not
  have to read through log files to tell whether the tests pass. If the tests
  aren't self-validating, then failure can become subjective and running the tests
  can require a long manual evaluation.

Timely
  Unit tests should be written just before the production code that makes them
  pass. If you write your unit tests after the production code you may not
  design the production code to be testable.

Unit Test Method Name
=====================

A unit Test Method should be composed of three parts:

1. Method unter test
2. Condition/Tested Usecase
3. expected Result

Customer Class : ``getFullname``

Unit test Method Name: ``getFullName_whenMiddleNameIsBlank_returnsNameAsFirstSpaceLast()``

Seeing the method name immediately tells you what goes wrong in case a test fails.

Nest testing Methods for one reference Method in inner Classes of your Test class.
This is very useful for **Parameterized tests**.
