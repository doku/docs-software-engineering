==============
General Smells
==============

Multiple Languages in one source file
-------------------------------------
The ideal is for a source file to contain one. and only one, language.
Realistically, we will probably need more than one, but we should try to minimize
the number and the extend of extra languages in our source files.

Obvious Behavior Is Unimplemented
---------------------------------
Following the **Principle if least surprise** any function or class should implement
the behaviors that another programmer could reasonably expect. When an obvious
behavior is not implemented, readers and users of the code can no longer depend
on their intuition about function names.

Incorrect Behavior at the Boundaries
------------------------------------
Developers often write functions that they think will work, and the trust their
intuition rather than proving that their code works in all the corner and
boundary cases. Every boundary condition, every corner case, every quirk and
exception represents situations that can confound an elegant and intuitive algorithm.
Don't rely on your intuition. Look for every boundary condition and write a
test for it.

Overridden Safeties
-------------------
Safety mechanisms such as compiler warnings, unit tests and access control for functions
have a good reason for their existence and should not be overridden.

Duplication
-----------
Every time you see duplication in the code, it represents a missed opportunity for
abstraction.

Simple duplication can be replaced by a method.
A more subtle form is the ``switch/case`` or ``if/else`` chain appears again and
again in various modules, always testing for the same set of conditions. This
should be replaced by polymorphism.

**One Switch Rule** says: There may be no more than one switch statement, for a
given type of selection. The cases in that switch statement must create polymorphic
objects that take the place of other such switch statements in the rest of the
system.

Still more subtle are the modules that have similar algorithms, but don't share
similar lines of code. This is still duplication and should be addressed by using
the :ref:`template-method-pattern` or :ref:`strategy-pattern`

Code at wrong level of abstraction
----------------------------------
It is important to create abstractions that separate higher level general concepts
from lower level detailed concepts. Good software design requires that we separate
concepts at different levels and place them in different containers. The separation
must be complete, we don't want lower and higher level concepts mixed together.

Isolation abstractions is one of the hardest things that software developers do,
and there is no quick fix when you get it wrong.

Base Classes depending on their derivatives
-------------------------------------------
Base classes should know nothing about their derivatives.

Too Much Information
--------------------
Well-defined modules have very small interfaces that allow you to do a lot with a
little. Poorly defined modules have wide and deep interfaces that force you to use
many different gestures to get simple things done.

Concentrate on keeping interfaces very tight and very small. Help keep coupling
low by limiting information.

Dead Code
---------
Dead code is code that isn't executed. It can be found in the body of an ``if``
statement that checks for a condition, that can't happen, in the ``catch`` block
of a ``try`` that never ``throws``, in utility methods that are never called or
in ``switch/case`` statements that never occur.

The problem with dead code is that is not completely updated when designs change,
it was written at a time when the system was different. When you find dead code,
delete it from the system.

Vertical Separation
-------------------
Variables and functions should be defined close to where they are used.

Inconsistency
-------------
If you do something in a certain way, do all similar things in the same way. Be
careful with the conventions you choose, and once chosen, be careful to continue
to follow them.

If a function uses the variable name ``response`` to hold an ``HttpServletResponse``
then use the same variable name consistently in the other functions that use
``HttpServletResponse`` objects. If you name a method ``processVerificationResponse``
then use a similar name, such as ``processDeletionRequest`` for the methods that
process other kinds of requests

Artificial Coupling
-------------------
Artificial coupling is a coupling between two modules that serves no direct purpose.
It is a result of putting a variable, constant, or function in a temporarily convenient,
though inappropriate, location.

For example ``enums`` should not be contained within more specific classes because
this forces the whole application to know about these more specific classes.

.. _feature-envy:

Feature Envy
------------
The methods of a class should be interested in the variables and the functions
of the class they belong to, and not the variables and functions of other classes.
When a method uses accessors and mutators of some other object to manipulate the
data within that object, then it *envies* the scope of the class of that other
object. It wishes that it were inside that other class so that it could have direct
access to the variables it is manipulating.

Sometimes, however, Feature Envy is a necessary evil. You don't want your business
objects know about the format of the report. Moving a ``fromatReport`` Functionality
into these objects would violate the :ref:`single-responsibility-principle`, the
:ref:`open-close-principle` and the :ref:`common-closure-principle`.

Selector Arguments
------------------
Selector Arguments are parameter (mostly ``boolean`` but ``int``, enum also possible)
in a function that select the specific functionality. There is no valid reason to
use a Selector Argument.

Selector arguments are just a lazy way to avoid splitting a large function into
several smaller functions. In general it is better to have many functions than
to pass some code into a function to select the behavior.

Obscured Intent
---------------
Hungarian notation, and magic numbers obscure the author's intent. Smallness and
density should not be prioritized over readability.

Misplaces Responsibility
------------------------
Code should be placed where it is intuitive to the reader not where it is convenient
for us.

Inappropriate Static
--------------------
You should prefer nonstatic methods to static methods. When in doubt, make the
function nonstatic. If you really want a function to be static, make sure that there
is no chance that you'll want it to behave polymorphically.

Use Explanatory Variables
-------------------------
One of the more powerful ways to make a program readable is to break a calculation
up into intermediate values that are held in variables with meaningful names.

Function names should say what they do
--------------------------------------
You should be able to tell from the name of a function what it does. If you have
to look at the implementation or documentation of the function to know what it does,
then you should work to find a better name or rearrange the functionality so that it
can be places in functions with better names

.. code-block:: java

  Date newDate = date.add(5); //does  this add 5 days, weeks or years?

  Date newDate = date.increadeByDays(5);

Understand the algorithm
------------------------
Before you consider yourself to be done with a function, make sure you understand
how it works. It is not good enough that is passes all the tests. You must know
that the solution is correct.

There is a difference between knowing how the code works and knowing whether the
algorithm will do the job required of it. Being unsure that an algorithm is appropriate
is often a fact of life. Being unsure what your code does is just laziness.

Often the best way to gain this knowledge and understanding is to refactor the
function into something that is so clean and expressive that it is obvious to work.

Make logical dependencies physical
----------------------------------
If one module depends upon another, that dependency should be physical, not just
logical. The dependent module should not make **assumptions** (in other words, logical
dependencies) about the module it depend upon. Rather it should explicitly ask that
module for all the information it depends upon.

Follow Standard Conventions
---------------------------
Every team should follow a coding standard based on common industry norms.

Replace Magic Numbers with Named Constants
------------------------------------------
In general it is a bad idea to have raw numbers in your code. You should hide them
behind well-named constants.

Encapsulate Conditionals
------------------------
Boolean logic is hard enough to understand without having to see it in the context
of an ``if`` or ``while`` statement. Extract functions that explain the indent
of the conditional.

Avoid Negative Conditionals
---------------------------
Negatives are just a bit harder to understand than positives. So, when possible,
conditionals should be expressed as positives.

Functions should do one thing
-----------------------------
It is often tempting to create functions that have multiple sections that perform a
series of operations. Functions of this kind do more than one thing, and should be
converted into many smaller functions, each of which does one thing.

.. code-block:: java

  // does three things: loops over all employees, checks if payday and pays employee
  public void pay() {
    for (Employee e : employees) {
      if (e.isPayday()) {
        Money pay = e.calculatePay();
        e.deliverPay(pay);
      }
    }
  }

  //better solution
  public void pay() {
    for (Employee e : employees) {
      payIfNecessary(e);
    }
  }

  private void payIfNecessary(Employee e) {
    if (e.isPayday()){
      calculateAndDeliverPay(e);
    }
  }

  private void calculateAndDeliverPay(Employee e) {
    Money pay = e.calculatePay();
    e.deliverPay(pay);
  }

Hidden Temporal Couplings
-------------------------
Temporal couplings are couplings like one function has to be executed before another
one because it initializes instance variables which the other function uses etc.
Temporal couplings are often necessary, but you should not hide the coupling.
Structure the arguments of your functions such that the order in which they should
be called is obvious.

Communicate the reason for structure
------------------------------------
Have a reason for the way you structure your code, and make sure that reason is
communicated by the structure of the code. If a structure appears arbitrary, others
will feel free to change it. If a structure appears consistently throughout the
system, others will use it and preserve the convention.

Encapsulate Boundary Conditions
-------------------------------
Boundary conditions are hard to keep track of. Put the processing for them in one
place. Don't let them leak all over the code. We don't want swarms of ``+1`` or
``-1`` scattered in the code.

Functions should descend only one level of abstraction
------------------------------------------------------
The statements within a function should all be written at the same level of
abstraction, which should be one level below the operation described by the
name of the function.

Keep Configurable Data at High Levels
-------------------------------------
If you have a constant such as a default or configuration value that is known and
expected at a high level of abstraction, do not burry it in a low-level function.
Expose it as an argument to that low-level function called from the high-level function.
