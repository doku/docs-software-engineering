===========================
Basic Smells and Heuristics
===========================

Names
=====

Choose Descriptive Names
------------------------
Don't be too quick to choose a name. Make sure the name is descriptive. Remember
that meanings tend to drift as software evolves, so frequently reevaluate the
appropriateness of the names you choose.

Names in software are 90% of what make software readable. You need to take the time
to choose them wisely and keep them relevant. Names are too important to
treat carelessly.

The power of carefully chosen names is that the overload the structure of code
with description. That overloading sets the readers expectations about what
the other functions in the module do.

Choose Names at the Appropriate Level of Abstraction
----------------------------------------------------
Don't pick names that communicate implementation. Choose names that reflect the
level of abstraction of the class or function you are working in. You should not
mix different levels of abstraction. Each time you make a pass over your code,
you will likely find some variable that is named at too low level. You should
take the opportunity to change those names when you find them.

Use Standard Nomenclature Where Possible
----------------------------------------
Names are easier to understand if they are based on existing conventions or usage.
For example, if you are using the :ref:`decorator-pattern`, you should use the word
``Decorator`` in the names of the decorating classes.

It is better to  follow conventions than invent your own. Teams will often invent
their own standard system of names for a particular project. The more you can use
names that are overloaded with special meanings that are relevant to your project,
the easier it will be for readers to know what the code is taking about.

Unambiguous Names
-----------------
Choose names that make the workings of a function or variable unambiguous.
The name of a method state in detail what it does and not in broad and vague
terms. There should not be any surprises to the user of a method. The explanatory
value outweighs the increase in name length

Use Long Names for Long Scopes
------------------------------
The length of a name should be related to the size of its scope. Yo can use very
short variable names for tiny scopes, but for big scopes you should use longer names.

Variable names like ``i`` are just fine if their scope is five lines long. Variables
and functions with short names lose their meaning over long distances. So longer
the scope of the name, the longer and more precise the name should be.

Avoid Encodings
---------------
Names should not be encoded with type or scope information (Hungarian notation).
Prefixes such as ``m_`` or ``f_`` are useless in today's environments and distracting.
Keep your names free of Hungarian pollution.


Names should describe Side-Effects
----------------------------------
Names should describe everything that a function, variable or class is or does.
Don't hide side effects with a name. Don't use a simple verb to describe a function
that does more than just a simple action.

.. code-block:: java

  public Window getWindow() {
    if (this.window = null) {
      this.window = new Window();
    }
    return this.window;
  }

This function does a bit more than get an ``window``, it creates the ``window`` if
necessary. Thus a better name might be ``createOrReturnWindow``

Comments
========

Inappropriate  Information
--------------------------
It is inappropriate for comments to hold information better held in a different
kind of system such as your source code control system, your issue tracker, or any
other record-keeping system. In general, meta-data such as authors, last-modified-date,
and so on should not appear in comments. Comments should be reserved for technical
notes about the code and design.

Obsolete Comment
----------------
A comment that has gotten old, irrelevant, and incorrect is obsolete. It is best
to write a comment that will not become obsolete.

Redundant Comment
-----------------
A comment is redundant if it describes something that adequately describes itself.
An example is a Javadoc that says nothing more than the function signature.
Comments should say things that the code cannot say for itself.

Poorly Written Comment
----------------------
A comment worth writing is worth writing well. If you are going to write a comment,
take the time to make sure it is the best comment you can write. Choose your words
carefully. Use correct grammar and punctuation. Don't state the obvious and be brief.

Commented Out-Code
------------------
Commented-out code is an abomination. When you see commented-out code, **delete it**.
Don't worry, the source code control system still remembers it. If anyone needs it,
they can go back and check out a previous version.

Environment
===========

Build Requires More than One Step
---------------------------------
Building a project should be a single trivial operation. You should not have to
check many little pieces out from source control or search for all the various
little extra JARs and XML file and other artifacts that the system requires.
You *should* be able to check out the system with a single simple command and then
issue one other simple command to build it.

Tests require more than one step
--------------------------------
You should be able to run all the unit tests with just a single command. In the
best case you can run all the tests by clicking one button in you IDE.

Functions
=========

Too many arguments
------------------
Functions should have a small number of arguments. More than three is very
questionable and should be avoided.

Output Arguments
----------------
Output arguments are counterintuitive. Readers expect arguments to be inputs, not
outputs. If your function must change the state of something, have it change the
state of the object it is called on.

Flag Arguments
--------------
Boolean arguments loudly declare that a function does more than one thing.
They are confusing and should be eliminated.

Dead Functions
--------------
Methods that are never called should be discarded. Keeping dead code around is
wasteful. Delete the function, the source code control system will remember it.
