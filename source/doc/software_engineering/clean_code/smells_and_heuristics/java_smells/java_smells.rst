===========
Java Smells
===========

Avoid long Import lists
------------------------
Specific imports are hard dependencies, whereas wildcard imports are not. If you
specifically import a class, then that class must exist. But if you import a package
wit a wildcard, no particular classes need to exist.

Don't inherit Constants
-----------------------
When constants are part of an interface, don't gain access to those constants by
inheriting that interface. The constants will be hidden at the top of the inheritance
tree. Don't use inheritance as a way to cheat the scoping rules of the language.
Us a static import instead.
