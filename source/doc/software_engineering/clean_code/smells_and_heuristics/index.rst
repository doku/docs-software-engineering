=====================
Smells and Heuristics
=====================

.. toctree::
  :maxdepth: 2
  :glob:

  general_smells/*
  basic_smells/*
  test_smells/*
  java_smells/*
