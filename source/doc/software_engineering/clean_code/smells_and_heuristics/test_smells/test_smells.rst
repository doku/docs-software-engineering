===========
Test Smells
===========

Insufficient Tests
------------------
A test suite should test everything that could possibly break. The test are
insufficient as long as there are conditions that have not been explored by the
tests or calculations that have not been validated.

Coverage tools report gaps in your testing strategy. This makes it quick and easy
to find ``if`` or ``catch`` statements whose bodies haven't been checked.

Don't Skip Trivial Tests
------------------------
They are easy to write and their documentary value is higher than the cost to
produce them.

An Ignored Test Is a Question about an Ambiguity
------------------------------------------------
Sometimes we are uncertain about a behavioral detail because the requirements are
unclear. We can express our question about the requirements as a test that is
commented out, or as a test that is annotated with ``@Ignore``.

Test Boundary Conditions
------------------------
Take special care to test boundary conditions. We often get the middle of the
algorithm right but misjudge the boundaries.

Exhaustively Test Near Bugs
---------------------------
Bugs tend to congregate. When you find a bug in a function, it is wise to do an
exhaustive test of that function. You'll probably find that the bug was not alone.

Patters of Failure are Revealing
--------------------------------
Sometimes you can diagnose a problem by finding patterns in the way the test
cases fail. This is another argument for making the test cases as complete as
possible. Complete test cases, ordered in a reasonable way, expose patterns.

As a simple example, suppose you noticed that all tests with an input larger than
five characters fail.

Test Coverage Patterns can be Revealing
---------------------------------------
Looking at the code that is or is not executed by the passing tests gives clues
why the failing tests fail.

Tests should be fast
--------------------
A slow test is a test that won't get run. When things get tight, it's the slow
tests that will be dropped from the suite. So do what you must to keep your
tests fast.
