==========================
Object and Data Structures
==========================

Hiding implementation is not just a matter of putting a layer of functions between
the variables. Hiding implementation is about abstractions. A class does not
simply push its variables out through getters and setters. Rather it exposes abstract
interfaces that allow its users to manipulate the essence of the data, without
having to know its implementation.

We don't want to expose the details of our data. Rather we want to express our data
in abstract terms. Serious thought needs to be put into the best way to
represent data that an object contains. The worst option is to blithely add
getters and setters.

.. hint::
  Objects hide their data behind abstractions and expose functions that operate
  on that data.

  Data structures expose their data and have no significant behavior.

OO vs. Procedural
=================
Procedural code (code using data structures) makes it easy to add new functions
without changing the existing data structures. OO code, on the other hand, makes
it easy to add new classes without changing existing functions.

The complement is also true:

Procedural code makes it hard to add new data structures because all the functions
must change. OO code makes it hard to add ne functions because all the classes
must change.

In any given system we will sometimes want the flexibility to add new data
types, and so we prefer objects for that part of the system. Other times we will
want the flexibility to add new behaviors, and so in that part of the system
we prefer data types and procedures. Good software developers understand
these issues without prejudice and choose the approach that is best for the
job at hand.

Hybrid Objects
==============
The confusion whether something is an object or a data structure leads to
unfortunate hybrid structures, that are half object and half data structure.
They have functions that do significant things, and they also have either
public variables or public accessors and mutators that make the private
variables public, tempting other external functions to use those variables
way a procedural program would use a data structure. This is called Feature
Envy (:ref:`feature-envy`)

Hiding Structure
================
We should tell objects **to do something** and not ask them about their internals.

Data Transfer Objects
=====================
The quintessential form of a data structure is a class with public variables
and no functions. These objects are called data transfer objects (DTO). They
are especially useful when communicating with databases or parsing messages from
sockets. They often become the first in a series of translation stages that
convert raw data in a database into objects in the application code.

Beans have private variables manipulated by getters and setters. The
quasi-encapsulation of beans seems to make some OO purists feel better but
usually provides no other benefit.

Active Record
-------------
Active Records are special forms of DTOs. They are data structures with
public (or bean-accessed) variables, but typically have navigational methods
like ``save`` and ``find``. Usually these Active Records are direct translations
form database tables, or other data sources.

Active Records should not contain any business rule methods. Active Records should
be treated as data structures and you should create separate objects that contain
the business rules and that hide their internal data.
