==============
Notes
==============

Unit test
==========

- One assert per test


Functional Test and Unit Test
=============================
slow compared to unit tests
Customer focused

Functional tests for Happy Path and one of the sad path

Unit tests for the sad path.

Integration test
Technology focused

TDD
===

Think as the customer flow.

The Test code is the very first user of the new production code you are writing.
Testing gives you direct feedback in usability design.

TDD slows exploring development down. TDD needs a specifcation to test against. If
you are not sure w

Where do comments fit in TDD?

Modularity
----------
Splitting projects in smaller modules makes it easier to maintain etc.

faster deployment.
You only have to understand the small module to start making changes
