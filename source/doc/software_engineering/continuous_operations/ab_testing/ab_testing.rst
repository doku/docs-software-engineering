===========
A/B Testing
===========

A/B Testing is a way of testing features in your application for various
reasons like usability, popularity, noticeability.

.. figure:: ab_testing_example.png
	 :alt: ab_testing_example

Multi-armed bandit tests
========================
