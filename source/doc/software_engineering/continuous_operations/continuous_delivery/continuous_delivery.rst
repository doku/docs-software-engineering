===================
Continuous Delivery
===================

The goal is to reduce the cycle time between an idea and usable software in
production.

.. glossary::

  cycle time
    duration from deciding to make a change to having it in production

  continuous delivery
    The ability to get changes - features, configuration changes, bug fixes, experiments -
    into production or into the hands of users safely and quickly in a sustainable
    way. (Jez Humble, 2015)

The repeatability and reliability derive form two principles: automate almost
everything, and keep everything you need to build, deploy, test and release
your application in version control.

.. note::

  Operations at web scale is the ability to consistently create and deploy
  reliable software to an unreliable platform that scales horizontally. (Jesse Robbins)

Don't optimize your development stream for the case that you build the right feature.
Even in established projects (with a well known business case) 70% of the features
create zero negative value for the user. In new projects almost 90% of features
don't create value. Therefore your development should be optimized to allow you
to prototype features and get a MVP to your user as fast and cheaply as possible.
Only real user feedback allows you to distinct valuable features form all others.

.. hint::
  The goal of every action in the software development process should be to
  optimize the **value stream** that delivers value to your users.

Building features with zero or negative impact is extremely expensive:

#. We have opportunity cost from valuable features that we could have build in the same time.
#. These features add complexity to our system and create (as every feature)
   endless maintenance cost.
#. The increased complexity from the non-valuable features reduces the speed at which
   you can build future features.

.. hint::
  We need to gather data to find out if the features we are building actually
  deliver value to our customers.

- Be always ready to present new features in a working version of your software
- deploy critical bug fixes as soon as possible (a :term:`cycle time` of minutes is required)

Software releases should be a fast, repeatable process.

Our aim is to make the delivery of software from the hands of developers into
production a reliable, predictable, visible, and largely automated process with
well-understood, quantifiable risks.

.. note::
  Your software should be provable releasable on a daily basis (even if you don't
  release daily).

  Treat every check-in as a release candidate!

Anyone in your team should be able to deploy changes in normal business hours.

For successful software, the first release is just the beginning of the delivery
process.

.. hint::
  Many people when they talk about continuous delivery focus the fact that
  as a developer I can check code in and it can unimpeded by any problems go
  into production. But more importantly: if you truly embrace continuous delivery
  what you have done is lowering the risk of deploying into production
  and you have made it possible to deploy when it is important from the
  perspective of functionality, not when it fits the constraints that we had
  to put around the deployment process because it is so hard and error prone.
  (Rebecca Parsons)

Architectural Constraints
=========================
The most important factor in being able to implement continuous delivery is
the software architecture.

Can you deploy the service you are working on in isolation, with out having to
deploy all the dependencies?

Can you test the service you are working on, on your workstation, without
requiring an integrated environment?

There is a way to incrementally evolve your architecture using the
:ref:`strangler-application`.

Small Releases
==============
If you shrink down the size of each release you have a smaller delta between each
release. That means the risk of each release is much smaller and you are less
likely to have a problem with your release. If you do have a problem it is much
easier to diagnose and fix it.

If you release more often, your release process is more likely to work.

Deployment Pipeline
===================

Every change that is made to the application's configuration, source code, environment,
or data, triggers the creation of a new instance of the pipeline. The application
gets build, tested and automatically released.

.. figure:: deployment_pipline.png
	 :alt: deployment_pipline

The deployment pipeline has three goals:

#. Make every part of the process of building, deploying, testing, and releasing software
   visible to everyone involved, aiding collaboration.
#. It improves feedback so that problems are identified, and resolved, as early in
   the process as possible.
#. It enables teams to deploy and release any version of their software to any
   environment at will through a fully automated process.

Deploying software ultimately involves three things:

#. Provisioning and managing the environment in which your application will run
   (hardware configuration, software, infrastructure, and external services).
#. Installing the correct version of your application into it
#. Configuring your application, including any data or state it requires.

Continuous Deployment vs. Continuous Delivery
=============================================

With continuous delivery you are (from a technical perspective) able to deploy
every change into production but (from a business perspective) you may choose
not to. The decision of putting ever little change out into production is a
business decision not a technical decision.

With continuous deployment you actually deploy every change. In order to do
continuous deployment you have to practice continuous delivery.

Release Antipattern
===================
Usually the high degree of risk associated wit the release process makes it scary.
In many software projects, release is a manually intensive process.

There should only to things a human has to do on release day: pick a version and
environment and press the *deploy* button.

Automated deployment is reliable, testable and cheaper.

Your application should not be deployed to a production-like environment only after
development is complete.

.. hint::
  The longer a release cycle, the longer the development team has to make incorrect
  assumptions before the deployment occurs and the longer it will take to fix them.

If your application is installed by users, you may not have much control over their
environments. A great deal of extra testing will be required.

Antipattern: Manual Configuration Management of Production Environments

Manual configuration management of production environments (change in DB connection
etc.) should be avoided. All aspects of each of your testing, staging and production
environments, specially the configuration of any third-party elements of your system,
should by applied from version control through an automated process. You should
be able to repeatably re-create every piece of your infrastructure used by your
application. That means operating system, patch levels, OS configuration, application
stack, application configuration, infrastructure configuration etc. should all be
managed. You should be able to recreate your production environment exactly.

.. note::
  Every change made to production should be recorded and auditable. It should
  not be possible to make **manual** changes to testing, staging and production
  environments. The only way to make changes to these environments should be through
  an automated process.

It should be possible to use the same automated process to roll back to a previous
version of production if the deployment goes wrong.

Software Delivery
=================
We need ways to delivery high-quality valuable software in an efficient, fast,
and reliable manner.

.. hint:: Releasing software is too often an art; it should be an engineering discipline.

A working software application can be decomposed into four components:
executable code, configuration, host environment, and data. If any of them
changes, the behavior of the application could change and the whole application
has to be tested.

The key to fast feedback is automation.

.. hint::
  People are expensive and valuable, they should focus on producing valuable software
  not boring, error prone tasks like regression testing or deployment.

The ability to easily deploy any version of the software into any environment has
many advantages.

- Testers verify changes in behavior between older and newer versions
- any released version can be deployed into a test environment to reproduce defects
- a known good build can be deployed in case of errors in a new version
- releases are cheap and fast

.. note::
  Large systems depend on a lot of libraries and third-party code. All dependencies
  have to be version controlled to ensure reliability. Configuration Management
  makes sure your application is always in the right environment.

Configuration Information should be tested.

Hardware configuration should be changeable just by editing a configuration file.
This allows the system to be deployed everywhere from a laptop to a large enterprise
server.

Even high-profile product and feature releases become routine by using dark launch
techniques (:term:`feature toggles`). Long before the launch date, we put all the
required code for the feature into production, visible to everyone except internal
employees and small cohorts of real users, allowing us to test and evolve the
feature until it achieves the desired business goal. With the change of a configuration
setting or feature toggle we make the new feature visible to ever-larger segments
of customers, automatically rolling back if something goes wrong. As a result,
our releases are controlled, predictable, reversible, and low stress.


Value stream
============
The input to our process is the formulation of a concept/business objective.
From there development will transform the idea into user stories and implement,
test and integrate the code with the rest of the system. Value is created when
the service is running in production.

.. glossary::

  value stream
    A value stream is the sequence of activities an organization undertakes to
    deliver upon a customer request.

    In DevOps we define our technology value stream as the process required to
    convert a business hypothesis into a technology-enabled service that delivers
    value to the customer.

To enable fast and predictable lead times in any value stream there is usually a
relentless focus on creating a smooth and even flow of work, using techniques
such as mall batch sizes, reducing work in process (WIP), preventing rework to
ensure we don't pass defects to downstream work centers, and constantly optimizing
our system toward our global goals.

Lead Time vs. Processing Time
-----------------------------

The Lead time clock starts when the request is made and ends when it is fulfilled.
The process time clock starts only when we begin work on the customer request,
specifically, it omits the time that the work is in queue waiting to be processed.

.. figure:: leadtime_processtime.png
	 :alt: leadtime_processtime

Because lead time is what the customer experiences, we typically focus our
process improvement attention there instead of on process time.

The proportion of process time to lead time serves as an important measure of
efficiency.

In the DevOps ideal, developers receive fast, constant feedback on their work,
which enables them to quickly and independently implement, integrate and
validate their code and  have the code deployed into production environment.

Flow
----

The batch size ist the unit at which work-products move between stages in a
development process. For software, the easiest batch to see is code. every time
an engineer checks in code, they are batching up a certain amount of work. There
are many techniques for controlling these batches,

The single piece flow (form lean production) is realized with continuous
deployment, where each change committed is integrated, tested and deploy
into production.

We try to reduce the number of handoffs for any task to reduce the required
communication overhead.

We strive to reduce the number of handoffs, either by automating significant
portions of the work or by reorganizing teams so they can deliver value to
the customer themselves instead of having to be constantly dependent on others.
As a result, we increase flow by reducing the amount of time that our work
spends waiting in queue, as well as the amount of non-value-added time.

Improving flow through the technology value stream is essential to achieving
DevOps outcomes. We do this by making work visible, limiting WIP, reducing
batch sizes and the number of handoffs, continually identifying and
evaluating our constraints, and eliminating hardships in our daily work.

The principles of feedback
==========================
