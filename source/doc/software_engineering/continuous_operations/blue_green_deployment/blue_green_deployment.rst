.. _blue-green-deployment:

=====================
Blue Green Deployment
=====================

Blue Green Deployment is a deployment technique which you deploy your new blue
application version while your current green production version keeps running.
For Blue Green Deployment you need two identical environments (infrastructure).
Once your new version is running in the second environment you switch the router
so that all incoming request go to the new version.

If anything goes wrong you can easily switch the router back to your previous
(still running) version for a fast rollback.

.. figure:: blue_green_deployment.png
	 :alt: blue_green_deployment

Once your are happy with the new versions stability you can use the environment
of the old version as your staging environment for the final testing step for
your next deployment.

The fundamental idea is to have two easily switchable environments to switch
between.

If you need to change the database schema to support a new version of the
software, you need to separate the deployment of the schema changes form the
application upgrades. First you change the schema to support both the old and
the new version, deploy that and if everything is working fine you deploy the
new application version.
