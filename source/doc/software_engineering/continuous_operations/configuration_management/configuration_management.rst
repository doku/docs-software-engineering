========================
Configuration Management
========================

.. glossary::

  configuration management
    Configuration management refers to the process by which all artifacts
    relevant to your project and the relationships between them, are stored
    retrieved, uniquely identified and modified. (Jez Humble)

It is impossible to do continuous integration, release management, and deployment
pipelining without configuration management.

Version Control
===============
Version control systems allow you to keep multiple versions your files.

Every single artifact related to the creation of your software should be
under version control. From source code, release plan, specifications and
environment settings like firewall configuration have to be easily accessible
for every teammate.

Version control allows you to be aggressive about things that you don't
think you need. In case you were wrong, they can easily be restored.

The most important reason to write descriptive commit messages it so that,
when the build breaks, you know who broke the build and why.

Managing Dependencies
=====================
Your build system should always specify the exact version of the external
libraries that you use. It makes it much easier to correlate versions of
your software with the versions of the libraries that were used to build
them, if you keep your external libraries in version control. However,
it makes version control repositories bigger and check-outs longer.

Managing Server Configuration
=============================
You should threat the configuration of your system in the same way you treat
your code: Make it subject to proper management and testing.

Changes in configuration of you application or environment can just as
easily break you system as changes in source code can. If we change the
source code, there are a variety of ways in which we are protected form ourselves:
the compiler will rule out nonsense, and automated tests should catch most
other errors. On the other hand, most configuration information is free-form
and untested. Most systems won't catch a configuration error until run time.

Highly configurable software tends to become so complex to configure that
many of the benefits of its flexibility are lost, to the extend where the
effort involved in its configuration is comparable to the cost of custom
development. It's almost always better to focus on delivering the high-value
functionality with little configuration and then add configuration options
later when necessary.

Keep the available configuration options for your application in the same
repository as its source code, but keep the values somewhere else.
Configuration settings have a lifecycle completely different from that of
code, while passwords and other sensitive information should not be
checked into version control at all.

Usually there are no tests in place to verify that your software has been
configured correctly in testing and production environments. You should
use deployment smoke tests (:ref:`deployment-smoke-test`). You could at least
ping all external services.

Generally it is a bad practice to inject configuration information at build
or packaging time, because you should be able to deploy the same binaries to
every environment so you the thing that you release is the same thing that
you tested.

.. _deployment-smoke-test:

Deployment Smoke Test
---------------------
You should run smoke tests once your application is installed to make sure
it is operating as expected. Test should exercise functionality that depends
on the configuration setting being correct. If the results are not as expected
the installation or deployment process should fail.

Managing Environments
=====================
Every application depends on hardware, software, infrastructure and external
systems in order to work. This is referred to as your applications environment.
The configuration of the environment your application runs in is as important
as the configuration of the application.

In order to reduce the cost and risk of managing environments, it is essential
to turn our environments into mass-produced objects whose creation is
repeatable and takes a predictable amount of time. The key to managing
environments is to make their creation a fully automated process.

Fixing one of your environments can take many hours. It is always better to
be able to rebuild it in a predictable amount of time than trying to get
it back into a known good state. It is essential to be able to create copies
of production environments for testing purposes.

Important environment configuration information:

- operating systems with version, patch levels and configuration settings
- required software packages
- networking topology
- required external services
- any data or state that is present in the environment (for example databases)

.. hint::
  Keep binary files independent form configuration information and keep all
  configuration information in one place.

Puppet and CfEngine are tools that make it possible to manage operating system
configuration in an automated fashion.

A production environment should be completely locked down. It should not be
possible for anybody to make a change to it without going through your organization's
change management process. A change must be tested before it goes into production
and for that it should be scripted and checked into version control.

You should not only manage production environments but also development environments.
It should be possible to execute a simple command to set up a workstation from
scratch, which sets up everything, from required programs to code-repositories and
local testing environments.
