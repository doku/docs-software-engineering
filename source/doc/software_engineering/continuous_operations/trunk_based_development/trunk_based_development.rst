.. _trunk-based-development:

=======================
Trunk-Based Development
=======================

Team members have to commit their changes to master multiple times a day, to allow
:ref:`continuous-integration`. The creation of a branch defers the integration of new
functionality, integration problems are only found when the branch is merged.

Trunk Based Development prevents you form painful merge conflicts.
Merging refactoring is **really** hard. Working in feature branches discourages
refactoring. Merging tools often don't solve semantic conflicts, such as somebody
renaming a method in one branch while somebody else adds a new call to that
method in another branch.

.. figure:: trunk_based_development.png
	 :alt: trunk_based_development

.. hint::

  Trunk-based development predicts higher throughput and better stability,
  and even higher job satisfaction and lower rates of burnout. (Jez Humble,
  data from 2015 State of DevOps Report)

If you are using branches to avoid integration, you are probably not going
to be optimizing for frequent delivery of software, you are not optimizing
for early integration. By definition, if you are working on a branch, your
code is not being integrated with that of other developers.

Branches can be used for experimental and release branches.

To ensure you aren't going to break the application when you check in, you should
run your commit test suite before the check-in and commit your changes regularly..

Half-finished features
======================

.. glossary::

  Feature Toggles
    Allow you to hide partially implemented features in the running system.

Through configuration files or command line flags features can be activated or hidden.

:ref:`feature-toggles`

.. todo:: Architecture for Feature Toggles

You can use tools like ``Zoo`` to allow configuration management of your running
software.

This technique is easy when you are developing a new feature. If you are changing
existing behavior you need

Rather than using feature branching, where your change in behavior is, instead
you are coexisting both implementations in the same codebase at the same time
via an abstraction point. This pattern is called **Branch-by abstraction**.

.. todo:: branch-by abstraction research

.. figure:: implementation_coexisting_abstraction.png
	 :alt: implementation_coexisting_abstraction

This requires you to refactor your code to create an abstraction point.

This use of an branch-by abstraction modle allows you to be checking in a
change on existing behavior without having branching. You then can turn which
abstraction you are using with a feature toggling mechanism.

Rules for Feature Toggles
-------------------------

#. Use a flag in as few places as possible (only one place in codebase)
#. Remove flags once your are done

Feature toggles allow you to use **A/B testing** and you can ship
each implementation to different costumers and compare their interaction with
your software.

Additionally you can test the behavior of your two different coexisting behaviors
against each other.

You can use feature toggles for **canary releasing**. You release a change to a
small subset of your users and if it behaves correctly you can ramp up the number
of people who see it.

Short-living Dev-Branches
=========================
To make sure master always stays green, feature development happens in short living
development branches.

#. Create Issue for Feature

A new WIP Merge Request is created automatically and the changes go through CI
testing. A codereview is done and the code gets merged into master.

Open Source Projects
====================
Trunk based development only works for trusted committers. When working on
a project with many untrusted committers safety mechanisms such as
Merge Requests are required.

Sources
=======

https://trunkbaseddevelopment.com/

https://www.youtube.com/watch?v=lqRQYEHAtpk&index=18&list=WL
