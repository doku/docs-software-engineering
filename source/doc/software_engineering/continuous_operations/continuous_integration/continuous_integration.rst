.. _continuous-integration:

======================
Continuous Integration
======================

You are doing CI if, and only if:

#. Everybody in the team commits to the main line at least once per day.
#. You have a solid test suite, you are confident will find any bug before it went
   into production.
#. You fix the pipeline within 10 minutes if it breaks.

Continuous integration detects any changes that breaks the system or does not
fulfill the customer's acceptance criteria at the time it is introduced to the
system. Crucially, if the build or test process fails, the development team
stops whatever they are doing and fixes the problem immediately. The goal of
continuous integration is that the software is in a working state all the time.
Never check in on a broken build. If you do so you could introduce additional bugs
increasing the time it takes to get the build passing again.

Without continuous integration, your software is broken until somebody proves
it works, usually during a testing or integration stage. With continuous integration
you software is proven to work (assuming a sufficiently comprehensive set of
automated tests) with every new change and you know the moment it breaks and can
fix it immediately. Bugs are caught much earlier in the delivery process when they
are cheaper to fix, providing significant cost and time savings.

.. glossary::

  integration
    The process of combining software components, hardware components, or both
    to an overall system.

  integration testing
    Testing in which software components, hardware components, or both are
    combined and tested to evaluate the interaction between them.

Im Zuge der Integration soll aus den Bestandteilen ein vollständiges und
funktionsfähiges System entstehen. Die Software-Architektur liefert den
Bau- und Montageplan.

Solange das System noch nicht vollständig integriert ist, wird das Ergebnis jedes
Integrationsschritts als teilintegriertes System bezeichnet. Typische Probleme
bei der Integration sind **inkompatible Schnittstellen**. Diese können durch
Fehler in der Umsetzung oder in der Spezifikation entstehen.

Bei einer **Big-Bang-Integration** ist die Wahrscheinlichkeit hoch, dass das
System nicht lauffähig ist, wenn die Komponenten Fehler oder Inkonsistenzen
enthalten.

.. hint::

  Continuous Integration reduces risk and helps ship a more stable product.

  Continuous Integration detects any change that breaks a system or does not
  fulfill the customer's acceptance criteria at the time it is introduced into the
  system.

With CI, if your tests are sufficiently comprehensive and you are running tests
on a sufficiently production-like environment, then your software is always in a
*releasable* state. The most important practice for continuous integration to work
properly is frequent check<-ins to master (:ref:`trunk-based-development`).

The CI pipeline should be finished after 2-3 minutes to give you first feedback.
The entire regression Testsuite may take longer.

The CI process gives you trust that the system will catch any mistakes and problems!
This will completely change your relationship to your code. Every change is expected
to pass all tests and be released into production. CI shows that a particular release
candidate is not fit to make it into production.

Developers should be able to build and run unit tests and :ref:`smoke-test`
against a working system on their developers machine prior to each check-in.
Before committing you should pull changes from version control and run all
commit tests locally. If you follow this practice you know that either the build
fails because someone else has checked in in the meantime, or because you have
forgot to add the new class of configuration file (a very common reason for failed
builds) to version control. You can setup a pretested-commit stage, where your
CI server takes your local changes, runs the tests on the CI grid and checks-in
your changes to the repository if they pass the tests.

If a check-in fails and you can't fix the problem quickly, you should revert to
passed version, keeping the failed change on your local machine.

When the build breaks on check-in, try to fix it for ten minutes. If, after ten
minutes, you aren't finished with the solution, revert to the previous version
from your version control. You are responsible for every build failure caused by
your changes. To fix regressions you need access to the entire code base and
you need to be able to modify all code sections.

CI Rules
========
#. Don't check in on a broken build.
#. Always run all commit tests locally before committing, or get your ci server
   do it for you.
#. Wait for commit tests to pass before moving on.
#. Never go home on a broken build (fix it or revert it or better don't check-in at
   the end of a work day.)
#. Always be prepared to revert to the previous revision.
#. Don't comment out failing tests! Either fix the code if a regression has been
   found, modify the test if one of the assumptions has changed, or delete it if
   the functionality under test no longer exits.
#. Take responsibility for all breakages that result form your changes and fix them.

.. figure:: ci_certificate.png
	 :alt: ci_certificate

Useful CI Checks
================

Failing a Build for Architectural Breaches
------------------------------------------
You can check for architectural patterns that are required/forbidden.
This is especially useful if you want to assert that there are no unexpected
dependencies between specific components. You can check in you CI that components
don't call each other if they are not allowed to do so.


Failing a Build for Slow Tests
------------------------------
You can let the build fail for any tests that take longer than two seconds

.. note::

  This can result in flakey test behavior, where tests fail
  if the CI environment is under unusual load. You can occasionally run this
  test performance checker if your suite starts running slow.

Failing the Build for Warnings and Code Style Breaches
------------------------------------------------------
Using code analysis you can fail the build if the number of todos or warnings
increases.

Promiscuous integration
=======================
Promiscuous integration is a small step away form continuous integration.
In this model, contributors pull changes not just between their fork and the central
repository, but also between different forks. This pattern is common in larger
projects that use GitHub, when some developers are working on what are effectively
long-lived feature branches and pull changes from other repositories that are
forked off the feature branch.
