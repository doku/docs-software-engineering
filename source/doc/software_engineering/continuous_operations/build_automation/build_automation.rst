=========================
Infrastructure Automation
=========================

Basics
======

Treat your infrastructure like it was code.

Testing your infrastructure
===========================

test your infrastructure code and infrastructure

unit testing infrastructure
---------------------------

Formatter Rubocop

Linters enforce language standards.

enforce standards

chefcook

chefspec

integration testing
-------------------

serverspec

security tests
--------------

inSpec

Gauntlt

artifacts to systems
====================

.. glossary::

	provisioning
	  The process of making a server ready for operation, including hardware, OS,
	  system services, and network connectivity.

	Deployment
	  The process of automatically deploying and upgrading applications on a server.

	Orchestration
	  is the act of performing coordinated operations across multiple systems

	Immutable:
	  Deployments that are not intended to be changed after deployment, but instead
	  are just redeployed if needed.

	  Most Docker architectures and Netflix's baked image Amazon architecture are
	  immutable. The deployed services do not use convergent CM to change themselves.
	  But instead, they're destroyed and recreated when you want to affect change.

	idempotent
	  code can be run over and over without any unintended changes.


Software as a service or SaaS is where an entire application is provided for you.

Platform as a service or PaaS is where you can deploy custom applications to the
cloud platform without managing the underlying systems yourself.
Google App Engine, Microsoft Azure App Service, and Pivotal Web Services which
has hosted Cloud Foundry are examples of public PaaSes.

Infrastructure as a Service, pronounced IaaS for obvious reasons, is where you
get operating system level access to discrete systems in the cloud. These are
usually Vms and not bare metal. Google Compute Engine, Microsoft Azure Virtual
Machines, and Amazon Web Services EC2, showcase IaaS offerings.

Differences in development and production environment can lead to unrecognized
errors. With virtualization environments can be consistent from dev to production.

``Vagrant`` multi platform dev tool, emulate production environment.

.. figure:: timeline.png
	 :alt: timeline

.. figure:: value_vs_work.png
	 :alt: value_vs_work

Value
  steps that add specific value to your end product

Waste
  work that does not add value, even if it is nnecessary. Hosting is waste, as
  is delay

Value Stream
  the full sequence of steps to create your end product, including value creatin
  steps and waste.

Tools
-----

``rundeck``   define tasks and workflow to run one klick commands to reduce human errors
