======
DevOps
======

DevOps is the application of agile methodology to system administration
(from: The practices of cloud system administration)

.. hint::
	The DevOps movement is focused on encouraging greater collaboration between
	everyone involved in software delivery in order to release valuable software
	faster and more reliably.

The old way was to build and ship software. In the new way services are running
and managed. These services are never done and are never turned off. Operations
become the driving point of a cloud-based DevOps organization.

The entire application lifecycle needs to be covered, understood and automated within
the process of DevOps.

With Devops a organization shifts away from a product-centric view to a service-centric
view.

.. figure:: service_centric_view.png
	 :alt: service_centric_view

.. note::
	Culture is the most important aspect to devops succeeding in the enterprise.
	(Patric Debois)

.. figure:: application_livecycle.png
	 :alt: application_livecycle

.. note::
  Cloud and DevOps can significantly improve how we deliver enterprise IT serivces.

.. figure:: software_livecycle_automation.png
	 :alt: software_livecycle_automation

.. figure:: infrastructure.png
	 :alt: infrastructure

Focus on design and implementation rather than on deploys.

- Automate everything
- Test everything
- continually build and deploy everything
- keep collaboration in mind

.. figure:: software_factory.png
	 :alt: software_factory

.. figure:: devops_example.png
	 :alt: devops_example

For an efficient operating model we need automation of builds, changes,
:term:`provisioning`, testing and operations. Releases should be small and frequent.
No longer are we releasing software which is based on the needs at an instance of time.
We are constantly approving and updating the software as our needs suggest, as the
business dictates to us.

.. attention::
	The important thing is the focus. Devops is a wonderful result of a healthy
	culture and healthy thinking.

Continuous Everything
=====================

.. figure:: continuous_delivery.png
	 :alt: continuous_delivery

The goals of testing your code

- confirm the desired functionality
- catch programming syntax errors
- Standardize code patterns and format
- reduce bugs
- make the application mode secure

Continuous testing is the bridge between continuous deployment and  continuous delivery.

.. figure:: continuous_testing.png
	 :alt: continuous_testing

Continuous Testing should include security testing and performance and integration
tests.

.. figure:: continuous_integration.png
	 :alt: continuous_integration

Having humans involved in deployment and delivery introduces a lot of latency
ultimately reducing the pace at which developers are able to deliver changes to
the end users.

.. figure:: automation_stages.png
	 :alt: automation_stages

By leveraging automation you can take all the latency out of the process.

DevOps Stages
-------------

1. Chaos Reigns
2. Continuous Integration
3. Continuous Delivery
4. Continuous Deployment
5. Continuous Operations


.. figure:: devops_maturity_model.png
	 :alt: devops_maturity_model

Devops maturity model or continuous delivery maturity model

Cloud Adoption
--------------

Clouds, as shared and abstracted hardware resources, are able to leverge resources
im much more effective ways. You are only paying for what you use and you don't
have to buy hardware, software and the required maintenance. The economies of scale
that cloud computing is able to provide reduce the cost of keeping your application
up and running, while giving you the ability to scale up and scale down your
applications hardware resources as needed.

Testing the DevOps Process
==========================

- test each tool as a unit
- test clusters of tools (integration)
- test the complete process

DevOps Business Benefits
========================

.. figure:: devops_buisness.png
	 :alt: devops_buisness

Core Conflict
=============
IT organizations must pursued the following goals simultaneously:

#. Respond to the rapidly changing competitive landscape
#. Provide stable, reliable, and secure service to the customer

Usually Development will take responsibility for responding to changes in the
market, deploying features and changes into production as quickly as possible.
IT Operations will take responsibility for providing customers with stable and
reliable services, making it difficult for anyone to introduce production
changes that could jeopardize production. Configured this way, Development and IT
operations have diametrically opposed goals and incentives.

.. hint::
	The core, chronic conflict: Organizational measurements and incentives across
	different silos prevent the achievement of global, organizational goals.

Ideally, small teams of developers independently implement their features, validate
their correctness in production-like environments, and have their code deployed
into production quickly, safely and securely. Code deployments are routine and
predictable. They should occur at regular business hours when everyone is at the
office. Automated testing helps developers discover their mistakes quickly
(usually within minutes), which enables faster fixes as well as genuine learning
- learning is impossible when mistakes are discovered six months later during
integration testing.
