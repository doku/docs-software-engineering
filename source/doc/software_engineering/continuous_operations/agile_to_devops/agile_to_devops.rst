====================
From Agile to DevOps
====================

Problem
=======

- When code gets tossed over the fence, nobody wins
- There is a lack of an operational mindset in the development process
- There is a lack of a development midnset in the operational process
- Coordination of group resources happens too late

How do I get an Idea/Feature/Bugs (something new) out to Production so that my
customers is using it and giving you feedback? And Based on that feedback you
can improve. You improve your software (based on feedback on usability or
additional feature requests), the environment (based on performance analysis of
your application), and the delivery process (deployment process of application
and infrastructure, collaboration of dev, qa etc. )

If you get to a level of high efficency than you are able to innovate. You can
do several small releases and test them out on the market (A-B Releases etc.)

Das Ziel von eines Unternehmens ist es immer value beim Kunden zu erzeugen.
Dieser Value wird erst dann erzeugt, wenn der Kunde die Software tatsächlich
nutzen kann.

Devops is a Movement that aims to help development and operations work better together,
to have better colaboration and better communication.
The goal was to reduce friction for getting.

Devops gives us rapid feedback which we can use for two things:
  - improve the product: how do the customers use the new feature etc.
  - improve the software delivery process it self (by reducing rework and overhead)

Introduce Devops to your Organization
=====================================
