.. _feature-toggles:

===============
Feature Toggles
===============

Source: https://martinfowler.com/articles/feature-toggles.html

Feature toggles:

Your Toggle router can take the Toggle Context (specific users, environment, locale
etc.) into account and enable features based on context.

This allows you to do :ref:`canary-release` and enable new features for a
small test group of your users.

.. figure:: feature_toggles_grafic.png
	 :alt: feature_toggles_grafic

Categories of toggles
=====================

.. glossary::

  Release Toggles
    Release Toggles allow incomplete and un-tested codepaths to be shipped to
    production as latent code which may never be turned on. They are used
    to enable :ref:`trunk-based-development`. Using Release Toggles is the most
    common way to implement the :ref:`continuous-delivery` principle of
    "separating feature release from code deployment"

  Experiment Toggles
    Experiment Toggles are used to perform multivariate or A/B testing.
