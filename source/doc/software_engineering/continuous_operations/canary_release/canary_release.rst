.. _canary-release:

==============
Canary Release
==============

Source: https://martinfowler.com/bliki/CanaryRelease.html

**Canary release** is a technique to reduce the risk of introducing a new
software version in production by slowly rolling
out the change to a small subset of users before rolling it out to the entire infrastructure and making it available
to everybody.

.. figure:: canary_release.png
	 :alt: canary_release


(:ref:`blue-green-deployment`)
