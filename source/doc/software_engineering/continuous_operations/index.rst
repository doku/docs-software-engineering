=====================
Continuous Operations
=====================

.. toctree::
  :maxdepth: 2
  :glob:

  continuous_integration/*
  continuous_delivery/*
  configuration_management/*
  dev_ops/*
  build_automation/*
  trunk_based_development/*
  feature_toggles/*
  agile_to_devops/*
  blue_green_deployment/*
  canary_release/*
  ab_testing/*
