========================
Requirements Engineering
========================

Stakeholder
===========

Quadranten Image

Herausforderungen
-----------------

- Zielkonfilkte zwischen Stakeholdern

Requirements
============

One-Criteron Classification:

Essential
  System is inaceptable until all of these requirements are met.

Conditional
  Requirements would improve the Product but are not essential

Optional
  Requirements can be useful

Kano Model


Wow-Effect Requirements werden im lauf der Zeit zu Basic Requiremets, da sich
Nutzer daran gewöhnen und es ab dann erwarten.

Anforderungsmanagement
======================

Jeder Stakeholder muss die für ihn relevanten Anforderungen wiederfinden und
zumindest lesend zugreifen können.

Die Abhängigkeit zwischen Anforderungen müssen dokumentiert werden. Es muss
möglich sein, eine Anforderung durch verschiedene Artefakte zu verfolgen.
(Traceability)
