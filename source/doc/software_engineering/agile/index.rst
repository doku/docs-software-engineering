=================
Agile Development
=================

.. toctree::
  :maxdepth: 2
  :glob:

  nature_of_software_development/*
  software_development/*
  agile_software_engineering/*
  movements/*
  scrum/*
  extreme_programming/*
  monolithic_repository_development/*
  task_priorisation/*
  application_lifecycle/*
