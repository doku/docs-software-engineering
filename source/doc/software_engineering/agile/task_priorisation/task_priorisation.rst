=================
Task Priorisation
=================

.. figure:: strategic_vs_utility.png
	 :alt: strategic_vs_utility


You have to manage projects in strategic areas of your business very different
from utilitarian projects, because the goals and measures of success are
different.

The distinction is in business function not in the software function. 

.. figure:: strategic_utility_vs_two_speed_it.png
	 :alt: strategic_utility_vs_two_speed_it
