=================================
Monolithic Repository Development
=================================

Adventages of a monolithic repository:

- Unified versioning, one single source of truth
- Extensive code sharing and reuse
- Simplified dependency  management
- Atomic changes (largescale, backwards incompatible changes are easily possible.)
- Large scale refactoring, codebase modernization
- Collaboration across teams (you can fix bugs in other teams code)
- Flexible team boundaries and code ownership

Monolithic Repositories need significant investment in tools to allow the required
scalability.

You need to enforce sane and healthy dependency management, as a monolithic
repository makes is very easy to add new dependencies. APIs should have a
visibility setting, allowing teams to explicitly set their APIs as appropriate
for use. 
