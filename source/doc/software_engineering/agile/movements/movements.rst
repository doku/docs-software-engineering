=========
Movements
=========

The Lean Movement
=================

The Agile Manifesto
===================
In 2001 the Agile Manifesto created a lightweight set of values and principles
against heavyweight software development processes such as waterfall development.

One  key principle was to *deliver working software frequently*, emphasizing
the desire for small batch sizes, incremental releases instead large, waterfall
releases.

Agile is credited for dramatically increasing the productivity of many development
organizations.

Agile Infrastructure and Velocity Movement
==========================================
It was started in 2008 when a conference session showed the possibilities of
applying the agile principles on infrastructure.

  
