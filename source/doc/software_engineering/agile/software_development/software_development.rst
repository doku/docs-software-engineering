====================
Software Development
====================

.. hint::
  The goal of software development is to sustainably minimizing lead time to
  business impact.

We want to be able to create business impact at will in a very short period
of time sustainably.

.. note::
  The goal is not to produce software.

Code is not the asset.

Code is the cost:
  - writing code costs
  - waiting for code (cost of delay)
  - changing code costs (Maintenance)
  - understanding code costs

There are three types of code: The code that you know (because you have recently
written it), the code that everyone in the team knows (because it is well documented,
stable and around for a long time) and then there is the code that no one knows.

In a typical code base there is a huge percentage of code is old enough that
no one exactly remembers it and not really cared for.

Code should be very new or already very stable.

.. note::
  Code should be **stabilized** or **killed off**.

  Kill code fearlessly!

That result in a code base where we only have code we wrote recently or code that
is stable and well-known.

Short Software Half-Life
========================

The half life of software is the time period it takes for module to have half
of its code naturally changed and half of it stays the same.

Design considerations:

- write discrete components
- define component boundary
- define component purpose (why is it there?)
- define component responsibility (what it owns?)

Well written Code self documents **what** it does. It doesn't tell you its
purpose.

Stewardship considerations

- write component tests and docs (:ref:`architecture-decision-records`)
- optimize for replaceability
- expect to invest in stabilizing

Fits in my head
===============

You can only reason about something that fits in your head.

Mocking and stubbing is a fantastic way of actively ignoring things happening
around you. In order to reason about and test this module you have to ignore
lots of other things, which suggests that it is coupled.

Consistency
===========

We need consistency in the code base. You should not be able to tell from looking
at the code who wrote it.

- agree guiding principles
- agree idioms
- difference is data

Objects-oriented focused on Messages

Objects are Entities.

Spike and Stabilize
===================

A Spike is an experiment in the codebase. Spike rules:

- a spike has to be time boxed
- you have to promise you throw it away and do it properly

The point of a spike is to learn something.

You write all of your code and act as if you code is likely to be thrown away
afterwards. Therefore you don't overthink it.

The Spike and Stabilize pattern only works if you promise to stabilize.
The first time you come back to your spike you have to stabilize it. Set a timer
to make sure that even if you don't revisit the code you stabilize it within
the first 6 weeks of its creation.

Hair Trigger
============
Hair Trigger is the ability of a team to deploy its own code into production.
The team needs to understand the risks and impacts of releasing something.

Ginger Cake
===========
The Ginger Cake Pattern is about copying code in order to get out the gate
quickly.

You only use this if  you know that code intimately.

The dark side of **DRY** (:ref:`dry-code`) is, that it introduces coupling.
As soon as you introduce an abstraction or pull out a library and share it between
two components, those two components are now coupled together. You now no longer
can independently reason about one of these components without having to think
about the implications about the other one.

DRY reduces duplication but introduces coupling.

Structural Ginger Cake
----------------------
copy the shape and replace the behavior

Behavioral Ginger Cake
----------------------
copy behavior and change some pieces

Shallow Silos
=============
Teamorganiation

Try things, see what works for you.

Burn the ships
==============
Learning Pattern

When you learn a new technology set yourself a deadline to do something specific
with that technology.

To trigger a organization change you need:

- Downward pressure (deadline)
- Run out of options
- Informations

Burn the ships creates the first two artificially.
