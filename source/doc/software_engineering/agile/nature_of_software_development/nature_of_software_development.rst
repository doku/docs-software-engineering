==================================
The Nature of Software Development
==================================

There is a **Natural Way** to build software. The essential flow of software
development is simple. Good work is simple; it is not easy.

The first thing we need to know for planning is the projects deadline.

Value
=====

#. Value
#. Quality (well designed and tested)
#. Slicing (break features down to the smallest possible value-bearing size, build a capable product as early as possible)
#. Building (feature by feature with continuous delivery to frequently deliver value)
#. Planning (prioritize features)
#. Organizing (teams, skills)
#. Guiding (when, what)

The goal of every system is to create **value**. In software, we generally get value
by delivering features. A project delivers value only when we ship the software and
put it in to use. So break each feature in minimum marketable features and release
them as soon as possible. This creates value for your user and allows you to have
real world testing of your new feature. One very good way to find out if we are
going in the right direction is to ship a small version of the product early. If
it flops, we can change direction at low cost.

High-value, low-cost features should be developed and released first.

.. hint:: Best value comes form small, value-focused features, delivered frequently.

.. figure:: feature_prioritization_by_cost_of_delay.png
	 :alt: feature_prioritization_by_cost_of_delay

Agile methods ask us  to choose the order in which we do things based on their
*business* or *customer* value. The task of the **product owner** (:ref:`product-owner`)
is to look deeply at the many things we value, and choose a sequence of development
that gives us the best possible result in return for our time, money, and effort in
building our product. We need to choose the things that matter, among all the things
things we might do.

Project management
==================
With a monolithic project and closing deadlines we can't do much to cut costs.
We have already written requirements for things we'll never get. We have designed
and even written code for things that we'll never complete. All that work is wasted.
The sequential software development (:ref:`ref-sequential-software-development`)
uses an all-or-nothing mentality. You analyze everything, design everything
and try to code and test everything but fail.

A project that delivers feature by feature is more predictable.
You can ship real, valuable features in frequent intervals.

.. hint:: Feature by feature gives better information, better guidance, better results.

Organize the developers in small teams, each of which builds features form start
to finish. The teams need a diverse skillset to complete the task. Responsibility
and authority are aligned. If features must be passed through multiple teams, it
takes longer and results in lower quality (unnecessary coordination overhead).

These diverse teams of developers, UX and database experts encourage knowledge transfer
(both in terms of experience and expertise) and offer great horizontal scalability.

If you need faster development just add a new feature team. Your growing suite of
tests and automated checks help the teams know when they have completed a feature.
The tests also serve as a growing collection of regression checks that ensure all the
features build keep working.

**Side Note**: Each development team will most likely perform local optimization to
improve its efficiency. This is similar to a :ref:`greedy-algorithm` and
can substantially hinder any global optimization.

Planning
========
The product *vision* starts with big, grand ideas, vague yet enticing. Vision
is about big ideas, not tiny bites.

.. note:: "Plans are useless, but planning is indispensable" (Eisenhower)

It's important to identify key features that we'll need to have early, as well
as features we can't live without.

Humans are extremely bad at estimating time and cost of a project. So don't even
try. Just set a time and money budget and produce the most valuable features first.
Keep the product ready to ship at any time and stop when the clock runs out.

As an extreme idea: your product should be shippable under any commit. This
mentality helps you to make commits with clear propose and ensures you **never**
commit any broken code.

Stories should be split in smaller features, rather than in  technical steps.

During planning it is tempting to set up *stretch goals* and try to add *just one more feature*.
The team will try to please the goals and has to unconsciously hurry, leading
to poor quality and test coverage. It is far better to do eight things well, than
ten things poorly.

Under pressure, teams give up the wrong things. They don't test enough; they leave
the code in poor conditions. This reduces value, increases the delay to getting
the value, and reduces the value they can deliver later.

.. hint:: Plan often, select what's next, don't overeat.

If you must try to go faster, analyze sources of delay. These usually have more
impact than individual productivity. Increase individual productivity by increasing
capability not by urging people to work harder: "*Work smarter, not harder*".

Once we become proficient at breaking all features down to approximately the same
size, we can manage the  project very nicely, because we have a good sens of how
long things will take. Since our job is to select the work to do versus the work
to defer, and to select the most valuable work first, we can steer the project to
success without the overhead of estimation.

Generally speaking, estimates are likely to be wrong, and they focus our attention
on the cost of things rather than on the value. Consider de-emphasizing or eliminating
cost estimates and steering to success by a focus on value.

The time needed to build a feature comes from two main components: its inherent
difficulty and the accidental difficulty of putting it into whatever code already
exists. Teams are good at estimating the inherent difficulty. Bad code slows us
down and highly increases the accidental difficulty.

Feature by Feature
==================

.. hint:: Build a tiny product, completely, in each cycle.

We plan and manage our project in short one- or two-week cycles. In each cycle, we
define the next few features to build and define the acceptance tests.
In each of these one- or two- week iterations, we go through a complete product
development cycle, form concept to ready to ship.

When we envision a major feature, we see it in its fullest possible glory, just
as we see our product with every possible feature. Just as it would be wrong
to build every possible feature. it is wrong to start by building each feature out
to its fullest glory. The desirability of our product depends on a set of features
that is complete enough to attract and satisfy our users.

We take our grand vision of a major feature and break each component down into
elements small enough for our team to build in one week.

The business side has to break down large, vague, sweeping requirements into small.
practical steps that deliver maximum value for minimum effort. We need to sharpen
our vision of what the product must do and what's just *nice to have*. The result
is a faster return on our software investment.

It's critical to do the most valuable features first. Everyone needs to see the
actual progress on a feature. We can't accept "90% done". Features are either
**done** and pass all required acceptance tests or they are **not done**.
In :ref:`ref-scrum-introduction` the :ref:`ref-definition-of-done` defines when an
artifact is done. A feature that can not be shown is not done.
When we can see real, running features, we have a clear and solid information about our
project's condition. Each feature should be continuously deployed to a development
environment so business, marketing and other developers can see it up and running.
This enables them to give feedback.

We absolutely need a releasable, done product increment every couple of weeks. To
accomplish this, our work must be fully designed, fully tested and completed
in every way.

In each cycle we develop a potentially shippable software. If a feature is not
completely done, it will not be included in the release and it will be part of the
next iteration. Half finished or untested code should never be merged into master.
This prevents a long and painful block of "testing and fixing" old features.
At the time the team works on a feature everyone knows the most about the
internal process and potential bugs and difficulties. So all of them should be
addressed when the team is. If we have to fix bugs or write tests for features
released months ago, the team has to read all the code once again making progress
slower and more expensive.

For a feature-by-feature development to work, the software needs to be nearly free
of defects at the end of every two-week iteration.

Growing Architecture
====================

As our project grows and we constantly add new features we need our design to grow
too. If we design too much, we won't get as many features. If we design too little,
features will be hard to do, we'll slow down. By observing how our velocity (the pace
at which we ship features) changes, we'll learn how to *right-size* our design effort.
Tweak, observe, tweak.

Most of us have seen software whose design got so bad that progress became almost
impossible. The techniques for keeping the design good enough are easy to learn.
They are also easy to forget, especially if the team is under a lot of pressure.

.. hint::

  Everything we build must rest on a solid architecture, design and infrastructure.
  Features do need to be supported by a foundation, and the foundation needs to be
  well designed.

To keep the features coming smoothly, we nee d to keep the system's design solid
from the first day to the last. Without a food design foundation, the product will
be full of defects and hard to work with.

.. note:: Foundation first means too few features get to market.

If we build out the design first, we almost always wind up with to few shippable
features. Each feature represents value to our users, and revenue or other benefits
to our business. We need as many as we can reasonably get.

.. attention::

  We dare not build the foundation first: it will defer and inevitably
  reduce the product's value.

It's safer by far to build a simple yet functional version of each feature first.

We get the most value if we do small versions of each necessary feature, with just
enough foundation to be solid. We build what is called a **minimum viable product**
as quickly as we can. Rather than take chances with how much infrastructure to
build, or how many features we will be able to get done by our finish date, we work
in small versions. Each feature version makes the product a bit better so that we
always have the best possible product at every moment. In every iteration we can
make priority decisions. Because we are always ready, we can ship early if there
is any reason to do so.

.. hint:: Refine each feature in multiple iterations.

.. note::

  Our product is made up of a growing set of correctly working features, build
  on a growing, evolving foundation of design.

The design can easily deteriorate. Inferior design will slow us down. Skill and
care are required to keep our project alive.

Every change tends to break our current design. Refactoring (:ref:`refactoring`)
keeps the design good as it changes.

In each development cycle at least one maintenance task should be added to the
feature list. This allows you to have a constantly planned maintenance cycle
keeping your code clean. The proactive maintenance prevents you from degrading
code quality and maintenance in a rush. (:ref:`ref-software-maintenance`)

Quality Assurance
=================

We're trying to plan and grow by features. Any defects in our features amount
to negative features. Defect repair adds unknown delay. Repair as you go to provide
the clarity on what's done. Finding and fixing defects takes a lot of time. So make
sure you get it right before calling your features done.

If we don't know in time what's done, and how well it's done, we will have no choice
but to ship late with visible defects. That's bad for business.

Because features are being added and enhanced, and because the design is evolving,
we will make mistakes. We need continuous comprehensive testing. At the end of
each iteration we need to have the software as close to defect-free as possible.
We have to test our new features to make sure they fulfil the requirements and we
have to test our old features to make sure we didn't break anything. When we use
small releases, the chance that they break something is very small.

We must build business-level tests that check every aspect of every feature that
we possibly can. If we don't check something, we don't know whether it works.

Automated tests give us assurance that the feature works now and form now on.
This is called **acceptance test-driven development** (:ref:`ref-atdd`)

Unit Tests make sure that problems are found sooner and fixed more readily.

Almost paradoxically, all this testing makes our team go faster. The reason: we make
fewer mistakes, and they are found more quickly. It takes longer to find a problem,
and fix it, than it does to prevent the problem in the first place. Testing as a
part of our ongoing work prevents defects from getting into the program.

Team Management
===============

Many of us, when managing something, feel the need to provide a lot of directions.
You should rather ensure that your teams know **what** to do and let them figure
out **how**.

Purpose, Autonomy, and Mastery are drivers of both employee satisfaction and
improved productivity. Bring concerns and problems to the team and let the
whole team create solutions in concert.

The product owner (:ref:`product-owner`) shows *what problem to solve*
and the team decides *how to solve it*. As the team becomes more self-organized
they'll have more autonomy, bring more creativity to the problem and become more
productive. Team members do their own testing, their own documentation, everything.
The closer we get to this ideal, the less coordination we need.

.. hint:: We need self-organized teams with a common understanding of its purpose.

A manager should delegate his team (delegation). The Project Owner selects the
core team members ans the team as a whole selects the rest. Try to push detailed
organizational decisions downward as far as possible.

The most valuable thing you can do to speed up development ist to build skill in
the development team. The fastest teams move smoothly and gracefully.
