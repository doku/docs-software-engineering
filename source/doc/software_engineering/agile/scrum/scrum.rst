.. _ref-scrum-introduction:

=====
Scrum
=====

Scrum ist ein agiler Ansatz für die Entwicklung von Produkten in einem komplexen
Umfeld.

Scrum ist ein agiler Prozess, der es erlaubt auf die Auslieferung der wichtigsten
Geschäfts-Anforderungen innerhalb kürzester Zeit zu fokussieren.
Scrum gestattet es schnell und in regelmäßigen Abschnitten (von 2 Wochen bis einem Monat)
tatsächlich lauffähige Software zu inspizieren.

Das Business setzt die Priorität. Selbst-organisierende Entwicklungsteams legen das
beste Vorgehen zur Auslieferung der höchstprioren Features fest. Denn ca. die Hälfte
der Funktionalität wird fast nie verwendet. Nur ca 20% der Funktionalität wird oft
verwendet.

Alle zwei Wochen bis zu einem Monat kann jeder lauffähige Software sehen und
entscheiden, diese so auszuliefern oder in einem weiteren Abschnitt zu ergänzen.

Im Scrum ist die Hierarchie flach. Scrum-Master, Product-Owner und Entwicklerteams
sind auf einer Hierarchieebene.

Scrum Charakteristika
=====================
- Fokussierung auf Ergebnisse und verbindliche Ziele
- selbst-organisierende Teams
- Produkt schreitet in Abschnitten von monatlichen **Sprints** fort

  - Beherrschen von Komplexität durch kurze Iterationsintervalle
  - Möglichkeit schnell auf veränderte Anforderungen zu reagieren

- Anforderungen sind als Listeneinträge im **Produkt Backlog** festgehalten
- Schaffen von Vertrauen durch maximale Transparenz

- Keine spezifische Entwicklungsmethoden sind vorgeschrieben, stattdessen:

    - Generative Regeln um ein agiles Umfeld für die Auslieferung von Produkten zu
      schaffen.
    - Einer der agilen Prozesse

Agiles Manifest als Wertesystem
-------------------------------

.. figure:: agilesmanifest.png
	 :alt: agilesmanifest

Scrum Ablauf
============

In Scrum gibt es klare Rollen, effiziente Events und Artefakte.

.. figure:: scrum_ablauf.png
	 :alt: scrum_ablauf


Rollen
======

.. _product-owner:

Product Owner (PO)
------------------
Der Produkt Owner hat die inhaltliche Verantwortung für das Produkt
repräsentiert den Kunden gegenüber dem Team. Er stellt eine Art *Mini-CEO* für
das Projekt dar und ist auch für den wirtschaftlichen Erfolg des Projekts
verantwortlich.

Er definiert die Produkt Features und bestimmt Auslieferungsdatum und Inhalt fest.
Er priorisiert die Features und nimmt die fertigen Features ab.

Das Entwicklungsteam und der PO müssen eng zusammenarbeiten. Der PO sollte genau
eine Person sein und muss Entscheidungen treffen dürfen. Er ist nicht der
Teamchef.

Entwicklerteams
---------------
Die Entwicklerteams entwickeln die Inkremente und schätzen das Product Backlog.

Typischerweise sind 5-9 Personen im Team. Die Teams sind Funktionsübergreifend.
Das Teams sollte also verschiedene Speziallisten beinhalten und alle Mitglieder
sollten trotzdem eine grobe Ahnung der verschiedenen Bereiche haben.
Die Teams organisieren sich selbst.

Die Teams verantworten die innere Qualität des Produkts. (:ref:`ref-taxonomie-software-qualitaet`)

Idealerweise arbeitete das Team an einem Standort. Nach dem **Musketier-Prinzip**
(Einer für alle, und alle für einen) arbeitet das Prinzip.

Durch die Agilität werden die Teammitglieder gezwungen so lange und intensiv
miteinander zu arbeiten wir nie zuvor (Deutlich geringere Fluktuation als beim
Wasserfall). Für eine gute Arbeitsatmosphäre sind **shared Values** und Working agreements
(Pünktlichkeit etc.) sind daher von großer Bedeutung.

Beim Umstieg auf Scrum ist es besonders schwierig, den Teams Selbstständigkeit
beizubringen.

Scrum Master
------------
Der Scrum Master repräsentiert das Management gegenüber dem Projekt. Er ist verantwortlich
für die Einhaltung von Scrum-Werten und Techniken.

Als Scrum Master ist man Mentor, Trainer, Problemlöser, Unterstützer und Konfliktnavigator.

Teams sind oft nicht gewöhnt sich selbst zu organisieren. Dieser Übergang
vom Wasserfall benötigt viel

Zoo: Wenn ich dem Tiger sein Käfig wegnehme, und erwarte dass er nun sein
Essen selber jagt, wird er trotzdem jeden Tag zur Futterschüssel kommen.

Events
======

Sprint
------
In einer festen Zeitspanne (1-4 Wochen) werden die Anforderungen umgesetzt.

Ein Sprint stellt ein mini-Projekt dar, durch welche inkrementell das Produkt
entwickelt und erweitert wird. Während dem Sprint gibt es keine Änderungen im Sprint
Backlog.

Das Team wählt die Einheiten aus dem Product Backlog aus, zu deren
Implementierung es sich verpflichten kann. Das Ziel jedes Sprints ist es, am eine
ein **Potentially shippable product** zu erreichen.

Sprint Planning
---------------
Vor Beginn des Sprints soll im Meeting das Sprintziel definiert werden.
Der Product Owner bestimmt *Was* getan wird, die Teams bestimmen *wieviel*
getan wird. Das Entwicklerteam erstellt den Umsetzungsplan.

.. figure:: scrummeetings.png
	 :alt: scrummeetings

Das Ergebnis des Planning Meetings ist das Sprint Backlog.

Daily Scrum
-----------
Das Daily Scrum ist ein tägliches 15-minütiges Standup-Meeting, das für die
Synchronisation und Planung der nächsten 24h. PO und Scrum Master können optional
beim Daily Scrum auch dabei sein.

Jeder beantwortet 3 Fragen:

- Was hast du gestern erreicht?
- Was wirst du heute tun?
- Welche Hindernisse sind in deinem Weg?

Das Daily Scrum sollte immer zur selben Zeit am selben Ort stattfinden (Routine).

**WIP Limit**: Anzahl der begonnen Userstorys limitieren. Dadurch wird das Daily
fokussiert und verhindert viele halbfertige Features.

Ein **Burndown-Chart** hilft dabei den Fortschritt zu tracken.

Review Meeting
--------------
Am Ende des Sprints findet das Sprint Review statt. Der Product Owner, Dev. Team
Scrum Master und Stakeholders sind anwesend. Das Team stellt das Ergebnis des Sprints
live vor und erhält wichtiges Feedback von den Stakeholders.

Im Sprint Review Meeting wird das Feature (das Sprint Ergebnis) mit dem Kunden
diskutiert und Feedback gegeben.

Sprint Retrospective
--------------------
Die Sprint Retrospective findet nach dem Sprint Review statt. Das Ziel ist die
kontinuierliche Prozessverbesserung. Die geplanten Maßnahmen müssen schriftlich
festgehalten werden. Dadurch ist ein **Inspect & Adapt** für den Prozess möglich.
Zudem hat man die Chance die :ref:`ref-definition-of-done` zu überarbeiten.

Improvements:
  When trying an improvement: define in advance what better would look like in
  measurable term. Come back later and see if the tried improvement actually
  made things better.

Artefakte
=========

Product Backlog
---------------
Funktionale und nicht funktionale Anforderungen an das Produkt. Es stellt die
**Single Source of Truth** dar.

DEEP-Kriterium (Detailed appropriately, Emerged, Estimated, Prioritized)

Für ein Projekt gibt es nur ein Backlog, auch bei mehreren Teams. In Refinement-Meetings
wird regelmäßig das Backlog überarbeitet und aktualisiert. In den Refinement-Meetings
werden schon die User-Stories für die nächsten Sprints geplant. So können bei
Problemen noch neue **Spikes** (Nachforschung zur genaueren Einschätzung einer Userstory)
in den Sprint eingefügt werden.

.. figure:: backlog_refinement.png
	 :alt: backlog_refinement

.. figure:: feature_hirarchie.png
	 :alt: feature_hirarchie

Zu Beginn gibt es grobe Backlog Items, die erst mit der Zeit und je nach Priorität
detailliert werden. Da wie am Anfang das geringste Wissen über das System haben
wird diese Technik es **progressive Refinements** verwendet. Dies stellt sicher,
das immer nur die Anforderungen genau beschrieben werden, die wir tatsächlich
umsetzen werden.

.. note::
  **Just in time requirements engineering** keeps your production lean and reduces waste.

.. figure:: scrum_anforderungsanalyse.png
	 :alt: scrum_anforderungsanalyse

Sprint Backlog
--------------
Enthält die Anforderungen und den Plan (Tasks) für den Sprint. Es beinhaltet
kleine Tasks von maximal 1 Tag Arbeitsaufwand. Diese werden während dem Sprint
immer präziser. Das Sprint Backlog zeigt, wo das Team in der Entwicklung steht.
Dieser Fortschritt lässt sich auf einem **Sprint Burndown Chart** verdeutlichen.
Das Sprint Backlog ist ein Arbeitsmittel für die Entwickler.

Produkt Inkrement
-----------------
Ein Produkt Inkrement sollte immer done, also Potentiell auslieferbar sein.

.. _ref-definition-of-done:

Definition of Done
~~~~~~~~~~~~~~~~~~
Das Dev. Team definiert mit dem PO die **Definition of Done**

In der Praxis sind je nach Person verschiedene Ansichten von einem *fertigen*
Feature, die Definition of Done räumt Missverständnisse aus. Nichtfunktionale
Anforderungen können direkt in die Definition of Done einfließen. Dadurch wird
sicher gestellt, dass sie in jedem Sprint überprüft werden.

.. figure:: definition_of_done_example.png
	 :alt: definition_of_done_example

Durch Anpassung kann die Qualität schrittweise erhöht werden.

Begriffe
========

.. _user-story:

User Story
----------
Eine User Story beschreibt eine Funktionalität aus Sicht des Anwenders. Sie soll
die Konversation anregen.

.. hint:: Als ``<Benutzerrolle>`` will ich ``<Ziel>``, sodass ``<Grund für das Ziel>``

.. figure:: userstory_example.png
	 :alt: userstory_example

Bei User Stories ist besonders wichtig, das das **wozu** klar ist. Sie können zudem
gleich die Akzeptanzkriterien für den Acceptance Test (:ref:`ref-atdd`) definieren.

.. note::
  Three C's define a Userstory:

  - Card
  - Conversation with Stakeholders
  - Confirmation or Condition of Satisfaction

Schätzen des Umfangs mit geschieht mit Storypoints. Dabei werden Schätzwerte in
Anlehnung an die Fibonacci-Zahlen verdeckt im Team auf Karten geschrieben und dann
gleichzeitig besprochen und begründet. Es wird dabei die relative Größe/Komplexität
statt die Dauer geschätzt.
Absolute Schätzungen fallen relativ schwer (wie viel Seiten hat das Buch?),
hingegen sind relative Schätzungen deutlich leichter (Welches Buch ist dicker?).

Userstories should follow the **INVEST** Acronym. They should be:

- Independent: could be tackled in any sprint
- Negotiable
- Valuable
- Estimatable
- Sized Appropriately
- Testable: with defined acceptance criteria

Scrum in der Praxis
===================
