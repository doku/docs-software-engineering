==========================
Agile Software Entwicklung
==========================

.. note::
	Agile is not what you do, Agility is how you do it.

.. figure:: stacey_graph.png
	 :alt: stacey_graph

Jedes Projekt hat 3 Probleme

- Ich kenne nie alle Anforderungen am Anfang
- Anforderungen werden sich ändern
- Es gibt immer mehr zu tun als Geld und Zeit zur Verfügung ist

.. figure:: agilitaetsdreieck.png
	 :alt: agilitaetsdreieck

.. hint::
  "Agile is like teen sex, Everybody wants to do it, many say they are doing it,
  only some are actually are and very few are doing it right"

When starting to implement any agile strategy in your organization the very first
step is understand your goals in measurable terms. There are acceptance criteria
for organizational transformation.

.. attention::
	The incremental approach doesn't just apply to product development it also
	applies to process improvement.

.. hint::
	Lean development is not about cutting cost. Lean development is about investing
	to reduce waste. You make it much cheaper to evolve your software.

.. figure:: path_to_agile.png
	 :alt: path_to_agile

The key thing with most agile artifacts is the shared understanding of the whole
team that created the artifact, the artifact itself is not the important thing.

.. figure:: impact_map.png
	 :alt: impact_map

Hypothesis-driven delivery
==========================

When building a feature

You need to define what signal from your user indicates that the feature your build
actually achieved the desired outcome.

.. figure:: hypothesis_driven_delivery.png
	 :alt: hypothesis_driven_delivery

	 Jeff Gothelf "Better product definition with Lean UX and Design"

Agile Frameworks
================

Agile Frameworks such as Scrum (:ref:`ref-scrum-introduction`) and Extreme
Programming (:ref:`ref-extreme-programming-introduction`) give guidance and tips
for agile software development.

Try to have to little framework, not to much. As the Agile Manifesto says, we
prefer "individuals and interactions over processes and tools". Keep the process
light and control the framework, don't let the framework control you. Modify your
framekork to make your projects more effective. However don't change the framework
just because it asks you to do difficult things. The ideas in your framework, are
there to challenge you to improve.

Agilität
========

Agile Development is like team sport: There may be plans, but what happens in the
game is always different, and success lies in the ability of the team members to
interact in the moment. Paradoxically, our ability to seize the moment comes from
the thinking we do before the action start.

Agile teams work daily with their business-side associates. They deliver working
software frequently, every couple of weeks. They measure themselves with working
software, work in a sustainable fashion, and pay constant attention for technical
excellence and good design

.. glossary::

  Agilität
    Die Fähigkeit, schnell und bewusst auf Veränderungen zu reagieren und dabei das
    Risiko zu kontrollieren.

Der Prozess und das Produkt muss immer überprüft und angepasst werden. Dazu ist Transparenz
und Vertrauen nötig. Im agilen Ansatz wird iterativ nach dem Ansatz **Just in Time**
(*at the last responsible moment*)
entwickelt. Nach jedem Iterationsschritt muss ein **Potentially shippable Product**
erstellt werden.

Planbasiert versucht durch gute Planung am Anfang des Projektes erfolgreich zu sein.

Agil geht davon aus, das es nicht möglich ist im ersten Wurf nicht das richtige Produkt
zu entwickeln

Release Planung
===============

Ein guter Releaseplan erfordert ein geordnetes und geschätztes Backlog.

Zudem erfordert er eine bekannte Velocity. Velocity beschreibt mit welcher Geschwindigkeit
das Team in den Sprints abreiten kann. Sie wird in durchschnittlichen Story-Points pro
Sprint geplant.

.. figure:: sprint_velocity.png
	 :alt: sprint_velocity

Beim **Date Target Planning** wird das Produkt an einem bestimmten Datum released.
Beim **Feature Target Planning** wird das Produkt released, wenn bestimmte Features
fertig sind.
