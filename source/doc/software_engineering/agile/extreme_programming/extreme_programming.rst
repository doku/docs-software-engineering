.. _ref-extreme-programming-introduction:

========================
Extreme Programming (XP)
========================

.. figure:: extreme_proramming_cycle.png
	 :alt: extreme_proramming_cycle

Hauptcharakteristiken

- Test Driven Development
- Pair Programming
- Refactoring
- Collective Ownership
- Continuous Integration


XP Planning Game
================

.. figure:: xp_planning_game_skizze.png
	 :alt: xp_planning_game_skizze
