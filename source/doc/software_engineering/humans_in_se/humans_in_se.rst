================================
Menschen im Software Engineering
================================

Der Mensch steht im Mittelpunkt des Software Engineering, da ihre Motivation,
Fähigkeit, Rolle und Wertvorstellung maßgeblich den Erfolg und Produktivität
in Projekten bestimmt.

Software-Hersteller und Kunden sind meist **juristische Personen**.

Personen die den Kunden repräsentieren sind **Klienten**

Der Kunde hat Wünsche die nur schwer bestimmbar sind. Geforderte Spezifikationen
müssen diskutiert werden und bei Schwierigkeiten müssen Kompromisse gefunden werden.
(Wird die Anforderung wirklich benötigt, obwohl sie schwierig/teuer ist?)

Rollen und Verantwortlichkeiten
--------------------------------
Deutlich effektiver wenn man an nur einen Projekt arbeitet, da man dann nicht immer
zwischen dem Kontext wechseln muss.

.. glossary::

  Entwickler und seine Spezialisierung
    Spezifizieren, Entwerfen, Testen, auch Prüfen und Verwalten

  Analytiker
    Erhebung der Anforderungen (Analyse)

  Programmierer
    Feinentwurf, Codierung, Test

  Software-Architekt
    Der Architektur hat im Software-Projekt eine ähnliche Rolle wie der Architekt
    auf dem Bau, er entwirft nicht nur die Struktur, sondern leitet und überwacht
    auch die Realisierung. Er ist dabei aber kein Künstler und befolgt strikte
    Ingenieursprinzipien.

  Wartungsingenieur
    führt Softwareanpassungen an neue Technologie und Nutzeranforderungen durch

  Projektleiter
    leitet das Projekt und ist Übermittler zwischen Management der Herstellerfirma
    und den Entwicklern.

    Es gibt zwei verschiedene Interpretationen der Vorgesetzten-Rolle: die
    :term:`Stabsfunktion` und :term:`Linienfunktion`.

  Stabsfunktion
    Repräsentant der Geschäftsleitung gegenüber den Mitarbeitern

  Linienfunktion
    Vertreter der Gruppe gegenüber dem Management

  Kunden
    Unsichtbare Klienten: Gesetzgeber und Kontrollgremien
