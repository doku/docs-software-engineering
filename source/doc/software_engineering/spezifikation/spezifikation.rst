.. _ref-specification:

=============
Spezifikation
=============

Die Anforderungsspezifikation dokumentiert im wesentlichen die Anforderungen an
eine Software und ihre Schnittstellen. Alle Anforderungen müssen präzise, vollständig
und **überprüfbar** sein.

In der Spezifikation wird festgelegt was ein System leisten soll.

.. glossary::

  specification
    A document that specifies, in a complete, precise, verifiable manner the
    requirements, design, behavior or other characteristics of a system or
    component, and often the procedures for determining whether the provisions
    have been satisfied.

  software requirements specification (SRS)
    Documentation of the essential requirements (functions, performance, design
    constrains, and attributes) of the software and its external interfaces.

Merkmale der Anforderungsspezifikation
======================================

- **Adäquat**: beschreibt das, was der Kunde will und braucht
- **Vollständig**: beschreibt alles, was der Kunde will und braucht
- **Widerspruchsfrei**: falls nicht, kann die Spezifikation nicht realisiert werden
- **Verständlich für alle Beteiligten**: auch für Nicht-Techniker und Nicht-Domain-Experten
  lesbar
- **Eindeutig**: vermeidet Fehler durch Fehlinterpretation
- **Prüfbar**: man kann feststellen, ob das System die Anforderungen tatsächlich
  erfüllt.
- **Risikogerecht**: Bei hohem Umgang gibt es geringeres Risiko für Fehler?

Nur mit prüfbaren Anforderungen können wir am Ende wirklich beantworten, ob das
System die Anforderungen erfüllt.

Spezifikationsmethoden
======================

- Begriffsmodell
- Anwendungsfälle / Use case
- Datenmodell
- Strukturmodell
- Formales Modell
- Ablaufmodell
- Zustandsmodell
- Verteilungsmodell
- Textuell
- Strukturierter Text

Am häufigsten werden Texte zur Spezifikation verwendet. In vielen Fällen können
diese aber durch andere Methoden ergänzt werden, um das Verständnis zu erleichtern
und präziser zu spezifizieren.

Funktionsspezifikation
======================
Die Funktion einer Software ist die in der Zeit ablaufenden Transformation von
Eingabedaten in Ausgabedaten.

Die Funktionsbeschreibung sagt also aus, wann welche Daten benötigt werden, wann
welche Daten erzeugt werden und welche Mittel in welcher Menge dazu beansprucht
werden.

Use Case
========

.. glossary::

  use case
    A sequence of interactions between an actor (or actors) and a system triggered
    by a specific actor which produces a result for the actor.

Ein use case ist zielorientiert (Akteur möchte Ziel erreichen) und beschreibt alle
Interaktionen zwischen den System und den Beteiligten. Das System wird vom
Hauptakteur durch ein spezielles Ereignis (Trigger) angestoßen. Der use case endet, wenn
das angestrebte Ziel erreicht ist oder wenn klar ist, dass es nicht erreicht werden
kann.

Anwendungsfälle beschreiben also aus der  **Außensicht**, was ein System leisten soll
und welche Interaktionen dazu notwendig sind. Jeder Anwendungsfall muss das
Systemverhalten für eine konkrete Situation vollständig spezifizieren (mit
Ausnahmefällen).

Es bietet sich an zwischen **Hauptfunktionen** und **Basisfunktionen** zu unterscheiden.
Hauptfunktionen beschreiben die geforderte fachliche Funktionalität des Systems.
Basisfunktionen werden von den Hauptfunktionen verwendet und liefern dort einen
Beitrag zur Funktionalität.

.. figure:: usecase_diagram.png
  :alt: usecase_diagram

  Anwendungsfalldiagramm

  Die Anwendungsfälle werden als Pakete gruppiert.
  ``Authentifizieren`` beschreibt eine Basisfunktion

Erstellen von Anwendungsfällen
------------------------------

- Bestimme alle Akteure und alle Ein- und Ausgabedaten
- Lege die Systemgrenzen fest: welche Komponenten und Funktionen gehören zum System?
- Identifiziere und formuliere die Hauptfunktionen, sodass die Funktionalität vollständig
  abgedeckt ist.
- Strukturiere die Anwendungsfälle und Akteure, identifiziere die Beziehungen
- Beschreibe den Normalablauf zum angestrebten Ziel des Hauptakteurs im Anwendungsfall
- Beschreibe alle Sonderfälle und Alternativabläufe die auftreten können.
  Wie und warum unterscheiden sich diese vom Normalfall?
- Extrahiere gleiche Interaktionssequenzen als Basisfunktionen. (Generalisierung
  und Wiederverwendung)
- Prüfe die Anwendungsfälle mit dem Klienten in Reviews und korrigiere diese gegebenenfalls
  bevor sie für die Spezifikation freigegeben werden.

Spezifikation von Qualitätsseigenschaften
=========================================

Nicht-funktionale Anfordenungnen sind oft nach der Spezifikation vage,
nicht quantifiziert oder testbar. Vor allem fehlender Kontext ist ein Problem.
