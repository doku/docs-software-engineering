===================================
Geschichte des Software Engineering
===================================

Seit den 60er Jahren verdoppelt sich die Integrationsdichte alle 18 Monate,
wächst also exponentiell.

Programmierung massiv paralleler Probleme ist ein weitgehend ungelöstes
Problem.

In den 50er Jahren waren die Computer das **schwächste Glied der Kette** und die
Leistung der Programmierer wurde eingesetzt, um seine Unzulänglichkeiten zu kompensieren.
Hardware musste effizient genutzt werden.

Rechner wurden kleiner, billiger, schneller und zuverlässiger.

Die Software wurde in dieser Zeit weder billiger noch zuverlässiger.
Die Software war von nun an die Schwachstelle des Systems. Dies führte zur
*Software-Kriese* (software crisis) in den 70er Jahren, da die Hardware die Software überholte.

Die Kunden wollten die neuen Hardwaremöglichkeiten ausnutzen, die Softwareentwicklung
kam aber nicht hinterher die geforderten Features zu implementieren.

Wenn eine Ware relativ zur anderen knapp und teuer wird, substituieren die Menschen die
teure Ware durch billigere. Wir verwenden daher **schlechte Software auf gigantischen Rechnern**
statt guter teurer Software auf kleinen, billigen Rechnern

Das Verhältnis zwischen Hardware- und Softwareausgaben blieb konstant bei 6:4.

Entstehung des Software Engineerings
====================================

.. hint::
  The whole trouble comes from the fact that there is so much tinkering with software.
  It is not made in a clean fabrication process which it should be. What we need is
  **software engineering**. (Bauer, 1993)

Software Engineering ist die Herstellung und Anwendung einer Software, wobei mehrere
Personen beteiligt sind oder mehrere Versionen entstehen. (Parnas)

Software engineering ist jede Aktivität, bei der es um die Erstellung oder Veränderung
von Software geht, soweit mit der Software Ziele verfolgt werden, die über die Software
selbst hinausgehen.

Software Engineers
==================

Every 5 years the number of programmers doubles. Which has an interesting implication:
every second programmer has less than 5 years of experience. This exponential curve
guarantees perpetual inexperience.
