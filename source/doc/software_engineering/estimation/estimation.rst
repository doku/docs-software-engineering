====================
Effective Estimation
====================

Usally when we are doing something, we are doing it for the first time, so 
we can't possibly know  how long it is going to take.

When scheduling there are two time sections involved everytime we write code:

#. The time it takes to write the code.
#. The time it takes to make the code work right.

The time to make the code work right is usally the biggest part and 
very hard to estimate.

Don't use time estimates
========================
Estimates can be widely wrong for very good reasons.

.. hint::
  Everything in software will take you three times longer than you think
  even if you know this and take it into account.

Estimates should have three characteristics:

#. An estimate needs to be honest. (You have to be able to communicate bad news
   when they are bad.) Regardless of what your estimate is, if it is honest you
   will bread trust.
#. An estimate must be accurate.
#. An estimate must be precise.

An estimate is never a date it is always a date range.

- Best case estimation: 95% chance of missing
- Worst case estimation: 5% chance of missing
- Nominal case estimation: 50% chance of missing

When asked for a estimate always give best, worst and nominal case and make sure
everyone knows that the worst case could happen.

.. math::

  & standardDeviation = \frac{W-B}{6} \\
  & mean = \frac{B+W+4N}{6} \\
  & Project mean = \sum(mean) \\
  & Project standardDeviation = \sqrt{\sum(standardDeviation^2)}

Estimates are always wrong. Specify the **Return of Investment** (ROI) of each
task and let the developers pick the tasks with the best relation between
**educated guess** and **return of value**.

For each task the project management should specify the importance/value/priority on a defined
scale and say what would be the longest acceptable duration for someone to work on
this task.

Expertenschätzung
=================
Fachleute nutzen ihre Erfahrung, um den Aufwand zu schätzen.

Algorithmische Schätzung
========================
Kosten werden aus Größen berechnet, die frühzeitig bekannt sind oder leichter
und genauer als der Aufwand geschätzt werden können.

Das algorithmische Kostenschätzverfahren COCOMO basiert auf dem Codeumfang.

Top-down
========
Aus globalen Größen (Laufzeit, Preis oder Aufwand) wird abgeleitet, welche Größen
bezüglich Umfang usw. zu erwarten sind. Dies ist sinnvoll, wenn die globalen
Größen früh feststehen.

Bottom up
=========
Aus atomaren Größen (Anzahl der Codezeilen, Ein-/Ausgaben, Usecases) wird der
Aufwand abgeleitet. Der Integrationsaufwand muss hier zusätzlich eingerechnet
werden.

With this approach it is very likely that you miss a lot of tasks resulting in an
inaccurate estimation.

.. hint::
  The only way to estimate accurately using a work-breakdown structure is to implement the
  project.

There is an extremely high cost at refining the estimate.

Als Anhaltswert für Lines-of-code bezogene Schätzung: 1-2 DLOC/h (Delivered Line
of code pro Aufwandsstunde im Projekt insgesamt)

Aktivitäten-basierte Schätzung
==============================
Als Basis werden die durchzuführenden Aktivitäten gesammelt und auf Unteraktivitäten
heruntergebrochen. Die Schätzung der Teilaktivitäten wird dann zur Gesamtschätzung
aggregiert.

Produkt-basierte Schätzung
==========================
Die zu erstellenden Produkte werden als Teilprodukt heruntergebrochen. Die
Schätzungen der Teilprodukte werden zur Gesamtschätzung aggregiert.



Wideband delphi
