===============
Projekt Planung
===============


Während der Projektdurchführung muss kontinuierlich geprüft werden, inwieweit der
Stand des Projekts (IST-Wert) mit der Planung (SOLL-Wert) übereinstimmt.

Die Planung deckt folgende Aspekte ab:

#. Planung der Aufgaben (Was muss getan werden um Projektziele zu erreichen?, Arbeitspakete)
#. Planung der Termine (Wann müssen welche Resultate verfügbar sein?)
#. Planung der Ressourcen (Mitarbeiter, Budget)
