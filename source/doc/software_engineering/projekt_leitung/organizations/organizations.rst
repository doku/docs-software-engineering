=============
Organizations
=============

Organizations of humans are complex adaptive systems.
In a complex adaptive system

#. Nobody has perfect information
#. You can never predict the consequences of your systems. There will always be
   side effects.

Therefore in complex adaptive systems **failure** is inevitable.
You should not ask how to prevent failure, but how to recover from failure as fast
as possible and how to detect failure.

.. figure:: westrum_typology.png
	 :alt: westrum_typology


IT Performance
==============

IT Performace can be measured by throughput and stability:

- lead time for changes
- release frequency

- time to restore service
- change fail rate

High performance teams achieve both high throughput and high stability.
The same technology that allows you to move fast also us to detect and respond
to failure as fast as possible.

.. hint::
  Stability and throughput is not a tradeoff.

Teams
=====

Who is on a team matters less than how the team members interact, structure their
work, and view their contributions

.. figure:: team_dynamics.png
	 :alt: team_dynamics

https://rework.withgoogle.com/blog/five-keys-to-a-successful-google-team/
