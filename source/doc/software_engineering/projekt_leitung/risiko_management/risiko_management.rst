=================
Risiko Management
=================

.. glossary::

  Risiko
    Ein Problem, das noch nicht eingetreten ist, aber wichtige Projektziele oder
    Projektergebnisse gefährdet, falls es eintritt. Ob es eintreten wird, kann
    nicht sicher vorausgesagt werden.

Risikomanagement umfasst:

#. Identifikation von Risiken
#. Analyse und Bewertung der Risiken
#. Planung von Gegenmaßnahmen
#. Verfolgen der Risiken und der gewählten Gegenmaßnahmen

Risikobewertung
===============

Jedes Risiko hat eine Eintrittswahrscheinlichkeit :math:`p` und im Schadensfall
entstehende Kosten :math:`K`.

.. math:: Risikowert = p * K

.. figure:: risiko_bewertung.png
	 :alt: risiko_bewertung

.. figure:: risiko_bewertung_2.png
	 :alt: risiko_bewertung_2

.. figure:: risiko_klassifizierung.png
	 :alt: risiko_klassifizierung

Präventivmaßnahmen sollen Eintrittswahrscheinlichkeit oder den erwarteten Schaden
reduzieren.

Notfallmaßnahmen werden reaktiv ergriffen. Auch diese können geplant sein.

Da sich die Risiken während der Projektdurchführung verändern (vorhandene Risiken
schwächen sich ab oder verstärken sich, neue Risiken kommen hinzu), muss die
Risikosituation eines Projekts während der gesamten Laufzeit immer wieder
bewertet werden. 
