.. _ref-software-maintenance:

=======
Wartung
=======

Software-Systeme sind eingebunden in Technologie und angepasst auf die
Anforderungen des Kundens. Doch sowohl die verfügbare Technologie, als auch die
Anforderungen ändern sich. Wenn Software ihren Nutzen nicht verlieren soll, muss
sie sich mit entwickeln und benötigt **Software-Wartung**.

Risiken und Probleme bei der Wartung
====================================

Changing priorities
  Wartungstätigkeiten werden oft unterbrochen - andere Tätigkeiten sind wichtiger
  oder dringender - und müssen anschließend wieder aufgenommen werden.

Inadequate testing methods
  Der Regressionstest nach der Wartung bereitet Probleme, weil spezielles Know-how
  fehlt und weil die Testfälle und Testdaten nicht ausreichen.

Performance measurement difficulties
  Oft ist nicht definiert, wie Wartung durchzuführen ist. Wartung ist nicht
  explizit geplant, die benötigten Aufwände werden weder geschätzt noch erhoben.

System documentation is incomplete
  Oft ist die vorhandene Dokumentation unvollständig und nicht mehr aktuell.
  Rückgriffe auf das Wissen von Personen: "walking documentation"

Adapting to a rapidly changing business environment
  Der Anwendungskontext und die Anforderungen an viele Systeme ändern sich so schnell,
  dass kaum mehr Stabilität erreicht werden kann.

Wartungstätigkeiten müssen meist unter Zeitdruck stattfinden
  Qualität der Architektur, der Dokumentation und des Codes sinkt.

Durch Wartung entstehen neue Fehler
  Die neuen Fehler werden zu einem beträchtlichen Teil nicht entdeckt, sondern
  mit der neuen Version ausgeliefert.

Wartung ist eine ungeliebte Tätigkeit
  Wartung ist eine analytische, die Entwicklung ist eine konstruktive Tätigkeit.

.. glossary::

  operation and maintenance phase
    The period of time in the software life cycle during which a software
    product is employed in its operational environment, monitored for satisfactory
    performance, and modifies as necessary to correct problems or to respond to
    changing requirements. (IEEE 610.12)

  maintenance
    The process of modifying a software system or component after delivery to
    correct faults, improve performance or other attributes or adapt to a
    changing environment. (IEEE 610.12)

  legacy system
    A large software system that we don't know how to cope with but that is vital
    to our organization.

Die vollständige Ersetzung ist teuer, zeitaufwändig und riskant. Die Ablösung eines
Systems ist Vernichtung von Kapital, welches bereits in das alte System investiert
wurde. An Stelle einer Systemrevolution bietet sich eine schrittweise Systemevolution
an.

.. note::
  Programs, like people, get old

Bei legacy Systemen sollte ein automatisiertes Buildscript erstellt werden. Mit
continuous integration und unit-tests lassen sich zudem Regressionsfehler vermeiden.

.. figure:: restructuring_software.png
	 :alt: restructuring_software

.. glossary::

  impact analysis
    impact analysis can be defined as the activity of identifying which object to
    modify to accomplish a change, that is, estimating the potential consequences of
    carrying out a change.

  Reverse Engineering
    the process of analyzing a system to identify the system's components and their
    interrelationships and create representations of the system in another form
    or at a higher level of abstraction.

  Restructuring
    the transformation from one form of representation to another at the same relative
    level of abstraction. The new representation is meant to preserve the semantic
    and external behavior of the original.

  Reengineering
    is the examination and alteration of a subjects system to reconstitute it in a
    new form and the subsequent implementation of the new form.

  Forward Engineering
    is the traditional process of moving from high-level abstractions and logical,
    implementation-independent designs to the physical implementation of a system.

  Redocumentation
    is the creation or revision of a semantically equivalent representation within
    the same relative abstraction level.

  Design recovery
    recreates design abstractions from a combination of code, existing documentation,
    personal experience, and general knowledge about problem and application domain.

.. figure:: wartungsfaelle.png
	 :alt: wartungsfaelle

.. glossary::

  Preventive Maintenance
    Anpassung an geänderte Anforderungen und Implementierung neuer Anforderungen

  Adaptive Maintenance
    Anpassung an Technologie-Änderungen

  Corrective Maintenance
    Fehler-Behebung

  Preventive Maintenance
    Vorbereitende Maßnahmen, Qualitätsverbesserung

.. figure:: maintenance_types.png
	 :alt: maintenance_types

.. _effective-maintenance:

Effective Maintenance
=====================

.. figure:: maintenance_hotspots.png
	 :alt: maintenance_hotspots

Start by cleaning up the sections of code that are modified most frequently.

http://www.adamtornhill.com/articles/software-revolution/part1/index.html
