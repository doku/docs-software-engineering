==========
Unit Tests
==========

Unit tests test a individual component in isolation. Therefor they often rely on
simulating other parts of the system using :ref:`test-doubles`.
Unit tests validate the moste atomic behavioral units of a system.

Because unit tests test pieces of code independently, we are able to localize
errors quickly. A good unit test suites run very quickly.

A test is not a unit test if:

- it talks to a database.
- it communicates across a network.
- it touches a file system.

Unit Tests should cover virtually every code-path in the system (a bare minimum
of 80%).

.. _cyclomatic-complexity:

Cyclomatic Complexity
=====================

To achieve a high test coverage complex methods need multiple tests. Ideally the
number of tests is equivalent to the **cyclomatic complexity** (DE: zyklomatische
Komplexität) of the given method.

**Cyclomatic complexity** is a source code complexity measurement

A lower **cyclomatic complexity** in a program leads to lower risks of mistakes being
made during modification and makes the code easier too unterstand.

It can be calculated using this formula::

    Cyclomatic complexity = E - N + 2*P
    where,
        E = number of edges in the flow graph.
        N = number of nodes in the flow graph.
        P = number of nodes that have exit points

Example
-------

.. code::

  IF A = 10 THEN
      IF B > C THEN
        A = B
      ELSE
        A = C
      ENDIF
  ENDIF

  Print A
  Print B
  Print C

.. figure:: flowgraphexample.png
	 :alt: flowgraphexample

In this example the control flow diagram has 7 nodes and 8 edges, hence the
cyclomatic complexity is :math:`8-7+2*1=2`
