====================
Infrastructure Tests
====================

.. _ref-destructive-test:

Destructive Tests: Chaos Monkey
===============================

 Failure will happen. By running destructive tests in production you gain
 understanding whether your technology and your team members are ready to deal
 with a real system failure.

.. hint:: Destructive Tests are just like fire training.

 Run your destructive tests as close to production as you are comfortable with
 (ideally in production).

Chaos Monkey is a service which identifies groups of systems and randomly
terminates one if the systems in a group.

https://github.com/Netflix/chaosmonkey
