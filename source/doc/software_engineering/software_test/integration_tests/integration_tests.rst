=================
Integration Tests
=================

Integration Tests are also called **Component tests**.

Integration tests test larger clusters of functionality and are able to find
bugs occurring due to the lifecycles of your data or objects not being managed
correctly.

.. figure:: integration_tests.png
	 :alt: integration_tests
