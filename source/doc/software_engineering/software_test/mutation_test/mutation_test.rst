=============
Mutation Test
=============

A high code coverage can give you a false sense of security.

In Mutation Testing you test the quality of your test suite.

You introduce mutations in your production code and run your test suite against
the production code and the same test against the mutated production code.
You expect the production code to pass the test suite and the mutated production
code to fail the test suite.

PIT is a java Mutation testing tool.

:ref:`mutation-test`
