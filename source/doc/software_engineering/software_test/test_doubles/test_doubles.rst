.. _test-doubles:

============
Test Doubles
============

.. figure:: testdouble.png

.. glossary::

  Dummy objects
    Dummy objects are passed around but are never actually used. Usually they are
    just used to fill parameter lists.

  Fake objects
    Fake objects actually have working implementations, but usually take some
    shortcuts that make them not suitable for production. A good example of this
    is the in-memory database.

  Stubs
    Stubs provide canned answers to the calls made during the test, usually not
    responding at all to anything outside what's programmed in for the test.

  Spies
    Spies are stubs that also record some information based on how they were
    called. One form of this might be an email service that records how many messages
    it was send.

  Mocks
    Mocks are preprogrammed with expectations that form a specification of the
    calls they are expected to receive. They can throw an exception if they
    receive a call they don't expect and are checked during verification to
    ensure they got all the calls they are expecting.

It is easy to misuse mocks by writing tests that are both pointless and fragile,
using them simply to assert the specific details of the workings of some code rather
that its interactions with collaborators. Such tests break if the implementation
changes.
