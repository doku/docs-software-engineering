================
Acceptance Tests
================

Acceptance tests tell the developers if a feature is done and the uses if their
desired functionality is implemented.

Ideally the customers or users write acceptance tests, since they define the
success criteria for each requirement.

Acceptance tests comes in two categories: functional test and nonfunctional tests.

.. glossary::

  happy path
    In general, for each story or requirement there is a single canonical path through
    the application in terms of actions that the user will perform.

  alternate path
    Variations of initial state and performed actions with the same desired finale
    state

  sad path
    a specific path that is supposed to cause error conditions

.. code::

  Given [a few important characteristics of the state of the system when testing begins]
  When [the user performs some set of actions]
  Then [a few important characteristics of the new state of the system]

Acceptance tests should be run in a production-like mode.

In general, we tend to limit our automated acceptance testing to complete coverage
of :term:`happy path` behaviors and only limited coverage of the most important
other parts.

Automating Acceptance Tests
===========================
Automated acceptance tests allow testers to concentrate on exploratory testing
and higher-value activities.

A good rule of thumb is to automate once you have repeated the same manual test
a couple of times, and when you are confident that you won't end up spending a lot
of time maintaining the test.

In an ideal world acceptance tests would run directly against the UI of the
application. However most testing tools couple the tests tightly to the UI, resulting
in a lot of false positive when the UI changes. A good solution is to run
acceptance tests against the public API that sits just below the UI and supplies the
data to the UI.

Every story or requirement should have at least one automated :term:`happy path`
acceptance tests.

Manual Acceptance Tests
=======================
In Manual Acceptance Tests testers use the applications standard user interface
in order to perform testing.

However, automated acceptance tests can be costly to maintain. There are many aspects
of a system that people are genuinely better at testing. Usability,
consistency of look and feel are difficult things to verify in automated
tests. Exploratory testing is also impossible to do automatically.

.. _exploratory-testing:

Exploratory Testing
-------------------
Exploratory testing is a form of manual testing in which the tester actively controls
the design of the tests as those tests are performed and uses information gained
while testing to design new and better tests.

Exploratory testing is a creative learning process that will not only discover bugs,
but also lead to the creation of new sets of automated tests, and potentially
feed into new requirements for the application.

Usability Testing
-----------------
Usability testing is done to discover how easy it is for users to accomplish their
goals with your software.

Usability testers gather metrics from users, noting how long it takes users to
finish their task, watching out for people pressing the wrong button, noting how
long it takes them to find the right text field, and getting them to record their
level of satisfaction at the end.

Beta testing programs
---------------------
You can give your application to real users using beat testing programs.
You can use canary releasing where several subtly different versions of the
application are in production simultaneously and their effectiveness is compared.
This provides an evolutionary approach to the adoption of features which is very
effective.

Nonfunctional Acceptance Tests
==============================
You should implement basic nonfunctional tests testing at least system performance
and load. This will catch errors which seriously hurt you system performance early.
If for example you mistakenly call a logging functionality and logging basically
everything in your system resulting in a memory leak your system performance test
will fail after the first introduction of the bug.

Additionally you can measure important performance metrics (like response time with
500 logged in users) and display them in a graph with their corresponding commit.
Significantly changes in performance are easily visible.
