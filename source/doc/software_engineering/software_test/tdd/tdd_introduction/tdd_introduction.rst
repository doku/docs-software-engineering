===================
Introduction to TDD
===================

The Three Laws of TDD
=====================

1. You are not allowed to write any production code unless it is to make a failing
   unit test pass.
2. You are not allowed to write any more of a unit test than is sufficient to fail;
   and compilation failures are failures.
3. You are not allowed to write any more production code than is sufficient to pass
   the one failing unit test.

.. note::
  TDD forces you to switch between writing test and production code in a 5 second
  to 1 minute interval.

Der fehlschlagende Test spezifiziert das Problem. Wird nun der Code verändert und
immer mit diesem Test getestet, kann man sicher stellen, dass das Problem (wie
durch den Test spezifiziert) auch wirklich behoben wurden.

Die Testüberdeckung sollte während der TDD Zyklen also immer 100% betragen.
Ungetesteter Code wird nicht in den Tests benötigt und kann daher weggelassen werden.

In *Test-Driven Development* (**TDD**) we write our tests before writing the code.
Instead of just using our tests to verify our work after it is done, TDD turns testing
into a *design* activity. We use the tests to clarify our ideas about *what* we want
to code. With TDD we are able to separate logic from the physical design. Writing
tests first gives us rapid feedback about the quality out our design ideas. Testable
code is often cleaner and more modular.

The cycle at the heart of TDD is: write a failing test, then write some code to get
it working, *refactor* the code to be as simple an implementation of the tested feature
as possible. **Repeat**.

.. figure:: tdd_flow.png
    :align: center
    :scale: 70%

.. attention:: TDD does require approx. 15% more time.

.. note::

  **Golden Rule of TDD:**
  Never write new functionality without a failing test.

Writing Unit tests after developing code is a waste of time. You already know that
the code works because you have tested it manually. The code is often not designed
to be testable. A test suite full of whole tells you nothing if it passes.

If you write the test first it is impossible to write a function that is hard to
test. A function is easy to test if its decoupled.

.. hint::
  If you do TDD and your test suite has been created by TDD it means that your
  system works and your system is shippable.

It is impossible to write good code on the first try, because we are so focused
on solving the problem that we leave a mess, which has to be cleaned up afterwards.

A trustable and fast test suite gives you control over your code and enables you
to refactor in a save way.

.. attention::

  TDD is a way to incrementally derive solutions to problems.

  It is a discipline that supports incremental development of algorithms.

With every new tests, the tests become more and more constraining. The tests become
more specific. To pass the tests we have to make the production code
more general.

As the tests get more specific the code gets more general.

The point of TDD is to drive out the functionality the software actually needs,
rather than what the programmer thinks it probably ought to have.

Writing the test before you write the code focuses the mind -
and the development process - on delivering only what is absolutely necessary.
In the large, this means that the system you develop does exactly what it needs
to do and no more.

.. _ref-atdd:

Acceptance test-driven development
==================================

When we are implementing a feature, we start by writing an *acceptance test*, which
exercises the functionality we want to build. These acceptance tests demonstrate that
the system does not have the feature yet and tracks our progress towards completion.

.. figure:: atdd_cycle_advanced.png

.. figure:: atddcycle.png
	 :alt: atddcycle

Acceptance tests should only exercise the system end-to-end without directly calling
its internal code. They test what a system should do and not how.

New acceptance tests describe work yet to be done. Once passing, the acceptance tests
now represent completed features and should not fail again. A failure means that there's
been a regression, that we have broken our existing code.
A system is deployable when the acceptance tests all pass.

.. hint:: Start testing with the simplest success case.

Constant testing is required, so we can add new features without breaking existing ones.

Code has to be as simple as possible, so it is easier to understand and modify. Because
developers spend far more time reading code than writing it.
With refactoring we can improve and simplify our code design, remove duplicates and ensure
that the code clearly expresses what it does. The automated test suits will protect us
against our own mistakes as we improve the code.

Developers should write and maintain the acceptance tests (in cooperation with QA staff).
This creates a feedback loop to the developers and helps them to write more
testable code. If code is well testable the maintenance of your acceptance tests
is cheaper.

Internal and External Quality
=============================
Running acceptance tests tells us about the :term:`external quality` of a system but they
don't tell us about how well the code is written.

Writing unit tests gives us a lot of feedback about the :term:`internal quality` of the code, and
running them tells us that we haven't broken any classes - but unit tests don't give
us enough confidence that the whole system works.

**Code needs to be loosely coupled and highly cohesive - in other words, well designed.**

What the test will tell us (if we are listening)
================================================

Keep knowledge local
  Some of the test smells we have identified, such as needing "magic"
  to create mocks, are to do with knowledge leaking between components.
  If wen can keep knowledge local to an object (either internal or passed in),
  then its implementation is independent of its context. We can safely
  move it wherever we like. Do this consistently and your application,
  build of pluggable components, will be easy to change.

If it's explicit, we can name it
  One reason why we don't like mocking concrete classes is that we like
  to have names for the relationships *between* objects as well as the objects
  themselves. As the legends say, if we have somethings true name, we
  can control it. If we can see it, we have better chance of finding its
  other uses and so reducing duplication.

More names mean more domain information
  When we emphasize how objects communicate, rather than what they are,
  we end up with types and roles defined more in terms of the domain than
  of the implementation. This might be because we have a greater number of smaller
  abstractions, which gets us further away from the underlying code.
  Somehow we seem to get more domain vocabulary into the code.

Pass behavior rather than data
  By applying "**Tell, Don't Ask**" consistently, we end up with a coding style
  where we tend to pass behavior (in the form of callbacks) into the
  system instead of pulling values up through the stack. More precise
  interfaces give us better information-hiding and cleaner abstractions.


We care about keeping our tests and code clean as we go, because it helps
to ensure that we understand our domain and reduces the risk of being unable
to cope when a new requirement triggers changes to the design.

**It's much easier to keep a codebase clean than to recover from a mess.**

Once a codebase starts to "rot",  the developers will be under pressure
to botch the code to get the next job done. It doesn't take many such
episodes to dissipate a team's good intentions.

Tests are Documentation
=======================
Unit Tests are the perfect kind of low level documentation. They describe exactly
how elements can be used and they can't get out of sync with the production code.
Unit tests are a stream of documentation that covers the entirety of production code
and are excellent code examples.

.. hint:: Tests should act as documentation for the code.

A comprehensive automated test suite provides the most complete and up-to-date
form of application documentation, in the form of an executable specification
not just of how the system should work, but also of how it  actually does work.

There are tools that generate documentation for your code based on your
tests. Such generated documentations give us a fresh perspective on the test names
and highlight the problems we are to close to the code to see.

TDD: Benefits
=============
If we are in the 20 second TDD loop it means that 20 seconds ago everything worked
and passed all its tests. This results in a debugging time approaches zero.

The act of writing tests first forces you to decouple things.

The goal is to get the test suite pass and when the test suite passes you deploy,
No manual tests and QA required.

If you have a suite of tests that you trust, you control the code. You can change
and refactor it without having to worry. This is the **most important** advantage
of TDD. Allowing you to iteratively improve the :term:`internal quality` of your
code while regularly running the test suite to check you didn't break anything.
If you have a suite of tests you eliminate the fear of refactoring and allow you
to clean your code.

TDD is like *double entry book keeping*, everything is said twice: once on the
test side and once on the production side.

TDD: The bad parts
==================

To prevent test coupling and the test knowing about your implementation design
not every class/public method should be  pared with a unit test, but:

.. hint::
  Every **behavior** should be paired with a well-design unit-test.

This allows you to minimize the surface area of the test suite. We have to be able
to refactor code and change its structure with out having to modify any unit tests.
