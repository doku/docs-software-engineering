===============
TDD in Practice
===============

Kick-Starting the TDD-Cycle
===========================

View feedback as a fundamental tool! You need to know as early as possible whether or
not you are moving in the right direction. The only way to be sure about your progress
is to test right from the start.

First we work out how to build, deploy and test a **walking skeleton**, then use the
infrastructure to write he acceptance tests for the first meaningful feature.

A **walking skeleton** is an implementation of the thinnest possible slice of real functionality
that we can automatically build, deploy and test end-to-end. The skeleton is as simple
as possible, so we can concentrate on the infrastructure.

The point of a walking skeleton is to help is understand the requirements well enough to
propose *and validate* a broad-brush system structure. For most projects, developing
a walking skeleton takes a surprising amount of effort.

One important task for "iteration zero" (project setup, infrastructure) is to use
the walking skeleton to test-drive the initial architecture.

End-to-end tests should start from scratch: build a deployable system, deploy it in
a production-like environment and then run the tests through the deployed system.

This makes sure our deployment process is rigged enough for real production and gets
thoroughly exercised. Whiles building the "walking skeleton", we concentrate on the structure and don't
worry to much about cleaning up the test to be beautifully expressive.

Early integration of the features frontloads the stress in a project. The most
difficult part is right at the start of developing a new feature.
All the mundane but brittle tasks, such as deployment and upgrades, will have been
automated to a point where they just work long before the first release. A "walking skeleton"
will flush out issues early in the project when there is still time, budget and goodwill
to address them.

Sustainable Test-Driven Development
===================================

Difficulty in testing might imply that we need to change our test code, but often
it's a hint that our design ideas are wrong and that we ought to change the
production code.

The qualities that make an object easy to test also make our code responsive to change.

TDD is about testing code, verifying its external visible qualities such as
functionality and performance. TDD is also about feedback on the codes internal qualities:
the coupling and cohesion of its classes, dependencies that are explicit or hidden -
the qualities that keep the code maintainable.

There are two categories of test smell to consider. One is where the test itself is not
well written -  it might be unclear or brittle.
The other is were a test is highlighting that the target code is the problem.

Unit-testing tools that let the programmer sidestep poor dependency management in the
design waste a valuable source of feedback. When the developers eventually do need
to address these design weaknesses to add a urgent feature, they will be harder to do.

.. hint:: As with dirty pots and pans, it's easier to get the grease of before it's
    baken in.

Test classes should focus on at most a few classes and should not need to create a
large fixture or perform a lots of preparation just to get the objects
into a state where the target feature can be exercised. Such tests are
hard to understand - there is just too much to remember when reading
them.

Test-driven development can be unforgiving. Poor quality tests can slow
development to a crawl, and poor internal quality of the system under
test will result in poor quality tests. By being alert to the internal
quality feedback we get from writing tests, we can nip this problem in
the bud, long before our unit tests approach 1000 lines of code, and end
up with tests we can live with. We should make an effort to write tests
that are readable and flexible. They give us more feedback about the internal
quality of the code we are testing.

**We end up with tests that help, rather than hinder, continued development.**

How to act when a bug occurs
----------------------------

1.  Add bug and description to the issue tracker
2.  create Unit test that is reproducing the bug and reference the
    issue number. This test should fail, since you haven't fixed the bug jet.
3.  Fix the bug and the test should be green.
4.  Commit fix and close the issue
