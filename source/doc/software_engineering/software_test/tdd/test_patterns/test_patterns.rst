=============
Test Patterns
=============

Logging
=======
Support Logging (errors and info)
    is a part of the user interface of the application. These messages are intended
    to be tracked by support staff, as well as perhaps the system administrator,
    to diagnose a failure or monitor the progress/health of a running system.

Diagnostic Logging (debug and trace)
    is infrastructure for programmers. These messages should not be turned on in
    production because they are intended to help the programmers understand what's
    going on inside the system they are deploying.


Support Logging should be test driven from somebody's requirements. The tests will make sure
we have thought about what each message is for and made sure it works.

Diagnostic Logging is driven by the programmers need for fine-grained tracking of
what is happening in the system.

.. hint:: Don't add Logging Messages in your methods. Uses a helper Object and call
    a method. After all your code should only do one thing and not logging and logic
    at the same time!

    Bad example:

    .. code-block:: java

          Location location = tracker.getCurrentLocation();
          for (Filter filter : filters) {
            filter.selectFor(location);
            if(logger.isInfoEnabled) {
                logger.info("Filter " + filter.getName() + ", " + filter.getDate()
                + "selected for " + location.getName());
            }
          }

    Good example

    .. code-block:: java

        Location location = tracker.getCurrentLocation();
        for (Filter filter : filters) {
          filter.selectFor(location);
          support.notifyFiltering(tracker, location, filter);
        }

    logging should not blow up your code. Make sure it does not obstruct readability.
    The Support Object could be implemented by a logger, message bus or pop-up windows.
    This detail is not relevant to the code at this level.

The idea of encapsulating support reporting sound like overdesign. But it means we
are writing code in terms of our **intent** (helping the support people) rather than
implementation (logging), so it is more expressive.

Finally the act of writing tests for each report helps us avoid the
"I don't know what to do with this exception so I'll log it and carry on" syndrom,
which leads to log bloat and production failure.

We might decide that in-line code is the wrong technique for diagnostic logging,
because it interferes with the readability of the production code that matters.
We could use some aspects instead.

..todo:: aspect-oriented programming link

.. hint:: Overbloated Logging hides the important messages, so be careful and log only useful information.


Mocking
=======
when a mocked object in a test has to many expectation it is hard to see what's
important and what is really under test. As with ``asserts``, avoid too
many expectations in a test.


Objects in Tests
================
In production code we create objects in relatively few places and all the required
values are available to hand. In tests, however, we have to provide all the constructor
arguments every time we want to create a object. The code to create all these objects
makes the tests hard to read. It also makes tests brittle, as changes to the constructor
arguments or the structure of the objects will break many tests.

Object Mother Pattern
---------------------
The **objects mother pattern** is one attempt to avoid this problem. An object
mother is a class that contains a number of factory methods that create objects
for use in tests.

.. code-block:: java

  Order order = ExampleOrders.newCheesePizzaOrder();

An object mother makes tests more readable by packing up the code that creates new
object structures and giving it a name. It also helps with maintenance since its
features can be reused between tests. But this pattern does not cope well with
variations in the test data - every minor difference requires a new factory method.

Builder Pattern
---------------
For a class that requires complex setup, we create a *test data builder* that has a field
for each constructor parameter, initialized to a safe value. The builder has
"chainable" public methods for overwriting the default values and by  convention a
``build()`` method that is called last to create a new instance of the target object from
the field values. You can add a static factory method for the builder itself so that it
is clearer in the test what is being build.

.. code-block:: java
  :linenos:

  public class OrderBuilder {
    private Customer customer = new CustomerBuilder().build();
    private List<OrderLine> lines = new  ArrayList<OrderLine>();
    private BigDecimal discountRate = BigDecimal.ZERO;

    public static OrderBuilder anOrder() {
      return new OrderBuilder;
    }

    public OrderBuilder withCustomer(Customer customer) {
      this.customer = customer;
      return this;
    }

    public OrderBuilder withOrderLines(OrderLines lines) {
      this.lines = lines;
      return this;
    }

    public OrderBuilder withDiscount(BigDecimal discountRate) {
      this.discountRate = discountRate;
      return this;
    }

    public Order build(){
      Order order = new Order(customer);
      for(OrderLine line : Lines) {
        order.addLine(line);
        order.setDiscountRate(discountRate);
      }
      return order;
    }
  }


If a test just needs an Order object, you can create it in a single line:

.. code-block:: java

  Order order = new OrderBuilder().build();

If a test needs particular values within an object you can specify those values that
are relevant. This makes the test more expressive because it only includes the  values
that are relevant to the expected result.

**Test data Builders** help keep tests expressive and resilient to change.

- They wrap most of the syntax noise when creating a new object.
- They make the default case  very simple and the special cases not much more complicated
- They protect the tests against change in the structure of its objects.
- We can write test code that's easier to read and spot errors, because each builder
    method identifies the purpose of its parameter.

If we need to create two very similar objects, we can initialize a single builder
with the common state and then for each object to be build, define the differing
value and call its ``build()`` method.

.. code-block:: java

  OrderBuilder hatAndeCape = new OrderBuilder().withLine("Deerstarker Hat", 1).withLine("Tweed Cape", 1);

  Order orderWithSmallDiscount = hatAndeCape.withDiscount(0.10).build();
  Order orderWithLargeDiscount = hatAndeCape.withDiscount(0.25).build();

We can name the builder after the features that are common and the domain objects
after their differences.

.. danger::
    This technique works best if the objects differ by the same
    field. If the objects vary by different fields, each ``build()`` will pick up the
    changes form the previous uses. For example, it's not obvious in this code that
    ``orderWithGiftVouche`` will carry the 10% discount as well as the gift voucher:

    .. code-block:: java

      Order orderWithDiscount = hatAndeCape.withDiscount(0.10).build();
      Order orderWithGiftVoucher = hatAndeCape.withGiftVoucher("abc").build();

    For complex setups, the safest option is to make the "with" methods functional
    and have each one return a new copy of the builder instead of itself.

Where a test data builder for an object uses other "build" objects, we can pass in those
builders as arguments rather than their objects.

.. code-block:: java
  :linenos:

  Order orderWithNoPostcode = new OrderBuilder()
    .fromCustomer(
      new CustomerBuilder()
        .withAdress(new AddresBuilder().withNoPostcode().build())
        .build()
    ).build();

  //pass the Builder Objects in the methods

  Order order = new OrderBuilder()
    .formCustomer(
      new CustomerBuilder()
        .withAddress(new AddresBuilder().withNoPostcode())
    ).build();


Introducing Factory methods
---------------------------
We can reduce the noise in the test code by wrapping the builders in factory methods.

.. code-block:: java

  Order order = anOrder().fromCustomer(aCustomer().withAddress(anAddress().withNoPostcode())).build();

We can take adventage of Java's method overloading by collapsing this to a single
``with()`` method.

.. code-block:: java

  Order order = anOrder().from(aCustomer().with(anAddress().withNoPostcode())).build();


We use test data builders to reduce duplication and make the test code more expressive.
It's another technique that reflects our obsession with the language of code, driven
by the principle that **code is there to be read**. Combined with factory methods and
test scaffolding, test data builders help us write more literate, declarative tests
that describe the *intention* of a feature, not just a sequence of steps to drive it.

Using these techniques, we can even use higher-level tests to communicate directly with
non-technical stakeholders. We can use the tests to narrow down exactly what a feature
should do and why.
