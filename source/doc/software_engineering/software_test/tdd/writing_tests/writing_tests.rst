=============
Writing Tests
=============
Write the tests clear and give them a descriptive error message. When you execute the
test for the first time it will fail. Make sure you get the expected error and check
that the diagnostics are helpful.

.. hint:: Unit-test behavior, not Methods

Tests should focus on the feature tat the object under test should provide.

When we find a feature that's difficult to test we don't just ask ourselves *how* to
test it, but also *why* is it difficult to test.
When code is difficult to test, the most likely cause is that our design needs improvement.
Tests are a valuable early warning of potential maintenance problems and we should
use these hints to fix a problem while it is still fresh.

- starting with the test means that we have to describe *what* we what to achieve before we
  consider *how*
- to keep unit tests understandable and maintainable we have to limit their scope. If the
  tests end up too long we might have to break the component unter test into smaller pieces.
- to construct an object for a unit test, we have to pass its dependencies to it,
  which means that we have to know that they are. This encourages context independence (:ref:`OOP_Context`).

.. note::
  Break up an object if it becomes to large to test easily, or if its test failures
  become difficult to interpret. Then unit-test the new parts separately.

We start by writing our test as of the implementation already exists, and then filling
in whatever is needed to make it work - This is called **programming by wishful thinking**.
Working backwards from the test helps us focus on *what* we want the system to do,
instead of getting caught up in the complexity of *how* we will make it work.
So first we code up a test to describe our intentions as clearly as we can. Then we
build the infrastructure to support the way we want to test the system, instead of
writing the test  to fit in a existing infrastructure. With the infrastructure in
place, we can implement the feature and make the test pass.

.. hint::

  Testmethods consist of three parts: **AAA**
    - Arrange
    - Act
    - Assert

.. tip::

    Great structure for writing tests:

    - write the test names which helps us deciding what we want to achieve.
    - write the act part (call to the target code , which is the entry point to the feature)
    - write the expectations and assertions
    - then the setup and teardwon

How many Assertions in a Test Method?
    Generally: each test should only contain one expectation or assertion.
    This can be impractical. A better rule is to think of one coherent feature
    per test, which might by represented by up to a handful of assertions.


.. todo:: ``assume`` Functionality

We only catch exceptions in a test if we want to assert something about them.

The assertions and expectations of a test should communicate precisely what
matters in the behavior of the target code. If tests assert too much detail,
tests get difficult to read and brittle when things change. We try to keep
expectations and assertions as narrowly defined as possible.

.. todo:: pre and post conditions

Third Party Code
================
.. hint:: Only Mock Types that you own. Don't Mock Types you cant change.

We write layer of *adapter objects* that use the third party API to implement these interfaces.
We test these layers with focused integration tests to confirm our understanding of
how the third-party API works. This approach produces a set of interfaces that define the relationship
between our application and the external API and discourages low-level technical
concepts from leaking in our applications domain model.


Test Readability
================
Teams that adopt TDD usually see and early boost in productivity because
the tests let them add features with confidence and catch errors immediately.
For some teams, the pace then slows down as the tests themselves become
an maintenance burden. For TDD to be sustainable, the tests must do more
than verify the behavior of the code. They must also express that
behavior clearly. They must be **readable**!

This matters for the same reason that code readability matters: every
time the developers have to stop and puzzle through a test to figure
out what it means, they have less time left to spend on creating new
features.

We take as much care about writing our test code as about production code.
But we use two difference styles since the two types of code serve different purposes.

Testcode should describe *what* the production code does. That means
that it tends to be concrete about the values it uses as examples of what
the results to expect, but abstract about how the code works.

Production code, on the other hand, tends to be abstract about the values
it operates on but concrete about how it gets the job done. Similarly,
when writing Production code, we have to consider how we will compose our
objects to make up a working system and manage their dependencies carefully.
Test code should have as little dependencies as possible.

Because test code is more concreate it tends to have more literal values.
Literal values without explanation are difficult to understand because the
programmer has to interpret whether a particular value is significant (e.g.)
just outside of the allowed range) or just a arbitrary placeholder.

A great solution to that is to use variables and constants with descriptive
names.

.. code-block:: java

  public static final Chat UNUSED_CHAT = null;

  public static INVALID_ID = 666;

  public static final HIGHEST_PAYMENT_ALLOWED = 9999;

We name these variables to show the roles their values or objects play in
the test and their relationship to the target object.

We want our test code to read like a declarative description of what is being
tested.

Bad tests:
    - unclear test names
    - single tests that exercise multiple features
    - to much set up code that buries logic
    - tests that uses literal values ("magic numbers"), and do not explain their usage clearly

All code should emphasize "what" it does over "how", including test code.

Test names
----------
The name of the test should be the first clue for a developer to understand
what is being tested and how the target object is supposed to behave.

TestDox convention
  Each test name reads like a sentence, with the target class as the
  implicit subject.

  .. code-block:: java

    public class ListTest{
      @Test public void holdsItemsInTheOrderTheyWereAdded(){...}
      @Test public void canHoldMultipleReferencesToTheSameItem(){...}
    }

  This encourages the developer to think in terms of what the target object
  *does*, not what it *is*.

These names can be **as long as we like** because they are only called through
reflection - we never have to type them in to call them. The testname
should say something about the expected result.

.. code-block:: java

    pollsTheServersMonitoringPort() //does not tell us enough

    notifiesListenersThatServerIsUnavailableWhenCannotConnectToItsMonitoringPorts() //very descriptive

Test Diagnostics
================
The point of a test is not to pass but to fail. A "failing" test has actually
succeeded at the job it was designed to do. We want the production code to pass
its test, but we also want the tests to detect and report any errors that do exist.

One situation we want to avoid, is when we can't diagnose a test failure that has
happend. If a failing test clearly explains what has failed and why, we can quickly
diagnose and correct the code. We want to make sure that tests give us the necessary
information at runtime.

If a test is small, its name should tell us most of what we need to know about what
has gone wrong.

Hamcrest Matchers are a great way to get expressive failure messages.

A test that has both expectations and assertions can produce a confusing failure.
In jMock the expectations are checked after the body of the test. If, for example,
a collaboration  doesn't work properly and returns a returns a wrong value, an
assertion might fail before any expectations are checked. This would produce a
failure report that shows, say, an incorrect calculation result rather than the missing
collaboration that actually caused it.

So sometimes it is worth calling the ``assertIsSatisfied()`` method on the ``Mockery``
before any of the test assertions to get the right failure report.

.. code-block:: java

  context.assertIsSatisfied();
  assertThat(result, is(expectedResult));

This demonstrates why it is so important to *watch the test fail*. It lets your check
whether the failure report is meaningful and useful for the future developer when
something breaks.

Test Flexibility
================
There is a virtuous relationship with test readability and resilience. A test that
is focused, has clean setup and has minimal duplication is easier to name and is more
obvious about its purpose.

.. hint:: **Specify precisely what should happen and no more**

The more precise we are, the more the code can flex in other unrelated dimensions
without breaking tests misleadingly.

A test might need to pass a value to trigger the behavior it is supposed to exercise
in tis target object. The value could either be passed in as a parameter to a method
on the object, or returned as a result from a query the object makes on one of its
neighbors stubbed by the test. If the test is structured in terms of how the value
is represented by other parts of the system, then it has a dependency on those parts
and will break when they change.

For example, imagine we have a system that uses a ``CustomerBase`` to store and
find information about our customers. One of its features is to look up a ``Customer``
given an email address. It returns ``null`` if there's no customer with the given address.

.. code-block:: java

  public interface CustomerBase {
    //Returns null if no cusomer found
    Customer findCustomerWithEmailAddress(String emailAddress);
  }

  //bad testcode
  allowing(customerBase).findCustomerWithEmailAddress(theAddress);
  will(returnValue(null));

Better to use declarative Variable names:

.. code-block:: java

  public static final Customer No_CUSTOMER_FOUND = null;

  //change to:
  public static final Maybe<Customer> No_CUSTOMER_FOUND = Maybe.nothing();

If we change the return values of our CustomerBase methods we can easily change
the variable without modifying the tests themselves.

Tests should be written in terms of the information passed between objects, not
of how that information is represented. Doing so will both make the tests more
self-explanatory and shield them from the changes in implementation controlled
elsewhere in the system. Significant values, like ``No_CUSTOMER_FOUND``, should be
defined in one place as a constant. For more complex structures, we can hide the
details of the representation in test data builders.

In a test, focus the assertions on just what's relevant to the scenario being tested.
Avoid asserting values that aren't driven by the test inputs, and  avoid reasserting
behavior that is covered in other tests.

Each test method should exercise a unique aspect of the target code's behavior.
This makes the tests more robust because they're not dependent on unrelated results
and there's less duplication.

.. hint::

      Allow Queries; Expect Commands
        *Commands* are calls that are likely to have side effects, to change the
        world outside the target object. The state of the system will be different
        if we call the method a different number of times.

        *Queries* don't change the world, they can be called any number of times,
        including none.

With jMocks ``ignoring()`` we can simplify a test by "ignoring" collaborators that are not relevant
to the functionality being exercised. jMock will not check any calls to ignored objects.

As programmers, we must also make sure that ignored features are tested somewhere
and that there are higher-level tests to make sure everything works together.
In practice we usually introduce ``ignoring()`` only when writing specialized
tests after the basics are in place.
