=================
5 Premises of TDD
=================

http://geepawhill.org/five-underplayed-premises-of-tdd-2/

Money
=====
We make money in software development through **shipping more value faster**.
We TDD because test-driven development is the best way we’ve devised so far to
actually do that.

TDD is not about good citizenship. You are not immoral if you don’t TDD.
TDD very often does increase the quality of teams that aren’t doing it to
begin with, but that’s actually neither here nor there, because TDD isn’t about
that. TDD is about more value faster.

Judgement
=========
The judgment premise says we are absolutely, routinely, every day, all the time
happily reliant on individual humans using their individual best judgment to guide
us through the process.

Internal Quality And Productivity Correlate Directly
====================================================
The correlation premise says that internal quality and productivity are correlated.
They go up together and they go down together.

You can trade external quality for productivity, but not internal.

Test A Chain By Testing Its Links
=================================
chain premise is telling us that we write tests such that mostly what they
concentrate on is individual connections in that dependency chain. So if I have a
bunch of things, A, B, C, D, and E, where A calls B, B calls C, and so on, what I
focus on first is how to write tests that can prove that A works assuming B works.

Tests & Testability Help Steer Design
=====================================
The steerability principle says we use tests and testability as one of the
factors all the way through development, just as we do the other factors.
