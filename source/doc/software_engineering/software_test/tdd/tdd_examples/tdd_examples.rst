============
TDD Examples
============

Prime Factors
=============

Standard TDD exercise: create prime factor algorithm

Sorting
=======

put in if statements and create new array leads to quick sort

Best solutions
==============

As you do TDD there are usually a different courses to take, when you take
the right course you get to an better algorithm.

If your solution involves assignment find a solution that does not. If you can


#. Transformation priority premise. Production code can be transformed into
   ever more general states. You prefer transformations that don't have assignment
   in them.
