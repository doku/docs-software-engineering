============
TDD Workflow
============

#. Specify need for new Feature (requirements analysis and specification)
#. Split the Feature in individual components
#. Write Acceptance Test to cover at least the happy paths through the feature
#. Write Unit test for one Component (public method), this test defines the flow
   of the component and is the very first user of the given component
#. Add all needed method bodies until there are no compiler errors anymore
#. Run the Test and **see** it fail. Make sure the given Error message is meaningful.
#. Start implementing the method unter test. While writing you that all helper
   methods you might need already exist. Just call them in your implementation
   and write the concreate implementation later.
#. Pass the first Unit Test
#. Refactor your code to be beautiful.
#. Write more failing unit tests and make them pass.
#. Pass the Acceptance Test.

.. todo:: Errorhandling, performance test etc.

.. todo:: how long should be one cycle from failing test to test passed?
   (10min)


Releasecycle
============

When developing a feature make sure to finish the **minimal viable product**
as fast as possible. This is a version of your feature which is broken as far
down to its essentials as possible. This version, while not beautiful or completely
egged out, should still be *releasable*.
