======================
Structuring Unit Tests
======================

Inner Class Structure
=====================

For each class in your production code you create a test class. For each method
in you production class you have a nested class inside you test class. To allow
you to use common setup methods, each nested class inherits for the outer parent
class, which contains the setup methods.

This approach makes it easy to find a specific test you are searching for, and
you can collapse all tests you don't need. All test methods for one production
method are in one place.
