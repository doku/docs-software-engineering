==============================
Acceptance Tests with Cucumber
==============================

Why Cucumber
============
Cucumber tests help non-technical stakeholders engage and understand testing,
resulting in collaboration and bridging the communication gap.
Every ``*.feature`` file consists in a single feature, focused on the business value.

Structure of a test
===================

Documentation: https://github.com/cucumber/cucumber/wiki/A-Table-Of-Content

.. note::
  define functional requirements rather than procedures

In feature files, what you and your client should focus on is that
which has to happen, not how you expect it to happen. That way when
somebody later decides that challenge and response authentication
schemes are passé then you simply need change the authentication
process steps behind the scenes. Your outward facing feature
files—the ones that your clients get to see—need not change at all.
In fact, a good question to ask yourself when writing a feature
clause is: Will this wording need to change if the implementation
does?


Template
========

.. code-block:: language

  Feature: Title (one line describing the story)

  Narrative Description: As a [role], I want [feature], so that I [benefit]

  Scenario: Title (acceptance criteria of user story)
  Given [context]
  And [some more context]...
  When [event]
  Then [outcome]
  And [another outcome]...

  Scenario: ...

Keywords
========

Scenarios
---------
.. note::
  Keep each scenario independent.

Background
----------
If you use the same steps at the beginning of all scenarios of a feature,
put them into the feature’s background scenario.
The background steps are run before each scenario.



https://semaphoreci.com/community/tutorials/introduction-to-writing-acceptance-tests-with-cucumber
