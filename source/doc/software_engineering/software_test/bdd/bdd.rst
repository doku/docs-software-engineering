=================================
Behavior-Driven Development (BDD)
=================================
Ziel von BDD ist das Spezifizieren von Verhalten in einer Form, wie es auch von
Nicht-Softwareexperten verstanden werden kann.

The two main practices of BDD are **Specification by Example** and
Test-Driven Development.

.. figure:: bbd_scenario.png
	 :alt: bbd_scenario

Specification by Example
------------------------
Specification by Example (SbE) is a technique that enables product owners,
business analysts, testers and programmers to eliminate common misunderstandings
about business requirements. SbE uses examples in converstions to illustrate
business rules and behavior of the software to be build.
