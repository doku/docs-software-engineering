==============
Bug Management
==============

Where a backlog of bugs exist, it is important for the problem to be clearly
visible to everyone, and for members of the development team to be responsible
for facilitating the process of reducing the backlog.

You can treat defects the same way as features. Working on a bug takes time and
effort away form working on some other feature, so it is up to the customer to
prioritize the relative importance of a particular bug against a feature.

Bugs can be prioritized in your backlog depending on how often they occur, what
their effect on the user is, and if there is a workaround.


Bug Fixing
==========
When you try to fix a bug you should first build a unit test recreating the bug
consistently. If you pass that test you know that you have fixed the bug.
Reference the issue tracker id of the bug in the test that is supposed to reproduce
the bug. 
