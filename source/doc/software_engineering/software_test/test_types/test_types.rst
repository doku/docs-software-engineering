==========
Test Types
==========

Contract Tests
==============

Contract tests test your test double of an external service against the
real service. The make sure that your double accurately represent the external
service and alert you when the external service changes its contract.

.. figure:: contract_tests.png
	 :alt: contract_tests

.. todo:: SelfInitializingFake
