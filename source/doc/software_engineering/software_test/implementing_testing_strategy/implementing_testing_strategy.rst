===============================
Implementing a Testing Strategy
===============================

Greenfield
==========

Brownfield
==========

The best way to introduce automated testing is to begin with the most common,
important and high-value use cases of the application. You should automate
happy path tests to cover these high-value scenarios. It is useful to maximize
the number of actions that these tests cover

Legacy Projects
===============
Test the code that you change. Additionally you should write tests for the high-
value stuff and explain your :ref:`product-owner` that a regression test suite
will protect these functions of the system. You should not spend too long doing
this, since this is a skeleton to protect the legacy functions.

Similar to Brownfield applications you can create a

New behavior should be added using stories with acceptance criteria for the new
feature and automated test should be mandated to represent the completion of
these tests stories.

It is important to remember that you should only write automated tests where
they will deliver value.
