=============
Test Coverage
=============

Generally speaking our set of automated tests is comprehensive if the code coverage
is greater than 80%.

Beim Glassbox Test lässt sich mittels Werkzeuge die Testüberdeckung messen.

.. hint::
  Auch eine vollständige Überdeckung ist kein **Korrektheitsbeweis**

- Befehlsüberdeckungen
- Zweigüberdeckung
- Termüberdeckung
- Pfadüberdeckung (:ref:`cyclomatic-complexity`)

.. hint:: It is easy to reach a test coverage of 100% without testing anything useful.

Beispiel
========

.. figure:: ueberdeckung_example.png
   :alt: ueberdeckung_example
   :scale: 50%
   :align: center

- Befehlsüberdeckung: *A, B, C, D* müssen ausgeführt werden.
- Zweigüberdeckung:  *p, q, u, v* müssen jeweils in beiden Zweigen durchlaufen werden
- Termüberdeckung: Alle möglichen Ursachen für die Verzweigungen müssen wirksam
  geworden sein.
- Pfadüberdeckung: Alle möglichen Pfade müssen durchlaufen werden. (wegen *q* sind
  das hier möglicherweise unendlich viele).
