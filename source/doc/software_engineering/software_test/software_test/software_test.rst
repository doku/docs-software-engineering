==============
Software Tests
==============

Testen ist die Ausführung eines Programms auf einem Rechner. Das
erwartete Verhalten/Ergebnis ist bekannt und es werden die Sollwerte mit
den Istwerten des Programmes verglichen. Stimmen diese nicht überein hat
der Test einen Fehler gefunden. Dieser liegt entweder im Programm oder
im Test selber (bei falschen Sollwerten).

.. figure:: test_setup.png
	 :alt: test_setup

Testen ist ein **natürliches** Prüfverfahren. Der Test ist **reproduzierbar**
und damit objektiv. Tests sind kostengünstig reproduzierbar, schweigen aber über die
Fehlerursache. Zudem wird die Zielumgebung mit geprüft und Systemverhalten wird
sichtbar gemacht.

.. hint:: "Programme sind nie fehlerfrei"

Oft wir aber die Aussagekraft des Tests überschätzt, denn er zeigt nicht Korrektheit.
Tests sagen über die meisten Software-Eigenschaften nichts. Man kann auch nicht
alle Anwendungssituationen nachbilden. Zudem zeigt der Test nicht die Fehlerursache
sondern nur, dass ein Fehler vorliegt. Auch wenn alle Tests grün sind, heißt das
nicht, dass die Software fehlerfrei ist.

.. note::

  Testen kann man immer nur die Anwesenheit, aber nie die Abwesenheit
  von Fehlern zeigen.  (Edsger Dijkstra)

  Es unmöglich bei nichttrivialen Programmen die Fehlerfreiheit
  zu beweisen.

Die Eingabe- und Ausgabedaten sollten in sinnvolle Äquivalenzklassen eingeteilt
werden. Für diese Äquivalenzklassen können einzelne Tests geschrieben werden.

.. figure:: KlassifikationTestverfahren.png
   :alt: Klassifikation von Testverfahren

   Klassifikation von Testverfahren

Tests sollten möglichst automatisiert durchgeführt werden können. Nach
jeder Änderung am Code werden diese dann ausgeführt und man erhält
direkt Rückmeldung darüber, dass die aktuelle Änderung keine bisherige
Funktionalität gebrochen hat.

Testmanagment
=============
Die Auswahl der Testfälle ist die zentrale Aufgabe des Testers. Ziel ist es mit
möglichst vielen Fehlern auf die Spur zu kommen.

.. figure:: testmanagement_steps.png
	 :alt: testmanagement_steps

Es lässt sich zwischen automatisierten Tests und explorative Tests unterscheiden.

Mit **automatisierten Tests** lassen sich Regressionsfehler aufdecken. Das
**explorative Testen** (:ref:`exploratory-testing`)findet neue Fehler. Hierbei wird manuell gearbeitet und
man versucht sich kreativ neue Szenarien zu überlegen, in denen die Software
fehlschlagen könnte. Wird ein Fehler gefunden, sollte dieser für die Zukunft
durch einen automatisierten Test abgesichert werden.

Die Anforderungsspezifikation wird mit Black-Box-Tests getestet. Die Testfälle
werden direkt aus der Spezifikation abgeleitet.

Tests sollten die Äquivalenzklassen und Grenzwerte der Eingabedaten abdecken.

Der Quelltext wird durch Glassboxtest getestet.

Häufig verwendete Komponenten sollten besonders oft verwendet werden. Komponenten
sollten je nach Ausfallrisiko und Wichtigkeit besonders intensiv getestet werden.
Komponenten und Funktionen die vom Nutzer besonders oft verwendet werden, sollten
auch besonders gut getestet werden.

.. todo:: Humble Object Pattern

	The Humble Object Pattern is the way to separate the things that are testable
	form the things that are difficult to test.

	http://xunitpatterns.com/Humble%20Object.html

Suche nach Testfällen
---------------------

-  unsystematische Methode

   -  Testen mit Zufallswerten

-  Grenzwertanalyse Methode

   -  mögliche Extrema und Grenzwerte testen

-  Fehlererwartungs Methode

   -  Ermittlung möglicher, häufig übersehbarer Fehlerquellen
      -Äuivalenzmethode
   -  Klassifikation der Eingabedaten bezüglich ihres Verhaltens, Test
      mit einem Vertreter jeder Klasse

-  Kontrollflussorienterte Methode

   -  Alle Quellcodeteile >=1 mal ausführen


Die Testfälle können also durch Anforderungen, Struktur, Statistik oder Risiko
motiviert sein.

Levels of testing
=================

.. figure:: types_of_tests.png
	 :alt: types_of_tests

+------------------+----------------------------------------------------------------------+
| **Acceptance**   | Does the system meet the clients requirements?                       |
+------------------+----------------------------------------------------------------------+
| **System**       | Does the whole system work? (performance, penetration test etc.)     |
+------------------+----------------------------------------------------------------------+
| **Integration**  | Does our code work against code we can't change? (external libraries)|
+------------------+----------------------------------------------------------------------+
+ **unit**         | Do our objects do the right thing, are they convenient to work with? |
+------------------+----------------------------------------------------------------------+

.. figure:: testing_hirarchie.png
	 :alt: testing_hirarchie

#. Unit- oder Modultests testen die kleinsten Einheiten (Klassen, Module) separat.
#. Bei Integrationstests werden die Schnittstellen und die Zusammenarbeit zwischen
	 integrierten Einheiten getestet.
#. Im Systemtest wird das komplett integrierte System in Produktionsumgebung (oder
	 ähnliche Testumgebung) getestet.
#. In Akzeptanztests, wird das System gegen die Kundenanforderungen aus der
	 Spezifikation getestet.

.. figure:: v_model_testing.png
	 :alt: v_model_testing

.. attention:: Don't test business rules through the UI!

Blackbox Test
=============
Blackbox Tests haben keine Kenntnisse über die innere Funktionsweise des
Programms. Die Tester sehen nur die Spezifikation und die Ergebnisse.

Bei Blackbox Tests wird das Gesamtsystem anhand der Spezifikation
verifiziert. Es ist nicht möglich um Fehler herum zu testen. Man hat
aber einen großen organisatorischen Aufwand.

Blackbox Tests sollten nicht vom Entwicklerteam durchgeführt werden.

Whitebox oder Glasbox Test
==========================

Glasbox Test haben Kenntnisse über die innere Funktionsweise. Es sind
ablaufbezogene Tests und können die fehlerverursachenden
Systemkomponenten identifizieren. Glasbox Tests sind geeignet für
einzelne Module und Teilsysteme, aber zu umfangreich für komplette
Systeme.

Glasbox Tests können die Abweichung von der Spezifikation nicht
erkennen. Dafür ist ihre Durchführung relativ einfach.

Das Ziel bei einem Glasbox Test ist die vollständige Codeüberdeckung.

.. _smoke-test:

Smoke Test
==========
In a smoke test you run a subset of test cases that cover the most important
functionality of a component or system, to ascertain if crucial functions of
the software work correctly.
The process of smoke testing aims to determine whether the application is so
badly broken as to make further immediate testing unnecessary.

Smoke testing performed on a particular build is also known as a
**build verification test** (:ref:`deployment-smoke-test`).

Äquivalenzklassen-Tests
=======================

.. glossary::

	Äquivalenzklassen im Test
		Wenn es Gründe für die Annahme gibt, dass das Programm mit zwei verschiedenen
		Startzuständen :math:`S_1` und :math:`S_2` gleiches Verhalten zeigt, also
		mit beiden Werten korrekte oder mit beiden Werten falsche Ergebnisse liefert,
		dann sind :math:`S_1` und :math:`S_2` **äquivalent** und es muss nur **einer**
		der beiden getestet werden. (Myers, 1979)

Die Menge der zu testenden Startzustände kann aus den Äquivalenzklassen abgeleitet
werden. Jede Äquivalenzklasse der Eingabedaten muss in mindestens einem Testfall
berücksichtigt werden. Dabei sollten speziell **Grenzwerte** zwischen den
Äquivalenzklassen berücksichtigt werden. Es sollte also neben den repräsentativen
Werten einer Äquivalenzklasse auch immer Werte  **auf**, **unter** und **über** der
Grenze getestet werden.

Besonders an Grenzwerten entstehen oft Fehler.

Integration Tests
=================
Integration tests ensure that each independent part of your application works
correctly with the services it depends on.

In integration tests you want to run against test services of your external
services. Make sure to test how your applications behaves when the external
service doesn't behave as expected.

Automated integration tests can be reused as smoke tests during deployment of your
system into production.

Unit-Tests
==========

Unit-Tests untersuchen das Verhalten der kleinsten Arbeitseinheiten. In
Java sind das einzelne Methoden. Die Interaktion mehrere Einheiten ist
nicht von Interesse. Ganz im Gegenteil alle Abhängigkeiten sollen
möglichst reduziert werden. Nach der Implementierung werden die Einheiten als
erstes durch den Unit-Test getestet.

Testcases
=========

Die Testmethoden zur einer Klasse können in einer eigenen Testfall-Klasse geordnet
werden. Es können aber auch eigene Testklassen für Features des *System under test* (SUT)
erstellt werden.

.. figure:: testclasses_features.png
	 :alt: testclasses_features

State Verification
------------------
Das SUT liefert keinen Wert zurück, sein Zustand ändert sich aber. Hier
wird die Zustandsänderung überprüft. Wir vergleichen den Zustand des zu testenden
Objekts mit dem Zustand eines erwarteten Objekt

Behavior Verification
---------------------
Das SUT zeigt ein sichtbares Verhalten, welches wir mit dem erwarteten Verhalten
vergleichen.
