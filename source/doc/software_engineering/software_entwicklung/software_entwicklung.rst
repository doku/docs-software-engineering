====================
Software Entwicklung
====================

.. tip::
  Software is like a plant: to keep it beautiful and healthy you have to constantly
  water and  feretilize it and cut of unwanted branches.

.. _ref-sequential-software-development:

Sequentielle Software-Entwicklung
=================================

Wasserfall Modell
-----------------

The waterfall is a model the software development process in which the constituent
activities, typically a concept phase, requirements phase, design phase, implementation
phase, test phase and installation and checkout phase, are preformed in that order,
possibly with overlap but with little to no iteration. (IEEE)

.. figure:: wasserfallmodell.png
	 :alt: wasserfallmodell

Da Zyklen als überflüssig erklärt werden, entsteht gewissermaßen ein **Einbahnstraßenmodell**.

Systementwicklungsprozess (Embedded Systems) ist sehr sequentiell. Hier kann das Wasserfall Modell
gut angewendet werden.

.. note::

  The waterfall methodology works as long as the solution and the problem are
  both relatively well understood.

Prototypen Entwicklung
======================
Attempt to do the job twice - the first result provides an early simulation of the
final product.

Software-Prototypen realisieren meist nur sehr rudimentäre Funktionalität. Oft zeigen
sie die Oberfläche des vermuteten Zielsystems. Dabei bezeichnet Prototyping die
Vorgehensweise, bei der die Mock-Ups entworfen, konstruiert, bewertet und revidiert
werden.

Der Zweck des Software-Prototyping ist das Klären der Anforderungen, um eine lange und
teure Entwicklung zu verhindern, die am Ende ein Produkt liefert, das der Kunde
so nicht haben will.

.. figure:: prototypen_entwicklung_vorgehen.png
	 :alt: prototypen_entwicklung_vorgehen

.. glossary::

  Exploratives Prototyping
    Ziel: Analyse unterstützen und ergänzen, Es werden Demonstrationsprototypen und
    funktionale Prototypen erstellt.

  Experimentelles Prototyping
    Ziel: Technische Umsetzung eines Entwicklungsziels (funktionale Prototypen, Labormuster)

  Evolutionäres Prototyping
    Eigentlich kein Prototyping sondern ein spezielles Verständnis des Entwicklungsprozess

Inkrementelle Entwicklung
=========================

Schritt für Schritt wird ein System aus mehreren Inkrementen aufgebaut. Das zu
entwickelnde System wird nicht in einem Zug konstruiert, sondern in einer Reihe von aufeinander
aufbauenden Ausbaustufen. Jede Ausbaustufe wird in einem eigenen Projekt erstellt und
in der Regel auch schon ausgeliefert.

.. hint::
  **Incremental**: build a system feature by feature

.. figure:: inkrementell_komponenten.png
	 :alt: inkrementell_komponenten

Das zu entwickelnde System bleibt in seinem Gesamtumfang offen. Es wird in
Ausbaustufen realisiert. Die erste Stufe ist das Kernsystem. Jede Ausbaustufe
erweitert das vorhandene System und wird in einem eigenen Projekt erstellt. Mit
der Bereitstellung einer Erweiterung ist in aller Regel auch (wie bei der iterativen
Entwicklung) eine Verbesserung der alten Komponenten verbunden.

.. figure:: inkrementellesvorgehen.png
  :alt: inkrementellesVorgehen

Die Inkrementelle Vorgehensweise führt zu früher Rückkopplung der Erfahrung
und kurzer Entwicklungszeit der einzelnen Inkremente.

Iterative Software-Entwicklung
==============================
In der Softwareentwicklung können wir an dem Endprodukt immer weiter bauen, ohne
dass wir dadurch Qualitätsverluste haben. Daher bietet sich ein iteratives Entwicklungsmodell
an.

.. hint::
  **Iterative**: progressively refining the implementation of a feature in response to feedback

In mehreren Iterationen werden Wasserfallmodelle durchgeführt und das Projekt ständig
angepasst.

Software wird in mehreren geplanten und kontrolliert durchgeführten Iterationsschritten
entwickelt. Ziel dabei ist, dass in jedem Iterationsschritt (beginnend mit der
zweiten Iteration) das vorhandene System auf der Basis der im Einsatz erkannten
Mängel korrigiert und verbessert wird. Bei jedem Iterationsschritt werden die
charakteristischen Tätigkeiten Analysieren, Entwerfen, Codieren und Testen
durchgeführt.

.. figure:: iterativeentwicklung.png
	 :alt: iterativeentwicklung

Spiralmodel
===========
Das Spiralmodel soll sicherstellen, das Risiken erkannt und möglichst früh im Projekt
bekämpft werden.

.. figure:: spiralmodel.png
	:alt: Spiralmodel

Dabei werden alle Risiken gesucht, die das Projekt bedrohen.
Gibt es keine Risiken, gilt das Projekt als erfolgreich beendet.
Alle vorhandenen Risiken werden bewertet, um das größte Risiko zu identifizieren.
Man sucht dann nach einer Möglichkeit, dieses größte Risiko zu beseitigen. Wenn
sich das größte Risiko nicht beseitigen lässt, ist das Projekt gescheitert.
Dieses Vorgehen wird immer wiederholt.

Direkte Anwendung hat das Spiralmodell heute kaum. Es diente aber als Vorbild für
viele heute hoch-aktuelle Prozessmodelle.


Ideale Entwicklungs Struktur
============================

Ein System wird in einzelne Komponenten (Inkremente) geteilt. Diese Komponenten
werden iterativ (in mehreren Zyklen) entwickelt. In jedem Iterations Schritt werden
nach dem Wasserfallmodel Analyse, Spezifikation, Entwurf und Codierung durchgeführt.

Besonders bei unklaren Zielen und hochinovativen Projekten bietet sich eine solche
Struktur an.
