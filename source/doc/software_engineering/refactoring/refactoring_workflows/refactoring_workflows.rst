=====================
Refactoring Workflows
=====================

.. note:: The essence of refactoring are small steps towards code quality improvements.


TDD Refactoring
===============


Litter-Pickup Refactoring
=========================

Boy-scout Rule

Judement of how much refactoring is appropriate.



Comprehension Refactoring
=========================

Whenever you have trouble understanding a code section, it should be refactored
after you have understood it. Or otherwise the next one who has to read the code
will waste time again. You save your understanding of the code through a better
design.

.. figure:: Refactoring_decision.png
	 :alt: refactoring_decision


Preparatory Refactoring
=======================

If your code is in a shape where you don't have a good fit for what you want
to add, you first want to begin refactoring in to the shape that now obviously
you want be in. Then you can easily add your new feature.

"We should have done it this way"

Start a new feature by changing the existing design to allow you to easily add the
new feature.

Every time we do a feature, we start by cleaning up the area where we are going to
do the work. We don't have to make it perfect, just sufficiently better to help our
feature ease in. And once our feature works, we clean up the code as we always should.
Areas where er do little work aren't slowing us down, and they are left mostly alone.
Areas where we do lots of work get more attention and they clean up quickly.


Planned Refactoring
===================

When you have build up technical depth to a point where you have to add
Refactoring to your project schedule it is called "planned refactoring".

A good team should hardly ever do a planned refactoring.

Long-Term Refactoring
=====================

Use the small scale refactorings to gradually move towards your long term
design goal.
