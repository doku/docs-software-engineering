.. _refactoring:

===========
Refactoring
===========

https://www.refactoring.com/

.. note::
  Refactoring is a disciplined technique for restructuring an existing
  body of code altering its internal structure without changing its external behavior.
  (Martin Fowler)

The word **refactoring** refers to a simple, regular process of keeping the code
clean. It means changing the internal structure of an existing body of code
without changing its behavior. The point is to improve the code to that it's a better
representation of the features it implements and making it more maintainable.

Dadurch lassen sich Codesmells verbessern ohne bisherige Funktionalität zu zerstören.
Automatisierte Unit Tests stellen sicher, dass die Strukturverbesserungen
nicht die Funktionalität verändern.

.. hint:: For save refactoring automated unit-tests are essential.

Durch Refactoring sollte die Wartbarkeit der Anwendung verbessert werden, es wird
lediglich die interne Struktur bearbeitet.

.. glossary::

  Boy Scout Rule
    Leave the campground a little better than you found it.

.. note:: Check-in cleaner, than Check-out

.. note::

  It's always tempting to start over and rewrite code. That's almost never the
  best plan. Instead, get good at refactoring!

Durch permanentes Refactoring, kann man die Wartung dann durchführen, wenn es nicht
*weh tut* und nicht so teuer ist.

After cleaning up your code check if it follows to the single-responsibility principle
(:ref:`single-responsibility-principle`). The last step in any kind of feature or
module development is to separate into very clear responsibilities. Always ask yourself the
question before finishing a feature: is my code well separated?

.. hint::
  Any time your are in the situation where your tests brake even though the behavior
  has not changed, all you have done is refactoring the implementation, you are
  witnessing coupling.

Successive Refinement
=====================
We refactor in tiny steps and run our test suite after each refactoring to validate
that we have not broken anything.

It is common to reverse several refactoring decisions you made earlier. Often
one refactoring leads to another that leads to the undoing of the first.
Refactoring is an iterative process full of trial and error, inevitable converging
on something that we feel is worthy of a professional.

No module is immune from improvement, and each of us has the responsibility to
leave the code a little cleaner than we found it (:term:`boy scout rule`).

Why Refactor
============

.. attention::

    You don't refactor because of clean code or professionalism but because of
    simple economics. We are refactoring in order to deliver more functionality
    more quickly.

Ask yourself the question "is this refactoring going to make me faster in
the long term"

You want to fix hotspots. (:ref:`effective-maintenance`)
