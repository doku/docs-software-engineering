===========
Refactoring
===========

.. toctree::
  :glob:

  refactoring/*
  code_smells/*
  refactoring_workflows/*
  refactoring_techniques/*
