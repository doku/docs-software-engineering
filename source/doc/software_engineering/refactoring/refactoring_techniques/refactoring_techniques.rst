======================
Refactoring Techniques
======================


.. figure:: refactorings_overview.png
	 :alt: refactorings_overview

- extract Method
- Pull-up Method (verschiebe Methode in Vererbungshierarchie)
- Primitivize Parameter
- Extract Interface


Method Object
=============

If you have a big method with many temporary variables, you can't easily extract
multiple methods because you would have to coordinate the sharing of variables
among these methods which would make it hard to see the underlying structure.

Therefore, turn the method into an object with an single method. All temporary
variables are now instance variables of the object. Since these are automatically
shared among any method of the object, you can now extract little methods
without worrying about coordination. As the little methods get extracted, the
object's structure and responsibilities become cleaner.
