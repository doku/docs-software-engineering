===========
Code Smells
===========
Code smells are key signs that refactoring is necessary.
In the process of refactoring, we get rid of smells,
enabling further development of the application with equal or greater speed.

The lack of regular refactoring, can lead to a complete paralysis
of a project over time.
Therefore, it is necessary to get rid of code smells while they are still small.

Usually these smells do not crop up
right away, rather they accumulate over time as the program evolves
(and especially when nobody makes an effort to eradicate them).

.. csv-table:: code smells
	:header: "Smell Category", "Description", "Examples"

	"Bloaters", "size of program elements make them hard to understand/change", "long methods, large classes, long parameter lists"
	"OO Abusers", "failure to leverage OO design", "switch statements, refused bequest"
	"Change Preventers", "make it harder to evolve code", "divergent changes, shotgun surgery"
	"Dispensables", "unnecessary complexity", "duplicate code, dead code, speculative generality"
	"Couplers", "unnecessary coupling", "feature envy, middle man"


Bloaters
========

Bloaters are code, methods and classes that have increased to such gargantuan
proportions that they are hard to work with.

Long methods
------------

.. figure:: long_method.png
	 :alt: long_method

A method contains too many lines of code (generally, any method that is longer
than ten lines should make you start asking questions).
Among all types of object oriented code, classes with short methods live longest.
The longer a method or function is, the harder it becomes to understand and maintain it.
In addition, long methods offer the perfect hiding place for unwanted duplicate code.

**Reason for the Problem:** something is always being added to a method but nothing
is ever taken out

**Treatment:** As a rule of thumb, if you feel the need to comment on something
inside a method,
you should take this code and put it in a new method. Even a single line can and
should be split off into a separate method, if it requires explanations. And if
the method has a descriptive name, nobody will need to look at the code to see
what it does.

**Recipies:**

- Extract Methods
- Reduce Local Variables and Parameters Before Extracting a Method
  - Replace Tempvariable with Query/ function call
  - Introduce Parameter Objects to group parameters
  - Pass on Whole Object, instead of many values from it
- Replace Method with Method Object
  - Transform the method into an separate class and split the method in several methods within the class
- Conditionals and Loops are a good clue that code can be moved to a separate method.
  - For conditionals, use Decompose Conditional, Decompose the complicated parts of the conditional into separate methods
  - If loops are in the way, try Extract Method and move the code in a separat function

Does an increase in the number of methods hurt **performance**, as many people claim?
In almost all cases the impact is so negligible that it's not even worth worrying about.
Plus, now that you have clear and understandable code, you are more likely to
find truly effective methods for restructuring code and getting real performance
gains if the need ever arises.
