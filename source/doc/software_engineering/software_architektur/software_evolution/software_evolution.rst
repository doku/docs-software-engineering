==================
Software Evolution
==================

When you find a low level design problem you want to fix with a refactoring, take
one step back to a higher level of abstraction and make sure the high level
structure of the code is fine.
A low level cleanup is only worth the effort if the high level structure is
fine.

Possible issues in the code need to be tracked in an issue-tracker and each
issue needs a estimated cost. This allows you to compare issues and find the
most important ones.

Man ist jahrelang mit Syntome Fixen beschäftigt, wenn die Ursache nicht behoben
ist.

- Change by split
- Strangulate Bad Parts

Software Improvements
=====================

You need to compare the cost of the problem to the cost of the steps required
to fix it.


.. figure:: aim42_issue_domain.png

  aim42 issue domain

#. Cost of Issue
#. Business value
#. Solution efforts

- What are the cost factors influencing the cost of my problem?
