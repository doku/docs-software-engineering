==========================
Architecture Documentation
==========================

Man tauscht Korrektheit gegen detailgrad

Low Level hat deutlich höhere Volatilität

Kontext Abgrenzung
==================

Übersicht über das System und seine Außenschnittstellen.

.. _architecture-decision-records:

Architecture Decision Records
=============================

http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions

- Here is the decision we made
- Here is people who made it and when
- Here are the things we considered
- Here are the things we discarded/ the choices we decided not to do
- So this is what we ended up with
- Here are some of the risks that the solution introduces

Plans for changes can be analyzed whether the environment under which the decisions
have to be made have changed substantially.


It is really important to highlight exemplar parts of the code. These pieces become
very important drivers in architecture.
