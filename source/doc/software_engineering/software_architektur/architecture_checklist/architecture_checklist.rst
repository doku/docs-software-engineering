======================
Architecture Checklist
======================

- Gather those things together that change at the same time and for the same reasons. 
  Separate those things that change at different times or for different reasons. (CCP and SRP)
- Don't depend on things you don't need. (CRP and ISP)
- The granule of reuse is the granule of release (REP)
- Allow now cycles in the component dependency graph. (Acyclic Dependencies Principle)
- A component should be as abstract as it is stable. (Stable Abstractions Principle SAP)
- Depend in the direction of stability. (Stable Dependencies Principle SDP)
        