=================
Architekturmuster
=================

.. glossary::

  Architectural Pattern
    An architectural pattern expresses a fundamental structural organization schema
    for software systems. It provides a set of predefined subsystems, specifies
    their responsibilities, and includes rules and guidelines for organizing the
    relationships between them. (Buschmann 1996)

Architekturmuster sind auf der Ebene der Systemarchitektur definiert und stellen
Vorlagen dar, nach denen konkrete Architekturen entworfen werden können.

Die Design Patterns (:ref:`ref-design-pattern`) sind ähnliche Muster für den Entwurf
von Komponenten.

Wenn wir ein Architekturmuster verwenden, legen wir damit die Struktur auf der
obersten Ebene der Architektur fest. Die Wahl des Architekturmusters ist darum
eine zentrale Entwurfsentscheidung und bestimmt Aufbau, Erweiterbarkeit, Kommunikation etc.
Ob das vorgesehene Architekturmuster geeignet ist, hängt davon ab, ob seine
speziellen Rahmenbedingungen erfüllt sind.

.. _layer-architectural-pattern:

Layers architectural pattern
============================

.. glossary::

  Layers architectural pattern
    The layers architectural pattern helps to structure applications that can be
    decomposed into groups of subtasks in which each group of subtask is a particular
    level of abstraction. (Buschmann 1996)

Schichtenarchitektur trennt die Bestandteile eines Softwaresystems in aufeinander
aufbauende Schichten.

Eine Schicht fasst logisch zusammenhängende Komponenten zusammen und stellt
Dienstleistungen zur Verfügung, die an der (oberen) Schnittstelle der Schicht angeboten
werden. Die Dienstleistungen einer Schicht können nur von Komponenten der direkt
darüberliegenden Schicht benutzt werden. Schichten sind nur gekoppelt, wenn sie benachbart sind.

.. figure:: layers_architectur_structur.png
	 :alt: layers_architectur_structur

Schichten, die nur mit ihren Nachbarschichten kommunizieren, werden auch als
**protokollbasierte Schichten** bezeichnet. Eine solche Schicht verbirgt die
unter ihr liegende Schicht und definiert ein Protokoll das der darüberliegenden
Schicht zur Verfügung steht.

Soll eine tiefere Schicht für alle höheren Schichten sichtbar sein, dann wird diese
Schicht **objektorientierte Schicht** genannt.

.. hint::

  Die Komponenten einer Schicht dürfen immer nur Komponenten der nächsttieferen
  protokollbasierten Schicht benutzen.

  Die Komponenten einer Schicht können die Komponenten aller tieferen objektorienterten
  Schichten nutzen.

Viele interaktive Software-Systeme sind aus einer Datenhaltungs-, einer Anwendungs-
und einer Präsentationsschicht aufgebaut.

In der Anwendungsschicht sind alle Komponenten zusammengefasst, die fachliche
Funktionalität realisieren. sie sind so konstruiert, dass sie keine Informationen
über die Präsentation enthalten. Um Daten zu manipulieren oder an die Präsentationsschicht
weiterzuleiten, greifen die Komponenten der Anwendungsschicht auf die Datenhaltungsschicht
zu.

.. figure:: schichtenarchitektur.png
  :alt: schichtenarchitektur
  :scale: 50%
  :align: center

Pipe-Filter-Architektur
=======================

Das **Pipe-Filter-Architekturmuster** dient dazu, eine Anwendung zu strukturieren,
die Daten auf einem virtuellen Fließband verarbeitet.

Die einzelnen Verarbeitungsschritte werden in den sogenannten **Filtern** realisiert.
Ein Filter verbraucht und erzeugt Daten. Pipes leiten die Ergebnisse des Filters
an die nachfolgenden Filter weiter. Der erste Filter bekommt seine Daten aus der
Datenquelle und der letzte liefert sie and die Datensenke.

.. figure:: pipfilter_architektur_example.png
	 :alt: pipfilter_architektur_example

Zuerst müssen die einzelnen Verarbeitungsschritte identifiziert werden. Anschließend
sind Datenformate für je zwei durch eine Pipe verbundenen Filter festzulegen.
Kann man ein einheitliches Dateiformat für alle Filter festlegen, dann können diese
auch leicht neu kombiniert werden. Dadurch lassen sich leicht neue Verarbeitungseinheiten
realisieren.

Die Filter benutzen keine globalen Daten. Um trotzdem systematisch mit Fehlersituationen
umzugehen, wird eine gemeinsame Strategie für alle Filter benötigt. Diese muss
festlegen, wie die Daten im Fehlerfall weiter verarbeitet werden sollen.
Im schlimmsten Fall muss die Verarbeitung abgebrochen und vollständig wiederholt
werden.

Model-View-Controller Architektur (MVC)
=======================================
MVC gliedert eine interaktive Anwendung in drei Komponenten.

.. glossary::

  Model
    Das **Model** realisiert die fachliche Funktionalität der Anwendung. Es kapselt
    die Daten der Anwendung, stellt jedoch Zugriffsoperationen zur Verfügung, mit
    denen die Daten abgefragt und verändert werden können.

  View
    Eine **View** präsentiert dem Benutzer die Daten. Sie verwendet die Zugriffsoperationen
    des Models. Zu einem Model kann es für unterschiedliche Darstellungen derselben Daten
    beliebig viele Views geben.

  Controller
    Jeder :term:`View` ist ein **Controller** zugeordnet. Dieser empfängt die Eingaben
    des Benutzers und reagiert darauf. Muss nach der Interaktion die Visualisierung
    geändert werden, dann informiert der Controller die Views entsprechend. Wählt
    der Nutzer eine Funktion der Anwendung aus, dann ruft der Controller diese beim
    :term:`Model` auf.

.. figure:: mvc_architektur_struktur.png
	 :alt: mvc_architektur_struktur

View und Model sind allen Komponenten bekannt, der Controller ist es nicht.

Wenn Daten durch den Nutzer verändert werden, müssen die betroffenen Views
darauf reagieren. Um Änderungen des Models den Views mitzuteilen, verwaltet das
Modell einen **Register**, in dem sich alle Views, die das Model darstellen, eintragen
müssen. Ändert sich der Zustand des Models, so werden alle registrierten Views
informiert und können die Darstellung anpassen. Dieser Ablauf wird auch als
**Change-update-Mechanismus** bezeichnet.

.. figure:: mvc_oo_struktur.png
	 :alt: mvc_oo_struktur

.. figure:: mvc_sequenzdiagramm.png
	 :alt: mvc_sequenzdiagramm

Es ist möglich, für dasselbe Model mehrere View-Controller-Paare vorzusehen. Auf
Grund der sauberen Entkopplung vom Model können View-Controller-Paare sogar
zur Laufzeit  hinzugefügt oder weggenommen werden.

Der Change-update-Mechanismus führt dazu, dass das Model in allen Views immer
aktuell visualisiert wird. Wenn sich die Daten des Models sehr oft  und sehr schnell
ändern, kann das dazu führen, dass die Views diese Veränderungen nicht mehr schnell
genug anzeigen können, weil jedes Mal die Daten vom Model erfragt werden müssen.

The input comes in through the controller, the controller tells the model what
to do with the input.

Plug-in-Architekturmuster
=========================
Das Plug-in-Architekturmuster bietet allen Benutzern die Möglichkeit, ein System
an dafür vorgesehenen Punkten zu erweitern, ohne es zu modifizieren (:ref:`open-close-principle`).

Die Anwendung besteht aus einem **Host**, der durch **Plug-ins** um neue
Funktionen erweitert werden kann. Der Host definiert spezielle Schnittstellen, die
**Erweiterungspunkte**, auf die die Plug-ins Bezug nehmen können.

.. figure:: plugin_architecture.png
	 :alt: plugin_architecture

Damit ein Plug-in im Kontext des Hosts ausgeführt werden kann, muss es gemäß den vom
Host vorgegebenen technischen Konventionen realisiert sein, der Code muss entsprechend
abgelegt und das Plug-in beim Host registriert sein. Beim Start des Hosts werden die
aktuell vorhandenen Plug-ins identifiziert und entweder sofort oder bei Bedarf geladen.
Das Plug-in kann selbst ein Host sein, d.h. Plug-ins können Erweiterungspunkte für
weitere Plug-ins definieren.

Mobile First Architecture
=========================

- cache on client
- offload to client side computing
- Endpoint Computing
- backend only for coordination, backup and statistics

.. figure:: mobile_first.png
	 :alt: mobile_first

app types:

- single user apps: all the app is owned by and only accessible by a single user
  (but this  user may have multiple devices) -> all logic on device
- colaborative apps: The user has access to a large dataset, where parts are
  shared with other users (chat app)
- browser (data) apps: the user is mostly searching and browsing a larger set of
  date created by other users (youtube, twitter): not practical for endpoint computing


Challenge: two way sync (with handle conflicts) for cross device and cross user collaboration

Mobile Driven Design
--------------------
Start from device, building the best possible experience for the users. Then let
that shape the backend, rather than the other way around.

.. _strangler-application:

Strangler Application
=====================

source: https://www.martinfowler.com/bliki/StranglerApplication.html

The Strangler Application is a way of doing a rewrite of an important system.

You gradually create a new system around the edges of the old, letting it grow
slowly over several years until the old system is strangled. The fundamental strategy
is **EventInterception**, which can be used to gradually move functionality to the
strangler and to enable **AssetCapture**.

The most important reason to consider a strangler application over a
cutover rewrite is **reduced risk**. A strangle can give value steadily and the
frequent releases allow you to monitor the progress more carefully. Since you
have much shorter release cycles you can avoid a lot of unnecessary features that
cutover rewrites often generate.

When designing a new application you should design it in such a way as to make it
easier for it to be strangled in the future.

.. attention:: All we are doing is writing tomorrow's legacy code today.
