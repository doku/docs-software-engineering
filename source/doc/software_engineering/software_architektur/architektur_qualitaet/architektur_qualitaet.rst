===================
Architectur Quality
===================

.. figure:: design_stamina_hypothesis.png
	 :alt: design_stamina_hypothesis

	 design stamina hypothesis

https://martinfowler.com/bliki/DesignStaminaHypothesis.html

The usage of continuous delivery and rapid feature delivery increases the importance
of great internal quality.

.. figure:: internal_quality.png
	 :alt: internal_quality

.. note::
	Good Architectures **scream** their intended usage.

You should be able to look at the top-level architecture and see what that system
is (banking or trading etc.).

The structure of a system should show the use cases. When looking at the system
the delivery mechanism should not be apparent.

The purpose of a good architecture is to defer decisions, delay decisions.
The job of an architect is not to make decisions, the job of an architect is to
build structures that allows decisions to be delayed as long as possible. When
you make a decision early you have the minimum amount of information to make it.

A real architect spends time with the developers coding, so they know the hell they
have made for everybody.

Good architectures are not composed of tool and framework decisions. Good architectures
allow you to defer tool and framework decisions like: the database, the web server
and the dependency injection framework.

.. hint::
	A good architect build a structure that does not commit to major decisions.

	A good architect **maximizes** the number of decisions **not** made.

You do not need to start your project with all the tools working.

Don't let the application know about it's tools.

Design Rules
============

.. figure:: design_rules.png
	 :alt: design_rules
