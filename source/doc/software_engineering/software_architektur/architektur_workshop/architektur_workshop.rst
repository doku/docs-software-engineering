====================
Architektur Workshop
====================

Architektur Aufgabe: Aufzug und Artgerechte Haltung von Stakeholdern

Eberhart Rechtin: Systemarchitektur Buch

There are no technical problems.

Umsetzung begleiten und Reviews um Reparatur Kosten gering zu halten

Tipps
=====

#. Angemessenheit beachten (der Situation angemessen entscheiden)
#. Lerne die **Sprachen** deiner Stakeholder
#. Iteriere immer
#. Explizit statt implizit (sonst werden falsch Annahmen getroffen)

Adapter etc. machen Sachen nur komplizierter

geek and poke

Architekturagenten: Stefan Toth Vorgehensmuster für Softwarearchitektur


Ständiges Lernen, alle 2 Jahre Reflektieren, neu ausrichten

Architekten beraten das Management in Technischen Fragen.

Iterieren heißt Arbeit plus Feedback.
Wir breuchen Feedback, damit wir in der Nächsten Iteration besser entscheiden können.

Der Grund für Iterationen ist das Feedback welches man dadurch früh bekommt.

Patternbox

Kontextabgrenzung so früh wie möglich machen, als einfaches Diagramm entwerfen
und mit allen Stakeholdern durchsprechen und verifizieren.

Business at School: Boston Consulting


Hinsichtlich Verfügbarkeit, Korrektheite etc. unterscheiden sich die Schnittstellen
anforderungen der Externen Schnittstellen.



Make or Buy: Kerngeschäft selbst, standard Module kaufen

Look for partners who do the undifferentiated heavy lifting (work that needs to
be done but having it done doesn't bring the customer direkt benefit). 

Kontextabgrenzung wesentliche Architektursicht

Programmieren Fähigkeit ist eine notwendige aber nicht hinreichendes Kriterium
für Softwarearchitektur. Fehlende: Kommuniktion

Ein Pullrequest ist die Explizite Aufforderung von Feedback.

Qualität ist eine Summe verschiedener Eigenschaften. (die sich gegenseitig beeinflussen
und gegeneinander abgewägt werden müssen).

Qualitätsszenarien präzesieren Qualitätsanforderungen.


Bausteinschicht zeigt Aufbau und Sourcecode Struktur. Laufzeitsich erklärt des Verhalten

Bei architekur bescheibung: trenne erkläreihenfolge von der durchführungsreihenfolge


Domaine = Sachbereich, Fachbereich

Mit der Domaine Verdienen Unternehmen Geld, es entsteht mehr wert
Die Domaine bleibt über lange Zeit eher Konstant

In der Domaine steckt die **inherente Komplexität**, die Fachliche Schwierigkeit kann
nicht vereinfacht werden.

Nur Technische Komplexität kann vereinfacht werden.

Fachliche Aspekte von technischen trennen

Durch Usecase Clustering bekommt man sehr schnell sehr langlebige strukturen.

Technical Depth
===============

.. hint::
  Only improve, if the problem cost are worse than the improvement cost.

Patterns: Circut Braker Pattern

Heterogene Architekturstile
===========================
in der Realen Systemen verwendet man eine Mischung verschiedener Architekturmuster


Ein Provider sollte einen Consumer niemals zwingen etwas über interne Abläufe
und Datenstrukturen zu wissen.

Vererbung ist in Punktung Kopplung, das schlimmste was man machen kann, da man
den gesamten Implementierungsballast mitbekommt.

Kopplung ist viel schwieriger, als

Kopplung über:

- Datenbanken
-

Man kann nicht alle Kopplung im Source Code sehen.


Kohesionskriterien

Mögliche Kohesionskriterien sollten explizit gemacht werden



Sichten
=======

Bausteinsicht
-------------
Die Bausteinsicht zeigt die statische Struktur des Systems.

Laufzeitsicht
-------------
Die Laufzeitsicht zeigt den zeitlichen Ablauf der Instanzen von Bausteinen.

Verteilungsschicht
------------------


Iterationen mit Perspektivenwechsel ist die Rettung aus komplexen
Entwurfssituationen.
Den Perspektivwechsel kann man auch explizit auf verschiedene Köpfe aufteilen.
