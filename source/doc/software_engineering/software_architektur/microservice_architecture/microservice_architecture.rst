=============================
Introduction in Microservices
=============================

.. glossary::

	Microservices Architecture
		The microservice architectural style is an approach to developing a single application
		as a suite of small services, each running in its own process and communicating
		with lightweight mechanisms, often an HTTP resource API. These services are
		built around business capabilities and independently deployable by fully
		automated deployment machinery. There is a bare minimum of centralized
		management of these services, which may be written in different programming
		languages and use different data storage technologies. (James Lewis and Martin Fowler)

Each Microservice offers a Service to other Microservices. We create applications
by configuring and reconfiguring these microservices.

**Microservices** is just an architectural approach to design the applications in a
way that is modular, easy to deploy and scale independently.

Years ago we had massive applications with millions of lines of code. This led to
extremely long build times and deployment times. So typically software was releases
relatively infrequently because releasing a new version and updating meant a lot
of work.

Recent software applications are broken in smaller chunks.

These small applications wrapped in containers take seconds as supposed to ours
to deploy. So releasing new features is a lot cheaper and can be done as soon
as the code passes quality control.

The main reason for microservices is to speed up development cycles.
If you have a lot of people contributing to a single large piece of code, any small
thing (like a bug) that blocks its release is blocking a huge amounts of work
from reaching your costumers.

Breaking the application up into microservices allows everybody to run at their
own speed. So a brand new feature can have ten releases a day while a very mature
one night be updating once a month.

To ensure compatibility across releases you have to make sure that the interfaces
of each microservice is relatively stable. In that way you can deploy any piece
of it independently of any other piece.

The entire process (build, deploy and rollback) has to be automated.

.. danger::
	Microservices are much harder to run in production. They add significant levels
	of complexity in deployment, monitoring and failure pervention/ recovery.

If you start out building a microservice app chances are that you will get your
service boundaries wrong. So start by building a clean monolith. As you get a better
understanding of your domain you can start to breake some of these services out.
(Rebecca Parsons)

.. hint::
	Microservices can be implemented using serverless or containerbased technology.
	While microservice architecture is the decomposition of application in small,
	reusable services providing business services through an api, serverless and
	container are a technology approach to implement this functional decomposition.

Twelve-Factor Apps
==================
The **Twelve-Factors** are best practices for building deployable
software-as-a-service (SaaS) apps.

- Portable
- Continually Deployable
- Scaleable

1. **Codebase**: One codebase tracked in revision control, many deploys
2. **Dependencies**: Explicitly declare and isolate dependencies
3. **Config**: Store all Configuration files in the environment
4. **Backing services**: Treat backing services as attached resources
5. **Build, release, run**: Strictly separate build and run stages
6. **Processes**: Execute the app as one or more stateless process
7. **Portbinding**: Export services via port binding
8. **Concurrency**: Scale out via the process model
9. **Disposability**: Maximize robustness with fast startup and graceful shutdown
10. **Dev/prod parity**: Keep development, staging and production as similar as possible
11. **Logs**: Treat logs as event streams
12. **Admin process**: Run admin/management tasks as one-off process

Microservices-Architektur
=========================

In the Microservice approach each capability of an application an puts them in
a separate process. Instead of one process you have a network of communicating
processes. The network must be dumb, all the smarts must live in the application.

Microservices is a subset of the Service-Oriented Architecture (SOA).

Your services should be stateless (except the persistence/ cashing layers) and
must not rely on sticky sessions. Users have to be able to switch instances of
your service on the fly in case one fails. This can be proven by Chaos testing
(:ref:`ref-destructive-test`).

Adding instances allows you to scale horizontal.

.. figure:: microservice_structure.png
	 :alt: microservice_structure

Common Characteristics of Microservices

#. Componentization via services
#. Organized around business capabilities
#. Products not Projects
#. Smart endpoints and dumb pipes
#. Decentralized Governance
#. Decentralized Data Management (Every Service is responsible for its own persistence)
#. Infrastructure Automation
#. Design for failure
#. Evolutionary Design

Languages and tools can be individual chosen for each Microservice based on its
own need.

Microservices allow a partial deployment and have high availability. If the recommendation
service goes down, your shopping chart service is still functional.

For successful Microservies you need **rapid Provisioning**, **basic monitoring**
and **rapid application deployment**.

A common language between the services is required to allow each service to talk
to ever other.

Microservices at scale require organizational change and centralized infrastructure
investment.

Replaceable Component Architecture
==================================

.. figure:: replaceable_component_architecture.png
	 :alt: replaceable_component_architecture

You can reason a component purely based on the messages it consumes and
produces. That means you can test a component purely based on the messages it
produces and consumes.

Microservices can be a Replaceable Component Architecture if you choose to optimize
for replaceability and consistency. Smaller is not necessarily better, more
replaceable is better.

JWT
===

Json Web Tokens

Clients can not be trusted
