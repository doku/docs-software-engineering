=======
Entwurf
=======

Wenn in einem großen System immer wieder Komponenten verändert oder ausgetauscht
werden, ist nach einigen Jahren kaum noch eine Zeile Code des ursprünglichen Systems
enthalten, aber die Struktur ist völlig unverändert. Da die Software-Struktur großen
Einfluss auf die Brauchbarkeit (Effektivität), vor allem aber auf die Wartbarkeit hat,
ist die Wahl einer sinnvollen Struktur die wichtigste (technische) Entscheidung
der ganzen Software-Entwicklung.

Der Entwurf eines Systems hat drei Ziele:

- Gliederung des Systems in überschaubare (handhabbare) Funktionen
- Festlegung der Lösungsstruktur
- Hierarchische Gliederung

Die Architektur sollte dauerhaft und prüfbar dokumentiert werden, denn sonst ist es
unmöglich, sie zu diskutieren und zu prüfen. Im Laufe der Wartung besteht die Gefahr,
dass die Architektur durch ein eine Vielzahl kleiner Modifikationen schleichend
korrumpiert wird und dadurch nicht nur Klarheit und Verständlichkeit einbüßt, sondern
sich auch von ihrer Beschreibung entfernt, in der die Änderungen meist nicht nachgeführt
werden. Damit verliert die Architekturbeschreibung ihren Wert für die Wartung der
Software.

.. note::

    Die statische Struktur lässt sich später nur mit sehr hohem, meist unangemessenem
    Aufwand verändern. Was als romanische Basilika gebaut wurde, wird niemals zu einer
    freitragenden Messehalle.

Der Entwurf sollte möglichst einfach gehalten werden.

Aus Sicht der funktionalen Anforderungen ist der Entwurf beliebig wählbar.

.. figure:: einfuesse_auf_entwurf.png
	 :alt: einfuesse_auf_entwurf


.. todo:: model-code cap
