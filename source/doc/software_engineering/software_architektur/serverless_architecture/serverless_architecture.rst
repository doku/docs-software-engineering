=======================
Serverless Architecture
=======================

.. figure:: sys_architecture.png
	 :alt: sys_architecture

On the left, is a traditional web application architecture. A client talks
linearly to the stack and the web servers are running any number of services.
On the right, in a serverless architecture, you decompose the services into
functions with no central web server to speak of, as all of it's roles and
duties are distributed.

This is an implicit microservices approach. Serverless hinges on functions as a
service. These are Amazon Lambda, Google Cloud Functions, and Azra functions.
Under the hood, these functions are containers that get spun up in milliseconds
in a read only file system. They execute the event that is given to them and
then they disappear.
Serverless encourages functions as deploy units, coupled with third-party
services that allow running end-to-end applications without worrying about
system operation.

- scaling is built in
- pay for compute in 100ms increments (server overalloction is a non issue)
- no management of servers (or containers)

.. glossary::

  serverless architecture
    A **severless architecture** approach replaces long-running virtual machines
    with ephemeral compute power that comes into existence on request and
    disappears immediately after use.

.. figure:: evolution_serverless.png
	 :alt: evolution_serverless

- Virtual machines abstract the hardware
- Containers abstract the OS
- Serverless abstracts the language runtime

No longer are we dealing with the notion of having to provision some type of
server for most operations on cloud-based platforms.

We only define the behavior within the function and when the function gets invoked
it automatically takes the resources it needs.

Mircoservices and Serverless
============================

Microservices have a event

We take Microservices and embedd them into our functions

Principles of Serverless Architecture
=====================================

#. Use a compute service to execute code on demand
#. Write singe-purpose stateless functions
#. Design push-based, event-driven pipelines (one event triggers another event)
#. Create thicker, more powerful front ends
#. Use third party services

Benefits of Serverless architecture
===================================

- no more servers to manage (maintenance etc.)
- Rapid time to market
- Scale effortlessly (compensate for extreme traffic spices)
- lower cost (you only pay when you actually use resources)
- Applications can be gradually be migrated to serverless

Serverless Computing is a great fit for applications that are developed
net new, benefit from scalability and can be designed service oriented.

Vendor lock-in has to be considered.

Event-programming model
=======================
Serverless makes it easy to develop event driven applications

AWS Lambda
==========
when building serverless applications on AWS Lambda, the core components are
Lambda Functions and event sources.

You define a function which will be executed based on a event (such as http-request).

You only get charged for what you actually utilize. You are not paying for idling
servers.

.. figure:: media_encoding_pipeline.png
	 :alt: media_encoding_pipeline
