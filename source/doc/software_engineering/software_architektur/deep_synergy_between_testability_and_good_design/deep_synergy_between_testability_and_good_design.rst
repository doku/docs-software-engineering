================================================
Deep Synergy between Testability and Good Design
================================================

Source: https://www.youtube.com/watch?v=4cVZvoFGJTU

Many of the fundamental things we do to make design better, also improves testability.

Every time you encounter code that is poorly testable, there is a underlying
design problem.

Solving Design

.. hint::
  Making Code Testable does not necessarily make the Design better.

Testing Problems:

- State hidden in Method: You find yourself wishing you could access local variables
  in tests: SRP, too long method
- Difficult Setup: Instantiating a class involves instantiating 12 others: Too much Coupling
- Incomplete Shutdown: Pieces of your code lose resources and you never realize
  it until you test. (Classes Don't take care of themselves, poor encapsulation)
- State-leaks across tests: The state of one test affects another (Singletons, or
  other forms of global mutable state.)
- if you have insufficient domain separation, testing in the presence of a framework
  is hard.
- A violation of the :ref:`law-of-demeter` makes mocking difficult. You will find your
  self writing mocks for objects returned by other objects.

Testing makes the pain of global state very real to you.

The pain we feel when writing tests shows us areas where we can improve our design.


When testing we feel the concrete pain of our bad design decisions. The problem
is not that testing is painful, but more that we need to improve our design.

Testing isn't hard. Testing is easy in the presence of good design.

WHY
===

Test are another way of understanding our code. They make us work for that
understanding. The things that make it hard
to understand code mentally also make it hart to check these things
in an automated fashion.

Something that is easy to test has a well defined description. A test destinctly 
describes the behavior.
