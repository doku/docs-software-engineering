.. _ref-design-pattern:

===============
Design Patterns
===============

.. glossary::

	design pattern
		A design pattern names, abstracts and identifies the key aspect of a common
		design structure that makes it useful for creating a reusable object-oriented
		design. (Gamma et al.)

Contrary to **best practices**, **patterns** depend on the context. Best practices
are context free.

You have to specify where a design pattern applies and more importantly where
it doesn't apply.

What is a Pattern?
==================

Each pattern is a three-part rule, which expresses a relation between a certain
context, a problem, and a solution.
-- Christopher Alexander

Once is an event,  twice is an incident,  thrice it's a pattern.
-- Jerry Weinberg

Each pattern is a three-part rule, which expresses a relation between a certain
context, a certain system of forces which occurs repeatedly in that context,
and a certain software configuration which allows these forces to resolve themselves.
-- Richard P. Gabriel


.. _lazy-instantiation:

Lazy instantiation
==================

Lazy Instantiation/Evaluation is just an performance optimization.

Object Pool
===========

Model-View-Controller
=====================

Neither controller nor view should know about the business objects.

MVC is a low level structure.

.. _special-case-pattern:

Special Case Pattern
====================
You create a class or configure an object so that it handles a special case
for you. When you do, the client code doesn't have to deal with exceptional
behavior. That behavior is encapsulated in the special case object.

Reurseion
=========

.. code-block:: ruby

  fac:


Extension Object
================


Half object + Protocol (HOPP)
=============================

Mobile code
