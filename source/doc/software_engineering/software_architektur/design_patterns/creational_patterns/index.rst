===================
Creational Patterns
===================

.. toctree::
  :glob:

  creational_patterns/*
  singleton/*
  null_object_pattern/*
  builder_pattern/*
  factory_pattern/*
