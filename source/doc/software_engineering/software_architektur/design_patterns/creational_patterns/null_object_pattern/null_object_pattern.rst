===================
Null Object Pattern
===================

The Null object pattern describes how to build an Object that should do nothing

The null object pattern tries to eliminate null checks. The goal is to push
out all the special behavior in case something is null to a class representing
a null instance of a given type. Instances of this class know how to act or
display themselve. Instead of returning null in your application you return an
instance of the null object of an specific interface.

This prevents you from null checking and removes special case treatment in your
application.

.. hint::

	The null object pattern is a polymorphic approach to make special cases implicit
	rather than expicit.

.. figure:: null_object_uml.png
	 :alt: null_object_uml

Null Object as Singleton
========================

Implementing a null object as :ref:`singleton` has a couple of design advantages.

If you have two versions of null object, they should be identical (don't waste
memory by having nothingness repeated). This has a big advantage for equality
checks, because they can be compared by reference.


Beispiele
=========

Smalltalk:

Nullstream
NoController
