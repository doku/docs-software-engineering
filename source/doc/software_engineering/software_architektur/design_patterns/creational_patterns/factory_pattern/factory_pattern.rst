===============
Factory Pattern
===============

.. _abstract-factory-pattern:

Abstract Factory Pattern
========================
The purpose of the Abstract Factory is to provide an interface for creating
families of related objects, without specifying concrete classes.

The Abstract Factory defines a Factory Method per product. Each Factory Method
encapsulates the ``new`` operator and the concrete, platform-specific, product
classes. Each platform is then modeled wit a Factory derived class.



Factory Method
==============
