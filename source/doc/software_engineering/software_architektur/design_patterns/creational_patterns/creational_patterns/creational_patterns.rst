===================
Creational Patterns
===================

Creational Patterns abstract the instantiation process.

Creational Patterns give you a lot of flexibility in  *what* gets created, *who*
creates it, *how*  it is created and *when*
