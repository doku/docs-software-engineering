.. _singleton:

Singleton Pattern
=================
Es soll sichergestellt werden, dass zu einer Klasse höchstens eine Instanz gibt.
Diese soll global verfügbar sein, alle Objekte haben also die Möglichkeit diese
Instanz anzusprechen.

.. attention::
	Singletons are just the object-oriented spinn of global variables. They cause
	global state, which is a bad design choice.

Die mehrfache Instanziierung soll verhindert werden.

Im Singleton Pattern ist die Klasse selbst dafür verantwortlich, dass es von ihr
nur ein Exemplar gibt. Sie stellt diese Instanz zur Verfügung.

.. code-block:: java

	class Singleton {

		private static Singleton instance = null;

		// only private Constructors
		private Singleton () {

		}

		public static getInstance () {
			if (Singleton.instance == null) {
				instance = new Singleton();
			}
			return instance;
		}

	}
