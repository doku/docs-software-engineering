===================
Structural Patterns
===================

.. toctree::
  :glob:

  composite_pattern/*
  adapter_pattern/*
  decorator_pattern/*
  facade_pattern/*
