=========
Composite
=========

Es soll ein hierarchisches Teil-Ganzes-Beziehungen modelliert werden.

Primitive objects and container objects use the same interface.

.. figure:: composite_example.png
	 :alt: composite_example

The Composite Pattern allows clients to ignore the difference between compositions
of objects and individual objects. Clients will treat all objects in the composite
structure uniformly.
