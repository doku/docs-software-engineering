=====
State
=====

Ein Objekt soll sich abhängig von seinem Zustand sehr verschieden verhalten.

Der Zustand wird als Klasse gekapselt. Spezielle Zustands Klassen mit überladenen
Methoden implementieren die jeweiligen Zustandsabhängigkeiten.

.. figure:: state_example.png
	 :alt: state_example
