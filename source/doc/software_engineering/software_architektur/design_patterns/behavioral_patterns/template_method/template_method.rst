.. _template-method-pattern:

=======================
Template Method Pattern
=======================

Uses inheritance to replace parameterized portions of an algorithm at compile
time... in contrast to :ref:`strategy-patterrn`, which uses dynamic delegation to replace entire
algorithms at run-time. The template is a concrete method in an abstract class;
it invokes abstract methods that are given different concrete realizations by
subclassing, thus fleshing out the algorithm template in different ways for
different subclasses.

The Template Method lets subclasses redefine certain steps of an algorithm without
changing the algorithm's structure.

.. code-block:: java

	public class VacatioPolicy {
		public void accureUSDivisionVatation {
			//calculate vacation based on hours worked to date

			//ensure vacation meets US minimums

			//apply vacation to payroll record
		}

		public void accureEUDivisionVatation {
			//calculate vacation based on hours worked to date

			//ensure vacation meets EU minimums

			//apply vacation to payroll record
		}
	}

The code across the two methods is largely the same, with the exception of
calculating legal minimums. This duplication can bee removed using the Template
Method pattern.

.. code-block:: java

	abstract public class VacationPolicy {
		public void accureVacation() {
			calculateBaseVacationHours();
			alterForLegalMinimus();
			applyToPayroll();
		}

		private void calculateBaseVacationHours() { /*...*/}
		abstract protected void alterForLegalMinimus();
		private void applyToPayroll() { /*...*/}
	}

	public class USVacationPolicy extends VacationPolicy {
		@Override
		protected void alterForLegalMinimus(){
			//US Specific logic
		}
 	}

	public class EUVacationPolicy extends VacationPolicy {
		@Override
		protected void alterForLegalMinimus(){
			//EU Specific logic
		}
 	}

The subclasses supply the only information that is not duplicated.
