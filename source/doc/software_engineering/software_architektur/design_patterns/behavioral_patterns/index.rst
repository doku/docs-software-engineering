===================
Behavioral Patterns
===================

.. toctree::
  :glob:

  template_method/*
  state_pattern/*
  observer_pattern/*
  strategy_pattern/*
  visitor_pattern/*
