===============
Visitor Pattern
===============

Source: https://sourcemaking.com/design_patterns/visitor

Represent an operation to be performed on the elements of an object structure.
Visitor lets you define a new operation without changing the classes of
the elements on which it operates.

The Visitor's primary purpose is to abstract functionality that can be applied to
an aggregate hierarchy of "element" objects.

.. figure:: visitor_pattern.png
	 :alt: visitor_pattern
