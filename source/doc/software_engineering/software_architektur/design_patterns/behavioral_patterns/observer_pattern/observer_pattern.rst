================
Observer Pattern
================

The intent is t define a one-to-many dependency between objects so that when
one object changes state, all its dependents are notified and updated
automatically.

.. figure:: observer_pattern_structure.png
	 :alt: observer_pattern_structure

One ``subject`` can have many ``observer``. All  ``observers`` are notified,
when the object changes state.


Example
=======

Gibt in Smalltalk zwei Implementierungen einmal in Object und einmal in Model.

bei Object -> ein Dictionary
beim Model ein Array von

.. code-block:: ruby

  s := SWASubject new.
  s number: 32.
  o := SWAObserver new.
  s addDependent: o
  s removeDependent:o

  self changed
