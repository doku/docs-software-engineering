.. _strategy-pattern:

================
Strategy Pattern
================

Define a family of algorithms, encapsulate each one, and make them interchangeable.
Strategy lets the algorithm vary independently from the clients that use it.

.. figure:: strategy_pattern.png
	 :alt: strategy_pattern

The ``strategy`` declares a common interface to all supported algorithms.
