========
Overview
========

Null Object
===========

Beim Null Object Pattern wird anstelle von ``null`` eine Referenz zu einer Instanz
einer Nullobjekt Implementierung übergeben. 
Dadurch kann das explizite Behandeln von ``null`` Sonderfällen implizit gelöst
werden.

Singleton
=========
Das Singelton Pattern stellt sicher, dass es von einer Klasse nur eine Instanz
gibt und ermöglicht globalen Zugriff auf diese Instanz.

Abstract Factory
================
Die Abstrakte Factory wird verwendet, um Clients eine Familie von zusammengehörenden
Objekten bereitzustellen, ohne deren konkrete Klasse zu spezifizieren.

Composite
=========
Das Composite Pattern ordnet Objekte in eine Baumstruktur und modelliert eine
hierarchische Teil-Ganze Beziehung. Die Clients können individuelle Objekte und
Compsites von Objekten gleich behandeln. Der Kern des Composite Patterns ist eine
abstrakte Klasse welche Primitive und ihre Containar repräsentiert.

State
=====
Das State Pattern erlaubt Objekten ihr Verhalten zu ändern, wenn sich der interne
Zustand ändert. Das Objekt erweckt nach außen den Eindruck, als würde es seine Klasse
ändern.

Strategy
========
Es wird eine Familie von Algorithmen definiert die alle ein gemeinsames Interface
implementieren.
Das Strategy Pattern erlaubt Clienten dynamisch den Algorithmus zu wechseln.

Observer
========
Das Observer Pattern definiert eine one-to-many Beziehung zwischen Objekten,
sodass falls ein Objekt seinen Zustand ändert, alle abhängigen Objekte
darüber informiert werden und automatisch geupdated werden.

Visitor
=======
Ein Visitor repräsentiert eine Operation, welche auf einem Element einer
Objektstruktur ausgeführt werden soll.
Das Visitor Pattern erlaubt es neue Operationen zu definieren, ohne die Klassen
der Objekte zu ändern auf welchen diese Operationen ausgeführt werden sollen.

MVC
===

Bei MVC wird die Anwendung in die drei Komponententypen Model, View und
Controller aufgeteilt.
MVC entkoppelt die programminterne Repräsentation von Informationen (Model) von der Art
in welcher sie dem Nutzer präsentiert wird (Views).
