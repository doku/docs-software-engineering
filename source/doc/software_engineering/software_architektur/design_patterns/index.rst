===============
Design Patterns
===============

.. toctree::
  :glob:

  patterns/*
  overview/*
  creational_patterns/*
  structural_patterns/*
  behavioral_patterns/*
