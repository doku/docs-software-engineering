========================================
Entity Interceptor Boundary Architecture
========================================

.. figure:: entity_interactor_boundary_model.png
	 :alt: entity_interactor_boundary_model

You can divide your objects in three types.

Entities have application independent business rules.

Boundaries are interfaces that allow you to hand data structures across the
user interface to interactor boundary. Your data structures come through the
boundaries into the interactor, the interactors talk to the entities and then
they hand data structures back through the boundaries independent of the concrete
UI implementation.

.. hint::
	The use cases are the central organizing principles of your application
	and the UI is just an appendix, which makes it easy to remove when they
	get inflamed.

Interactors have application specific business rules. Integrators control the
business objects in a particular way to achieve the goals of the application.

Entities are business objects that are application independent. An entity has
application independent business rules, which means that you can use the entities
in many different applications.

For every use case you have an different interactor. The boundary interfaces are
used by the delivery to pass data into the interactor and for the interactor
to pass data out. Interactors have the name of their use case in them. Typically
there are many entities the interactor talks to.

The user talks to the delivery mechanism. The delivery mechanism will use a
boundary interface which the interactor implements in order to receive the
incoming data structure. The interactor will use another boundary interface
which the delivery mechanism implements in order to receive the result data
structure.

The delivery mechanism constructs a data structure from the user input. This data
structure is pure data and called a request model.

You can test all your use cases though that interactor interface.

Database (persistence layer) and presentation are **plugins** to your application.

UIs have to be removable and replaceable with a new one.

The application does not depend on the delivery. The delivery system should be a
swappable plugin to the application.

.. hint::
	These practices allow you to easily test your application using mock objects, resulting
	in very fast test executions.

A use case object is an object that takes in a data structure and emits another
data structure. These objects are very testable. You can put all of your business
rules in use case objects and write them as objects that take in data structures,
manipulate them according to your business rules and output new data structures.
In that way you can easily test all use cases in your system.
