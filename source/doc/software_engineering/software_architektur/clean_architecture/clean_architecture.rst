==================
Clean Architecture
==================

Programming Paradigms
=====================

.. note::

    - Structured programming is discipline imposed upon direct transfer of control
    - Object-oriented programming is discipline imposed upon indirect transfer of control.
    - Functional programming is discipline imposed upon variable assignment.

OO is the ability, through the use of polymorphism, to gain the absolute control
over every source code dependency in the system.

Functional Programming
----------------------
All race conditions, deadlock conditions and concurrent update problems are due to
mutable variables.

Segregation of Mutability is one of the most common compromises in regard to immutability.
Well-structured applications will be segregated into those components that do not mutate 
variables and those that do. 

Event sourcing is a strategy wherein we store the transactions, but not the state. When 
state is required, we simply apply all the transactions from the beginning of time.

Component Principles
====================

Component Cohesion
------------------

The Reuse/Release Equivalence Principle (REP)
    The Granule of reuse is the granule of release. Classes and modules that are grouped
    together into a component should be releasable together. The fact that they share the same
    version number should make sense to the author and the user. 

    People who want to reuse software components cannot, and will not, do so unless those components
    are tracked through a release process and release numbers. 

The Common Closure Principle (CCP)
    Gather together those things that change at the same time and for the same reason. Separate 
    those things that change at different times and for different reasons. (Single Responsibility Principle on component level)

The Common Reuse Principle (CRP)
    Don't force users of a component to depend on things they don't need.
    Thus when we depend on a component, we want to make sure we depend on every class in the component.
    The CRP is a generic version of the Interface Segregation Principle (ISP)

Component Coupling
------------------

.. glossary:: 

    Acyclic Dependencies Principle
        Allow now cycles in the component dependency graph. 
        You have to partition the development environment into releasable components.

    Stable Dependencies Principle (SDP)
        Depend in the direction of stability. 
        Some of our components are designed to be volatile. We expect them to change. 
        Any component that we expect to be volatile should not be depended on by a component
        that is difficult to change. Otherwise, the volatile component will also be difficult
        to change. One sure way to make a software component difficult to change, is to make lots
        of other components depend on it. 

    Stable Abstractions Principle (SAP)
        A component should be as abstract as it is stable.

        A stable component should also be abstract so that its stability does not prevent it
        from being extended. An unstable component should be concrete since it its instability
        allows the concrete code withing to be easily changed.

Stability Metrics
.................

.. glossary::

    Fan-in
        Incoming dependencies. This metric identifies the number of classes outside of this component
        that depend on the class within the component.

    Fan-out
        Outgoing dependencies. This metric identifies the number of classes inside the component that
        depend on classes outside of the component.

    Instability
        :math:`I=Fanout / (Fanin + Fanout)` This metric has the range of :math:`[0,1]`. :math:`I=0` indicates a maximally
        stable component. :math:`I=1` indicates a maximally unstable component.

The :term:`Fan-in` and :term:`Fan-out` metrics are calculated by counting the number of classes outside the component
in question that have dependencies with the classes inside the component in question.

The :term:`Stable Dependencies Principle` says that the :math:`I` metric of a component should be larger
than the :math:`I` metrics of the components it depends on. That is, :math:`I` metrics should decrease in
the direction of dependency. A violation of this can be fixed by employing the Dependency Inversion Principle.

The :term:`Stable Dependencies Principle (SDP)` and :term:`Stable Abstractions Principle (SAP)` result 
in the DIP for components. This is true because the SDP says that dependencies should run in the direction
of stability, and the SAP says that stability implies abstraction. 
Thus **dependencies run in the direction of abstraction**.