.. _architecture-principles:

=====================
Architekturprinzipien
=====================

Eine Software-Architektur ist gut, wenn die
**funktionalen und nichtfunktionalen Anforderungen** erfüllt werden können.

.. figure:: architektur_prinzipien.png
	 :alt: architektur_prinzipien

Die Modularisierung steht im Zentrum, die anderen Prinzipien tragen dazu bei,
den Prozess der Modularisierung effektiv zu unterstützen.

Die konzeptuelle Grundlage der Entwurfsprinzipien ist die Abstraktion. Indem
wir **Abstraktionen** bilden, konzentrieren wir und auf das Wesentliche und
blenden das Unwesentliche aus. Es ist eine wichtige Fähigkeit jedes guten Architekten,
in Abstraktionen zu denken und  Abstraktionen zu bilden.

.. hint::

  Resilience, security, scalability, deployability, testability are
  architectural concerns.

.. danger::
	The idea that there is a perfect architecture for your system is false and a great
	source of misery. Every stage of evolution of an organization requires a different
	architecture. Your architecture will always be evolving.

	Go to your customers as early as you can, find out what features the would request
	and then build the abstractions in your application that protect you from those
	changes.

.. note:: Distribution and Asynchronity are huge complexity drivers.

.. todo:: Consistency vs. Availability

Modularisierung
===============
Das Aufteilen des Systems in seine Komponenten wird als Modularisierung bezeichnet.

.. glossary::
  modular decomposition
    The process of breaking a system into components to facilitate design and
    development; an element of modular programming. (IEEE 610.12)

  modularity
    The degree to which a system or computer program is composed of discrete
    components such that a change to one component has minimal impact on other
    components. (IEEE 610.12)

Modularität ist eine Eigenschaft der Architektur. Sie ist hoch, wenn es dem Architekten
gelungen ist, das System so in Komponenten zu zerteilen, dass diese möglichst
unabhängig voneinander verändert und weiterentwickelt werden können.

Die Struktur eines Moduls soll einfach und leicht verständlich sein. Die Implementierung
eines Moduls soll austauschbar sein. Informationen über die Implementierung
der anderen Module sind dazu nicht erforderlich. Die anderen Module sind vom
Austausch nicht betroffen. Die Module sollen so entworfen werden, dass die
wahrscheinlichsten Änderungen ohne Modifikation der Modulschnittstellen durchgeführt
werden können. Große Änderungen sollen sich durch eine Reihe kleiner Änderungen
realisieren lassen. Solange die Schnittstelle der Module nicht verändert sind,
soll es möglich sein, alte und neue Modulversionen miteinander zu testen.

.. figure:: modularten_bsp.png
	 :alt: modularten_bsp

.. glossary::

  Funktionale Module
    Funktionale Module gruppieren Berechnungen, die logisch zusammengehören. Sie haben
    kein *Gedächtnis*, also keinen variablen Zustand. Die Wirkungsweise der an der
    Schnittstelle angebotenen Berechnungsfunktionen sind nicht abhängig vom
    vorherigen Programmablauf. Beispiele für solche Module sind Sammlungen von
    mathematischen Funktionen oder Transformationsfunktionen.

  Datenobjektmodule
    Datenobjektmodule realisieren das Konzept der Datenkapselung. Ein Datenobjektmodul
    versteckt dazu Art und Aufbau der Daten und stellt an seiner Schnittstelle
    Operationen zur Verfügung, um die gekapselten Daten zu manipulieren. Beispiele
    sind Module, die globale Konfigurationsdaten kapseln, und Module, die gemeinsam
    benutzte Datenverwaltung realisieren. Auch jede Datenbank ist ein Datenobjektmodul.

  Datentypmodule
    Datentypmodule implementieren einen benutzerdefinierten Datentyp in Form eines
    abstrakten Datentyps. Dadurch ist es möglich beliebig viele Exemplare des Datentyps
    zu erzeugen und zu benutzen.

Bei einem objektorientierten Entwurf entspricht die Klasse dem Datentypmodul
(:term:`Datentypmodule`). Ein Datenobjektmodul (:term:`Datenobjektmodule`)
entspricht einer Klasse, die lediglich Klassenmethoden
anbietet und von der keine Objekte erzeugt werden können (:ref:`singleton`).




Highlevel Concepts should be immune to changes from low level modules.

Kopplung und Zusammenhalt
=========================
Bei der Software-Architektur versucht man die Module so zu entwerfen, dass die
(inter-modulare) Kopplung (d.h. die Breite und Komplexität der Schnittstellen
zwischen den Modulen) möglichst gering, der (intra-modulare) Zusammenhalt
(d.h. die Verwandtschaft zwischen den Bestandteilen eines Moduls) möglichst hoch
wird.

.. hint::

  Man kann die Module auch mit Knödeln vergleichen, die beim Kochen dazu neigen,
  zusammenzukleben oder zu zerfallen. Ideal sind Module die nicht verkleben (geringe
  Kopplung) und nicht zerfallen (hoher Zusammenhalt).

.. glossary::

	Coupling and Cohesion
		*Coupling* and *cohesion* are metrics that describe how easy it will be to change
		the behavior of some code.

	loose coupling
		Vernetzung der Komponenten mit möglichst schmaler Schnittstelle (wenige Methoden).
		Aufrufe sollten nur selten statt finden.

		Elements are *coupled* if a change in one forces a change in the other. *Loosely coupled*
		features are easier to maintain.

	Zusammenhalt
		Jedes Modul nur die eigenen Aufgaben erledigen.

		An element's *cohesion* is a measure of whether its responsibilities form a meaningful
		unit. For example, a class that parses both dates and URLs is not coherent, because
		they're unrelated concepts. Features with *high coherence* are easier to maintain.


Ziel geringe Kopplung bei hohem Zusammenhalt.

.. figure:: kopplung_zusammenhalt.png
	 :alt: kopplung_zusammenhalt

Eine hohe Kopplung bewirkt, das Korrekturen und Änderungen über mehrere Einheiten
verteilt sind und dadurch die Wartung entsprechend aufwändiger und unsicherer ist.
Eine Einheit mit geringem Zusammenhalt kann ohne Nachteile weiter aufgespalten werden,
und wäre dann leichter und sicherer zu verstehen, zu korrigieren und zu warten.

.. note::

  Geringe Kopplung und hoher Zusammenhalt bewirken also gleichermaßen hohe
  Lokalität und damit gute Wartbarkeit.

Klassen sollten untereinander nur lose gekoppelt sein und jeweils aus Methoden
bestehen, die starken Zusammenhalt haben.

.. figure:: coupling_cohesion_metriken.png
	 :alt: coupling_cohesion_metriken

Arten der Kopplung
------------------

#. Datenkopplung: Zwei Module kommunizieren über elementare Daten, beispielsweise
   einfache Parameter

   .. figure:: Datenkopplung.png
	   :alt: 	 datenkopplung

#. Datenstrukturkopplung: Zwei Module kommunizieren über eine komplexe
   Datenstruktur, von der aber nur ein Teil verwendet wird

   .. figure:: datenstrukturkopplung.png
	    :alt:    datenstrukturkopplung

#. Kontrollkopplung: Ein Modul nimmt Einfluss auf den Kontrollfluss eines
   anderen; beispielsweise durch einen Parameter, der die Art der konkreten
   Aktion definiert.

   .. figure:: kontrollkopplung.png
	    :alt: kontrollkopplung

#. Externe Kopplung: liegt vor, wenn Komponenten über globale Daten miteinander
   kommunizieren.

   .. figure:: externe_kopplung.png
	    :alt: externe_kopplung

Information Hiding
==================
.. hint::

  Information sollte nur dann weitergegeben werden, wenn der Adressant diese
  zwingend benötigt, um seine Funktion auszuüben. (**Need-to-know-Prinzip**)

Die Idee des **Information Hiding** besteht darin, dem Programmierer Informationen,
die er nicht verwenden darf, gar nicht erst zugänglich zu machen. Das ermöglicht
spätere Änderungen der internen Struktur.

.. glossary::

  Information Hiding
    A software development technique in which each module's interface reveal as
    little as possible about the module's inner workings, and other modules are
    prevented from using information about the module that is not in the module's
    interface specification (IEEE 610.12)

.. figure:: informationhiding_modulschnittstelle.png
	 :alt: informationhiding_modulschnittstelle

Ein Modul stellt an seiner Schnittstelle nur genau die Operationen zur Verfügung,
die seine Kundenmodule wirklich benötigen. Ein Kundenmodul greift also nicht direkt
auf eine Variable zu, sondern ruft eine Operation auf, die den gewünschten Effekt
hat.

Wird die Darstellung der Daten geändert, so müssen nur die Operationen im Kernmodul
angepasst werden (solange die Schnittstelle unverändert bleibt).

Der Schutz, der durch das Information Hiding erzielt wird, verhindert vor allem
unbeabsichtigte Fehler. Über die Schnittstelle können zudem die Zugriffe überwacht
werden (über Logger etc.).

.. _separation-of-concerns:

Trennung der Zuständigkeiten
============================
Die Trennung von Zuständigkeiten (**separation of concerns**) ist ein grundlegendes
Prinzip im Software Engineering.
Für den Software-Entwurf ist es von zentraler Bedeutung, dass jede Komponente
nur für eine ganz bestimmten Aufgabenbereich abdecken. Komponenten, die gleichzeitig
mehrere Aufgaben abdecken, sind unnötig komplex. Das erschwert Verständnis und
Wartung und verhindert Wiederverwendung.

Zusammengehörende Daten und Operationen werden in einer Klasse zusammengefasst und
von anderen Klassen getrennt. Man trennt fachliche und technische Komponenten.
Funktionalität, die variable ist oder später erweitert werden soll wird in eine
eigene Komponente ausgelagert.

Alle interaktiven Programme enthalten neben den Komponenten, die Funktionalität
realisieren, auch Komponenten für die Interaktion. Die Trennung der Zuständigkeiten
bedeutet hier, dass Interaktion und Funktionalität strikt gegeneinander abgeschottet
werden. Dadurch ist es möglich, Funktionalität oder Interaktion zu verändern, ohne
den anderen Programmteil zu gefährden. (Model-View-Controller)

Hierarchische Gliederung
========================
Komponenten einer Architektur müssen so lange verfeinert werden, bis sie so einfach
sind, dass wir sie spezifizieren und realisieren können. Eine hierarchische
Gliederung ermöglicht es, die Komplexität zu reduzieren.

.. glossary::

  Hierarchie
    Eine Hierarchie ist eine Struktur von Elementen, die durch eine (hierarchiebildende)
    Beziehung in eine Rangfolge gebracht werden.

.. figure:: hierarchiearten.png
	 :alt: hierarchiearten

Eine **Aggregationshierarchie** gliedert ein System oder eine Komponente in ihre
Bestandteile (Ganzes-Teile-Hierarchie). Die Beziehung zwischen dem Ganzen und
seinen Teilen ist von der Art **besteht aus**. Aggregationshierarchien werden auf
Ebene der Systemarchitektur verwendet. Sie teilen das System in Komponenten auf,
die hierarchisch weiter verfeinert werden.

Eine **Schichtenhierarchie** ordnet Komponenten derart, dass jede Schicht genau
auf einer darunterliegenden Schicht aufbaut und die Basis für genau eine darüber liegende
Schicht bildet. Eine Schichtenhierarchie ist eine strengere Form der Monohierarchie.

Eine **Generalisierungshierarchie** ordnet Komponenten nach Merkmalen (Funktionen und
Attributen), indem fundamentale, gemeinsame Merkmale mehrerer Komponenten in einer
universellen Komponente zusammengefasst werden. Davon abgeleitete, spezialisierte
Komponenten übernehmen diese Merkmahle und  fügen spezielle hinzu. Damit werden
die fundamentalen Merkmale nur einmal definiert. Der objektorienterte Entwurf
stellt Generaliserungshierarchien von Klassen und Schnittstellen in den Vordergrund,
da diese mit Hilfe der Vererbung direkt in der objektorientierten Programmiersprache
implementiert werden können.
