==============
UML Einführung
==============

UML (Unified Modeling Language) ist eine standardisierte graphische
Sprache zur **Visualisierung**, **Spezifizierung**, **Konstruktion** und
**Dokumentation** von Software-Artefakten.

.. figure:: Strukturdiagramme.png
   :alt: Strukturdiagramme

   Strukturdiagramme

.. figure:: Verhaltensmodelierung.png
   :alt: Verhaltensmodellierung

   Verhaltensmodellierung


.. figure:: uml_im_project_verlauf.png
	 :alt: uml_im_project_verlauf
