===============================
Zustandsautomat / State Machine
===============================

Ein Zustandsautomat (engl. State Machine) zeigt die Zustände und Zustandsübergänge
innerhalb eines Objektes an. Eine State Machine ist ein endlicher Automat
(:ref:`endlicher-automat`) in UML Notation.

Dabei können die folgenden Elemente verwendet werden:

.. figure:: state_machine_elements.png
	 :alt: state_machine_elements

.. figure:: state_machine_overview.png
	 :alt: state_machine_overview

Bei Ereignissen wird ein Zustandsübergang angestoßen.

.. figure:: state_machine_example.png
	 :alt: state_machine_example
