=================
Use Case Diagramm
=================
Ein Use Case Diagramm beschriebt, wie sich ein System oder eine Anwendung
unter bestimmten Bedingungen verhält. Anwendungsfälle repräsentieren die Anforderungen
der Kunden.

.. figure:: use_case_overview.png
	 :alt: use_case_overview

Akteure
=======

Akteure sind Rollen (Personen oder Systeme). Akteure werden im Diagramm durch
Assoziationen mit den Use Cases verbunden, an denen sie beteiligt sind. Die
Assoziation ist binär und kann Mulitplizitäten aufweisen.

.. figure:: use_case_example.png
	 :alt: use_case_example

include Beziehung
=================

Mit ``include`` kann Use Case A einen Use Case B an einer bestimmten Stelle einfügen.

Die Include Beziehung zwischen zwei Anwendungsfällen kann mit einem Prozeduraufruf
verglichen werden: Der benutzte Anwendungsfall wird im aufrufenden Anwendungsfall
ausgeführt.

.. figure:: use_case_include.png
	 :alt: use_case_include

Extend Beziehung
================

Zwischen zwei Use Cases ist die Extend Beziehung, wenn Use Case A eine Erweiterung
von Use Case B ist.

Die Extend-Beziehung zwischen zwei Anwendungsfällen bedeutet, dass ein Anwendungsfall
an einer definierten Stelle (Extensionpoint) im anderen Anwendungsfall ausgeführt
wird, wenn eine dazu definierte Bedingung erfüllt ist.

.. figure:: use_case_extend.png
	 :alt: use_case_extend

Generalisierung
===============

Use Case A ist eine Generalisierung von B, wenn B eine spezielle Ausprägung von
A ist.

.. figure:: use_case_generalisierung.png
	 :alt: use_case_generalisierung

Auch Akteure können Generalisiert werden.

Use Case Beschreibung
=====================

.. figure:: use_case_beschreibung.png
	 :alt: use_case_beschreibung
