====================
Komponenten Diagramm
====================

Ein Komponenten Diagramm modelliert die Software-Architektur mit Bibliotheken,
Komponenten, Datenbanken usw.

.. figure:: component_diagramm.png
	 :alt: component_diagramm

Eine Komponente ist ein abgegrenzter Teil des Gesamtsystems. Ein Konnector
(``required Interface``) ist eine von der Komponente benötigte Schnittstelle.
Ein Interface (``provided Interface``) hingegen ist eine von der Komponente
bereitgestellte Schnittstelle.

Ein Port ist ein Interaktionspunkt zwischen einem Classifier (Klasse, Paket,
Komponente, ...) und seiner Umgebung. Er gruppiert eine beliebige Anzahl von
Interfaces zu einem Dienst, der einen bestimmten Zweck verfolgt. Ein Port
dient zur Kapslung des Classifiers gegenüber seiner Umgebung und kann für
eingehende und ausgehende Signale genutzt werden.

.. figure:: port.png
	 :alt: port

``protected`` und ``private`` ports werden zur internen Nachrichtenverteilung
genutzt.

.. figure:: component_diagramm_example.png
	 :alt: component_diagramm_example
