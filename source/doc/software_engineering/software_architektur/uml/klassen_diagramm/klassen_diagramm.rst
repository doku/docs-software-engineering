===============
Klassendiagramm
===============

.. figure:: klassendiagramm_overview.png
	 :alt: klassendiagramm_overview

Klassen
=======

-  Klassenname
-  Attribute

   -  Bezeichner
   -  Datentypen
   -  Sichtbarkeiten
   -  ggf. Vorbelegungen

-  Methoden

   - Signatur
   - Sichtbarkeit
-  Sichtbarkeit

   - public (+), protected (#), private (-)
   - static (unterstrichen)
   - abstract (kursiv)

.. figure:: Klassendiagramm_Klasse.png
   :alt: Klassendiagramm Klassen

   Klassendiagramm Klassen

Assoziation
===========

Beziehungen zwischen Klassen werden als Assoziationen bezeichnet und im
Klassendiagramm mit Verbindungen dargestellt. Diese Verbindungen
zwischen Objekten werden als Links bezeichnet.

.. figure:: Klassendiagramm_Assoziation.png
   :alt: Klassendiagramm Assoziationen

Navigierbarkeit
---------------
Die Pfeilrichtung einer Assoziation gibt die Richtung der
Navigierbarkeit an.
Beidseitig navigierbare Assoziationen haben aber keine Pfeile. Nichtspezifizierte
Navigierbarkeit hat ebenso keine Pfeile.

Multiplizitäten
---------------
Die Multiplizitäten geben an, wie viele Objekte der jeweiligen Klasse an
einer Assoziation teilnehmen. z.B.: 0..1, 1, 0..\*, 1..\*

Rollen
------
Mit der Angabe von Rollen kann die Beziehung zwischen den Objekten genauer
spezifiziert werden.

Aggregation und Komposition
===========================

Aggregierte Objekte werden von dem besitzenden Objekt dominiert. Es ist
kein externer Zugriff auf aggregierte Objekte möglich.

Bei der Komposition wird die Lebensdauer (Instanziierung und Freigabe)
von dem dominierenden Objekt beherrscht. Die Kindklasse kann nicht
unabhängig von der Elternklasse existieren.

.. figure:: Klassendiagramm_AggregationKomposition.png
   :alt: Klassendiagramm Aggregation und Komposition

Assoziationsklassen
-------------------
Assoziationen können über Attribute oder Methoden verfügen.
Assoziationsklassen repräsentiert die Assoziation. Insbesondere bei ``n:m``
Assoziationen werden häufig Assoziationsklassen verwendet.

.. figure:: klassendiagramm_assoziationsklasse.png
	 :alt: klassendiagramm_assoziationsklasse

Eine ``n:m`` Assoziation mit Assoziationsklasse ``A`` ist semantisch mit einer
``1:n`` und ``m:1`` Assoziation ``B`` gleichzusetzen.

.. figure:: klassendiagramm_assoziationsklasse_aequivalenz.png
	 :alt: klassendiagramm_assoziationsklasse_aequivalenz

Bestehen mehrere Assoziationen zwischen zwei Klassen, so werden oft
rollenbeschreibende Assoziationsklassen eingeführt.

.. figure:: assoziationsklasse_rollen.png
	 :alt: assoziationsklasse_rollen

Assoziationen können zur selben Klasse verbunden sein. Damit ist nicht gemeint,
dass ein Objekt mit sich selbst in Beziehung steht sondern zu Objekten der selben
Klasse. Rekursive Assoziationen können auch Assoziationsklassen haben.

.. figure:: rekrusive_assoziation.png
	 :alt: rekrusive_assoziation

Vererbung und Interfaces
========================

Für Vererbung verwendet man eine nicht ausgefüllte Pfeilspitze. Der Pfeil
zeigt dabei immer von der Subklasse zur Superklasse. In der Subklasse
werden nur die Attribute und Methoden angegeben, die nicht geerbt
werden.

Für Interfaces wird eine gestrichelte Linie mit nicht ausgefüllter
Pfeilspitze verwendet. Die Pfeilspitze zeigt von der Klasse zum
Interface.
