Sequenzdiagramm
===============

Ein Sequenzdiagramm beschreibt einzelne und genau definierte Abläufe. Es
dient zur Beschreibung von Use Cases und zur Beschreibung des
dynamischen Verhaltens von Objekten. Man sieht die zeitliche Reihenfolge
der Nachrichten.

Die Summe aller Sequenzdiagramm beschreibt das dynamische Verhalten des
Systems. Botschaften werden bei der Klasse des Empfängerobjekts als Methoden
implementiert.

.. figure:: sequenzdiagramm_overview.png
	 :alt: sequenzdiagramm_overview

.. figure:: sequenzdiagramm_building_blocks.png
	 :alt: sequenzdiagramm_building_blocks

.. figure:: Sequenzdiagramm_Einfach.png
   :alt: Sequenzdiagramm Einfach

   Sequenzdiagramm Einfach

.. figure:: Sequenzdiagramm_Alternative.png
   :alt: Sequenzdiagramm mit Alternative

   Sequenzdiagramm mit Alternative

.. figure:: Sequenzdiagramm_Loop.png
   :alt: Sequenzdiagramm mit Loop

   Sequenzdiagramm mit Loop

.. figure:: Sequenzdiagramm_Ref.png
   :alt: Sequenzdiagramm mit Referenz

   Sequenzdiagramm mit Referenz
