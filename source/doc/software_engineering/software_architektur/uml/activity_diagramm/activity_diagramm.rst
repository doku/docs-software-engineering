==================
Aktivitätsdiagramm
==================

Ein Aktivitätsdiagramm ist eine Aktivitätenfolge mit Kontrollfluss. Es wird
zur Beschreibung von Use Cases oder Klassen eingesetzt.

.. figure:: activitiy_diagramm_example.png
	 :alt: activitiy_diagramm_example

.. figure:: activity_diagramm_overview.png
	 :alt: activity_diagramm_overview

Vereinigungen
=============

Merge Node
----------
Bei einem Merge Node muss einer der Eingänge aktiv sein.

.. figure:: merge_node.png
	 :alt: merge_node

Join Node
---------
Bei einem Join Node müssen alle Eingänge aktiv sein.

.. figure:: join_node.png
	 :alt: join_node


Verzweigung
===========

Decision Node
-------------
Bei einem Decision Node wird einer der Ausgänge aktiv.

.. figure:: decision_node.png
	 :alt: decision_node

Fork Node
---------
Bei einem Fork Node werden alle Ausgänge aktiv.

.. figure:: fork_node.png
	 :alt: fork_node

Knoten
======

.. figure:: knoten.png
	 :alt: knoten

Signale
=======

.. figure:: signale.png
	 :alt: signale

Partitions
==========
Mit Partitions können die Zuständigkeitsbereiche eines bestimmten Objekts
veranschaulicht werden.

.. figure:: partitions.png
	 :alt: partitions
