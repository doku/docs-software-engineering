=========================
evolutionary architecture
=========================


.. glossary::

  evolutionary architecture
    Evolutionary architecture is trying to elevate the notion of evolvability
    into architecture. architecture has to be as easy to change as well

    The stuff that the expert developers find important to describe the system,
    is gradually changing over time to the point where the origins are no longer
    discernable.

.. hint::
  One of the core drivers of complexity is irreversibility.

  If you can't go back and change your mind, that then constraints you
  very much.

Instead of focus on layered architceture and crossfunctional requirements
focus on slicing the problem vertically in end-to-end development teams.

How much Upfront Design is reasonable?
======================================

Architecture is mostly, or even exclusively an ongoing activity.


Evolution
=========

#. Recombination
#. Mutation
#. Fitness function


- refactoring to patterns
- enterprise architecture and integration patterns
