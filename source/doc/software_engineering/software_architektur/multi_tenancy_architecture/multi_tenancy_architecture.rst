==========================
Multi-Tenancy-Architecture
==========================

(Mandantenfähigkeit)

Multi-Tenancy-Architecture is a software architecture used in cloud-computing in
which multiple customers ("Tenants") access the application and infrastructure
platform. Tenant specific data and configuration is logically separated and can
only be accessed by the authorized tenant while platform wide data and configuration
is shared across all tenants.

.. figure:: multi_tenancy.png
	 :alt: multi_tenancy

Multi-tenant application architecture improve

Cost
====
With multi-tenant architectures, the cost to run the system is fixed.
Each new customer you add drives down the marginal cost of adding the next one.
With single-tenant architectures, the marginal cost of adding new customers never goes down.

Updates
=======

With multi-tenant, deploys are typically all or nothing. A beautiful side effect
of single-tenant maintenance is that you get incremental rollouts and targeted beta releases for free.


Disaster recovery is much easier when each customer’s data is physically isolated,
rather than simply being logically isolated within a single database.

Source
======

https://hackernoon.com/exploring-single-tenant-architectures-57c64e99eece
