===========
Architektur
===========

Die Softwarearchitektur stellt den Grobentwurf dar.

Das System wird in überschaubare Einheiten gegliedert. Diese einzelnen Einheiten
können dann

Durch eine hierarchische Struktur lässt sich die Komplexität durch Abstraktionsebenen
verstecken.

.. figure:: vorgehensrichtungen_entwurf.png
	 :alt: vorgehensrichtungen_entwurf

Bei der **outside-in** Entwicklung geht man von der Bedienoberfläche in Richtung
Datenstrukturen und Algorithmen. Sehr oft entsteht diese Situation, wenn die Kunden
bereits mit einem Prototyp der Bedienoberfläche einverstanden waren.
Die **Inside-out** Entwicklung (vom Inneren des Systems zur Bedienoberfläche) ist
typisch für eine Situation, in der ein bestehendes System um eine neue Funktion
erweitert wird.

.. figure:: begriffsmodell_softwarearchitektur.png
	 :alt: begriffsmodell_softwarearchitektur

Durch den Architektur-Entwurf werden die Zuständigkeiten vorgeschrieben.

.. hint:: Code is the **embodiment** of architecture.

.. glossary::

  System
    A collection of components organized to accomplish a specific function
    or set of functions. (IEEE 1471)

  Software-System
    Ein Software-System ist eine Menge von Software-Einheiten und ihren Beziehungen, wenn
    sie gemeinsam einem bestimmten Zweck dienen. Dieser Zweck ist im allgemeinen
    komplex, er schließt neben der Bereitstellung eines ausführbaren Programms
    gewöhnlich die Organisation, Verwendung, Erhaltung und Weiterentwicklung des
    Programms ein.

  Component
    A **component** is one of the parts that make up a system. A component may be
    hardware or software and may be subdivided into other components. (IEEE 610.12)

		Components are independently replaceable and independently upgradeable.
		A component is a part of a system that could be swapped out for another implementation.
		(Martin Fowler)

  Software component
    A software component is an architectural entity that encapsulates a subset
    of the system's functionality and/or data. It restricts access to that
    subset via an explicitly defined interface and has explicitly defined
    dependencies on its required execution context. (Taylor, Medvidoc, Dashofy 2010)

    Komponenten sind atomar oder werden verfeinert, d.h. eine Komponente kann weitere
    Komponenten enthalten.

.. glossary::
  Modul
    Ein **Modul** ist eine Menge von Operationen und Daten, die nur so weit von außen sichtbar
    sind, wie dies die Programmierer explizit zugelassen haben.

    Die Struktur jedes Moduls soll einfach und leicht verständlich sein. Die
    Implementierung des Moduls sollte austauschbar sein, ohne dass dadurch
    andere Module betroffen sind.

    Die Module sollen so entworfen sein, dass **wahrscheinliche Änderungen**
    ohne Modifikation der Modulschnittstelle durchgeführt werden können.

Interfaces
==========

.. glossary::

  Schnittstelle
    Die Schnittstelle ist die Grenze zwischen zwei kommunizierenden Komponenten. Die
    Schnittstelle einer Komponente stellt die Leistung der Komponente für ihre
    Umgebung zur Verfügung und/oder fordert Leistungen, die sie aus der Umgebung
    benötigt. (Ludewig, Lichter)

  interface
    An interface is a boundary across which two independent entities meet and interact
    or communicate with each other.

Für eine Schnittstellendefinition müssen folgende Aspekte dokumentiert werden:

- **Syntax und Art der Kommunikation**: Namen, Anordnung und Parametertypen der Operationen
- **Funktionale Merkmale**: angebotene Dienste mit benötigten Daten,  Vor- und Nachbedingungen, Fehlerverhalten
- **Allgemeine Qualitätsangaben**: nichtfunktionale Anforderungen, wie Antwortzeit, Genauigkeit

Um die syntaktischen Merkmale präzise zu beschreiben, verwenden wir formale Sprachen,
sogenannte **Interface Definition Languages**.


Architektur
===========

.. glossary::

  architecture
    The fundamental organization of a system embodied in its components, their
    relationships to each other and to the environment, and the principles guided
    its design and evolution (IEEE 1471)

  software architecture
    The software architecture of a program or computing system is the structure
    or structures of the system which comprise software elements, the externally
    visible properties of those elements, and the relationships among them.
    (Bass, Clements, Kazman 2003)

Software Architecture is a lot more like city planning than it is like the work
of an actual architect.

Eine Software-Architektur besteht aus Komponenten und ihren Beziehungen zur Umgebung,
in verschiedenen Strukturen.

.. note::
	The architecture of a system is the expert developers' shared understanding
	of the system design. (Ralph Johnson)

	All documents/diagrams about the architecture are just a (usually inaccurate)
	representation of this shared	understanding.

.. hint::
	The software architecture is a set of design decisions that you wish you could
	get right early. (Ralph Johnson)

	The software architecture are the decisions that are hard to change.

.. figure:: architecture_definition.png
	 :alt: architecture_definition

A Software Architect should ask, where are the things that are hard to change,
and how can we make them easy to change. Architecture is sometimes a tricky, even
dangerous, metaphor.

.. glossary::

  architectural description
    An architectural description is a model - document, product or other artifact -
    to communicate and record a system's architecture. An architectural description
    conveys a set of views each of which depicts the system by describing domain
    concerns.

Um Architekturen zu beschreiben, können spezielle Architekturbeschreibungssprachen
(Architecture Description Language, ADL) verwendet werden.

.. glossary::

  ADL
    An ADL for software applications focuses on the high-end structure of the
    overall application rather than the implementation details of any specific
    source module. ADLs provide both a concrete syntax and a conceptual framework
    for modelling a software system's conceptual architecture. (Medvidovic, Rosenblum 1997)

In den letzten Jahren hat sich UML (:ref:`ref-uml`) für die Beschreibung objektorientierter
Architekturen durchgesetzt.

4+1 View Model of Architecture
------------------------------
Eine bestimmte Sicht auf die Architektur liefert spezifische Informationen
für die verschiedenen Interessen der Projektbeteiligten. Eine Sicht selbst
fügt aber keine neuen Information zur Architektur hinzu , sondern fasst
Informationen zusammen, die möglicherweise in verschiedenen Strukturen oder Modellen
der Architektur enthalten sind.

Die **Systemperspektive** stellt dar, wie das zu entwickelnde System in die
Umgebung eingebettet ist und mit welchen anderen Systemen es wie interagiert.
Dadurch sind die Systemgrenzen und die Schnittstellen zu den Nachbarsystemen
festgelegt.

Die **statische Perspektive** zeigt die zentralen Komponenten der Architektur,
ihre Schnittstellen und Beziehungen. In der Regel muss diese Zerlegung hierarchisch
verfeinert werden, bis die einzelnen Komponenten so weit modularisiert
sind, dass sie codiert werden können.

Die  **dynamische Perspektive** stellt dar, wie die Komponenten zur Laufzeit
zusammenarbeiten (Kontrollfluss, zeitliche Abhängigkeiten), sie zeigt also das
Systemverhalten. Dabei beschränkt man sich auf die Modellierung der wichtigen
Interaktionen.

Die Sichten der **Verteilungsperspektive** zeigen, wie die Komponenten der statischen
Perspektive auf Infrastruktur- und Hardware-Einheiten abgebildet werden. Sie
stellen aber auch dar, wie die Entwicklung, der Test etc. auf die Teams der
Entwicklerorganisation aufgeteilt werden

.. hint::

  Die Architektur einer Software ist die Gesamtheit ihrer Strukturen auf einer
  bestimmten Abstraktionsebene. Dabei geht es normal um die statische Struktur
  der höchsten Gliederungsebene, also um die Systemarchitektur.

Architecture Artifacts
======================

- Design diagrams and models
- Patterns
- Frameworks
- code
- Tests
- Guidelines, Principles
- Exemplars (a.k.a. copy/paste ready code): highlight well done examples

.. hint::

	The most important artefact of an architect are	**improved developers**.
