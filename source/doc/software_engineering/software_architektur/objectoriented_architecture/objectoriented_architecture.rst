==========================
Objektorientierter Entwurf
==========================

.. figure:: metamodell_objektorientierung.png
	 :alt: metamodell_objektorientierung

Zentrale Aktivitäten des objektorientierten Entwurfs:

#. Identifizieren von Objekten und Klassen
#. Festlegen des Verhaltens der Objekte und Klassen
#. Identifizieren von Beziehungen zwischen den Klassen
#. Festlegen der Schnittstellen zwischen den Klassen

Die richtige Wahl der Klassen ist der wichtigste Schritt zu einem gute objektorientierten
Entwurf.

Anwendungssoftware enthält immer ein Modell der Realität, d.h. ein Modell ihres
Anwendungsbereichs. Die Software ist damit eng and die Anwendung gekoppelt. Wenn
sich die Anwendung ändert, muss diese Änderung in der Software ebenfalls vollzogen
werden. Darum ist es zweckmäßig, die Konzepte und Begriffe des Anwendungsbereichs
in der Architektur explizit zu modellieren.

Im Anwendungsbereich wird die Ist-Analyse durchgeführt und das fachliche Modell
entwickelt. Es besteht im Kern aus den fachlichen Abläufen (den Funktionen oder
Geschäftsprozessen) und deren relevanten Gegenständen. Die fachlichen Abläufe kann
man in :term:`use case` modellieren, die Gegenstände des Anwendungsbereichs
und ihre Beziehungen werden durch ein objektorientiertes Begriffsmodell beschrieben.

Die technischen Klassen sollten von den fachlichen Klassen getrennt werden,
da sich technische und fachliche Klassen unterschiedlich schnell entwickeln.

SOLID
=====

.. _single-responsibility-principle:

Single Responsibility
---------------------
.. note:: A class should have one, and only one, reason to change. (Martin 2002)

Things that change for the same reasons should be in the same module and things
that change for different reasons should be in different modules.

Die Klasse sollte möglichst nur eine möglichst präzise Aufgabe haben.
Dadurch wird die Komplexität der Klassen gering gehalten.

A class should not contain methods that change at different times and different
reasons.

.. attention::

	Do **not** put functions that change for different reasons in the same class.

	Do **not** mix concerns in your classes.

The code has a responsibility to the persons that want it to be changed. There
should always be only one person who wants a module to be changed.

The single responsibility says that if you are designing your software you consider
who is using it, who are the roles that are using that software and how can you
isolate the software, so that if one guy asks for a change, that change will not
effect anybody else. (Robert Martin)

.. glossary::
	rigidity
		The tendency of a system to resist change by forcing you to make a change
		in multiple places.

	fragility
		Fragility occurs when you touch a module and something completely unrelated
		on the other end of the system breaks.

.. _open-close-principle:

Open-Close-Principle
--------------------

.. note:: Modules should be open for extension, but closed for modification. (Bertrand Meyer, 1997)

You should be able to change what the module does without changing the module.
A Module should be open for extension, so its behavior can be extended, but closed
for modification.

Polymorphic calls can be implemented in any way you would like.

Möchte man ein Komponente verwenden, obwohl sie nicht exakt den Anforderungen
entspricht, muss diese angepasst werden. Eine Komponente, die diese Veränderung
zulässt heißt **offen** (Bertrand Meyer 1997).

Wird eine Komponente verwendet, sollte die Schnittstelle und die Funktionen
der angebotenen Operationen nicht mehr verändert werden, da alle Änderungen in
den Kundenkomponenten nachvollzogen werden müssen. Eine solche Komponente heißt
**geschlossen**.

Wir versuchen unsere Komponenten so zu entwerfen, dass sie sowohl geschlossen,
also stabil benutzbar, also auch offen für Erweiterung sind.
Änderungen werden durch :term:`Information hiding` versteckt und Erweiterung ist
durch :term:`Vererbung` möglich.

Um Entwürfe zu erstellen, die dem Offen-geschlossen-Prinzip genügen,
müssen die richtigen Abstraktionen gefunden und die veränderbaren und erweiterbaren
Aspekte expliziert modelliert werden.

.. figure:: open_closed_example.png
	 :alt: open_closed_example

.. hint::

  Modules should be both open for extension and closed for modification.

Liskovsches Substitutionsprinzip (substitution principle)
---------------------------------------------------------
Let :math:`q(x)` be a property provable about objects :math:`x` of Type :math:`T`.
Then :math:`q(y)` should be true for objects :math:`y` of type :math:`S` where :math:`S`
is a subtype of :math:`T`. (Liskov, Wing 1999)

.. hint::

	Derived classes must be usable trough the base class interface,
	without the need for the user to know the difference.

Dadurch wird sichergestellt, dass für Subklassen immer mindestens die gleichen Eigenschaften
wie für ihre Superklasse gelten. So kann man Superklassen immer bedenkenlos durch
Subklassen ersetzen, da für die Subklassen immer mindestens die gleichen Eigenschaften
wie ihre Superklasse gelten.

Daher sollte man beim Überschreiben von Methoden darauf achten, dass die Methodenimplementierung
nicht grundsätzlich verschiedene Rückgabewerte/Methodenfluss hat. Der Aufrufer der
Methode erwartet ein bestimmtes Verhalten durch die Implementierung der Vaterklasse.
Auch die Kindklassen sollten diese Erwartungen erfüllen.

Interface-segregation Principle (ISP)
-------------------------------------

.. glossary::

	interface-segregation principle
		The interface-segregation principle (ISP) states that no client should be forced
		to depend on methods it does not use.

Schnittstellen sollten möglichst schmal sein und eine möglichst :term:`loose coupling`
realisieren.

Dependency Inversion
--------------------
High-level should not depend on low level modules. Both should depend on abstractions.
Abstractions should not depend upon details. Details should not depend upon abstractions.

Dadurch können sich detaillierte Objekte ändern, ohne die Objekte der hohen Abstraktionsebene
anpassen zu müssen.

.. _law-of-demeter:

Law of Demeter
--------------
The Law of Demeter heuristic says that a module should not know about the innards
of objects it manipulates. An object should not expose its internal structure
through accessors because to do so is to expose, rather than hide, its internal
structure.

The Law of Demeter says that a method :math:`f` of a class :math:`C` should only
call the methods of these:

- :math:`C`
- An object created by :math:`f`
- An object passed as an argument to :math:`f`
- An object held in an instance variable of :math:`C`

The method should **not** invoke methods on objects that are returned by any of
the allowed functions.

Rather we want our immediate collaborators to offer all the services we need.

.. code-block:: java

  //bad example
  String outputDir = ctxt.getOptions().getScratchDir().getAbsolutPath();

  //good example
  BufferdOutputStream bos = ctxt.createScratchFile(fileName);

Dadurch entsteht hohe Abhängigkeit zu anderen Objekte.

OO Principles
=============

.. _common-closure-principle:

Common Closure Principle
------------------------

Composition vs. Inheritance
---------------------------

Source: https://www.thoughtworks.com/de/insights/blog/composition-vs-inheritance-how-choose


The purpose of composition is obvious: make wholes out of parts.

**Inheritance captures semantics** (meaning) in a classification hierarchy (a taxonomy), arranging
concepts from generalized to specialized, grouping related concepts in subtrees, and so on.

**Inheritance captures mechanics** by encoding the representation of 
the data (fields) and behavior (methods) of a class and making it 
available for reuse and augmentation in subclasses. Mechanically, 
the subclass will inherit the implementation of the superclass and 
thus also its interface.

Cross-Domain inheritance relationships should be avoided.

.. hint:: 
	Domain classes should use implementation classes, not
	inherit from them (this would obviously violate the *is substitutable for*
	relationship an inheritance models).

Unless you are creating an implementation class, you should not
inherit from an implementation class. The preferred solution is
to inherit from utility classes as much as necessary to implement
your mechanical structures, then use these structures in 
domain classes via composition, not inheritance.

The most common - and beneficial - use of inheritance is for
differential programming. We want to reuse the entire interface
and implementation from the superclass, and our changes are
primarily additive.

Inheritance is most useful for grouping related sets of concepts,
identifying families of classes, and in general organizing
the names and concepts that describe the domain.

Inheritance should only be used when:

- Both classes are in the same logical domain
- The subclass is a proper subtype of the superclass
- The superclass’s implementation is necessary or appropriate for the subclass
- The enhancements made by the subclass are primarily additive.

There are times when all of these things converge:

- Higher-level domain modeling
- Frameworks and framework extensions
- Differential programming

If you’re not doing any of these things, you probably won’t need class inheritance very often. 
