====================
Software Architektur
====================

.. toctree::
  :maxdepth: 2
  :glob:

  entwurf/*
  software_architektur/*
  architektur_prinzipien/*
  architectural_patterns/*
  architecture_checklist/*
  clean_architecture/*
  architectural_antipatterns/*
  design_patterns/*
  objectoriented_architecture/*
  domain_driven_design/*
  microservice_architecture/*
  eventdriven_architecture/*
  serverless_architecture/*
  cloud_architecture/*
  entity_interactor_boundary_architecture/*
  architektur_qualitaet/*
  software_evolution/*
  architecture_documentation/*
  deep_synergy_between_testability_and_good_design/*
  evolutionary_architekture/*
  multi_tenancy_architecture/*
  uml/*
  architektur_workshop/*
