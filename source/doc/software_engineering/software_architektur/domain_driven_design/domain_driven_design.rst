========================
Domain Driven Design DDD
========================

.. glossary::

  ubiquitous language
    domain language

  bounded context
    The


The essence of domain driven design is to model a :term:`ubiquitous language`
in a :term:`bounded context`

Context Mapping
