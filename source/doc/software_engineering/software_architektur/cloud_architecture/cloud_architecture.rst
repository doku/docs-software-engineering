==================
Cloud Architecture
==================

Cloud architecture is the configuration of cloud computing resources to form solutions
and solve the business needs.

Cloud-Parts
===========

Storage
-------

Object
  Object storage, such as AWS S3 and Microsoft Azure Blob storage, is optimized
  for storing large volumes of unstructured data. Data is stored as immutable objects,
  the contents of which can only be written or updated in their entirety.

Block
  Cloud block storage, such as AWS Elastic Block Storage, in essence
  provides a virtualized storage area network with logical volume management
  provisioning via a simplified web service interface.

.. figure:: cloud_architecture.png
	 :alt: cloud_architecture

Building a Cloud Architecture
=============================

#. Define your requirements
#. Define your desired end state
#. Mapping "as is" to "to be"
#. Create Final Architecture (Logical and Physical Architecture)

.. figure:: cloud_management_platform.png
	 :alt: cloud_management_platform

Design Decisions
================

As a cloud architect you have to understand the business requirements, map
those requirements into solution patterns (meta-architecture or logical architecture)
and then map the solution patterns into actual technology.

- soft cost are cost savings that are more difficult to define, such as time to market
  or agility. (Strategic impact to the advantage of the business)
- hard cost: mostly operational cost

.. figure:: cloud_impact.png
	 :alt: cloud_impact
