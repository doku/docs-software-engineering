===============
Implementierung
===============

Ziele guter Programmierung
==========================

#. Korrektheit
#. Lesbarkeit und Verständlichkeit
#. Effizienz
#. Änderbarkeit
#. Wiederverwendbarkeit

.. figure:: programmcomic.png
	 :alt: programmcomic

.. note::
  Programming is the art of telling another human being what one wants the computer
  to do. (Donald Knuth)

Code wird deutlich öfter gelesen als geschrieben. Lesbarkeit ist daher wichtiger
als geringer Schreibaufwand.

.. danger::
  Entwickler brauchen deutlich länger um Code zu lesen, als um Code zu schreiben.

Coderichtlinien
===============
Richtlinien müssen begründet und nachvollziehbar sein. Ihre Einhaltung muss überprüft
werden durch manuelle Überprüfung (Reviews) und automatisierte Überprüfung.

- Namenskonventionen
- Formatierungen
- Einsatz von Konstrukten der Programmiersprache (Ternäre Ausdrücke, Anonyme Klassen)
- Dokumentation (jede Klasse, public Methode)
- Umgang mit Compiler-Warnungen

.. hint::
  "Go To Statement Considered Harmful" E. Dijkstra (1968)

  Für Strukturierte Programmierung werden ausschließlich Sequenzen, Bedingte
  Anweisungen und Schleifen benötigt.

  "Global Variable Considered Harmful"

  Beschränkung des Gültigkeitsbereichs der Variablen

Bezeichner
==========

Bezeichner sollten **konsistent** und **prägnant** sein.

Konsistenz
  Eindeutiger Name für ein Konzept

Prägnanz
    Korrekter und direkter Name des Konzepts

.. _dry-code:

Don't Repeat Yourself (DRY)
===========================
Die Codebase wird unnötig aufgebläht. Code Klone lassen sich gut mit statischer
Codeanalyse finden.
