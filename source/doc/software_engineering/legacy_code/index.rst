===========
Legacy Code
===========

.. toctree::
  :maxdepth: 2
  :glob:

  introduction/*
  changing_software/*

This section is a summery of "Working Effectively, with Legacy Code" from
Michael Feathers.
