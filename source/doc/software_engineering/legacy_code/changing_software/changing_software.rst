=================
Changing Software
=================

Adding new Features and Fixing Bugs
===================================

Behavior is the most important thing about software. It is what the users depend on.
Users like it when we add behavior (provided it is what they  really wanted), but
if we change or remove behavior they depend on (introduce bugs), they stop
trusting us.

Improving Design
================
Design improvements alter the software's structure to make it more maintainable,
but generally we want to keep its behavior intact. This is called :ref:`refactoring`.
One of the main reasons why
many programmers don't attempt to improve design often is because it is relatively
easy to lose behavior (introduce a bug) or create bad behavior in the process
of doing it.

Preserving existing behavior is one of the largest challenges in software development.
Even when we are changing primary features, we often have very large areas of
behavior that we have to preserve.

Working with Feedback
=====================

Edit and Pray
  You carefully plan the changes your are going to make, you make sure that you
  understand the code you are going to modify, and then you start to make changes.
  When you are done, you run the system to see if the change was enabled, and then
  you poke around further so make sure that you didn't break anything

Cover and Modify
  A safety net of tests detects undesired changes in behavior. You can keep most of
  the behavior fixed an know that you are changing only what you intend to.

Unit testing is one of the most important components in legacy code work. They
give you fast feedback as you develop and allow you to refactor with much more
safety.

Dependency is one of the most critical problems in software development. Much
legacy code works involves breaking dependencies so that change can be easier.
When classes depend directly on things that are hard to use in a test, they
are hard to modify and hard to work with.

.. note::

  Legacy Code Dilemma
    When we change code, we should have tests in place. To put tests in place,
    we often have to change code.

When we break dependencies, we can often write tests that make more invasive
changes safer. The trick is to do these initial refactorings very conservatively.
Some dependencies break cleanly, others end up looking less than ideal from a
design point of view.

The Legacy Code Change Algorithm
--------------------------------

#. Identify change points.
#. Find test points.
#. Break dependencies.
#. Write tests.
#. make changes and refactor.
