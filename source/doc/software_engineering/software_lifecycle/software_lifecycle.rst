===================
Software Life Cycle
===================

The software life cycle is the period of time that begins when a software
product is conceived and ends when the software is no longer available for use.
The software life cycle typically includes a concept phase, requirements phase,
design phase, implementation phase, test phase, installation and checkout phase,
operation and maintenance phase and sometimes a retirement phase. These phases
may overlap or be performed iteratively. (IEEE 610.12)

Software durchläuft folgende Phasen von der Entstehung bis zur Verschrottung.

Konzeption
==========

.. figure:: produktplanung.png
	 :alt: produktplanung

Analyse
=======
Das bestehende Problem zu durchdringen und zu verstehen.

Wie ist die Situation im Augenblick? Was will der
Kunde in Zukunft haben, wie soll sich die Situation ändern?

Liefert das Rohmaterial der Spezifikation

Ein Projekt steht und fällt mit der Analyse

Ergebnis einer Analyse ist ein großer Zettelasten.
Viele Informationen die zunächst ungeordnet sind.

Spezifikation der Anforderungen
===============================
Anforderungen müssen geordnet, dokumentiert, geprüft, ergänzt und korrigiert werden.

"Aufgeräumte" variante des Analysematerials

Architekturentwurf und Spezifikation der Module
===============================================
Software-Systeme bestehen aus Modulen oder Komponenten, die miteinander die
Gesamtfunktionalität des System

Wenige Köpfe - aber die aller besten. Viele Leute können hier nicht helfen.

Die Schnittstellen der Komponenten werden so präzise wie möglich festgelegt,
damit die Komponenten parallel entwickelt und am Ende problemlos integriert werden
können.

Verlässliche Schnittstellen stellt sichere Kooperation der Komponenten sicher.

Codierung und Modultest
=======================
Die module werden codiert und auf Basis der Spezifikation getestet und korrigiert.
Test Ablauf vom Unit über den Integrationstest bin zu Modultest.

Integration, Test, Abnahme
==========================
Das System wird aus dem fertig gestellten Modulen zusammengebaut (**integriert**)
und **getestet**.

Testen dient dazu um Fehler zu finden.

Testen ist destruktiv und kann daher nicht vom selben Entwickler des Moduls durch
geführt werden.

Schließlich **Abnahme** durch den Kunden.

Betrieb und Wartung
===================
Software-System wird beim Auftraggeber **installiert** und in Betrieb genommen.

Bekannter Fehler für den es einen Workaround gibt ist immer besser als eine schlampige
Reparatur.

Bei der Reparatur wird viel kaput gemacht, die Fehlerquote it 1-2 Zehnerpotenzen höher
als normal.

Während der Nutzung des Systems fallen Fehler auf und werden behoben,
und fast immer werden auch neue Anforderungen an das System gestellt und umgesetzt (**Wartung**)

Wir müssen die Wartung zulassen. In der Entwicklung kann sicher gestellt werden, das die
Wartung so billig, einfach und fehlerarm ist.

Ablauf und Ersetzung
====================
Jedes Software-System erreicht irgendwann einen Zustand, in dem die weitere Wartung
unvertretbar aufwändig und schwierig ist.

Software verliert an Qualität wenn an ihr gearbeitet/gewartet wird.

Dann wird es aus dem Betrieb genommen und in der Regel durch Nachfolgesystem ersetzt.

Tätigkeiten während der gesamten Entwicklungszeit
=================================================
- Leitungsfunktion

Software Entwicklung **ist** Dokumentation.
