==================
Forschungsmethoden
==================

Deductive and inductive Method
==============================
In the inductive method, we try to generalise from observing single 
or few cases in a very detailed way.

In the deductive method we first find a theory to explain a certain 
phenomenon. Then, the scientist deduces a concrete hypothesis that can
be tested. 
The predicted results based on the hypothesis are then compared to the
observation.

The Scientific Method
=====================

- Methodolical Research: What is the best way to perform a certain task in a software project?
- Constructive Research: What is the best tool support for a specific task?
- Theoretical Research: How can I describe an aspect of a software system in a mathematical and precise way?
- Emperical Reserach: Common question: What is the effect of a method or tool? Which one is more effective and efficient?

Writing a Paper
===============

**Structure**

- Title (1,2 Sentences that describe the paper, self-contained, search engines, main results/purpose of paper)
- Authors (names and affiliations)
- Abstract (1 paragraph 250-300 words that summarize paper content)
- Introduction
- Literature Review (Background, Related Work)
- Method
- Results
- Discussion
- Conclusion
- Acknoledgements
- References
- Appendix

Abstract
--------
purpose, 
  experimental design & methods, major findings, implications and Conclusions)

Evaluate a Paper
================

Soundness
- Appropriate reseach metohds
- Results scouped to what can be supported
- Limitations are discussed, not hidden

Sigificance
- Extends body of knowledge
- Discusses relevance and usefullness of the research (theory and practice)
- Compares with pertient related Work

Evaluate
- Verifiability (Information is available to support the verification or replication of the claimed contributions

Experiments
===========

- What are the dependent and independent variables?
- What is the treatment?
- What does the control group?

- Random assignment of treatment and control group

It's a quasi-Experiment if you have lack of randomisation of either subjects or objetcs. 

Scoping
-------

Analyse 'Objects of study' for the purpose of 'purpose' with respect to their 'Quality focuse' 
from the point of the 'Perspective' in the context of 'context'.

.. figure:: type1-type2-errors.png
