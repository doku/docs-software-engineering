==========
Statistics
==========

- Population: all elements from a set of data (All fishes of the bodensee)
- Sample: one of more observations drawn from a Population (Bodensee fishes in my net)
- Sample size :math:`n`: number of measurements in the sample
- Sample range :math:`R`: Range of a quantitative data set is equal to the largest measurement minus the smallest measurement.
- Mean :math:`x`: of a set of :math:`n` quantitative data is the sum of the measurements divided by the number of measurements.


I observe what I have collected (**descriptive statistics**)
to say something about the entire population (**inference statistics**).


