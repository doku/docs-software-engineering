=======================
Developer Documentation
=======================

**literate programming**

https://de.wikipedia.org/wiki/Literate_programming

The biggest difficulty with developer documentation is keeping it uptodate. When
time pressure increases the last thing developers thing about is updating or writing
developer documentation.

Documentation has to be part of your code base always reminding the developers to
update it. As with classes code and it's documentation should be loosely coupled and
have high coherence. When the code changes most likely the documentation has to change.

Documentation vision
~~~~~~~~~~~~~~~~~~~~
Fully featured doku editor embedded in source code with ability to add tables, screenshots
diagrams etc. Code describes it self.
