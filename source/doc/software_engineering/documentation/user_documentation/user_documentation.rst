==================
User Documentation
==================

Like laws software documentation should have a expiration date, after which it
has to be reactivated or updated.

The **User Documentation** of your software is part of the specification. The
documentation symbolizes a contract between you and your user. It defines which
input and precondition is needed to result in a desired output. You guarantee the
user that if he acts according to the documentation and enters the right inputs
he will achieve his goal and get the expected output. You have to make sure that
you keep your end of the contract. The user will be extremely unhappy if he gets
the wrong output even though he entered the right input. Therefore your user
documentation has to be tested. In classical black box tests

**User Documentation setup** The documentation is essential part of the application
and has to be treated exactly like production code. Therefor it is version controlled
ideally in the same repository as your application. Your documentation consists of
several articles all fulfilling basic standards in software engineering (DRY, short
files etc.). Each article explains a small feature. In the same file in somewhat natural
language should be a blackbox test defined using example data. The test should assert
that whatever you describe in your documentation actually is true to your software.
For Example if you write that *entering user name and password and clicking on login will open your dashboard*
you have to assert that exactly that happens.
If you write that *marking a checkbox in settings will now open the front page whenever you login*
you have to assert that with a test. So in case you change your application and you
change the default screen or remove this checkbox your tests will fail and remind
you to update tests and documentation.

These documentation tests will be part of your normal CI process. Whenever your
application or documentation changes they will be automatically tested.

The whole reason for putting this blackbox test code exactly in your documentation
files is that they will most likely have to be changed at the same time.
If your change the behavior of a feature you have to make sure your documentation
is still uptodate. In case it isn't the test case will fail and you not only have
to fix the test case but also change the documentation to describe the new functionality.

Especially with agile development it is easy to break something along the way. Your
documentation is a contract with your user you have to fulfill at any given time.
The User documentation tests will ensure that under any given time your application
behaves exactly as described in your manual.

Screenshots should be taken automatically and be inserted in the documentation.
These Screenshots can be captured when running the blackbox tests defined in the
test section of each article. Even with layout changes and new Stylesheets
your documentation is always up to date. You could also generate small video
sequences showing the user how a feature is used just by capturing your applications
behavior when tested and adding subtitles or a computer voice.

Your documentation should be as deeply embedded in your application as possible.
With machine learning you could predict when a user needs help and offer the right
documentation article. There is a very fine line between helpfulness and annoyance
of your user.

Context Sensitive User Documentation
====================================

A user performs a specific action in your application, but does not follow your
applications *ideal* path
for this specific goal. After the user has performed his action, you display him
a tutorial how he could have achieved his goal faster/more efficient.

This sort of user feedback/tips is especially useful if there is a shortcut
for this action and the user is not aware of it.

For example the user changes the color of a calendar entry and uses a configuration
in the settings panel. After the user changed the color of the entry, you know what
he tried to achieve and you can tell him that there is a faster way, if he just
right-clicks on the entry and can select the color.

To determine these suggestions use anonymized data from other users who achieved
the same outcome but used a different path through the application resulting in
a faster result/ less interactions until the achievement of the desired outcome.
In this way you can learn from your users about the fastest way through your
application and transfer the knowledge form power users to regular users who
don't know all the shortcuts. This will show you design problems in your UI.

Automated Screenshots
=====================
Your screenshots in the user documentation should be generated automatically.
You specify which view you want based on which dataset.

Based on this information the screenshots are captured automatically and
inserted into your documentation.

In this way your documentation will be up-to-date if you change you style sheets
etc. Additionally when some parts of your software are customizable you are able
to generate user specific documentation with screenshots identically to the
users real environment.

User Feedback
=============
When offering a user feedback or bug report site, have a direct button to add
screenshot. The user should be able to remove/hide any sensitive data in the
screenshot.
