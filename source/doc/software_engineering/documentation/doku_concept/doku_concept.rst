============
Doku Concept
============

Vision
======
Wiki-Website Dienst: Kunden liefern Artikel, diese werden in Website angezeigt.
Artikel können Nutzerbasiert bearbeitet werden, und hervorhebungen und Kommentare
können zu verschiedenen Abschnitten eingefügt werden.

Business Model
==============
B2B: für interne Nutzung, Artikel können auch ausgewählt online geschalten werden.

Komponents
==========

- rst to html parser
- user statistic analysis

Interface
=========

.. figure:: highlight.png
	 :alt: highlight

Allow users to highlight and comment on parts of the article and mark these
sections in your article.
