================
Software-Projekt
================

Software-Prozesse
=================
Um auch beim *programming in the large* systematisch erfolgreich zu sein, wurden
Prozessmodelle eingeführt.

Ein **Prozess** ist eine abstrakte Folge von Schritten, die beliebig vielen
Projekten zum Grunde liegt, ihnen als Muster, also Modell, dienen kann.

Ein **Projekt** ist die konkrete (real ausgeführte) Folge von Schritten, die ein bestimmtes,
konkretes Ergebnis haben.

Ein Prozess ist ein Schema um daraus Projekte zu bilden.

**Systematik und Dokumentation** sollten dafür sorgen, dass mit akzeptablem Aufwand
gute, fehlerarme Software entstehen.

Software-Projekte
=================
A temporary activity that is characterized by having a start date, specific
objectives and constraints, established responsibilities, a budget and schedule and
a completion date.

In der Realität hat kaum ein Projekt stabile Zielen und Randbedingungen.

Jedes Projekt hat einen "Erzeuger" (Eine Person oder Institution, die es initiiert
hat, typisch das höhere Management.)
Der **Projekteigentümer** ist der Erzeuger oder vertritt dessen Interessen. Ihm
ist der **Projektleiter** verantwortlich.

Jedes Projekt hat einen Zweck, ein Bündel von Zielen. Das wichtigste ist meist,
eine Software herzustellen oder zu verändern. Diese Software ist also das
Resultat des Projekts ist das **Produkt**. Andere wichtige Ziele: Erweiterung
des Know-hows, Bereitstellung von Bausteinen für spätere Projekte, Auslastung
der Mitarbeiter.

Das Produkt hat einen Abnehmer, den Kunden.

Projekttypen
============

.. glossary::

  Entwicklungsprojekt
      Ein Software-Produkt wird entwickelt, damit es später auf dem Markt angeboten
      werden kann. Der Auftraggeber ist die Marketingabteilung und das Projekt
      wird aus dem Entwicklungsbudget finanziert.

  Auftragsprojekt
      Der Software-Hersteller entwickelt die Software nach den Wünschen eines
      **externen  Auftragsgeber** Klare Kostenverteilung, expliziter Vertrag. Oft
      gibt Kunde Merkmale der Projekts vor, z.B. die einzusetzende Programmiersprache.
      Anforderungen sollten keine Methoden vorgeben, sondern nur die Ziele angeben.
      Mögliche Änderungen der vorgegeben Methoden müssen abgesprochen werden.
      Der Kunde kann etwas fordern, das für das Projekt eigentlich nicht relevant ist.
      Es gibt eine klare Verteilung der Interessen und Kompetenzen

  IT-Projekt
      Im Hause wird Software für den **eigenen Bedarf** entwickelt. Hersteller und
      Auftraggeber sind in derselben Organisation. Konflikte werden durch den
      gemeinsamen Vorgesetzten gelöst (oder nicht gelöst).

  Systemprojekts
      Der Kunde bestellt ein **komplexes System**. Das System enthält mehr oder
      minder viel Software (d.h. Software ist **Teil des Systems**). Die Softwareleute
      erbringen ihre Leistung also zusammen mit den übrigen Ingenieuren des Herstellers.

      Ähnlich: Entwicklung eines Projekts aus Hardware und Software.

      Bei **Embedded Systems** hat es höchste Qualitätsanforderungen, da es kaum
      Toleranz bei Ausfall und Fehlfunktionen gibt.

.. figure:: projecttypen.png
	 :alt: projecttypen

Formen der Teamorganisation
===========================

.. attention::
  **Conway's Law** Organizations which design systems are constrained to produce
  designs which are copies of the communication structures of these organizations.
  (Melvin Conway)

Software-Entwicklung ist ein arbeitsteiliger Prozess.

Je nach Größe des Projekts kann das Projektteam in mehreren Teams zerfallen.
Ein team sollte einen definierten Bereich bearbeitet werden.
Teams sollten höchstens aus fünf bis sieben Personen bestehen.
Größere Teams führen zur ungewünschten Gruppenbildung intern in Team.

Ein-Personen-Team
-----------------
Eine Person arbeitet sehr selbstständig an einer Aufgabe. Fast kein Kommunikationsaufwand
keine Dokumentationsdruck und Risiko des Ausfalls.

Gruppen aus 2 Personen
----------------------
Doppel: durch freien Beschluss der Beteiligten zur Zusammenarbeit. Keine Führungsrolle

Tandem: Ein Helfer ("Sherpa") unterstützt einen Spezialisten.
Sinn des Tandems kann es auch sein, dass Wissen das Spezialisten auf zwei Köpfe
zu Verteilen, dann ist der Sherpa der Schüler. Typisch für Pair Programming und Einarbeitung.

Anarchisches Team
-----------------
Die Entwickler arbeiten nach eigenen Vorgaben in der Regel autonom. Es gibt praktisch
keine Hierarchische Struktur. Es gibt ein Mangel der Führungsfähigkeit.
Die Entwickler sind selbst bestimmt und es gibt kaum bürokratische Hemmnisse.

Standards und Normen lassen sich nicht durchsetzen und die erforderlichen Resultate
sind Glückssache. Doe Organisation ist insgesamt nicht lernfähig.

Typisch für Organisationen mit schwacher Führungsstruktur.

Demokratisches Team
-------------------
Die Beteiligten sind grundsätzlich gleichberechtigt. Sie erzielen durch ausreichende
Kommunikation einen Konsens über Ziele und Wege, und sie verhalten sich diszipliniert.

Die Beteiligten müssen faktisch von den Voraussetzungen homogen sein. So ist es
schwierig neu eingestellte in das demokratische Team einzubinden.

Die Fähigkeiten der Beteiligten werden optimal genutzt und Probleme werden frühzeitig
erkannt und gemeinsam bekämpft.

Problem ist der hohe Kommunikationsaufwand und mögliche Fraktionsbildung, denn
es gibt keinen definitiven Vorgesetzten und Entscheider.


Hierarchisches Team
-------------------
Die Gruppe steht unter der Leitung einer Person, die für die Personalführung
und je nach Projektform auch für das Projekt selbst verantwortlich ist.

Hierarchische Teams sind die Normalform in Organisationen.

Der Gruppenleiter übernimmt :term:`Stabsfunktion` oder :term:`Linienfunktion`.

Es gibt eine einfach Kommunikationsstruktur, klare Zuständigkeiten und auf
der Mitarbeiterebene gibt es eine gute Ersetzbarkeit.

Es gibt relativ lange Kommunikationswege (und daher oft schlechte Information),
der Gruppenleiter stellt ein hohes Risiko dar; Gruppenmitglieder sind kaum zur
Kooperation motiviert.

Der Teamleiter ist im wesentlichen absorbiert durch seine Leitungsfunktion.
Er ist primär Manager. Die Mitarbeiter arbeiten fachlich. Der Leiter führt.

Chief-Programmer-Team
---------------------
Das Chief-Programmer-Team ist eine spezielle Variante des hierarchischen Teams.

Die wesentlichen Unterschiede sind die Differenzierung der Rollen in der Gruppe
und die Entwickler-Funktion des Chief-Programmers.

Die Gruppe besteht aus dem Chief-Programmer und seinem Stellvertreter, einem
Bibliothekar, der alle Verwaltungsfunktionen übernimmt, sowie aus einigen Programmierern

Vorbild ist das Operationsteam im Krankenhaus. Bei einer Operation führt der Chefarzt
die Operation durch und ist nicht nur der Vorgesetzte des Teams. Er ist also Leiter
des Teams und auch fachlicher Experte des Teams. Alle Vorarbeiten werden durch
die Mitarbeiter gemacht. Der Chefarzt macht nur den aller kritischsten und schwierigsten
Teil der Operation. Er regiert in der Operation diktatorisch.

In der Software gibt es also einen Programmierer, der fachlich besser ist als alle
anderen. Er bestimmt wie die Arbeit aufgeteilt wird und macht die aller schwierigsten
Teile selbst.

Dieses Konzept hängt besonders stark an den Mitarbeitern. Funktioniert das ganze
gut, kann man hiermit fantastische Ergebnisse erzielen. Die Gruppe kann (wie ein
Operationsteam) außerordentlich effizient arbeiten.

Die Struktur ist typisch für Open-Source-Projekten (mit Einschränkungen)

Ausgebildete Entwickler werden als Projektleiter eingesetzt und sind dann hin und her
gezogen zwischen Entwickler und Leitungsfunktion. Der Leiter hat unbewusst ein
Chief Programmer Team im Hinterkopf. Die Mitarbeiter hingegen erwarten ein Hierarchisches
Team.

Agile Prozesse
--------------
Traditionell hat man im Software Engineering gelehrt, dass man größere Projekte
sehr organisiert und gut geplant durchführt (Entwerfen der Spezifikation).

Bei Agilen Prozessen wird aus der Aufgabe eine kleine Teilaufgabe gewählt und
schließt diese relativ zügig fertig.

Ein Beispiel einer agilen Arbeitsstrategie ist Scrum (:ref:`ref-scrum-introduction`).

Vorteil: sehr viel schneller ein Erfolgserlebnis und schnelles Kunden Feedback
wodurch die Kosten von Fehlern geringer werden.

Kurze Zyklen in der Entwicklung.

Organisation der Software-Hersteller
====================================
Ein Hersteller hat eine Organisationsstruktur, die die Aufgabenverteilung und die
Beziehungen der Mitarbeiter untereinander vorgibt.

Diese Struktur bezeichnet man als Primärorganisation. Sie legt alle statischen Beziehungen
der Mitarbeiter fest. Dazu gehärt die Linienorganisation, also die Hierarchie.

Zusätzlich kann es eine Sekundärorganisation geben, insbesondere die Zusammenfassung
von Mitarbeitern in einem Projekt.

Funktionale Organisation
------------------------
Der Hersteller hat immer wieder ähnliche Aufgaben. Die Gruppe oder Abteilung besteht
aus Spezialisten für die einzelnen Teilaufgaben. Das Produkt entsteht durch das
Zusammenwirken der Abteilung.

Die Linienorganisation reicht aus, eine Sekundärorganisation ist nicht erforderlich.

Die Struktur sind projektunabhängig, es gibt keine Projekt! Jede Person hat eine
definierte (feste) Rolle. Die Zwischenresultate fließen von Abteilung zu Abteilung.

Vorbilder sind Fabrik mit Serienfertigung oder Behörden. Es gibt stabile Zuordnung
der Mitarbeiter. Alles was am Projektbegriff hängt fehlt und es gibt auch keine
persönlichen Ziele und Motivation.

Projekt-Organisation
--------------------
Hersteller muss eine spezielle Aufgabe unter vorgegebenen Bedingungen lösen.
Die Aufgabe wird einem Projektteam übertragen. Das Projektteam bildet eine temporäre
Sekundarstruktur in der (schwachen) Primärstruktur. Nach dem Abschluss des Projekts
wird die Struktur wieder aufgelöst.

Ein Projekt kann auf die Umgebung (z.B. sich ändernde Anforderungen) sehr schnell
reagieren. Der Projektleiter hat alle Kompetenzen um das Projekt zu führen. Das
Team ist auf das Projekt zugeschnitten und die Mitglieder identifizieren sich mit
dem Projekt. Erfolg oder Misserfolg ist leicht zuzuordnen und die Mitarbeiter sind hoch
motiviert Schwierigkeiten zu vermeiden oder zu überwinden.

Besonders der Start und das Ende des Projekts sind schwierig. Am Anfang braucht
man 3-4 sehr gute Leute und hat daher meist zu viele Mitarbeiter und später hat
man zu wenige.

Matrix-Organisation
-------------------
In großen Firmen will man weder die Trägheit der funktionalen Organisation
noch das Durcheinander der Projektorganisation. Jeder Mitarbeiter steht mit einem
Bein in der stabilen Linienorganisation, und mit dem anderen in (mindestens) einem
Projekt.

Die Primär- (Linienstruktur) und die Sekundärstruktur (Projekte) bilden also eine
Matrix. So erreicht man große Flexibilität und kann Spezialisten leicht einteilen.
Jeder Mitarbeiter hat eine stabile Heimat im Unternehmen.

.. figure:: matrix_organisation_beispiel.png
	 :alt: matrix_organisation_beispiel

Dadurch hat der Mitarbeiter mehrere Vorgesetzte und es kommt zu Zielkonflikten.
