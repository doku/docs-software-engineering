==========================
Software-Kosten und Nutzen
==========================

.. glossary::

	Kosten
		Alle Nachteile, die eine Lösung hat, seien sie in Geldwert auszudrücken oder
		nicht.

	Nutzen
		Jede Art von Vorteile die eine Lösung liefert.

Rational geht es im Projekt vor allem um die Minimierung von Kosten und
Maximierung von Nutzen.

Make or Buy
===========

:term:`Kosten` der Neuentwicklung werden in der Regel unterschätzt und zwar um so stärker,
je mehr neu entwickelt wird. Die Kosten der Entwicklung steigen meist während der
Entwicklung. Die Gesamtkosten stellen die Differenz zwischen :term:`Nutzen` und
:term:`Kosten` dar. Das übergeordnete Ziel ist also eine **Minimierung der Gesamtkosten**

Größte **Flexibilität** spricht für Eigenentwicklung oder Vergabe eines Auftrags.
Allerdings ist es meist unwirtschaftlich, eine **perfekte Lösung** anstreben, eine
einfachere ist oft der bessere Kompromiss.

Die **Planungssicherheit** steigt, wenn mehr fertige Komponenten eingesetzt werden.
Der Kauf wird unter Umständen billiger als ursprünglich erwartet, weil die
Preise der Software fallen oder ihr Funktionsumfang wächst.

Durch Kauf erhält man **rasch** eine relativ **ausgereifte** Software, in die höherer
Entwicklungsaufwand geflossen ist, als der einzelne Kunde bezahlen muss. Der Aufwand für
Wartung und Portierung ist geringer. Ein stabiler Lieferant ist sehr wichtig.

Es gibt keine Möglichkeit, diese Anforderungen exakt und vollständig zu erheben.
außerdem ändern sich die Anforderungen während der Entwicklung.

Kosten haben nur vordergründig mit Geld zu tun und sind ein viel allgemeinerer Begriff
der alle Arten von Nachteilen beschreibt und quantifiziert.

Migrationsaufwand zu neuer Software ist meist gigantisch.

Die Kosten sind vor allem Herstellungs- und Betriebskosten.
Um Kosten und Nutzen abzuschätzen lässt sich der **BreakEven** Point bestimmen.

.. figure:: breakevenpoint.png
	 :alt: breakEvenPoint

Es gibt viele Vorteile, wenn die Softwareentwicklung schnell ist:

 - Kaum ändernde Anforderungen
 - Zinsen ändern sich nicht so stark

Ein Projekt wird in viele Kleinteile zerlegt, sodass schon nach wenigen Wochen/Monaten
dem Kunden etwas geliefert werden kann.

Kosten der Entwicklung
======================

Eine Faustregel für die Kostenverteilung in der Entwicklung ist 40-20-40. Wobei:

- 40% des Budgets vor der Kodierung
- 20% für die Kodierung
- 40% für Test, Integration und Auslieferung

Je schlechter das Softwareengineering in einem Projekt, desto höher die Kosten der
Kodierung.

Bei modernen Vorgehen verschiebt sich der Aufwand Richtung Spezifikation und Entwurf

- 65% des Budgets vor der Kodierung
- 15% für die Kodierung
- 20% für Test, Integration und Auslieferung

Investiere am Anfang das Projekt in gründliche Vorarbeit. Das zahlt sich besonders
später in der Wartung aus. Die **Wartung** (Korrektur, Anpassung und Erweiterung) kostet
über die gesamte Lebensdauer eines Projekts typischerweise mehr als die Entwicklung.

Kostenverteilung in der Softwareentwicklung:

.. figure:: kostenverteilung.png
	 :alt: kostenverteilung

Heute ist der Anteil der Wartungskosten sogar noch deutlich höher und liegt bei ca. 80%.

Durch ein sauberes und solides Vorgehen in den frühen Phasen führt schon bei der Implementierung
und bei den Tests aber vor allem bei der Wartung zu massiven Einsparungen.

Die Integration kann mit sehr geringem Aufwand über die Bühne gehen, wenn die Planung
zuvor gründlich war.

.. figure:: softwarekosten.png
	 :alt: softwareKosten


Netto-Herstellungskosten
    Kosten um die Funktionalität herzustellen

Fehlerverhütungskosten
    Kosten von Standupmeetings
    erhöhter Aufwand für bessere Software

Prüf- und Nachbesserungskosten
    Bugfixes und testen

Fehlerkosten

Fehlerfolgekosten
    Schaden der durch mögliche Fehler entsteht.


Kosten der Qualitätssicherung hängt ab von den Risiken der Entwicklung.

Angehen eines Projekts:
        .  Suche nach Risiken und Kostenabschätzung dieser um dann zu entscheiden
        wieviel Aufwand für Qualitätssicherung angemessen ist.

Beziehung zwischen Fehlerentstehung und -Entdeckung
---------------------------------------------------

Die Programmentwicklung läuft bis zur Codierung **top-down**, d.h. vom Allgemeinen
(Aufgabe) zum Speziellen (Code).

Nach der Codierung **bottom-up**, von der Codezeile zum Gesamtsystem.

Fehler werden typisch auf derselben Abstraktionsebene entdeckt, auf der sie
entstanden sind.

.. figure:: fehlerwanne.png
	 :alt: fehlerWanne

Fehler die wir am Anfang der Entwicklung machen, werden in der Regel ganz am Ende
erst auffallen.

Fehlerkosten steigen mit der Latenzzeit exponentiell an. Die Kosten eines Fehlers
in der Spezifikation steigen etwa auf das 100-fache, wenn er nicht sofort sondern
erst nach der Auslieferung entdeckt wird.

.. figure:: kostenfehler.png
	 :alt: fehlerKosten

	 Latenzzeit: Zeit in der Fehler unentdeckt bleiben

Fehler in der Softwareentwicklung sind gewissermaßen wie ansteckende Krankheiten
und breiten sich aus.

Software muss daher so schnell wie irgend möglich überprüft werden, sodass Fehler
auffallen genau dann, wenn sie entstehen. Frühe Fehler führen zu gigantischen
Kosten. Fehler in der Analyse müssen daher unbedingt verhindert werden.
