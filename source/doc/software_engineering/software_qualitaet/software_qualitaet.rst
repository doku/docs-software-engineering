=================
Software-Qualität
=================

.. glossary::

  Qualität
    Die Gesamtheit von Eigenschaften und Merkmalen eines Produktes oder einer Tätigkeit,
    die sich auf die Eignung zur Erfüllung gegebener Erfordernisse beziehen.

Software-Qualität umfasst im Kern die Funktionalität.

Qualität hat immer zwei Enden. Das eine Ende liegt an dem Produkt selber. Das andere
Ende ist an dem Nutzer des Produkts. Qualität liegt immer im Auge des Betrachters.
Qualität ist daher kein absoluter Begriff.

.. _ref-taxonomie-software-qualitaet:

Taxonomie Software-Qualität
===========================
Man kann zwischen verschiedenen **Aspekten der Qualität** unterschieden.

Aussagen über die Software-Qualität können sich auf folgende Teilqualitäten beziehen:

- Entwicklungsprozess (**Prozess-Qualität**)
- das Produkt (**Produkt-Qualität**)

  * Qualität aus Sicht des Benutzers (**Gebrauchsqualität** oder **Äußere Qualität**)
    Sie besteht aus Eigenschaften wie Korrektheit, Performance, Benutzbarkeit und
    Sicherheit.

  * Qualität aus Sicht des Bearbeiters (**Wartungsqualität** oder **Innere Qualität**)
    Sie besteht aus Eigenschaften wie Lesbarkeit, Wartbarkeit, Erweiterbarkeit und
    Wiederverwendbarkeit.

.. glossary::

  external quality
    is  how well the system meets the needs of its customers and users.
    (is it functional, reliable, available, responsive etc.)

  internal quality
    is how well it meets the needs of its developers and administrators
    (is it easy to understand, easy to understand etc.)

Für eine Software die lange leben soll ist die Wartungsqualität wohl das wichtigste.
Der Kunde interessiert sich vordergründig nur für die Gebrauchsqualität.
Indirekt ist dem Kunden die Wartungsqualität trotzdem wichtig, um lange von der Software zu
profitieren.

.. figure:: qualitaetsgraph.png
	 :alt: qualitaetsGraph

Am Ende des Lebenszyklus wird Wartung wieder besonders wichtig. Da das
Nachfolger System wieder mit dem System kooperieren muss.

.. _ref-qualitaetenbaum-software:

Qualitätenbaum der Software
============================

.. figure:: qualitaetenbaum_software.png
	 :alt: qualitaetenbaum_software

Qualität hat sehr viele verschiedene Aspekte. Daher muss einzelnen Qualitätsmerkmal
höhere Priorität zugewiesen werden, möglichst schon in der Planungsphase des
Projekts. Diese Struktur hilft bei der Vollständigkeit der Spezifikation. So kann überprüft
werden dass alle Aspekte in Betracht gezogen wurden.

Qualitätsmodelle
================

Komplexere, vor allem **quantifizierte** oder wenigstens **bewertete** Modelle
können Hinweise zur **Bewertung einer Software** oder zur **Schwachstellenanalyse**
liefern.

.. glossary::

  technical depth
    :Verzinsung: (solange Schulden nicht abgetragen sind)  Das System ist schlechter
      und leistet weniger. Minderung des Nutzens
    :Tilgung: (Abtragen der Schulden) Reengineering, Neuangehen des Problems

    Technische Schulden müssen immer gering gehalten werden. Bei hoher Schuld
    ist man mehr beschäftigt mit innerer Komplexität als neue Features zu
    entwickeln.

Qualität zahlt sich aus. Gute Qualität ist auf lange Sicht billiger.

Wir haben 2 Qualitätsbegriffe in Kopf:

  - **transzendente Qualität**: intuitiver Qualitätsbegriff (wahrnehmbar) subjektiv
  - **modellierte Qualität**: messbare/beurteilbare Qualität (diskutierbar) objektiv

Technical Depth
===============

.. figure:: technical_depth_types.png
	 :alt: technical_depth_types

Qualitätssicherung
==================

Durch Prüfung lässt sich nur die Anwesenheit oder das Fehlen von Qualität feststellen.
Organisatorische und konstruktive Maßnahmen ermöglichen es hingegen die Qualität
zu erhöhen.

.. figure:: qualitaetssicherung_kategorien.png
	 :alt: qualitaetssicherung_kategorien

.. figure:: qualitaetssicherung_kategorien_all.png
	 :alt: qualitaetssicherung_kategorien_all

- Qualitätssicherung ist nicht nur die Aufgabe der Tester, alle Projektbeteiligten
  sind für die Qualität verantwortlich.
- Es ist besser einen Fehler zu vermeiden, als ihn zu entdecken und zu beheben.
- Je früher ein Fehler gefunden und behoben wird, desto besser.
- Qualität lässt sich nicht in das Produkt "hinein" prüfen.

Validierung: "Haben wir das richtige System entwickelt?"

Verifikation: "Haben wir das System richtig entwickelt?"

.. figure:: softwarepruefung.png
	 :alt: softwarepruefung

Wird ein Fehler entdeckt ist das Prüfresultat **positiv**. Liegt der Fehler im
Prüfzweig, so ist es **falsch positiv**. Werden Fehler nicht entdeckt, ist das
Prüfresultat **falsch negativ**. Falsch negative Resultate sind schlimmer als
keine Resultate, denn sie suggerieren falsche Sicherheit. Falsch positive Resultate
machen unnötige Arbeit, sind aber sonst harmlos. Daher ist es bei jeder Prüfung
weit wichtiger, falsch negative zu vermeiden.

.. figure:: pruef_resultat.png
	 :alt: pruef_resultat

Manuelle Prüfung
----------------

.. figure:: manuelle_pruefung_stages.png
	 :alt: manuelle_pruefung_stages

Beim Peer-Review wird das Prüfobjekt einem Kollegen zur Durchsicht gegeben.

Technisches Review
~~~~~~~~~~~~~~~~~~
Zuständigkeiten in Review sind klar definiert.

- Der Moderator leitet das Review und ist für den ordnungsgemäßen Ablauf verantwortlich
- Der Notar führt Protokoll
- Die Gutachter sind Kollegen, die den Prüfling begutachten können
- Der Autor ist der Urheber des Prüflings
- Die Review-Sitzung ist auf 2h beschränkt

Die Gutachter bekommen im technischen Review immer **konkrete Aufträge**. Dadurch
wird das ganze Spektrum abgedeckt und die Gutachter prüfen aufmerksamer, da sie nach
bestimmten Mängeln suchen.

Die Software-Einheit wird dezentral von mehreren Gutachtern inspiziert und in
einer gemeinsamen Sitzung werden Mängel zusammengetragen und dokumentiert.

.. figure:: ablauf_inspection.png
  :alt: ablauf_inspection

  Ablauf einer Inspection

Ziel des Review ist es, Fehler zu finden, nicht diese zu beheben.

Inspektionen sind effektiv und effizient. Der Aufwand einen Fehler zu entdecken
ist vergleichbar mit den Aufwänden üblicher Testmethoden.

Automatische statische Analyse
------------------------------

Analyse Tools (:ref:`ref-static-code-analysis`) können die manuelle Prüfung
ergänzen.

Zuverlässigkeit
===============

.. glossary::

  reliability
    The ability of a :term:`system` or :term:`component` to perform its required
    functions under stated conditions for a specified period of time.

Zuverlässigkeit ist die Kombination aus Korrektheit, Ausfallsicherheit und Genauigkeit.
