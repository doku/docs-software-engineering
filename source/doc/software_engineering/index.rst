====================
Software Engineering
====================

Zusammenfassungen und Vorlesungsnachbereitung zur Vorlesung "Einführung in die
Softwaretechnik" der Universität Stuttgart.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   basics/*
   software_qualitaet/*
   history_softwareengineering/*
   software_kosten_nutzen/*
   software_lifecycle/*
   software_projekt/*
   humans_in_se/*
   software_entwicklung/*
   projekt_leitung/index
   anforderungsmanagement/*
   requirement_analysis/*
   requirements_engineering/*
   spezifikation/*
   estimation/*
   agile/index
   usability/*
   gamification/*
   software_test/index
   automated_code_analysis/*
   implementierung/*
   clean_code/index
   refactoring/index
   software_architektur/index
   continuous_operations/index
   wartung/*
   legacy_code/index
   documentation/*
   stp/*
