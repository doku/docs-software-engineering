======================
Anforderungsmanagement
======================

Unterschied zwischen Theorie und Praxis: Theorie bestimmt ein ideales model. In
der Praxis sorgt der Faktor Mensch dafür, dass dieser Idealzustand nicht
erreicht werden kann.

Viele Projekte kämpfen mit Schwierigkeiten oder müssen abgebrochen werden.
ca. 30% aller Projekte sind erfolgreich. 20% scheitern und 50% sind "challenged".

Das große Problem im Projektgeschäft sind immer die Anforderungen.
Die meisten Projekte scheitern wegen der Anforderungsanalyse oder durch
Änderungen der Anforderungen.

Die richtige Anforderungsanalyse ist also entscheidend für den Projekterfolg.

.. glossary::
  Anforderungen
    Aussagen über zu erfüllende Eigenschaften oder eine zu erbringende Leistung

Es muss falsche Erwartungen vorgebäugt werden und mit Ausgrenzungen wird klar
festgelegt, welche Anforderungen ausdrücklich nicht im geplanten Leistungsumfang
sind.

Anforderungsanalyse ist gut investiertes Geld. Frühe Erkennung von Problemen
führt dazu dass diese deutlich günstiger verbessert werden können.
Man sollte 10% des Budgets für Anforderungsanalyse investieren.
Beheben von Fehlern einer ausgelieferten Software ist Faktor 500 teurer als
in der Planungsphase.

Anforderungen sind die Basis für alle Phasen eines Projekts zur Systemerstellung.
In jeder Phase müssen alle relevanten Anforderungen bekannt sein.

Anforderungen und Spezifikation
  Anforderungserhebung ist analytisch. Es werden viele atomare Einzelaspekte
  gesammelt. O-Ton der Kunden

  Spezifikation ist konstruktiv. Sie beschreibt eine ideale Maschine, die möglichst
  viele der Anforderungen erfüllt.

Anforderungen geben Kundenbedarf wieder und sind primär Prozessanforderungen.
Der Hauptzweck eines Projekts besteht immer darin, das Geschäft des Kunden zu
ermöglichen.

Anforderungen unterscheiden sich stark, je nachdem, ob sie aus Führungs-,
Kern- oder Supportprozess kommen. Der Kernprozess macht das Geschäft des Unternehmens
aus. Für den Kernprozess wird hautsächlich individual Software entwickelt.

Der Führungsprozess steuert die weiteren Prozesse im Unternehmen.
Der Supportprozess ist notwendig, damit die Kernprozesse laufen.
Die Anforderungen müssen erfasst werden, sodass Anforderungen für den einzelnen
Kunden passen, aber trotzdem für das Standardprodukt verwertet werden können.

Herausforderungen
=================

Die Fachbereiche haben nur eine laienhafte Vorstellung von der IT-Lösung seines
Problems. Sie kennen ihre Fachlichkeit und Geschäftsprozesse.

Die IT Abteilung kennt nicht die Fachlichkeit. Die Anforderungsanalyse soll als
Brücke zwischen den Abteilungen dienen.

Hinter Projekten stehen Menschen, die alle ihre Anforderungen und Bedürfnisse
befriedigt sehen möchten. Die Menschen haben unterschiedliche, oft berufsspezifische
Wertevorstellung und gewichten daher Anforderungen anders.

Mangelnde Zielorientierung
--------------------------
Im Projekt ist man oft mit dem Verlust der Zielorientierung konfrontiert. Oft
verliert man aus dem Auge wie das Projekt mit den Geschäftszielen und
Prozessanforderungen zusammen hängen.

Besonders Kompliziert wird es: wenn das eigentliche Projektziel ist, nicht
eine System-Implementierung, sondern die Änderung eines Geschäftsprozesses
(nicht kommuniziert). Auch rein politische Motive führen zu Schwierigkeiten bei
Projekten.

Erkennen was den Kunden zufriedenstellt
---------------------------------------
Es genügt oft nicht nur die gestellten Anforderungen zu erfüllen.

.. figure:: kano_anforderungen.png
	 :alt: kano_anforderungen

Begeisterungsfaktoren: dem Kunden unbewusst, oft irgendwie erwartet.

Leistungsfaktoren: Explizit (bewusst) gefordert

Standard-/ Basisfaktoren: Vom Kunden selbstverständlich vorausgesetzt
(unterbewusst), daher oft nicht (vollständig) gestellt. Unzufriedenheit
hoch wenn diese nicht erfüllt werden.

Missverständliche Formulierung
------------------------------
Leicht können Anforderungen missverständlich formuliert werden, und dadurch
bei verschiedenen Beteiligten unterschiedliche Reaktionen hervorrufen.

Folgendes sollte vermieden werden:

- Tilgung: (selektive Reduktion der Wahrnehmung)
- Generalisierung: (Verallgemeinerung einer einzelnen Erfahrung)
- Verzerrung

Formulierungsregeln Anforderungen

- Formulierungen aktiv
- keine unvollständigen Vergleiche
- keine fehlenden Definitionen

Anforderungen sind manchmal so detailliert formuliert, dass der Eindruck entsteht,
sie seien mit dem Fachbereich abgestimmt.

Anforderungen widersprechen sich
--------------------------------
Anforderungen werden über einen langen Zeitraum erfasst und können sich selbst
widersprechen.

Die Anforderungen stehen oft auch im starken Gegensatz zu den gesetzten
Rahmenbedingungen.

Zeit Budget, Funktionalität

Veränderte Anforderungen
------------------------
Neue Rahmenbedingungen oder Fehler in den ursprünglichen Anforderungen führen zu
ändernden Anforderungen. Anforderungen sind dynamisch.
Das Geschäft des Kunden entwickelt sich weiter.

Ein Umsetzungsprojekt ist auf die Stabilität der Anforderungen angewiesen.

Warum ist Anforderungsmanagement so wichtig?
============================================

Wie halte ich Anforderungen so stabil das ich ein erfolgreiches Projekt umsetzen
kann?

Versionierung der Anforderungen damit der aktuelle Stand nachvollzogen werden kann.

**Qualitätssicherung** der Anforderungen: Vollständigkeit

Das Dilemma zwischen Stabilität des Release-Umfangs und neuen Erkenntnissen wird
über Change-Request-Verfahren gelöst.

Statusmodell für Anforderungen

Agile Entwicklung
-----------------
Die agile Umsetzung in Software-Projekten bringt weitere Ansprüche an das
Anforderungsmanagement mit sich.

Frühzeitige und bruchlose Übergänge in der Realisierung gemäß SRCUM:

Agile Projekte haben einen höheren Aufwand, liefern aber früher Ergebnisse.

Nichtfunktionale Anforderungen
------------------------------

- Architekturvorgaben
- Technische Vorgaben
- Performance Vorgaben

Nichtfunktionale Anforderungen müssen am Anfang einmal eingetragen werden und
können im Projekt nur sehr schwer später verändert werden.
Sie determinieren sehr stark wie die Anwendung gestaltet werden kann.
Man muss sich früh ein Bild machen, was die nicht funktionalen Anforderungen sind.

Es gibt keine Fachbereiche, die für nichtfunktionale Anforderungen zuständig sind.
Es ist daher schwer diese zu bekommen und schwer zu verifizieren.

Aus IT-Sicht ist entscheidend, dass mit neuen Releases nicht nur der Funktionsumfang
erhöht, sondern die IT-Landschaft bewusst weiterentwickelt wird. Technisch und
Strategisch.

Architektur-Frameworks sind eine wertvolle Hilfe darin, große Vorhaben zu
strukturieren und zu führen.

In dem Architekturframework werden die grundlegende Strukturen gesammelt.

Kern von TOGAF ist ein Vorgehensmodell für die Gestaltung von Geschäfts und
IT-Architektur.

**Architecture-Development-Process**
