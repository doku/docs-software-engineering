===================
Anforderungsanalyse
===================

.. hint::
  The hardest single part of building a software system is deciding precisely
  what to build. No other part of the conceptual work is as difficult as
  establishing the detailed technical requirements. No other part of the work
  so cripples the resulting system if done wrong. No other part is as difficult
  to rectify later. (Fred Brooks 1987)

.. figure:: anforderungen_project_cycle.png
	 :alt: anforderungen_project_cycle

Die Anforderungen werden erhoben und geprüft.
Alle Ergebnisse der Softwareanalyse werden in der Spezifikation
(:ref:`ref-specification`) festgehalten. Die wird dann im Entwurf umgesetzt und
schließlich implementiert und getestet. Das Resultat geht zurück an den Kunden.
Der Klient bekommt nur dann, was er haben will, wenn seine Anforderungen sorgfältig
erhoben und unterwegs nicht verfälscht wurden.

.. glossary::

  requirement
    #. A condition or capability needed by a user to solve a problem or achieve an
       objective.
    #. A condition or capability that must be met or possessed by a system or system
       component to satisfy contract, standard, specification, or other formally imposed
       documents.

    (IEEE 610.12)

  requirement analysis
    #. The process of studying user needs to arrive at a definition of system,
       hardware, or software requirements.
    #. The process of studying and refining system, hardware, or software
       requirements.

    (IEEE 610.12)

Ziel der Analyse ist es den **Soll-Zustand** festzustellen. Die Stärken und
Schwächen den **Ist-Zustands** müssen bestimmt werden. Bei der Kundenbefragung
werden meist nur Schwachpunkte des aktuellen Systems genannt. Implizit erwartet
der Kunde aber, dass alles was bisher akzeptabel oder gut war, unverändert bleibt
oder besser wird. Die Anforderungen des neuen Systems bestehen also nur zum kleinen
Teil aus Änderungswünschen, die allermeisten Anforderungen gehen in Richtung
Kontinuität. Daher ist die Ist-Analyse, also die Feststellung des bestehenden
Zustands und der aktuelle Prozesse, von großer Bedeutung für die erfolgreiche
Projektdurchführung.

.. figure:: anforderungs_quellen.png
  :alt: anforderungs_quellen

  Anforderungsquellen

Allgemein kann man die Anforderungen am Qualitätenbaum (:ref:`ref-qualitaetenbaum-software`)
festmachen. Jede Forderung nach einer bestimmten Qualität ist eine Anforderung.

.. note::
  Silent assumptions and requirements both from client and from the developers
  are very dangerous for the success of the project.

Der Auftragnehmer erstellt zunächst ein **Lastenheft** welches die Anforderungen
sammelt. Es beschreibt alle Anforderungen aus Sicht des Auftraggebers. Es ist
abstrakt und problemorientiert.

Der potentielle
Auftragnehmer erstellt eine Leistungsbeschreibung und schließlich ein **Pflichtenheft**
(oder Anforderungsspezifikation). Dieses konkretisiert alle im Lastenheft erhaltenen
Anforderungen und beschreibt die angestrebten Lösungsansätze. Das Pflichtenheft
ist Vertragsbestandteil bei der Auftragsvergabe. Es muss daher präzise, konsistent
und messbar (Es muss bestimmt werden können ob die Anforderungen tatsächlich erfüllt
wurden.) sein.

.. figure:: analyse_pipeline.png
	 :alt: analyse_pipeline

Die unstrukturierte Sammlung von Anforderungen muss gruppiert und
priorisiert werden.

Analysetechniken
================

.. figure:: analysetechnik.png
	 :alt: analysetechnik

#. Vorhandene Daten auswerten. Welches System wird bisher verwendet?
#. Beobachtung des Alltäglichen Betriebs.

Das Erstellen von User Stories (:ref:`user-story`) zusammen mit dem Kunden
kann klare Vorstellungen zu dem System schaffen und dokumentieren.

Analyse Fragen
--------------

#. Begriffe
#. Daten und Formate
#. Datenmodell
#. Benutzer und Nutzerrechte
#. Technik
#. Juristische Aspekte (Dürfen wir überhaupt machen was der der Kunde fordert?)
   Unter welchen Lizenzen soll das Projekt veröffentlicht werden?
#. Userinterface
#. Lieferumfang (Ist der Quellcode Teil des Lieferumfangs?)
#. Übersetzungen und Lokalitäten
#. Vertragliche Rahmenbedingungen (Auslieferungszeitpunkt, Kosten)
#. Mengengerüst (Anzahl der Datensätze, Nutzer, Größe eines Datensatz -> Maximalgrößen)
#. Ist-Analyse (Welche Systeme können wir wieder verwenden?)
#. Widersprüche
#. Zuverlässigkeit des Systems
#. Fehlertoleranz
#. Effizienz
#. NFR (Non Functional Requirements)

Begriffslexikon
===============
Während der Analyse wird das Begriffslexikon angelegt.
Das Begriffslexikon definiert  wichtige fachliche Begriffe in natürlicher
Sprache. Es nennt Synonyme, missverständliche Verwendungen und grenzt den
Begriff von anderen Begriffen ab.
So können Missverständnisse zwischen Entwicklern und Kunden verhindert werden.

Ein Eintrag sollte folgende Informationen enthalten.

#. Begriff und Synonyme (im Sinne der Spezifikation)
#. Bedeutung (Definition und Erklärung)
#. Abgrenzung (wo ist der Begriff nicht anzuwenden?)
#. Gültigkeit (zeitlich, räumlich, sonst)
#. Fragen der Beziehungen, Eindeutigkeit
#. Unklarheiten, die noch nicht beseitigt werden konnten
#. Querverweise zu verwandten Begriffen

Das Begriffslexikon wird während der gesamten Software-Entwicklung verwendet und
ergänzt.


Begriffsmodell
==============
Ein Begriffsmodell definiert die Zusammenhänge zwischen den fachlichen Begriffen.
Oft wird hierfür ein UML (:ref:`ref-uml`) Klassendiagramm mit Multiplizitäten und beschrifteten
Relationen angegeben.
