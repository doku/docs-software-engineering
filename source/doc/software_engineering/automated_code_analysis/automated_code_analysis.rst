=========================
Automatische Code Analyse
=========================

.. _ref-static-code-analysis:

Statische Code Analyse
======================

Automatische statische Analyse ist die Analyse von Software durch Software ohne
die Ausführung des Prüfobjekts. Im allgemeinen ist sie durch ihre hohe
Automatisierbarkeit effizient, aber liefert auch viele Falschpositive. Laut
Studien finden statische Analyse tools nur Untermengen der Fehler, die bei Reviews
gefunden werden. Sie finden außerdem andere Fehlertypen als dynamische Tests.

.. figure:: statische_analyse_kategorien.png
	 :alt: statische_analyse_kategorien

Statische Code Analyse lässt sich billig im CI Prozess ausführen und hilft
die *Grundhygiene* eines Softwaresystems Sicherzustellen.

Mögliche Tools sind `Sonar`_.

.. _Sonar: https://www.sonarqube.org/

Stylechecker
------------

Fehlermuster
------------

Syntaktische Fehler können gut mit Fehlermustertools erkannt werden.
Semantische Analyse sind

Statische Analyse hat oft mehr Auswirkungen auf Wartbarkeit und kaum Überschneidung
mit Fehlern die in Tests gefunden werden.

FindBugs ist hier ein gutes Tool für Java.

Kontrollfluss und Datenfluss
----------------------------

Abstrakte Interpretation
------------------------
