======
Matrix
======

Das Volumen des von den Spalten vektoren aufgespannten Spates ist die Determinante.

Hadamard-Ungleichung.


Orthogonale Matrizen
====================

:math:`Q \in \mathbb{R}^{n \times n}` heißt orthogonal, wenn

.. math:: Q^T Q = I

Ist :math:`Q` eine Orthogonale Matrix, so bilden die Zeilen und die Spalten
jeweils eine Orthonormalbasis.

Orthogonale Matrizen sind längenerhaltend.

.. math::

.. math::

  \det(Q^T Q)  = \det(I) = 1
  \det(Q^T)*\det(Q) = \det(Q)
