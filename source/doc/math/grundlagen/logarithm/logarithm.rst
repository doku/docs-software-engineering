===========
Logarithmus
===========

Rechenregeln
============

.. csv-table::

  "Basiswechsel", ":math:`\log_a(P) = \frac{\log_b(P)}{log_b(b)}`"
