===========
Abbildungen
===========

.. glossary::

  injektiv
    Jedes Element der Bildmenge wird höchstens einmal getroffen.

    .. math:: \forall x, y \in M f(x) = f(y) \Rightarrow x=y

  surjektiv
    Jedes Element der Bildmenge wird mindestens einmal getroffen.

    .. math:: \forall y \in N

    Wobei :math:`f: M \rightarrow N`

  bijektiv
