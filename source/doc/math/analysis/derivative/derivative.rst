=========
Ableitung
=========

.. math::

  -1 < x < 1

  \frac{d}{dx} \arcsin{x} = \frac{1}{\sqrt{1-x^2}}

  \frac{d}{dx} \arccos{x} = - \frac{1}{\sqrt{1-x^2}}

  \frac{d}{dx} \arctan{x} = - \frac{1}{1+x^2} = - \sin^2{(\arctan{x})}
