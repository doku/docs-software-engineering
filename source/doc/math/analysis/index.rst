========
Analysis
========

.. toctree::
  :maxdepth: 2
  :glob:

  folgen_reihen/*
  derivative/*
  integral/*
