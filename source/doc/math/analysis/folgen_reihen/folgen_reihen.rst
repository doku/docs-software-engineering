=================
Folgen und Reihen
=================

.. glossary::

  Supremum
    kleinste obere Schranke einer Folge

  Infimum
    größte untere Schranke einer Folge

  Harmonische Zahlen
    :math:`H_n := \sum_{k=1}^n \frac{1}{k} \leq \ln(n) + 1`

Eine konvergente Folge ist beschränkt.

Reihen
======

.. _geometrische-reihe:

Geometrische Reihe
------------------

Für :math:`|q|<1` gilt:

.. math:: \sum_{k=0}^{\infty} q^k = \frac{1}{1-q}

Partialsumme
~~~~~~~~~~~~

:math:`i`-te Partialsumme:

.. math:: \sum_{k=0}^i q^k = \frac{1-q^{i+1}}{1-q}

Ableitungen der Geometrischen Reihe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Erste Ableitung

.. math::

  \sum_{k=1}^{\infty} k q^{k-1} = \frac{d}{dq} \left( \sum_{k=0}^{\infty} q^k \right)
  = \frac{d}{dq} \left( \frac{1}{1-q} \right)
  = \frac{1}{(1-q)^2}

Zweite Ableitung

.. math::

  \sum_{k=1}^{\infty} k(k-1) q^{k-2} = \frac{d^2}{dq^2}\left(\sum_{k=0}^{\infty} q^k \right)
  = \frac{d^2}{dq^2} \left( \frac{1}{1-q} \right)
  = \frac{1}{(1-q)^2}


Taylor
======

.. math:: T_n(f,x,x_0) = \sum_{k=0}^n \frac{f^{(k)}(x_0)}{k!}(x-x_0)^k
