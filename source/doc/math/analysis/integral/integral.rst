========
Integral
========

Generalsubstituion
-------------------

.. math::
  t = tan(\frac{x}{2})
  
  dx = \frac{2}{1+x^2}dt

  sin x = \frac{2t}{1+t^2}

  cos x = \frac{1-t^2}{1+t^2}
