==========================
Erwartungswert und Varianz
==========================

Erwartungswert
==============

Sei :math:`(\Omega, P)` ein endlicher W-Raum und :math:`X: \Omega \rightarrow \mathbb{R}`
eine Zufallsvariable. Dann ist der Erwartungswert von :math:`X`:

.. math::

  EX =  E(X) = \sum_{\omega \in \Omega} X(\omega) * P(\{ \omega \}) = \sum_{x \in X(\Omega)} x * P(X=x)

Eigenschaften des Erwartungswerts
---------------------------------

#. Der Erwartungswert ist linear: Für Zufallsvariablen :math:`X_i: \Omega \rightarrow \mathbb{R}`
   und :math:`\alpha_i \in \mathbb{R}, 1 \leq i \leq n` gilt:

   .. math:: E \left( \sum_{i=1}^n \alpha_i X_i \right) = \sum_{i=1}^n \alpha_i E(X_i)

#. Erwartungswert einer Indikatorfunktion: :math:`E(1_A) = P(A)`
#. Für Zufallsvariablen :math:`X,Y` gilt: :math:`\forall \omega \in \Omega : X(\omega) \leq Y(\omega) \Rightarrow E(X) \leq E(Y)`

Multiplikationsregel für Erwartungswerte: Für :ref:`unabhaengige-zufallsvariablen` auf
einem W-Raum :math:`(\Omega, P)` gilt:

.. math:: E(X * Y) = E(X)*E(Y)

Varianz
=======

Sei :math:`(\Omega, P)` ein endlicher W-Raum und :math:`X: \Omega \rightarrow \mathbb{R}`
eine Zufallsvariable. Dann ist die Varianz von :math:`X`:

.. math:: V(X) := E((X-E(X))^2) = E(X^2) - (E(X))^2

Standardabweichung: :math:`\sigma(X) := \sqrt{V(X)}`

Eigenschaften der Varianz
-------------------------

#. Für alle Zufallsvariablen :math:`X` und :math:`a,b \in \mathbb{R}` gilt:
   :math:`V(aX+b) = a^2 V(X)`

Für :ref:`unabhaengige-zufallsvariablen` :math:`X_1, ..., X_n` gilt:

.. math:: V \left( \sum_{j=1}^n X_j \right)

Tschebyscheff-Ungleichung
-------------------------

Für eine Zufallsvariable :math:`X` auf einem endlichen W-Raum :math:`(\Omega, P)`
und jedes :math:`\epsilon > 0` gilt:

.. math::

  P(|X - E(X) | \geq \epsilon) \leq \frac{1}{\epsilon^2} V(X)

.. _markov-ungleichung:

Markov-Ungleichung
------------------

Sei :math:`X` eine ZV mit :math:`X(\omega) \geq 0 \forall \omega \in \Omega` und
:math:`E(X) > 0` dann gilt:

.. math:: \forall \lambda > 0: P(X \geq \lambda * E(X)) \leq \frac{1}{\lambda}

.. attention:: Die Markov Ungleichung gilt nur auf positiven Zufallsvariablen.
