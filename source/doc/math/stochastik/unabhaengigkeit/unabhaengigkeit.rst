==============
Unabhängigkeit
==============

Unabhängigkeit zweier Ereignisse
================================
In einem endlichen W-Raum :math:`(\Omega, P)` heißen zwei Ereignisse :math:`A, B \subset \Omega`
genau  dann (stochastisch) unabhängig, wenn:

.. math:: P(A \cap B) = P(A) * P(B)


.. _unabhaengige-zufallsvariablen:

Unabhängige Zufallsvariablen
============================
Zufallsvariablen :math:`X_1, ..., X_n: \Omega \rightarrow \mathbb{R}` auf einem
endlichen W-Raum :math:`(\Omega, P)` heißen (stochastisch) unabhängig, wenn für
alle :math:`B_1 \subset X_1(\Omega), ..., B_n \subset(\Omega)` gilt:

.. math:: P(X_1 \in B_1, X_2 \in B_2, ..., X_n \in B_n) = \prod^n_{i=1} P(X_i \in B_i)

.. math:: \forall x_1, ..., x_n: P(X_1 = x_1, ..., X_n =  x_n) = \prod^n_{i=1} P(X_i = x_i)
