.. _kombinatorik:

============
Kombinatorik
============

.. csv-table:: Zufälliges Ziehen von :math:`k` aus :math:`n` Kugeln
  :header: "", "ohne Reihenfolge", "mit Reihenfolge"

  "ohne Zurücklegen", ":math:`\binom{n}{k}`", :math:`k! \binom nk`
  "mit Zurücklegen", ":math:`\binom{n+k+1}{k}`", ":math:`n^k`"

.. math:: \binom{n}{k} = \frac{n!}{k!* (n-k)!}
