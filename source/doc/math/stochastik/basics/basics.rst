==========
Grundlagen
==========

.. glossary::
  
  Unvereinbarkeit
      Zwei disjunkte Ereignisse :math:`A` und :math:`B` (:math:`A \cap B = \emptyset`)
      heißen auch unvereinbar.

  Indikatorfunktion
      .. math::

        1_A : \Omega \rightarrow \mathbb{R}, 1_A(\omega) =
            \begin{cases}
              1 \text{ falls } \omega \in A \\
              0 \text{ sonst}
            \end{cases}

Zufallsvariablen
================
Eine ZV eines W-Raums ist eine Abbildung :math:`X: \Omega \rightarrow \mathbb{R}`

.. math:: X^{-1}(A)=\{ \omega \in \Omega : X(\omega) \in A \} \subset \Omega

Endlicher Wahrscheinlichkeitsraum
=================================

Definition
----------

Ein endlicher W-Raum ist ein Paar :math:`(\Omega, P)` mit einer endlichen
nichtleeren Menge :math:`\Omega` (dem Grundraum) und einer Abbildung
:math:`P: \mathcal{P}(\Omega) \rightarrow \mathbb{R}` (der W-Verteilung) mit

#. Nichtnegativität: :math:`\forall A \subset \Omega: P(A) \geq 0`
#. Normiertheit: :math:`P(\Omega) = 1`
#. Additivität im Fall unvereinbarer Ereignisse:
   :math:`\forall A,B \subset \Omega, A \cup B: P(A + B) = P(A) + P(B)`

Folgerungen
-----------
#. Das unmögliche Ereignis hat die Wahrscheinlichkeit :math:`0`: :math:`P(\emptyset) = 0`
#. Endliche Additivität von unvereinbaren Ereignissen :math:`A_i`:
   :math:`P \left( \sum_{i=1}^n A_i \right)= \sum_{i=1}^n P(A_i)`
#. :math:`\forall A \subset \Omega: 0 \leq P(A) \leq 1`
#. Komplement-Wahrscheinlichkeit: :math:`\forall A \subset \Omega: P(\bar{A}) = 1 - P(A)`
#. Monotonie: :math:`A \subset B \subset \Omega \Rightarrow P(A) \leq P(B)`
#. Additionsgesetz nicht notwendigerweise disjunkter Mengen:
   :math:`\forall A_1, ..., A_n \subset \Omega: P(A \cup B) = P(A) + P(B) - P(A \cap B)`
#. Subadditivität für nicht notwendigerweise disjunkte Mengen:
   :math:`\forall A_1, ..., A_n \subset \Omega: P \left( \bigcup_{i=1}^n A_i \right) \leq \sum_{i=1}^n P(A_i)`
#. Siebformel Für alle :math:`A_1, A_2. A_3 \subset \Omega`

   .. math::

      P(A_1 \cup A_2 \cup A_3) &= P(A_1) + P(A_2) + P(A_3) \\
                             &- P(A_1 \cap A_2) - P(A_1 \cap A_3) - P(A_2 \cap A_3) \\
                             &+ P(A_1 \cap A_2 \cap A_3)

Verteilungsfunktion
-------------------

Jedes nichtleere Ereignis :math:`A \subset \Omega` kann man als die disjunkte
Vereinigung von Elementarereignissen schreiben:

.. math::

  A &= \sum_{\omega \in \Omega} \{\omega \} \\
  P(A) &= \sum_{\omega \in A} p(\omega)

Für eine Zufallsvariable :math:`X : \Omega \rightarrow \mathbb{R}` heißt :math:`P^X`
**Verteilung** von :math:`X`.

.. math::

  & P^X : \mathcal{P}(X(\Omega)) \rightarrow \mathbb{R} \\
  & P^X(B) := P(X^{-1}(B)) \text{ für } B \subset X(\Omega) \\
  & \forall B \subset X(\Omega): P^X(B) = \sum_{x \in B} P( \{ X = x \})

Für eine Zufallsvariable :math:`X` heiße die Funktion :math:`F^X` die
Verteilungsfunktion von :math:`X`

.. math::

  F^X &: \mathbb{R} \rightarrow \mathbb{R} \\
  F^X(x) &:= P(X \leq x)

Eine Verteilungsfunktion ist monoton wachsend und legt die Wahrscheinlichkeiten
für beliebige Intervalle :math:`(a, b]` fest.

.. math:: P(a < X \leq b) = P(X \leq b) = P(X \leq a) = F^X(b) - F^X(a)

Laplace Experimente
-------------------

:math:`(\Omega, P)` heißt **Laplacescher W-Raum** und :math:`P` die (diskrete)
Gleichverteilung, falls alle Ergebnisse :math:`\omega \in \Omega` die selbe
Elementarwahrscheinlichkeit :math:`p(\omega)` haben, also:

.. math:: \forall \omega \in \Omega: p(\omega) = \frac{1}{|\Omega|}

mit

.. math:: \forall A \subset \Omega: p(\omega) = \frac{|A|}{|\Omega|}

"Anteil der günstigen Ergebnisse an der Grundgesamtheit"

Für die Bestimmung der Elementarwahrscheinlichkeiten :math:`P^X(\{x\})=P(X=x)`
müssen wir nur :math:`|X^{-1}(x)|` bestimmen. Dabei helfen kombinatorische
Argumente (:ref:`kombinatorik`).
