==============
W-Verteilungen
==============

Binomial Verteilung
===================

Bernoulli Experiment
--------------------

Eine Zufallsvariable :math:`X` mit Wertebereich :math:`X(\Omega) = \{ 0, 1 \}`
und Verteilung :math:`P(X=1)=p` mit :math:`0 \leq p \leq 1` heißt
Bernoulli-Experiment mit Trefferwahrscheinlichkeit :math:`p`.

Es gilt:

.. math:: E(X) = p

.. math:: V(X) = p(1-p)

Definition
----------

Eine Zufallsvariable :math:`X` heißt **binomial verteilt**, mit Parametern
:math:`n` und :math:`p`, kurz:

.. math:: X \sim Bin(n,p)

wenn sie die Bildmenge :math:`\{ 0,1,2, ..., n\}` hat und die Verteilung:

.. math:: P(X=k) = \binom{n}{k} p^k (1-p)^{n-k}

Folgerungen
-----------

Wenn :math:`X \sim Bin(n,p)`, dann gilt:

.. math:: E(X) = np

.. math:: V(X) = np (1-p)

.. _geometrische-verteilung:

Geometrisch Verteilung
======================

Die Zufallsvariable zählt die Wahrscheinlichkeitsverteilung der Anzahl
der Fehlversuche vor dem ersten Erfolg.

Definition
----------

Eine Zufallsvariable :math:`X` heißt geometrisch verteilt mit Parameter :math:`p`
(:math:`0 < p < 1`), kurz:

.. math:: X \sim G(p)

wenn sie folgende Verteilung hat bei :math:`k \in \mathbb{N}_0`:

.. math:: P(X=k) = (1-p)^k p

Folgerungen
-----------

Wenn :math:`X \sim G(p)`:

- :math:`E(X)=\frac{1-p}{p} = \frac{1}{p}-1`
- :math:`V(X)=\frac{1-p}{p^2}`

Für die Verteilungsfunktion :math:`F^X` gilt:

.. math:: F^X(i) = P(X \leq i) = \sum_{k=0}^i P(X=k) = 1 - (1-p)^{i+1}

Geometrisch verteilte Zufallsvariablen sind *gedächtnislos*:

.. math:: P(X \geq x+y | X \geq x) = P(X \geq y)

Poisson Verteilung
==================

Die Poisson-Verteilung (benannt nach dem Mathematiker Siméon Denis Poisson) ist
eine Wahrscheinlichkeitsverteilung, mit der die Anzahl von Ereignissen modelliert
werden kann, die bei konstanter mittlerer Rate unabhängig voneinander in einem
festen Zeit Intervall oder räumlichen Gebiet eintreten.

Dabei geben wir den Erwartungswert vor und fordern, dass es im Mittel :math:`\lambda`
Ereignisse pro Zeitintervall geben soll.

Die Poisson Verteilung erhalten wir als Grenzwert einer Folge von Binomialverteilungen.
Gedanklich betrachten wir das Einheitsintervall :math:`[0, 1]`, für das wir im
Mittel :math:`\lambda` Ereignisse fordern, und unterteilen es in :math:`n`
Teilintervalle. In jedem davon führen wir ein Bernoulli-Experiment mit der
Erfolgswahrscheinlichkeit ("Eintritt des Ereignisses") :math:`p_n` durch. Wir führen
Zufallsvariablen

.. math:: X_n \sim Bin(n, p_n)

ein, die Bernoulli-Kette der Länge :math:`n` darstellen und die Zahl der Ereignisse
zählen. Da :math:`E(X_n) = \lambda` gilt :math:`p_n = \frac{\lambda}{n}`


Definition
----------

Eine Zufallsvariable :math:`X` heißt Poisson-verteilt mit Parameter :math:`\lambda`
(:math:`\lambda > 0`), kurz:

.. math:: X \sim Po(\lambda)

wenn sie folgende Verteilung hat für :math:`k \in \mathbb{N}_0`

.. math:: P(X = k) = e^{- \lambda} \frac{\lambda^k}{k!}

Folgerungen
-----------

Wenn :math:`X \sim Po(\lambda)` dann gilt: :math:`E(X) = V(X) = \lambda`

Wenn :math:`X \sim Po(\lambda)` und :math:`Y \sim Po(\mu)` mit unabhängigen
Zufallsvariablen :math:`X` und :math:`Y`, so gilt das Additionsgesetz:
:math:`X+Y \sim Po(\lambda + \mu)`
