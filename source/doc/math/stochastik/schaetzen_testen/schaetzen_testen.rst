=========================
Schätzprobleme und Testen
=========================

Schätzer
========


Maximum-Likelihood
------------------


.. math:: L_k(p) := P_p(S_n = k)

.. math:: L_k(p^*) := \max_{0 \leq p \leq 1} L_k(p)


Für Binomialverteilte :math:`S_n` gilt :math:`p^* = \frac k n`
