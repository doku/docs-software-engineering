==============
Grenzwertsätze
==============

Gesetz der Großen Zahlen
========================

Schwaches Gesetz der großen Zahlen
----------------------------------

Sei :math:`(\Omega, P)` ein diskreter W-Raum, :math:`X_1, X_2, ...` eine Folge
unabhängiger Zufallsvariablen auf :math:`\Omega` mit gleichem Erwartungswert:

.. math:: \mu := E(X_1) = E(X_2) = ...

und mit gleicher Varianz (mit Bezeichner :math:`\sigma^2`):

.. math:: \sigma^2 := V(X_1) = V(X_2) = ...

:math:`M_n` das arithmetische Mittel der ersten :math:`n` Zufallsvariablen

.. math:: M_n := \frac{1}{n} \sum_{j=1}^n X_j

und :math:`\epsilon > 0` dann gilt:

.. math:: \lim_{n \rightarrow \infty} P(|M_n - \mu| \geq \epsilon) = 0

Zentraler Grenzwertsatz von Moivre-Laplace
==========================================

Es sei :math:`S_n \sim Bin(n,p)` mit festem :math:`0<p<1`, :math:`q=1-p`,

.. math:: S^*_n = \frac{S_n - np}{\sqrt{npq}}

dann  gilt für alle :math:`a,b \in \mathbb{R}` mit :math:`a<b`

.. math:: \lim_{n \rightarrow \infty} P(a \leq S^*_n \leq b) = \int_a^b \varphi(x) dx

und

.. math:: \lim_{n \rightarrow \infty} P(-\infty \leq S^*_n \leq b) = \int_{-\infty}^b \varphi(x) dx

mit der Gaußschen Glockenfunktion:

.. math:: \varphi (x) := \frac 1 {\sqrt{2 \pi}} e^{-\frac{x^2} 2}

.. figure:: binomial_grenzwert.png
	 :alt: binomial_grenzwert


.. math:: \phi(t) := \int^t_{- \infty} \varphi(x) dx

.. math:: \int_a^b \varphi(x) dx = \phi(b) - \phi(a)

Für hinreichend große :math:`n` wird der Fehler beliebig klein.

.. note::
  Eine Faustregel lautet, dass für :math:`npq > 9` die Approximation
  für praktische Zwecke ausreichend ist.
