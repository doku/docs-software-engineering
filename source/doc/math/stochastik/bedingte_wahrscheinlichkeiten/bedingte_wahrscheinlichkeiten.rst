=============================
Bedingte Wahrscheinlichkeiten
=============================

Definition
==========

Sei :math:`(\Omega, P)` ein endlicher W-Raum, :math:`A` und :math:`B` Ereignisse
mit :math:`P(A) > 0`, dann heißt:

.. math:: P(B|A) := \frac{P(B \cap A)}{P(A)}

bedingte Wahrscheinlichkeit von :math:`B` unter der Bedingung :math:`A`.

Die Abbildung :math:`P_A` heißt die bedingte Verteilung von :math:`P` unter
Bedingung :math:`A`.

.. math::

  & P_A(\Omega) : \mathcal{P}(\Omega) \rightarrow \mathbb{R} \\
  & P_A(B) := P(B|A)

Für unvereinbare Ereignisse :math:`B,C \subset \Omega` (mit :math:`B \cap C = \emptyset`)
gilt: :math:`P_A(B+C) = P_A(B) + P_A(C)`

Satz von der totalen Wahrscheinlichkeit
=======================================

Sei :math:`(\Omega, P)` ein endlicher W-Raum, :math:`A_1, ... A_n \subset \Omega`
paarweise unvereinbare Ereignisse, die eine Partition von :math:`\Omega` bilden:

.. math:: \Omega = \sum_{i=1}^n A_i

und es sei :math:`P(A_i) > 0` für alle :math:`1 \leq i \leq n`. Dann gilt für
jedes Ereignis :math:`B \subset \Omega`:

.. math::  P(B) = \sum_{i=1}^n P(B|A_i) * P(A_i) = \sum_{i=1}^n P(B \cap A_i)

Satz von Bayes
==============

Sei :math:`(\Omega, P)` ein endlicher W-Raum, :math:`A_1, ... A_n \subset \Omega`
paarweise unvereinbare Ereignisse, die eine Partition von :math:`\Omega` bilden:

.. math:: \Omega = \sum_{i=1}^n A_i

und es sei :math:`P(A_i) > 0` für alle :math:`1 \leq i \leq n`. Dann gilt für
jedes Ereignis :math:`B \subset \Omega` mit :math:`P(B) > 0` und jedes :math:`A_j`:

.. math::

  P(A_j | B) = \frac{P(B|A_j)P(A_j)}{P(B)} = \frac{P(B|A_j)P(A_j)}{\sum_{i=1}^n(P(B|A_i)*P(A_i)}
