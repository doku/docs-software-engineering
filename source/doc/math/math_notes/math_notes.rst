=====
Mathe
=====

Trigonometrische Funktionen
---------------------------

.. math::
  cos(2x) = 1-sin^2(x)

Integrale
---------

.. math::
  \int \frac{1}{x^2 + 1} dx = arctan x + c

Substituieren immer dann, wenn Ableitung einer Teilfunktion in dem Integral
ist.

Allgemein gilt:

.. math::
  \int \limits_{-a}^a f(x) dx = \int \limits_0^a f(x) + f(-x)

Jede stetige Funktion besitzt eine Stammfunktion.

Mengen
------

.. math:: |A\cup B| \leq |A|+|B|
