=====
Mathe
=====

.. toctree::
  :maxdepth: 2
  :caption: Contents
  :glob:

  grundlagen/index
  lineare_algebra/index
  analysis/index
  numerik/index
  stochastik/*
  math_notes/*
