==========================
Summierte Quadraturformeln
==========================

.. glossary::

  geschlossene Formeln
    Man spricht von der geschlossenen Form, wenn die Intervallränder Stützstellen
    sind, also: :math:`\tau_i = x_0, \tau_{i+1} = x_m`

    Vertreter sind z.b. die Trapetzregel und Simpson Regel.

  offene Formeln
    Von offenen Formeln spricht man, wenn die Ränder keine Nullstellen sind. Ein
    Vertreter ist die Mittelpunktsregel.

Trapezsumme
===========

Der Fehler der Tarpezsumme ist in :math:`O(h^2)`

Simpsonsumme
============

Der Fehler sinkt in :math:`h^4`
