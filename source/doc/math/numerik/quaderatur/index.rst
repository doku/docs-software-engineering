=========
Quadratur
=========

.. toctree::
  :glob:

  problemstellung/*
  newton_cotes_formeln/*
  clenshaw_curtis_quadratur/*
  summierte_quadraturformeln/*
  gauss_quadratur/*
