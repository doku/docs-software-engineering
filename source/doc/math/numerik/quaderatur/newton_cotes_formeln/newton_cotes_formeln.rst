====================
Newton-Cotes-Formeln
====================

Wählen wir die Stützstellen :math:`x_j` äquidistant (:math:`x_j=a+(b-a) \frac j m`),
so spricht man von **Newton-Cotes-Formeln**.

.. math:: I_m(f)=(b-a) \sum_{j=0}^m c_j f(x_j)

Die Newton-Cotes-Formel für gerades :math:`m` hat Exaktheit :math:`m+1`

Die Newton-Cotes-Quadratur wird für :math:`m \geq 8` zunehmend numerisch instabil,
da negative Quadraturgewichte auftauchen.

Rechtecksregel
==============

Die Rechtecksregel wählt eine beliebige Stützstelle und hat Exaktheit :math:`0`,
d.h. konstante Funktionen werden exakt integriert.

.. math:: \tilde{I}(f) = (b-a)f(\tilde{x}), \tilde{x} \in [a,b]

Mittelpunktsregel
=================

Bei dar Mittelpunktregel wird die Stützstelle in der Mitte des Intervalls gewählt.
Sie hat Exaktheit :math:`1`.

.. math:: I_0(f) = (b-a) f(\frac{a+b}{2})


Trapezregel
===========

Mit zwei Stützpunkten erhält man die Trapezregel. Sie hat Exaktheit :math:`1`.

.. math:: I_1(f)=(b-a) \frac{f(a)+f(b)}{2}

Keplersche Fassregel / Simpsonregel
===================================

Aus dem quadratischen Interpolationspolynom an den Stützstellen :math:`a, \frac{b+a}{2}, b`
erhalten wir schließlich die Keplersche Fassregel oder Simpsonregel:

.. math::
  I_2(f) = (b-a) \frac{1}{6} \left(f(a) + 4 f \left(\frac{a+b}{2}\right) + f(b)  \right)
