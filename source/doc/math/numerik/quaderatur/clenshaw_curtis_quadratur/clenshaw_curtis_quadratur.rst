=========================
Clenshaw-Curtis-Quadratur
=========================

Bei der Clenshaw-Curtis-Quadratur wird auch die Polynominterpolation mittels
Lagrangepolynomen verwendet, aber die Kondition wird verbessert, indem
die Stützstellen zum Rand des Intervalls hin dichter beieinander liegen.

Die Stützstellen werden so gewählt:

.. math:: x_{m-j} = \frac{a+b}{2} + \frac{b-a}{2} \cos(\frac{\pi}{m}j)

.. figure:: stuetzstellen.png
	 :alt: stuetzstellen
