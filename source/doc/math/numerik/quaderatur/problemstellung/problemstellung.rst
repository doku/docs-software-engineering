===============
Problemstellung
===============

.. glossary::

  Quadratur
    approximative Integration

Die Integration ist ein Spezialfall der Lösung von (gewöhnlichen)
Differentialgleichungen.

:math:`I_m(f) = \int_a^b P(f | x_0, ..., x_m)dx` hat die Form

.. math:: I_m(f) = (b-a) \sum_{j=0}^m c_j f(x_i)

mit den Gewichten

.. math:: c_j = \frac{1}{b-a} \int_a^b l_{j,m}(x) dx

und den Lagrangepolynomen :math:`l_{j,m}(x)` zu den Stützstellen :math:`x_0, ..., x_m`

Exaktheitsgrad
==============
Ist eine Quadraturformel exakt vom Grad :math:`m`, so integriert sie noch Polynome
vom Grad :math:`m` exakt.
