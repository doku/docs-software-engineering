==============
Gauß-Quadratur
==============

.. math:: \sum_{i=0}^m w_i f(x_i) = \int_a^b P(f|x_0, ..., x_m)(x)dx

Mit einer Formel dieser Form lässt sich ein Exaktheitsgrad von :math:`2m+1` nicht
erreichen.

Legendre-Polynome
=================

.. math:: P_k(x):= \frac{k!}{(2k)!} \frac{d^k}{dx^k}((x^2-1)^k)

.. figure:: legendre_polynome.png
	 :alt: legendre_polynome

Die Legendre-Polynome sind orthogonal bezüglich des Skalarproduktes.

.. math:: span{P_k}_{0 \leq k \leq m} = \Pi_m
