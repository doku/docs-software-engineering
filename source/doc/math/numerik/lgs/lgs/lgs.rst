===
LGS
===

- :math:`Rang(A)`: Die Dimension des von den Zeilen bzw. Spalten aufgespannten
  Untervektorraum.
- :math:`Kern(A) := \{x \in \mathbb{K}^n: Ax = 0\}`
- reguläre/ invertierbare Matrizen:
  :math:`\det A \neq 0 \Leftrightarrow Rang(A) = n \Leftrightarrow Kern(A)=\{0\}
  \Leftrightarrow \exists A' \in \mathbb{K}^{n\times n} : A * A' = A' * A = I_n`

Gauß-Elimination
================

Der Aufwand die Matrix in die Obere rechte Dreiecksform zu bringen ist :math:`\Theta(n^3)`

Die anschließende Rücksubstitution ist in :math:`\Theta(n^2)`

Stabilität
==========

.. glossary::

  Residuum
    Für :math:`A \in \mathbb{K}^n` heißt :math:`\tilde{r}:= b-A \tilde{x}` Residuum
    von :math:`\tilde{x}` beim Lösen von :math:`A \tilde{x} =b`
