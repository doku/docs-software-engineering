===============
LGS Zerlegungen
===============

LR Zerlegung
============

Die :math:`LR`-Zerlegung berechnet zu :math:`A \in  \mathbb{K}^{n \times n}` eine
obere Dreiecksmatrix :math:`R` und eine untere Dreiecksmatrix :math:`L` mit
Diagonalelementen :math:`1`, sodass :math:`A = L * R`.


.. figure:: l_matrix.png
	 :alt: l_matrix

Multipliziert man :math:`A` mit :math:`L`, so wird das :math:`l_{i,j}`-fache der
Zeile :math:`j` von :math:`A` von der Zeile :math:`i` abgezogen.

.. math:: Ax = b \Leftrightarrow LRx = b

Berechne die Vorwärtssubstitution:

.. math:: Ly = b

und dann die Rückwärtssubstitution:

.. math:: Rx = y

Die :math:`LR` Zerlegung einer Matrix ist eindeutig.

.. figure:: lr_algorithm.png
	 :alt: lr_algorithm

Cholesky Zerlegung
==================

Falls A symmetrisch positiv definit ist kann man die LR Zerlegung modifizieren
zur Cholesky-Zerlegung:

.. math:: A=LDL^T

- :math:`L` ist eine untere Dreiecksmatrix mit Diagonalelementen :math:`1`
- :math:`D` ist eine Diagonalmatrix mit positiven Einträgen

QR Zerlegung
============

Orthogonale Matrix :math:`Q`, obere Dreiecksmatrix :math:`R`

.. math:: A = Q R
