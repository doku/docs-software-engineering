==========
Grundlagen
==========

.. glossary::

  gewöhnliche Differentialgleichung
    Eine gewöhnliche Differentialgleichung (ordinary differential equation, ODE),
    ist eine Gleichung, die eine unbekannte Funktion, sowie deren Ableitung(en)
    bezüglich **einer** unabhängigen Variable enthält.

    Eine gewöhnliche Differentialgleichung (ODE) 1. Ordnung für eine Funktion
    :math:`y: [t_0, T] \rightarrow \mathbb{R}^m` ist eine Gleichung der Form

    .. math:: y'(t)=f(t,y(t))

    mit rechter Seite :math:`f: [t_0, T] \times \mathbb{R^m} \rightarrow \mathbb{R}`

  Anfangswertproblem
    Eine ODE 1. Ordnung und eine Anfangsbedingung :math:`y(t_0) = y^0, y^0 \in \mathbb(R)^m`
    bilden zusammen ein Anfangswertproblem (1. Ordnung)


Die ODE ohne :math:`b(t)` heiß
