===================
Einschrittverfahren
===================

Euler
=====

Nutzt Rechtecksregel der Quadratur.

Explizites Eulerverfahren
-------------------------

.. math:: y^{j+1} = y^j + hf(t_j, y^j)

Implizites Euler Verfahren
--------------------------

.. math:: y^{j+1} = y^j + hf(t_{j+1}, y^{j+1})

Ein Rechenschritt des

Heun
====

Nutzt die Trapezregel

.. math::

    \tilde{y}^{j+1} &= y^j + hf(t_j, y) \\
    y^{j+1} &= y^j + \frac{h}{2} (f(t_j, y) + f(t_{j+1}, \tilde{y}^{j+1}))
