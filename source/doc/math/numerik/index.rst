=======
Numerik
=======

.. toctree::
  :glob:
  :maxdepth: 2

  grundlagen/index
  lgs/*
  interpolation/*
  quaderatur/*
  ode/index
  iterative_loesungsverfahren/*
