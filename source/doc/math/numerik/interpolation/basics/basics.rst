======
Basics
======

.. glossary::

  Lineares Funktional
    Ein lineares Funktional ist eine lineare Abbildung :math:`G_n \rightarrow \mathbb{K}`


Hau-Ruck Methode
================

Interpolationsmatrix

.. figure:: interpolationsmatrix.png
	 :alt: interpolationsmatrix

Wobei :math:`G_n=span\{ \phi_0, ..., \phi_n \}`
