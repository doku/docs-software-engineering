===========================
Interpolation mit Polynomen
===========================

Man spricht von Extrapolation, wenn man die Funktion an einem :math:`x_i \notin [x_0, x_n]`
auswerten möchte.

Polynome
========

Ein Term der Form :math:`x^k` heißt Monom in :math:`x`, ein Term der Form
:math:`\sum_{j=0}^{n} a_j x^j` mit :math:`a_0, ...a_n \in \mathbb{K}` heißt
Polynom in :math:`x`.

Ist :math:`a_n \neq 0`, heißt :math:`n` der Grad des Polynoms. Der Spezialfall
:math:`p=0`, (d.h. :math:`n=0, a_0=0`) hat den Grad :math:`- \infty`

Eine Funktion :math:`f: \mathbb{K} \supset X \rightarrow \mathbb{K}` heißt
Polynomfunktion vom Grad :math:`n`, wenn :math:`\forall x \in X: f(x)=p(x)` mit
einem Polynom :math:`p` vom Grad :math:`n`.

Der Raum aller Polynomfunktionen :math:`\mathbb{K} \supset X \rightarrow \mathbb{K}`
vom Grad höchstens :math:`n` wird bezeichnet mit :math:`\Pi_n`

.. math::
  \Pi_n:=\left\{ p: X \rightarrow \mathbb{K}, p(x) = \sum_{j=0}^n a_jx^j, a_0, ..., a_n \in \mathbb{K} \right\}

.. note::

  Wenn ein Polynom :math:`p` vom Grad höchstens :math:`n` mehr als :math:`n`
  Nullstellen hat, muss :math:`p=0` sein.

  Wenn zwei Polynome :math:`p` und :math:`q` vom Grad höchstens :math:`n` in
  :math:`n+1` Stellen übereinstimmen, müssen sie gleich sein. (Die Differenz
  :math:`p-q` hat :math:`n+1` Nullstellen und höchstens Grad :math:`n`, muss
  also :math:`0` sein.)

.. glossary::

  führender Koeffizient
    Für ein :math:`p(x) = \sum_{j=0}^n a_j x^j \in \Pi_n` heißt :math:`a_n` der
    führende Koeffizient von :math:`p`.

Tschebyscheff-Polynome
----------------------

Die Tschebyscheff-Polynome :math:`T_k, k=0,1,2` werden rekursiv definiert als:

.. math::

  T_0(x) &:= 1 \\
  T_1(x) &:= x \\
  T_{k+1}(x) &:= 2x T_k(x) - T_{k-1}(x)

.. math::

  T_k(x) = \cos(k \arccos(x))

Die Extremstellen der Tschebyscheff-Polynome :math:`T_n` liegen bei:

.. math:: x_j = cos(j \frac{\pi}{n}), 0 \leq j \leq n

Lagrange-Interpolationsaufgabe
==============================

Die Interpolationsaufgabe, ein :math:`g \in \Pi_n` zu finden, das in :math:`n+1`
paarweise verschiedenen Punkten :math:`x_0, ..., x_n` vorgegebene Werte :math:`f(x_i)`
annimmt:

.. math:: g \in \Pi_n : g(x_i) = f(x_i), 0 \leq i \leq n

heißt Lagrange-Interpolationsaufgabe.

- :math:`x_i` heißen Stützstellen
- :math:`f(x_i)` heißen Stützwerte
- :math:`(x_i, f(x_i))` heißen Stützpunkte

.. hint:: Die Lagrange-Interpolationsaufgabe ist eindeutig lösbar.

Lagrange Polynom
----------------

Das eindeutig bestimmet Polynom :math:`P_n \in \Pi_n`, das eine Funktion :math:`f`
in den Stützstellen :math:`x_0, ..., x_n` interpoliert
(d.h :math:`P_n(x_i) = f(x_i,), 0 \leq i \leq n`), heißt
Lagrange-Interpolationspolynom. Wir bezeichnen es mit:

.. math:: P(f|x_0, ..., x_n)

Zu gegebenem Polynomgrad :math:`n` und paarweise verschiedenen Stützstellen
:math:`x_0, ... x_n` heißen die Polynome:

.. math:: l_{j,n}(x) := \prod_{i=0, i \neq j}^n \frac{x-x_i}{x_j-x_i}

Lagrange Polynome.

Lagrange Polynom :math:`l_{j,n}` ist an allen Stützstellen 0 außen an :math:`j`

Summe über alle Lagrange Polynome ist 1.

Diese sind so definiert, dass:

.. math::

  l_{j,n}(x_i) = \delta_{j,i} =
    \begin{cases}
      1 \text{ falls } i=k \\
      0 \text{ falls } i \neq k
    \end{cases}


Dadurch ist an jeder Stützstelle :math:`x_i` genau das Lagrange Polynom :math:`l_{i,n}(x_i)=1`
Alle anderen Lagrange Polynome hingegen sind :math:`0`.



Die Lösung der Lagrange-Interpolationsaufgabe können wir schreiben als:

.. math:: P_n = \sum_{j=0}^n f(x_j) * l_{j,n} = \sum_{j=0}^n f(x_j) \prod_{i=0, i \neq j}^n \frac{x-x_i}{x_j-x_i}

In der Basis der Lagrange Polynomen ist der Vector des Interpolations

Die Interpolationsmatrix zur Basis der Lagrange Polynome ist die Einheitsmatrix.

Aitken-Neville-Schema
---------------------

Es sei für :math:`0 \leq i \leq n` und :math:`0 \leq k \leq i`:
:math:`P_{i,k}:=P(f|x_i)` das Polynom aus :math:`\Pi_k`, das in den :math:`k+1`
Stützpunkten :math:`(x_{i-k}, f(x_{i-k})), ..., (x_i, f(x_i))` interpoliert.

Rekursionsformel:

.. math::

  P_{i,0}(x) &= f_i, i = 0, ..., n \\
  P_{i,k}(x) &= P_{i,k-1}(x) + \frac{x-x_i}{x_i-x_{i-k}} (P_{i,k-1}(x)-P_{i-1,k-1}(x))

.. figure:: aitken_neville_schema_tabelle.png
	 :alt: aitken_neville_schema_tabelle

:math:`P_{n,n}` ist das gesuchte Polynom :math:`P \in \Pi_n`, das in allen
:math:`x_0, ..., x_n` interpoliert.

Newtonsche Interpolationsformel
-------------------------------
Die Grundidee ist nun: Sei :math:`P_{n-1} :=  P(f|x_0, ..., x_{n-1})` gegeben.
Suche nun einen Korrekturterm, um daraus :math:`P_n` zu erhalten.

.. glossary::

  Dividierte Differenz
    Der führende Koeffizient (:term:`führender Koeffizient`) zu
    :math:`P_{i,k} \in \Pi_k` (also der Koeffizient zu :math:`x^k` in dem Polynom,
    das :math:`f` in :math:`x_{i-k}, .... x_i`
    interporiert) heißt **Newtonsche dividierte Differenz** und wird bezeichent
    mit:

    .. math:: [x_{i-k}, ...x_{i-1}, x_i]f

.. math:: P_n(x) = P_{n-1}(x)+ [x_0, ..., x_n]f * \prod_{i=0}^{n-1}(x-x_i)

Newton-Darstellung von :math:`P_n`:

.. math:: P_n(x) = \sum_{k=0}^{n} \left( [x_0, ..., x_k]f * \prod_{i=0}^{k-1}(x-x_i) \right)

Mit der folgenden Rekursionsformel können wir das gleiche Schema wie bei
Aitken-Neville zur Newtonschen dividierten Differenz bilden:

.. math::

  [x_i]f &= f(x_i) \\
  [x_{i-k}, ..., x_i]f &= \frac{[x_{i-k+1}, ..., x_i]f - [x_{i-k}, ...x_{i-1}]f} {x_i - x_{i-k}}

Nach Bestimmen des dividierten Differenzen Schemas kann man :math:`P_n(x)` bestimmen.

Baryzentrische Interpolation
----------------------------

Zu gegebenen Stützstellen :math:`x_0, ..., x_n` heißt :math:`\Phi_n(x)` das
**Stützstellenpolynom**:

.. math:: \Phi_n(x):= \prod_{i=0}^n (x-x_i)

.. math:: q_j := \prod_{i=0, i \neq j}^{n} \frac{1}{x_j - x_i}

Die :math:`q_j` sind unabhängig von :math:`x`, d.h. wir müssen sie nur einmal
berechnen (:math:`O(n^2)`) und können sie dann für alle :math:`x` benutzen.

Die **baryzentrische Interpolationsformel** ist:

.. math::

  P_n(x)=\frac{\sum_{j=0}^n f(x_j \frac{q_j}{x-x_j})}{\sum_{j=0}^n \frac{q_j}{x-x_j}}

.. figure:: baryzentrische_interpolation_tschebyscheeffpunkten.png
	 :alt: baryzentrische_interpolation_tschebyscheeffpunkten
