==============================
Trigonometrische Interpolation
==============================

Wenn wir periodische Funktionen interpolieren, versagt die Polynominterpolation,
sobald wir verlangen, dass auch Auswertungen außerhalb des Bereiches der Stützstellen
möglich sein sollen, wir also extrapolieren. Denn es gilt für jedes Polynom :math:`p(x)`
mit Grad :math:`>0`:

.. math:: \lim_{x \rightarrow \pm \infty} |p(x)| = \infty

Diskrete Fourier-Transformation
===============================

Schnelle Fourier-Transformation
===============================
