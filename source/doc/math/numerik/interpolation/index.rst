=============
Interpolation
=============


.. toctree::
  :glob:

  basics/*
  lineare_ausgleichsrechnung/*
  interpolation_mit_polynomen/*
  trigonometrische_interpolation/*
