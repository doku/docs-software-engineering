==========================
Lineare Ausgleichsrechnung
==========================

Lineare Ausgleichsrechnung mittels Normalengleichung
====================================================

Berechne zu :math:`A \in \mathbb{R}^{m \times n}, m \geq n` mit :math:`Rang(A)=n`
und :math:`b \in \mathbb{R}^m` das :math:`x^* \in \mathbb{R}^n` mit:

.. math:: ||Ax^*-b||_2 = \min_{x \in \mathbb{R}^n} ||Ax-b||_2

1. Bilde :math:`A^T A \in \mathbb{R}^{n \times n}` und :math:`A^Tb \in \mathbb{R}^n`
2. löse :math:`A^T A x^* = A^T b`

Lineare Ausgleichsrechnung mit QR-Zerlegung
===========================================

Berechne zu :math:`A \in \mathbb{R}^{m \times n}, m \geq n` mit :math:`Rang(A)=n`
und :math:`b \in \mathbb{R}^m` das :math:`x^* \in \mathbb{R}^n` mit:

.. math:: ||Ax^*-b||_2 = \min_{x \in \mathbb{R}^n} ||Ax-b||_2

1. Bringe :math:`A` durch orthogonale Transformationen auf obere Dreiecksgestalt
   :math:`A=QR` (z.B. mit Givens-Rotationen)
2. Löse die ersten :math:`n` Zeilen des LGS :math:`Rx^*=Q^Tb` nach :math:`x^*` auf

Das Residuum :math:`r^*` zu :math:`x^*` steht senkrecht auf dem von :math:`A`
aufgespannten Raum.
