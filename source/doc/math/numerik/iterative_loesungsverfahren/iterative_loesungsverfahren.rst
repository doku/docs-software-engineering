==========================
Iterative Lösungsverfahren
==========================

Newton Verfahren
================

Das Newton-Verfahren ist ein Standardverfahren zur numerischen Lösung von
nichtlinearen Gleichungen.

.. math::

  x_{n+1} = x_n - \frac{f(x_n)}{f'(x_n)}

.. figure:: newton_verfahren.png
	 :alt: newton_verfahren

Sekantenverfahren
=================

Berechnet die Nullstelle der Sekante zweier Punkte :math:`x_{k-1}, x_k` als
nächsten Auswertepunkt.

.. figure:: sekanten_verfahren.png
	 :alt: sekanten_verfahren

Newton und Sekantenverfahren konvergieren nur lokal.
