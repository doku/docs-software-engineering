==========
Grundlagen
==========

.. toctree::
  :glob:
  :maxdepth: 2

  diskretisierung/*
  zahlen/*
  runden/*
  kondition_stabilitaet/*
