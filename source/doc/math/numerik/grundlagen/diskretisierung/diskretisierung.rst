===============
Diskretisierung
===============

Mathematische Objekte sind oft (überabzählbar) unendliche Mengen
(:math:`\sqrt(\pi), U \subset \mathbb{R}^2`).
Computer nutzen hingegen endliche Mengen und können daher immer nur eine Auswahl
von Zahlen speichern. Das Kernthema der Numerik ist die Diskretisierung.

Durch das Diskretisieren ist unser Ergebnis nicht das Exakte Ergebnis sondern eine
Näherung. Dadurch entsteht ein Fehler (Datenfehler, Rundungsfehler,
Diskretisierungsfehler, Abbruchsfehler).

Die Aufgabe der Numerik ist es den Fehler und den Aufwand gering zuhalten.

.. glossary::

  Kondition
    Die Empfindlichkeit numerischer Lösungsverfahren für Störungen eines Problems

  Stabilität
    Die Empfindlichkeit numerischer Lösungsverfahren für Störungen des Lösungsverfahrens

  Effizienz
    Der numerische Aufwand zur Erzielung einer bestimmten Genauigkeit des
    Lösungsverfahrens

Absoluter und relativer Fehler
==============================

.. glossary::

  Absoluter Fehler
    Der absolute Fehler zwischen einem Wert :math:`x` und einer Näherung (einem
    gestörten :math:`x`) :math:`\tilde{x} = x + \Delta x` ist die Norm der Differenz:

    .. math:: ||\Delta x|| = ||x - \tilde{x}||

  relativer Fehler
    Der relative Fehler zwischen einem Wert :math:`x` und einer Näherung
    :math:`\tilde{x} = x + \Delta x` ist:

    .. math:: \delta_x := \frac{||\Delta x||}{||x||} = \frac{||x - \tilde{x}||}{||x||}`

    Die :math:`k` Dezimalstellen Genauigkeit entsprechen dann einem relativen Fehler
    :math:`\delta_x \leq 10^{-k}`.
