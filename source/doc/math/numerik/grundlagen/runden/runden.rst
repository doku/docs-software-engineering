======
Runden
======

Da mit den Gleitpunktzahlen nur eine Teilmenge der reellen Zahlen abgebildet werden,
kann, ist Runden eine Abbildung:

.. math:: \mathbb{R} \rightarrow \mathbb{F}_{B,t}

Nachbarn
========
Jede Zahl :math:`x \in \mathbb{R}` hat genau einen linken :math:`f_l(x)` und einen
rechten :math:`f_r(x)` Nachbarn in :math:`\mathbb{F}_{B,t}`.

.. math:: f_l (x) := \max \{f \in \mathbb{F}_{B,t}: f \leq x\}

.. math:: f_r (x) := \min \{f \in \mathbb{F}_{B,t}: f \geq x\}

Rundungsverfahren
=================

Jedes Rundungsverfahren :math:`rd :  \mathbb{R} \rightarrow \mathbb{F}` für eine
Gleitpunktzahl :math:`\mathbb{F}` sollte folgende Eigenschaften erfüllen:

#. Surjektivität: :math:`\forall f \in \mathbb{F} \exists x \in \mathbb{R}: rd(x) = f`
#. Idempotenz: :math:`\forall x \in \mathbb{R}: rd(rd(x)) = rd(x)`
#. Monotonie: :math:`\forall x,y \in \mathbb{R}: x \leq y \Rightarrow rd(x) \leq rd(y)`

.. hint:: Das Arbeiten mit Gleitpunktzahlen ist keine exakte Rechnung.

Zahlen die dezimal gut darstellbar sind, können binär schlecht darstellbar sein.

Wichtigsten Rundungsverfahren:

- Abrunden (gerichtet): :math:`rd_-(x) := f_l(x)`
- Aufrunden (gerichtet): :math:`rd_+(x) := f_r(x)`
- Abschneiden (gerichtet): :math:`rd_0 = \begin{cases} rd_-(x) falls \; x \geq 0 \\rd_+(x) \text{ sonst} \end{cases}`
- korrektes Runden: :math:`rd_* = \begin{cases} f_l(x) falls \; x < \frac{f_l(x)+f_r(x)}{2} \\f_r(x) \text{ sonst} \end{cases}`

In der Praxis wird meist das korrekte Runden mit der Ausnahme für den Fall, dass
eine Zahl genau zwischen zwei Maschinenzahlen liegt
:math:`(x = \frac{f_l(x)+f_r(x)}{2})`. In diesem Fall wird auf den Nachbarn mit
gerader Mantisse gerundet.

Rundungsfehler
--------------
Rundungsfehler sind eine wohl definierte Fehlerquelle, die gravierende Auswirkungen
auf das Ergebnis einer numerischen Problemlösung haben kann.

.. glossary::

  absoluter Rundungsfehler
    :math:`rd(x)-x`

  relativer Rundungsfehler
    :math:`\epsilon = \frac{rd(x)-x}{x}, \; x \neq 0 \Leftrightarrow rd(x) = x (1+\epsilon)`

Für den relativen Fehler gilt beim gerichteten Runden :math:`|\epsilon| < \varrho`,
weil im Extremfall fast ein kompletter "Zahlenzwischenraum" beim Runden übersprungen
wird.

Beim korrekten Runden gilt dagegen :math:`|\epsilon| \leq \frac{\varrho}{2} = eps`.
Dies gilt natürlich nur für reelle Zahlen, die zwischen der kleinsten und der
größten Maschinenzahl liegen.
Durch das korrekte Runden entsteht ein relativer Fehler der Maschinengenauigkeit.
