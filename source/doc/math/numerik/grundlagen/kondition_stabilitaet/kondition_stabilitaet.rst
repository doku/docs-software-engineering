========================
Kondition und Stabilität
========================

Kondition ist eine Eigenschaft des Problems und beschreibt wie sich Fehler
in den Eingabedaten auf das Ergebnis auswirken, wenn man sonst alles "richtig"
macht. Da wir auf Fehler in den Eingabedaten keinen Einfluss haben, sind wir
bei einer schlechten Kondition machtlos. Die Kondition is daher
**unabhängig vom gewählten Lösungsverfahren** und gibt an, welche Genauigkeit
wir bestenfalls bei Fehlern in den Eingabedaten erzielen können.

Die Stabilität ist eine Eigenschaft des Lösungsverfahren. Es beschriebt, wie sich
Fehler in Verfahren (Diskretisierung, Rundung, ...) auf das Ergebnis auswirken
bei exakten Eingabedaten. Die Stabilität können wir durch die Wahl von besseren
Lösungsverfahren erhöhen. Die Stabilität gibt uns einen Hinweis, auf die Größe des
Fehlers im Ergebnis, bei fehlerbehafteter Rechnung.

Stabilität ist eine Eigenschaft des Algorithmus.

Bei Stabilitätsüberlegungen werden die Eingabedaten als exakt angenommen, und
man untersucht den Algorithmus auf Genauigkeit. Bei Konditionsüberlegungen nehmen
wir an, dass der Algorithmus exakt ist, aber die Eingabedaten nicht exakt sind.

Kondition
=========

Bei der Kondition geht es um die Untersuchung der Fehlerverstärkung bei
Datenfehlern. Bei einer hohen Sensitivität des Ergebnisses gegenüber
Fehlern in den Eingabedaten spricht man von **schlechter Kondition**, bei geringer
Sensitivität von **guter Kondition**.

Bei schlecht konditionierten Problemen kann es keine Algorithmus geben,
der vernünftige Ergebnisse liefert.

Konditionszahl
--------------
Für ein Problem :math:`f : X \rightarrow Y`, gestörte Eingabedaten
:math:`\tilde{x}= x + \Delta x` mit gestörtem Ergebnis
:math:`\tilde{y} = f(\tilde{x}) = y + \Delta y` ist:

.. glossary::

  absolute Kondition

    .. math:: \mathcal{k}_{abs}(x) = \frac{||\Delta y||_Y}{||\Delta x||_X}

  relative Kondition

    .. math::

      \mathcal{k}_{rel}(x) = \frac{\delta_y}{\delta_x } =
      \frac{||\Delta y||_Y}{||\Delta x||_X} * \frac{||x||_X}{||y||_Y} =
      \mathcal{k}_{abs}(x) * \frac{||x||_X}{||y||_Y}

Ein Problem ist umso besser konditioniert (im Punkt :math:`x`), je kleinere
Schranken für :math:`\mathcal{K}` mit :math:`\Delta x \rightarrow 0` existieren.
Die Kondition hängt also von der Ableitung ab.


.. math::

      \mathcal{k}_{rel}(x) := |f'(x)| * \left| \frac{x}{f(x)} \right| \dot{=}
       \frac{\delta_y}{\delta_x }

Lineare Abbildungen
-------------------

.. todo:: Operatornorm

Stabilität
==========
Ein Algorithmus heißt gutartig oder stabil, wenn die durch ihn am Laufe der
Rechnung erzeugten Fehler in der Größenordnung des durch die Kondition des
Problems bedingten unvermeidbaren Fehlers bleiben.

.. glossary::

  Stabilität
    Ein numerischer Algorithmus heißt stabil, wenn für alle Eingabedaten
    :math:`x` unter dem Einfluss von Rundungs- und Approximationsfehlern das
    Ergebnis :math:`\tilde{y}` das exakte Ergebnis zu leicht modifizierten
    Eingabedaten ist.

    .. math:: \tilde{y} = f(\tilde{x}) \text{ mit } \tilde{x} \in U_{\epsilon}(x)


.. figure:: stabilitaet.png
	 :alt: stabilitaet
