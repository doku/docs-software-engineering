======
Zahlen
======

Wir benötigen eine Diskretisierung der reellen Zahlen im Rechner. Die Anzahl
der darstellbaren Zahlen auf einem Computer ist unvermeidlich endlich, also
diskret und beschränkt.

Ganze Zahlen
============
Ganze Zahlen :math:`\mathbb{Z}`, :math:`\text{int} \subset \mathbb{Z}`

Festpunktzahlen
================

Bei Festpunktzahlen ist die Anzahl der Stellen vor und nach dem Komma fix gegeben.

Festpunktzahlen: :math:`x = M * B^E`

- Basis: :math:`B \in \mathbb{N} \backslash \{1\}`
- Exponent: :math:`E \in \mathbb{Z}` fest
- Mantisse :math:`M \in \mathbb{Z}`

Bsp: :math:`x = 3,1416; B = 10; E = -4; M = 31416`

Benachbarte Zahlen :math:`x = m*B^E`, :math:`x' = (M+1) B^E` haben den Abstand
:math:`\Delta x = x' - x = B^E`, damit haben Festpunktzahlen wie die ganzen
Zahlen einen konstanten Abstand zu den Nachbarn und der Zahlenraum ist
beschränkt.
Der Relative Abstand wird umso kleiner je größer die Zahlen werden.
Fürs Numerische Rechnen sind Festpunkzahlen daher völlig ungeeignet.

Gleitpunktzahlen
================
Bei Gleitpunktzahlen ist der relative Fehler auf dem gesamten Zahlenspektrum gleich.
Dadurch hat man im Bereich sehr kleiner Zahlen eine hohe Genauigkeit und im Bereich
großer Zahlen größere Abstände zwischen den Zahlen, kann aber größere Zahlen
darstellen.

.. math::

  \mathbb{F}_{B,t} := \{0\} \cup \{x = M*B^E: B^{t-1} \leq |M|<B^t \; M,E \in \mathbb{Z} \}

- :math:`B \in \mathbb{N} \backslash \{1\}` fest
- :math:`t \in \mathbb{N}` fest
- :math:`M: t` Stellen zur Basis B und :math:`E \in \mathbb{Z}` speichern

:math:`\mathbb{F}_{B,t}` bezeichnet die Menge der normalisierten :math:`t`-stelligen
Gleitpunktzahlen zur Basis :math:`B`.

Durch die Forderung :math:`B^{t-1} \leq |M|<B^t` wird sichergestellt, dass die
Darstellung der Gleitpunktzahlen :math:`M, E` eindeutig ist, sie heißen daher
**normalisiert**.

Die Menge der Gleitpunktzahlen ist diskret aber unendlich.

Genauigkeit/ Auflösung
----------------------
Der absolut Abstand zwischen den Gleitpunkzahlen ist nicht konstant. Der Relative
Abstand :math:`\frac{|x' - x|}{|x|}` hingegen schon.

.. glossary::

  Auflösung
    .. math:: \varrho:=\frac{1}{B^{t-1}} = B^{1-t}

    Herleitung:
    :math:`\frac{(M+1)*B^E - M*B^E}{M*B^E} = \frac{B^E}{M*B^E} = \frac{1}{M} \leq \frac{1}{B^{t-1}} = B^{1-t} := \varrho`
    (da :math:`B^{t-1}` die kleinste erlaubte Mantisse ist)

  Maschinengenauigkeit

   Die Maschinengenauigkeit beschränkt also den relativen Abstand einer Zahl
   :math:`x \in \mathbb{R}` zur nächstgelegenen Gleitpunktzahl in
   :math:`\mathbb{F}_{B,t}`

   .. math:: eps = \frac{\varrho}{2} = \frac{B^{1-t}}{2}

   .. math::

      eps =
      \begin{cases}
        \varrho \text{ für gerichtetes Rundes} \\
        \frac{\varrho}{2} \text{ für korrektes Rundes} \\
      \end{cases}

.. figure:: gleitpunktzahlen_beispiel_f22_zahlenstrahl.png
	 :alt: gleitpunktzahlen_beispiel_f22_zahlenstrahl

Maschinenzahlen
===============

Die Maschinenzahlen sind eine endliche Teilmenge der Gleitpunktzahlen, bei denen
man nicht nur die Mantisse beschränkt, sondern auch den Exponent. Dadurch wird
die Zahl in einem endlichen Speicher abbildbar.

.. math::
  \mathbb{F}_{B,t,\alpha,\beta} = \{ M*B^E: B^{t-1} \leq |M|<B^t \; M,E \in \mathbb{Z} \; \text{mit} \; \alpha \leq E \leq \beta\}

.. math::
  \mathbb{F}_{B,t,\alpha,\beta} = \{x \in \mathbb{F}_{B,t} : \alpha \leq E \leq \beta\}

- kleinste positive Zahl: :math:`\sigma = B^{t-1} * B^{\alpha} = B^{t-1 + \alpha}`
- größte Zahl: :math:`\lambda = (B^t -1)* B^\beta`

IEEE Standard
-------------

.. figure:: ieee_standard_values.png
	 :alt: ieee_standard_values

Beim numerischen Rechnungen akkumulieren sich die Fehler, sodass man durch hohe
Anzahl der Nachkommastellen versucht diese Effekte gering zu halten.

Ausnahmebehandlung
------------------

Not-a-number (NaN)
  das Ergebnis ist keine Zahl und somit auch nicht als Maschinenzahl darstellbar,
  klassisches Beispiel ist hier :math:`\frac{1}{0}`.

exponent overflow
  das Ergebnis ist (betragsmäßig) größer als die größte darstellbare Zahl
  :math:`|f| > (B^t - 1) B^\beta`

exponent underflow
  das Ergebnis ist (betragsmäßig) kleiner als die kleinste darstellbare Zahl
  :math:`|f| < B^{t-1+\alpha}`

Je nach Implementierung werden diese Ausnahmen gemeldet (signaling) oder nicht
(quiet).
