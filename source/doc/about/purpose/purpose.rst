============
Docs Purpose
============

This documentation has a number of advantages:

Repeat
======
Since especially introductory courses are very repetitive,
this documentation aims to bundle up all information about a certain topic.

Searchability
=============
This documentation gives you the ability to search for key phrases in all of your
documents.

You are very likely not to remember every detail especially for things you have
learned a long time ago. This documentation gives you the ability to fresh up
your mind.

Content Ownership
=================
Often public articles are very good but miss a couple of points or lack some
examples. The articles in this documentation give you full control over your content.
Enabling you to modify them and add new sections over time should you so desire.

Single Source of truth
======================
Everything important is documented at exactly one place. You always know where to
search for information and where to update files.

Portability
===========
This online hosted doc is always with you. You don't have to remember to bring a
Book etc.

Broader Workflow
================
For studying purposes it is intended to create a folder containing all articles
associated with the specific lecture. These articles might be spiced up with some
outside sources but for exam preparation purposes it is recommended to keep all
exam relevant articles inside the folder of the lecture.

As soon as the exam is passed the articles should be sorted according to their topic.
