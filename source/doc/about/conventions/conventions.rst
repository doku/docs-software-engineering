============
Konventionen
============

Konventionen für ``rst`` Artikel
================================

Dateistruktur
-------------
Alle Artikel sind in einem Ordner, der gleich wie der Artikel selbst benannt wird.
In diesem Ordner befindet sich eine ``.rst`` Datei (diese ist der Artikel selbst)
und alle Bilder die in dem Artikel verwendet werden. Im folgenden wird dieser Ordner
stets als ``Artikelordner`` bezeichnet.

Fügt man ``.png`` Dateien ein, so wird ``png`` **klein** geschrieben. Die Bilder
werden **nicht** in einem Extraordner gespeichert, sondern direkt in dem Verzeichnis
mit dem Artikel abgelegt.

Alle Dateinamen (Bilder, Artikel etc.) und Ordnernamen sollten ausschließlich klein
geschrieben werden. Man sollte **nicht** die CamelCase Schreibweise für Dateinamen
verwenden. Möchte man in einem Dateinamen mehrere Wörter trennen, so kann man das
mit dem Unterstrich ``_`` machen.

.. hint::
  Da Windows nicht *casesensitv* ist aber Linux führt sonst zu einigen Schwierigkeiten,
  da es bei Windows sehr umständlich ist eine Änderung der Groß- und Kleinschreibung
  des Dateinamens zu commiten.

Dieses Beispiel soll die Ordnerstruktur besser veranschaulichen

.. code::

    source
      + documentation
        + sphinx_config
          + sphinx_config.rst
          + bild1.png
          + bild2.png
        + sphinx_cheatsheet
          + sphinx_cheatsheet.rst
          + bild1.png
          + bild2.png
      + programming
        + google_maps_api
          + google_maps_api.rst
          + bild1.png
          + bild2.png

Jeder Artikel muss einen Artikel Titel haben (siehe :ref:`cheatsheet-kapitel-ref`).
andernfalls gibt es Errormeldungen bei der Konvertierung in ``html``.

Code
-----
Code Beispiele sollten nicht als Bilder eingefügt werden, denn dadurch wird es deutlich
schwieriger die Code Beispiele zu kopieren, zu verändern oder zu updaten.
