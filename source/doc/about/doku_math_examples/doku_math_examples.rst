=======================
Examples for Mathsyntax
=======================

.. csv-table::

  ":math:`\overset{\text{Lemma}}\geq`", "``\overset{\text{Lemma}}\geq``"

Cases
-----

.. code::

  .. math::
    X_{i,j} =
      \begin{cases}
        1 \text{ falls während RQS } s_i \text{ mit } s_j \text{ verglichen werden, } \\
        0 \text{ sonst}
      \end{cases}

.. math::
  X_{i,j} =
    \begin{cases}
      1 \text{ falls während RQS } s_i \text{ mit } s_j \text{ verglichen werden, } \\
      0 \text{ sonst}
    \end{cases}

Align
-----

.. code::

  .. math::
    Pr(\bigcap^{n-1}_{i=1} E_i)
    &\geq \prod^{n-2}_{i=1} (1-\frac{2}{n-i+1}) \\
    &= \prod^{n-2}_{i=1} (\frac{n-i+1-2}{n-i+1}) \\
    &= \prod^{n-2}_{i=1} (\frac{n-i-1}{n-i+1}) \\

.. math::
  Pr(\bigcap^{n-1}_{i=1} E_i)
  &\geq \prod^{n-2}_{i=1} (1-\frac{2}{n-i+1}) \\
  &= \prod^{n-2}_{i=1} (\frac{n-i+1-2}{n-i+1}) \\
  &= \prod^{n-2}_{i=1} (\frac{n-i-1}{n-i+1}) \\

.. todo:: math cancel

"``:math:`\require{cancel} \cancel{2-2}``", "``\require{cancel} \cancel{2-2}``"

.. warning::

  - % Zeichen in math müssen escaped werden
