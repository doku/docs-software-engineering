ABOUT
=====

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   purpose/*
   todo
   cheatsheet/*
   doku_math_examples/*
   conventions/*
   setup/index
