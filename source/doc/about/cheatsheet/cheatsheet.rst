Cheatsheet
==========

Its article is a summary of the following sources:

*  http://build-me-the-docs-please.readthedocs.io/en/latest/Using_Sphinx/OnReStructuredText.html
*  http://docutils.sourceforge.net/docs/user/rst/quickref.html
*  http://docutils.sourceforge.net/docs/ref/rst/directives.html


Inline Markup
-------------

*  *italics* - ``*italics*``

*  **bold** - ``**bold**``

*  ``inline code`` - ````inline code````
*  :sup:`Superscript` is marked with ``:sup:`superscript text```.
*  :sub:`Subscript` is marked with ``:sub:`subscript text```.


Be aware of some restrictions:

*  The markup may not be nested.
   For example, this markup is wrong: ``*italics with **bold** inside*``
*  The text content within the markup may not start or end with whitespace.
   For example, this markup is wrong: ``* text*``
*  The markup must be separated from surrounding text
   by non-word characters (whitespace or punctuation).
   Use a backslash-escaped-space to work around that.
   For example: ``thisis\ *one*\ word`` is rendered like thisis\ *one*\ word.
*  If asterisks or backquotes appear in running text
   and could be confused with inline markup delimiters,
   they have to be escaped with a backslash.
*  Whitespace or punctuation is required around interpreted text,
   but often not desired with subscripts & superscripts.
   Backslash-escaped whitespace can be used;
   the whitespace will be removed from the processed document::

      The chemical formula for molecular oxygen is O\ :sub:`2`.

   To improve the readability of the text, the use backslash-escapes is discouraged.
   If possible, use :ref:`rst-substitutions-ref` instead::

      The chemical formula for pure water is |H2O|.

      .. |H2O| replace:: H\ :sub:`2`\ O

   Keep all substitutions together (e.g. at the end of the file).


Paragraphs
----------
The paragraph is the basic block in a reST document.

Paragraphs are simply chunks of text separated by one or more blank lines.

Indentation is significant in reST, so all lines of the same paragraph must be
left-aligned to the same level of indentation.

Remember that changing to next line does not create a paragraph, unless the
chunks of text is separated by a blank line.

.. _cheatsheet-kapitel-ref:

Sections
--------
Section are created by underlining (and optionally overlining) the section title
with a punctuation character. Any punctuation character can be used to define a section title
(``= - ` : ' " ~ ^ _ * + # < >``).

For Example

.. code-block:: rst

    ==============
    Document Title
    ==============

    Chapter
    =======

    Section
    -------

    Subsection
    ~~~~~~~~~~

    Subsubsection
    ^^^^^^^^^^^^^

.. note::

    By convention we use this hirarchie:

    *  ``=`` for the Document Title
    *  ``=`` for Chapters ("Heading 1")
    *  ``-`` for Sections ("Heading 2")
    *  ``~`` for Subsections ("Heading 3")
    *  ``^`` for paragraphs ("Heading 4")

Lists
-----

Bulleted lists
~~~~~~~~~~~~~~

List markup is natural:
just place an asterisk at the start of a paragraph and indent properly::

   *  This is a bulleted list.
   *  It has two items, the second
      item uses two lines.

Nested lists are possible,
but be aware that they must be separated
from the parent list items by blank lines::

   * this is
   * a list

     * with a nested list
     * and some sub-items

   * and here the parent list continues


Numbered lists
~~~~~~~~~~~~~~

The same goes for numbered lists;
they can also be auto-numbered using a ``#`` sign::

   1. This is a numbered list.
   2. It has two items too.
   #. This is a numbered list.
   #. It has two items too.

Renders like this:

1. This is a numbered list.
2. It has two items too.
#. This is a numbered list.
#. It has two items too.

Definition lists
~~~~~~~~~~~~~~~~

Definition lists are created as follows

.. code-block:: rst

   term (up to a line of text)
      Definition of the term, which must be indented

      and can even consist of multiple paragraphs

   next term
      Description.

term (up to a line of text)
  Definition of the term, which must be indented

  and can even consist of multiple paragraphs

next term
  Description.

The Sphinx documentation generator
provides a more flexible alternative to definition lists
(see :ref:`rst-glossaries-ref`).

.. _rst-glossaries-ref:

Glossaries
~~~~~~~~~~

The Sphinx ``..glossary::`` directive
contains a reST definition-list-like markup
with terms and definitions.

See the following example::

   .. glossary::
     :sorted:

      environment
         A structure where information about all documents under the root is
         saved, and used for cross-referencing.  The environment is pickled
         after the parsing stage, so that successive runs only need to read
         and parse new and changed documents.

      source directory
         The directory which, including its subdirectories, contains all
         source files for one Sphinx project.

.. glossary::
  :sorted:

  environment
     A structure where information about all documents under the root is
     saved, and used for cross-referencing.  The environment is pickled
     after the parsing stage, so that successive runs only need to read
     and parse new and changed documents.

  source directory
     The directory which, including its subdirectories, contains all
     source files for one Sphinx project.

The definitions will then be used in cross-references with the ``:term:`` role.
For example::

   The :term:`source directory` for this project is ...

In contrast to regular definition lists,
a glossary supports *multiple* terms per entry
and inline markup is allowed in terms.
You can link to all of the terms.  For example::

   .. glossary::

      term 1
      term 2
         Definition of both terms.

When the glossary is sorted, the first term determines the sort order.

To automatically sort a glossary in alphabetical order, include the following flag::

   .. glossary::
      :sorted:

Using ``:term:`term 1``` you can cross reference the definition through out your
documentation

Field lists
~~~~~~~~~~~
Field lists are two-column table-like structures
resembling database records (label & data pairs).

:Date: 16.04.2017
:Version: 1.0
:Authors: fsc
:Indentation: Since the field marker may be quite long, the second
   and subsequent lines of the field body do not have to line up
   with the first line, but they must be indented relative to the
   field name marker, and they must line up with each other.
:Parameter i: integer

.. code-block:: rst

  :Date: 16.04.2017
  :Version: 1.0
  :Authors: fsc
  :Indentation: Since the field marker may be quite long, the second
     and subsequent lines of the field body do not have to line up
     with the first line, but they must be indented relative to the
     field name marker, and they must line up with each other.
  :Parameter i: integer

Tables
------

The reStructuredText markup supports two basic types of tables.
For *grid tables*, you have to "paint" the cell grid yourself.
They look like this.

.. code-block:: rst

    +------------+------------+-----------+
    | Header 1   | Header 2   | Header 3  |
    +============+============+===========+
    | body row 1 | column 2   | column 3  |
    +------------+------------+-----------+
    | body row 2 | Cells may span columns.|
    +------------+------------+-----------+
    | body row 3 | Cells may  | - Cells   |
    +------------+ span rows. | - contain |
    | body row 4 |            | - blocks. |
    +------------+------------+-----------+


Grid table:

+------------+------------+-----------+
| Header 1   | Header 2   | Header 3  |
+============+============+===========+
| body row 1 | column 2   | column 3  |
+------------+------------+-----------+
| body row 2 | Cells may span columns.|
+------------+------------+-----------+
| body row 3 | Cells may  | Cells     |
+------------+ span rows. | contain   |
| body row 4 |            | blocks.   |
+------------+------------+-----------+


*Simple tables* are easier to write, but limited:
they must contain more than one row,
and the first column cannot contain multiple lines.
They look like this::

   =====  =====  =======
   A      B      A and B
   =====  =====  =======
   False  False  False
   True   False  False
   False  True   False
   True   True   True
   =====  =====  =======

Simple table:

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======

These are the basic types of tables, which are rather clumsy. Also available
(and easier to use) are special tables, namely list-tables and CSV-tables.

.. _rst-specialtables-ref:

Special tables
~~~~~~~~~~~~~~

The ``table`` directive associates a title with the following table::

   .. table:: User list

      ==========  =========
      First name  Last name
      ==========  =========
      John        Doe
      Jane        Dove
      ==========  =========

==========  =========
First name  Last name
==========  =========
John        Doe
Jane        Dove
==========  =========

A ``list-table`` is created from a uniform two-level bullet list::

   .. list-table:: User list
      :header-rows:1

      *  - First name
         - Last name
      *  - John
         - Doe
      *  - Jane
         - Dove

.. list-table:: User list
  :header-rows: 1

  *  - First name
     - Last name
  *  - John
     - Doe
  *  - Jane
     - Dove

A ``csv-table`` is created from comma-separated values
(either in the document or in an external file)::

   .. csv-table:: User list
      :header: "First name","Last name"

      "John","Doe"
      "Jane","Dove"

.. csv-table:: User list
  :header: "First name","Last name"

  "John A unnecessary long name to make sure this table will overflow. ","Doe A unnecessary long name to make sure this table will overflow. "
  "Jane","Dove"

Another example of ``csv-table``, using and external file::

   .. csv-table:: Table 1 - Legend of the table goes here...
      :header-rows: 1
      :stub-columns: 1
      :file: ../tables/table1.csv


An ``exceltable`` can also be used::

   .. exceltable:: Table 1 - Legend of the table goes here...
      :file: ../tables/tables.xls
      :sheet: table1
      :selection: A1:C20
      :header: 1

Using Excel tables requires an additional module `sphinxcontrib.exceltable`
that is an extension for Sphinx, that adds support for including spreadsheets, or part of them, into Sphinx document.
It can be installed using pip::

   pip install sphinxcontrib-exceltable

Then the project ``conf.py`` file needs to be updated::

   # Add ``sphinxcontrib.exceltable`` into extension list
   extensions = ['sphinxcontrib.exceltable']

Another alternative is xmltable (https://pythonhosted.org/rusty/xmltable.html).


Directives
----------

Code
~~~~

.. sectionauthor:: Guido van Rossum <guido@python.org>

.. code-block:: rst

  .. code-block:: java
      :linenos:
      :emphasize-lines: 2,3

      public static void main () {
        System.out.println("hallo");
      }

.. attention:: Nach dem codeblock und Optionen **muss** eine Leerzeile kommen!

Das Resultat:

.. code-block:: java
    :linenos:
    :emphasize-lines: 2,3

    public static void main () {
      System.out.println("hallo");
    }


Weitere Optionen

- line-nos
- highlight lines
- import file


Unterschied code-block vs code

Pseudo code ?



Admoniton

toc

Images
------
.. _rst-figures-ref:

Figures
~~~~~~~
A ``figure`` consists of image data (including image options),
an optional caption (a single paragraph),
and an optional legend (arbitrary body elements).

.. figure:: Sphinx_Metropolitan.jpg
  :scale: 30 %
  :alt: Sphinx Image
  :align: center

  Caption of the Image

  The legend consists of all elements after the caption.

.. code-block:: rst

  .. figure:: Sphinx_Metropolitan.jpg
    :scale: 30 %
    :alt: Sphinx Image
    :align: center

    Caption of the Image

    The legend constits of all elements after the caption.

There must be blank lines before the caption paragraph and before the legend.
To specify a legend without a caption,
use an empty comment (``..``) in place of the caption.

.. note::
  When using image captions and legends you have to be careful with your indentation.
  Make sure that your options perfectly align with the caption and legend. Otherwise
  the image will not be displayed.

.. _rst-figureOptions-ref:

``figure`` Optionen:
^^^^^^^^^^^^^^^^^^^^

``:alt:``
    Alternate text, displayed if image is not available

``:height:``
    Desired height of the image. If ``:scale:`` is also set, values are combined.
    A height of 200px and a scale of 50 is equivalent to a height of 100px with no scale.

``:width:``
    Width of the image, given as length or percentage of the current line width.

``:scale:``
    integer percentage the default is ``100 %``

``:align:``
    :Values: ``left``, ``right``, ``center``

``:target:``
    makes the image into a clickable hyperlink reference.
    The option argument may be a URI (relative or absolute), or a
    reference name with underscore suffix (e.g. ```a name`_``).

    :Values: a URI or reference name


Image
~~~~~

.. image:: Sphinx_Metropolitan.jpg
  :scale: 15 %
  :alt: Sphinx Image
  :align: left

The ``.. image::`` Tag is equivalent to the ``figure`` tag, but does not offer
support for captions or legends. Text will wrap around images or figures the ``:align:``
option is set to left or right.

.. code-block:: rst

  .. image:: Sphinx_Metropolitan.jpg
    :scale: 15 %
    :alt: Sphinx Image
    :align: left

Math
----

Inline Math
~~~~~~~~~~~
The math role marks its content as mathematical notation (inline formula).

The input format is LaTeX math syntax without the “math delimiters“ ($ $), for example:

The area of a circle is :math:`A_\text{c} = (\pi/4) d^2`.

.. code-block:: rst

  The area of a circle is :math:`A_\text{c} = (\pi/4) d^2`.


Math Block
~~~~~~~~~~

::

    .. math::

      a_t(i) = P(O_1, O_2, … O_t, q_t = S_i)

.. code-block:: rst

  .. math::

  a_t(i) = P(O_1, O_2, … O_t, q_t = S_i)



Hyperlinks
----------

External links
~~~~~~~~~~~~~~

Use ```link text <http://example.com/>`_`` for inline web links.
If the link text should be the web address,
you don't need special markup at all,
the parser finds links and mail addresses in ordinary text (with no markup).

You can also separate the link and the target definition, like this::

   This is a paragraph that contains `a link`_.

   .. _a link: http://example.com/

.. note::
     The use of inline web links is discouraged,
     to improve the readability of the reST text.

     Simple links (e.g. to institutional sites, software sites, and so on)
     should be kept together at the end of the text file
     (this is merely a way to simplify the editing procedure,
     and the update and verification of the links).

Internal links
~~~~~~~~~~~~~~

To support cross-referencing to arbitrary locations in any document,
the standard reST labels are used.
For this to work,
the label names must be unique throughout the entire documentation.
There are two ways in which you can refer to labels:

*  If you place a label directly before a section title,
   you can reference to it with ``:ref:`label-name```.
   Example::

      .. _my-label-ref:

      Section to cross-reference
      --------------------------

      This is the text of the section.

      In the end of this phrase is a reference to the section title, see :ref:`my-label-ref`.

   The ``:ref:`` role would then generate a link to the section,
   with the link  title being "Section to cross-reference".
   This works just as well
   when section and reference are in different source files.

   Automatic labels also work with :ref:`figures <rst-figures-ref>`::

      .. _my-figure-ref:

      .. figure:: my-image.png

         My figure caption

   A reference like ``:ref:`my-figure-ref```
   would insert a reference to the figure
   with link text "My figure caption".

   The same works for :ref:`tables <rst-specialtables-ref>`
   that are given an explicit caption using the ``table`` directive.

*  Labels that aren't placed before a section title can still be referenced
   to, but you must provide the text for the link, using this syntax:
   ``:ref:`Link text <label-name>```.

Using ``:ref:`` is advised over standard reStructuredText links to sections
(like ```Section title`_``) because it works across files, when section
headings are changed, and for all builders that support cross-references.


Footnotes
---------

For footnotes, use ``[#name]_`` to mark the footnote
location, and add the footnote body at the bottom of the document after a
"Footnotes" rubric heading, like so::

   Lorem ipsum [#first-footnote-name]_ dolor sit amet [#second-footnote-name]_

   .. rubric:: Footnotes

   .. [#first-footnote-name] Text of the first footnote.
   .. [#second-footnote-name] Text of the second footnote.

You can also explicitly number the footnotes (``[1]_``) or use auto-numbered
footnotes without names (``[#]_``).

.. admonition:: This is a tip.

   To facilitate editing, auto-numbered footnotes should **not** be used.
   Instead, use short descriptive names (that simplify cross-referencing).

.. _rst-citations-ref:

Citations
---------

Standard reST citations are supported::

   Lorem ipsum [Ref]_ dolor sit amet.

   .. [Ref] Book or article reference, URL or whatever.

Citation usage is similar to footnote usage,
but with a label that is not numeric or begins with ``#``.

When the documentation is built using the Sphinx  document generator,
the citations are "global",
meaning that every citation can be referenced from any .rst files.
In this case, a separate file may be created (e.g. a ``references.rst`` file).

.. _rst-substitutions-ref:

Substitutions
-------------

reST supports "substitutions",
which are pieces of text and/or markup referred to in the text by ``|name|``.
They are defined like footnotes with explicit markup blocks, like this::

   .. |name| replace:: replacement *text*

or this::

   .. |caution| image:: warning.png
                :alt: Warning!


If you want to use some substitutions for all documents,
put them into a separate file
(e.g. ``substitutions.txt``)
and include it into all documents you want to use them in,
using the ``include`` directive.

Be sure to use a file name extension
which different from that of other source files,
to avoid Sphinx finding it as a standalone document.
For example, use the ``.rst`` file extension for the source files,
and the ``.txt`` file extension for the files which are to be included.

.. admonition:: This is a tip.

   This is useful in technical documentation such as User's Manuals,
   where a substitution file can be built for each localized version
   of the interface elements (menus, messages, etc),
   guaranteeing the consistency of the document translation
   with the software's human user interface.


.. admonition:: Warning.

   Substitutions do NOT work inside directives (or inside the options of a directive).

   Do not try to google for a solution (...been there).
   It is a design limitation: RST markup can not be nested. Period.

.. _rst-comments-ref:

Comments
--------

Every explicit markup block
which isn't a valid markup construct is regarded as a comment.
For example::

   .. This is a comment.

You can indent text after a comment start to form multiline comments::

   ..
      This whole indented block
      is a comment.

      Still in the comment.

.. admonition:: This is a style convention.

   Comments can also be used as placeholders
   to mark places within the document.
   For example:

   *  the ``.. links-placeholder`` can mark the place
      where hyperlinks are kept together at the end of the document;

   *  the ``.. metadata-placeholder`` can mark the place
      where document metadata (author, date, etc)
      is kept together at the beginning of the document.


ToDos
-----

Es lassen sich in spezielle Todo Einträge setzen.

.. code-block:: rst

    .. todo:: Beispiel Code zu jMock2 einfügen @auther fsc

Diese werden ähnlich wie ``admonition`` gehandhabt:

.. todo:: Beispiel Code zu jMock2 einfügen

Mit dem Befehl ``.. todolist::`` lassen sich nun alle Todo-Einträge an einem Ort gesammelt auflisten.

.. code-block:: rst

    .. todolist::

ToDo Liste der Doku
~~~~~~~~~~~~~~~~~~~
