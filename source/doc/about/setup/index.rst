Setup
=====

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   dev_setup/*
   sphinx_setup/*
