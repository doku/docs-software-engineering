Sphinx Setup
============

Custom Theme einbinden mit relativem Pfad
-----------------------------------------

Nötig für CI Prozess mit gitlab runner:

.. code-block:: bash

    git submodule add ../../doku/sphinx-custom-theme source/sphinx-custom-theme

Gitlab runner können nur mit ``https`` authentifizieren, lokal möchte man aber
``ssh`` nutzen. Daher relativen Pfad angeben dann geht beides.
