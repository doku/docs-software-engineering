Developer Setup
===============

screenToGig

snipping Tool

markdown-image-paste
--------------------

Installation
~~~~~~~~~~~~

Mit der Atom Erweiterung ``markdownImagePaste`` lassen sich Bilder einfach mit
Copy-Paste in den ``md`` oder ``rst`` Artikel einfügen.

Dazu muss man die Settings öffnen.

.. figure:: atomsettings.png
	 :alt: atomsettings

Sucht man unter ``install`` nach ``markdown-image-paste`` findet man die Erweiterung.

.. figure:: markdownimagepasteinstall.png
	 :alt: markdownimagepasteinstall

Man muss nun einfach auf ``install`` klicken.

Verwendung
~~~~~~~~~~

#. Bild in die Zwischenablage kopieren
#. Gewünschter Bildname in eine Neue Zeile Schreiben (Ohne Leerzeichen, und mit
   einer freien Zeile über und unter dem Bild)
#. ``STRG V`` drücken um das Bild einzufügen. Die Datei wird automatisch angelegt
   und richtig benannt.

.. figure:: markdownimagepasteusage.png
	 :alt: markdownimagepasteusage

Atom

Sphinx
