==============
Business Ideas
==============

* crowdsourcing small tasks
* borrow it - share your stuff
* stackoverflow for medicin
* region based restaurant network
* preämtive refactoring
* manual feedback for recomender systems - not interested / bought it already button


region based restaurant network
===============================

* solution for entire restaurant process
* find restaurant
  * (local recomondations based on preference profil and ratings from google/tripadvisor)
  * integrated menu
  * create group and find restaurant for the group
  * place reservation
* order food
  * use qr code / website to order your food
* pay for the food
  * use build in payment to pay for food basket of table (easy split payment)

preämtive refactoring
=====================

The vision is to create a toolbox which help developers write better code faster.

Businessprocess plattform
=========================

* provide a plattform for digital Businessprocess execution.
* Businesses can model processes using BPMN
* BPMN functions are implemented as serverless functions
* developers can develop serverless functions and
  * recieve payment by usage or sell to us
  * can request certificate for function - quality assurance
* Businesses can request a new function and award developers
