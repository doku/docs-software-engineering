=======
Startup
=======

.. hint::
  You should start with the problem that you are trying to solve in the world
  and not by deciding that you want to build a company.

  The best companies that get build try to drive some kind of social change.

.. hint::
  In a world that is changing so quickly the biggest risk you can take is not
  taking any risk.

A great business idea is a vision of how a market can be fundamentally
improved.

You can achieve great success if you are able to combine great people and a
great market.

From an investment perspective people only fund companies that address
billion dollar markets.

You have to look at the consumer need and not only from a product perspective.

Your product has to be so universal and scalable from a technology point of
view so that you can serve many customers around the world. Any product that is
to local or that is to fragmented into a category is typically a bad product.

It is not about being first, but about building a better product. Your product
has to be a 10x improvement for your customer.

The risk of failure is almost always the same, independent on whether you try to
do something big or something small. So it is absolutely not rational to try and
do something small.

One of the key qualities of top founders is the ability to hire people who are
either more experienced or more specialized than they are themselves.

The basics for all startups are the same: from the first 1000 questions you will
have as a founder, 900 will be the same for any startup team. Senior advice
will be really helpful.

Idea
====

The idea should come first, the startup should come second. The company should
feel like an important mission.

The best companies are almost always mission oriented. It is difficult to get large
groups of people to the extreme level of focus and productivity that you need for
a startup to be successful unless the company feels like a important mission.

.. figure:: startup_idea.png
	 :alt: startup_idea

You have to find a small market in which you can get a monopoly and then quickly
expand.

.. hint::
  The truly good ideas don't sound like they are worth stealing!

Ask your self the question: **why now?**

If you are building something that you don't need yourself, you are at a big disadvantage
and you need to get very close to your customers.

Good startup ideas are almost always very easy to explain.

Understand your Market
======================

You can basically change everything in a startup but the market.

- Who are your customers?
- What problem am I solving for them?
- Who is the competition? (every product has competition: what are the other
  things a customer is doing if he is not buying your product.)
- Why should they buy with me?
- What are the fundamental market dynamics?
- How is the market evolving (much more important then how does the market look right now)?
- How will the market look in 10 years?

Competition is a fundamentally good thing in the market. Increased Competition
validates your product and makes you push to create more value to the user.

Product
=======

.. figure:: product_user_love.png
	 :alt: product_user_love

Find a small group of users and make them really love what you are doing.
It makes it a lot easier to expand. One way you know this is working, is if you
are getting growth by word of mouth.

Very view startups die from competition, they usually die because they fail to
build something that users love.

Recruit your early users by hand to get good feedback from them. Understand that
group extremely well.

Founding
========

Great businesses always have strong founding teams. A great founding team size
is 2-4. Being a single founder is very hard.

Select Co-Founders based on Vision, Skills and Cultural Fit. Your vision should
be aligned, the founding them needs the skills required to implement the vision
and should share the same values.

- Are we aligned on how we want to build the company?

Team
====

Assemble the best team, based on competency and cultural fit.

The real company values are things companies are willing to hire and fire based
upon.

Product-Market Fit
==================

The Product-Market fit (PMF) describes the positive reaction of the market to
your service or product.

Indicators to measure PMF:

- organic growth rate
- repeat customers
- net promoter score

Launch the first version of your product as early as humanly possible.

Business Plan
=============

Business Plans for tech company should follow a simple structure:

- Market (Size, competition, dynamics)
- Product (Customer value, technology)

  - How can you demonstrate that you are solving a real consumer pain?

- Business Model

  - Who do you plan to actually make money (subscription, transactional) and how
    can this scale over time?
  - Calculate Market Size and Monetization Opportunity to check if your business
    model works out

- Team (Competency, culture)
- Financial Plan (P&L)

  - If I scale this product where I found product market fit, what is the cost to
    build the product and distribute it, and then what is the revenue for it?

Startup Pitches
===============

- DO

  - Fucus: less is more
  - Visual format
  - Keep it simple (Does your mom get it?)

- DO Not

  - write essays, 50 page PPTs
  - complicated financial modelling
  - Misrepresent data

You have to be able to describe what you are doing within a minute.
If you give an overload of information you  will lose your audience.

The Idea is nothing special, it is very likely that the idea was already
tried many times before. Tell your idea to as many people as possible, because
that is actually user research. The more you talk about your startup vision, the
easier it is to get good feedback and outside ideas. This is always helpful.


Startup
=======

.. glossary::

  Startup
    A startup is a human institution designed to deliver a new product or
    service under conditions of extreme uncertainty.

    A startup is an Experiment.

A company is a group of people that are assembled to create a product or
service. If you are able to get great people to join the company and work
together towards a common goal you will end up with a great product.

The biggest problem that product development is facing today is not building
things inefficiently but building things very efficiently that nobody wants.

Company Success: Does your company live up to the aspirations dreams, time ,
talent, and energy of the founders and their investors and their employees.

When big companies buy startups they don't deprecate in a predictable way.

A software company can build anything they are able to imagine. The prominent
question is not: Can it be build? but rather: Should it be build? Can we build
a sustainable business around a particular product?

Business Goals
==============

Make your business goals explicit and visible to the entire team.

Useful links
============
https://hpi.de/connect/veranstaltungen/entrepreneurship/2017/startup-talkshpi.html

Startup Rules
=============

- Starting a startup is where gaming the system stops working.
- Running a startup never gets any easier.
- If you are actively thinking of startup ideas you will get ideas that are not only bad but plausible.
- The things that really matter for starting a startup is domain expertise.


Lessons
=======

- utilize the media
- talk to the gods not the angels
- dont postpone decisions like firings
- think about positive and negative events that may occur
- dont sell projects but products (well priced product with fixed Features) or at least be aware of it
