=====================
The Founders Dilemmas
=====================

.. warning::

    If entrepreneurship is a battle, most casualties stem from friendly fire or self-inflicted wounds.

Founding decisions have to be made by design.

Dilemmas
========


Per-founding Career Dilemmas
----------------------------

1. Should I become an entrepreneur?
2. If so, when should I make the leap into founderhood - early in my career or after I
   accumulate more career experience?
3. How wan I dispassionately evaluate my idea?

Career Dilemmas
---------------

- Career Handcuffs
- Golden Handcuffs
- Family Handcuffs

Idea Evaluation

    - Market potential: Are customers willing to pay for such a product ot service? How big is the market? Is it growing?
    - Competitive landscape: Is it favorable? Are many companies for scarce resources?
    - Ticking Clock: Is there a ticking clock that requires me to move quickly to pursue my idea? Is the window of opportunity about to close?
    

(lack of experience, market not ready, personal problems)


- Founding Team Dilemmas (solo vs team, relationships, roles, rewards, beyond the founding team: hiring, investors, founder ceo succession)

All dilemmas are uncomfortable but important decisions, 

- short term vs long term consequences
- 

