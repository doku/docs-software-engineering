=================
The Lean Movement
=================

- drawing on the knowledge and creativity of individual workers
- shrinking of batch sizes
- just-in-time production and inventory control
- accelerate of cycle times

.. note::

  The customer is the most important part of the production line. (Edwards Deming)

If the customer is the most important part of the production line, what do we do
if we don't know who the customer is?

The assumption of agile and all previous management approaches is that there is
somebody who can give us an authorative, definitive answer to design questions.

In entrepreneurship that assumption breaks down. We are working on products where
nobody knows what the customer wants, at best we have a hypothesis.

.. figure:: lean_startup_development.png
	 :alt: lean_startup_development

The goal is to minimize the cycle time:

.. figure:: lean_cycle.png
	 :alt: lean_cycle

All we are as a software startup is a catylist that turns ideas into code. When
customers interact with that code they create data, that we can measure and learn.

For any startup advice we use the heuristic of minimizing cycle time: Does this
advise reduce the time through the loop

Agile
=====

All agile methodologies have their origin in the IT-Departments of large Companies.
There is a reason for that: They are designed for situations where the problem
is known but the solution is unknown. By building something that is well understood
iteratively we can increase the odds that the project will be successful.

.. figure:: agile_product_development.png
	 :alt: agile_product_development
