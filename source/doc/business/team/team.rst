====
Team
====

Cofounder Relationships are amongst the most important in the entire company.

The number One cause for startup failure is cofounder blowups. The track record
for cofounders  who don't already know each other is really bad. You should know
your cofounders for years.

It is better to have no cofounder than a bad one. But it still really bad if you
don't have a cofounder.

You need relentlessly resourceful cofounders.

You need to be looking for cofounders that are unflappable, tough, they know
what to do in every situation, are decisive, creative. In other words you need
someone that behaves like James Bond more than someone who in as expert in a
particular domain.

Try not to hire. You want to be proud how much you can get done with a small amount
of employees. Try to stay small as long as you possibly can. The cost of getting
a early hire wrong is really high. The

You should either spend 0% or 25% hiring people.

.. hint:: Mediocre engineers do not build great companies.

If you compromise on the first 5-10 hires, this might kill your company. Ask
yourself: do you want to bet the future of the company on this single hire.

The best source by far for hiring are people that you already know. Personal
referrals are the trick to hiring.

For any potential employee answer the following questions:

- Are they smart?
- Do they get things done?
- Do I want to spend a lot of time around them?

If you don't know the individual, work with them on a quick project for a day or
to, to get to know them better.

.. todo:: the animal test

In the early days of a startup where communication and speed outweigh everything,
distributed teams work really bad.

Employee Equity
===============

You should give around 10% of the company to the first 10 employees. You should
be super generous with equity to early employees.

You have to make sure that your employees are happy and feel valued.

.. attention::
  Be aware that as a first time founder you are likely to be a very bad manager.
  You need to overcompensate for that.

Fire Fast
=========
Firing people is one of the worst parts of running a company.

Fire people who are:

- bad at their job
- creating office politics
- are persistently negative

When there are problem with cofounders or employees talk about that as early as
possible.

Culture
========

- Learning Culture (everyone should regularly learn new things and present them
  to the team with potential implementation in the product)
- King for a day (once or twice a year one employee gets drown and is the king.
  The king gets to tell everyone in the company what to do and over 48 hours
  the whole team improves one thing the king wants to change)
