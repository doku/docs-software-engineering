===============
Product Roadmap
===============

A product roadmap is a practical representation of a :ref:`product-strategy`.

A product roadmap translates your strategic choices of your :ref:`product-strategy`
into a series of milestones aimed to gradually achieve your business objectives.
