===========
Startup CEO
===========

Author: Matt Blumberg (former Returnpath CEO)

A Startup CEO has three main responsibilities: 

#. Set vision and strategy of the company and communicate it well to all stakeholders
#. recrut, hire, and retain the very best talent 
#. make sure there is always enough cash in the bank

Recognize that you may fail at some time!


Storytelling
============

Translate your vision into a story. Stories, like startups, paint a picture of what the 
future could be. 

Stories have a main character (**the customer or user**) and a supporting cast 
(**investor, employees, partners, competitors**). The y have a beginning (the problem)
middle (the product) and an end (the solution). 

We believe that x potential customers face y problem. Orur proposed solution to y problem is 
solution z at cost of q dollars -but we're also going to test the effectiveness of solutions
a,b,c. Here is our plan for testing that hypothesis.

Traditional business plans assume that their assumptions are correct, and startup business
plans assume that tehir assumptions are probably wrong. 

Customer - Problem - Product - Solution

- What are the major probblems customers
- What product/solution could you provide to adress these issues?
- Are they looking for an upgrade to an existing solution or something completely new?
- What would it be worth to them to solve the problem? How much would they be willing to pay for it to solve the problem?


People either buy stuff out of fear or gread. 
Fear-Gread Continueum


Vetting ideas
-------------

Create a list of all your ideas and evaluate them using the following criteria

- Customer Pain (30%) - Does the market need your idea?
- Market Opportunity (10%) - How many people need your idea? Today size, and Tomorrow growth
- Can we win? (20%) - Are there already competitors?
- Strategic fit (10%)- Is this a problem you can solve? Do you have the right expertise?
- Economics (30%) - Can you afford to solve this problem?

Rank each idea on a scale of 1-5 for each of the criteria. Multiply by the weight and consider the score.

.. hint:: 
    Be passionate about the substance of your business!

    You should live and use your businesses product. It is difficult to have
    a high level of credibility within your organisation if you are that personally disconected from your 
    what your company does?


Defining and testing your story
===============================

.. note::

    Start by admitting you are wrong!


Books

- the lean Startup
- the four steps to the apithany
- running lean 
- the entrepreurs guide to customer development

