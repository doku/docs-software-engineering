==============
Startup School
==============


How to Work Together
====================

In a startup founders need to optimize a relationship that can last for 10 years. 

.. warning::

    Everyone fights! You will fight with your cofounders. Make a Plan before you Fight!


The Four Horsemen
    - Criticism
    - Contempt (Intention to Insult)
    - Defensiveness (someone not owning responsibility)
    - Stonewalling

Once you decide ownership, also determine success and failure.
You can delegate things but you have to have a system that tells you when you'll have 
a conversation about it, when there is trouble. Do this while you are emotionally sober. 


Matter Dicision Disagreement Framework

When [observation], I feel [emotion] because I'm needing some [universal needs]. 
would you be able to [request]?



What Makes the top 10% of Founders different?
=============================================

Great founders:

- execute: when you say you do something, get it done and learn something 
- consistent ability to say what you want to do, do that and learn from it
- never get stuck in the execution step


- Formidability: get shit done consistently

- Communication: Startups require good Communication with users, investors, employees, cofounders
- The best founders can in 1 or 2 sencences explain exacetly what their business does (to all different stakeholders: customer, investor, mom) (to anyone with any knowledge about their industry)


- internal Motivation: they don't get too discouraged when things go wrong
- you will fail all the time
- there is a type of person that doesn't get discouraged
- beenig motivated in the face of failure is super important


The biggest Mistake first-time founders Make
============================================

- Choose a problem you actually don't care about (a lot of startups fail because they loose motivation to work on the problem)
- Help users you don't care about 
- choosing cofounders you don't know well
- not having transparent conversations between cofounders (performance, goal, roles)
- not launching
- not using analytics & measuring what your users are doing
- having no idea where your first users come from
- prioritation (getting product out and talking to users is the most important thing)


Finance : Startup Finance Pitfalls and How to Avoid Them
=========================================================

.. warning::
    Cash is the life-blood of your business!

Know this:
----------
- Bank Balance
- Money coming in
- Money going out

With this you can calculate:
----------------------------
- Burn (Money out - Money in)
- Runway (Bank Balance / Avg Burn)
- Growth Rate (Money in Month 2 - Money in Month 1) / Money in Month 2
- Default Alive?

Look at these numbers every week

.. note::
    You should know your numbers!

- Underrepresenting your Expenses
- Expenses do not remain constant
- “Fully loaded” cost of new hires

- Every hire is an investment, Are you getting your ROI?

- The best startups do more with less.
- Ratio of Revenue per employees
- You should avoid hiring before hitting Product Market fit
- More employees will not help you reach product market fit.

- Never let runway get too low before raising again.

- Always assume you will never raise money again!


Sales
=====

https://www.youtube.com/watch?v=xZi4kTJG-LE

Conferences
-----------

- find the big industry conferences for your startup (ask your user - Which conference do you go to?)
- get the list of attendees (who'll be there in advance)
- email people in advance and ask them to meet and show them your tool (have entire day booked up in 30 minute increments)

Cold Emails
-----------
- short
- personalized
- actionable


Hi
my name is tyler and I'm the CEO of Clever. My company has developed new technology that reduces the time spent doing SIS integration by 80%.

I figure this might be of intrest to you given the new middle school reading software Scholasitc just released.

I'd love to get your feedback even if you'te not in the market for this right now. Do you have 20 minutes this week?
I'm open Tuesday at 1 or 2 pm ET if either may work


- Shut up and listen
- Sales is about listening
- Build relationship with people, understand what their problem 

.. figure:: phonecall_listen_dont_talk.png

Questions
- Tell me about your problem
- How do you solve it today?
- Why did you agree to the call
- What would your ideal solution look like? 




- uberconference


Closing
-------
- Get a no or yes quickly
- you need to have an agreement to propose
- 

- how i picked my self up from failure to success in sales

- noj


Christoph Janz


Sales Team
----------
- retrain caregiver / your end user to be a sales guy.


Advice for Technical Founders
=============================
https://www.youtube.com/watch?v=tSW-GePDwn4&list=PLQ-uHSnFig5NVnJ_cLWM7dLuMQRDeekoX&index=21

First Version
-------------
- focuse on what's technically impossible and what will be your core advantage
-


- ask during 1:1 what people think will happen over the next 3-4 weeks



- What is your core competency? What is your IP? Don't out source that, otherwise you don't keep the learning in your organization.


Product Market fit
==================

https://www.youtube.com/watch?v=0LNQxT9LvM0

Hardest Things at a startup
---------------------------

1. Finding Product Market fit
2. Hiring and building a world-class team
3. Making Money (distant 3rd)
4. Later, hot to build an organization that scalably and repeatedly launches great products


- Find a hidden need 
    - What are you substitute for, what need are you serving better? What job are you gired for
    - where are you getting pulled? Where are people hacking the thing you got and using it in a way you didn't intend? Double down on that



.. figure:: dev_product_steps.png 


UX Testing Session
------------------

1. Get someone to use your app/service in front of you
2. Encourage them to give open & honest feedback
3. Ask them to perform a task (you are not allowed to touch the phone/keyboard)
4. DO NOT SAY OR DO ANYTHING
5. Whach in extreme agony as they struggle to figure it out

You only need 3-5 testing sessions to uncover the most critical issues


Minimum Remarcable 

- Launch when your product is better what is out there. 


Prioritize
----------

1. Only one thing matters: focuse only on the things that get you to the next milestone (getting your first customer, reaching kPIs, profitability, Product Market fit)

2. Optimize for learning: Most people prioritize by creating a list sorted by cost & benefit. Instead ask yourself: "What is our biggest unknown that would rewrite our priority list?"


hubspot-growth frame work


Team
----

**Do not scale team past 20 before product market fit**


Enterprise Software company
===========================

(https://www.youtube.com/watch?v=tFVDjrvQJdw&list=PLvqUpY1b_iSonLIPtW0-v5t9hvnAFm36_&index=25&t=0s)


any sector where the underlying fundamentals change is open for disruption.

What are the underlying technology factores that are going to change the business model of this industry

spot technology disruption
    Look for new enabling technologies that create a wide gap between how things have been done and how they can be done. When the delta is at it's widest, then you have a huge opportunity. Make sure your able to ride the wave of a technology trend. 

Intentionally start small
    Start with something simple and small, then you expand over time. If people call it a "toy" you're definetely onto something. what are the gaps in the (existing) full solution that are significant where the customer would want to solve it with a discrete technology. Over time you will be able to expand (build out more services and serve other customers)

Find Asymmetries
    Do things that incumbents can't or won't do because it is economically or technically infeasible.

Find the almost-crazy outliers
    Go after the customers tha are working in the future, but that haven't totally lost their minds.

Listen to customers
    But don't always build exactly what they want. Build what they need. What is going to build the best solution for their problem that the customer would otherwise not have known how to ask for.

Modularize, Don't customize
    Every customer will want something a little bit different. Don't make the product suffer for this. Build a platform

Focus on the user
    Keep "consumer" DNA at the core of your enterprise product. This will always pay dividends. 

Your product should sell itself
    Sales should be used to navigate customers and close deals, not be a substitute for great product. The prodct itself adopted, spread  


- crossing the casm
- The innovators dilema (Clayton jchristensen)
- behind the cloud 


How to build a Product 
======================

Talking to users
----------------

- What do your users want? What do they want to achive? What is the job to be done?
- talk to them in person
- which of your users do you care about? 
- don't just talk to people who are currently using your platform, but talk to people who are not yet using your system. The people who have choosen to deliberately not use your product are some of the best people to talk to. They know what's wrong with your product.
- these problems of your app are often very small and get unnoticed by you but make a huge difference for your users
- "I'm going to talk to my users to validate my product ideas" (Product validation) is horribly wrong. You do it the other way around. You don't talk to users to validate product ideas. You talk to users to have product ideas. You talk to your users first and then you get ideas for your product

users
-----
There are two kinds of startups:

- Startups that are building things for themselves (your users are you and people like you)
- startups that are building something for a thirdparty group of users (analytic mode)



Product Development Cycle
-------------------------
- Growth masks all problems.. (watch out for that)
- Where do we want to be in 1 year
- 

Product Design
--------------

- the best product ideas are the ones that your users do anyway (despite you not having intendet them) hack into your product. You just have to streamline them a bit. 
- try to invert your assumptions, (twitch add button, we don't have to figure out when to play adds, the streamer knows that best)

Product Market fit
------------------
Building a startup is like pushing a bolder up a hill. Hitting product market fit is like reaching the top of the mountain and having the bolder roll down the other side. Now you have to chase it and you will notice. There will be a point where you catch up with the bolder and have to push it up another hill. 

Most Startups die before reaching product market fit. 


Analytics
---------

- data deph can haunt you
- having historic base lines for important user actions is really important
- pick your 5 to 7 most important user actions and log those
- Look at your numbers every week 
- be able to explain with conviction: "we are up 20% this week, because this feature moved this conversion rate on this page by this amount, and therefore we're up" 
- 


The big redesign
----------------
- 2nd system syndrom
- fix each thing one at a time
- your users don't care about your tech


Minimum Sellable Product
------------------------

- prioritize low effort and high impact things
- don't  


Whatsapp
========

Metrics
=======

https://www.youtube.com/watch?v=zsBjAuexPq4&list=PLvqUpY1b_iSonLIPtW0-v5t9hvnAFm36_&index=38&t=477s


- For your differentiaton: you want to pick one that will be a benefit to your customers and difficult for your competitors to achive
- focus on the custom group that would benefit the most from your core differentiator

- Cohort measurements of retention
- focus on your existing users (prevent ring of fire)
- does the usage per existing user increase over time? (Account Expansion metric)
- Measure Growth in percent increase per week:

.. hint::
    
    Instead of graphing revenue, graph growth rate of revenue. Now you're fighting for flat!

Iteration drives progress: 

- you have to be iterating in the right direction
- measure your iteration speed 
- how long does it take from a commit change to deployment

Don't focus to much on the metric, Don't game the metric. Make sure your not just increasing the metric, but the underlying value.


Metrcs reduce delusion, but sometimes this will hurt.
Most leaders rely on delusion to keep followers going
You want to learn form the past, be grounded in the present and optimistic for the future.

How to build and manage Teams
=============================

https://www.youtube.com/watch?v=alqHBCkSN8I&list=PLoROMvodv4rNpMrTeeh-627Lajh6uSUgY&index=4&t=0s


Your first 10 employees are really important, they will hire your next 50 and even if you fire your first 10 emplyoees the DNA in the company stays the same.


Your team needs the ability to think from first principle. 

in large companies you need people with good process. in startups you need people
with good iterativ and adaptation abilities. 

Founders role

If there is any job you could hire for, you should hire for it so you don't have to do it.  

Book: Start with Why?

Leadership in the Abiquirty of a startup is the founders role. Your vision and belive system is needed.  

Hire some magnets: think about who you can hire, that everyone would want to work with, and who can attract the next 20 - 50 people. 

How do I judge who is a Great VP of Marketing/CFO? You are not qualified to do so (because of Technical background). Knowing what you don't know 

Who's judment do you trust?


You need to fill functions in your company. But more important then doing a good job in their function, is that they'll take part it all the critical decisions of your company. "Which path should we take?", When you hire a VP of engineering, how will it make my VP of Marketing better. 
 
You should ask the same questions 3 times in 3 different contexts. If someone 
is making stuff up, they won't notice it and their answers won't match. 

- What do you love doing? What are you good at?
- What did you do really well at you last job?
- If you join this company, what do you think your strength would be?

Hiring is a really long process. If you need to fill a critical role, the hiring can easly take 6 months. and than the hire needs 6 months to get up to speed. So you loose 1 year if you make a hiring mistake. You should over hire. So you can compensate for the ones that didn't work out. Expect to have no more than a 50% success rate when hiring. 

If you find someone great, make up a title and hire. No great talent is worth giving away because you don't have a slot. Make up slot. They will pay for themselve. A great talent is never to expensive to hire. 

Plan for failure and evaluate your people. Try to assimilate them well. 
It's your job to make your employees successful. 

What are your company risks? Why can you fail? How can we mitigate these risks?

Startups are a pass fail game. Startup are not about dilution. They are about the propability for success. They are much more digital (binary) than you think. If you can increase the propability of success by hiring great people, by asking a lot of questions by finding your risks early, by having people surface problems you are going to increase your probabiltiy of success. Any either you are successful or not. There is not much inbetween. People worry way to much about dilution. 

Take the feedback from advisors but don't respect it to much. You want them to surface issues. THen use first principle thinking.  


How to think about PR
=====================


.. figure:: the_startup_curve.png

- everything you say to a reporter is on the record



Who are you?
------------

- What does your company do?
- Describe your customer (no jargon, have examples)
- What is the primary problem you are solving and why has no one solved it before?
- What is your KPI and how ist it growing?
- Who are your competitors?
- What is the primary reason why people use your product over a competitor?
- Plans for the next year?
- Why did you start the company, and why are you uniquely qualified to start it?

How to get Meetings with investors
==================================

- know any top level metric out of your head
- take where you are right now and pitch the biggest vision you could be in 10 years
- Meetings != progress
- Only a yes is a yes
- Push to close as quickly as possible


Design for Startups
===================

https://www.youtube.com/watch?v=9urYWGx2uNk&list=TLPQMjkwNzIwMjD4Azam4Izf5g&index=2


How To Succeed with a Startup
=============================

- KEY: Build a Product so good People spontaneously tell their friends
- A product that is simple to explain and easy to understand
- Exponential Growth in Market 
- Real Trends vs. Fake Trends -> Real trends have intense usage per user because they love it so much
- Evangelical Founder (Sell, Recruit, Fundraise, Talk to Press)
- Ambitious Vision 
- Hard Startup vs. Easy Startup -> Ambitious Projects are interesting
- How is this going to evolve into a vision that a lot of people want to help with and a lot of people want to be accociated with. 
- Confident and definete View of the Future -> "This is was will happen, we're going to do this"
- The entire Startup Eco system works best for companies that have a low chance of success but are huge if it works. This attracts the best people

Team
----
- THe team you build is the company you build => all founders go through a switch where they change from building a product to building a company and the company is really all about the team.
- Optimism is needed
- you need idea generators on the team
- we'll figure it out, I've got it - you need people who step up and take responsibiltity, no one should say 'that's not my department'
- Action Bias - you win by moving quickly, you need people who are willing to act by much less data and certainty then they would like to have.
- Blessing of inexperience

Momentum
--------
- One of the most important jobs you have as a founder, is to make sure you never lose Momentum
- As a founder in 
- Startups survive on their own Momentum
- if you have momentum people keep delivering results
- if you lose momentum it is really hard to get motivation back



- competitive advantage over time
- what is the long term monopoly effect 
- where is the network effect


- sensible business model 
- distribution strategy

- Frugality, Focus, Obsession, Love 

Why Startups win
----------------
- look for ideas that sound like a bad idea but are actually good. it is much easier for you as a startup to get one yes that for a product manager at a big company to get all yeses from the upper management
- if the idea was obviously good the opportunity wouldn't exist and a large business would have build it 
- Fast changing Markets
- Platform shifts

What Startup Investors should look into
=======================================

- is this company going to be able to recruit hundreds of really talented employees that could otherwise start their own company
- mostly pick the founders
- obsession, focus, frugality and love
- Intelligence: idea generating. Good founders have ideas all the time.
- Communication and Evangelism Skills
- Execution speed (How quickly can you test a hypothesis, How much progress can you make in one week?)
- Relentless cadence of execution is a great predictor of success
- Rate of improvement of the founder: just like startups need a good growth rate, the founders need to grow too
- beaware of bad motivations
- Growth rate of Market - Care about the size of the Market in 10 years
- Prefer a small fast growing market over a very large market today
- Real trends vs. Fake trends (a real trend is a trend where a not of people are participating yet but the people who use it use it every day and tell their friends spontaneously how great it is.)
- Are people actually using the platform?
- Good ideas that look like bad ideas
- Great Products 
- a product so good that at so
- intuitions about exponential growth are terrible - model it out
- Accumulating advantage, network effect, moat (A company that has more pricing power as it gets bigger, that get's harder to compete with as it gets bigger)
- What do you understand that other people don't
- Investors can help with Hiering & Future Fundraising, super availability for tactical advice
- invest in optimism, invest in the future you want to see not in the future you fear 
- What have you done in the last month? A little bit of slop is worth a lot of y intercept
- Don't OVERRAISE - TOO much money is a Killer

Make sure you listen carefully to what the investor has to say. If you can get the investor to talk more than you, your probability of a deal skyrockets. In the same vein, do what you can to connect with the investor. This is one of the main reasons to do research. An investment in a company is a long term commitment and most investors see lots of deals. Unless they like you and feel connected to your outcome, they will most certainly not write a check.

Great Founder Checklist
=======================

- Clear, consise Communication
- moves fast
- Accomplishes a lot with little
- implausibly ambitious, or seemingly frivolous ideas
- is a talent magnet
- makes something people want
- determined and committed

How to build the future
=======================

- Pick the intersection between: What are you good at? What do you like? How can you create value for the world?
- work hard early in your career to benefit from compounding effects
- momentung is really enegizing, the lack of momentum is really draining
- Ask for what you want. Your not agressive enough


- Growth is the lifeblood of innovation
- don't procrastinate make decisions quickly
- investors can help you with management team, distribution & hiering, mentoring

- for a startup to work you have to get the team, the technology and the business strategy right. 
- to make money you have to create something of value to the world, and you have to capture some fraction of the value you create.

Leadership
==========
- you can only be yourself - so be authentic

3 attributes: 

- great leaders think and communicate clearly (paint clear and compelling vision of the future)
- great communication needs to be simple. this is hard and needs preparation
- amazon retail strategy: "customers will always want: lower prices, larger selection and faster delivery" -> everyone in company knows that if we do something to impact one of these three elements we're acting in amazons strategic long term intrest.

- great leaders have good judgment of people. 
- how do you decide who to empower?
- learn from the hiring experience: who did you hire? why did you hire them? what went wrong? what went right?
- 

- strong personal integrity and commitment.
- make work your life mission.

- best way to measure great leaders is by the trust of their employees

Get big fast business
---------------------

there is an attpmt to capture uncaptured teritory, before anybody else does the same thing (uber, amazen). Reason to try and grow fast is because there are network effects. The more users you have the more valuable your network is. The network effect creates user lockin. 

Organic growth business 
-----------------------

you already have a million competitors and all you're trying to do claw the customers away from them 1 at a time (ben&jerrys)

With an organic growth

Product management
==================
- listen to customers: be maniacally focused on understanding your customers problem
- stop listening to your customers when it comes to solutions. Your customers are the least qualified people to come up with solutions.
- Watch the Competition: they are a rich source of information that can help you better understand your customers.
- Don't wath the competition: don't worry about the competition
- Be a thief: don't be afraid to take ideas from your competition
- Get Paid: for every feature ask the customer: will you pay for this?
- stop worrying about getting paid: this causes incremental changes for incermental improvement: you need to appeal to the emotional and social need of the customer and it is hard to put an ROI on that. 
- Speed Up: inaction kills value. The cost of delay is huge. The features and products we ship have a limited shelflive. The longer it takes to ship them the less value they have. 
- Say, 'NO' - only build stuff for your customers and not any other stakeholder. 
- Say No for the right reasons, not because you don't like the person who suggested it
- Don't be a visionary: 
- Don't confuse yourself with the customer
- Be dumb. 