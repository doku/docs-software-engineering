===================
Product Development
===================

.. note:: Developers and Executives should do direct customer support.

Product Development Cycle
=========================

- Have a scheduled cycle (1 week or 2 weeks)

- have someone in charge of product (they don't dictate what gets build,
  but are responsible you meet the goals of the development cycle and you get product
  out of the door. 
- Establish your KPIs
- Head of product creates theme for this weeks product development cycle based on the KPIs you're currently
  tracking
- Everyone in company is in product meeting
- Have whiteboard with 3 Categories: "new features", "bugs" , "Tests"
- Everyone comes up with anything they think could move the KPI and it is written on the board
- Grade each item with "easy (couple hours)", "medium (half a day)" and "hard (multiple days)" (lead by your tech lead in company) 
- everyone feels included in the process and is able to back someone elses idea rather than just their own
- you have an objective framework to figure out what 
- pick the hard ideas first, then medium, then easy (picking the hards is the easiest, as 
- with everyone in the room you spec these ideas out, and who is responsible for each part
- you put this speck in your product development tracker

If you have confidence in your product development cycle, even if this cycle doesn't create the 
results you want the next cycle will or the one after. 

For tests  
