============================================
Continuous Experimentation in the B2B Domain
============================================

Empirical evaluation of new features and products is required to make datadriven
decisions.

#. evolving the software by frequently deploying new versions
#. using customers and customer data throughout the development process
#. testing new ideas with customers to drive the development process and increase
   customer satisfaction

Deployment is seen as a starting point for tuning the functionality rather than
delivery of the final product. 
