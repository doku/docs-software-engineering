============
Lean Startup
============

*A summary of Lean Startup from Eric Ries.*

Startup success is not a consequence of good genes or being in the right place
at the right time. Startup success can be engineered by following the right
process which means it can be learned.

.. note::
  The lean startup methodology can be characterized by an extremely fast
  cycle time, a focus on what customers want (without asking them), and a
  scientific approach to making decisions.

Startups do not yet know who their customer is or what their product should be.

Leadership in todays economy means, creating platforms for experimentation for
your teams. You need to put your product in a position where you are able to pivot.

Because startups often accidentally build something nobody wants, it doesn't
matter much if they do it on time and on budget. The goal of a startup is to
figure out the right thing to build - the thing the customers want and will pay
for - as quickly as possible.

Instead of making complex plans that are based on a lot of assumptions, you can
make constant adjustments with the **Build-Measure-Learn** feedback loop. You can
learn if and when to pivot or persevere on the current path.

To achieve their vision, startups employ a strategy, which includes a business
model, a product road map, a point of view about partners and competitors and
ideas about who the customer will be. The product is the end result of this
strategy.
The product changes through the process of optimization and the strategy changes
on a pivot.

A company's only sustainable path to long-term economic growth is to build a
"innovation factory" that uses Lean Startup techniques to create disruptive
innovations on a continuous basis. Leadership is required to create conditions
that enable employees to do the kinds of experimentation that entrepreneurship
requires.

Successful entrepreneurs posses a unique combination of perseverance and
flexibility.

Principles of the Lean Startup
==============================

1. Entrepreneurs are Everywhere
-------------------------------

2. Entrepreneurship is Management
---------------------------------
Entrepreneurship is management. Our goal is to create an institution, not just
a product. This requires practices and principles geared to thee startup context
of extreme uncertainty.

3. Validate Learning
--------------------
Startups exist to learn how to build a sustainable business. This learning can be
validated scientifically by running frequent experiments to test each element of
your vision.

Validated learning is the process of demonstrating empirically that a team has
discovered valuable truths about a startup's present and future business prospects.

Learning is the essential unit of progress for startups. **Validated learning**
is always demonstrated by positive improvements in the startups core metrics.

You think about productivity in a startup not in terms of how much stuff we are
building but in terms of how much Validated learning we are getting for our effort.

In a startup, who the customer is and what the customer might find valuable are
unknown. In a startup you need to systematically figure out the right things
to build. Every Startup in any industry is a grand experiment.

Every product, every feature, every marketing campaign - everything a startup
does - is understood to be an experiment designed to achieve validated learning.

A true experiment follows the scientific method. It begins with a clear hypothesis
that makes prediction about what is supposed to happen. It then tests those
predictions empirically. Just as scientific experimentation is informed by theory,
startup experimentation is guided by the startups vision. The goal of every startup
experiment is to discover how to build a sustainable business around that vision.

.. hint:: If you can not fail, you can not learn.

.. glossary::

  leap-of-faith assumptions
    The riskiest elements of a startup's plan, the parts on which everything
    depends, are called **leap-of-faith assumptions**. The :term:`value hypothesis`
    and :term:`growth hypothesis` are the two most important leap-of-faith
    questions to answer.

  value hypothesis
    The **value hypothesis** tests whether a product or service really delivers
    value to customers once they are using it.

  growth hypothesis
    The **growth hypothesis** tests how new customers will discover a product or
    service.

In the lean startup model an experiment is more than just a theoretical inquiry,
it is also a first product.

.. note::
  Success is not delivering a feature, success is learning how to solve the
  customers problem.

You need to identify the elements of a plan that are assumptions rather than
facts and figure out ways to test them.

Planning is a tool that only works in the presence of a long and stable operating
history.

4. Build-Measure-Learn
----------------------

At its heart, a startup is a catalyst that transforms ideas into products. As
customers interact with those products, they generate feedback and data. The
feedback is both qualitative (such as what they like and don't like) and
quantitative (such as how many people use it and find it valuable).

.. figure:: build_measure_learn_feedbackloop.png
	 :alt: build_measure_learn_feedbackloop

Turn Ideas into products, measure how customers respond, and then learn whether to
pivot or persevere. All processes should be geared to accelerate that feedback
loop.

.. hint::
  We need to focus our energies on minimizing the **total** time through the
  feedback loop.

Upon completing the Build-Measure-Learn loop we decide whether to pivot the
original strategy or persevere.

The planning of the feedback loop works in reverse order: we figure out what we
need to learn, use :ref:`innovation-accounting` to figure out what we need to
measure to know if we are gaining validated learning, and then figure out what
product we need to build to run that experiment and get that measurement.

The first step is to identify the :term:`leap-of-faith assumptions`, then enter
the build phase and build as quickly as possible a MVP.

.. glossary::

  MVP
    The MVP (minimum viable product) is that version of the product that enables
    a full turn of the Build-Measure-Learn loop with minimum amount of effort and
    the least amount of development time. This is not necessarily the smallest
    product imaginable.

    You must be able to measure the MVPs impact and get it in front of potential
    customers to gauge their reactions. A MVP helps entrepreneurs start the process
    of learning as quickly as possible.

    Unlike a prototype or concept test, an MVP is designed not just to answer
    product design or technical questions. Its goal is to test fundamental
    business hypothesis.

    Any additional work beyond what was required to start learning is waste, no
    matter how important it might have seemed at the time.

    MVPs require the courage to put one's assumptions to the test. If customers
    react the way we expect, we can take that as confirmation that our assumptions
    are correct.

    For an effective MVP: remove any feature, process or effort that does not
    contribute directly to the learning you seek.

    MVPs are often bad news. If an MVP fails, teams don't give up hope and
    end the project, but this is a solvable problem.

    Great Examples for MVP Testing:
      - Your hypothesis is that faster loan approval gives you a competitive advantage, 
        so go a head and just approve everyone automatically (regardless of who they are) 
        and see if you create value for your business and customer. As soon as this MVP has
        validated you hypothesis then you go ahead and invest in building the complicated
        approval based on credit score etc.

In the Measure Phase the biggest challenge is to determine whether the product
development efforts are leading to real progress.

Startups need extensive contact with potential customers to understand them. The
first step in this process is to confirm that your leap-of-faith questions are
based in reality, that the customer has a significant problem worth solving. The
goal of such early customer contact is not to gain definitive answers. Instead,
it is to clarify at a basic, coarse level that we understand our potential
customer and what problems they have. With that understanding, we craft a
**customer archetype**, a brief document that seeks to humanize the proposed
target customer.

Lean UX recognizes that the customer archetype is a hypothesis, not a fact. The
customer profile should be considered provisional until the strategy has shown
via validated learning that we serve this type of consumers in a sustainable
way.

If we don't know who the customer is, we don't know what quality is.

Customers don't care how much time something takes to build. They only care if
it serves their needs.
This doesn't mean operating in a sloppy or undisciplined way. There is a category
of quality problems that have the net effect of slowing down the Build-Measure-Learn
feedback loop. Defects make it more difficult to evolve the product. They actually
interfere with our ability to learn and so are dangerous to tolerate in any
production process.

There is no reason to fear other stealing your idea, and this should not be used
as an excuse for not releasing a MVP or talking to people about your product.

.. note:: It's not the idea that's important, the execution is what matters.

The only way to win against competitors is to learn faster than anyone else.

When you fear the MVP could damage your brand image, just launch it under a
different brand name.

.. _innovation-accounting:

5. Innovation Accounting
------------------------
We need to measure progress, set up milestones and prioritize work. This requires
innovation accounting.

Innovation Accounting is a systematic approach to figuring out if we are making
progress and discovering if we are actually achieving validated learning.

A startups job is to rigorously measure where it is right now, confronting the
hard truths that assessment reveals, and then devise experiments to learn how
to move the real numbers closer to the ideal reflected in the businessplan.

Innovation accounting enables startups to prove objectively that they are learning
how to grow a sustainable business. Innovation accounting begins by turning the
:term:`leap-of-faith assumptions` into a quantitative financial model.

The rate of growth in a manufacturing company depends primarily on three things:

- the profitability of each customer
- the cost of acquiring new customers
- the repeat purchase rate of existing customers

Innovation accounting works in three steps

#. use a MVP to establish a real data baseline on where the company is right now.
#. a Startup must attempt to tune the engine from the baseline toward the ideal
#. Pivot or persevere:

  - if you make good progress towards the ideal continue
  - else you must conclude that your strategy is flawed and needs serious change
  - when a company pivots an new baseline has to be established using a MVP

Every product development, marketing or other initiative that a startup undertakes
should be targeted at improving one of the drivers of its growth model.

.. hint:: A good design is one that changes customer behavior for the better.

.. glossary::

  cohort analysis
    In cohort analysis instead of looking at cumulative totals or gross numbers
    such as total revenue and total number of customers, one looks at the performance
    of each group of customers that comes into contact with the product
    independently. Each group is called a cohort. Each cohort represents a
    independent report card.

Every company depends for its survival on sequences of customer behavior called
flows (like logging in, purchasing an item in the online store). Customer flows
govern the interaction of customers with a company's product. They allow us to
understand a business quantitatively and have much more predictive power than
do traditional gross metrics.

Once your efforts are aligned with what the customer really wants, your
experiments are much more likely to change the customers behavior for the better.

Poor quantitative results force us to declare failure and create the motivation,
context, and space for more qualitative research. These investigations produce
new ideas - new hypothesis - to be tested, leading to a possible pivot. Each
pivot unlocks new opportunities for further experimentation, and the cycle
repeats. Each time we establish a baseline, tune the engine, and make a decision
to pivot or persevere.

.. glossary::

  vanity metrics
    Traditional metrics like (total users or total paying customers) can be vastly
    misleading for startups and are therefor called vanity metrics.

    A metric that does not clearly demonstrate cause and effect.

  actionable metrics

Innovation accounting requires us to avoid using :ref:`vanity metrics`.
Due to the cumulative nature of vanity metrics they paint the rosiest picture
possible and tend to be a hockey stick graph simply by adding new customers.
But the change in behavior of customers is not shown in vanity metrics.

In contrast by presenting data using the :term:`cohort analysis`, the graph
clearly shows if you are not improving the yield on each new group.

By looking at :ref:`vanity metrics` you can not tell whether you are building a
sustainable business. Innovation Accounting does not work if you are mislead
by vanity metrics. We need to judge our business and our learning milestones with
:ref:`actionable metrics`

The PROBLEM with Agile
----------------------

There is a fundamental problem when you apply agile development practices in
a startup context:
**How do we know if we are making the right decisions in terms of feature prioritization?**

.. hint::

  In agile development engineers agree to adapt the product to the business's
  constantly changing requirements but are
  **not responsible for the quality of those business decisions**.

Agile is an efficient system of development from the point of view of the
developers.

.. attention::
  That which optimizes one part of the system necessarily undermines the system
  as a whole. (system theory)

Feature Testing
---------------

When using cohort-based metrics you can eliminate seasonal-effects etc. by
launching each feature as a true split-test experiment.

.. glossary::

  split-test experiment
    Also called A/B Testing
    A split-test experiment is a experiment in which different versions of a
    product are offered to customers at the same time. By observing the changes
    in behavior between two groups, one can make inferences about the impact
    of different variations.

Lean startup incorporates split-testing directly in product development. Split-testing
helps you refine your understanding of what customers want and don't want.

A :ref:`user-story` should not be considered complete until they led to validated
learning.

Teams working in this system begin to measure their productivity according to
validated learning, not in terms of the production of new features.

Metrics
-------

Metrics need to be actionable, accessible and auditable.

For a report to be considered actionable, it must demonstrate clear cause and
effect. Otherwise it is a :term:`vanity metrics`. When cause and effect is
clearly understood, people are better to learn from their actions.

Reports need to be accessible, so that everyone can understand them.
This is why cohort-based reports are the gold standard of learning metrics:
they turn complex actions into people based reports.
Each cohort analysis says: among the people who used our product in this period,
here's how many of them exhibited each of the behaviors we care about.

Accessibility also refers to the widespread access to the reports. Every employee
(and investors etc.) should have access to these report and get regular updates.
This allows these experiment summaries to become the de facto standard for settling
product arguments.

We must ensure that the data is credible to employees. All metrics need to be
auditable. Therefor we need to be able to test the data by hand, by talking to
the customer. Managers need the ability to spot check the data with the real
customers. This also allows you to gain insight into why customers are behaving
in the way the data indicates.

.. attention::

  Only 5% of entrepreneurship is the big idea. The other 95% is the gritty work
  that is measured by innovation accounting: product periodization decisions,
  deciding which customer to target or listen to, and having the courage to
  subject a grand vision to constant testing and feedback.

Product Development
===================

#. Do consumers recognize that they have the problem you are trying to solve?
#. It there was a solution, would they bud it?
#. Would they buy it from us?
#. Can we build a solution for that problem?

It is common to jump to the fourth question and build a solution before confirming
that customers have the problem.

Pivot
=====

To Pivot is the act of changing directions but staying grounded in what we have
learned. One foot firmly rooted in what we have learned, changing one other thing
about the business at a time.

When deciding whether to pivot or persevere we need to answer the question:
are we making sufficient progress to believe that our original strategic
hypothesis is correct, or do we need to make a major change?. That change is
called pivot: a structured course correction designed to test a new fundamental
hypothesis about the product, strategy, and engine of growth.

Startup productivity is not about cranking out more widgets or features. It
is about aligning our efforts with a business and product that are working to
create value and drive growth. Successful pivots put us on a path toward
growing a sustainable business.

The goal of creating learning milestones is not to make the decision whether to
pivot or persevere easy, it is to make sure that there is relevant data in the
room when it comes time to decide.

.. tip:: Failure is a prerequisite to learning.


The **runway** of a startup is the amount of time remaining in which a startup
must either lift-off or fail. It is usually defined as the remaining cash in
the bank divided bd the monthly burn rate, or net drain on that account
balance

If we can reduce the time between pivots, we can increase our odds of success
before we run out of money.
You shouldn't calculate in how many months of burn do I have left but in how many
opportunities to pivot (number of opportunities it has to make a fundamental
change to its business strategy) do I have left. By reducing the time it takes to pivot
we can increase our runway.

To be able to get to each pivot faster we need to find ways to achieve the same
amount of validated learning at shorter cost or in shorter time.

The only way to get yourself in a position where you have to pivot, is to make
specific concrete predictions ahead of time, that if they turn out to be wrong
will actually call your theory into doubt.
When you have an unclear hypothesis, it's almost impossible to experience complete
failure, and without failure there is usually no impetus to embark on the
radical change a pivot requires.

Symptoms for a needed pivot are a decreasing effectiveness of product experiments
and the general feeling that product development should be more productive.

Every startup should have a regular "pivot or persevere" meeting. Each pivot or
persevere meeting requires the participation of both the product development and
business leadership team.

The product development team must bring a complete report of the results of its
product optimization efforts over time as well as a comparison of how those
results stack up against expectations. The business leadership team should
bring detailed accounts of their conversations with current and potential customers.

The decision to pivot is so difficult that many companies fail to make it.

.. hint::
  A pivot is a new strategic hypothesis that will require a new MVP to test.

If we take a wrong turn, we have the tool we need to realizes it and the
agility to find another path.

Catalog of Pivots
-----------------

A pivot is a special kind of change, designed to test a new fundamental hypothesis
about the product, business model and growth engine.

.. glossary::

  Zoom-in Pivot
    Refocusing the product on what previously had been considered just one feature
    of a larger whole.

  Zoom-out Pivot
    What was considered the whole product becomes a single feature of a much larger
    product.

  customer segment pivot
    The company realizes that the product it is building solves a real problem for
    real users but that they are not the type of customers originally planned
    to serve. The product hypothesis is partially confirmed, solving the right
    problem, but for a different customer than originally anticipated.
    Keeping the functionality of the product the same but changing the audience
    focus.

  Customer Need Pivot
    The target customer has a problem worth solving, but is is not the one we
    originally anticipated.

  Platform Pivot
    A platform pivot refers to a change from an application to a platform or vice
    versa.

  Business Architecture Pivot
    Companies generally follow one of two major business architectures: high
    margin, low volume (complex systems model) or low margin, high volume
    (volume operation model).

    In a business architecture pivot, a startup changes architecture

  Value Capture Pivot
    Capturing value (monetization or revenue model) is an intrinsic part of the
    product hypothesis. A change often has far reaching consequences for the
    rest of the business, product, and marketing strategies.

  Engine Growth Pivot
    There are three primary engines of growth that power startups: the viral,
    sticky and paid growth model. In this pivot a company changes its growth
    strategy to seek faster or more profitable growth.

  Channel Pivot
    A channel (distribution channel) is the mechanism by which a company
    delivers its product to customers. A channel pivot is a recognition that the
    same product could be delivered through a different channel with greater
    effectiveness.

  Technology Pivot
    Technology Pivots are a sustaining innovation, an incremental improvement
    designed to appeal to and retain an existing customer base. (more common in
    established businesses)
