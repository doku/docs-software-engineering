=========
Financing
=========

Valuation
=========

.. glossary::

  Pre-Money Valuation
    The Value of the Company before the Investment

  Post-Money Valuation
    The Money of the Company before the Investment plus the Investment Amount

  fully-diluted Valuation/ non-diluted Valuation
    - You take the company shares plus the share options of employees into account
    - Impact of Employees' pool

An Investor gets the percentage of shares equivalent to percentage his investment
is on :term:`Post-Money Valuation`. This has to be managed very carefully when
negotiating with multiple investors.

When talking to investors you should never commit to a :term:`Post-Money Valuation`,
because the percentages of shares each investor gets changes due to changes in
the :term:`Post-Money Valuation` in the round of financing.

.. note::

  Whenever you are talking to somebody you never tell them how many percentages
  they are getting in the end. You only discuss :term:`Pre-Money Valuation` and
  the amount of money the investor wants to invest.

There is never a transfer from existing shares of existing shareholders to new
investors for the purpose of company financing. The company is issuing new shares,
gives them to the investor and gets the money. This is called **primary transaction**.

If founders or shareholders sell part of their shares to investors this is
called **secondary transaction**.

When the investor talks about a non-assigned employees pool of 10% he means that
you will create additional 10% of shares to give future employees.

It makes a lot of sense to give the key people in your company rather bigger
portions of ESOP (Employee Stock Ownership Plan) so you can bind them to your
company. The effect of ESOP an employee binding is almost zero if the EOSP is
rather small. It is much more effective to give the key persons larger ESOPs
than giving everyone small ESOPs.

Founders Reverse Vesting
========================
If a founder leaves early, the founder needs to return a portion of his shares
to the company, so that the company can use it to find a new cofounder.

It is very toxic to have an outsider of the company holding a large portion of
the shares.
It is in the interest of everyone to have as strict reverse vesting as possible.

The standard practice is to have 4 year vesting and the clock does not start
until 1 year in.

Voting out a Founder
--------------------
There is a chance that one of the team members is not the right fit and you
should rather vote them out.

You need to make decisions for the benefit of the company. As soon as there is
a lawsuit between large shareholders or founders your company is dead.

Fundraising
===========

Raise Money from investors, when you can demonstrate Product-market fit.

Break down your product to the very core component, and then try to test that
before you go out and raise money.

It is very rare when you want to build something that is disruptive that
you really monetize hardcore from day one. But instead you want to take money,
invest it into the product and customer acquisition in order to grow your service.

You always have to choose an investor that shares your vision for the market and
that is a cultural fit to your company.

.. hint::
  You need to pick your investors and boardmembers (not just by brand also
  individually) very carefully, because they will be part of the team and they
  are the only part of the team that you can't fire.

Angel Investors fund you in the early stage of your product when you are still
working on your product market fit.
Venture Capitalist come in when it is just a matter of scaling.

#. Angle Round
#. Preseed Round
#. Seed Round (200 000- 500 000€)
#. A-Round (2M - 3M)
#. B-Round
#. C-Round

A proper finance-round should last 12 months of company operation.

Even if you can finance your self, it is advisable to get venture capital.
Convincing venture capitalist is in some way also a market test of your idea.

The role of a investor is that of a catalyst, to test the idea and demonstrate
trust in the founders.


You should know who your selling to. When you pitch your startup to an investor
you are trying to make a sale. That means that you have to know the specific goals
of the individual investor. A Angel investor will look for very different things 
in a startup compared to a private equity firm. If you know the goals of the investor
you are able to align you pitch with the goals of their fund and increase your 
chance of getting funded.

Knowing your investors very well is not only important for pitching to them it is 
also crutial for your startups success. You have to have a very good relationship
with your investors and your goals should be alligned. Communitcate frequently
with them, so that everyone is on the same page. They will be the only guys that you 
can't fire. So make sure your goals are aligned. When you are getting funded by
a VC fund you will most likely work very closely with only one VC from the firm.
Be aware that you are entering a potentialy 5 to 10 year long relationship with
the VC. You have to get to know them personally.

When you are an entrepreneur you want to get a yes or no from your VC as fast
as possible. If it takes the VC longer than 3 weeks you should start to look 
for different investors

https://www.startupfestival.com/speaker/angela-tran-kingyens/

Investment
----------

Seed Investors invest in a team and an idea. So team dynamic and motivation are
important.

Technical Entrepreneurs need to get a good grip on the core economics of what
they are trying to solve. You need to know that the things you are building have
an exceptional impact in the space you are going after.

To find investors, go to other startups and ask them to introduce you to
their investors. 

Venture Capital
===============

The the two key things that matter most in an actual term sheet negotiation are
**economics and control**.

