======================
Why work at a Startup?
======================


- Small team: you know everyone and know who makes what decisions
- Fast Decision making
- Relatively high impact on business
- Possibility to truely shape a product
