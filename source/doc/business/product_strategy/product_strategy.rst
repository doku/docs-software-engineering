.. _product-strategy:

================
Product Strategy
================

A product strategy is the set of choices a company makes in order to achieve it's
objectives.

The product strategy should be well documented and all important stakeholders
should be aware of it.

A good product strategy helps you to decide what to do, and what not to do.
In order to trust your strategy, you need to be able to clearly measure your
progress along the way.

.. hint::
  Even if you have a imperfect product, you should rather launch it and get
  feedback, than wait until you get the perfect product.

Elevator Pitch Framework
========================

**FOR** *target customer*, **WHO HAS** *customer need*, *product name* **IS A**
*market category* **THAT** *one key benefit*. **UNLIKE** *competition*, **THE PRODUCT**
*unique differentiator*.

Target Customer
---------------
Who you build for is a critical decision that can make or break your strategy.

It is better to create a critical mass of value for a distinct target audience
than to spread thin across many audiences, failing to satisfy any of them.

You should consider the size of your target customer segment.

Customer Need
-------------
Solutions must address real problems or satisfy needs.

Market Category
---------------

Key Benefits
------------
Features are the things that the product can do.
Benefits are the value created by the product for the user.
Customers adopt or by a product not for the features that they have but for the
benefits they create.

Differentiation
---------------
Differentiation isn't always about extra benefit, it's about doing something really
well and standing out.

Maintain your Strategy
======================
You develop a strategy considering the way things are at a certain point of time.
Due to changing environments you have to update and correct your course over time.
