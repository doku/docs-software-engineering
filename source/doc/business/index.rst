========
Business
========

.. toctree::
  :glob:

  startup/*
  why_start_a_startup/*
  monopoly_business/*
  financing/*
  employee_shares/*
  legal_foundation/*
  team/*
  execution/*
  scaling_business/*
  lean_startup/*
  startup_ceo/*
  lean_movement/*
  product_development/*
  product_strategy/*
  product_roadmap/*
  continuous_experimentation_b2b/*
  startup_school/*
