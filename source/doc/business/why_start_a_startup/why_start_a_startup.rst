====================
Why start a startup?
====================

Working on a startup is the  single best way to have the largest on
the most people in the shortest amount of time.

Passion: You **need**  to do it.

- You'll need passion to endure the struggle
- You'll need passion to effectively recruit

Aptitude: The world needs you to do it

- The world needs it (otherwise do something that the world needs)
- The

The ugly side of being a founder
================================

- Responsibility: your team bet the best years of their life on you

.. note:: The number one role of a CEO is managing your own psychology.

Founders burnout is a serious problem and traditional techniques aimed to deal
with burnout usually don't work for founders. 
