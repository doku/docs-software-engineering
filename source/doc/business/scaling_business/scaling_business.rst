==================
Scaling a Business
==================

If you are scaling a business there is no way around a Organisation structure
with clear responsibilities.

.. figure:: organisation_building.png
	 :alt: organisation_building

You need to start early thinking on how you structure and organize your company
(around 15-20 people). You need to embrace continuous change.

You should be rather careful with the titles you give out to employees. In fast
growing companies it is typically that the company grows faster then the employee
so you need to insert a new leadership layer.

.. figure:: process_scaling.png
	 :alt: process_scaling

Make sure you are able to change processes and tools as your company outgrows them.

If something can go wrong, and you have a large scale business it definitely
will go wrong.

.. hint:: Take measurable risk and learn quickly.

You need to have measurable results and learn out of it overtime and optimize
over time.

Culture is major foundation for startup success. Culture eats strategy for
breakfast (Peter Drucker).

Typical startup advantage in new business models against corporates:

- speed of decision making
- risk culture
- independence
- no technology legacy

You need granular, data-driven business decision.
