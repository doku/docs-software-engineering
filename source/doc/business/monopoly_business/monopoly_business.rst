================
Startup Monopoly
================

A business creates :math:`X` dollars of value in the world and captures :math:`Y`%
of :math:`X`. :math:`X` and :math:`Y` are independent variables.

To create a valuable company you have to create something of value and capture some
fraction of the value you have created.

There are only two kinds of companies: companies in perfect competition and
companies who have a monopoly.

If you are founder of a startup you always want to aim for monopoly and avoid
competition.

It's easier to dominate a small market than a large one. You start by dominating a
small market and then expand it in concentric circles. If you go for a giant market
from day one that's typically evidence that you have not defined your categories
correctly and you will face to much competition.

It is not enough to have a monopoly for just a moment, you need one that lasts
over time.

You should not overvalue growth rate, but also value durability. The question
whether a company will still be around in 10 years is what actually dominates
the value equation.

Retention is the single most important thing for growth. Retention comes from
having a great idea and a great product to backup that idea and great
product market fit.

.. figure:: retention_curve.png

   percent of monthly active users vs number of days from acquisition.

If you end up with a retention curve that asymptotes to a line parallel and
not equal to the x-axis you have a viable business and product market fit for
some subset of the market.

If this curve doesn't asymptotes don't try to do growth hacking but focus on
achieving product market fit.

Think about what the magic moment is for your product and get people connected
to it as fast as possible.

Building an incredible product is all about optimizing for the power user.

Characteristics of Monopoly
===========================

There is a time element to all of these characteristics.

- proprietary technology
- network effects
- economies of scale
- branding


- Vertical Integration (Tesla, SpaceX)

Growth
======

Every company needs different growth metrics, which have to be clearly defined
and everyone in the team has to be aware of the key growth metric.

- Growth Accounting Framework
- think about the marginal user

Growth is the interaction between conversion and churn.

Tactics
-------

- Virility (Payload, Frequency, Conversion-Rate), K-Faktor needs to be over 1
- SEO

	- What do people search for that is related to your site?
	- How many people search for it?
	- How many other people are ranking for it?
	- How valuable is that search term for you?

- ESPN (Email, SMS, Push-Notifications)

	- You have to get them delivered
	- Open-Rate and Conversion-Rate
	- What should you be notifying people of? Trigger based, personalized notifications

- SEM
- Affiliates/referral programs
