===============
Employee Shares
===============

ESOPS
=====

Virtuelle Anteile werden Mitarbeitern ersprochen. Die Mitarbeiter bekommen zum Beispiel Shares aus dem Employee Pool , in der Regel 10% sin dies der Total shares. 
Beim Exit werden 10% neue shares erzeugt und damit die bestehenden diluted, die bestehenden Shares verlieren also an Wert. 
Die Mitarbeiter sind verpflichtet diese Anteile später beim Exit direkt an der Käufer zu verpflicheten



Vesting
=======

Promised shares are provided over time.
Cliff: in the frist year no shares are provided
Catch up: after the first year the shares of the employee jumps to 
Beim Exit vestet der Mitarbeiter auf die vollen 100% hoch (exelerated vesting) unter der Bedingung, dass der Mitarbeiter noch 1-2 Jahre im Unternehmen bleibt.
Vesting Cap: Vesting wird bei e.g. 75% gecapt die übrigen Anteile werden erst beim Exit gevestet. 
Dies führt dazu, dass selbst komplett gevestete Mitarbeiter ein Incenitve post Exit zu bleiben

De-Vesting: Nachdem ein Mitarbeiter die Firma verlassen hat sinken seine gevesteten Anteile monatlich (in der selben Rate mit der sie ursprünglich gestiegen sind)
Dies schaft wieder freie Anteile für neue Mitarbeiter

Buy-Back-Option: Nach einem Leaver-Event ist der Mitarbeiter verpflichtet seine Anteile dem Unternehmen wieder zurückzuverkaufen.
