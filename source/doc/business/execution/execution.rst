=========
Execution
=========

What being a founder really means is signing up for the years long grind on
execution.

The way to have a startup that executes well is by executing well yourself.
Everything in a startup gets modeled my the founders.

.. hint::
  Ideas by themselves are not worth anything. Only executing well is what creates
  value.

The startup CEO has five jobs:

- set the vision
- Raise money
- Evangelize
- Hire and manage
- Make sure the entire company executes

Execution is determined by:

- Are you able to figure out what to do?
- Can you get it done?

    - Focus: What are you spending your time and money on? What are the two or three
      most important things?
    - Intensity

        - Relentless operation rhythm
        - Obsession with Execution
        - Bias towards action

You need to figure out what the two or three most important things are and then
just do those. If you don't get really good at limiting yourself to these most
important things every day, you will never be great at actually getting things
done. You need to say NO a lot. You need to set a small number of overarching
goals that everybody in the company knows. You can't be focused without great
communication.

You can have a startup an one other thing. Startups are not a great choice for
work-life balance.

It is easy to move fast or be obsessed with quality, but you need to have both.

Every time you talk to great founders, they've gotten new things done.

Momentum and Growth are the live blood of startups.
A good way to keep momentum is to establish a operating rhythm, where you
ship product and launch features in a regular basis.

Don't worry about competitors at all, until they are actually beating you with
a real shipped product.

.. note::
  The competitor to be feared is one who never bothers about you at all, but goes
  on making his own business better all the time. (Henry Ford)

Customer Feedback
=================

When someone tells you to build a feature, you should not build it right away, but
get to the bottom of why they are asking you to build the feature. Usually what
they are suggesting is not the best idea, but what they are really suggesting
is that they have this other problem.

The mistake most founders is that they don't talk enough to customers.

Cost Per Reasonable Decision (CPRD)
===================================

You should inform people who don't have valuable data/context to inform the
decision about the result of your decision.

Perhaps more importantly, have a system in place to learn from past decisions.
Lots of companies that freak about new decisions, do very little to learn from
past decisions.

How many decisions does your org have in progress (DIP)?

But assuming reasonable decisions, we want rapid throughput of small,
safe-to-fail decisions, coupled with focus and attention on the high-level
strategic decisions that will set the course for the org for months/years.
