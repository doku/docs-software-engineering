=============
Venture Deals
=============

Intro
=====

Talk to entrepreneurs who have worked with the vc before. The best 
reference checks are ones you can do where the company went through 
hard times.

An institutionalized super angel is called a micro vc.

A collection of investors is called a syndicate. Most syndicates have a lead investor. 
As the entrepreneur you have to make sure that you communicate with each investor in 
the syndicate.

When companies are created, the founders receive **common stock**. However, when VCs invest
in companies they purchase **equity** and usually receive **preferred stock**.

How to Raise Money
==================
Before you start talking to investors, figure out how much money you are going to
raise. That narrows down who you should talk to. 

To determine the amount you'll raise, focus on a length of time you want to fund your
company to get to the next meaningful milestone. Determine your monthly burn rate 
you'll need to get to this milestone and plan in a time cushion.

These estimations are likely wrong (or approximate at best), so just make sure that you 
have enough cash to get to a clear point of demonstrable success. 

If a VC decides not to fund you, insist on feedback at to why. THis is the most important
feedback an entrepreneur can learn. 

Fund-raising Material
---------------------
Executive Summary
  one- to three page description of your idea, product, team and business

Presentation
  Slide Deck you present and the one you send people per email can be different


VCs will look at:
    - the problem your are solving
    - the size of the opportunity
    - the strength of the team
    - the level of competition or competitive advantage you have
    - your plan of attack
    - current status
    - summary financials, use of proceeds and milestones

Finanicial Model
    You can't predict your renvenue with any level of precision, but you should
    be able to manage your expenses exactly to plan. VCs care about:

    - the assumptions underlying the revenue forecast
    - the monthly burn rate of the business

Closing the Deal
----------------
First step is to sign the term sheet and the second step is to sign the definitive 
documents and getting the cash.

Term sheet
==========
The term sheet is critical. Think of it as a blueprint for your future relationship
with your investor. 

There are really only two things that matter in the acutual term sheet 
negotiation: **economics and control**.

Economics refer to the return the investor will ultimately get in an liquidity event, 
usually sale or IPO. Control refers to the mechanism that allow the investors either to
affirmatively exercise control over the business or to veto certain decisions the company 
can make. 

Lawyers
-------
The lawyer is reflecting you, so a bad or inexperienced one can tarnish your reputation.
Your investors will be your partners, so don't ruin your relationship through unnecessary 
hard negotiation. Lawyers in VC investment usually cap their fees in advance of the deal.
The entrepreneur must take the responsibility for the final result.

Mentors
-------
Every entrepreneur should have a sable of experienced mentors.
Sometimes it will make sense to compensate mentors with options as 
long as you have some control over the vesting of the options based
on your satisfaction with the mentor'? performance as an ongoing 
advisor.

Economic Terms
--------------

Common evaluation traps: 

    1. Premoney vs. Postmoney
    2. option pool: VCs try to give companies a big option pool, thereby effectively lowering the actual premoney evaluation

When you are negotiating about the option pool, you should be prepared with an option budget.
List out all the hires you plan to do until the next round of financing and the approximate 
option grant they will take. 

A **warrent** is simular to a stock option; it is a right for an investor to purchase a 
certain number of shares at a predefined price fo a certain number of years. Try not to have 
any warrents because that will cause a lot of accounting headache down the road. It's better 
to negotiate a lower premoney evaluation than to have warrents. 

.. hint::

  The best way to negotiate a higher price is to have multiple VCs interested in investing 
  in your company.

VCs evaluate companies based on:

- Stage of the development
- Competition with other funding sources
- Experience of the entrepreneur and leadership team 
- The VC's natural entry point 
- Numbers
- current economic climate

Liquidation Preference
  an IPO is simply another funding event for the company, not a liquidation of the company. 
  In almost all IPO scenarios the VC's preferred stock is converted to common stock as part of 
  the IPO, eliminating the issue around the liquidity event in the first place. 

  - stacked prefereces

  1x Liquidation preference means that the investor gets

  - non participating: Money splitted equally based on stock ownership percentage
  - Fully participating: Stock will receive its participation amount before common stock 
    (Investors get their investment back) and the remaining about is shared on as converted basis
  - Capped participation: if the return is more then the invested money times the cap it behaves
    like non participating, if the return does not exceed the cap it behaves like Fully
    participating.

Pay-to-Play
  Conversion to common stock if you don't continue investing is reasonable.

Vesting
  Industry standard vesting for early stage companies is a one-year cliff and monthly
  vesting thereafter for a total of 4 years.
  Single-trigger acceleration and Double-trigger acceleration manage the vesting after a 
  aquisition.

Antidilution

Control Terms
=============

Board of Directors
------------------