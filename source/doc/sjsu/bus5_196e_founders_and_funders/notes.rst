===
VC
===

- dont really sign ndas
- everything you talk about in your pitch, asume that it will be disclosed
- look at vcs portfolio and check if they have direct competitors
- residual clause in NDA

- How do you objectively evaluate opportunity? 

- you want a founding team of complementing skill sets.

Leave Employer
==============

- get letter from employer indicating that you own all the IP 

Playing with Fire 
=================

Starting with family:

- define clear boundaries 

Founding team
=============

Hacker, Hustler, Designer


Equity Split
============

When to split
-------------

- depends on team relationships

early:

- Decide up front - founders know what the own
- If roles defined, then split
- Vesting and Cliff for protection
- attracts founders 

later:

- early lots of unknowns
- commitment known
- contributions to date known
- early can distract

Invest: $5M @ $15M pre-money = $20M post money
With an 20% option pool pre money basis

Investor: 25%
Founders: 55%
Employee pool: 20%


Accelerators
============

- Cohort Based Program to help entrepreneurs
- helps with growth, product, financing
- Increased exposure
- equity - 6-7% Equity for ca. 20k ($300k valuation)


Venture Funds
=============

J - Curve

Bridge Financing: Is a bridge or pier

Angel
-----
- 1 person
- **Own $**
- smaller $
- 

VC
---
- group or firm
- fund - LP's (personal fund, high-network-individuials
- **Other Peoples Money**
- ROI 
- later stage
- larger Ownership % target



.. figure:: vc_structure.png

   