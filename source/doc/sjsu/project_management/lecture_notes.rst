=====================
SE Process Management
=====================

Project Management
==================

.. glossary::

  project
    A complex, no routine, one-time effort limited by time, budget, resources,
    and performance specifications designed to meet customer needs.

Team
----
- Ownership
- Capabilities of Team (soft and hard skills): People are the problem (the
  environment and team influences the productivity), you have to understand your team members
- From looking at our team member list you wan see:

    - Personalities and technical capabilities
    - project priority (is this class A project to the enterprise)

Resposibilities of Project Managers
-----------------------------------

- Scope Management
- Time Management
- Resource Management
- Communications Management
- Risk Management
- Quality Management (act of overseeing all tasks and activities at sufficient quality)
- Cost Management
- Procurement Management (Method by which you aquire something from external entities)
- Project Integration


Resource Management
-------------------
to ensure that the resources required for a project are optained and accounted
for. Each resources' purpose has to be detailed.

You have to fully understand what resources you have and what they account for.

(tools, labor, material, equipment, etc.)

Communitcations Management
--------------------------

- communitation between all stakeholders (written, verbal, non-verbal)
- systematic planning, implementation, monitoring and revision of the communication between
  all stake holders

Risk Management
---------------


Tracking
--------

- make Tracking apparent to all team members (open up everything)


Agile
=====

Minimal Marketable Features: If we don't implement those features, we cannot market
the product, we don't have a product yet.

- Performance Measurement: Value, Quality, Constraint

Agile Manifesto
---------------

- Individuals and interactions over processes and tools (Teams over Tasks)
- Working software over comprehensive documentation (Value over Constraints)
- Customer Collaboration over Contract Negotiation
- Responding to Change over following a plan (Adapting over Conforming)


12 Agile Principles
-------------------

Agile Interdependence
---------------------

User Story
----------
- emphasis on value rather than on definition
- Go beyond the pure requirement and try to create the most value
- Invest: Independent, Negotiable, Valuable, Estimable, Small, Testable
- Story points: focus on how hard things are
- Estimation: get common understanding in the team
- Spike: gain enough knowledge to do proper estimation


Scrum
-----

- Look Ahead Meeting

Scrum Master
  A servant leadership role that is responsible for enforcing agile values and practices at the
  Scrum Team level, and who ensures that the Scrum Team is fully functional, productive and
  focused on the goal.
