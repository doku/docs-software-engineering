==========================
The startup Owner's Manual
==========================

A startup is a temporary organization in search of a scalable,
repeatable, profitable business model.

Startups are all about **unknowns**. Winners recognize their startup
is a series of untested hypotheses. On day one, a startup is a faith-based
initiative built on guesses. Failure is an integral part of the search for 
a business model. 

Customer Development is the process to organize the search for the
business model. Products developed by founders who get out in front
of customers early and often, win.

.. hint::
    There are no facts inside your building, so get the heck outside.

Low-fidelity MVPs test whether you've accurately identified a problem that customers
care about. 

While high-fidelity MVPs will later test whether the product is on the right path
to solving that problem.

Chapter 1: A Startup is not a small version of a big company
============================================================

Deadly Sins of Product Development
----------------------------------

1. Asuming "I know what the customer wants" 
2. The "I know what Features to build" Flaw
3. Focus on Launch Date
4. Emphasis on execution instead of hypotheses, testing, learning and iteration
5. Traditional Business Plans presume no trail and errors
6. Confusing traditional job titles with what a startup needs to accomplish
7. Sales and Marketing execute to a Plans
8. Presumption of success leads to premature scaling
9. Management by crisis leads to a Death Spiral

Chapter 2: The Customer Development Model 
=========================================

1. Customer Discovery
2. Customer Validation (if Validation fails pivot and go back to customer Discovery)
3. Customer Creation
4. Company building

Customer Discovery
==================

You need to estimate rough market size, to align everyones expectations and
objectives.

.. figure:: target_market.png

A bottom-up estimate is usually more realistic for startups. 

In an multi sided market you need to estimate all sides of the market, particuarly the 
side that will pay you. 

Chapter 4: Get out of the building
==================================

Chapter 6: Verify the Business Model
====================================
