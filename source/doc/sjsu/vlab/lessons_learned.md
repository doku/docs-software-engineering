# Though Things First

## Be a Visionary

The vision of an entrepreneur gives his actions meaning and purpose, allowing
to act with self discipline and drive. An entrepreneur is able to envision a 
better future and has the confidence that he soon will be able to life in this better
world after his startup has been successful.

The ability to inspire co-founders, investors, employees and customers with his
vision is what sets a successful entrepreneur apart from the rest. 

A clear vision will give the entire organization guidance and allows everyone
to focus on the long term goal of making the sure the vision comes true rather 
than on short term performance proxies and vanity metrics. Only if everyone in 
the team is alligned with the founders vision the company can perform efficiently.

This inspiration and allignment towards a common vision is an essential task for 
a founder and a founder has to be continuesly focus on that, as his company grows,
the vision adapts or the team changes.

## Endure temporary discomfort to satisfy your desire for long term success.

A entrepreneur realizes that there are many uncomfortable things that have
to be done to achieve the vision. 

It is important to focus on the long term goals of your startup. Often it is 
worth it to sacrifice short term success in order to meet your long term goals and
reach your vision. But the entire team has to be aware of the progress towards the
long term vision in order to keep team moral high.

Usually the uncomfortable tasks an a startup also have the highest probability of 
failure. By tackeling these tasks first you make sure that you fail early rather
than late so that you are still able to compensate and prevent the entire startup
from failing.

## Make Company Culture Part of Muscle Memory

Great company culture is essential for a companies success. 
A companies culture is reflected by the coworker to coworker, employee to customer
and management to employee relationships. To ensure that even under high pressure
everyone makes the right decisions, the company culture has to be part of
everyones muscle memory, so that they will do subconciesly the right thing.
To achieve that, the companies values and principles have to be reguarly 
repeated to everyone in the team.


# Startup Owner's Manual

## Startups are a collection of untested hypotheses

The authors define a startup as "a temporary organization in search of a scalable, 
repeatable, profitable business model".
Startups operate in an environment of extreme uncertainty. Everything has to be considered
unknown. To gain knowledge about your market and customer you have to define a hypothesis, 
build a MVP designed to test this hypothesis and learn from the results you gathered. 

Hypotheses that would cause the entire business plan to fail if proven wrong should be validated first.
Eric Ries calls those hypotheses "leap of faith assumptions". 
By proving or falsifying a hypothesis you learn about the environment in which your startup operates in
and you are able to adjust the business plan accordingly.

Great founders recognize that they operate in uncertainty and know that their startup is a 
series of untested hypothesis.

## Customer Development Process

Similar to the Build-Measure-Learn loop in Lean Startup, the Customer Development
process focuses on an highly iterative approach for designing and building products.
The goal is to learn as much as possible during each iteration and iterate as fast as 
possible. 
One of the possible (and very likely) learning outcomes is that you have been wrong in
one of your fundamental assumptions. In that case you have to pivot and validate new
hypotheses.

## Talk to your customers

All products and services have the goal to create value for a customer. To create value 
you have to solve one of the customers problems.

Never assume that you know what problems your customers have. You don't know what they
want and what features of you product are the most important for them. So you have 
to ask them.

"There are no facts inside your building, so get the heck outside."

It is often a lot easier to stay by your self and work with an "If I build it they will 
come" approach. A startup mentality like this makes success extremely unlikely.
Instead you should talk to your potential customers and endure the temporary
discomfort of having some of your hypotheses proven wrong. That is the only way to
detect, pivot before it's to late and prevent your startup from failure.

