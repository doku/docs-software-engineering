===================
Though things first
===================

The essence of the entrepreneurial spirit: If it looks like an impossible task
with determination, with practice you can do the impossible. You simply must find
the discipline to do so. 

- Focus
- Short time frames
- Frugality
- Being the best 

Discipline is doing what you don't like and doing it well - having the determination,
no matter how difficult the task is, to do it correctly.

Endure temporary discomfort to satisfy your desire for long term success.

True entrepreneurs have a vision and drive themselves and their organization relentlessly toward
that vision. 

Whether it is a policy, procedure, guiding principle, or corporate motto, the more it is 
repeated, the more the corporate body responds effortlessly.
Once you achieve that mental discipline, you earn the muscle memory that allows you or
your company to execute that principle at will and then move on to mastering the next
pillar. 

Goals help make principles meaningful to everyone in the organization, and this is
part of the process of instilling discipline in the corporate mind.

When entrepreneurs make that vision plain to everyone they hire, everyone works in a
fashion that drives towards that goal. By codifying principles that guide action. the
entrepreneur guides the action of all the employees without the burden of directly 
micromanaging them.

Surrounding yourself with bright people is wise but can lead to analysis paralysis or, 
worse yet, conventional thinking.

entrepreneurial principles:
    - An entrepreneur has some vision for his life and business that is not focused on financial gain
    - An entrepreneur always thinks outside the box. 
    - An entrepreneur is highly creative. 
    - An entrepreneur always believes he will succeed.
      Your doubt causes you to freeze, and this is what keeps most people from 
      achieving what they desire. Doubt restricts your ability to move. 
    - Entrepreneurs never give up.
    - Entrepreneurs attract success.
    - Entrepreneurs have laser focus
    - An entrepreneur must maintain control of the business. If you cannot maintain 
      control of your company, you cannot succeed in reaching the goal, the vision 
      you have in your mind. Venture Capital can be the antithesis of entrepreneurialism
      and success

Focus is the most important yer trickiest element in entrepreneurial eyesight.

Out of all the things an entrepreneur should pay attention to, cash is king. If 
you run out of cash, you run out of business. You have to be precise about how you
manage cash.

An entrepreneur who is to focused can also ignore warning signs of internal problems and external
market shifts that will destroy the  company. Select the key indicators of your company and industry
and limit your attention to them while remaining focused on the long-term goal. You must have a 
long-term view but  review it on a short-term basis.

Building an enduring business requires having an enduring view of that business. The short-term
mindset is inherently broken; it is a planned obsolescence of the organization and the people
in it. 

Entrepreneurs see relationships, markets, opportunities, and life situations through a 
different lens than the rest of the world uses. They see enduring meaning, not 
temporary convenience.

Intuition is related to wisdom. Wisdom is the proper application of knowledge. We all gain
knowledge, but if you lack wisdom, it means you will fail to see how to apply that knowledge
for the improvement of anything. 

