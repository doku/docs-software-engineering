============
Introduction
============

**TOGAF** (The Open Group Architectural Framework)

OS
===

- Unix: Ken Thompson, Dennis Ritchie at Bell Labs in 1970s
- execution is user mode (with fewest privileges) and privileged mode
- Interrupts are requests from an external device for a response from the processor

- KVM: Keyboard Video Mouse


Hyperconverged Systems
----------------------

- HCIS: Hyperconverged Integrated System
- Tightly coupled hardware that dispenses with the need for a regular storage area network (SAN)
- distribute locl storage in network nodes using SEPH

Linux
-----
- CentOS is the open-source variant of RHEL
- BSD is a Unix OS developed at UC Berkeley
- OSX and iOS are based on BSD

- GNU: "GNU's Not Unix" by Richard Stallman
- LDAP: Lightweight Directory Access Protocoll

Windows AD
----------

- Active Directory is a collection of services (Server Roles and Features) used to manage
  identity and access for and to resources on a network
- Samba is a open source AD

.. figure:: active_dircetory_services.png

- Federation services is a software component that facilitates cross-organizational access of Systems
  and applications. AD FS implements the standards based WS-Federation protocol and
  **Security Assertion Markup Language (SAML)**
- SAML is an xml based standard for exchanging authentication and authorization between security domains
- AD Rights management service is an infotmanion protechtion technology that works with applications to
  safeguard digital information
- AD LDS is a  Lightweight directory Access Protocol (LDAP)

- ACL: Access control list

- login is authentication, authorization is the process of making sure that a user has access to a resources


Web Service
-----------
- Web Service Choreography: decisions are made between individual services, no single service in control.
- Web Service Orchestration: One Service typically in control of others.


File Systems
------------

- Super block: segment of metadata that describes a file System (generated on filesystem creation)
- inode (index node) all information about file (except name) and pointers to data
- dentry (directory entry) points a filename to its corresponding Inode
- Virtual File System VFS

- Ext4: "extended file system" standard linux filesystem for long time, journaling file system, it can recover from failure
- NTFS: "New Technology File System" default windows filesystem
- ZFS: most advanced local file System
- NFS: Network File System

- SAN: Storage Area Network
- FC: Fibre Channel
- SCSI: Small Computer System Interface
- iSCSI: SCSI over IP
- FCIP: FC over IP

Ansible
-------
- IT automation engine
- agentless, uses SSH
- declarative YAML playbooks

- Ping: ``ansible -m ping all``
- Command: ``ansible -m command -a 'df -h' Servers``
- get full config: ``ansible -m setup server``
- run playbook: ``sudo ansible-playbook -i hosts.conf hello-world-webserver.yml --tags "undeploy"``

Networking
==========

.. figure:: networking_layers.png
	 :alt: networking_layers

- TCP (Transmission Control Protocol)
- UDP (User Datagram Protocol) for low latency, loss-tolerant connections

- Bridging is a L2 decision, Routing is a L3 decision

- unicast, broadcast, multicast
- Ethernet Trunking to carry multiple Vlans on a single physical link
- Switched Virtual Interface (SVI)
- Spanning Tree Protocol (STP): builds loop free topology of networks

Core Features
-------------

- P: Provider Core Router
- PE: Provider Edge Router
- CE: Customer Edge Router
- CPE: Customer Provided Equipment

Internet
--------

- RFC 1918: Private Network Address Space
- Packet jitter: variability in latency spacing of packets
- Border Gateway Protocol (BGP)
- DNS
- FQDN=Fully qualified domain name


Security
--------

.. figure:: layerd_security.png
	 :alt: layerd_security

4. Client Server
================

Rest
----
- Representational state transfer
- DELETE
- PUT
- GET
- POST

- ajax: retrieve data using xmlhttprequest object


Integration
-----------


Application Platforms
---------------------


5. Enterprise software
======================

- ERP
- SCM (Supply Chain management)
- CRM
- HRP (Human Resource Planner)
- BIS (Business Inteligence Software)
- Identity management

- Role-based Access control (RBAC)
- Public Key Infrastructure (PKI)


6. Modelling
============

BPMN
----

- A Pool contains at most 1 process.
- Lanes usually represent organizational Roles
- A Task is an atomic activity that is included in a process

7. Development Models
=====================

- Systems Development life cycle is composed of a number of clearly defined distinct work phases
- Devops
- Scrum
- Continuous Delivery


8. Concurrency
==============

Parallel is Always concurrent. 

Concurrency is about dealing with lots of things at once, Parallelism is about 
doing lots of things at once.

Problems:
    - Lost Update
    - Inconsistent Read
    - Correctness vs. Liveness (Freedom from deadlock and starvation.)


8. Databases
============

- Dr. E. F. Codd is father of relational data model (1970)
- Relational Data Model is defined by 12 Codd Rules


- MariaDB is open source version of MySQL

- ACID: Atomicity, Consistency, Isolation, Durability
- Cap Theorem: Consistency, Availability, Partition Tolerance

- Time series Databases
- NOSQL: Not Only SQL 

- Polyglot Persistence: when storing data, it's best to use multiple data storage 
  technologys 

9. ESB, Messaging
=================

10. Enterprise Microservices
============================

- Microservices shouldn't share Databases
- Orchestration vs Choreography
    - Service orchestration represents a single centralized executable business process
        (the orchestrator) that coordinates the interaction among different services.
        The orchestrator is responsible for invoking and combining the services.
    - Service choreography is a global description of the participating services,
        which is defined by exchange of messages, rules of interaction and agreements
        between two or more endpoints. Choreography employs a decentralized approach
        for service composition.

11. Business Inteligence
========================

- CRISP-DM: Cross Industry Standard Process for Data Mining

12 Security
===========

- PKI
- CIA: Confidentiality, Integrity, Availability

- Non-Repudiation: The inability to deny having taken an action.
- Authenticity: Assurance that a message or other exchange of information is from the source it claims to be.
- Threat: A potential danger that could cause harm to information or a system
- Threat Agent: An entity that exploits a Threat
- Exploit: A practical method to take advantage of a specific vulnerability
- Attack: the use of an exploit against an actual vulnerability
- Attack Vector: A theoretical application of an exploit
- Zero-Day Attack: an attack that exploits a previously unknown vulnerability for which there is not yet a defense

- Common Attacks: SQL Injection, Buffer Overflow, Cross-site Scripting, Phishing, DDOS

- Input Validation: Sanitization, Canonicalization, Filtration

- OWASP: Open Web Application Security Project
- CWE: Common Weakness Enumeration



13 standards
============

- Both ISO 9001 and ISO 14001 are process standards and don't focus on product or service.
- ISO 9001 is the standard that gives the requirements for a quality management system.
- ISO 14001 helps organizations to implement environmental management.

- Certification: an independent, external body audits an organization's management system
- ISO does not carry out Certification
- Accreditation is the Certification of a Certification body



- TOGAF: The Open Group Architecture Framework
