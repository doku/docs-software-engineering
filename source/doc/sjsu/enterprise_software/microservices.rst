=============
Microservices
=============

Evolutionary Architecture
=========================

A software architect is like a city planner.

Frank Buschmann: architects have a duty to ensure that
the system is habitable for developers

An architect should be worried about what happens between the boxes, and be
liberal in what happens inside.

Architects should feel the impact of their decisions and therefor should 
code together with the team (in pair programming).

