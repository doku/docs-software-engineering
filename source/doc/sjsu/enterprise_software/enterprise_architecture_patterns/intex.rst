================================
Enterprise Architecture Patterns
================================

This section is a summary of Martin Fowler's "Patterns of Enterprise Application Architecture".

.. toctree::
    :glob:

    introduction/*
    layering/*
    domainlogic/*
