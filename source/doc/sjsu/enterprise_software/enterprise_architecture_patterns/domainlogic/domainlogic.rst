============
Domain Logic
============

Transaction Script vs. Domain Model vs Table Module

You add a Service Layer over the Domain Model to expose an API
for the application. Transaction control and security can also be 
placed in the Service Layer.

