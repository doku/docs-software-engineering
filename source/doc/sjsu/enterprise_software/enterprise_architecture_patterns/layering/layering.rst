========
Layering
========

A multi ``tier`` architecture implies a physical separation
for example in a client-server context. In a ``layered`` architecture
this physical separation is not required.

:ref:`layer-architectural-pattern`