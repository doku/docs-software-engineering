============
Introduction
===========

Enterprise Applications
=======================

- Data Persistence
- Data Access Concurrency
- Data Polymorphism (UI)
- Reliability, Availability, Scalability (RAS)
- Integration, instrumentation, Manageability
- Authorization (what are you allowed to do), Authentication (who are you), Accounting (What did you do, Log) (AAA)
