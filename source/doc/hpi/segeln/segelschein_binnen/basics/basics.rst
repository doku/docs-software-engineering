Basics
======


Begriffe
--------

  - Backbord(links) / Steuerbord(rechts)
  - Luv(dem Wind zugewandte Seite) / Lee (Wind abgewandte Seite) -> Lee ist da wo der Baum ist
  - abfallen(Boot in richtung Lee fahren) / anluven (Boot in Richtung Luv fahren)

Fahren
------

Steuermann sitzt immer in Luv, Vorschoter sitzt da wo er gebraucht wird.

Boot
----

Wind teilt sich in drei Komponenten ein: Abdrift, Schräglage und Speed
Je schneller man ist desto weniger Kraft beleibt für Probleme
Immer versuchen Speed im Segel zu halten.


Segellatten: Das Segel ist nicht dreieckig sondern das Achterlick ist gebogen. Damit sind latten

Wanten und Vorstarg befestigen den Mast. Wanten werden mit einem Flügel(Saaling) nach hinten verschoben wodurch der Mast auch nicht mehr nach vorne fallen.

Gaffel: oberer Baum, daher hat man kein dreieckiges Segel, sondern ein Trapetzartiges. Wurde orientiert an von Vogelflügeln und ist heute outdated. Sie waren früher im Raumwindkurs schneller, da sie mehr Segelfläche hatten

Spinnacker: große Segelfläche

Schwert: staut Wasser, wenn druck auf das

Kränckung: Neigung des Bootes

Trapez:

Reffen: Nimmt der Wind zu wird die Segelfäche verkleinert

wave pircer

abdrift: Boot wird immer ein wenig nach Lee geschoben


Dagaboard: hat sich technisch nicht wirklich bewkann trockenfallen, klassische Jacht mit Kiel kann das nicht

Zwei konzepte Jole und jacht

Jolle kann kentern Jacht nicht, Jolle ist unsinkbar. Jacht ist ein Kielboot und hat unten so viel Gewicht, dass sie wieder aufgerichtet wird.

Fallen zum setzen, einholen des Segels

Schoten zum bedienen des Segels

Baum Niederholer: Zieht den Baum runter und biegt dabei den Mast.

Dichtholen: Leine Ziehen
Fieren: Leine geben

Regatta
-------

ist immer Sicherugspflichtig, d.h. der DLRG hilft bei Problemen



Kurse zum Wind
--------------

Bei einer Böhe kann man weiter anlufen, da sich der relative Wind geändert hat.
Dann beschleunigt dass Boot und der Wind wird schwächer. Damit sinkt der Winkel zum Wind un man muss wider abfallen.
Dadurch gewinnt man etwas höhe.

Imwindkurs
##########

zum anhalten und Segel setzen
Segel flattern und man treibt langsam nach Lee

Amwindkurs
##########

wie hoch man an den Wind segeln kann hängt vom Bootstyp und der Besegelung ab

Halbwindkurs
############

Achtung: nicht bei 90° zum Wind, durch den Fahrtwind muss man noch weiter abfallen

Raumwindkurs
############


Vorwindkurs
###########

Fahrtwind und atmospherischer Wind schwächen sich gegenseitig ab
