Gesetze auf Seien
=================

Ziel: Sicherheit auf see Schaffen

Internationale Abkommen
-----------------------

COLREG

KVR(Kolisions verhütungs Regeln)

 - Allgemeine Regeln
 - Schiffsführer
 - Wegerecht
 - Beleuchtung
 - Verkehrstrennungs

 COLREG
 solas
 MARPOL
 Helsinki

rechtliches Umfeld
------------------

- Schiffssicherheitsgesetz (SchSG)
- VO über die Sicherheit der Seefahrt (SchSV)
- Seesicherheitsuntersuchungsgesetz

§ 6 Abs. 3 SchSG
§13 Abs 1-3 SchSV

.. note::
  Das Führen eines Logbuches ist Gesetzlich vorgeschrieben!


Regatta
-------

keine Flaggen müssen geführt werden
WS Regeln


Grundsätze der Schiffsführung
-----------------------------

- Skipper wählen
- Deutliche Kurse fahren
- Manöver rechtzeitig und deutlich
- Kurshalten bei Wegerecht
 
  - im Zweifelsfall : langer Ton (Achtung!), Folge kurzer Töne (Gefahr eines Zusammenstoßes)
  - zur Not: Manöver des letzten Augenblickes

Beleuchtung
-----------

Soll nicht den Weg beleichten, sondern das

Beleuchtung zeigt...

- Lage des Bootes(Steuerbord grün, backboard rot, achtern weiß (135°))
- seinen Kurs
- Art des Antriebs
- Art des Einsatzes

.. note::
   Nutze eine rote Stirnlampe

.. note::
  Arbeitsboot hat rot weiß rot

Fischer

Fischer mit schleppnetz haben Wegerecht
