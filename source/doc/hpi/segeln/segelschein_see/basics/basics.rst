Basics
======


Navigation
----------

Meridian- linie, die die beiden Pole verbindet
man misst den Winkel gegen Norden (Meridian)
die Winkel werden dem Steuermann übergeben, der den Kompass entsprechend einstellt

Zur Navigation ist die Position sowie Richtung und Geschwindigkeit nötig.
Geschwindigkeit ist Weg \ Zeit. Um den Weg zu bestimmen benötigen wir Distanzen.
Nie auf Deck navigieren.
Karten müssen immer aktuell gehalten werden, da karten berichtigt werden

Koordinaten
###########

90° vom Äquator zu den beiden Polen
180° von gennich nach Ost und west, mit Datumsgrenze
1° = 60´
1´ = 60´´

Definition Distanz von einer 1´ (Minute) ist eine Seemeile.
Knoten entsprechen Seemeilen pro Stunde

Heute denken wir in

54°33,5´

Karten
######

Name, Maßstab, Mittelbreite
Alle Höhen und Tiefenangaben:
Land in Meter bezogen auf Normalnull
Wassertiefe nach LAT(lowest astronomical tide ->niedrigste Wassertiefe ohne Sturm  ect. die zu erwarten ist) tendentiell hat man immer mehr Wasser als angegeben
Wattentiefe auch nach lat vermessen. Idee Wasser, das die Karte Angibt ist immer da

3 Zohnen
-Land
-trockenfallendes Land, Übergang zwischen LAT und NN, haben strich drunter -> steht für negativ bezüglich LAT also
-see

Projektionsarten: aus Kugel was zwei dimensionales  machen
hat sehr lange mit zyliderprojektion gearbeitet -> streckt sachen an den polen

Merkator-Projektion: flächenähnliche Karten
Projektion von Blickpunkt auf der die Kugel.
Ändert abstände der Breitengrade und damit sind Objekte an den Polen größer

Achtung bachte: durch Gradverzerrung der Merkatorprojektion entsprechen ristanzen von 5 Grad bei den Polen mehr als 5 Grad am Äquators

Gibt karten mit Windstatistik zum Planen von turns

Auf Magenta Flächen darf man nicht drauf fahren

GPS
###

Bei problemen Karten Modell (WGS 84) überprüfen


Kompass
########

Deviation/Ablenkung
___________________
Kompas wird abgelenkt von Schiffsmagnetismus (Schiffe haben eines Magnetfeld) d.h. das Schiff lenkt eigenen Kompass ab. Selbst Plastikschiffe sind betroffen.
Deviationsdalben und Deviationstabellen helfen die Ablenkung herauszurechnen.
Ist abhängig von der Lage des Schiffes i Magnetfeld der Erde, also abhängig vom Kurs.
Zum Teil werden kleine Magnete in Kompass geklebt um der Ablenkung gegen zu wirken.

Handys sind auch Quellen von Deviation. Auch Stromlinen zu Lichtern sind problematisch

Fehler können herausgerechnet werden

Ablenkung
Missweisung : Magnetischer Nordpol liegt wo anders als der geografische Nordpol
Fehler kann von fast 25° West bis Ost gehen

Magnetkompasskurs (Mgk) 130°
Ablenkung (Abl)         +08°
Mißweisender Kurs ()    138°
Missweisung 	          -12°
Rechtweisender Kurs     126°

Positionsbestimmung
###################

Bestimmung von Längen und Breitenkreisen

Breitengrad:
____________
gibt Winkel an, den man zum nächsten Breitengrad hat
bsp. 0° wenn man entlang des Äquators schaut, 90° wenn man in Richtung Nordpol schaut

Bestimmen über Sextanten Nordstern
auf der Sübhalbkugel nutzt man eine Sternen Gruppe

Längengrad:
___________

Sonnenuntergang, sobald die Sonne das Wasserberührt (da durch Atmosphäre Licht gebrochen wird)

Anderer Ansatz: Irgendwann erreicht die Sonne ihren höchsten Punkt. Die Uhrzeit bestimmt die Ortszeit des Längengrades. Den Zeitpunkt zu dem Die Sonne am höchsten ist definieeen wir als 12Uhr Ortszeit
Mann misst den höhenwinkel zur Sonne und entscheidet später, wann der höchste Position war. Also kann man im Nachhinein sagen, wann man die höchste Höhe erreicht hatte. Darüber hinaus benötigt man eine Uhr, die als Referenz gelten kann.

Man misst vor Start die Uhrzeit wann die sonne beim Start am höchsten ist und stellt die Uhren entsprechend ein

360°/24 = 15
Bin ich eine Stunde von meinem Ausgangspunkt weg, bin ich 15° von meinem Ausgangspunkt weg
Wenn weine Referenz Uhr 1 Uhr Zeigt bin ich 15° Westlich von meinem Startpunkt.

Alle 15° gibt es eine neue Zeitzone
Alles ist immer nur relativ. Daher wird der London Meridian als Nullmeridian angegeben. Zählt von London 180°nach links und rechts und erhält so die Datumsgrenze

Zeitarten

Ortszeit
Zeitzonen
Gesetzliche Zeit(Sommerzeit)

Aufgaben

1.)
Wie spät ist es auf einer Yacht nach UTC, die auf 19°W steht, wenn die Borduhr die die Zeitzone anzeigt auf 19:40Uhr steht

Antwort 20:40
ist 19° Westlich, also
0 Meridian ligt in der Mitte seiner Zohne, davon gehen 7,5° ab. Damit ist das Boot eine Zone anderst
2.) Welche Uhrzeit zeigt die Uhr nach MESZ um 7:40UTC an?
S in MESZ steht für Sommerzeit.
UTC+ 1hMEZ +1h = MESZ



Kontakt
-------
erhard
info@winddieb.com


Weiteres
--------

dsv 
