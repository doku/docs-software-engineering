===========
Segeln
===========

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Contents:

   basics/*
   segelschein_binnen/*
   segelschein_see/*
   sonstiges/*
   wetter/*
