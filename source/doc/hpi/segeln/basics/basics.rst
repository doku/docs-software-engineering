Basics
======

Sportbootführerschein
---------------------

Der Sportbootführerschein teilt sich in Binnen und See. Der Binnenschein teilt sich in Segel und Motor.
Für See muss ab einem Motor mit 15 PS eine Prüfung abgelegt werden auch muss terrestrische Navigation(Navigation ohne elektronische Hilfe). Es wird bei See keine praktische Segelprüfung gemacht.
Es reicht aus für die Praxis den Motorschein See und den Segelschein Binnen zu machen. Theorieprüfungen müssen beide abgelegt werden.
Unterschiedliche Regelungen in Bundesländern, sehr streng in Bayern und frei in Brandenburg.
Bei Binnen steht das Gerät im Fokus, bei See geht es um terrestrische Navigation.

Sportbootführerschein See
-------------------------

Navigation beantwortet :Wo bin ich(noch auf meinem Kurs), Wie komme ich da hin wo ich will? 

Material
########
Karte
Karte int 1
Navigationszirkel,
Navigationslineal, Klardreieck
Bleistift

Bücher

Sportbootfüherschein See
Sportküstenfüherschein

Prüfung
#######

prüfungsauschuss-berlin.de
Kosten rund 100€



Sportbootführerschein Binnen
----------------------------

Der Führerschein gilt für Boote bis 15 Tonnen und 20 m.
Der Schiffsführer muss das Fahrzeug nicht selber steuern, steuern darf jeder der älter als 16 Jahre ist.


Prüfungsvoraussetzungen
#######################


  - Mindestalter
    - 14 Jahre Surfer und Segler
    - 16 Jahre für Motorbootfahrer
  - Eignungsprüfung -> ärztliches Attest (keine Kassenleistung)
    - Tauglichkeit
    - Zuverlässigkeit -> nachzuweisen durch Führerschein/oder Führungszeugnis Klasse O (Kopie des Führerscheins bei Prüfung bei legen)

Prüfung
.......

Theoretische Prüfung
  - Technik
  - Handhabung
  - Sicherheit
  - Gesetzeskunde
  - Wetter

Praktische Prüfung
  - Knoten
  - Fahren

Gebühren
  - Motor 73€
  - Segeln (Motor) 76€

Antrag
......
pruefungsausschuss-berlin.org

Sportküstenschifferschein
-------------------------

Versicherung
------------

Verleih will sich klar werden ob man in der Lage ist das Gerät zu steuern.
Daher sind Scheine Indikatoren für Wissen. Da sobald ein Schaden entsteht, in der Schiffart ein riesiger Schaden entsteht.

Funk
----

Sobald das gerät im Boot ist muss man einen Schein haben
