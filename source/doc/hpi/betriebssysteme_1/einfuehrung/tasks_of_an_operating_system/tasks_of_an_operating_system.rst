Tasks of an Operating system
============================

The main task of an operating system is the management and distribution of the different resources.

- Processor management - Scheduling

  - Fairness (not always desired: ABS in a car should have priority)
  - Non-blocking behavior
  - Priorities

- Memory management

  - Virtual versus physical memory, memory hierarchy(register, cache, ram, hdd)
  - Protection of competing/concurrent programs

- Storage management – File system

  - Access to external storage media

- Device management

  - Hiding of hardware dependencies
  - Management of concurrent accesses (HDD or printer -> different implementations necessary)

- Batch processing

  - Definition of an execution order; throughput maximization


Kernel- and User Mode Programs
------------------------------

Typical functionality implemented in either mode:
Kernel Mode – Kernel Space:

- Privileged mode
- Strict assumptions about reliability/security of code
- Memory resident

  - CPU-, memory-, Input/Output managment
  - Multiprocessor management, diagnosis, test
  - Parts of file system and of the networking interface

User Mode – User Space:
- More flexible
- Simpler maintenance and debugging

  - Compiler, assembler, interpreter, linker/loader
  - File system management, telecommunication, network management
  - Editors, spreadsheets, user applications

Different memory spaces available in modes: Kernel Space, User Space
Switch between the modes: Mode is determined by flag, which can be deleted from Kernel mode but not set from User mode, needs to call Interrupt to switch to Kernel mode

Layered Model of Operating System Concepts
------------------------------------------


.. csv-table::
  :header: "nr", "name", "typical objects", "typical operations"
  
  "1.", "Integrated circuits register", "gate, bus", "Nand, Nor, Exor"
  "2.", "Machine language instruction counter", "ALU Add", "Move, Load, Store"
  "3.", "Subroutine linkage", "procedure block", "Stack Call, JSR, RTS"
  "4.", "Interrupts interrupt handlers", "Bus", "error, Reset"
  "5.", "Simple processes", "process", "semaphore wait, ready, execute"
  "6.", "Local memory", "data block", "I/O channel read, write, open, close"
  "7.", "Virtual memory model", "page", "frame read, write, swap"
  "8.", "Process communication channel (pipe)", "message", "read, write, open"
  "9.", "File management", "files", "read, write, open, copy"
  "10.", "Device management",  "ext.memory", "terminals read, write"
  "11.", "I/O data streams", "data streams", "open, close, read, write"
  "12.", "User processes", "user processes", "login, logout, fork"
  "13.", "Directory management", "internal tables", "create, delete, modify"
  "14.", "Graphical user interface", "window, menu, icon", "OS system calls"



.. figure:: layered_model.png
	 :alt: layered_model

fork-exec: schreibt alten Prozess


Operating systems are mainly written in C

context Wechsel. Wechseln zwischen verschiedenen Programmen in C implementieren - > set jmp / long jmp

OS acts as Extension of Hardware
--------------------------------
OS always to a small part dependent on the hardware, do a large part generic

Windows is developed for multiple platforms, because this helps to detect issues and bugs in the software. To reduce overhead they support two architectures




.. figure:: os_evolution.png
	 :alt: os_evolution

Main Concept
------------

processes
_________

.. figure:: processes.png
	 :alt: processes

files
_____

- Files, directories, root
- Path, working directory
- Protection, rwx bits
- File descriptor, handle
- Special files, I/O devices
- Block I/O (Disks can only read a Block), character I/O
- Standard input/output/error
- pipes

.. figure:: file_system.png
	 :alt: file_system

In windows we have 26 root directories, one for each root-letter (C:)

Block I/O

System calls
_____________

- User programs access operating system services via system calls
- Parameter transmission via trap, register, stack
 count=read(file, buffer, nbytes);
- 5 general classes of system calls:

  - Process control
  - File manipulation
  - Device manipulation
  - Information maintenance
  - communications

Structuring of Operating Systems
--------------------------------

Layered OS
__________

Each layer is given access only to lower-level
interfaces

.. figure:: layered_os.png
	 :alt: layered_os

Principle is violated due to performance gains with the memory management


Commands
--------

od -bc file (octal dump)

strg shift esc öffnet Task manager

procexp - TODO


Prozessoren haben heutzutage viele Rechenwerke, die parallel rechnen können, dies ist praktisch aber auch nur möglich wenn der passende mix an Instruktionen kommt

Windows nutzt den orginal Berkly TCP IP Stack in einer Stream umgebung um. Heutzutage nutzt Windows einen eigenen IP v6 Protokolstack
