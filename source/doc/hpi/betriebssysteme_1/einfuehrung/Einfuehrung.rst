Einführung
==========

ACM/IEEE Operating Systems Body of Knowledge - Core and Elective units

Windows Kern sind 23 MB C code

Priorities : Tasks die lange laufen, erhalten eine niedrigere Priorität. Tasks die lange nicht laufen werden langsam wieder angehoben

Anderes Prinzip: Belohnungssystem für das ausführen von IO -> erhöht Priorität sodass bald erneut IO ausgeführt wird, da IO viel länger dauert
