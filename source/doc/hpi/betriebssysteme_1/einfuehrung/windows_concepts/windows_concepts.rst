Windows Operating System Family - Concepts & Tools
==================================================


processes(tasks) and Threads
----------------------------
Every process starts with one thread, the  first thread executes the start program and then the program’s “main” function. Can create other threads in the same process and also create additional processes.

Why divide an application into multiple threads?


process
#######

A process represents an **instance of a running program**. A Process is defined by an Address space, Resources and security profile (token).

.. figure:: process_and_its_resources.png
	 :alt: process_and_its_resources

Access token is given by the OS and determines your rights and capabilities. Rights are forwarded to the thread, but can also be overwritten



threads
#######
A thread is an execution context within a process, concret a **Unit of scheduling(threads run, processes don't)**

  All threads in a process share the same per-process address space
Services provided so that threads can synchronize access to
shared resources (critical sections, mutexes, events,
semaphores) All threads in the system are scheduled as peers to all others,
without regard to their “parent” process

Job
###

Combines multiple processes.

System calls
############

Virtual Memory
##############

Memory is virtualized and a memory manager maps virtual onto physical memory.

With 32 bits we have an limitation of addressing

Nowadays the 64bit machines form intel actually only have 42bit address busses but calculate with 64 bit addresses,

Memory Protection Model
_______________________

No user process can touch another user process address space
(without first opening a handle to the process, which means passing
through NT security)

Soft page faults: small programs bind huge library's and it would be stupid to load all these in memory multiple times

shared library's are usually stateless so calls from a different process doesn't change the state of on others programs library. shared library's are also splitted in pages with data and code, so the data part is extra for each process which uses the library.

Files
-----

Files are represented as Fileobjects where the file pointer(Lesekopf ist saved).
Because all Threads of one Process have the same address space, multiple threads on one process can theoratically write/read in one file.
The different address space of processes would lead to different file pointers and therefore can not be
Cool Tricks
-----------
Verschicken von Speicherbereichen zwischen zwei Prozessen als Nachrichten:
Betriebssystem flagt gesendeten Speicherbereich mit "copy on write". Fängt der zweite Prozess an den Speicher zu beschreiben, wird Seitenweise 4k an neuen Stellen im Speicher. So kann der Speicher ohne Kopieraufwand geteilt und von anderen Prozessen gelesen werden, erst beim Schreiben wird seitenweise kopiert.


Things to try
-------------

Windows Performance Monitor - Leistungsüberwachung

Nice to knows
-------------

Windows 9 musste ausgelassen werden, da viele Programme nach einer Nummer 9 in der Version suchen um Windows 95 o.ä. zu finden. A

Strg alt entf löst Hardware Interrupt durch die Tastatur aus. Daher kann man sich sicher sein, dass ein Mensch an der Tastatur sitzt und kein Virus.
