Structuring of the Windows Operating System
===========================================

OS Architecture
---------------

.. figure:: simple_os_architecture.png
	 :alt: simple_os_architecture

2 Wege um vom User in den Kernel mode zu kommen: direkt und über subsystem
Originally three environment subsystems: Windows, POSIX, and
OS/2
Subsystem has two parts
Windows subsystem are the API for which you develop windows programs
   Sobald man den Kernel aufruft

.. figure:: key_system_components.png
	 :alt: key_system_components

.. figure:: windows_architecture.png
	 :alt: windows_architecture



Kernel Mode Components
----------------------
Core OS

-Executive

   -base operating system services,
   -memory management, process and thread management,
   -security, I/O, interprocess communication.

kernel

low-level operating system functions,
thread scheduling, interrupt and exception dispatching,
multiprocessor synchronization.
provides a set of routines and basic objects that the rest of the
executive uses to implement higher-level constructs.
Both contained in file Ntoskrnl.exe


 thread scheduling makes context changes

Drivers:
- Device drivers (*.sys)

  - hardware device drivers translate user I/O function calls into specific hardware device I/O requests
  - virtual devices - system volumes and network protocols

- Windowing and Graphics Driver (Win32k.sys)

  -graphical user interface (GUI) functions (USER and GDI)
  -windows, user interface controls, and drawing

- Hardware Abstraction Layer (Hal.dll)

  -isolates the kernel, device drivers, and executive from hardware
  -Hides platform-specific hardware differences (motherboards)

Background System Processes
---------------------------

Usermode services.

- Core system processes,

  - logon process, the session manager, etc.
  - not started by the service control manager

- Service processes

  - Host Windows services
  - i.e.; Task Scheduler and Spooler services
  - Many Windows server applications, such as Microsoft SQL Server and Microsoft Exchange Server, also
		include components that run as services.

Portability
-----------
Hardware-specific code is isolated in low level layers of the OS (such as
Kernel and the HAL) and provides portable interface
Only HAL (ardware abstarctuion layer) has to be rewritten

Multiple OS Personalities
-------------------------
Windows was designed to support multiple “personalities”, called environment subsystems

-Programming interface
-File system syntax
-Process semantics

If a program starts the os checks whether the subsystem already runs, and if necessary starts it. They will run until the system is shut down

Microkernal concept
-------------------

Grafics are in kernal mode

Virtualisieren vs Subsystem erstellen
Bsp. Dos

Programm: file with code
Prozess: menge von ressourcen zur ausführung des Programs
Thread: ausgeführte Teil des Prozesses


kein exception handling in C Windows stellt system drum herum



.. figure:: 32bit_address_space.png
	 :alt: 32bit_address_space

originally used first bit to decide Kernal vs User
->in virtual address space


Hyperthreading

Nebenläufigkeit innerhalb einer CPU, tasks können in der Pipeline besser aufgeteilt werden. Performance Vorteil Faktor 1.1

Numa
jede cpu hat einen eigenen Speicher Block, wenn man auf den Speicherblock einer anderen Cpu zugreifen möchte muss man erst bei anderer CPU anfragen, daher kann schlechte Speicherplatzierung die Performance um Faktor 10 verlangsamen

law gibt immer teil, der sequentiell ist im Program
ist 10/90 sequentiell/parallel, dann kann man das Programm nicht um mehr als Faktor 10 verschnellern

subsystems
----------

You have the lib
process which encapsulates the state
lpc
