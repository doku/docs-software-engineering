Klausur Themen
==============


create Thread
Handle
Prozess
wait for all objects
zur synkronisation:


Kernal Objekte liegen im Nonpaged Pool (auch Gerätetreiber sind im nonpaged pool (Festplattentreiber kann man nicht auf festplatte auslagern))

kritische Abschnitsproblem lösen Kriterien nötig
- fortschritt (kritischer abschnitt endet nach endlicher zeit)
- Gegenseitiger ausschluss (immer nur ein Thread drin)
- bounded waiting

Interrupt
---------
hardware: Interrupt asynchron unabhängig von Software
Software: Trap, Exception sind Instruktionsabhängig (bsp teile durch null)
Event in Software,


Fragen
-------
Erkläre Thread Programm Prozess
Erkläre Fifo
Was ist die Beste seiten Ersetzungsstrategie wenn man genug Speicher hat

Allgemein
---------
Klausur Fragen präzise und kurz beantworten, nicht zu viel erklären

Thread
------
Ein ausführungskontext eines Prozesses

Prozess
-------
Instanz eines laufenden Programms
Ressourcencontainer für Thread

Program
-------
ausführbare Datei auf der Platte

Zustände die ein Thread haben kann
----------------------------------
initialize
ready
standby: Thread der ausgewählt wurde gleich zu laufen
terminated
quantum ende: wieder in ready
waiting: geht nach ready wenn er zu lange wartet
transition: war man zu lange in waiting wir man in transition ausgelagert


Übung 5
-------
Beladungsanomalie

working set: Seiten auf die ein Prozess zugreifen kann ohne einen Pagefault
pagefault: seite gehört uns nicht
soft : schon geladen gehört dem Prozess aber nicht
hard: liegt auf platte

Ist der Speicher voll müssen seiten rausgeschmissen werden
- Lifo
-

Addressraum: physisches limit ansprechbarer Speicherplätze
Virtueller Speicher: virtuelle ram und festplatten Addressen

Paging
Art der Speicherverwaltung, aufteilung in gleichgroße Stücke.

exterene Fragmentierung: genug speicher aber fragmentiert
interene Fragmentierung: verschnitt/ Platz auf den Seiten wird verschwendet

swapping: Auslagern von seite auf Festplatte, ganze Prozesse(große Teile, Seitentabelle und Kernelobjekte können nicht ausgelagert werden)
können ausgelagert werden
Jeder Prozess hat eine eingene Seitentabelle


Page table:
2 Teile einer Addressen

Welche Seite + Welches Byte

Flag-Bits: Dirty, Owner

Mehrstufige Tabelle
genutz für 64Bit Systeme

Pagelist
free: Freie Pages, gibt Prozess der Seiten nullt
zero: schon genullte Daten
standby
modinfied

man bekommt immer nur genullte oder seinen Eigenen Müll/ alte Seiten
ist ein Prozess gerade gestartet bekommt man von der zero Liste eine Page

Werden Seiten gelöscht, da das Working sett limit erreicht ist wird die Seite in modified geschoben

Übung 5 Aufgabe 4
33bit rechner
Wie viele Einträge hat die Seitentabelle: So viele hat der Ram/ Größe der Pages
Rechnung Seite 8 kiB groß, man benötigt also 13 bit um alle bytes anzusprechen
Damit bleiben also 20 Bit zum Ansprechen der Seiten übrig.
Kibibyte nächste Zehner Potenz also 2^10, 2^20...
Seitentabellen sind aber immer gefüllt, sind nicht alle Seiten benötigt sind Seiten verlinkt,
die zu Fehlern beim Zugriff führen

Aufgabe 5

Gant Diagramm kommt auf jeden Fall dran
Aufgabe genau lesen: der Scheduler entscheidet nur genau am Ende eines Quantums
wird ein Thread früher fertig ist es irrelevant

Jede Priorität hat eine queue und man nimmt immer das erste in der höchsten queue
gibt IO Boost in windows wird aber in der Klausuraufgabe beschrieben
gibt Aging in Unix, hier wird CPU zeit bestraft
sollen beide Starvation verhindern also, dass Programme also keine CPU zeit bekommen

Schlägt ein Assert fehl dann wird der Thread beendet, wird der Hauptthread beendet wird auch der Prozess beendet.



Lernen

Begriff

Interrupt: signal auf einem Draht
Kernalobkel
Mutex
Event
Affinität
Maschinenwort
Aufgaben eines Betriebssystems(Ressourchenverwaltung, Hardwareabstraktion, Isolation(Security, Prozesse isoliern, Komplexität verberegn))
Ziele eines OS
Concurrency von Threads
Kritischer Abschnitt
Backery
deadlock
lifelock
Threadzustände
scheduling strategien,
Kontextwechsel: Wechsel von einem Thread zum anderen
optimierungskriterien: Durchsatz(wenige Kontext Wechsel), minimale Wartezeit(viele Kontextwechsel)
Lifo,
Belagis anomali: Mehr Speicher und mehr Zugriffs Fehler bei Fifo
Interrupts DPC, APC

Speichereinteilung: Fragmentierung/ Paging
Flags/STeuerungsbits zu Seiten: Owner, Dirty
Seitenlisten
scheduling
ACL Access control list. geht immer bis zum ersten Treffer, egal wie viele weitere Hits noch kommen Match
- Liste von Tripeln (Aktion, Akteur, Recht: Bsp Access for Admin denied)
- Liste Leer/kein passender Eintag verboten, Null alles ist erlaubt
- Acl  ist mächtiger
- rwx kann man mit acl simulieren nicht andersherum
- jede datei hat das und die acl liste kann nur von owner geändert werden
rwx Bits
- rwx|rwx|rwx Owner|Gruppe|Other
Apc dpc gibt gutes Diagramm zu :switch von user zu kernal mode
dpc defered procedure call
