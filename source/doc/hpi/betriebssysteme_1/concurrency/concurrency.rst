===========
Concurrency
===========


The Critical-Section Problem
----------------------------


Example:
Multithreading of this program

.. code-block:: c

  int i = 0;
  for(int k = 1; k < 10; k++){
    i +=k
  }

will likely result in a lower result than expected

Solution
########

  - Mutual Execution
    - Only one thread at a time is allowed into its critical section
  - Progress
    - A thread remains inside its critical section for a finite time only
    - No assumptions concerning relative speed of the threads.
  - Bounded Waiting
    - It must no be possible for a thread requiring access to a critical section to be delayed indefinitely.
    When no thread is in a critical section, any thread that requests entry must be permitted to enter without delay.


Backery -Algorithm
##################
best software solution but dreadfully slow
so hardware solution is necessary.


Hardware solution
#################

Interrupt Disabling
- Concurrent threads cannot overlap on a uniprocessor
- Thread will run until performing a system call or interrupt happens

Special Atomic Machine Instructions
- Test and Set Instruction - read & write a memory location
- Exchange Instruction - swap register and memory location
Problems with Machine-Instruction Approach
- Busy waiting (checking all the time whether something is happening)
- Starvation is possible
- Deadlock is possible
