While - Programmierung
======================

Whileprogramme sind Zeichenketten bestehend aus
  - Variablensymbolen :math:`x_{2}`
  - Constanten :math:`0, 1`
  - Symbole :math:`;  :=  +  -`
  - Schlüsselwörter :math:`WHILE, DO, END`


Wir halten fest: WHILE-Programme sind rekursiv definiert; die folgenden beiden Zeichenketten sind atomare WHILE-Programme.

.. math::

  x_i:=x_j+x

  x_i:=x_j+c

Wenn *P1* und *P2* WHILE-Programme sind und *v* ein Variablensymbol, dann sind auch die folgenden Zeichenketten WHILE-Programme.

.. math::

  P1;P2

  **WHILE** v\neq0 **DO** P1 **END**

In dieser sehr simplen Programmiersprache können wir jetzt kompliziertere Funktionen bauen, zum Beispiel die Addition.

.. code::

   x_0:=x_1+0; //Kopieoperation
   **WHILE** x2!=0 **DO**
   x0:=x0+1;
   x2:=x2-1
   END


Zuweisungen von Konstanten, also :math:`xi:=c;`, sind auch nicht direkt möglich. Stattdessen kann man zum Beispiel das folgenden Programm nehmen.

.. code::

  WHILE x_i != 0 DO
  xi:=xi-1
  END;
  xi:=xi+c

Ebenso können andere, auch komplizierte arithmetische Operationen eingeführt werden, inklusive Relationen wie :math:`\leq \leq` und :math:`\geq \geq` (wobei wir eine Ausgabe von 00 als false interpretieren und alle anderen Werte als true). Auch können wir andere Konstrukte erstellen, zum Beispiel IF-Verzweigungen. Wenn wir ``IF v!=0 THEN P END`` implementieren wollen, können wir zum Beispiel das Folgende schreiben.

.. code::

  z:=v+0;
  WHILE z!=0 DO
  P;
  z:=0
  END

Wir erlauben uns IF-Verzweigungen und andere komplexe Operationen im WHILE-Code zu benutzen und verstehen das als verkürzte Darstellung des (etwas länglichen) tatsächlichen WHILE-Codes (mit entsprechend vorsichtiger Wahl der zusätzlich benötigten Variablen). Man spricht dann auch von Syntactic Sugar.


While Semantik
--------------
rechnen nur mit natürlichen Zahlen, daher haben Variablen


syntactic sugar
---------------

Allgemeine Definition und angeben, dass
xi, xj ->moodle Artikel

max
###

.. math::

  x_0 = x_1 - x_2

  x_0 = x_0 + x_2


WHILE
------
Alphabet {a...z, A...Z, :, =, !}
Wörter {WHILE; :=, !=, x1}
Sprache{x | x element Wörter}
