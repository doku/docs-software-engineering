Berechnungsmodelle
==================

LOOP- und WHILE-Programme
-------------------------

LOOP-Programme
______________

LOOP ist eine einfache Programmiersprache mit folgenden syntaktischen Komponenten:

  - Variablen: :math:`x_0 x_1 x_2 ...`
  - Konstanten: :math:`0  1  2 ...`
  - Trennsymbole: :math:`; :=`
  - Operationszeichen: :math:`+ -`
  - Schlüsselwörter: :math:`LOOP , DO , END`

.. note::
  Jedes Loop Programm is While berechenbar, nicht anders herum.
  In Loop hat man Schleifen fester Länge, While beliebiger Länge

Ohne wohldefinierte Semantik sind LOOP-Programme nur formale Wörter ohne Bedeutung.
Programme unterschiedlicher Syntax können die selbe Semantik haben.


Bedingte Anweisungen in LOOP:
#############################

  .. code-block:: ruby

    # sei P Loop Programm und xi1 Variable
    IF xi = 0 THEN P END

    #ist abküzende Schreibweise für
    xj:=1;
    LOOP xi DO xj := 0 END;
    LOOP xj DO P END;
    xj := 0;
    #wobei xj eine Variable ist, die in P nicht vorkommt.




LOOP Berechenbarkeit
####################

.. note::

    **Definition Loop-berechenbar**

    Eine Funktion :math:`f: N^k \rightarrow N` heißt
    LOOP-berechenbar, falls es ein LOOP-Programm
    P gibt, das f in dem Sinne
    berechnet, dass P, gestartet mit :math:`n_1,...,n_k` in
    den Variablen :math:`x_1,...,x_k` (und 0 in den restlichen
    Variablen) stoppt mit dem Wert :math:`f(n_1,...,n_k)` in
    der Variablen :math:`x_0`


.. attention::

  Loop Programme terminieren durch endliche Schleifenlänge immer. Daher müssen sie nach der Berechenbarkeitsdefinition immer ein Ergebnis zu einer beliebigen Eingabe finden.
  Eine LOOP-berechenbare Funktion :math:`f: N^k \rightarrow N` ist immer total!


LOOP Abgeschlossenheit
######################

Loop-Funktionen sind unter einer Vielzahl von Operationen **abgeschlossen**, z.B. Fallunterscheidung.

.. figure:: loop_abgeschlossen.png
	 :alt: loop_abgeschlossen




partielle vs. totale Funktion
_____________________________

Eine partielle Funktion von der Menge X in die Menge Y ist eine rechtseindeutige Relation, das heißt eine Relation, in der jedem Element der Menge X höchstens ein Element der Menge Y zugeordnet wird.

.. note::

  * **partielle Funktion** :math:`f: X \rightarrow Y` mit :math:`Def(f) \subseteq X`
  * **totale Funktion** :math:`f: X \rightarrow Y` mit :math:`Def(f) = X`
  Jede Totale Funktion ist eine Partielle, aber nicht jede Partielle ist Total.

Daraus folgt, dass partielle Funktionen über Definitionslücken verfügen.
**Definitionsbereich**:math:`dom(f) = {x \in \mathbb{N} | f(x) \neq \bot} = X`
**Wertebereich / Bild**:math:`img(f) = {x \in \mathbb{N} | x \in A} \subseteq Y`
f heißt total falls :math:`dom(f) = \mathbb{N} `



WHILE-Programme
_______________

Loop Schleifen können wir durch WHILE-Schleifen ersetzt werden. Damit können **alle Loop Programme mit While dargestellt werden, nicht anders herum.**

.. note::

    **Definition WHILE-berechenbar**

    Eine Funktion :math:`f: N^k \rightarrow N` heißt
    WHILE-berechenbar, falls es ein WHILE-Programm
    P gibt, das f in dem Sinne
    berechnet, dass P, gestartet mit :math:`n_1,...,n_k` in
    den Variablen :math:`x_1,...,x_k` (und 0 in den restlichen
    Variablen) stoppt mit dem Wert :math:`f(n_1,...,n_k)` in
    der Variablen :math:`x_0`


.. note::

  Ein WHILE-berechenbare Funktion :math:`f: N^k \rightarrow N` ist nur **eine partielle Funktion, also nicht notwendig total!**
  Daher terminieren WHILE-Programme nicht immer.

Da WHILE Programme durch die While-Konstruktion unendlich lange laufen können, ist es nach der Berechenbarkeitsdefinition auch möglich eine partielle Funktion darzustellen. Ist für eine Eingabe keine Ausgabe definiert terminiert das While-Programm einfach nicht.

.. figure:: sqrt_while_berechenbar.png
	 :alt: sqrt_while_berechenbar

Ackermannfunktion
-----------------

.. figure:: ackermann.png
	 :alt: ackermann

.. figure:: ackermann_total.png
	 :alt:    ackermann_total

.. figure:: ackermann_1.png
	 :alt:    ackermann_1

(für Gleichheit bei c für x, y 0 einsetzen)

.. figure:: f_loop.png
	 :alt:    f_loop


.. figure:: ackermann_wertetabelle.png
	 :alt: ackermann_wertetabelle

In Worten gibt f

Wollen Zeigen, dass es kein LOOP Programm geben kann, mit dem man Ackermann berechnen kann

Nach einer einzigen Zuweisung :math:`x_i = x_j +- 1` kann die gesamt Summe aller Variablen nicht größer als :math:`2n +1` sein.

Weiters Loop-Programm ist Konkatenation von zwei Programmen

.. note::
    Die Ackermannfunktion ist eine totale Funktion, die WHILE aber nicht LOOP-berechenbar ist.

.. attention::
    While-berechenbar ist echt mächtiger als LOOP-berechenbar,
    da mit WHILE auch partielle Funktionen und totale Funktionen wie Ackermann berechnet werden können.


Chruch-Turing-These
-------------------
.. note::

  **Chruch-Turing-These**

  Die Menge der WHILE-berechenbaren Funktionen charakterisiert genau die Menge
  der intuitiv-berechenbaren Funktionen.

Die These ist nicht beweisbar.




Gödelisierung
-------------
.. note::
  Sei P die Menge aller WHILE Programme. Eine bijektive Funktion
  :math:`göd: P \rightarrow \mathbb{N_0}` heißt bijektive Gödelisierung.
  Es gibt eine bijektive Gödelisierung aller WHILE-Programme.


While-Programme beliebiger Lenge können Zeilenweise in Nummern übersetzt werden.
Diese wiederum werden gepaart sodass ein Programm mit einer Nummer dargestellt werden kann.

Zwei verschiedene Programm mit unterschiedlicher Syntax aber selber Semantik haben unterschiedliche Gödelnummern.

jede Nummer ist ein Programm


Universalität
-------------

.. note::
  **Definition:**
  Für alle :math:`e \in \mathbb{N}` sei :math:`\varphi_e : \mathbb{N} \rightarrow \mathbb{N}` die Funktion, die von dem mit Gödel-Nummer e codierten Programm berechnet wird, falls :math:`e` ein Programm ist; sonst die konstant-0-Funktion.

  **Satz**
  Die Funktion :math:`U: (e,x) \rightarrow \varphi_e (x)` ist berechenbar.

Es gibt ein universelles Programm, man gibt ihm die Nummer des While Programms und die Eingabe und es kann die Ausgabe simulieren

.. figure:: universelles_programm.png
	 :alt: universelles_programm

Sprechweise
-----------
Berechenbarkeit:
u „berechenbar“ heiße ab jetzt immer
WHILE-berechenbar
u Ein „Programm“ sei immer
ein WHILE-Programm
u LOOP-berechenbar spielt keine Rolle mehr
