Formalisieren
=============

Eine sprachlich gegebene Aufgabe muss interpretiert werden; damit gelangen wir zu einer formalen Spezifikation für einen zu findenden Algorithmus. Zentral in jeder Formalisierung sind dabei die folgenden vier Punkte.

Eingabe;
Ausgabe;
Korrektheitsbedingung;
Interpretation.
Die ersten drei Punkte ergeben die Spezifikation des Algorithmus; der vierte Punkt stellt die Verbindung zur gegebenen, sprachlichen Formulierung her. Wir nutzen die folgenden Datentypen zur Spezifikation von Eingabe und Ausgabe.

Mögliche Eingabe und Ausgabe Datentypen
---------------------------------------

- Zahlen (als primitive Datentypen): :math:`mathbb{N, Z, Q, R, C}`
- Arrays (zur Formalisierung von angeordneten Objekten); immer gebunden an einen Typ von Elementen.
- Mengen (zur Formalisierung von nicht-angeordneten Objekten); immer gebunden an einen Typ von Elementen.
- Strings (z.B. zur Formalisierung von Wörtern, Sätzen oder DNA-Sequenzen); z.B. :math:`\{0,1\}*,\{A,T,C,G\}*`
- Graphen (z.B. zur Formalisierung von Netzen); :math:`G=(V,E)`, wobei :math:`V` die Menge der Knoten und :math:`E` die Menge der Kanten ist.
- Punktwolken (z.B. zur Formalisierung von Punkten im euklidischen Raum); :math:`Rd`, wobei :math:`d` die Dimension ist.
- Funktionen; :math:`f:A \rightarrow B`.

Interpretationen sind nicht eindeutig; eine gute Formalisierung bietet Alternativen an und geht auf deren Plausibilität ein.

Prozess des Algorithmendesigns
------------------------------

.. figure:: prozess_algorithmen_design.png
	 :alt: prozess_algorithmen_design
