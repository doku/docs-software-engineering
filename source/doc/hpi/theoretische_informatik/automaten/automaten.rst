=========
Automaten
=========

Es ist nicht möglich mit einem einfachen endlichen Automaten einen Automaten zu bauen,
der alle symmetrischen Wörter erkennen kann
Mit einem Kellerautomaten ist dies möglich.
Ein Kellerautomat hat einen Stack und
kann sich damit die erste hälfte der Eingabe speichern und anschließend auf Symmetrie untersuchen
