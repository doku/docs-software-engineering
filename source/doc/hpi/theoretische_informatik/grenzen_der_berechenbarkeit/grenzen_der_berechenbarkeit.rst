Grenzen der Berechenbarkeit
===========================

Nicht Berechenbarkeit
---------------------

Nach der Gödelisierung gibt es eine Bijektion zwischen der Menge der WHILE Programme und den natürlichen Zahlen.
Damit ist die Menge der Programmen abzählbar. Jedoch ist die Menge der totalen Funktionen überabzählbar.
Es folgt also:

.. note::
  Es gibt totale Funktionen die nicht WHILE berechenbar sind.


Berechnen vs Entscheiden
------------------------

Aktuell ist der Berechnenbarkeits Begriff auf Funktionen eingeschränkt. Im Folgenden soll dieser auf Sprachen erweitert werden.
Dabei sind :math:`x \in \Sigma^*` Wörter und :math:`A \subseteq \Sigma^*` Sprachen. Da :math:`\Sigma*` und :math:`\mathbb{N}` gleichmächtig sind können
wir auch :math:`A \subseteq \mathbb{N}` als Sprache bezeichnen.
Im Folgenden wird man also entscheinden müssen, ob ein Wort in einer Sprache enthalten ist also,
ob :math:`x \in A  \subseteq \Sigma^*` , bzw :math:`x \in A  \subseteq \mathbb{N}`

Auch dieses "Entscheiden" lässt sich durch Funktionen darstellen. Hierzu zunächst eine kurze Herleitung.

Ein WHILE-Programm :math:`e` berechnet bisher eine (partielle) Funktion :math:`\varphi_e : \mathbb{N}^k \rightarrow \mathbb{N}`.
Aufgrund der Pair-Funktion können wir die Eingabe zu einer Zahl zusammenfassen: :math:`\varphi_e : \mathbb{N} \rightarrow \mathbb{N}`
Nun führen wir eine weitere Trandformation der Funktion durch.
Bei normalen Funktionen erhalten eine Eingabe x und geben das Ergebnis f(x) aus. Nun wollen wir diese Berechnung in eine Entscheidung übetragen.
Dazu erstellen wir eine weitere Funktion :math:`g: \bigl< x , f(x) \bigr> \rightarrow \{0,1\}`
Wir haben also nun eine Funktion erstellt, der wir alle möglichen Kombinationen zwischen x und f(x) gepairt geben
und diese antwortet uns ob die Zuordnung eine valide ist oder nicht.
Dies ist der Übergang von der Berechnung zur Entscheidung.
Damit haben wir uns auf :math:`\varphi_e : \mathbb{N} \rightarrow \{ 0,1\}` beschränkt.

.. note::
  :math:`\varphi_e` lässt sich vollständig durch die Sprache :math:`L(e) : \{ x | \varphi_e (x) = 1  \}` beschreiben

Eine TURING-Maschine M entscheidet eine Sprache :math:`L(M) \subseteq  \Sigma^* ` , d.h. :math:`f:  \Sigma^* \rightarrow \{ 0,1 \}`
:math:`\mathbb{N} \rightarrow {0,1}` und :math:`\Sigma^* \rightarrow {0,1}` sind gleichmächtig, da :math:`\Sigma^*` und :math:`\mathbb{N}` gleichmächtg sind.


Charakteristische Funktionen
############################

Die **charakteristische** Funktion einer Sprache :math:`A \subseteq \Sigma^*` ist :math:`\chi_A : \mathbb{N} \rightarrow \{ 0, 1 \}`

.. math::
  \chi_A (x) =
    \begin{cases}
      1, x \in A \\
      0, x \notin A
    \end{cases}


Die **partielle charakteristische** Funktion einer Sprache :math:`A \subseteq \Sigma^*` ist :math:`\hat{\chi_A} : \mathbb{N} \rightarrow \{ 1 \}`

.. math::
  \hat{\chi_A} (x) =
    \begin{cases}
      1, x \in A \\
      \bot, x \notin A
    \end{cases}


Entscheidbarkeit
----------------

Terminiert ein WHILE-Programm M für eine Eingabe x nicht, sagen wir :math:`M(x) = \bot`, sonst ist
:math:`M(x)` gleich der Ausgabe.

Ein WHILE-Programm M berechnet f, falls

.. math::
  \forall x \in dom(f) \rightarrow M(x) = f(x) \\
  \forall x \notin dom(f) \rightarrow M(x) = \bot


.. note::
  Eine Sprache :math:`A \subseteq \mathbb{N}` ist **entscheidbar** wenn ihre charakteristische Funktion :math:`\chi_A ` berechenbar ist.

  Eine Sprache :math:`A \subseteq \mathbb{N}` ist **semi-entscheidbar** wenn ihre partielle charakteristische Funktion  :math:`\hat{\chi_A}` berechenbar ist.

.. note::
    Eine Sprache :math:`A \subseteq \mathbb{N}` ist **entscheidbar** :math:`\Leftrightarrow`
    Sowohl :math:`A` als auch :math:`\bar A` sind **semi-entscheidbar**

Beweis:
#######
Die Hinrichtung ist klar. Bei der Rückrichtung macht man sich zu nutze, dass jedes Element entweder in :math:`A` oder in :math:`\bar A` sein muss.
Und damit früher oder später einer der partiellen charakteristischen Funktionen eine 1 zurück liefern muss. Damit ist klar, das eine 1 kommen wird
uns sobald diese kommt ist auch klar ob das Element in A enthalten ist oder nicht.

.. figure:: beweis_entscheidbar_semi_entscheidbar.png
	 :alt: Beweis_entscheidbar_semi_entscheidbar


S-m-n Theorem
-------------

Gegeben einer Gödelnummer eines Programmes :math:`\mathbb{N}^{n+m} \rightarrow \mathbb{N}`

Rekursiv-Aufzählbar
-------------------
.. note::

  Eine Sprache :math:`A \subseteq \mathbb{N}` heißt rekursiv aufzählbar (kurz :math:`A \in RE`), falls :math:`A = \emptyset` oder das Bild :math:`img(f)`
  einer berechenbaren Funktion f ist.
  Die berechenbare Funktion f zählt A (rekursiv) auf: :math:`A = \{ f(0), f(1), f(2), ...\}`

.. note::
  **Satz:**
  Eine Sprache A is **rekursiv aufzählbar** :math:`\Leftrightarrow` A ist **semi-entscheidbar**

Beweis:
#######

.. figure:: beweis_re_semi_entscheidbar.png
	 :alt: beweis_re_semi_entscheidbar

Es ist recht offensichtlich, dass eine **semi-entscheidbare** Sprache rekursiv aufzählbar ist.


.. figure:: beweis_re_semi_entscheidbar2.png
	 :alt: beweis_re_semi_entscheidbar2

f ist total und berechenbar und hat den Wertebereich A. Also zählt f die Sprache A rekursiv auf.

Zusammenfassung
---------------

.. note::
  Folgende Aussagen sind äquivalent:
  * :math:`A \in RE`
  * A ist **rekursiv aufzählbar**
  * A ist **semi entscheibar**
  * :math:`\hat{\chi_A} ` ist Turing/While/Goto/... berechenbar
  * Es gibt eine berechenbare Funktion :math:`f` mit dem Definitionsbereich :math:`dom(f) = A`
  * Es gibt eine berechenbare Funktion :math:`f` mit dem Wertebereich :math:`img(f) = A`

.. note::
  Folgende Aussagen sind äquivalent:
  * :math:`A \in REC`
  * A ist **entscheibar**
  * :math:`A \in RE` und   * :math:`\bar A \in RE`
  * :math:`\chi_A` ist Turing/While/Goto/... berechenbar
  * Es gibt eine monotone berechenbare Funktion :math:`f` mit dem Wertebereich :math:`img(f) = A`


Nichtentscheidbare Probleme
---------------------------

Es gibt sehr viele unentscheidbare Probleme, nach einfachem Zählargument.
Die Menge der Entscheidungsverfahren ist gleichmächtg zu :math:`\mathbb{N}`
Die Menge der Sprache ist jedoch gleichmächtig zu :math:`\mathbb{R}`

Spezielles Halteproblem
#######################

Das (spezielle) Halteproblem ist die Sprache

.. math::
  H_0 = \{ e \in \mathbb {N} |  \varphi_e (e) \neq \bot \}

.. note::
  :math:`H_0` ist semi entscheidbar.

.. note::
  :math:`H_0` nicht entscheidbar.

Many-One-Reduktion
------------------

Seien :math:`A \subseteq \mathbb{N}` und :math:`B \subseteq \mathbb{N}` Sprahcen und sei :math:`f: \mathbb{N} \rightarrow \mathbb{N}` eine
**totale und berechenbare** Funktion, sodass gilt:

.. math::

  \forall x \in \mathbb{N} : x \in A \Leftrightarrow f(x) \in B


.. note::
  Seien A und B Sprachen mit :math:`A \leq B`. Dann gilt:
  (1) Wenn B entscheidbar ist, ist auch A entscheidbar.
  (2) Wenn B semi-entscheidbar ist, ist auch A semi-entscheidbar.
  (3) Wenn A nicht entscheidbar ist, dann ist auch B nicht entscheidbar.
  (4) Wenn A nicht semi-entscheidbar ist, dann ist auch B nicht semi-entscheidbar.
