Mathe
=====
Im folgenden soll an einige mathematische Sachverhalte erinnert werden, welche in der Theoretischen Informatik von nützen sind.

Mengen
------


* Zwei Mengen X und Y sind **gleich mächtig** falls es eine Bijektion von X nach Y gibt.
* Menge heißen **abzählbar** wenn sie gleich mächtig wie :math:`\mathbb{N}` sind; sonst **überabzählbar**.
* :math:`\mathbb{N}` und :math:`\mathbb{Q}` sind gleich mächtig, d.h. es existiert bijektive Abbildung zwischen ihnen.
  (Cantors erstes Diagonalargument)

.. figure:: cantors_erstes_diagonalargument.png
  :alt: cantors_erstes_diagonalargument
  :scale: 50%


* :math:`\mathbb{R}` ist echt mächtiger als :math:`\mathbb{Q}` (Cantors zweites Diagonalargument)

Um dies zu beweisen, nehmen wir an, dass :math:`\mathbb{R}` gleichmächtig zu :math:`\mathbb{N}` ist und daher aufgezählt werden kann.

.. figure:: cantors_zweites_diagonalargument.png
   :alt: cantors_zweites_diagonalargument
   :scale: 50 %
   :align: left

Nun kann man eine rationale Zahl :math:`x = 0,(a_{11} + 1)(a_{22} + 1)(a_{33} + 1)...` sie sich zu jeder bestehenden Zahl in mindestens einer Stelle unterscheidet.
Da diese Zahl nicht in der bestehende Aufzählung zu finden ist, führt dies zu Wiedersprich der Annahme, dass :math:`\mathbb{R}` gleichmächtig zu :math:`\mathbb{N}` ist.
Mit dem selben Schema kann gezeigt werden, dass die Menge der Funktionen überabzählbar ist.

.. figure:: funktionen_ueberabzaehlbar.png
	 :alt: funktionen_ueberabzaehlbar

Auch hier kann man nun einfach eine Funktion bilden, die sich zu jeder bestehenden in mindestens einer Stelle unterscheidet.

Weiteres
--------

* **Kontinuumshypothese**: Es gibt keine Menge, deren Mächtigkeit zwischen der von :math:`\mathbb{N}` und :math:`\mathbb{R}` liegt.
  Diese Hypothese ist grundsätzlich nicht entscheidbar, d.h. orthogonal zu Axiomen der Mengenlehre.
