=====
Heaps
=====

Kompexity
=========

Build in :math:`O(n)`
insert :math:`O(n)`


Gibt es eine Heap Datenstruktur mit:

- mit insert und extract O(1)
würde beides in unter O(log n) gehen könnte man sonst in unter O(n log n)sortieren

- insert findMin decreaseKey in O(1) delete extractMin in O(log n)
geht aber noch nicht klar wie es gehen soll

- funktioniert der such algorithmus für Suchbäume auch auf heaps: für manche Heaps(lineare ketten) im algemeinen zwischengespeichert


Nachfolger: der linkeste aller rechten
Vorgänger: der rechteste aller linken
