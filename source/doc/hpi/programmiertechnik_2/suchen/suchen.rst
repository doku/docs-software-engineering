======
Suchen
======

Problem Formailsierung
======================
Im Allgemeinen eine Menge möglicher Lösungen, der **Lösungsraum** sowie eine Bewertungsfunktion vorgegeben. Die Lösungsräume können eine praktisch nicht handhabbare Größe besitzen (z.B. hochdimensional, unendlich...)

Mögliche **Problemstellungen** lauteten nun wie folgt:

  - **Entscheidbarkeitsproblem**: ermittle, ob es mindestens eine Lösung gibt
  - **Optimierungsproblem**: ermittle die beste Lösung
  - **Suchproblem**: ermittle eine möglichst gute Lösung (**Approximationsalgorithmen** basierend auf Heuristiken helfen in der Praxis, das Suchproblem in Lösungsräumen zu beherrschen)

Evolutionärer Algorithmus
=========================


Suchen in sortierten Folgen
===========================

Ordnungsrelation
----------------
Auf sortierten Daten besteht eine Reihenfolge oder Ordnung, welche durch eine **Ordnungsrelation** definiert wird.

.. math::

  \circ : T \times T \rightarrow boolean
  (a,b) \mapsto a \circ b

Totale Ordnung
##############
Ordnungsrelation mit den Eigenschaften

 - Reflexivität :math:`x \leq x`
 - Antisymetrie
 - Transitivität :math:`x \leq y \wedge y \leq z \Rightarrow x \leq z`
 - Totalität

Ordnungsrelationen in C++
#########################

Ordnungsrelationen stehen in C++ als Binäre Prädikate für nummerische Datentypen bereits in ``<functional>`` zur Verfügung.

.. code-block:: cpp
    :linenos:

    less<T>(a,b)
    less_equal<T>(a,b)
    greater<T>(a,b)
    greater_equal<T>(a,b)
    equal_to<T>(a,b)
    not_equal_to<T>(a,b)

Für selbst definierte Datentypen müssen die Relationsoperatoren ``<;>;=,!=`` selbst definiert werden.

.. code-block:: cpp
    :linenos:

    bool operator<(const T& a, const T&b){
      //return true or false depending on your type-specific determination of a < b
    }


Zugriff auf Elemente
--------------------

Sortierte Folgen sind in der Regel in Containern gespeichert, auf die per Index zugegriffen werden kann. Der wahlfreie Zugriff auf Elemente eines Containers T wird über den **Subskriptoperator** implementiert.

.. code-block:: cpp
    :linenos:

    T& A::operator[](int i);
    const T& A::operator[](int i) const;

Iteratoren
##########

Iteratoren abstrahieren den sequentiellen Zugriff auf Elemente eines Datencontainers. Ein Iterator ist ein Objekt, das eine Traversierung eines Datencontainers vom Anfang bis zum Ende (oder in einem Teilbereich) ermöglicht. Ein Iterator kann für den Anfang eine Sequenz und für das Ende (genauer: nach dem letzten Element) z.B. durch den Container erzeicht werden (Bsp. für Container A: ``A.begin()``, ``A.end()``).
Über den Iterator kann auf das jeweils aktuelle Element zugegriffen werden. In C++ erfolgt dies über den **Dereferenzierungsoperator** ``*it``. Ein Iterator kann in der Sequenz um ein Element nach vorne bewegt werden. In C++ erfolgt dies über den **Präinkrementoperator** ``++it``. Steht der Iterator auf dem letzten Element und wird nach vorne bewegt, kann man daran das Ende der Traversierung erkennen ``it == A.end()``.


.. code-block:: cpp
    :linenos:

    std::vector<int> A; // Beispiel für Container

    auto it = A.begin();
    while(it != A.end()){ // While-Variante
      doSth(*it);
      ++it;
    }

    for(auto it = A.begin(); it != A.end(); ++it) doSth(*it); // For-Variante



Den sequentiellen Zugriff kann man mit Funktionstemplates noch weitergehend Kapseln.

.. code-block:: cpp
    :linenos:

    template<class InputIt, class UnaryFuction>
    UnaryFunction for_each(InputIT first, InputIt last, UnaryFunction f){

      for(; first != last; ++first)f(*first);
      return f;

    }
    // so ähnlich gibt es das bereits in der Standardbibliothek

    std::for_each(A.begin(), A.end(), [](int i){doSth(i);}); // For-Each mit Lamda

Sequentielle Suche
------------------
**Element-für-Element-Durchsuchen** einer Folge nach einem bestimmten Schlüsselelement. Sie wird auch **Lineare Suche** genannt. Dabei spielt es keine Rolle, ob die Folge sortiert oder unsortiert ist. Zurückgegeben wird die Position des ersten Elementes, das mit dem Schlüssel übereinstimmt, oder ein Wert, der das Nichtvorhandensein bezeichnet. Die Voraussetzung ist lediglich die Definition der Gleichheitsrelation ``==`` für die Daten vom Typ T.

Laufzeit-Komplexität
####################

- best case: :math:`O(1)`
- worst case: :math:`O(n)` worst case tritt auch bei erfolgloser Suche ein
- average case: :math:`O(n)` mit :math:`\frac{n+1}{2}` Suchvorgänge

Binäre Suche
------------
In der Binären Suche wird die Sortierung zur schnellen Ermittlung des gesuchten Schlüsselelementes im Suchbereich genutzt.


Suche auf unsortierten Daten
============================

Auf unsortierten Daten ist eine lineare Suche nötig, die den Datencontainer traversiert und das zu suchende Element bestimmt. Dazu ist ein Suchkriterium wie folgt gegeben: :math:`P:T \rightarrow boolean`. Das Suchkriterium ordnet jedem Element des Containers zu, ob es die Bedingungen erfüllt oder nicht.

.. code-block:: cpp
    :linenos:

    template<class InputIt, class UnaryFuction>
    UnaryFunction for_each(InputIT first, InputIt last, UnaryFunction f){

      for(; first != last; ++first)f(*first);
      return f;

    }
