=========
Sortieren
=========

Sortieren ist ein **grundlegendes Problem der Informatik** und Schätzungen nach entfällt rund ein viertel aller Rechenzeit auf Sortiervorgänge. Die Aufgabe besteht darin, Datensätze mit Schlüsseln entsprechend einer Ordnungsrelation zu sortieren.
So ist der input eine beliebige Permutation der Schlüssel, und das Ziel ist diese in eine Permutation zu überführen, welche die Ordnungsrelation erfüllt.

Grundbegriffe
=============
 -**Interne Sortierverfahren**: In Hauptspeicherstrukturen (Felder, Listen) sortieren
 -**Externe Sortierverfahren**: Datensätze auf externen Medien (Festplatte, Magnetbänder) sortieren (Wichtig in Datenbanksystemen)
 -**Stabilität**: Die Reihenfolge gleicher Schlüssel bleibt erhalten

Sortierverfahren
================

Insertionsort
-------------

Funktionsprinzip
################
Insertionsort arbeitet nach dem Prinzip des **Sortieren eines Kartenstapels**.
- starte mit der ersten Karte einen neuen Stapel
- nimm jeweils die nächste Karte des Orginalstapels und füge sie an der richtigen Stelle in den neuen Stapel ein

.. figure:: insertionsort_bsp.png
	 :alt:   insertionsort_bsp

Beispielimplementierung
#######################
  .. code-block:: cpp
    :linenos:

    void instertionSort(std::vector<int>& A){

    const int N = A.size();

    //start with the second element
    for(int j = 1; j < N; j++){

      int k = a[j]; // store a copy of the element to be inserted
      //move sorted elements to the right until k finds its place
      int i;
      for(i = j; (i >= 1) && (A[i-1]>k) i--){
        A[i] = A[i-1];
      }
      A[i] = k;
    }

    }

Komplexitätsanalyse
###################
- best case(korrekt sortierte Liste): :math:`O(n)`
- average case (unsortierte Liste): :math:`O(n^2)` mit :math:`\frac{n^2}{4}` Vergleichen und :math:`\frac{n^2}{8}` Vertauschungen
- worst case(umgekehrt sortierte Liste): :math:`O(n^2)`

Optimierung
###########
Suche der Einfügestelle mit binärer Suche und Einfügen in Baumstruktur

Shellsort
---------

Shellsort ist eine Erweiterung von Insertionsort und löst das Problem des ineffizienten Einfügens bei Insertionsort(bis zu n Elemente müssen bewegt werden). Dazu wird die **Eingabe in h Teile zerlegt** und diese Teile werden separat sortiert.
