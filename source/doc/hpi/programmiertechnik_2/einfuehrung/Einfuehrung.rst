Introduction
============

"Programming is the art of expressing solutions to problem" [Stroutstrup, 2009]

Softwareentwicklungsprozess
----------------------------

.. figure:: softwareentwicklung.png
	 :alt: Softwareentwicklung

V-Modell der Softwareentwicklung

.. figure:: v-Modell.png
	 :alt:    V-Modell

Programmiersprachen
-------------------

Progammiersprachen sind künstliche Sprachen zur Formulierung von Programmen, die keine Mehrdeutigkeit bei der Interpreation zulassen dürfen.

  - **Lexikalität** : definiert die gültigen Zeichen bzw. Wörter, aus denen Programme der Programmiersprache zusammengesetzt sein dürfen.
  - **Syntax**: definiert den korrekten Aufbau der Sätze aus gltigen Zeichen bzw. Wörtern, d.h. sie egt fest, in welcher Reihenfolge lexikalisch korrekte Zeichen bzw. Wörter im Programm auftreten dürfen.
  - **Semantik**: definiert die Bedeutung syntaktisch korekter Sätze, d.h. sie beschreiben, was passiert, wenn bestimmte Anweisungen ausgeführt werden.
  - **Pragmatik**: definiert ihren Einsatzbereich, d.h. für welche Arten von Problemen die Programmiersprache beonders gut geeignet ist.

Programmiersprachen variiren in deren Abstraktionsniveau Assambler - C - Java - Python

Algorithmen
-----------

  "An algorithm is a finite set of well-defined rules for the solution of a problem in a finite number of steps."

 "Ein Algorithmus ist eine eindeutige, endiche Beschreibung eines allgemeinen, endlichen Verfahrens zur schrittweisen Ermittlung gesuchter Größen aus gegebenen Eingaben. Die Beschreibung erfolgt in einem Formalisums mit Hilfe von anderen Algorithmen(Unterprogrammen) und letztlich elementaren Algorithmen (Sprachimplementierung). Ein Algorithmus muss ausführbar sein" [Balzert, 2004]

"Eine Berechnungsvorschrift zur Lösung eines Problems heißt genau dann Algorithmus, wenn eine zu dieser Berechnungsvorschrift äquivalente Turingmaschine existiert, die für jede Eingabe, die eine Lösung besitzt, stoppt."


Eigenschaften:
_______________

  - **statische Finitheit**: Das Verfahren muss in einem endlichen Text eindeutig beschreibbar sein. Auch darf die Ausführung nicht von Anforderungen wie Kenntnissen, Intelligenz und Intuition abhängen.
  - **dynamische Finitheit**: Das Verfahren darf zu jedem Zeitpunkt nur endlich viel Speicherplatz benötigen.
  - **Ausführbarkeit / Effekivität**: Jeder Schritt des Verfahrens muss tatsächlich ausführbar sein und die Effekte jeder Anweisung des Algorithmus mus eindeutig festgelegt sein.
  - **Terminierung**: Ein Algorithmus heißt terminierend, wenn er, bei jeder erlaubten Eingabe nach endlich vielen Schritten anhält oder kontrolliert abbricht. Ein nicht terminierender Algorithmus gerät für bestimmte Eingaben in eine Endlosschleife (dies kann bei manchen Abläufen gewünscht sein, z.B. Steuerungssysteme die auf Nutzereingabe warten). Die Terminiertheit eines Algorithmus festzustellen (das Halteproblem) ist nicht entscheidbar, d.h. das Problem, ob ein beliebiger Algorithmus mit beliebiger Eingabe terminiert, ist nicht durch einen Algorithmus lösbar.

Determiniertheit und Detreminismus
__________________________________

  - **Determiniertheit**: Der Algorithmus muss bei den selben Startbedingungen/im selben Startzustand und den gleichen Eingaben das gleiche Ergebnis liefern.
  - **Determinismus**: Ein Algorithmus heißt *deterministisch*, wenn zu jedem Zeitpunkt seiner Ausführung der nächste Handlungsschritt eindeutig definiert.

Beispiele:
  Der Quicksort-Algorithmus mit zufälliger Wahl des Pivotelementes ist ein determinierter, aber nicht deterministischer Algorithmus, da sein Ergebnis bei gleicher Eingabe immer dasselbe ist (eine korrekte Sortierung), der Weg dorthin jedoch zufällig erfolgt.

  Nicht jeder deterministische Algorithmus ist determiniert:

  .. code-block:: cpp

    int randomInt(){
      return(randomInt()+5)*3; // deterministisch aber nicht-determiniert
    }

  nicht jeder determinierte Algorithmus ist deterministisch.

  .. code-block:: cpp

    int f(){
      return randomChhoice() ? 1:2; // nicht deterministisch, aber determiniert
    }


Charakteristiken von Algorithmen:
_________________________________

  - **Beschreibug** : Zu jedem Problem gibt es zahlreiche Algorithmen, welche wiederum unterschiedlich implementiert werden können.
  - **Ausdrucksfähigkeit**: hängt von der Wahl der Sprache ab (Formale Sprachen benötigen Maschienenüberprüfbare Grammatik)
  - **Berechenbarkeit** : Fast nichts ist berechenbar -> Menge der Algorithmen ist abzählbar, da die Länge endlich sein muss (Programme nach Länge und lexicographisch sortieren); jedoch ist die Menge der Funktionen (mit Argumenten und Worten in N) überabzählbar. Es gibt nichtentscheidbare Probleme wie zum Beispiel das Halteproblem (Entscheidung, ob ein Algorithmus terminiert)
  - **Korrektheit**: Fehler in Programmen und deren Folgen nehmen immer weiter zu. Ein Maß für die Komplexität einer Software gibt der Anteil des Codes mit Nestinglevel >= 2 an: (Lines of Code)/(Lines of Code with Nestinglevel >=2). Die Ursache für Fehler ist oft Programm Comprehension und je größer das Nestinglevel, desto schwerer ist der Code zu verstehen.
  - **Zeitbedarf**: Algorithmen werden mit Laufzeitanalyse untersucht und in Asymtopische Komplexitätsklassen eingeteilt

    - *tracable problems*: O(log N) (sublinear), O(N), O(N Log N) (nearly linear), O(N^k) bei kleinem k
    - *intrackable problems*: O(N!), O(N^N), O(k^N) bei k > 1


Entwurfstechniken für Algorithmen
__________________________________

.. _ref-struktogramm:

Struktogramme
~~~~~~~~~~~~~

.. figure:: struktogramm-elemente.png
	 :alt: Struktogramm-Elemente


.. figure:: struktogramm_staerken_schwaechen.png
	 :alt: struktogramm_staerken_schwaechen

Flowcharts
~~~~~~~~~~

.. figure:: flowchart_ggt.png
	 :alt: flowchart_ggt

.. figure:: flowchart_elements.png
	 :alt:    flowchart_elements

UML-Aktivitätsdiagramme
~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: aktivitaetsdiagramm_ggt.png
	 :alt: aktivitaetsdiagramm_ggt

Aktivitätsdiagramme sind Modellierungskonstrukte der Unified Modelling Language(UML) zur abstrakten, graphischen Darstellung von "Verhalten". Hier werden Aktionen vernetzt und so Aktivitäten und deren Reihenfolge sowie Ablauffolge (z.B. Kontrollfluss, Datenfluss) dargestellt. Aktivitätsdiagramme unterstützen objektorientierte Programmierkonstrukte und sind semanitsch nah an Petri-Netzen
