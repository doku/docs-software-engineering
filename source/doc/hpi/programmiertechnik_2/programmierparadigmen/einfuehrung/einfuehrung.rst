=====================
Programmierparadigmen
=====================

"Ein Paradigma ist eine grundsätzliche Denkweise, ein Muster für ein grundsätzliches Herangehen oder Lösungsrahmenwerk. Im engeren Sinn ist es eine bestimmte Lehrmeinung oder Weltanschauung"[de.wikipedia]

"... a paradigm is a distinct set of concepts or thought patterns, including theories, research methods, postulates and standards for what constitutes legitimate contributions to a field"[en.wikipedia]

.. figure:: cartoon.png
	 :alt: cartoon

- Ein Programmierparadigma bezeichnet die gedankliche, konzeptionelle Grundstruktur, die der Entwicklung einer Lösung mit einer Programmiersprache zugrunde liegt.
- Grundlage eines Programmierparadigmas bildet eine mathematische Theorie oder eine andere Formalisierung
- Ein Programmierparadigma formuliert einen Satz kohärenter Konzepte, nach denen ein Programm entwickelt werden kann.
- Programmiersprachen unterstützen ein oder mehrere Programmierparadigmen

**Unterstützung eines einzelnen Programmierparadigmas:**

  - Bereitstellung von Mitteln für ein einzelnes Paradigma innerhalb einer Programmiersprache
  - Beispiel Haskell: funktionale Programmierung

**Unterstützung mehrere Programmierparadigmen:**

  - Bereitstellung von Mitteln für mehrere Paradigmen innerhalb einer Programmiersprache
  - "the idea of a multiparadigm language is to provide a framework in which programms can work in a variety of styles, freely intermixing constructs from different paradigms" [T. Budd]
  - Beispiele C++, Java, C#

.. figure:: programmier_paradigma.png
	 :alt: programmier_paradigma

.. figure:: programmiersprachen.png
	 :alt: programmiersprachen

Deklarative
-----------

Berechnungen erfolgen implizit, indem zuvor Regeln erstellt werden, die die Auswertung von Ausdrücken ermöglicht. Es wird kein "Lösungsweg" vorgegeben, dieser wird basierend auf definierten Regeln bestimmt (Bsp. CNC Maschine).

  - **Logisch**: Berechnungen erfolgen auf Basis von spezifizierten Regeln und Fakten (logischen Aussagen), die durch Ziehen von Schussfolgerungen ausgewertet werden
  - **Funktional**: Berechungsprozesse beruhen auf geschachtelten oder rekursiven Funktionsaufrufen, die ohne Seiteneffekte (Zustandsänderungen bei Variablen) arbeiten. In der Funktionalen Programmierung gibt es keine Variablen sondern nur temporäre Mengen

Imperativ

Objektorientiert
----------------

Berechnungen beruhen darauf, Methodenaufrufe zwischen Objekten zu versenden, die über einen gekapselten Zustand und ein klassenspezifisches Verhalten verfügen; Klassen können spezialisiert werden, um Zustand und Verhalten zu verfeinern.

Nebenläufig
-----------

Berechnungen werden in Form nebenläufiger Prozesse organisiert.

Reflektiv
---------

Programme manipulieren ihre eigenen STrukturen und Spezifikationen.

Generisch
---------

Berechnungslösungen werden bezüglich Datentypes und Funktionen parametrisiert und damit wiederverwendbar, um Klassen von Probleen effizient lösen zu können.

Genetisch
---------

Ergebnisse werden mit Millionen von unterschiedlichen Parametern bestimmt und die Ergebnisse anschließend mit einer Bewertungsfunktion bewertet. Man such nun Parameter, die zu einer best möglichen bewertung führen
