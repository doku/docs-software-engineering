Programierparadigmen
====================


.. toctree::
   :maxdepth: 2
   :glob:

   einfuehrung/*
   imperative_programmierung/*
