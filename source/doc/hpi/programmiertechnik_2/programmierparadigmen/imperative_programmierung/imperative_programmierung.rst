Imperative Programmierung
=========================

In der imperativen Programmierung wird mit Hilfe von Anweisungen(statements) der Programmzustand(program state) durch Modifikation von Speichervariablen verändert.
Imperative Kommandos **legen genau fest, wie etwas berechnet werden soll**, was im Gegensatz zur Deklarativen Programmierung steht, in der lediglich festgelegt wird was berechnet werden soll.
Imperative Programme definieren **Folgen von Anweisungen die abgearbeitet werden und zu Seiteneffekten** auf Speichervariablen führt.
Die Anweisungen spiegeln native Maschineninstruktionen wieder.

.. note::
  In a nutshell: **Wer will was von wem woraus?**
