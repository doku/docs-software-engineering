Object orientation in C++
=========================

Inheritance has an accumulating behavior, as the functionality from the parent classes is added together.

object orientated modelling
---------------------------
Important tools for modelling

"is a" Relation
"has a" Relation

.. code-block:: C++
    :linenos:

    class A{
      B b_;
      vector<B> ba_;
    }

"uses" Relation

.. code-block:: C++
    :linenos:

    class A{
    B* db;
    }

public, private and protected inheritance
-----------------------------------------

test

.. code-block:: cpp
    :linenos:

    class D{
     int m_;
     int n_;
     public:
     D(): m_{0},n{0}{};
     int n()const{return n_;};
     void setN(int m){if(m_<0){m_++; n_ = n*n;}}
     int* bad(){// bad returning pointer of private variable -> can access memory directly which can be used to access private variables
     return &n_; // return &this-> n_; would be equivalent
     }
    }
    # public attributes stay public -> are accessible from the outside - like "is a" Relation
    # private attributes form D are technically "in there" as well but not accessible
    # necessary for casting to parent class
    # from a semantic point of view exactly what you would e
    #
    class A: public D{
     int k_;
     public:
     B():k{0}{}; // B():A,k{0}{}; -> compiler adds call of the default constructor of A
     int k() const {return k_;};
     void g(){k_ = k_ +n(); // cant access attributes directly but via getter and setter
     // k_ = m_ -> Error
     // A::m_ --; -> Error
     setN(k_/2);
     this->setN(k_/2);
     A::setN(k_/2);

     }
     A a;
     a.D::k();
     a.m_ = 0 // Error
     a.k: = 0 // Error: not in scope of class
    }
    class x{
      int n_;
      public:
       void f();
    }
    // private Inheritance is the exception, not very frequently used
    class Y: private X{
      public:
      void g();
    }
    Y y1;

    // everything the parent provides can be accessed internally but is not in the public interface from the child class
    // sometimes useful but not so often
    class Stack: protected List{

      void push(int x){
        append(x); // List method which is not accessible from the outside
      }
      }


Up Casting
----------


virtual
-------

.. code-block:: cpp
  :linenos:

   Class A{
   virtual void f(); // = 0 ?
   public:
   }
   class B: public A{
    virtual f();
   }
