Classes
=========

A class is a user-defined type provides to represent a concept in the code of a program. Classes encapsulate the state and functionality of an object(Information hiding)

three important kinds of classes:

  - concrete classes
  - abstract classes
  - Classes in class hierachies

Concret classes
---------------
