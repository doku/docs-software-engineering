Types, Variables and Arithmatic
===============================

Every name and every expression has a type that determines the operations that may be performed on it. The *declaration* is a statement that introduces a name into th program. It specifies a type for the named entity:

- A type defines a set of possible values and a set of operations (for an object)
- An Object is some memory that holdes a value of some type
- A value is a set of bits interpreted according to a type
- A variable is a named object

C++ offers a variety of fundamental types:

.. code-block:: cpp

  bool // (1)
  char // character (1)
  int // integer (4)
  double, float  // (double-precision) floating point number (8),(4)
  unsigned // non-negative integer

Each fundamental type corresponds directly to hardware facilities and has a fixed size that determines the range of values that can bes stored in it. These sizes are multyplies (Vielfache) of the *natural size* on a given maschine, which is typically an 8-bit byte. The size of a type is implementation-defined (can vary among different maschines) and can be obtained by the **sizeof** operator. Usual sizes of the types is given in brackets().

The arithmatic operator can be used in appropriate combinations of these types, as well as the comparison operators. Furthermore, logical operators are provided:

.. code-block:: cpp

  x&y // bitwise and
  x|y // bitwise or
  x^y // bitwise xor
  ~x // bitwise complement
  x&&y // logical and
  x||y // logical or

A bitwise logical operator yields a result of their operand type, for which the operation has been performed for each bit. The logical operators && and || simply return true or false.

In assignments and in arithmetic C++ performs all meaningfull conversions between the basic types (more in the chapter chasting), so that types can be mixed freely. The conevrsions used in expressions are called *the usual arithmatic conversions* and aim to ensure that expressions are computed at the highest precision of its operands. For example, an addition of a double and an int is calculated using double floatingpoint precision arithmatic. C++ offers a variety of notations for expressing initialization, such as = and a universal form based on curly-brace-delimited initializer lists. The = form is traditional, but if in doubt, use the general {} -list form. If nothing else, it saves you from conversions that lose information.

.. code-block:: cpp

  double d1 = 2.3;
  double d2 {2.3};
  vector<int> v {1,2,3,4};
  vector<int> v = {1,2,3,4}; // = is optional with {}
  int i1 = 7.2; // i1 = 7
  int i2 {7.3}; // error: floating-point to integer conversion
  const int i = 3; // constants allways have to be initialized?



auto
-----
When defining a variable, you dont need to state its type explicitly, when it can be deduced from the initializer:

.. code-block:: cpp

  auto b = true; //bool b
  auto i = 123; //int i instead of short
  auto z = sqrt(i); // z has the type of whatever sqrt(z) returns

With auto we use = because there is no potential troublesome type conversion. We use **auto** where there is no explicit reason to give the explicit type, such as improved readability in large scopes or specification of variable range (double rather than float).
We use auto to avoid redundancy and writing long type names. This is especially important in generic programming where the exact type of an object can be hard to determine for the programmer and very long.

If you want an const auto variable you need to state it explicitly. Otherwise it wont be const

More Deatils https://stackoverflow.com/questions/5070521/does-const-auto-have-any-meaning

.. code-block:: cpp

  const float foo2(const float test) {
  	return test*2;
  }

  auto test2 = foo2(test); // type of test 2 is float not const float
