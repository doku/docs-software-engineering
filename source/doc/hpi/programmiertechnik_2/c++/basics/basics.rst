Basics
======

C++ is a compiled language. For a Programm to run, its sourcecode has to be processed by a compiler, producing object file, which are combined by a linker yielding an execuatble program. A C++ program typically consists of any source code files. An executable Programm is created for a specific hardware/system combination; it is not portable.

The ISO C++ standard defines two kinds of entities:

- **Core language features** such as build-in data types (int, char, ...) and controlstructures (for, if...)
- **Standard-libary components** such as containers (vector, map, ...) and I/O operations (<<, getline()) -> the stl is is implemented in ordinary C++

Getting started
---------------

minimal c++ programm: int main(){} -> no return value to the system indicated successfull execution.

see functions and types_variables_arithmetics for more details

Scope and Lifetime
------------------

A declatation introduces its name into a scope:

  - *local scope*
