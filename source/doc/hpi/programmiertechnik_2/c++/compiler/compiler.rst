========
Compiler
========

Warnings
--------

The compiler warns you if casting could result in data lost


.. code-block:: cpp

  const float foo(float test) {
  	int quadrat = test*test; // Warning possible data loss
  	return quadrat;
  }
