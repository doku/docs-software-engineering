Lamdas
======

.. code-block:: cpp
  :linenos:

  std::vector<int> v = {1,2,3,4};

  std::for_each(v.begin(), v.end(), [](int &i){std::cout << i << std::endl;})

  auto print = [](int &i){std::cout << i << std::endl;};

  std::for_each(v.begin(), v.end(), print);

  for(auto it = v.begin(); it != v.end(); it++){
    std::cout << *it <<std::endl;
  }

  for(int &i :v){
    std::cout << it <<std::endl;
  }

  //=====================

  [capture list](parameter list){function body}

  int x = 3;

  auto pos = std::remove_if(a.begin(), a.end(), [x](int i){return x < i});
  // remove if enfernt selbst keine Elemente aus dem Vector sondern schiftet zu entfernende Elemente(dort wo true zurückgeliefert wird) ans ende des Vectors
  // v = {3,4,1,2} und iefert als pos die Position des ersten zu entfernenden Elementes zurück
  a.erase(pos, a.end());
