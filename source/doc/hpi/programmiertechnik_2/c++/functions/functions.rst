Functions
=========

The main way of getting something done in c++ is to call a function to do it. A function cannot be called unless it has been previously declared. A function devlaration specifies the name of the function, the type of the return value and the number ant types of the arguments that mus be supplied in a call.

.. code-block:: cpp

  Element* next_element();
  void test(int);
  double sqrt(double):

When calling a function argument types are checked at compile-time and if necessary implicitly type converted or if not possible a compiler error occures.

.. code-block:: cpp

  double d1 = sqrt(2); // implicit type conversion to 2.0
  double d2 = sqrt("three") // error: sqrt() requires an argument of type double

For class member fuctions the name of the class is also part of the function type

.. code-block:: cpp

  char& String::operator[](int index);

Overloading
-----------

If two functions are defined with the same name, but with different argument types, the compiler chooses the most appropriate function for each call. If two alternative functions could be called, but neither is better than the other, the call is ambiguous an the compiler gives an error.

.. code-block:: cpp

    void print(int, double);
    void print(double, int);
    void test(){
      print(0,0); // error: ambiguous
    }

When a function is overloaded, each function with the same name should implement the same semantics
