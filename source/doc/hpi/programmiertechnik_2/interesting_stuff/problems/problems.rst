====================
Interesting Problems
====================

Halteproblem
------------

.. code-block:: cpp

  bool haltTest(in P){
    return P hält ? true : false;
  }

  void f(in P){
    	if(haltTest(P) == true) while(true);
      else return;
  }
  // Was ist f(f()) ->Logischer Widerspruch

Collatz-Problem
---------------
Sei *n* eine natürliche Zahl > 0 und die Zahlenfolge c wie folgt definiert:
c0 = 0
ci+1 = 3ci +1, falls ci ungerade
ci+1 = ci/2, falls ci gerade
Terminiere, falls ci = 1

Collatz-Vermutung
  "Jede so konstruierte Zahenfolge mit beliebigem n mündet in dem Zyklus 4,2,1"
ist bis heute unbewiesen (Idee: Rückwärtsbeweis, zeigen, dass von 1,2,4 jedes n erreicht werden kann)
