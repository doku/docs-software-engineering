Object Orientation
==================

Objects
-------
Objects as models of real world entities
Objects as cells

Features of Objects
###################

  - Messaging
  - Aggregation
    - Objects can be created over-and-over
    – Objects can be contained by other objects
  - Encapsulation
    - Object internals inaccessible from outside
  - Inheritance or delegation
     - Objects can get structure (data) and behavior (methods) from another

.. figure:: object_identity_behavior_state.png
	 :alt:   object_identity_behavior_state

Green: priority in the web context
Orange: Priority for Smalltalk
Original order by priority in C++



Identity
  –Objects can be identified
  –To distinguish one object from the other
Behavior
  –Response to messages received
  –Implemented via methods
State
  –Other objects it contains or refers to

.. figure:: messanges.png
	 :alt: messanges

Behavior
--------
describes what is done

.. figure:: behavior.png
	 :alt: behavior

Relationship
------------

Difference Relationship, Attribut

primitive datentypen: Attribute
komplexe Datentypen: Relationships


Encapsulation
-------------
.. figure:: encapsulation.png
	 :alt: encapsulation






Activities (not phases)

.. figure:: software_development.png
	 :alt: software_development
