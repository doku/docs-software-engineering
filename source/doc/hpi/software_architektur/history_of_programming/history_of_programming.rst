History of Programming
======================

Software crisis
---------------

High complexity of providing understandable, correct, and verifiable programs
result in project management and maintenance problems.
Positive tendencies: Improved methodologies, tools, and libraries
Negative tendencies: Exponential increase of functionality

What is good software?
----------------------

- External criteria: What clients expect from a solution (Demand behavior, not a particular implementation)
  - **Maintainability, Comprehensibility, Correctness, robustness, efficiency**
- Internal criteria: How a solution meets clients’ expectations ( Not only what, but also how (and why))
  - **Information hiding** : System boundaries to restrict change
  - **High cohesion and low coupling**: Create entities with a well-defined meaning, Use interfaces to decouple entities
  - **Separation of concerns**:  Consider different concerns independently

Software evolution
------------------


.. figure:: language_genealogy.png
	 :alt: language_genealogy

.. figure:: language_models.png
	 :alt: language_models
Assembler
_________

The Value of Subroutines

- Code reuse
  - Duplication avoided (penalty of call/return)
  - Enables recursion!
- Code maintenance
  - Fix a bug: Adapt code once, for all callers

Structured programming (1964-1975)
__________________________________

- Control structures in the language for better abstraction

Modular Programming
___________________

- Abstraction mechanisms
  -  From the outside only the module abstraction is visible
- Separation of concerns
  - Organize code according to common functionality

Object oriented programming
___________________________

Objects scale well
- Complexity
  - Distributed responsibility
- Robustness
  - Independent
- Growth
  - Same mechanism everywhere
-Reuse
  - Provide services, just like in the real world

Features of Objects
###################

- Messanging
- Encapsulation
  - Object Internals are inaccessible from outside
- Aggregation
  - objects can be contained by other objects
- Inheritance or delegation
  - Objects can get structure (data) and behavior (methods) from another



Sidenotes
---------

- GOTO considered harmful
- Proper choice of data structure does half the work
- Rich libraries yield short programs
