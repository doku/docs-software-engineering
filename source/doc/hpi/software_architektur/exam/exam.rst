Exam
====

OO-Programming
--------------
class, instance, messages

Software pattern
----------------
general
language ideoms - programming examples judge
design patterns - similarities, differences, roles, object structure, adapt systems

state - strategy


Points
------

main part 70 points

old Exams
---------

########################### Fragen WS 2010/11 ###########################

- Adapter, Decorator, Bridge - Unterschiede im Diagramm

- irgendwas zum Layer System-Design

x- Stack in Squeak implementieren

- Temperaturfühler Beispiel in Squeak (Observerpattern)

- Problem im Code das auf Nil getestet werden muss -> NULL-Object-Pattern(war meine Lösung)

- eine kurze Frage zu Frameworks (bin ich mir nicht sicher)

- codesmells finden (Kaskadierung fehlt, Magic Number...)

- crc karten (pipes and filters)

- 5 modularitäts begriffe



########################### Fragen WS 2011/12 ###########################

- allgemeine Fragen zur Objektorientierung (Konzepte, mussten keine Erklärungen abgeben)

- interne/externe Kriterien für Modularität (einfach nur zuordnen)

- detaillierte Erklärungen zu Uniform Access

x- Queue in Squeak implementieren

- Codesmells finden und begründen

- Vergleich von je 2 Codeschnipseln: welcher ist zu bevorzugen und warum?

- ein Composite Pattern (Beispiel Baum gegeben) durch einen Visitor erweitern

- crc Karten für MVC



########################### Fragen WS 2012/13 ###########################

- interne, externe Kriterien zuordnen

- ein Kriterium erklären, warum wie zugeordnet
- 3 Eigenschaften von Objekten nennen

- Polymorphimus definieren

- Stacks mit OrderedCollection, OrderedCollection-Methoden angegeben

- Abgrenzung Idiom vs. Pattern vs. Architekturstil

- Code Smells finden anhand von Idiomen erklären

- Eine Variante aus 2 Alternativen von Quelltexten auswählen und begründen


- 3 Vorteile von Modularität nennen
- Eine Modularitäts-Regel erklären mit Verweis auf zugehörige Kriterien

- Single Choice Principle erklären, zugehöriges Pattern nennen

- Observer implementieren mit Temperatursensor
	- Methoden ergänzen
	- Quelltext schreiben
	- Aufruf

- CRC von Caching

- Unterschiede zwischen Caching und Pooling bzgl. Erhalt von Objektidentität und Zustand der Ressourcen

- Inversion des Kontrollflusses bei Frameworks erklären

- White Box, Black Box, Gray Box Framework

- wo tritt Composition auf


Zusatzaufgabe: Vergleich Decorator, Adapter, Bridge
  (Rollennamen eintragen + Verantwortlichkeit angeben: Client oder Bibliothek)


########################### Fragen WS 2013/14 ###########################

- Stack implementieren auf Ordered Collection
- welches Pattern? (Adapter)

- Composite Baum mit Managern und Employees war gegeben und es sollte eine Methode Einkommen in Smalltalk implementiert werden

- CRC von Lookup

- Code Smells

- Verletzte Idioms finden und erläutern (Zur Wahl standen Kaskade, Magic Number, …)

- Welches Framework wird hauptsächlich durch Vererbung angepasst (White-Box-Framework)
- Frameworks/Libraries -> Wo liegt das Hauptprogramm, welcher Teil wird vom User implementiert
- Inversion des Kontrollflusses erklären

- Abgrenzung Pattern, Architektur, Idiome

- interne, externe Kriterien, identisch mit PDF von 12/13

- Unified Access erklären

- Unterschied Methode, Nachricht, Method Activation

- wie kann ifNil verhindert werden? (Null Object + Vor- und Nachteile nennen)


######################## Fragen WS 2015/16 ##################################

interne/ externe kriterien
eins genauer beschreiben

Unterschied Idiome Pattern Architekturstile mit Beispiel

Uniform Access

Bindung und Kopplung (was ist es und warum ist es wünschenswert erkläre mit modularitäts kriterien regeln und prinzipien)

hauptmerkmale eines objektes

zusammenhang und erklärung von objekt und klassse

crc cards mvc

Null-Objekt Pattern (Wie würde es implementiert werden am Beispiel)

temepraturfühler (Observer Pattern)

code smells

-auswahl zweier implemetierungsvarianten

frameworks, unterschied whitebox blackbox, vorteile nachteile, entwicklungsstufen

zusatz
chache vorteille nachteile
pooling caching vergleichen
