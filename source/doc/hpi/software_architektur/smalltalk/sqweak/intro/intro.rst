Intro
=====

operatoren werden von links nach rechts ausgewertet

Strg p

Examples
--------

1 negated
5 raisedTo: 2
7 + -5 raisedTo: 4 -2 negated  -> 2 raisedTo

3 verschiedene Nachrichten arten :
  - unär
  - binär
  - keyword
werden in der angegeben Reihenfolge ausgeführt, bei gleicher Priorität streng von links nach Rechts
kann geklammert werden

\|y\| temporary variable

= strukturvergleich
== Objektvergleich -> Verneinung: ~~

Punkt is wie Semikolon in anderen Sprachen

^ return

Unterscheidung zwischen Methoden
- Zustandsverändernde
- Wertzurückgebende


Abstrakte Klasse
----------------
Eine Klasse ist abstrakt  sobald sie mindestens eine abstrakte Methode hat.
Methoden können durch

self subclassresponsibility

als abstrakt definiert werden
