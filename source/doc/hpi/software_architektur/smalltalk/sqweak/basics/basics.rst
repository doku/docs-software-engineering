Squeak Basics
=============



Creating a class
----------------
All class names must be **unique** in Squeak. One cannot have a class in both category "a" and "b" called "Foo" - categories do not provide namespacing.
All class names must be **capitalised**.

Categories are displayed in the leftmost pane of the browser, with names like "Kernel-Objects". To create a new category, middle-click on the category list, to bring up a small menu with the item "Add item"; select that and you will be prompted for the name of your category.

To create a class, click on a category, and notice that the text in the bottom pane has changed to a weird object declaration with first line
 Object subclass: #NameOfSubclass
If you change "#NameOfSubclass" to "#MyNewCapitalisedName", then middle-click and choose "Accept" (or do Alt-s), you should find that your category has now acquired a class named "MyNewCapitalisedName". Let's assume that you chose the name "Foo".

InstanzMethoden, die "initialize" heißen werden von Konstruktor aufgerufen


Adding methods
--------------

Once again, one adds methods by altering a prepared template. To access this, click on a class to show its method categories, then click on a category, which will show you a template. Replace the first line with "method_name[: arg1_name second_part_of_method_name: arg2_name ...]"; that is to say, replace it with the method name, then if it takes an argument, separate the name of the argument from the method name with a colon, and subsequent arguments require that the method have multiple parts to its name.

The rest of the template should be fairly self-explanatory, but one thing is worth mentioning now: **if you don't explicitly return a value from a method, it will return the object upon which it was called** (this is useful for chaining otherwise value non-returning method-calls together). To explicitly return a value, use the "^" operator, e.g.:
 ^'Shoes'
will return 'Shoes' from the method.

If it annoys you that your method is in the "As yet uncategorized" category, then you can add a category by middle-clicking on the category pane (The third pane from the left), and hitting "add category"; once you have a category, you can change the category of a method by (middle) clicking upon it in the method pane (fourth pane) and choosing "Change category".

Methods without any arguments don't have a **:**


Inheritance and polymorphism
----------------------------

Smalltalk is a **single-inheritance language**, with the usual semantics that members and method implementations are inherited by subclasses. As the method invocation semantics are by-name, bound at invocation-time, on untyped variables, Smalltalk does not require a way of defining interfaces, as provided in other single-inheritance languages.

To extend another class, simply modify the class-creation template used before. The first line, by default reads "Object subclass #Foo"; to make Foo a subclass of Boolean, change "Object" to "Boolean", and accept the changes. In fact, one can do this to any of the elements in the class definition, such as the category.
