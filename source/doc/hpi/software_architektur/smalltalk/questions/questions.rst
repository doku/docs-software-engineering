What does it do?
================



.. code-block:: ruby

  self assert: false equals: (true and: [false]) "Difference to true and: false?"

  test08blocksAndConditions
	"You might have noticed that we use blocks for and: and or:.
	 Maybe you might want to take a moment to reflect on the reasons for that."

	self assert: true equals: ((1 + 1 = 2) or: [[1+2] repeat])


  test05statementsInAssignments
  | a b |
	a := 1.
	b := 2.
	false
		ifTrue: [a := 3]
		ifFalse: [b := 3].
	self assert: 1 equals: a.
	self assert: 3 equals: b

  test09loopExecution

	| a |
	a := 2.
	[a < 10] whileTrue: [a := a * a].
	self assert: 16 equals: a

.. code-block:: ruby

  "Even 'statements' are implemented as methods.
	Always remember: (Almost) Everything is a message send!"

	| expected |
	expected := true.
	self assert: expected equals: (Boolean canUnderstand: #or:).

symbols
-------

.. code-block:: ruby

  test04methodNamesBecomeSymbols
	"If you want to crack a hard nut think about this: Why do we convert
	the list of symbols to strings and then compare against the string value
	rather than against symbols?
  	Hint: How is the set of all symbols affected when you change the
	source code to use symbols?"

	| symbolsAsStrings |
	symbolsAsStrings := Symbol allSymbols do: [:x | x asString].
	self assert: true
		equals: (symbolsAsStrings includes: 'test04methodNamesBecomeSymbols')
