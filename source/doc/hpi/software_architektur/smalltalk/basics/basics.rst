Smalltalk Basics
================

General
-------

Difficulties: developing an interpreted language that is fast enough

Programming
-----------

.. code-block:: ruby

  self assert: true. "Kommentar"
  self assert: expectedValue equals: actualValue

Programm
L.
L.
L

Everything is an Object
#######################

.. code-block:: ruby

  self assert: true equals: (21 isKindOf: Object).
  self assert:  true equals: (Smalltalk isKindOf: Object)


	"Remember: Everything is an Object, even classes.
	The ancient button of implementors might help to find the true nature of hash."

	| anObject | "you need to state all used local variables at the beginning of an method"
	anObject := Object new. "creates a new object of the type Object"
	self halt. "pauses the program for debugging"
	self assert: SmallInteger  equals: anObject hash class


equality
########

.. code-block:: ruby

	"There are two sides to equal."

	| anObject anotherObject |
	anObject := 'abc'.
	anotherObject := String newFrom: anObject.
	self assert: true equals: anObject = anotherObject.
	self assert: false equals: anObject == anotherObject

  "= checks Structural equality
  == checks equality
  ~~ checks id equality of objects"


	anObject := Object new.
	anotherObject := Object new.
	self assert: xx equals: anObject ~~ anotherObject


  test05butNotSmallIntegers
	"Now think about this: What is the implication for the result of the
	identity check on small integers?"

	| aNumber anotherNumber |
	aNumber := 1.
	anotherNumber := 1.
	self assert: false equals: aNumber ~~ anotherNumber


  test06copyObjectsWithNewFrom

	| anyObject aNewObject |
	anyObject := Object new.
	aNewObject  := Object newFrom: anyObject.
	self assert: true equals: anyObject ~~ aNewObject


  test07useCascadesToNotRepeatButBeCareful

	| result |
	self halt.
	result := 'abc'
		reversed;
		asUppercase.

	self assert: 'ABC' equals: result.
	result := Set new
		add: 1;
		add: 2.
	self assert: false equals: result isCollection


  TODO test08


propositional logic
-------------------

.. code-block:: ruby

  self assert: True equals: true class.
  self assert: false equals: (true and: false)
  self assert: true equals: (true or: [false])
  self assert: true equals: false not.
	self assert: true equals: false ==> [true]

other functions
---------------

.. code-block:: ruby

  value	"Evaluate the elements of the receiver and answer the result of last evaluation."

BlockClousures
--------------
.. note::Blocks are like Lamdas and always return the value of the last block

.. code-block:: ruby

  | multiply |
	multiply := [:a :b | a * b].
	self assert: 6 equals: (multiply value: 2 value: 3).
	self should: [multiply value: 1] raise: Exception "arguments need to fit"


  "you can even use temporary variables"
  | processing |
	processing := [:a :b |
		| tempVariable |
		tempVariable := a + b.
		tempVariable * tempVariable].
	self assert: xx equals: (processing value: 4 value: 2)

  "This is part of the buggy way of the art of Smalltalk.
	The value of the last statement of a block is returned.
	Always the last!"

	| processing |
	processing := [:a :b | a. b].
	self assert: false equals: (processing value: true value: false).
	self assert: true equals: (processing value: false value: true)


statements
----------

.. code-block:: ruby

	"All statements take blocks as arguments.
	the blocks will only be evaluated when necessary."

	| a b |
	a := 1.
	b := 2.
	true or: [a := b. true].
	self assert: 1 equals: a.
	a := 1.
	b := 2.
	false or: [a := b. true].
	self assert: 2 equals: a


  "Statements can be execuded with conditions"
  | a b |
	a := 1.
	b := 2.
	false
		ifTrue: [a := 3]
		ifFalse: [b := 3].
	self assert: 1 equals: a.
	self assert: 3 equals: b

  "use implications to controll Statements"
  | a |
	self assert: false equals: true ==> [false].
	self assert: true equals: false ==> [false].
	a := 1.
	true ==> [a := 2].
	self assert: 2 equals: a.
	a := 1.
	false ==> [a := 2].
	self assert: 1 equals: a


Strings
-------

.. code-block:: ruby


  self assert: true equals: ('Hello World' isKindOf: Collection).
	self assert: $H equals: 'Hello World' first.
	self assert: $ equals: ('Hello World' at: 6).
	self assert: 'o W' equals: ('Hello World' copyFrom: 5 to: 7).
	self assert: 'World' equals: ('Hello World' last: 5)

  "Escapeing"
  self assert: $e equals: ('I''m here' at: 6)

  "Concatenation"
  | a b |
	a := 'Hello World'.
	self assert: a equals: 'Hello' , ' ' , 'World'.
	b := String streamContents: [:stream |
		stream
			nextPutAll: 'Hello';
			nextPutAll: ' ';
			nextPutAll: 'World'].
	self assert: a equals: b

  "New Lines are saved in a String"

  | a |
	a := 'Hello
        World'.
	self assert: a equals: 'Hello' , Character cr asString , 'World'

  'String formating'

  | a b c |
	a := 'Hello World'.
	b := '{ } \ foo 12 bar string'.
	c := '{ } \\ foo 12 bar string'.
	self assert: 'Hello World'equals: ('Hello {1}' format: {'World'}).
	self assert: '{ } \ foo 12 bar string' equals: ('\{ \} \\ foo {1} bar {2}' format: {12. 'string'})


Symbols
-------

.. note:: symbols are unique and imutable special strings
  method names become symbols, symbols can be made from strings

.. code-block:: ruby

  | symbol |
	symbol := #Smalltalk.
	self assert: true equals: symbol isSymbol

  | symbol1 symbol2 symbol3 |
	symbol1 := #aSymbol.
	symbol2 := #aSymbol.
	symbol3 := #somethingElse.
	self assert: true equals: symbol1 == symbol2.
	self assert: false equals: symbol1 == symbol3

  | symbol1 symbol2 |
	symbol1 := #aSymbol.
	symbol2 := #aSymbol.
	self assert: true equals: symbol1 == symbol2 "identical symbols are Identical Objects"

  | string |
	string := 'Cats and dogs'.
	self assert: #'Cats and dogs' equals: string asSymbol

  "Symbols are special strings"
  | symbol |
	symbol := #Smalltalk.
	self assert: true equals: symbol isString.
	self assert: true equals: symbol = 'Smalltalk'

  "Symbols are immutable"
  | symbol |
	symbol := #notAString.
	self should: [symbol at: 1 put: 'a' ] raise: Exception

  "Symbols can be dynamically created"

  self assert: #catsdogs equals: 'cats' , 'dogs' asSymbol

Collections
------------

.. note::Collection functions: includes: , anySatisfy: , allSatisfy:, select: , reject: , detect:, collect:
  lots of math functions

.. code-block:: ruby

  | flowers |
	flowers := {#rose . #lily . #bindweed}.
	self assert: (flowers includes: #rose)

  | smallNumbers |
	smallNumbers := {1 . 2 . 3 . 4 . 5 . 6}.
	self assert: true equals: (smallNumbers anySatisfy: [:aNumber | aNumber > 3]).
	self assert: false equals: (smallNumbers anySatisfy: [:aNumber | (aNumber raisedTo: 2) > 36])
  self assert: false equals: (smallNumbers allSatisfy: [:aNumber | aNumber > 3]).
	self assert: (smallNumbers allSatisfy: [:aNumber | aNumber > 0])

  | smallNumbers |
	smallNumbers := {1 . 2 . 3 . 4 . 5 . 6}.
	self assert: {4 . 5 . 6} equals: (smallNumbers select: [:aNumber | aNumber > 3]).
	self assert: {2 . 4 . 6} equals: (smallNumbers select: [:aNumber | aNumber even])
  self assert: {1 . 2 . 3} equals: (smallNumbers reject: [:aNumber | aNumber > 3] )

  "Collections have some convenience functions for mathematical operations."

	| smallNumbers |
	smallNumbers := {2 . 3}.
	self assert: 5 equals: (smallNumbers sum).
	self assert: 3 equals: (smallNumbers max).
	self assert: 1 equals: (smallNumbers range).
	self assert: {4 . 9} equals: (smallNumbers squared)

  detect: aBlock
	"Evaluate aBlock with each of the receiver's elements as the argument.
	Answer the first element for which aBlock evaluates to true."


  | smallNumbers |
	smallNumbers := {1 . 2 . 3 . 4}.
	self assert: 3 equals: (smallNumbers detect: [:a | a > 2]).
	self assert: 1 equals: (smallNumbers detect: [:a | a < 2])

  collect: aBlock

  "Evaluate aBlock with each of the receiver's elements as the argument.
	Collect the resulting values into a collection like the receiver. Answer
	the new collection."

  self assert: {11 . 12 . 13 . 14} equals: (smallNumbers collect: [:aNumber | aNumber + 10]).
	self assert: {false . false . true . true} equals: (smallNumbers collect: [:aNumber | aNumber >2])
