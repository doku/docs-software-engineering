Smalltalk Introduction
======================

How Smalltalk was Implemented
-----------------------------

- Bytecode compiler
  - Machine language for an imaginary computer
- Virtual machine to create the make-believe computer – Invented years earlier by Burroughs – Used in UCSD Pascal, Java, Python, JavaScript, etc.
- Four files needed for this implementation: – Virtual machine (VM) – Image file (bytecode, objects, snapshot) – Sources file (all sources always provided)
– Changes file (added sources by user)


General Information
-------------------

.. figure:: syntax_postcard.png
	 :alt: syntax_postcard

.. figure:: language_constructs.png
	 :alt: language_constructs

.. figure:: syntax1.png
	 :alt:    syntax1

.. figure:: syntax2.png
	 :alt:   syntax2

.. figure:: runtime_architecture.png
	 :alt: runtime_architecture

Smalltalk uses Messages Instead of a Predefined Syntax(like if, etc)


Everything is an object. Operationen bestehen aus Nachrichten, die zwischen Objecten
Immer wenn man speichert, wird der Code sofort akutalisiert

Smalltalk beginnt bei 1 zu Zählen

Nachrichten: unär, binär, n-när (mit Doppelpunkt)

Morph ist ein graphisches Object
Orange dreiecke in Textfeldern zeigen, das Zustand noch nicht gespeichert ist

new erzeugt neues object
openInWorld zeigt dies in der Welt an

Strg + f Suche in den Packages

^ steht für return

:= Variablenzuweisung

alt . hält den aktuellen Prozess an

Smalltalk ist Nachrichtenbasiert, es gibt keine Vorrangregeln somit auch kein Punkt vor Strich

7   "a number"
$z  "a character"
'colourless ideas sleep furiously'   "a string"
#(#tom #dick #harry)  "an array of 3 components"
#(#one 'should shower at least' 3 'times a week')


Project export as sar
---------------------

https://www.hpi.uni-potsdam.de/hirschfeld/trac/SqueakCommunityProjects/wiki/squeak_faq
https://github.com/HPI-SWA-Teaching/Scamper/wiki/How-to-create-a-.sar-file-from-multiple-packages
