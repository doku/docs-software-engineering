====================
Idioms in Smalltalk
====================

Idioms are guidelines and not laws.


Law of demeter
--------------
.. note:: Do not talk to strangers


composition over Inheritance
----------------------------


constructors
------------

eine kanonische variante

comparing
---------

<=>

blocks
------

value with arguments


choosing message
----------------

solve selection with polimorphy

double dispatch
---------------

ich bekomme etwas übergeben und gebe dies und meine Typinformation Weiters

selber nachmachen mit Integer

lazy initialisation
-------------------

im getter mit ifNil zu setzen


Collection

sorted -> sortierte Kopie
sort -> inplace

Fehlerbehandlung
----------------
methoden selbst können am besten mit Fehlern umgehen

ifNone


Creational Patterns
===================

Builder
-------

Split object from its construction.
If a Client wants a Car, it
Difference to Construktor construction.

abstract Factory
----------------
bündelt Konstruktion
