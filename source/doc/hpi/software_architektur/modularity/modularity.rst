Modularity and Principles of Good Design
========================================

What is good software?
----------------------
An software developers task is to hide complexity

external criteria
#################

**perceivable by (end) users**

- **Correctness**: software performs, as defined by specification
- **Robustness** : ability of software to react appropriately to abnormal conditions, complements Correctness
- **Efficiency** software should demand as few  hardware resources as possible
- **Extensibility** ease of adapting software to changes of specification

.. figure:: good_bad_extensibility.png
	 :alt: good_bad_extensibility

**Reusability** is the ability of software elements to serve for the construction of many different applications


internal criteria
#################

  - perceivable by developers, maintainers
  - modularity: Comprehension of separate system parts, Reusability, extensibility
  - readability

external criteria are the essential one, but internal criteria are necessary to achieve them


flexible and robust in inputs and destincitve an restrictive in output

Modularity
__________
Software systems are structured into units called modules chosen to encourage –Extensibility
–Reusability
–Maintainability

Modularity is the degree to which a set of designs (or tasks) is partitioned into components, called modules, that are
–highly dependent within a module,
–nearly independent across modules

.. note::High cohesion, low coupling

.. figure:: modularity_criteria.png
	 :alt: modularity_criteria

- Modular Decomposability
  - divide and conquer
- Modular Protection
  - catch errors and exception within the module, where it is caused, but only if you can sensible do so

The modular structure devised in the process of building a software system should remain compatible with any modular structure devised in the process of modeling the problem domain
Every module should communicate with as few others as possible.
If two modules communicate, they should exchange as little information as possible
modules should state clearly what is done with the data

information hiding / abstraction: Ignore details in favor of the essential, Used in generalization hierarchies
