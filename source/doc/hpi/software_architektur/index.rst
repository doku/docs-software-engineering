Software Architektur
====================

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Contents:

   einfuerung/*
   history_of_programming/*
   modularity/*
   object_orientation/*
   smalltalk/*
   summary/*
   exam/*
