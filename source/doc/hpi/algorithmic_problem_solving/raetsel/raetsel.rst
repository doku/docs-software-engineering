Rätsel
======



Rätsel 1: Eierwerfen
--------------------

Du hast zwei identische magische Eier zur Verfügung und sollst diese nun
testen.
Die Eier haben die Eigenschaft, dass sie genau dann kaputt gehen, wenn
sie aus mehr als Höhe h herunterfallen.
Für deinen Test suchst du dir das nächstbeste Hochhaus mit 100 Stockwerken.
Nun willst du mit so wenigen Würfen wie möglich das
niedrigste Stockwerk herausfinden, welches höher als h liegt.
Wie viele Würfe benötigst du im schlimmsten Fall?

Lösung
######

Mit einem Ei müsste man anfangend bei 0 Schritt für Schritt die Stockwerke nach oben durchgehen.


Rätsel 2: Streichholzgleichung
------------------------------

Lege ein Streichholz um, um eine wahre Gleichung zu erhalten!

VII=I

Lösung:
#######
 Wurzel oder V als Zeichen für den KgV


Rätsel 3: Katzen und Mäuse
--------------------------

3 Katzen fangen 3 Mäuse in 3 Minuten. Wie lange benötigen sie für 100 Mäuse?

Lösung
######

1 Katze fängt eine Maus in 3 Minuten. Sie benötigen also 300 Minuten.

Rätsel 4: Hin und zurück
------------------------

Du fährst mit dem Auto von A nach B mit durchschnittlich 40
km/h. Auf dem Rückweg fährst du durchschnittlich 60 km/h.
Wie schnell bist du im Gesamtdurchschnitt gefahren?

Lösung:
#######
48kmh

Rätsel 5: Hin und schneller zurück
----------------------------------

Du fährst mit dem Auto von A nach B mit durchschnittlich 40
km/h.
Wie schnell muss du im Durchschnitt von B nach A zurückfahren,
damit dein Gesamtdurchschnitt bei 80 km/h liegt?

Lösung
######
Das Auto müsste unendlich schnell sein und würde es immernoch nicht rechtzeitig schaffen. Es ist also nicht möglich

Rätsel 6: Turmuhr
-----------------

Morgens um 6 schlägt die Turmuhr 6 mal
innerhalb von 30 Sekunden.
Wie lange benötigt die Turmuhr für 12
Schläge?

Lösung
######

Annahme: der Schlag an sich benötigt keine Zeit.
Anschließend ist zwischen den Schlägen eine immer gleich lange Pause.
Damit gibt es in den 30 Sekunden 5 Pausen, also 6s pro Pause.
Würde man die 6 Schläge 2 mal direkt hintereinander ausführen gäbe es keine Pause zwischen Schlag 6 und 7.
Es kommt also noch eine 6 Sekündige Pause hinzu. Damit benötigt die Turmuhr 66s für 12 Schläge



Rätsel 7: Flug über den Atlantik
--------------------------------

Du fliegst von Berlin nach New York City und später wieder von
New York City nach Berlin zurück. Auf beiden Flügen ist das
Wetter identisch.
Spielt es eine Rolle, ob es windig ist?

Lösung
######

Durch die Erdrotation nach Osten ist der Hinflug kürzer. Ist der Wind bei beiden Flügen gleich sollte er keine Rolle spielen

Rätsel 8: Münzendrehen
----------------------

Du hast eine Reihe mit Münzen, einige liegen mit "Zahl" nach
oben, andere mit "Kopf".
Kannst du alle auf "Zahl" drehen, wenn du immer nur zwei gleichzeitig
drehen darfst?
Geht es, wenn du entweder 5 oder 7 drehen darfst?

Lösung
######

Durch eine Ungerade Anzahl an Drehoperationen wechselt man immer den Zustand der Münze.
Dreht man immer zwei Münzen gibt es zwei Zustandsänderungen. Bei einer Ungeraden
Anzahl von Münzen, die auf einer Seite liegen wäre es also nicht möglich, sie alle richtig rum zu drehen

Rätsel 9: Schokoladenteilen
---------------------------

Du hast eine hübsch verzierte Tafel Schokolade mit 10 Reihen aus
je 6 Stücken Schokolade. Diese Tafel möchtest du jetzt in ihre 60
Einzelteile zerlegen. Dazu darfst du die Tafel an beliebiger Stelle
entweder quer oder längs brechen.
Wie kannst du optimal so brechen, dass du so selten wie möglich
die Tafel brichst? Dabei darfst du nie mehrere Stücke gleichzeitig
(zum Beispiel durch übereinanderlegen) brechen.
Wie viele Brüche brauchst du, um eine 6×10 Tafel in die einzelnen
Stücke zu zerbrechen? Geht es auch schneller?

Lösung
######

Invariante: Jeder Bruch teilt ein Schokolandenstück in zwei. Damit erhöht sich die Anzahl der Teile mit jedem Bruch um eins. Da man mit einem beginnt und am ende 60 haben möchte benötigt man 59 Brüche.

Rätsel 10 Piratendemokratie
---------------------------

1000 Piraten haben einen Schatz. Den Schatz wollen sie gerecht
verteilen, jeder Pirat bekommt gleich viel.
Jeder Pirat möchte natürlich so viel vom Schatz wie möglich
haben, weshalb, ganz demokratisch, darüber abgestimmt wird ein
paar Piraten zu töten. Alle Piraten haben einen Rang (von 1 bis
1000) wobei 1 der Kapitän ist.
Erst wird über den Tod von Pirat 1000 abgestimmt, dann über Pirat
999 und so weiter, bis mindestens die Hälfte der abstimmenden
Piraten gegen den Tod ist (dann wird aufgehört und der Schatz
gleichmäßig verteilt).
Wie viele Piraten teilen sich den Schatz?

Lösung
######

Sperre bei Zweierpotenzen, wird der erste der Zweierpotenz getötet, stirbt auch der Rest und damit stimmt die eine Hälfte für und die andere Hälfte der Zweierpotenz gegen den Tod
512

Rätsel 11
---------
x Piraten haben einen Schatz von genau 10 Goldmünzen. Jeder Pirat
möchte natürlich so viel wie möglich vom Schatz haben. Alle
Piraten haben einen Rang (von 1 bis x) wobei 1 der Kapitän ist. Die
Vorgehensweise ist nun wie folgt:
Der Kapitän macht einen Vorschlag, wie der Schatz aufzuteilen
ist; über den Vorschlag wird demokratisch abgestimmt. Falls
mindestens die Hälfte der Piraten für den Vorschlag ist, wird er
angenommen. Ansonsten wird der Kapitän über Bord zu den
Haien geworfen und der nun ranghöchste Pirat darf einen Vorschlag
machen, und so weiter.
Piraten stimmen nur dann für das Überleben anderer Piraten, wenn
sie dadurch einen Vorteil haben.
Welchen Vorschlag sollte der Kapitän machen?
52 Piraten haben einen Schatz von 10 Goldmünzen

Rätsel 12 : Schachbrett und Dominos
-----------------------------------
Kannst du ein n×n Schachbrett so mit 1×2 Dominos auslegen, so
dass sich zwei benachbarte Dominos nie eine lange Seite teilen?

.. figure:: schachbrett12.png
	 :alt: schachbrett12

Rätsel 13 Münzen wiegen
-----------------------

Du hast m Münzen und du weißt, dass darunter genau eine
gefälschte Münze ist. Du weißt auch, dass die gefälschte Münze
leichter ist als eine echte Münze. (alle echten Münzen sind gleich
schwer)
Wie kannst du mit Hilfe einer Balkenwaage die gefälschte Münze mit
so wenig Wiegeoperationen wie möglich finden?

Lösung
######

Man teilt die Münzen in drei gleiche Haufen und vergleicht zwei. Sind beide Gleich, ist die Münze im dritten. Andernfalls nimmt man den leichteren Haufen. Nun geht man rekursiv weiter


Rätsel 14  Münzen wiegen in der Küche - Gewicht bekannt
-------------------------------------------------------
Du hast m Münzen und du weißt, dass darunter genau eine
gefälschte Münze ist. Du weißt auch, dass die gefälschte Münze
leichter ist als jede echte Münze. Echte Münzen wiegen alle gleich
viel und du kennst deren Gewicht.
Wie kannst du mit Hilfe einer Küchenwaage die gefälschte Münze
mit so wenig Wiegeoperationen wie möglich finden?

Lösung
######

wiegt man einen haufen, weiß man ob in dem Haufen die Falsche Münze ist. Der einzige Unterschied zur Balkenwaage ist, dass wir lediglich
Halbieren ist besser als dritteln, da man 2 mal wiegen muss um im worst case den stapel zu finden in dem die falsche münze ist.
2 teilen: log2(n)
3 teilen: log3(n) = log2(n)/log2(3)

Rätsel 15  Münzen wiegen in der Küche
-------------------------------------
Du hast m Münzen und du weißt, dass darunter genau eine
gefälschte Münze ist. Du weißt auch, dass die gefälschte Münze
leichter ist als jede echte Münze. Echte Münzen wiegen alle gleich
viel, doch du kennst deren Gewicht nicht.
Wie kannst du mit Hilfe einer Küchenwaage die gefälschte Münze
mit so wenig Wiegeoperationen wie möglich finden?

Lösung
######

Am Anfang kann man beide Haufen wiegen und dann das Gewicht berechnen dadurch bestimmen hat effektiv eine münze mehr
im ersten Schritt kann man daher auch drei teilen da man eh zwei mal wiegen muss

Rätsel 16 Münzen wiegen in der Küche - ohne Vorwissen
-----------------------------------------------------

Du hast m Münzen und du weißt, dass darunter genau eine
gefälschte Münze ist. Echte Münzen wiegen gleich viel, doch du
weißt nur, dass die gefälschte Münze ein anderes Gewicht hat.
Wie kannst du mit Hilfe einer Küchenwaage die gefälschte Münze
mit so wenig Wiegeoperationen wie möglich finden?

Lösung
######

teilen durch drei und wiegen zwei. damit konnen wir bestimmen wie normale münzen wiegen und dann gewicht der anderen münze spätestens im nächtsten schritt wiegen



gibt eine gefälschte Münze. Mit zwei Wigunge

Rätsel 17 Schachbrett mit Loch
------------------------------

Kannst du ein :math:`2^m * 2^m` Schachbrett, in dem genau ein beliebiges Feld
fehlt, mit Bausteinen der Form abdecken?

.. figure:: schachfeld17.png
	 :alt: schachfeld17

Lösung
------
Legt man den Baustein in die Mitte des Feldes kann man das vorherige Feld in 4 Teilfelder teilen, welche alle ein fehlendes Feld haben.
Da das Feld :math:`2^m * 2^m` groß ist kann man dies solange fortsetzen bis man nur noch Felder der Kantenlänge 4 hat,
bei denen 1 Feld fehlt. All diese können mit dem Baustein ausgefüllt werden.
Damit kann man ein beliebiges :math:`2^m * 2^m` Schachbrett mit dem Baustein abdecken.


Rätsel 18 Straße über Fluss
---------------------------
Du willst eine Straße von A nach B bauen, doch dabei muss
ein (gerader und überall gleich breiter) Fluss überquert werden.
Brücken sind teuer, deshalb muss die Brücke so kurz wie möglich
sein.
Wo muss die Brücke gebaut werden, um die Wegstrecke von A
nach B zu minimieren?

Lösung
######

Man betrachtet den Fluss als Nullbreit, also als eine Gerade.
Nun bilden wir die Strecke von A nach B, sie ist die kürzest mögliche Verbindung.
Da die Brückenlänge unabhängig von der Position dieser immer gleich ist gilt es nur die Verbindung von A und B zu minimieren.
Wir haben das Problem auf ein einfacheres zurückgeführt (Strecke AB).

Rätsel 19
---------
Gegeben ist ein gleichseitiges Dreieck einer Seitenlänge von :math:`2^n`
, bei dem eine Spitze so abgeschnitten ist, dass genau ein gleichseitiges
Dreieck der Seitenlänge 1 fehlt. Wir nennen das resultierende Dreieck
ein defektes Dreieck.
Für welche n kannst du dieses defekte Dreieck mit nicht-überlappenden
Trapezinos (Trapeze bestehend aus drei gleichseitigen Dreiecken der
Seitenlänge 1) abdecken?

Lösung
######
Auch hier kann man das Problem wieder auf kleinere Instanzen zurückführen.
unten in der Mitte von Dreieck ein Trapezino reinlegen. Nun hat man 4 Dreiecke bei denen eine Ecke gefehlt hat und macht rekursiv weiter. Das Trapezino ist selbst ein Dreieck mit fehlender Ecke und der Kantenlänge 2.
Da man für alle n ein Dreieck der Seitenlänge :math:`2^n`vierteln kann, geht dies für alle n.

Rätsel 20 Nim
-------------
Von einem Haufen mit 21 Streichhölzern nehmen zwei Spieler abwechselnd
Streichhölzer weg. Verloren hat, wer keinen Zug mehr
machen kann.
Angenommen man darf pro Zug immer 1 oder 2 Streichhölzer wegnehmen.
Kann der Startspieler immer gewinnen?

Lösung
######
einfach immer zur 3 ergänzen

DP/ backtracking nutzen


Rätsel 21: Nim – Version 2
--------------------------
Von einem Haufen mit 21 Streichhölzern nehmen zwei Spieler abwechselnd
Streichhölzer weg. Verloren hat, wer keinen Zug mehr
machen kann.
Angenommen man darf pro Zug immer 1 oder 3 Streichhölzer wegnehmen.
Kann der Startspieler immer gewinnen?

Rätsel 22: Nim – Version 3
--------------------------
Es gibt zwei Haufen mit jeweils 21 Streichhölzern und zwei Spieler
nehmen abwechselnd Streichhölzer weg. Verloren hat, wer keinen
Zug mehr machen kann.
Angenommen man darf pro Zug immer einen Haufen wegwerfen
und den anderen Haufen beliebig in zwei Haufen aufteilen.
Kann der Startspieler immer gewinnen?

Rätsel 23: Nim mit Nachlegen
----------------------------

Lösung: Wir bauen uns ausgehend von der
21 ist ein Gewinner Position

Rätsel 24: Damenproblem
-----------------------

Gewinner und Verlierer felder in Schachbrett einzeichen

Rätsel 25: Telefone verwanzen

Lösung:
b, e, f, g, j
Optimierungsproblem von vortex cover

Datenreduktionsregeln suchen

Rätsel 26
---------

Zyklen suchen und

Rätsel 27
---------

Dominating Set

Rätsel 28
---------

in jeder Klicke mus man genau einen wählen

Rätsel 29
---------

Man sucht sich eine Reihe, die mehr als 5 Ballons hat muss man durch diese schießen, denn wenn man es nicht tut muss man jeden der Ballons einzeln zerschießen und braucht mehr als 5 Schüsse


Rätsel 39
---------

erster, zweiter  kann nicht sagen ob alle eins Wollen
sagen aber auch nicht nein,
daher weiß der dritte, dass

Rätsel 42
---------

:math:` `

Rätsel 47
---------

Lösung ist 1+1 = 2

Rätsel 48
---------

würde die andere Person sagen, dass sie am richtigen weg steht

Rätsel 49
---------

Würdest du die Frage, ob links der richtige Weg ist, mit ja beantworten.
kann auch fragen: Was würde Mittglied des anderen Stammes sagen

Rätsel 51
---------

keiner, da alle danach wegfliegen

Rätsel 52
---------

Wie weit kann man in den Wald hineinlaufen?

Rätsel 53
---------

Er fällt aus dem Erdgeschoss, oder gebäude ist niedrig.

Rätsel 54
---------

Sarg

Rätsel 55
---------

Mensch: Baby krabbelt, dann auf zwei Beinen und dann noch einen Gehstock

Rätsel 57
---------
Wie unterschied zwischen Bauarbeitern (C++) und Architekten (Java)

Rätsel 62
---------

Der Affe ist immer auf gleicher Höhe.

Rätsel 63
---------

Wenn man einen Kreis mit dem Radius r/4 erreicht und die Hexe auf der gegenüberliegenden Seite ist erreicht

Rätsel 66
---------

Zündschnur von beiden enden anzünden -> wenn abgebrannt ist eine halbe stunde vergangen
zweite Zündschnur nur von einer Seite anzünden
und nach der halben Stunde wird das zweite Ende der zweiten Zündschnur angezündet

Rätsel 67
---------

ein Teil aufteilen und verwenden um die drei anderen zu verbinden

Schokoladenrätsel
-----------------

Schokolandenbruch als Baum darstellen. Jeder Bruch führt zu zwei Teilung ->

Invariante: bei jedem Bruch erhöht sich die Anzahl der Schokolandenteilen um genau 1.
und wir starten mit einem Stück Schokolade

Monty Hall Problem
------------------

Hexaflexagon
------------

Eulertour
---------

Invariante: Man kann einen Knoten nur so oft
