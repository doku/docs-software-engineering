Introduction
============

Arten von Rätseln
-----------------

Grundlegend gibt es zwei Arten von Rätseln.

 - systematisch/algorithmisch lösbar
 - benötigen Aha! - Erlebnis



Grundregeln
-----------

.. note::
  Vergewissere dich, dass du das Rätsel und alle enthaltenen Begriffe verstehst?

.. note::
  Gebe alle getroffenen Annahmen an.

.. note::
  Verlasse dich nicht auf deine Intuition, rechne nach!

.. note::
  Suche erst eine geeignete Formalisierung des Rätsels, dann löse es!

.. note::
  Suche nach Invarianten! D.h. suche nach Aussagen oder Eigenschaften, die immer wahr sind bzw. nach Werten, die sich nie ändern.

.. note::
  Induktion

  (V) Löse zuerst kleine Instanzen! Verallgemeinere dann ausgehend von deinen Beobachtungen.
  (R) Versuche das Problem geeignet zu verkleinern, um eine äquivalente kleinere Probleminstanz zu erhalten!

.. note::
  Versuche das Problem in (identische/andere) Teilprobleme zu zerlegen, mit deren Lösung du dann eine Gesamtlösung konstruieren kannst.

.. note::
  Rekursive Problemlösung
        -Benutze Induktion und Zerlegen in identische/andere Teilprobleme
        -merke dir gefundene Zwischenlösungen
        -nutze Zwischenlösungen per Table Look-up


Invarianten
-----------


Induktion
---------
Beweise, dass eine Basis gilt und dass der nächste Schritt gilt sofern eine beliebige aber feste Basis gezeigt wurde.

Binärbaum
_________

Man startet mit einem Knoten, dieser hat 2 null-Pointer. Fügt man einen Knoten zu einem bestehenden Baum hinzu

Besser man nehme einen beliebigen Baum und entfernt immer einen Knoten, Für jeden Knoten der entfernt wird, wird ein null-Pointer entfernt. 2 bleiben beim letzten Knoten übrig
