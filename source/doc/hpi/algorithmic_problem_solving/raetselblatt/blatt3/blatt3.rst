Rätselblatt 3
=============

Rätsel 1 Dreifarbenbrett
------------------------
Du hast ein Brett mit drei Zeilen und n Spalten; in den 3n Zellen liegen jeweils ein
Marker. Insgesamt gibt es n weiße, n blaue und n rote Marker. Du darfst nun die
Marker innerhalb einer Zeile beliebig vertauschen.
Kannst du das so machen, dass jede Spalte genau einen weißen, einen blauen und
einen roten Marker enthält?

Rätsel 2 Lineal und Färbungen
-----------------------------
Du zeichnest auf ein Blatt Papier mit einem Lineal Linien, welche komplett uber das ¨
Blatt reichen.
Zeige, dass du die dadurch entstandenden Zonen auf dem Papier mit zwei Farben
färben kannst, so dass zwei benachbarte Zonen unterschiedliche Farben haben.

Lösung
######

IA: Ein Blatt ohne Striche lässt sich färben.

IV: Sei ein Blatt mit n Strichen korrekt gefärbt.

IS: Wir machen einen neuen Strich. Auf beiden Seiten des Strichs ist alles weiterhin korrekt gefärbt, aber entlang des Strichs gibt es Kollisionen. Hier stößt jede Fläche, die durch den Strich geteilt wurde, an ihre andere Hälfte mit derselben Farbe. Um dieses Problem zu lösen, invertieren wir auf einer Seite einfach die Farben.

Rätsel 3 Piratenschatz
----------------------
1000 Piraten haben einen Schatz von genau 10 000 Goldmünzen. Jeder Pirat möchte
natürlich so viel wie möglich vom Schatz haben. Alle Piraten haben einen Rang (von
1 bis 1000) wobei 1 der Kapitän ist. Die Vorgehensweise ist nun wie folgt:
Der Kapitän macht einen Vorschlag, wie der Schatz aufzuteilen ist; über den Vor-
schlag wird demokratisch abgestimmt. Falls mindestens die Hälfte der Piraten fur
den Vorschlag ist, wird er angenommen. Ansonsten wird der Kapitän über Bord zu
den Haien geworfen und der nun ranghöchste Pirat darf einen Vorschlag machen, und
so weiter.
Welchen Vorschlag sollte der Kapitän machen?

Lösung
######
Bleibt ein Pirat als Letzter übrig, kann er sich den ganzen Schatz nehmen. Bleiben zwei Piraten, so kann der Kapitän von beiden ebenfalls alles für sich beanspruchen. Erst bei 3 verbliebenen Piraten muss er dem rangniedrigsten eine Münze abgeben. Der wird sie annehmen, da er ansonsten leer ausgeht. Dieser Vorschlag funktioniert auch noch bei 4 verbliebenen Piraten. Der 3. bekommt eine Münze, der 2. und 4. keine. Bei 5 Piraten werden drei Stimmen gebraucht, nun muss der 5. Pirat mit ins Boot geholt werden und er muss eine Münze bekommen. Mit einem besseren Angebot kann er nicht rechnen und er hat auch keine Chance dazu, selber Kapitän zu werden, also wird er annehmen. Dieses alternierende Schema lässt sich fortsetzen. Es bekommen alle Piraten mit ungeradem Rang eine Münze. Am Ende bleiben dem Kapitän also 9501 Münzen. Mit diesem Vorschlag hat er die besten Chancen.

Rätsel 4 Anarchist im Flugzeug
------------------------------
Einhundert Leute betreten nacheinander ein Flugzeug und begeben sich zu ihrem
Platz; nur der erste Passagier ist ein Anarchist und wählt sich einen der einhundert
Sitzplätze (uniform) zufällig. Jeder spätere Passagier, dessen Sitzplatz besetzt ist,
möchte keinen Arger machen und wählt sich einfach einen Sitzplatz (uniform) zufällig
aus den noch freien.
Was ist die Wahrscheinlichkeit, dass der als letztes einsteigende Passagier auf seinem
zugeordneten Platz sitzt?

Lösung
######

Wahrscheinlichkeit ist 1/2
Anarchist blockiert den Sitz der Person i. Bis zur Person i läuft alles ganz normal. i wird dann zum Anarchisten. Noch Frei ist Platz 1 und Alle plätze ab i.
Nun setzt sich i auf Platz j. Damit wird j wieder zum nächsten Anarchisten. Dies geht bis zum vorletzten, der dann als Anarchisten fungiert und zwischen Platz 1 und 100 wählen kann. Daher ist die Wahrscheinlichkeit 1/2

Induktion über die Anzahl der Sitzplätze
IA: n = 2 W = 1/2
IV: Statement gilt für alle Sitzplätze < n
IS: von n nach n+1
Anarchist setzt sich mit der Wahrscheinlichkeit von 1/n auf seinen Platz, 1/n auf platz 100
Wahrscheinlichkeit dass er sich auf den Platz eines anderen(nicht Platz 100) setzt = n-2/n

Wahrscheinlichkeit das P100 seinen Platz bekommt

1/n *100% + n-2/n \* 50%(IV) + 1/n *0% =

1/n + n-2/n \*(1/2) = 1/2 \*(2/n + n-2/n)
