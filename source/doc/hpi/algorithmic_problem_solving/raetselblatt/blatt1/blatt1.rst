=============
Rätselblatt 1
=============

Rätsel 1: Ganovenduell
----------------------
Drei Ganoven streiten sich über die Beute; am Ende können
sie sich nicht einigen und wollen sich gegenseitig erschießen.
Eddie "Flinker Finger" ist der mit der besten Reaktion und schießt
zuerst; er hat eine Trefferwahrscheinlichkeit von 1/3. Harry "Die
Zange" ist zweitschnellster und trifft mit Wahrscheinlichkeit 2/3;
schließlich schießt Bud "Der Hammer", der immer trifft. Wer
getroffen ist, geht zu Boden und schießt nicht mehr; nachdem
alle geschossen haben geht die Runde von vorne los.
Wen sollte Eddie angreifen, um seine Überlebenschancen zu
maximieren?

Rätsel 2: Briefmarkengeschäfte
------------------------------
Du verkaufst deine Briefmarkensammlung an Frau A für 10000
EUR. Später merkt Frau A, dass deine Sammlung nicht so viel
wert ist, wie sie ursprünglich dachte und verkauft sie dir wieder
für 9000 EUR zurück. Du gibst nicht auf und verkaufst deine
Sammlung schließlich für 9500 EUR an Herrn B.
Was ist dein Gesamtgewinn?

Rätsel 3: Ring mit Fertigungsfehler
-----------------------------------

Du hast eben erfolgreich dein Start-Up verkauft und verwendest
nun dein Vermögen, um vom Schmied deines Vertrauens ein
Ring schmieden zu lassen, der genau um die Erde passt (und
wir nehmen an, dass die Erde eine Kugel ist). Der Schmied legt
los und präsentiert dir nach einigen Jahren deinen bestellten
Ring. Leider ist dessen Umfang um 1 Meter länger geworden
als geplant. Angenommen der Ringe würde nun so um die Erde
gelegt, so dass er an einem Punkt die Erde berührt.
Wie weit würde der gegenüberliegende Punkt vom Erdboden
abstehen?
Was für Tiere passen darunter durch: Ameise, Maus, Hund, Pferd
oder Giraffe?

Rätsel 4: Saftmischung
----------------------
Du hast zwei exakt gleich volle Gläser, das eine gefüllt mit Apfelsaft,
das andere gefüllt mit Birnensaft. Du nimmst nun einen
Löffel voll Saft aus dem Apfelsaftglas und gibst ihn in das Birnensaftglas,
rührst gut um, und nimmst nun einen Löffel voll Saft
wieder zurück.
Ist danach mehr Birnensaft im Apfelsaftglas oder mehr Apfelsaft
im Birnensaftglas?

Lösung
------
Da am Ende gleich viel Saft in beiden Gläsern ist, muss gleich viel Saft von dem einen Glas in das andere gewechselt haben.
