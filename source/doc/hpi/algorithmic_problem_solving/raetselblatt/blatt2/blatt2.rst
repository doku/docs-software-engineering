Blatt 2
=======

Rätsel 1 Puzzle
---------------
Du hast ein Puzzle mit 500 Teilen. Eine ”Komponente“ des Puzzles ist eine Menge aus einem oder mehr Puzzleteilen, welche miteinander verbunden sind. Ein ” Schritt“ besteht darin, zwei Komponenten zu verbinden. Was ist die minimale Anzahl an
Schritten, mit denen man das Puzzle fertigstellen kann?

Lösung
######
Invariante: bei jedem Schritt wird die Anzahl an Puzzelteilen um 1 reduziert. Damit benötigt man 499 Schritte.

Rätsel 2 Springer auf Schachbrett
---------------------------------
Du hast einen Springer auf einem Schachbrett ganz unten rechts stehen. Kann der Springer so gezogen werden (jeweils gültige Springer-Züge machend), dass er alle Felder des Schachbretts genau einmal betritt und dann ganz oben links zum Stehen kommt?

Lösung
######

Es ist nicht möglich mit einem Springer von unten rechts nach oben links zu kommen, wenn dabei jedes Feld genau einmal betreten werden soll. Durch die Zugart des Springers kommt es bei jedem Zug zu einem Wechsel der Farbe des Feldes, auf dem der Springer steht. Da die diagonal liegenden Ecken auf einem Schachfeld die gleiche Farbe haben, muss der Springer also eine gerade Anzahl an Zügen vornehmen. Um alle 64 Felder genau einmal zu betreten muss der Springer aber 63 Züge machen. Daher ist es nicht möglich jedes Feld genau einmal zu betreten.

Invariante: Zwei nebeneinander liegende Fehler haben nie den gleichen Zustand, Paritätsargument

Rätsel 3 Sesseldrehen
---------------------

Du hast in deinem Wohnzimmer einen sehr schweren Sessel stehen, mit Grundfläche 1 × 1 Meter. Diesen Sessel möchtest du aber lieber einen Meter weiter rechts haben (er soll aber noch in die gleiche Richtung zeigen). Du hast ein bisschen Platz um den Sessel herum gemacht weil du ihn nur dadurch bewegen kannst, dass du ihn an einer Ecke stehen lässt und um diese Ecke um genau 90 Grad drehst (in eine beliebige Richtung). Wie viele Schritte benötigst du mindestens, um dein Ziel zu erreichen?


Lösung
######

Es ist nicht möglich den Sessel um einen Meter nach rechts zu verschieben. Sei das doppelt umrandete Quadrat im Bild die Startposition des Sessels. Dabei stellen die Punkte die Achsen dar, um die man den Sessel dreht, wenn man ihn auf eines seiner Beine stellt und rotiert. Wir bezeichnen nun die Ausrichtung des Sessels als Zustand und fassen eine nach oben oder unten zeigende Sitzfläche als ein Zustand zusammen. Analog wird dies für eine nach lins/rechts zeigende Sitzfläche unternommen. Es wird klar, dass bei jeder Rotation der Zustand des Sessels von oben/unten nach rechts/links wechselt. Damit bildet sich also eine Art Schachfeld mit Zuständen und zwei direkt aneinander liegende Felder können nie den gleichen Zustand haben. Also kann man den Sessel nicht um einen Meter nach rechts verschieben in dabei die Ausrichtung beibehalten. Möglich wäre es den Sessel um 2 Meter nach rechts zu verschieben.

.. figure:: r31.png
	 :alt: r31


.. figure:: r32.png
	 :alt:  r32


Rätsel 4  Spalten und Zeilen
----------------------------

Gegeben sind die folgenden zwei Matrizen. Ein "‘Schritt"’ ist
entweder das Vertauschen zweier Zeilen, oder das Vertauschen
zweier Spalten.
Wie viele Schritte benötigst du mindestens, um die linke Matrix in
die rechte zu überführen?

.. figure:: matrizen.png
	 :alt: Matrizen

Lösung
######
Die Überführung der beiden Matrizen ist nicht möglich. Durch die gegeben Operationen von Zeilen und Spalten tauschen ist es nicht möglich, dass die Elemente der Zahlengruppen, welche die Zeilen zu Beginn gebildet haben (Bsp. {5,6,7,8}) wechseln. Damit bleiben diese Zahlengruppen immer zusammen, womit die 5 und die 15 ihre Positionen in der zweiten Matrix nicht hätten erreichen können. Dies kann man sich sehr einfach erklären. Durch das tauschen zweier Spalten bleiben die Elemente in einer Zeile immer gleich, lediglich die Reihenfolge ändert sich. Durch das tauschen zweier Zeilen bleiben die Zeilen als Ganzes erhalten und wechseln nur ihre Position in der Matrix. Damit sind die Elemente in einer Zeile immer gleich und ändern nur ihre Reihenfolge. Somit kann also die Zahlengruppe {5,6,7,8} nicht in {8,6,7,15} umgewandelt werden.

Alternative: Determinante berechnen, hier ändert sich maximal das Vorzeichen bei Zeilen und Spalten Tausch

Rätsel 5 Punktespiegeln
-----------------------
Gegeben sind vier Punkte in der Ebene, angeordnet als Quadrat.
Du darfst nun die folgende Operation ausführen. Wähle einen der
vier Punkte als Fixpunkt. Wähle einen anderen der vier Punkte als
zu bewegenden Punkt. Der zu bewegende Punkt wird nun am Fixpunkt
"‘gespiegelt"’, er bewegt sich also von seiner momentanen
Koordinate auf die andere Seite des Fixpunktes, am Ende ist der
Abstand vom Fixpunkt gleich.
Wie viele Züge brauchst du mindestens, bis die vier Punkte so angeordnet
sind, dass sie ein Quadrat ergeben, welches die doppelte
Kantenlänge vom Anfangsquadrat hat?

.. figure:: fixpunkt.png
	 :alt: fixpunkt

Lösung
######

Schachbrett drunter legen und dann bleiben die Punkte immer in den gleichen Farbe durch Spiegelung. Sie bewegen sich immer um eine gerade Anzahl Um das Ziel zu erreichen würden alle

Alternative: Annnahme es geht. Wenn ich verdoppeln kann, kann ich auch halbieren. Punkte bleiben immer auf den Kooredinaten Punkten, durch halbieren würde dies nicht gegeben sein

Rätsel 6: Ziffern summieren
---------------------------
Wie lautet die Summe der Ziffern aller Zahlen von 0 bis
1000000000?

Lösung: jede Ziffer kommt an jeder Stelle genau 10^k-1 mal vor

Alternative

Paare bilden wie bei Gaußescher Summern Formel

bsp für 1000

999 +0
998 +1
...
980 +19
...

gibt 500 Paare alle mit der Quersumme 27 -> 27*500+1

für 10^9: 5\*10^8 Paare mit 9\*9
