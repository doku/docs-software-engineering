Rätselblatt 4
==============

Rätsel 1 Polygonzerlegung
-------------------------
Gegeben ist ein k-Eck (in der Ebene) und ein n; du möchtest das k-eck in n rechtwinklige
Dreiecke zerlegen. Fur welche (k, n) kannst du dies machen?

Rätsel 2 Münzwiegen
-------------------
Geben sind :math:`3^n` identisch aussehende Münzen; genau eine dieser Münzen ist gefälscht
und hat ein anderes Gewicht als jede beliebige echte Münze. Du weißt nicht, ob die
falsche Münze leichter oder schwerer als eine echte Münze ist. Du hast eine Balkenwaage zur Verfugung, mit der du zwei beliebige Haufen Münzen gegeneinander wiegen kannst.
Wie kannst du mit so wenig Wiegeoperationen wie möglich die falsche Münze identifizieren? Wie viele Wiegeoperationen brauchst du im schlimmsten Fall?

Lösung:
#######


Rätsel 3 Trapezinos
-------------------
Du hast ein gleichseitiges Dreieck mit Seitenlänge 3^n gegeben und möchtest dies nun
mit nicht-überlappenden Trapezinos (bestehend aus drei gleichseitigen Dreiecken der
Seitenlänge 1) abdecken. Wie geht das?

.. figure:: dreieck.png
	 :alt: dreieck

Rätsel 4 Quadratzerlegung
-------------------------
Gegeben ist ein Quadrat in der Ebene. Fur welche n kannst du das Quadrat in n
Quadrate (die nicht notwendiger Weise gleich groß sein müssen) zerlegen?

Lösung:
#######
Wir können durch vierteln von einem Quadrat + 3 Rechnen
Nun teilen wir die Unterschiedlichen Quadrate in Restklassen mit den Repräsentanten: 1,5,9 ein und können ausgehend von diesen immer plus 3 rechnen. Damit können wir alle Natürlichen Zahlen bis auf die 2, 3 und 5 erreichen.
