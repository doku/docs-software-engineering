=============
Rätselblatt 1
=============

Rätsel 3 Kompression
====================

Eine Derartige Komprimierung ist nicht verlustfrei (Daten gehen also unwiederherstellbar verloren). Sei X eine beliebige Datei.
Nun soll das Betriebssystem diese Datei X in eine Datei Y umwandeln können, welche
nur die halbe Länge (gegebenenfalls aufgerundet) von X hat. Nun kann ich aber wiederum
Y als Startdatei nehmen und dann sollte das OS die Datei wieder um die Hälfte
komprimieren können und man würde Z erhalten. Nun kann man immer so weiter machen
und ständiges halbieren führt dazu, dass man irgendwann nur noch die Länge 1 zur
Verfügung hat. Damit werden alle Dateien auf 1 Bit abgebildet und somit gehen alle Informationen
verloren. Ein Bit kann nur zwei Zustände annehmen, und daher ist es nicht möglich
beliebig viele unterschiedliche Dateien dadurch darzustellen.
