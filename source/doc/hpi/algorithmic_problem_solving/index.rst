Algorithmic Problem Solving
===========================

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Contents:

   introduction/*
   raetsel/*
   raetselblatt/blatt1/*
   raetselblatt/blatt2/*
   raetselblatt/blatt3/*
   raetselblatt/blatt4/*
   raetselblatt/blatt5/*
   raetselblatt/blatt6/*
   raetselblatt/blatt7/*
   raetselblatt/blatt8/*
   raetselblatt/blatt9/*
   raetselblatt/blatt11/*
