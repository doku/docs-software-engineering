=======
Rockets
=======


Propulsion
----------
Any system which uses a thrust of some sort to make itself go forward is a propulsion system.

That weight has inertia. It doesn't want to move. That's Newton's first law. In order to make it move in that direction, I have to exert a force and push on it.
That's Newton's second law. And when I do that, this object exerts an equal and opposite force back on me. That's Newton's third law. And so the essence of engines that we use in aerospace, whether flying through the atmosphere or a rocket flying through space, is we carry a certain amount of material inside and we push that material out at a certain speed, and that makes us go forward.
The fluid that we push is called the working fluid of the engine.
Now, in the case of a propeller plane,
the working fluid is the air.
Now, a jet plane is a little bit different.
Sometimes, we call them gas turbine engines.
They carry their fuel-- which is burned together
with oxygen, which is brought in-- you
can see here the air intake, and then
you take the hot combustion gases
and push them out the end of the jet engine,
and that gives you thrust to go forward.

What is the critical difference between a jet engine and a rocket engine?

The difference between a jet engine
and a rocket engine, the jet engine
only needs to carry its own fuel.
A rocket, on the other hand, has to carry not only its own fuel
but also the oxidizer, which in most cases
is oxygen, either a compressed gas or liquid oxygen. Instead of talking just about putting rocket fuel in,
we talk about loading it up with propellants. And there's two types of propellants for a rocket.
You need the fuel-- whether it's kerosene,
or liquid hydrogen, or alcohol, methane-- that's the fuel,
and then you need the oxidizer.
