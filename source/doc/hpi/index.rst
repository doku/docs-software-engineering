===========
HPI
===========

.. toctree::
   :maxdepth: 2

   aerospace_engineering/index
   algorithmic_problem_solving/index
   betriebssysteme_1/index
   building_interactive_devices/index
   datenbanksysteme_1/index
   internet_security/index
   programmiertechnik_2/index
   recht_1/index
   recht_2/index
   segeln/index
   software_architektur/index
   softwaretechnik_2/index
   stuff/index
   theoretische_informatik/index
