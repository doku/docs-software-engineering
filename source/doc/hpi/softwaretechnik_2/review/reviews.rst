======
Review
======

Motivations
===========

- Improve code
- Discuss alternative solutions
- Transfer knowledge
- Find defect

Challenges
==========

- delays feature
- reviewers need to switch context
- overloading when changes are to big
- overcrowding when there are to many reviewers(review quality decreases)
- good reviews are important to ensure quality

.. figure:: small_changes.png
	 :alt: small_changes


At Google
=========

- Mandatory review of every change
- Median completion times: 15.7h (Chrome), 20.8h (Android)
- Median patch size: 78 lines (Chrome), 44 lines (Android)
- Median number of reviewers: 2

Good review
===========
- Find problems, but don’t try to solve them
- Problems should be clearly identified / extracted

**should be reviewed:**

- Parts with complicated algorithms
- Critical parts where faults lead to system failure
- Parts using new technologies / environment / …
- Parts constructed by inexperienced team members

**might not be reviewed**

- trivial parts
- Parts which won’t break the functionality if faults occur
- Parts which are similar to those previously reviewed
- Reused or redundant part

Review Types
============

Old techniques:

management review
technical review
inspection
walk through

Modern Code Reviews:
--------------------
- Follows more lightweight, flexible process
- Change sizes are smaller
- Performed regularly and quickly, mainly just before code committed to main branch
- Shift from defect finding to group problem solving activity
- Prefer discussion and fixing code over reporting defects
- Code review coverage and review participation share significant link with software quality
- Most comments concern code improvements, understandability, social communication
- Only ~15% of comments indicate possible defect
- Developers spend approximately five hours per week (10-15% of their time) in code reviews

.. note::
  Maintainability and code improvements identified as most important aspects of modern code review

Pre und post commit
###################

Post-commit Code Review (using PRs):
  - Pro
    - Developers can commit continuously
    - all team members can see changes
  - Contra
    - branches required
    - chance of unreviewed code in repo


pre-commit code review (suitable for trunk based development)
  - Pro
    - Ensures review was performed and code quality is met before commit
  - Contra
    - overhead decreases productivity
    - review and commit are not tightly coupled (review changes in new commit)
  

CodeClimate & Code Linters
==========================

Testing checks code function via dynamic analysis
Code reviews manually check code quality via static analysis
Automated static analysis (linters) can help as well
- SimpleCov (code coverage, https://github.com/colszowka/simplecov)
- Flog (code complexity, http://ruby.sadi.st/Flog.html)
- Reek (code smells, https://github.com/troessner/reek)
- Cane (code quality, https://github.com/square/cane)
- Rails_best_practices (Rails specific, https://github.com/flyerhzm/rails_best_practices)
