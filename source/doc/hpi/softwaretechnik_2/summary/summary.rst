Summary
=======

Scrum
-----
.. note::
  Why traditional Projects fail: The later we find a defect, the more expensive it is to fix it!

Stakekolder: Customers, User, Management


Agile Manifesto
_______________

- Individuals and interactions over processes and tools
- Working software over comprehensive documentation
- Customer collaboration over contract negotiation
- Responding to change over following a plan

development in short cycles that result in a working product

- Outcome based planning (not upfront), shorter planning horizon
- Streaming requirements
- evolving design (need to refactor)
- CI
- continual regression testing
- frequent releases
- potentially shippable increment
  - Definition of done
  - no regrets if projects ends now
- incremental cost estimation, but better than mistakes from waterfall

Value-based Requirement Analysis
________________________________

.. note::
  80% of the value is expressed in 20% of the requirements

- Identify the system's success-critical stakeholders
- Obtain their value propositions with respect to the system
- Estimate / find out value of a requirement to the stakeholders
- Estimate effort to implement a requirement

.. figure:: value_effort_rule.png
	 :alt: value_effort_rule

MVP
___

Bare minimum to determine what the customer wants?
minimal Customer demo vs. working product?

.. figure:: mvp.png
	 :alt: mvp

Helps with faster feedback cycles.
Challenges: Requires smart requirement management,  Requires “product” quality early on (problem with bugfixing)

Organize your Project
_____________________

Use in Github

- Milestones
- Tags
- project management tools like waffle
- Tag versions that can be presented
  - `` git tag –a v0.1 –m 'version after Sprint#1 without US #2'``
- use burn down charts

definition of done as team consensus
definition of ready ( Estimated , Acceptance criteria, Mockups for UI stories)

communicate
###########

- between Teams/other peoples code
- about architecture Changes
- about dependancies (Ambassador)

How to...

- Github wiki but as brief as possiple
- code comments to explain larger context
- **ONE** common communication channel

Process
_______

.. figure:: scrum_process.png
	 :alt: scrum_process

PO
###

- costumer communication
- backlog (stories and priorities)
- Acceptance Criteria and Tests
- workflow
  - talk to user and create list of desired functionality
  - create prioritized user stories (with acceptance tests)
  - present US to team

SM
###

- Process manager
- removes impediments

DEV
###

- Communication (discuss US, share insights, represent Team)
- Developing
- workflow
  - estimate US
  - create, est Tasks

Backlog
#######

- Requirements(modification requests): Features and Bugs (INVEST)
  - often as US: As <role>, I want <feature> to <reason>
- prioritized

Sprint Backlog
##############

- tasks are signed up for not assigned
- no new features during Sprint

Meetings
########

- Planning / Backlog refinement
  - estimate Backlog Items
  - split US in Tasks: Tasks should be SMART:
    - S – Specific
    - M – Measurable
    - A – Achievable
    - R – Relevant
    - T – Time-boxed
  - select stories until sprint is full and assign devs
- daily scrum
  - standup 2min per person
- review meeting
  - acceptance of features, demo to PO(should be prepared)
- Retro/review
  - internal team evaluation
  - discuss process and problems
  - measure improvements
- PO Meetings
  - meet customers
  -  create backlog
- Scrum of scrums, meeting of SMs
  - coordinate inter Team dependancies
  - discuss technical decisions

estimation
##########

Planning poker
''''''''''''''

Precondition

- backlog is complete and prioritized
- known to team
- planning poker cards
- reference items

Process

- PO presents US and answers questions
- team chooses hidden estimation card
- show them all at once
- discuss high and low cards in team
- new vote and team agrees on one size(or new round)
- ends when time over or all estimated

Affinity Estimaion
''''''''''''''''''

Precondition

- backlog is complete and prioritized
- known to team
- US in physical form
- reference items

Process

- silent relative sizing
- rearrange, discuss changes (clarification PO)
- place size categories and assign each story a size based on location



BDD
---
BDD is about implementing an application
by describing its behavior from the perspective of its stakeholders

.. figure:: bdd_cycle.png
	 :alt: bdd_cycle

Principles

- story based definition of application behavior
- Definition of Features
- driven by business value

for the developer

 - BDD Cycle
 - coding with TDD
 - automated testing

Definition of Done
__________________

- acceptance criteria fulfilled
- tests green
- good code quality
- code review
- Internationalization
- mobile

.. figure:: bdd_pyramid.png
	 :alt: bdd_pyramid

- vision: all stakeholders one statement
- goals: how the vision will be achieved? e.g.  Easier ordering process
- epics: huge feature sets: Too high level to start coding but useful for conversations
- Usecases / Features: Describe the behavior we will implement in software
- User stories: demonstratable functionality(vertical, Invest), token for conversations
- scenario steps: Each scenario describes one aspect of a User Story, Describe high-level behaviors
- test Cases: Describe low-level behavior
