Softwaretechnik II
==================

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Contents:


   scrum/*
   summary/*
   kanban/*
   kunden/*
   lessons_learned/*
   testing/*
   deployment/*
   git/*
   review/*
   rubyonrails/index
