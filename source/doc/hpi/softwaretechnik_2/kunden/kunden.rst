Product Owner Tips
==================

.. note::
  Each sprint is a potentially shipable increment

Wichtige skills
---------------

- Priorisieren
- Grundverständnis
- Domaine verstehen
- Dickes Fell zu haben(schafft es nie allen Stackholdern alles recht zu machen)
- Immer den Elevator pitch bereit haben
- Produkt immer demo ready haben

Review
------

während des Sprints Zwischenabnahme machen, Missverständnissen vorbeugen

Retrospective
-------------

Qualitätsprobleme, Kommunikationsprozesse einbringen

Planung
-------

High level business wert vorstellen

Tickets vorstellen

Neue und alte Tickets neu priorisieren
Profile der Developers (Fähigkeiten) berücksichtigen

Requirements engineering
------------------------

- Kunden am System spielen lassen
- Work shadowing (Was macht der Kunde)
- mockups
- design Thinking
- nach Proritäten fragen
- Sortieren lassen
- Web Analytics
- Data driven development


Tickets
-------

- Definition of done
- Definition of ready
- business value

- Akzeptanz Kriterien



Abschätzen
##########

siehst du Problem/ Abkürzungen Tickets


Interaktion mit dem Kunden
--------------------------

5 Whys

Nein ist valide Antwort

Kunden über ablauf infomieren, Transparenz des Entwicklungsprozesses

nicht zu viele Alternativen

Deadlines sind schwer anzugeben


Userstories
-----------

- lieber beobachten als fragen
- Mockup Balsamiq

tags
 - effort
 - value
 - Kunden
 - beteiligte Komponenten
