Behavior-driven Development and Testing in Ruby on Rails
========================================================

Goals of Automated Developer Testing
####################################

Automated testing of functionality saves time in course of the development process.
You ensure that changes and refactoring don't break original functionality.
The longer the project the more time is saved.

Find errors faster
- Better code (correct, robust, maintainable)
- Less overhead when testing à tests are used more frequently
- Easier to add new features
- Easier to modify existing features
- But
- Tests might have bugs
- Test environment != production environment
- Code changes break tests

Writing Software that Matters
#############################

BDD is about implementing an application by describing
its behavior from the perspective of its stakeholders

.. figure:: bdd_cycle.png
	 :alt: bdd_cycle

Definition of Done
##################

- Acceptance criteria fulfilled
- All tests are green
- Code looks good
- Objective quality goals
- Code review
- Second opinion
- Internationalization
- Security
- Documentation

.. note::

	**The Definition of Done is the team’s consensus of what it takes to complete a feature.**

BDD Pyramid
###########
.. figure:: bdd_pyramid.png
	 :alt: bdd_pyramid


Vision
######

.. note:: All Stakeholders, one statement

Example: Improve Supply Chain; Understand Customers Better
Stakeholder Jeder der irgendwas mit dem Project zu tun hat /beinflussen kann

Goal
####

based on the vision, aspect that can be decided whether it was reached

Epics
#####

Huge themes / feature sets are described as an “epic”

- Too high level to start coding but useful for conversations
- Examples
- Reporting
- Customer registration

Use Cases / Features
####################

Should be clear that one stakehoders benefits from his featuer

User Stories
############

Stories are demonstrable functionality
INVEST

Story content

- Title
- Narrative
- Description, reason, benefit
- “As a <stakeholder>, I want <feature> so that <benefit>”
- “In order to <benefit>, a <stakeholder> wants to <feature>”
- Acceptance criteria

Gives context to make developer understand the reason for the feature and give insights

Scenarios, Scenario Steps, Test Cases
#####################################


Agile Methods
-------------


.. figure:: agile_methods.png
	 :alt: agile_methods


testing
-------

.. figure:: levels.png
	 :alt: levels


requirement test very difficult to Automated



.. figure:: bdd_vs_test.png
	 :alt: bdd_vs_test


rspec vs unit
_____________

unit test:

.. figure:: unit_test.png
	 :alt: unit_test

RSpec offers syntactical sugar, different structure


.. figure:: rspec_example.png
	 :alt: rspec_example


.. figure:: rspec_example2.png
	 :alt: rspec_example2

Rspec Matchers (Assertions):  ``expect(…).to <matcher>, expect(…).not_to <matcher>``
there are many specialized matchers for Object (Identities, Equivalence), Comparisons,
Collections and many more

Test in general
_______________

Keep them small
Don’t share complex test data
Don’t use complex objects
Tests should be independent

good test
_________

- Independence (of external test data and other test )
- Repeatability (same result each run)
- Clarity (test purpose immediatly understandable, short)
- Conciseness (Use the minimum amount of code and objects, use context & describe one assertion per test)
- robustness (Underlying/tested code is correct -> test passes, else not)

Model Tests
___________

A Rails model
- Accesses data through an ORM
- Implements business logic
- Is “fat”

Model tests in Rails
- Easiest tests to write
- Test most of application logic
- Do not test framework functionality like “belongs_to”
- Tests should cover circa 100% of the model code


View test
_________

A Rails view, Has only minimal logic, Never calls the database! Presents the data passed by the controller

Challenges
 - time-intensive
 - how to test look and feel
 - Brittle with regard to interface redesigns

view Tests
 - Specify and verify logical and semantic structure
 -  Validate that view layer runs without error
 - Check that data gathered by the controller is presented as expected

controller tests
________________

A Rails controller
- Is “skinny”
- Calls the model
- Passes data to the view
- Responds with a rendered view

Goal of controller tests
- Simulate a request
- Verify internal controller state
- Verify the result

Setup and Teardown
___________________

.. figure:: before_after.png
	 :alt: before_after


.. figure:: test_run.png
	 :alt: test_run

Testdata
________

Tests should be independent : If a bug in a model is introduced Only tests related to this model should fail

.. note::
	dont share complext test data

to main ways to proide date for test cases Fixtures  and Factories


Fixtures
########

- Fixtures represent sample data
- Fixed state at the beginning of a test
- Assertions can be made against this state

.. figure:: fixtures_example.png
	 :alt: fixtures_example

Problems
	- Populate testing database with predefined data before tests run
	- Stored in database independent YAML files (.yml)
	- One file per model, location: test/fixtures/<name>.yml

.. figure:: fixtures_pain.png
	 :alt: fixtures_pain

Testdata should be local(defined close to test), compact (easy and quick to specify), robust(independent from other tests)

**Solution** Data factories

Factories
#########

- Blueprints for sample instances
- Used to generate test data locally in the test


.. figure:: factories_example.png
	 :alt: 	factories_example

http://www.rubydoc.info/gems/factory_bot/file/GETTING_STARTED.md

test doubles
############

Generic term for object that stands in for a real object during a test

- used in automated testing when real object is difficult to access
- Following a strategy to re-create an application state

verifying behaviour during test
###############################

- Usually: test system state after a test
	- Only the result of a call is tested, intermediate steps are not considered
- With test doubles: Test system behavior
	- E.g. How often a method is called, in which order, with which parameters

You can use RSpec Mocks as framework

stubs
#####
Method call on the real object does not happen
Returns a predefined value if called
Strict by default (error when messages received that have not been allowed)


.. figure:: stubs1.png
	 :alt: stubs1
Alternatively, if all method calls should succeed: Null object double

.. figure:: stubs2.png
	 :alt: stubs2

Spies
#####
Stubs with Given-When-Then structure
Allows to expect that a message has been received after the message call


.. figure:: spy1.png
	 :alt: spy1

Alternatively, spy on specific messages of real objects

.. figure:: spy2.png
	 :alt: spy2

this pattern is also called act-arrange-assert

Mocks
#####

**Mocks are Stubs with attitude**

.. figure:: mocks.png
	 :alt: mocks


.. figure:: stubs_vs_mocks.png
	 :alt: stubs_vs_mocks

troubleshooting
###############

- Reproduce the error
	- Write a test
- What has changed?
 	- Isolate commit/change that causes failure
- Isolate the failure
	- thing.inspect
	- Add assertions/prints to your test
	- Rails.logger.error
	- save_and_open_page (Capybara method to take a snapshot of a page)
- Explain to someone else
	- Rubber duck debuggin

Manual fault seeding
####################

Conscious introduction of faults into the program
 - Run tests
 - Minimum 1 test should fail
If no test fails, then a test is missing
	- Possible even with 100% line coverage
	- Asserts functionality coverage

.. _mutation-test:

Mutation testing
################

.. figure:: mutation_testing.png
	 :alt: mutation_testing

Metamorphic Testing
###################

test changes and check whether change is sufficient

.. figure:: metaphoric_test.png
	 :alt: metaphoric_test


.. figure:: Lighting_test.png
	 :alt: 	 lighting_test
