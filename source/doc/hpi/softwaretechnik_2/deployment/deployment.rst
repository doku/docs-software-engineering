==========
Deployment
==========

"Working software is the primary measure of progress"—Agile Manifesto
Is software that is not deployed working? (DevOps)

DevOps
======

Software needs to be operated(developer vs. Admin), a task which needs to be
automated with ever shrinking Deployment Cycles.

.. figure:: devops.png
	 :alt: devops

**DevOps:** Formalized process for deployment, Focus on communication, collaboration,
and integration between Dev and Ops

Terminology
===========

- Release
  - planned state of application
- Verion
  - every commit could be a version
- Build
  - attempt to implement a release (Snapshot of application)
  - Version number is “<Release Number>.<Build Number>”
- Environment
  - A System on which the application can be deployed and used
- to promote
  - to deploy a build on the next Environment
- to release
  - to promote a build to Produnction, thereby finishing the release


Overview of Environment
=======================

.. figure:: environments.png
	 :alt: environments

.. figure:: workflow.png
	 :alt: workflow

.. figure:: devops_process.png
	 :alt: devops_process

.. note::
  **Builds are immutable** If changed, previous testing was pointless,
  Even the smallest change has to go through all environments

Application hosting options
===========================

.. figure:: hosting_options.png
	 :alt: hosting_options

- PaaS Platform as a Service: Providers deliver OS, execution environment, database, web server, monitoring, etc.
  - Heroku, Azure Compute, Google App Engine
  - minimal effort, low knowledge Requirements
  - provider SLA(Service level agreement) may differ from yours (downtime, support, performance)

- IaaS Infrastructure as a Service: Providers deliver virtual private servers (VPS) with requested configuration
  - Amazon EC2, Google Compute Engine, Rackspace Cloud, DigitalOcean
  - flexible regarding hosting environment
  - its still a VM, admin know-how required
  - now hardware management needed
  - on demand scaling

- Dedicated Hosting: Providers allocate dedicated hardware, classical approach
  - Hetzner, OVH, Rackspace, Host Europe
  - complete control, no virtulization
  - dedicted SLA
  - no easy scaling
  - admin effort

- Own Datacenter
  - complete control, custom server designs
  - huge up front costs, expensive expanding
  - 24/7 support from admin needed


Automated Environment Setup
===========================

for initial experiments: standardized VM using Vagrant
automated VM configuration: Provisioning tools: e.g. chef.io / puppet
Formalize software install and configuration state into recipes

deployment
==========

.. figure:: deployment_travis.png
	 :alt: deployment_travis

monitoring Server
=================

- Uptime Robot—HTTP GET / ping every 5 mins (https://uptimerobot.com/)
- Nagios—Monitor infrastructure, down to switches and services (http://nagios.org)

continuous deployment
=====================

Deploy 50 times a day?

- shorter feedback loop
- use canery deployment with analytics to monitor effect of Changes (A/B testing )
- reduces amount of code changes per release
- only feasible with extensive set of good tests
- CI needs to be fast
-


Migration gibt an, wie man von einer Version des Schemas zur nächsten kommt und wieder zurück
