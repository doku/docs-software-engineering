Ruby on Rails - Introduction
=============================

Web	application	development	framework	written	in	Ruby
- http://rubyonrails.org/
Philosophy
- "Don't	repeat	yourself"	–	DRY
- Convention	over	Configuration	–	there	is	"the	Rails	way"
- RESTful	architecture
- Everything	in	its	place

Basic Architecture
------------------

.. figure:: rails_core_components.png
	 :alt: rails_core_components

.. figure:: rails_architecture.png
	 :alt: rails_architecture

cheatsheet: https://gist.github.com/mdang/95b4f54cadf12e7e0415


Basic Principes
_______________

Requests are routed to controllers which handle requests. The routing is defined in `config/routes.rb`

A resource is the term used for a collection of similar objects, such as articles, people or animals. You can create, read, update and destroy items for a resource and these operations are referred to as CRUD operations.
Rails provides a resources method which can be used to declare a standard REST resource.


Controller
__________
A controller is simply a class that is defined to inherit from ApplicationController. It's inside this class that you'll define methods that will become the actions for this controller. These actions will perform CRUD operations on the articles within our system.
