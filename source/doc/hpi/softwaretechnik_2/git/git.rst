===
Git
===

Commands
========

.. figure:: git_diagram.png
	 :alt: git_diagram

.. figure:: git1.png
	 :alt: git1

.. figure:: git2.png
	 :alt: git2

.. figure:: git3.png
	 :alt: git3


.. figure:: rebase.png
	 :alt:    rebase


.. figure:: rebase2.png
	 :alt: rebase2

.. figure:: cherry_pick.png
	 :alt: cherry_pick

Get state of certain commit: ``git checkout <commit tag>``

rules
=====

- Never merge in master or release branches
- Never break build in shared branches

resources
=========

- Git cheat sheet – by Github
 - https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf
- Git FAQ – answers to common questions
  - http://gitfaq.org/
  - https://git.wiki.kernel.org/index.php/Git_FAQ
- Git pretty – troubleshooting flowchart
  - http://justinhileman.info/article/git-pretty
- How to undo (almost) anything with git
  - guide by Github https://github.com/blog/2019-how-to-undo-almost-anything-with-git one

Git Backgroundknowledge
=======================

Operations
----------

.. figure:: operations.png
	 :alt: operations

file status
-----------

.. figure:: file_status.png
	 :alt: file_status

Git objects
-----------

- Blob
  - content of a file
- Commit
  - set of changes
  - references tree object
  - metadata
  - 0..* parent commits
- Tag
  - reference to other objects
- Tree
  - File structure
  - references Blobs

Commit in detail
################

.. figure:: commit_in_detail.png
	 :alt: commit_in_detail
