======
Kanban
======


.. note::
  Motto: “Stop Starting, Start Finishing”

Remove bottlenecks:

.. figure:: bottenecks.png
	 :alt: bottenecks

Lean Principles
===============

**Eliminate Waste:**

- Anything not delivered to the customer
- Artifacts that do not deliver a business value

**Amplify learning**

- “Try-it, test-it, fix-it” rather than “do it right the first time”
- shorter iteration cycles

**Decide as late as possible**

- Avoid up front design decisions, make choices when information is available

**Deliver as fast as possible**

- Working system at every iteration, fast feedback cycle

**Empower the team**

- Motivate the team, self-organization
- “Find good people and let them do their own job”

**Build integrity in**

- Maintain the consistency of a system’s design
- E.g. through refactoring, automated tests, complete build system

**See the whole**

- Focus on overall progress of the project
- Strong common sense



Core Kanban Practices
=====================



**Limit work in progress**

- Limits for each column
- pull from previous columns
- reduce context switching (waste)

**Visualize**

- postits as tickets travel on board from left to right

**manage flow**

-  Measure length of queue, average cycle time and throughput
- Identify bottlenecks and allow planning

**Make policies explicit** 

- Create explicit shared understanding of rules and assumptions
- E.g. what columns mean, Definition of Done, which ticket to pull next

**Implement feedback loops **

- Process of continuous improvement (“kaizen” in Japanese)
- Don’t wait for feedback, build it into the process

**Improve collaboratively, evolve experimentally**

- Try things out, evaluate

push vs. pull
-------------

How a team handles the inventory & scheduling of work items
**Push production:** based on forecasted demand, schedule
**Pull production:** based on consumed products, take only what is needed, process immediately

Scrum Sprint Planning: Push

- Forecasted demand (business needs)
- Estimated capacity of team

Kanban: Pull

- No need for planning, no queue to push into
- Stories worked on based on actual demand and actual capacity

push and pull are subjective and it can be argued both ways for scrum.

Kanban Board
============

.. figure:: kanban_board.png
	 :alt: kanban_board

post mortem
===========

If something broke then "stop the line" and everybody tries to help resolve the issue.

Catoon
======


.. figure:: catoon1.png
	 :alt: catoon1

.. figure:: catoon2.png
	 :alt: catoon2

Metrics
=======

.. figure:: metrics.png
	 :alt: metrics
