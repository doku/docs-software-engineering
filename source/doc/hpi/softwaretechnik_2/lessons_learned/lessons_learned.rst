============================
Lessons learned SWT2 Project
============================


Scrum teams
===========

- have software architect in Scrum teams
- have more teams than people, so additional teams like design, architect, devops..
- have head PO als leader (like SM for Scrum)

process
=======

- use process and use case diagrams to illustrate processes and talk about them, for refinement
- use requirement engineering
- define MVP clearly
- define value hypothesis and test it
- single communication channel
- protocols
- punctuality
- retros for POs
- timer for working on US
- rules/roles explicit (who writes protocol)
- early design
- use epics goals
- have area product owners
- models/ workflows for synchronization and documentation
  - structure User stories with workflows
- loosely coupled and highly cohesive team responsibilities
time effective
decisions
use scetch

Feedback
========

- testquality bad
- protocol for customers
- architecture
- frequent and small questions to customers
- involve customer prior to meeting for questions and information as preparation

Design tool
===========

https://www.sketchapp.com
