Getting started with VIM
========================

Vim is the most common text editor on Unix systems.

Vims main concept is, that it has 3 modes which determines the editors reaction to keyboard input.
It has an **insert mode, command mode, and last-line mode**. Let's start with the default mode you'll see when you start up Vim--command mode.

When you run vim filename to edit a file, **Vim starts out in command mode**. This means that all the alphanumeric keys are bound to commands, rather than inserting those characters. Typing j won't insert the character "j"--it will move the cursor down one line. Typing dd will delete an entire line, rather than inserting "dd."

**To enter the insert mode, type i** (for "insert") and now the keys will behave as you'd expect. You can type normally until you want to make a correction, save the file, or perform another operation that's reserved for command mode or last-line mode. To get out of insert mode, hit the Escape key.

Once you press Escape, you're in command mode again. What if you'd like to save your file or search through your document? No problem, press : and Vim will switch to last-line mode. Vim is now waiting for you to enter a command like \:w to write the file or \:q to exit the editor.

The Basics of Moving in Vim
---------------------------
The first thing you'll want to learn is how to move around a file. When you're in command mode, you'll want to remember the following keys and what they do:

h moves the cursor one character to the left.
j moves the cursor down one line.
k moves the cursor up one line.
l moves the cursor one character to the right.
0 moves the cursor to the beginning of the line.
$ moves the cursor to the end of the line.
w move forward one word.
b move backward one word.
G move to the end of the file.
gg move to the beginning of the file.
\`. move to the last edit.

.. note::prefacing a movement command with a number will execute that movement multiple times.

So, if you want to move up six lines, enter 6k and Vim will move the cursor up six lines. If you want to move over five words, enter 5w. To move 10 words back, use 10b.

https://vim.rtorr.com/

Commands
--------
