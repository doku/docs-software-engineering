============
Random Stuff
============

.. toctree::
   :maxdepth: 2
   :glob:

   google/*
   pitches/*
   vim/*
