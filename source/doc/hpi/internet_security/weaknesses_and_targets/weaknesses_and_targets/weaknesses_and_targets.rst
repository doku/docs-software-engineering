Weaknesses and Targets
======================

Human and Technical Failures
----------------------------

Due to enormous complexity of todays networks and software components contain many errors and design flaws. Systems are too complex to test all possible situaions and guarantee accurate operations.
These errors and flaws are serious security risks due to potential **exploits**(misuse of errors/leaks).
Nevertheless, most break-ins into networks arise from **human failures** as well was from **organizational issues**(like malicious mails, leak of awareness).

Defective Design
################
Many security problems of the Internet result from missing/inadequate security mechanisms.
- IP, TCP, UDP, FDP, HTTP were designed without any security mechanisms
  - FDP and Telnet transmit passwords in plaintext
  - TCP/IP cannot predict the nodes over which the packets are transferred

Many further problems occur in server implementation and internet technologies(Java Script, Flash, Java Applets) or operating systems, which could be abused by attackers.

Misconfigured Servers and a leak of professional knowledge. Default accounts and standard passwords, or in github published configuration details are a major problem.

Social Hacking/engineering
##########################

Social hacking is one of the most common and successful techniques for network break-ins and often caused by missing user awareness.

Reverse Social Hacking/engineering
##################################

Attackers try to present themselves as an authority , e.g. administrator, offering help and services, so that the targets come to them instead of the original admin.

Defective Organization
######################
