Weaknesses of Internet Protocols
================================

Security aspects played a minor role during the initial design and implementation of the Internet and its TCP/IP protocols.
Besides security leaks other problems are: small address space, hardly any possibilities to prioritize real-time data, few possibilities for encryption and authentication
Security leaks of TCP/IP protocols are often used to attack protocols of the application layer, such as FTP, DNS, SMTP, NNTP, .. and  applications themselves (above application layer)


Attack scenarios on Link layer
------------------------------
ARP – Address Resolution Protocol – serves for allocation of Internet addresses (IP) to corresponding hardware addresses (MAC)
*  ARP attacks

Attack scenarios on Internet layer:
* IP-Spoofing
* ICMP attacks
* Routing attacks
* IP-fragmentation attacks
* IP-Bombing

Attack scenarios on TCP layer:
* SYN-flooding
* TCP sequence number attacks
* Abort of TCP-connections
* Takeover – hijacking – of TCP-connections

Attack scenarios on UDP-layer:
* UDP-spoofing

Attack scenarios on SSL/TLS:
* MITM attacks on SSL/TLS

Attack scenarios on application layer (resp. above application layer) :
* Attacks on DNS systems
* Attacks on Email systems
* Telnet-attacks
* FTP-attacks
* Attacks on web servers
