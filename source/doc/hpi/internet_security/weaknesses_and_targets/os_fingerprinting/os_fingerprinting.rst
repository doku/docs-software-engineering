OS fingerprinting
=================

OS fingerprinting is a methode to determine the type and version of the OS. This is used to utilize known vulnerabilities of the OS for attacks.
Although OS Fingerprinting does not always provide a 100% correct result, it is still an important step when planning
subsequent attack steps
Generally you are trying to use as many methods as possible to collect indicators


TSN Telnet Session Negotiation
------------------------------
Telnet is a protocol used on the Internet or local area networks to provide a bidirectional interactive text-oriented communication facility using a virtual terminal connection.
User data is interspersed in-band with Telnet control information in an 8-bit byte oriented data connection over the Transmission Control Protocol (TCP).

To determine the OS you just try to create a telnet connection.
If successful the System shows banner with information about OS and then asks for login.

` telnet <host> <portnumber> `

example:

.. code-block:: ruby

  telnet jupiter 25 220 jupiter.mca.ac ESMTP Sendmail 8.9.3/8.9.3;
  Sun. 19 Mar 2000 08:38:20

Preventive measure:
###################

Configuration file provides possibility to customize banners. Administrators should therefore completely turn off banners or
modify them in order to hide such mentioned information

TCP Stack Fingerprinting
------------------------

By **sending packets to a host and comparing the responses with a database**, you can determine the OS relatively accurately.
The basic concept works as follows: Stacks of several systems differ in some small details, e.g. in responses to non-standardized requests
Responses to certain carefully selected requests provide a pretty exact copy – „fingerprint“ - of the system.

**nmap** is a free port scanning software with fingerprinting functionality (http://nmap.org/book/osdetect.html)

`nmap –v –sS –O hostX.hpi.uni-potsdam.de`

Fin probe
#########
Send a FIN packet and wait for response. Correct behavior is not to respond but many broken implementations such as
MS Windows etc. send a RESET back

BOGUS flag probe
################

Set an undefined TCP flag in the TCP header of a SYN packet and check the responses.
Linux boxes prior to 2.0.35 keep the flag set in their response other operating systems reset connection when they get a
SYN+BOGUS packet

TCP ISN Sampling
################

Find patterns in the initial sequence numbers chosen by TCP implementations when responding to a connection
request

IPID Sampling
#############

Most operating systems increment a system-wide IPID value for each packet they send. Others use a random IPID and some use an IPID of 0 in many cases where the “Don’t
Fragment” bit is set

TCP Timestamp
#############
Check sequence of TCP timestamp option values. Sometimes this option is not supported by systems or other increment the value at different frequencies

Don’t Fragment bit
##################

Many OSs set DF bit on some packets they send, but not all
OSs do this and some do it under different conditions

TCP Initial Window
##################

Check window size on returned packets

ACK Value
#########

Check sequence number of ACK packets. Most implementations will set the ACK to be same as ISN but
other OSs will send seq +1 or even a random number

TCP/IP Timeout Detection
########################
Because TCP timeout values are merely loosely specified in RFCs, OSs mostly use their own parameters

Tool “RING” uses this circumstance to identify an OS.
It causes timeouts and then analyzes delay between successive SYN/ACK resends
Procedure

- Sending a SYN segment to target (like normal TCP connection)
- Target sends back a SYN/ACK segment
- SYN/ACK is ignored -> no ACK
- Target sends from time to time new SYN/ACK segments
- time is measured between these segments and thereby the OS can be determined

Application scanning
--------------------

Another scanning tool is AMAP – Application MAPper
Applications normally use standard ports to offer a certain service
If the service is configured that the application listens on a different port than the standard port, some scanning tools can’t
determine the right service.
AMAP is also able to detect services which are not running on the standard port by sending trigger packets and comparing the
responses against a database (containing triggers and responses)

Passive Fingerprinting
----------------------

Instead of sending special packets to a remote target host and analyzing the response packets, passive fingerprinting uses a sniffer
to quietly map a network.
OSs use different TCP stack implementations, so that the TCP/IP flag settings may vary

- Initial TTL
- Initial packet size
- Window size
- Maximum segment size
- DF flag

Tools like p0f (www.stearns.org/p0f) can fingerprint a system without sending packets to a host.
Source host can’t recognize that machine is fingerprinted
Firewalls can’t prevent this, because only the outgoing packets are analyzed (no scan of the host necessary)
p0f analyzes parameters of packets

Fuzzy OS Fingerprinting
-----------------------

Use of a fuzzy matching system to correlate received results with a known fingerprints’ signature database.
Different from the method used in most tools where they apply a strict static signature matching to determine the OS. If a 100% match
is not found, than there is no match and no result.
The tool “Xprobe2” (http://sourceforge.net/apps/mediawiki/xprobe/) uses a fuzzy matching system to identify OS
Xprobe2 is based on a simple matrix representation of the scan (or scans), and the calculation of ‘matches’ by simply summing up
scores for each ‘signature’ (OS)

The ‘score’ value can take one of the following values:  YES(3) ROBABLY_YES(2)  PROBABLY_NO(1)  NO(0)
The OS with highest total score will be declared as the final result (see Present_and_Future_Xprobe2-v1.0.pdf on http://www.sys-
security.com)
