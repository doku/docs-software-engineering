Attacks
=======

Spoofing
--------
Technique used to pretend a fake identity(i.e. fake IP, email), mainly used to prepare or obfuscate other attacks

IP Spoofing
###########

The Attacker sends data packets with a faked sender address from the address domain of the target network with the consequence that
his packets seem to be packets from authorized users

Defacement
----------
Maliciuos change of the appearance of a Website to demonstrate hacking knowledge and security flaws.


Sniffing
--------
Monotoring data transmitted over networks using special software (or sometimes hardware tools).
Sniffers can be extremely dangerous to a networks security, because they are almost impossible to detect.
The goal is to obtain passwords or system identifications.

(Distributed) Denial of Service Attacks
---------------------------------------
Computers with access to the Internet get bombarded with numerous IP-packets simultaneously.
As a consequence normal access becomes difficult or impossible.

SYN-Flooding
############

Malware
--------

Viruses
#######

destructive piece of code loaded onto computer without owners knowledge.
Most viruses replicate themselves locally

Worms
#####

program or algorithm that replicates itself over computer networks and usually performs malicious actions

Trojan horses
#############

destructive program that is masquaraded as a usefull application. Unlike viruses, Trojan horses dont replicate themselves.

Exploitation
------------


Buffer overflows
----------------

Others
------

cold boot attack

social engineering
Reverse social engineering
