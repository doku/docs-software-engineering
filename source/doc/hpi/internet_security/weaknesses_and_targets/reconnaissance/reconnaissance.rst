Reconnaissance
==============

Before an attack is executed there is always a reconnaissance phase that the attacker needs to find and determine targets, potential security leaks, or defense capabilities of the victim by gathering
information about the target system.

Offline
-------

-human intelligence / social engineering
-wiretapping, e.g. Video, Audio, Phone tapping
-Hardcopy material analysis: e.g. searching in system manuals

Online
------
-Web search
-Web site analysis, e.g., personal homepages, weblogs, profiles on public social sites
-IT resource mapping, e.g. port scanning,  vulnerability scanning

passiv recon
############

Using third party resources to perform queries
first step of gathering information(Web hosting, IP Addresses, Employee names)

Utilities
_________
 -Host/ nslookup: Request is forwards to the appropriate DNS server of the target domain(based on this guess other IPs)
 -whois: gather information about domains (owner, contacts, IP addresses, telephone numbers)
 -traceroute/ pathping: is used to recognize the path of a packet to its receiver. It sends multiple IP-packages (of the type ICMP echo request) to the host.
  These packages have an increasing time to live and thereby send all the Hops of the packages on the path to the host back.

These information can be uses used as a starting point for further attacks (email attacks, social engineering)

active recon
############

In contrast to passive reconnaissance, in case of active recon information is gathered by contacting the target network resources
directly

by E-mail
- send an e-mail to a non-existing person of company
- a response message (550 User Unknown) is send to original sender containing interesting information about mail server system(mail server system and version, email topology, existence of antivirus defenses, operating systems, email addresses)

by  Web site analysis
- investigating website’s structure and technology, e.g. URLs, URL extensions, comments in HTML code
- simple access tests, e.g. access directory listings

by FTP
-if company operates an anonymous FTP site, perhaps there are some interesting documents, e.g.,  forgotten confidential files or MS Word documents with hidden meta information…

web recon
#########

- web search to gather information about victim google`site:company_name`
- Office files contain sensitive data hidden in meta information
- search job offerings to determine technologies based on job requirements
- archive.org to check out old versions of websites
- social networks to connect to victims and ask for information (123people.com is a people search engine)
