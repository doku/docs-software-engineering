Attacks on Accounts and Passwords
=================================

Authentication systems are used for identifying and authorizing users and permit access to computer systems.
Most authentication systems identify a user by username and password.
User-password authentication is a weak authentication method since it only requires information of **"something you know"** from users
It is vulnerable to such attacks as Guessing, Dictionary Attacks, Keystroke Monitoring, Network Sniffing, Man-in-Middle attack,
Social Engineering attack, etc.
Authentication is the proof of identity, Authorization is the access control based on Authentication
strong authentication
---------------------
Higher security is guaranteed by using means for strong authentication, e.g., one-time-passwords, smartcards, biometric identification attributes like fingerprint, voice, iris, gesture, …
The strong authentication demands many additional information
- something **one is** (Biometrics)
- something **one has** (smartcards)
- something **one can do** (press a button)

Strong Authentication is also named two/multiple factors authentication

attacks on weak passwords
-------------------------

Password guessing
#################

Guessing of login/password-combinations is rather trivial but often the fastest method to get unauthorized access to a computer system:
Use of known or frequently used logins (e.g. default settings), e.g. admin/admin, guest/guest, field/service, root/toor

This method works usually when chosen passwords are very simply structured
Fast guessing is possible since many of the used passwords have been very simply chosen

Password cracking
#################

systematical password guessing using file with encrypted passwords
Attackers analyze system files with encrypted passwords. Although a direct decryption is not possible (use of trapdoor functions), special
cracking programs can guess at least some passwords quickly

„Crack“ or „L0phtcrack“(http://www.l0phtcrack.com) uses same encryption algorithms like operation system. They systematically encrypt a long list of words and additionally use a brute force mode which also encrypts all combinations of letters, numbers and special symbols. Finally, these programs compare results with entries in the encrypted password file

.. figure:: password_cracking.png
	 :alt: password_cracking

A very popular password cracker is John the Ripper (http://www.openwall.com/john). It combines several cracking modes and supports different hash types like DES, MD5, Blowfish…

Another famous tool is Hydra (http://www.thc.org/thc-hydra). It’s a login hacker tool that supports many protocols (ssh2, telnet, ftp, vnc, …)
Cracker-Jack does not only encrypt a wordlist, but also checks several modifications of words (case sensitivity, letter permutations,...)

.. figure:: cracker_jack.png
	 :alt: cracker_jack

Rainbowtables
_____________

A rainbow table is a precomputed table for reversing cryptographic hash functions, usually for cracking password hashes.
It is a practical example of a space–time tradeoff, using less computer processing time and more storage than a brute-force attack which calculates a hash on every attempt, but more processing time and less storage than a simple lookup table with one entry per hash.

countermeasures
'''''''''''''''
Add random value (Salt) to hash functions, e.g., add a 10-byte random value to password and store it with the hash: salt,MD5(password+salt).
Pre-calculated rainbow tables useless, as the effective password size is increased by 10 characters and each password has its own salt

.. figure:: rainbowtable.png
	 :alt: rainbowtable

The rainbowtable works as follows:

You start with a password hash function and an finite set of passwords P.
The goal is to precompute a data structure that, given any output h of the hash function, can either locate an element p in P such that H(p) = h, or determine that there is no such p in P. The simplest way to do this is compute H(p) for all p in P, but the space complexity would be linear to the length of your password list.

Hash chains are a technique for decreasing this space requirement. The idea is to define a reduction function R that maps hash values back into values in P. Note, however, that the reduction function is not actually an inverse of the hash function.
It is important that the reduction function maps the hashes to "usefull" passwords from the password list.

To generate the table, we choose a random set of initial passwords from P, compute chains of some fixed length k for each one, and store only the first and last password in each chain. The first password is called the starting point and the last one is called the endpoint.

Given a hash you can determine the plaintext password as follows:

1. Example `0xI`
''''''''''''''''
1. lookup in all endpoints and found in chain 1
2. chain is known and we are thereby able to calculate the plaintext using the start point :math:`R_2(H(R_1(H_1(BMW)))) = BMW`

2. Example `0xF`
''''''''''''''''

1. lookup in all endpoints and not found
2. assume Password is in P and will be found in rainbowtable. :math:`H(R_2(0xF)) = 0xF' = 0xJ`
3. lookup in all endpoints and found in chain 2
4. It is known how many steps we took to reach the endpoints and thereby know how many we have to take from the startpoints to reach the password. :math:`R_1(H(VW)) = Nissan`

3. Example `0xD`
''''''''''''''''
1. lookup of :math:`0xD` in endpoints
2. lookup of :math:`H(R_2(0xD))` in endpoints
3. lookup of :math:`H(R_3(H(R_2(0xD))))` in endpoints
4. based on the found chain and the hash nesting level you take the start point of this chain and calculate the plaintext password with the correct number of hash/reduction operations.


Good Passwords
______________

- difficult( not able to guess from wordlists)
- easy to remember (so there is no need to write it down)
- periodically changed ( protection against brute-force-attacks , protection against Man-In-The-Middle (MITM) attacks)
- well protected (encrypted/ inaccessible) storage

* Change passwords every 3-6 months or even more frequently
* Passwords should be case sensitive
* Passwords should contain numbers and special characters (. , : ; ...)
* To remember passwords more easily, it is useful to form abbreviations
* Combination of several words
* Minimum length of 8 characters  as longer the passwords as higher their security (since it becomes exponentially more difficult to crack)

.. note::
  Good passwords are useless, when infiltrated sniffing- and monitoring-programs can log every keystroke

Test own passwords with freely available password-crackingprograms and wordlists,
e.g. from following sources  http://www.openwall.com/john/  http://www.openwall.com/wordlists/ http://www.oxfordwordlist.com/

Password sniffing
#################

- Capture network packets and analyze them by filtering out usernames and passwords
- Installation of small programs - Keylogger -, which permanently control every keystroke in the background and save strings like „login ...“ or „passw ....“

Password Phishing
#################
Phishing is the attempt to obtain sensitive information such as usernames, passwords, and credit card details (and money), often for malicious reasons, by disguising as a trustworthy entity in an electronic communication.

Victim user is forced to visit faked sites of social network portals, online banks, online shops, etc., which look similar as the real ones, by links published in online social communities (e.g., BBS,
blogs, etc.) or contained in spam emails

Fishing
_______
- By means of social engineering methods users are requested to reveal their passwords
- By means of technical methods(like MITM, Trojan horses, Evil twin attacks)

Evil Twins attack Access Point (AP) Impersonation
'''''''''''''''''''''''''''''''''''''''''''''''''

Victim user automatically connects to a fake WLAN access point (AP) which has same SSID as the one he/she expects to connect when a faked AP is nearby and
therefore has a stronger signal
Identities from WLANs (SSID) can agree, e.g., two different WLANs can be named “hpi_staff” at the same time
No authentication mechanism is deployed for being used by the user to authenticate AP
This vulnerability provides a opportunity for attacker to set up a faked AP with the same SSID and put it closer to the victims
Usually, a Radius Server is affiliated with an AP to authenticate users at that beginning due to no logic connection. Users build TLS
connections to the server by tunneling AP for being authenticated
Attacker can also have a Radius Server for the faked AP with a certain patch to record/calculate login information of victims, which can be
furtherly used to access the real AP
countermeasure:  Client should have AP‘s certificate or public key for authenticating it

Salt and Pepper
###############

Salt and Pepper are random Strings which are concatinated with the password to make hash cracking more difficult
Salt is unique for each password
Pepper is unique for each application
Using these Rainbowtables become effective, because they have to be calculated for each salt and pepper combination, eliminating their benifit.

Keylogger and Trojan horses
###########################

that have been implanted on victim’s system for login/password-monitoring

Social engineering hacking
##########################
