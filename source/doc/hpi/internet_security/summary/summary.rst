Summary
=======



- Exercise contents (sheets and sessions)
- Hands-on contents
- Previous exams

General
 - Security goals
 - Human factor
 - General structure of Internet
 - Virus/ worm/ trojan horse
 - Regular expressions
 - Log files
 - Attack types
 - Risk analysis
 - History

Reconnaissance
 - (OS) Fingerprinting
 - Types
 - Scan techniques
 - Tools

Passwords and cryptography
 - Hash functions
 - Common attacks
 - Password cracking/ countermeasures
 - Tools
 - Rainbow tables
 - Good passwords
 - Alternative authentication schemes

Internet Protocols
 - OSI / TCP/IP stack/ layer
 - ARP protocol/ spoofing/ broadcast
 - FTP protocol/ bouncing/ spoofing
 - DNS protocol/ spoofing/ poisoning
 - SMTP protocol/ relay
 - TLS/SSL protocol/ MITM (corresponding protocols)
 - EGP
 - DoS/ DDoS approaches
 - IP-bombing
 - SYN-Flooding
 - ICMP routing
 - Ping of death
 - TCP / IP / HTTP / UDP/ ...
 - Attacks ...

Exploitation
 - Buffer overflows
 - Stack layout / Intel Assembler
 - Counter measures / mitigations
 - Teardrop

Bonus
 - Knowledge
 - Creativity
 - Imagination
