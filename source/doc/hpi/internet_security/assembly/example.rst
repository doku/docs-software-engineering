Disassembly Example
===================

.. code-block:: ruby

  .text:08048422 ; S U B R O U T I N E 
  .text:08048422
  .text:08048422
  .text:08048422 programm_start  proc near               ; DATA XREF: start+17o
  .text:08048422
  .text:08048422 var_30          = dword ptr -30h
  .text:08048422 var_2C          = dword ptr -2Ch
  .text:08048422 var_28          = dword ptr -28h
  .text:08048422 var_1C          = dword ptr -1Ch
  .text:08048422 var_14          = byte ptr -14h
  .text:08048422 arg_0           = dword ptr  4
  .text:08048422
  .text:08048422                 lea     ecx, [esp+arg_0]
  .text:08048426                 and     esp, 0FFFFFFF0h
  .text:08048429                 push    dword ptr [ecx-4]
  .text:0804842C                 push    ebx
  .text:0804842D                 push    ecx
  .text:0804842E                 sub     esp, 24h        ; char * ; clear space vor local variables
  .text:08048431                 mov     [esp+30h+var_30], offset aXxxxxxxxxxxxxx ; write output to stack top"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"...
  .text:08048438                 call    _printf print stacktop
  .text:0804843D                 mov     [esp+30h+var_30], offset aKey ; write output to stack top "Key: "
  .text:08048444                 call    _printf ; print stacktop
  .text:08048449                 mov     eax, ds:stdin ; write handle for input stream in eax
  .text:0804844E                 mov     [esp+30h+var_28], eax ; esp + 8h ; save handle
  .text:08048452                 mov     [esp+30h+var_2C], 10h ; esp + 4h ; could be the character limit for fgets
  .text:0804845A                 lea     ebx, [esp+30h+var_1C] ; ebx is now pointer to esp + 14h
  .text:0804845E                 mov     [esp+30h+var_30], ebx ; pointer to esp +14h is now on top of the stack -> input will be written here
  .text:08048461                 call    _fgets ; will read characters from one line until its one short of the limit, then it adds an EOL
                                          ; fgets wants the parameters on the stack as follows: starting from the stack top:
                                          ; address of character buffer,
                                          ; character limit (actual number)
                                          ; file handle
                                          ;
                                          ; fgets read input including the "\n" when pressing enter so with the null terminator the password could be 8 characters long
  .text:08048466                 mov     [esp+30h+var_30], ebx ; write pointer to char array to top of stack as input to _strlen
  .text:08048469                 call    _strlen ; eax is standard register for function output
  .text:0804846E                 cmp     eax, 0Ah ; check whether result of _strlen equals 10: INCLUDING the NEW LINE: so actually its 8 long
  .text:08048471                 jnz     short INVALID ; if _strlen is not equal to 10 the password is invalid
                                ; next we will iterate through the char array
  .text:08048473                 mov     edx, 1 ; edx is our running variable and will be increased in the following step by step
  .text:08048478                 lea     ecx, [esp+30h+var_1C] ; ecx is pointer to start of char array
  .text:0804847C
  .text:0804847C loc_804847C:                            ; CODE XREF: programm_start+6Bj
  .text:0804847C                 movzx   eax, byte ptr [edx+8049707h] ; movzx moves the byte ptrs value into eax and zeros the most significant bits

                                      ;Where does the byte ptr point to:
                                      ; exd = 1 : .data:08049708                 db  45h ; E
                                      ; exd = 2 : .data:08049709                 db  38h ; 8
                                      ; exd = 3 : .data:0804970A                 db 0A9h ;
                                      ; exd = 4 : .data:0804970B                 db 0CAh ; -
                                      ; exd = 5 : .data:0804970C                 db 0CCh ;
                                      ; exd = 6 : .data:0804970D                 db  13h
                                      ; exd = 7 : .data:0804970E                 db 0E1h ;
                                      ; exd = 8 : .data:0804970F                 db  78h ; x

  .text:08048483                 xor     [edx+ecx-1], al ; al are the least significant 8 bit of eax, and therefore we xor the first 8bit of our char array[edx-1]
  .text:08048487                 add     edx, 1 ; increase edx to get new char, iterate byte wise over the char array
  .text:0804848A                 cmp     edx, 9 ; sets ZF = 1 if edx == 9
  .text:0804848D                 jnz     short loc_804847C ; while edx < 9 repeat
  .text:0804848F                 mov     ecx, 0 ; sets ecx = 0
  .text:08048494                 mov     dl, 0 ; least significant 8 bit of edx are zeroed. edx should be 0 now
  .text:08048496                 lea     ebx, [esp+30h+var_1C] edx is pointer to start of char array
  .text:0804849A
  .text:0804849A loc_804849A:                            ; CODE XREF: programm_start+84j
  .text:0804849A                 movsx   eax, byte ptr [edx+ebx]
  .text:0804849E                 add     ecx, eax ; accumulates eax of each iteration in ecx
  .text:080484A0                 add     edx, 1 ; increase running variable
  .text:080484A3                 cmp     edx, 8 ; loop over char array, from array[0] to array[7]
  .text:080484A6                 jnz     short loc_804849A
                                 ; loop finished
  .text:080484A8                 movsx   edx, [esp+30h+var_14] ; start of char array
  .text:080484AD                 movzx   eax, cl ; least significant 8 bits moved to
  .text:080484B0                 cmp     edx, eax; compare first character of password with its characters accumulation(after xor)
  .text:080484B2                 jnz     short INVALID
  .text:080484B4                 lea     eax, [edx-61h] ; stack cannary
  .text:080484B7                 cmp     eax, 19h
  .text:080484BA                 ja      short INVALID; unsigned greater than: jumps if CF = 0 and ZF = 0
  .text:080484BC                 mov     [esp+30h+var_30], offset aGoodKey ; "Good key !\n"
  .text:080484C3                 call    _printf ; print top of stack
  .text:080484C8                 mov     eax, 0
  .text:080484CD                 jmp     short loc_80484E0
  .text:080484CF ; ---------------------------------------------------------------------------
  .text:080484CF
  .text:080484CF INVALID:                                ; CODE XREF: programm_start+4Fj
  .text:080484CF                                         ; programm_start+90j ...
  .text:080484CF                 mov     [esp+30h+var_30], offset aInvalidKey ; "Invalid key !\n"
  .text:080484D6                 call    _printf
  .text:080484DB                 mov     eax, 1
  .text:080484E0
  .text:080484E0 loc_80484E0:                            ; CODE XREF: programm_start+ABj
  .text:080484E0                 add     esp, 24h
  .text:080484E3                 pop     ecx
  .text:080484E4                 pop     ebx
  .text:080484E5                 lea     esp, [ecx-4]
  .text:080484E8                 retn
  .text:080484E8 programm_start  endp ; sp = -4
  .text:080484E8
