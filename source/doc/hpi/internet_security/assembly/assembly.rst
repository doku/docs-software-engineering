=================
Assembly commands
=================

Registers
=========
https://www.aldeid.com/wiki/X86-assembly/Registers
EAX
---
EAX generally contains the return of a function. If you see the EAX register just after a function call, chances are that EAX contains the return value of the function.
EAX and EDX are always implied in multiplication and division instructions
32bit

ESP
---
Stackpointer

EBP
---
Basepointer

Used to reference arguments and local variables


Instructions
============
https://www.aldeid.com/wiki/X86-assembly/Instructions

lea
---
The ``lea`` (load effective address) instruction is used to put a memory address into the destination.

.. code-block:: asm

  lea eax, [ebx+8]
    Put [ebx+8] into EAX. After this instruction, EAX will equal 0x00403A48.
    In contrast, the instruction mov eax, [ebx+8] will make EAX equal to 0x0012C140
   +------------------+                  +------------+
   | Registers        |                  | Memory     |
   +------------------+                  +------------+
   | EAX = 0x00000000 |       0x00403A40 | 0x7C81776F |
   | EBX = 0x00403A40 |       0x00403A44 | 0x7C911000 |
   +------------------+       0x00403A48 | 0x0012C140 |
                              0x00403A4C | 0x7FFDB000 |
                                         +------------+

mov
---
The mov instruction is used to move data into registers or RAM. In other words, it is used to read and write into memory.

``mov destination, source``

.. code-block:: asm

  mov eax, ebx
  ;Copies the content of EBX into EAX register. Since EBX equals 0x00403A40, EAX will also equal 0x00403A40 after the instruction.
  mov eax, [ebx+8]
  ;Copies the 4 byte at memory location specified by the the result of the operation [ebx+8] into EAX register. After this instruction, EAX will equal 0x0012C140.

   +------------------+                  +------------+
   | Registers        |                  | Memory     |
   +------------------+                  +------------+
   | EAX = 0x00000000 |       0x00403A40 | 0x7C81776F |
   | EBX = 0x00403A40 |       0x00403A44 | 0x7C911000 |
   +------------------+       0x00403A48 | 0x0012C140 |
                              0x00403A4C | 0x7FFDB000 |
                                         +------------+

  mov     bx, 0C3EEh  ; Sign bit of bl is now 1: BH == 1100 0011, BL == 1110 1110
  movsx   ebx, bx     ; Load signed 16-bit value into 32-bit register and sign-extend
                      ; EBX is now equal FFFFC3EEh
  movzx   dx, bl      ; Load unsigned 8-bit value into 16-bit register and zero-extend
                      ; DX is now equal 00EEh

MOVSX (Move with sign extension) and MOVZX (Move with zero extension) are special versions of the mov instruction that perform sign extension or zero extension from the source to the destination. This is the only instruction that allows the source and destination to be different sizes. (And in fact, they must be different sizes).

MOVSXD: (MOVSXD r64, r/m32) Move doubleword to quadword with sign-extension.


jumps
-----

jnz
___

JNZ is short for "Jump if not zero (ZF = 0)", and NOT "Jump if the ZF is set".
If it's any easier to remember, consider that JNZ and JNE (jump if not equal) are equivalent.
Therefore, when you're doing cmp al, 47 and the content of AL is equal to 47, the ZF is set, ergo the jump (if Not Equal - JNE) should not be taken.

ja and jg
_________
G interprets the flags as though the comparison was signed, and JA interprets the flags as though the comparison was unsigned (of course if the operation that set the flags was not a comparison or subtraction, that may not make sense). So yes, they're different. To be precise,

ja jumps if CF = 0 and ZF = 0
jg jumps if ZF = 0 and SF = OF

cmp eax, edx
ja somewhere ; will go "somewhere" if eax >u edx
             ; where >u is "unsigned greater than"

cmp eax, edx
jg somewhere ; will go "somewhere" if eax >s edx
             ; where >s is "signed greater than"

>u and >s agree for values with the top bit zero, but values with the top bit set are treated as negative by >s and as big by >u (of course if both operands have the top bit set, >u and >s agree again).
