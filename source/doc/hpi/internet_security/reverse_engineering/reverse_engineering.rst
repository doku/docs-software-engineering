===================
Reverse Engineering
===================

Reverse Engineering binary files
================================

Explanation of binary
---------------------

32 bit binary die auf Linux kompiliert wurde
check architecture:

.. code-block:: shell

  dpkg --print-architecture
  sudo dpkg --add-architecture i386 && sudo apt update //support additional architecture
  apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386

If a Permission error occures try to add an executable flag to the file

.. code-block:: shell

  chmod +x binary_file_name
 # then execute it
 ./binary_file_name

IDA
---
https://www.hex-rays.com/products/ida/support/download_freeware.shtml
Speicheradressen umbenennen(rechtsklick rename)
Arrays festlegen
create function

gdb
---


Assembler
---------
https://www.aldeid.com/wiki/Category:Architecture/x86-assembly

Basicblocks

repne
scasb
_atoi: erwartet nullterminierten string: schreibt rückgabewert in eax

imul edx: edx:eax = eax *edx: höherwertigen 32 bit werden in edx geschrieben niedrigen in eax.

mov edx, 55555556h
mov eax, ecx
imul edx -> modulo
