Risk Analysis
=============
  
  .. figure:: host_count.png


  The commercial use of the Internet led to a significant increase in the number of computer attacks and criminal activities.
  Risks may occur in
  - computer systems(infrastructure): non-availability, loss of data, Human Factor (insufficient system configuration, missing backup, intrusion of viruses, faulty operation)
  - Internet connection: loss of data, confidentiallity, Sabotage(Infiltration of malware, operational fault, overloading of services), forgery of the identity
  - intranets of enterprise: 80% incorrect usage of IT-Systems by own staff

Risk Definition
---------------
two different dimensions:

  - expected damage dimension
  - expected frequency of a dangerous event

There are **4 phases of Risk Analysis**

  - Phase 1: Definition of analysis domain (focus analysis only to a specific domain and define interfaces between the different analysis domains)
  - Phase 2: Acquisition of risks

    - Scenario analysis: Construction of hypothetical events, that lead to a security incidents
    - Simulation studies
  - Phase 3: Evaluation of risks

    - cardinal risk evaluation: for every security indident the financial damage is multiplied with occurrence probability per year
    - ordianal risk evaluation
    //TODO
  - Phase 4: Interpretation of the results

Evaluation of Risks
-------------------

Categorical difference between:

  - Attack
  - Security incident


.. figure:: cert.png
	 :alt: cert

Constriction of a **Risk Matrix**, a matrix which has two risk factors
  - Amount of damage
  - propability of occurrence


.. figure:: risk_matrix.png
	 :alt:   risk_matrix


.. figure:: risk_evaluation.png
	 :alt:   risk_evaluation


Basic Risks of Internet
-----------------------

- falsification of messages /forgery of a certain sender (mail sender forgery)
- Online-shopping / banking fraud (Transactions are well secured, but user PCs suffer from insufficient safety)
- Illegal, malicious online business practices (criminals use Internet because of its anonymity and wide reachability)
- Data protection (Data leeks, Internet provider can access entire data stream)
- Legal uncertainty in digital cyberspace (§202c StGB Hackerparagraph makes usage and development of possible attack tools a punishable offence ) Basic Problem: **Global communication - national legislation**
