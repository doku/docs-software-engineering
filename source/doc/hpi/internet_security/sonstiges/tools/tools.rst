Password cracking
-----------------

Password listen ausprobieren
-> Dictionary durchgehen
-> daher keine Passwörter

use tools to crack hashes from passwords

Hacker dumpen/holen sich Passwörter von Plattformen, cracken diese und nutzen diese um sich bei anderen Seiten einzuloggen

John the ripper
###############

john -single
john -wordlist


Rainbow tables
##############

nicht für jeden Salt and Paper berechnen
computational power gegen Speicher tauschen

system das nur pepper verwendet
damit müsste für jedes System, das pepper verwendet


sobald salt verwendet wird müsste für jede Person ein neuer rainbow table berechnet werden

heute macht es nicht mehr viel sinn diese zu verwenden, durch salt and pepper

rainbow table Bestandteile

- Hashfuntion, für die die hashes berechnet wurden, hat immer nur eine. Braucht andere Rainbow table für char und md5
- Reduktionsfunktion so viele schritte wie die chain hat braucht man Reduktionsfunktionen
- initial plaintext
- last hashes per chain

.. figure:: rainbowtable.png
  :alt: rainbowtable

Hashfunktion H

Beispiel


Verschlüsselung
---------------

RSA (Rivest, Shamir, Adleman (1997)):
verschlüsseln public key (e,n)
entschlüsseln: private key (d,n)


Sicherheit basiert auf Faktorisierungsproblem
Anwendung: z.B. Schlüsselaustausch, Übetragungsprotokolle
ist deutlich langsamer als symmetrische Verschlüsselung, wird daher eher zum schlüsselaustiasch verwendet

1. wähle 2 sehr große Primzahlen 600stellen p,q
2. Berechne Produkt n = p*q (RSA modul)
3 Berechne eulerfunktion(n) = (p-1)*(q-1) gibt die Anzahl aller teilerfrenden zahlen zu n an
4. wähle e, sodass 1< e < euler(n)  mit ggt(e, euler(n)) = 1
5. Berechne d, sodass d =mod e^-1mod(euler(n))
e\*d + a\*euler(n)= 1
d > 0

Beispiel

P = 11, q = 13
n = 11*13 = 143
euler(n) = 10*12 = 120

120 = 2\*2\*2\*3\*5
wählen e = 7


keys berechnen


symmetrisch
