Shell Commands
==============

Textverarbeitung
----------------
head
____

Mit head (engl.: "Kopf") geben Sie die ersten Zeilen einer Datei am Bildschirm aus. Standardmäßig schreibt der Aufruf head datei.txt die ersten zehn Zeilen auf den Bildschirm. Die Option -n nummer verändert diese Vorgabe. Die Konfigurationsdateien im Verzeichnis /etc enthalten in den ersten Zeilen häufig den Namen und Verwendungszweck der Datei. Um von sämtlichen dort liegenden Files die ersten drei Zeilen auszugeben, verwenden Sie den Aufruf head -n 3 *.conf.
Praktisch: Lassen Sie head auf mehr als eine Datei los, fügt das Tool eine optische Trennung ein (Listing 1). Wer auf dieses Feature verzichten möchte, verwendet den Parameter -q (für --quiet).

tail
____

Das Kommando tail (engl.: "Ende, Rest") arbeitet ähnlich, gibt aber, wie der Name vermuten lässt, die letzten Zeilen einer Datei aus. Auch hier dürfen mehrere Dateien zur Ausgabe übergeben werden. Neben dem Parameter -n nummer (default ist wie bei head zehn) wird oft die Option -f (Langform: --follow) verwendet. Damit kann man sich das Ende von Dateien anschauen, die immer weiter anwachsen, wie beispielsweise Logfiles. So zeigt tail, auf ein ISDN-Logfile losgelassen, wer gerade durchklingelt

cat
___
Das Programm cat verkettet (engl.: "to concatenate") nicht nur mehrere Dateien zu einer, es eignet sich auch dazu, einzelne Files auf der Standardausgabe anzuzeigen. Der einfache Aufruf cat datei.txt bringt den Inhalt der Datei auf den Bildschirm.
Mit der Option **-n** kann zu beginn jeder Zeile eine **Zeilennummer** ausgegeben werden
Das anzeigen von **binären Formaten** mit *cat* führt oft zu **unleserlicher Aus und Eingabe** Abhilfe schafft hier das Programm reset: Tippen Sie (blind) reset am Prompt und drücken Sie [Return], um das Terminal zu reparieren. Falls auch das nicht hilft, probieren Sie es mit echo [Strg-v][Esc][c][Return].

Häufig wird cat dazu verwendet, Dateien durch eine Pipe an ein anderes Programm zu schicken. So lässt sich eine Liste mit cat ausgeben und direkt an das Kommando sort [1] weiterleiten: **$ cat liste.txt | sort**

Die Ausgabe von cat lässt sich mit den für Unix typischen Mitteln umlenken: mit dem Operator > schreiben Sie den Output in die neue datei2:
**cat datei1 > datei2**
Das funktioniert auch mit mehreren Dateien. Dazu führen Sie die Dateinamen einfach nacheinander auf:
**cat datei1 datei2 > datei3**
Existiert datei3 noch nicht, wird sie angelegt. Ist sie allerdings schon vorhanden, überschreibt cat sie einfach. Mit einem anderen Operator bringen Sie cat dazu, **an vorhandene Dateien anzuhängen**:
**cat datei1 datei2 >> datei3**

less
----

less ist ein pager mit dem Dateien angezeigt werden können. Zum verlassen des Pager muss **q** gedrückt werden.

awk
___
AWK is a programming language that is designed for processing text-based data, either in files or data streams, or using shell pipes. In other words you can combine awk with shell scripts or directly use at a shell prompt.
The basic format of an awk command looks like this:

..code-block::

  awk 'pattern {action}' input-file > output-file

This means: take each line of the input file; if the line contains the pattern apply the action to the line and write the resulting line to the output-file. If the pattern is omitted, the action is applied to all line. For example:

..code-block::

  awk '{ print $5 }' table1.txt > output1.txt

You can access the first, second, and third column, with $1, $2, $3, etc.
By default columns are assumed to be separated by spaces or tabs (so called white space).
If the column separator is something other than spaces or tabs, such as a comma, you can specify that in the awk statement as follows. It separates by the comma.

..code-block::

  awk -F, '{ print $3 }' table1.txt > output1.txt

The list of statements inside the curly brackets ('{','}') is called a block. If you put a conditional expression in front of a block, the statement inside the block will be executed only if the condition is true.

..code-block::

  awk '$7=="\$7.30" { print $3 }' table1.txt

In this case, the condition is $7=="\$7.30", which means that the element at column 7 is equal to $7.30. The backslash in front of the dollar sign is used to prevent the system from interpreting $7 as a variable and instead take the dollar sign literally.

You can also use regular expressions as the condition. For example:

..code-block::

  awk '/30/ { print $3 }' table1.txt

The string between the two slashes ('/') is the regular expression. In this case, it is just the string "30." This means if a line contains the string "30", the system prints out the element at the 3rd column of that line.
If the table elements are numbers awk can run calculations on them as in this example:

..code-block::

  awk '{ print ($2 * $3) + $7 }'

Besides the variables that access elements of the current row ($1, $2, etc.) there is the variable $0 which refers to the complete row (line), and the variable NF which holds to the number of fields.

You can also define new variables as in this example:

..code-block::

  awk '{ sum=0; for (col=1; col<=NF; col++) sum += $col; print sum; }'

This computes and prints the sum of all the elements of each row.

sed
___

The primary use of the Linux command sed (short for stream editor) is to modify each line of a file or stream by replacing specified parts of the line. It makes basic text changes to a file or input from a pipeline. For example, say you have a file named "songs.text" that contains these lines:

grep
____

Look for text in files. List out lines containing text (with filename if more than one file examined).
grep "hi there" file1 file2 ... # look for 'hi there' in files
grep -i "hi there" filename     # ignore capitals in search
cat filename | grep "hi there"  # use pipe
grep -v "foo" filename          # list lines that do not include foo

sort
____
tr
___
time
____
fold
____
cgrep
_____
curl
____

cut
___
wc
___
uniq
____
