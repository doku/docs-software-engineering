Portscanner
===========

send oackages and check responses

Ports are 16 bit -> 0-5535

Checks for open ports

- open
- closed: tells that machine is running
- filtered: swallowed by machine or firewall without any response
- filtered/open

methods
-------

TCP connect scan: weakest, can only differenciate between open and closed

Os is asked to connect to a system, if connection is

TCP-SYN-Scan

TCP-ACK-Scan : create package with set ACK flag: checks whether port is filtered

TCP-FIN: fin flag set
Null
XMAS: pin push and ack flag is set -> lights like a XMAS tree

OS Detection
############

nmap tool for portscan
nmap-o sends different packages and checks responses with database to determine operating systems : general OS Detection
nmap-sV : checks special version
    -T : different Timings at which the packages are send

    add -p 443 for port

By knowing which port is open and which programs check the ports, you can use these doors to enter the system

use wireshark to follow nmap commands

hpi-vdb.de
