Fragen so exakt wie möglich beantworten, Wichtiges
Bonus sind schwerer
EIne Frage in einer Sprache antworten

kleine Zusatzfragen am ende

1 General
---------

(a)Briefly explain the difference between the network settings NAT, Bridged and Host-only of virtual (3)
machines in VirtualBox, VMWare or comparable solutions.

NAT: Nur eine IP Addresse Hostrechner Mapt das
Bridged: haben 2 IP Addressen, eine wird direkt durchgeleitet
Host-only: nicht aufs Internet Zugreifen


(b)  Mention the most important security goals and explain them.

Verfügbarkeit -
Integrität - soll auffallen, wenn die Daten geändert wurden
Vertraulichkeit -

C
I
A



(c) What is the primary difference between a virus and a worm?

Virus repliziert sich lokal, der Wurm übers

(d) What does the acronym VPN stand for?

(e) List the four phases of risk anlysis.

Definition der Domäne
Aquise der Risiken
Evaluierung
Interpretation
Interpretation


(f) What is a cold boot attack?

Automatic Restart with default settings (F)

2 Reconnaissance
----------------

(a) Name two methods (one example each) for offline reconnaissance and online reconnaissance

offline - social engineering / Müll durchwühlen

online - Portscann

(b) What is the purpose of the whois tool? How is it useful during reconnaissance? Why is it difficult (4) to protect yourself against whois?

Whois -> gibt an wem die Domaine gehört
Hat ein neues Ziel -> Email
Informationsbeschaffung -> als owner bei
Man muss seine Daten angeben

Wie schützt man sich dagegen
-> Privacy Proxy davor schalten ()


(c) The traceroute (tracert) tool can be used to determine the exact network route to a target address. (3)
Each ”hop” is one intermediate host between the source and the destination (e.g. a router or
firewall). How does traceroute detect the IP address of these intermediate hosts?

Schickt Packete und setzt die Time to live auf 1,2,3,4
Wenn eine Response kommt ist
requirement: achten auf ICMP

(d) Think of a hypothetical scenario of a reverse social engineering attack and describe it. Why it is (8)
called reverse and what are the benefits over a regular social engineering attack?

Man bringt den anderen dazu einen zu kontaktieren "Ich bin der Admin"
Schaft gleich  Vertrauen

3 Passwords and cryptography
----------------------------

(a) What is a hash function?

Einwegfunktion

(b)Name and explain two methods for user authentication apart from username/password schemes.

Biometric,
RSA (Public, Private Key)
One time Passwords

(c) Name and shortly explain three different methods of gathering username-password combinations.

(reverse)social engineering
phishing
snithing
guessing
brute force

(d) Explain the concept of Rainbow Tables. Describe their structure and the necessary steps to find (12)
the password (in plaintext form) of a given hash. Support your explanation with an illustration.
Specify a countermeasure and the reason why it prevents the usage of Rainbow Tables



counter measuer

Salt -> für jeden User eigene Rainbow table
Peper -> für jede Website eine eigene Rainbowtable

4 Internet Protocols
--------------------

(a)Name the different layers of the OSI model. Give an example protocol for layers 2, 3, 4, 5 and 7

(b) Imagine yourself as a foreign intelligence agency that wants to eavesdrop on the traffic of a defense (8)
company that is connected to the Internet. Explain how this could be achieved by an attack based
on the BGP protocol. What does BGP stand for and what is its general purpose? Why is it difficult
to protect against such an attack?

BGP - Border Gateway Protocol
Verbinet größere Zohnen, z.B Bei Providern.
Company Hat keinen Einfluss arauf


(c) Explain the term TCP-Hijacking and give one counter measure against this type of attack. Name (4)
two other methods for becoming a Man-in-the-Middle

TCP Hijacking

Wie wird bei TCP gesichert, das die Packete zusammengehören?
Sequenznumber wird hochgezählt.
Diese muss man mitbekommen und mit der richtigen Sequenznumber

Weitere Methoden

ARP Spoofing
DNS spoofing


(d) Imagine you are head of eavesdropping in an evil corporation. You are given the task to monitor (6)
all SSL traffic (i.e. the unencrypted contents) of all employees, ideally without them noticing it.
What prerequisites are required? Briefly explain the general idea of SSL interception. Why could
your employees notice the eavesdropping, especially when visiting ”high-profile” websites, such as
online banking or Twitter, with a modern browser?


prerequisites - Man in the middle, Zertifikat -> CA machen der die Clients Vertrauen

SSL Interception : Conection zum Target und zum Ziel des targets
CErificate Pinning

5 Exploitation
--------------
(a) Briefly explain the terms exploit, payload and stager in the context of Metasploit. How are they (6)
related? Give one example for a payload

Exploits nutzen Schwachstellen in software
payload ist der Schadcode der ausgeführt wird
Stager - nicht der eigentliche Schadcode sondern ein Forbereiter, der die Backdor

(b) Explain the concept of stack canaries. Which attack do they prevent? Why is it only a mitigation (5)
and not a solution?

(prevent) buffer overflow -> only detects it



Bonus aufgabe
abwechselnd erster und letzte Buchstabe
