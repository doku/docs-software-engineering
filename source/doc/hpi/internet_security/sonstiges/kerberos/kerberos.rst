========
Kerberos
========

Kerberos wird bei windowsnetzwerken zur online autentifizierung gemacht
User meldet sich bei domain controller an
Älteres protocoll
- Ticketbasiert
- Single sign-on
- principal: useres, machines, services, applications
Key distribution Center (KDC)
- Trusted third party (TGT, Service Ticket)
- Components: AUthentication Service (AS), Ticket Grantiers Service (TGS)
- Roalen DB: user_id, user_key


Ablauf
======

.. figure:: kerberos.png
	 :alt: Kerberos

1. Client(C) -> KDC: Request Ticket granting Ticket (TGT)
2. KDC(AS) -> C:
  TGT -> {tgs_session_key, u_id} _tgs_key}; // mit tgs_key verschlüsselt tgs_key ist long term key
  {tgs_session_key, u_id}_user_key  // mit userkey verschlüsselt
3. C -> KDC(TGS)
  TGT->{tgs_session_key, u_id}_tgs_key
  {timestamp, u_id}tgs_session_key
4. KDL(TGS) ->c
  Service Ticket{s_session_key, u_id} _service_key
  {s_session_key, s_id} _tgs_session_key
5. C-> Service
  Service Ticket: {s_session_key, u_id}_service_key
  {timestamp, u_id} _s_session_key
6. Service -> Client
  {timestamp}s_session_key

Chain:

userkey -> tgs_session_key -> tgs_service_key ->s_session_key

Schritt 1 und 2 werden einmal ausgeführt und dann ist das TGT für 8 stunden gültig

In der Regel auch schon Preauthentication im erste schritt nötig
timestamp mit user_key verschlüsseln, dass der user computational effort einsetzt, soll ddos verhindern,
da server in schritt 2 recht viel machen muss.


Authentication vs. Authorization
================================
Authentication: Identitätscheck
Authorization: permission to access
