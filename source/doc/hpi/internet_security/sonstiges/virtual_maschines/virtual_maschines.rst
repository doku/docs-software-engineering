
Network Adapter Settings
------------------------

NAT(Network Address Translation): Der Netzwerkadapter nutzt das physische NIC des laptops mit der selben IP Addresse des Laptops
Bridged: Das NIC erhält noch eine IP Addresse, über die die VM von außen erreichbar ist. Alle offenen Ports sind verfügbar.
Host only: VM ist nicht mit dem NIC verbunden, sodass keine Internetverbingung in der VM besteht.
