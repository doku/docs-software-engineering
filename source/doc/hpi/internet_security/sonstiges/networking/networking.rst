Networking
==========

.. figure:: package_path.png
	 :alt: package_path

MTU
---

MTU -> larger

  - better distribution between header and data
  - higher latency: needs to fill buffer with data and thereby we wait for Data to accumulate, Receiver needs to process large packages, before any action can be tacken
  - more delay
  - in case of an error a larger package needs to be resend.

MTU -> smaller inverse effects

Ethernet standard: MTU  1500 Bytes
minimal header 20 bytes -> rest can be body

Fragmentation
-------------
ID identifies fragments which belong together

Fragmentation flag: morefragments
  - = 1 -> if not last fragment
  - = 0 -> last fragment

offset determines the order of the packages

Attacks
#######

Teardrop: Change Offsets of fragments, so they overlap and result in an

Ping of Death: usually max cap with 65k bytes, and by sending many fragments this limit can be surpassed causing Segmentation faults, nowadays not possible any more





ICMP
----
Ping uses ICMP
Auf welchen Port hört ICMP -> gibt keine Port ist auf der selben schicht wie TCP und UDP und erst innerhalb dieser Schichten gibt es Ports, daher gibt es bei ICMP keine Ports

Time to live
############
pakete gehen unterschiedliche Wege, und benötigen unterschiedlich lange
Packete hopen von System zu system

traceroute 8.8.8.8
->unterschiedliche Modes standardmäßig UDP, auch TCP oder ICMP mögl
kann genutzt werden um Probleme auf dem Daten Pfad feststellen zu können



UDP
---

.. figure:: udp_header.png
 	:alt: udp_header

- Used for DNS, Streams
- has smaller headers and less overhead
- connection / stateless -> just sending a package without negotioating the session.
- not possible to ensure that everything was received.
- is suitable for broadcasting information, used usually if it is not so important for every package to reach the target (file download vs skype stream)
- order not ensured

- UDP DDOS : send requests with spoofed sender address. Small request results in large responses which are send to the DDOS victim. (DNS Amplification)

TCP
---

.. figure:: tcp_header.png
	 :alt: tcp_header


TCP characteristics
###################

- Reliable
- Ordered
- high overhead


Flags (9 bits) (aka Control bits)
#################################

Contains 9 1-bit flags
  - NS (1 bit): ECN-nonce - concealment protection (experimental: see RFC 3540).
  - CWR (1 bit): Congestion Window Reduced (CWR) flag is set by the sending host to indicate that it received a TCP segment with the ECE flag set and had responded in congestion control mechanism (added to header by RFC 3168).
  - ECE (1 bit): ECN-Echo has a dual role, depending on the value of the SYN flag. It indicates:
    If the SYN flag is set (1), that the TCP peer is ECN capable.
    If the SYN flag is clear (0), that a packet with Congestion Experienced flag set (ECN=11) in IP header was received during normal transmission (added to header by RFC 3168). This serves as an indication of network congestion (or impending congestion) to the TCP sender.
  - URG (1 bit): indicates that the Urgent pointer field is significant
  - ACK (1 bit): indicates that the Acknowledgment field is significant. All packets after the initial SYN packet sent by the client should have this flag set.
  - PSH (1 bit): Push function. Asks to push the buffered data to the receiving application.
  - RST (1 bit): Reset the connection
  - SYN (1 bit): Synchronize sequence numbers. Only the first packet sent from each end should have this flag set. Some other flags and fields change meaning based on this flag, and some are only valid for when it is set, and others when it is clear.
  - FIN (1 bit): Last packet from sender.


Sequenznummern müssten random gewählt werden
if someone knows the sequenz nummer the can inject a package with the right nummer which will be processed. The sequence number is increased by the amount of data, which was send by the last package.

.. figure:: handshake.png
	 :alt: Handshake

Server sends
    SEQ#
P1. 5942
  - 1189 Size P1
P2. 7131 (= 5942 + 1189)
  - 1448 Size P2
->ACK#: 7131 + 1448

https://de.wikipedia.org/wiki/Transmission_Control_Protocol#Verbindungsaufbau_und_-abbau
