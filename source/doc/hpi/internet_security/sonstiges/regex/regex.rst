RegEx
=====

Special Characters
------------------
- **^** : Anfang des Strings (Anfang Zeile bei Multiline)
- **$** : Ende des Strings
- **[]** : In eckigen Klammern kann man eigene Zeichenklassen definieren (Minuszeichen - hat Sonderfunktion A-Z, es hat keine Sonderfunktion in der ersten Stelle in den eckigen Klammern )
- **|** : Alternative
- **.** : any
- **{}** : Quantify (Bsp. {1,7})
- ***** : 0-infinity
- **?** : 0-1
- **
- **\** : escape-char
- **\s** : whitespace
- **\w** : wordchar
- **\b** : wordboundry (matcht an den grenzen der Wörter ohne ein Zeichen zu "verschlucken")
- **\d** : digit

Special Characters in groß geschrieben ist die Negation des

Matcharten:
-----------

**greedy**: frisst immer das ganze Wort und backtraced dann um das g zu
**nongreedy**: ``\w*?g`` prüft

Grouping
--------

- () - groups
- (?:) : Gruppe wird nicht gezählt , non capturing -> frisst Wörter weg (Bsp. (?:ist)(\w+)(\1) zwei mal das gleiche Wort hinter ist)
- (?!) : negative look ahead
- (?=) : positive look-ahead
- (?<=) : positive look-behind
- (?<!) : positive look-behind
- (?P<name>) : Gruppe mit name benennen
- (?P=name) : Benannte Gruppe referenzieren
- \1 :
