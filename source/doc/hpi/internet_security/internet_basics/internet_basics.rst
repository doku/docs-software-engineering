Internet Technology and WWW
===========================

The Internet is a network of computer networks.
Today the worldwide Internet connects more than 1 billion hosts all
over the world

Computer networks
-----------------

Computer networks consist of

* Computers:

  * Servers (provide resources)
  * Clients (use the provided resources)
* Transmission media (cable or other physical media for connecting computers and intermediate systems)

  * Wired (twisted pair, coaxial cable, optical fiber, etc.)
  * Wireless (radio waves, infrared signals, etc.)

* Infrastructure components
  * Network adapter - transform computer data into signals that could be transferred over the transmission media

Communication within a computer network is organized by Network Protocols that define

* format of transmitted messages
* address schema
* each possible step in the communication process
* methods for controlling the communication process

.. figure:: network_protocol_map.png
	 :alt: network_protocol_map


Classification of Computer Networks
-----------------------------------


.. figure:: network_classification.png
	 :alt: network_classification


Difference between Distributed Systems and Computer Networks
############################################################

**Common Properties:**

* Cooperation of many individual components, which are physically and logically different
* Individual components are spatially distributed and operate autonomously, but cooperatively

**Differences:**

* In distributed systems, each involved component works
  transparently and is usually managed by a control process. What
  happens on a component is unavailable for processes at other
  components as well as for the end users
* A computer network is controlled by a network operating system
  that coordinates the processing steps of a user request


Computer networks can be classified along usage criteria
########################################################

**Function sharing networks**

* making different functions available at different locations by allowing
  access to specialized servers (e.g., supercomputers, vector computers)

**Load sharing networks**

*uniform utilization of resources by distributing the work load to
  different computers

**Data sharing networks**

* better utilization of disks, increased availability, increased safety

**High Reliability networks**
* another system will take its functionality when a system fails

More classification criteria:
#############################

**Homogeneous vs. heterogeneous computer network**

* all computers are the same vs. different types

**Open vs. private computer network**

* open Access vs. Access only to limited group of users

**VPN – Virtual Private Network**

* Internet-based, but behaves as a private network

Internet Structure
------------------

As a network of networks the Internet is a virtual computer network
Physically, the Internet consists of different computer networks that
are coupled by means of gateways (i.e. routers).
The illusion that the different networks form one homogenous
network is provided by means of the Internet protocols specifying

- how to identify participants of the Internet
- who is the addressee of the message
- how to transmit a message
- which path through the network of networks should be taken by a message
- that messages arrive completely and correctly

.. figure:: internet_structure.png
	 :alt: internet_structure


.. figure:: internet_access.png
	 :alt: internet_access

Internet Protocols
------------------

To manage the high complexity of communication over the Internet,
Internet protocols are conceptually organized in **hierarchical layers**
There are two  famous Layering Models of Communication:
** OSI Reference Model, TCP/IP Model**

.. figure:: protocol_layer_models.png
	 :alt: protocol_layer_models


TCP/IP Stack
############

**Network Interface Layer**
* responsible for transporting IP-datagrams over specific networks
* Protocols: Address Resolution Protocol (ARP), Reverse Address Resolution Protocol (RARP)

**Internet-Layer**
* handles communication from one machine to another by encapsulating the message into IP packets, so-called IPdatagrams, e.g. IPv6, IPv4

**Transport-Layer**
* provides a universal transport service for (end-to-end) communicating applications, e.g. TCP, UDP

**Application-Layer**
* provides functions and services for programs that want to communicate via the Internet (extends network operation system), e.g. HTTP, SMTP, FTP, …



WWW
---

WWW is based on the following three basic standards:

**HTML - HyperText Markup Language**
* specifies format and assembly of hypertext documents “HTML-documents” that form the Web

**URL - Uniform Resource Locator**
* addressing scheme, which indicates location of a data file on the Internet as well as the used protocol

**HTTP - HyperText Transport Protocol**
* communication protocol for transmission of HTML-documents
