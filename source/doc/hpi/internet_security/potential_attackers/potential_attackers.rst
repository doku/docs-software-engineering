Potential Attackers
====================

Staff of the own Enterprise
----------------------------

A large portion of the misuse of computer systems is caused by staff.

**Motives**: incompetence, carelessness, frustration, revenge
  - Naivety or carelessness, e.g. receive information by clever interrogation: **social hacking**
  - non-compliance of internet security instructions

Hackers from High-Schools and Universities
------------------------------------------

The largest number of break-in attempts (unsuccessful) are executed by students.

**Motives**
- Combination of curiosity, self-affirmation
- usually aimless, without much bad intent
- Usually low amount of loss
- script-kiddies, dangerous DDOS

Hackers / Crackers
-------------------

Unprofessional elite of hacker-community

- Crackers want to shut down communication systems

Professional Hacker Criminals and Secret Agents
------------------------------------------------

Commercialization of the Internet opens a market for professional data theft and data manipulation

- execute specific tasks
- black market for information

Governmental Intelligence Services
----------------------------------

Mass surveillance to prevent crime and terrorism

- nearly unlimited recourses
- usage of dubios and potentially unconstitutional methods
- "cooperation" with foreign agencies and companies
- often connected to **APTs** (Advanced Persistent Threats)

Ordinary Criminals
------------------
 crime scene, especially organized crime

 - new types of computer crime: attacks against financial systems
 - Authorities are not yet prepeared for law enforcement in the Internet
 - institutes like CIA whish back doors in cryptografic operations


Terrorists
----------

- raise funds & recruit people,
- spread propaganda
- communication
