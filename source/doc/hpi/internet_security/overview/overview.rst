Internet Security Overview
==========================

The Internet started as a research project in a small and trusted community of researchers.

.. note::
	Security was not a primary consideration in the design of Internet protocols.

There is also no single entity, no single Internet authority which controls the Internet completely.
Furthermore the Internet is an **easy Target** with Billions of entry points, many design errors resulting in security flaws and easy access to hacking tools.

.. figure:: attack_development.png
	 :alt: attack_development

Risks when using Internet-based Information Systems
---------------------------------------------------

- Non-availability of computer systems and databases
- Loss of data
- Loss of confidentiality (competition, publicity)
- Sabotage
  - Intrusion via "malware" (Virus, Worms, Trojan Horse)
  - Obstruction of management
  - Denial-of-Service attacks
- Spoofing (IP, DNS, E-Mail)
- Unauthorized access to confidential data

Changing Nature of the Threats
------------------------------

- Complexity of Internet, protocols and applications are increasing with our reliance on them
- Internet has become am a critical infrastructure itself
- attacking over the Internet is easy, low risk, and hard to trace
- Intruders are often well prepared and organized
- Intrusion/ hacking tools are increasingly sophisticated, easy to use and support large scale attacks

.. note:: Rapid adoption and an increasing amount of exploitable vulnerabilities combined with lack of awareness and qualified experts result in high risks from attacks

Possible attacks
----------------

.. figure:: possible_attacks1.png
	 :alt: possible_attacks1

.. figure:: possible_attacks2.png
	 :alt:    possible_attacks2


Who are the Intruders
---------------------
- Insiders
- Computer specialists
- script kiddy's
- Criminals and terrorists
- Industrial spies
- Foreign government intelligence services

.. note::  99% of intrusions result form known vulnerabilities or misconfiguration where countermeasures have been available

Success of the Internet
-----------------------

1993 -explosive expansion in every-day use of Internet by introduction of WWW and the development of web browsers with graphical UI


Most Common attacks
-------------------

- spoofing: Technique used to pretend a fake identity
- defacement: Malicious change of appearance of web sites
- sniffing: monitoring of transmitted data
- DOS Attacks: services are bombared with requests
- Malware:
  -Viruses
  -Worms
  - Trojan horses
- Exploitation of software bugs
- phishing (social engineering)
- physical security risks
