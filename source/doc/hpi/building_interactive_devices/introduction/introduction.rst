Introduction
============

What will this article be about?
--------------------------------
Generally, we will talk about the future of human computer interaction.
Therefore, we will consider the following topics:

- gesture detection

  - depth detection
  - image processing
  - 2D image classification
  - homography
- interaction with physical matter

  - kinematics
  - dynamics
  - robotics
  - 3D printing



Development of the computer interaction
---------------------------------------

.. figure:: development_computer_interaction.png
	 :alt: change in computer interaction over time

History:

- 1946: plugboard and switches (1946)
- 1941: punched tape (Z3)
- 1963: keyboard +printer terminals
- 1968: mouse
- 2000s: Webcam
- 2007: motion sensors in phones (accelerometers)
- 2010+: depth cameras (Kinect / Google Tango)
- 2017: Motion capture (HTC Vive)
