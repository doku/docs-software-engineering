openCV
======

OpenCV is a powerful open computer vision toolkit, which comes with C++ and Python bindings.
Documentation: http://docs.opencv.org/2.4.13.2/


cv::threshold
-------------

.. figure:: media/hand_depth_image.png
	 :alt:

.. figure:: media/hand_depth_image_threshold.png
	 :alt:

check out http://docs.opencv.org/2.4/doc/tutorials/imgproc/threshold/threshold.html for more information.

cv::blur
--------


cv::findContours
----------------

cv::contourArea
---------------

cv::fitEllipse
--------------

cv::absdiff
-------------

cv::subtract
------------

cv::addWeighted
---------------
