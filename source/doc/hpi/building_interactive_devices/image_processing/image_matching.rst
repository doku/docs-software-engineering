Image matching
==============

To pair up elements in two images there are a couple of different methods.

.. figure:: media/image_matching.png
	 :alt:

feature-based matching
----------------------

You can extract features from the image using techniques like SIFT (Scale Invariant Feature Transform) or the harris corner detector.
Then segments of the image can be matched using standard machine learning techniques.
The feature extraction allows doing more complex operation, such as locating objects in a scene. It provides a significant data reduction which reduces computational load.

stereo matching
---------------

If you have stereo image pairs the search space is reduced from 2D to 1D, because both images have a fixed combined axis.

area-based matching
-------------------

.. figure:: media/area_based_matching.png
	 :alt:

.. figure:: media/area_based_matching2.png
	 :alt:

To solve issues with differently exposed images, you can normalize the pixel brightness from the images.

The following issues remain:
  - image noise
  - perspective distortion & occlusion
  - specular reflections

Whether stereo matching works tends to come down to how many "features" the scene offers.

The kinect solves this issue by artificially creating features.

.. figure:: media/area_based_matching_kinect.png
	 :alt:

But technically the kinect only has to solve a stereo matching problem, because both cameras are fixed together.
