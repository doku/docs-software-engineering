hough algorithm
===============

The hough algorithm is a method to detect lines in images.
The image has to be edited with edged detection in advance.
To find lines in images, you can consider one dot at a time and think of all possible lines (quantified) that might belong to it. If you do this now for all dots the resulting image might look like this:

.. figure:: media/hough1.png
	 :alt:

To find out which lines are supported by many dots, you can use voting.
That’s exactly what the hough algorithm does, it only uses a clever data structure to store the lines for voting.
The question that now needs to be answered is: How to store the space of all line equations?

.. figure:: media/hough2.png
	 :alt:

.. figure:: media/hough3.png
	 :alt:

.. figure:: media/hough4.png
	 :alt:

Now all possible lines for a point form a sinus curve. Each point on this curve represent a line and this curve represents the point.
This space transformation is called hough transform.

.. figure:: media/hough5.png
	 :alt:
	 
If we do this for every point in the image and overlay the resulting curves we get a pretty cool looking image.
Each pixel of the image shows the accumulated curves it is part of, this is the voting process mentioned earlier. The number of votes is represented in the colour of the pixels.

.. figure:: media/hough6.png
	 :alt:

To determine the lines we simply blur the image and threshold to configure how many votes a line needs to be considered as one. The exact line equation can be extracted from these blobs by fitting an ellipse to the shape and taking the centre point as representant.
Remember in this cool space points are lines.

Check out this nice visualization:
https://www.youtube.com/watch?v=ebfi7qOFLuo

Using hough transformation for circle detection
-----------------------------------------------

With some minor changes you can use the hough transformation for circle detection.
Based on the edge detection image, you consider each edge pixel as created by a circle with a certain radius.

Now you get a 3 dimensions Hough-space, x,y of the centre point and r for the radius. The radius range is given in advance and need to be determined by A-priori knowledge.
You do this again for every point and determine candidates from the voted image using the technique mentioned above.

Furthermore, the hough transformation can be used to detect ellipsis (two additional dimension in the space, one for the second centre point, one for the second radius)


Disadvantages
-------------

The hough transformation is a brute force approach and therefore computationally expensive.
