Image processing
==============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   image_segmentation
   hough_algorithm
   image_processing_with_maschine_learning
   image_matching
   openCV
   face_detection
