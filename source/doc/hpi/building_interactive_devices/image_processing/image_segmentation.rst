Image segmentation techniques
=============================

To understand the meaning of images it is important to locate objects in the image.
This process is called image segmentation and there are a wide variety of different techniques to achieve this goal.

Thresholding
------------

Thresholding typically converts a grayscale image into a black-and-white image.


.. figure:: media/Image_processing_pre_otsus_algorithm.png
	 :alt:

.. figure:: media/otsusThresholdingExample.png
	 :alt:

Check out the OpenCV documentation for more details http://docs.opencv.org/2.4/doc/tutorials/imgproc/threshold/threshold.html/
The main difficulty of thresholding is, choosing the threshold.

-------------
Otsu's Method
-------------

In computer vision and image processing, Otsu's method is used to automatically perform clustering-based image thresholding, or, the reduction of a grey level image to a binary image.
The algorithm assumes that the image contains two classes of pixels following bi-modal histogram (foreground pixels and background pixels), it then calculates the optimum threshold separating the two classes so that their combined spread (intra-class variance) is minimal,
or equivalently (because the sum of pairwise squared distances is constant), so that their inter-class variance is maximal.

In Otsu's method we exhaustively search for the threshold that minimizes the intra-class variance (the variance within the class), defined as a weighted sum of variances of the two classes:

.. figure:: media/thresholdingOtsusMethod.png
	 :alt:

In order to choose a optimal threshold you calculate all possible cut-offs (usually the number is relatively low 256 for grey-scale images). The thresholded image shown above has been calculated using Otsu’s method.


segmentation of objects
-----------------------

------------------------------
connected-components labelling
------------------------------

Connected-component labelling is an algorithmic application of graph theory, where subsets of connected components are uniquely labelled based on a given heuristic.
Connected-component labelling is used in computer vision to detect connected regions in binary digital images (use thresholding as a pre-processing step, in order to get a binarized Image), although colour images and data with higher dimensionality can also be processed.

A graph, containing vertices and connecting edges, is constructed from relevant input data.
An algorithm traverses the graph, labelling the vertices based on the connectivity and relative values of their neighbours. Connectivity in image graphs, for example, can be 4-connected or 8-connected.

In the following we will talk about the tow-pass algorithm, which is relatively simple to implement and understand.

The two-pass algorithm iterates twice through 2-dimensional, binary data:

- in the first pass assign temporary labels and record equivalences
- in the second pass replace each temporary label by the smallest label of its equivalence class

.. figure:: media/Two-pass_connected_component_labeling.png
	 :alt:

You sequentially go through every pixel and assign labels according to the following rules:

.. figure:: media/Two-pass_connected_component_labeling2.png
	 :alt:

In the case which is marked green, you found to differently labelled pixel blocks which belong to the same component. To solve this issue, you simply save the two labels in an equality list to mark them as one component.
In the second pass, you relabel each pixel with the lowest equivalent label. Now all connected components are labelled correct. This Algorithm has a complexity of O(n).




--------------------
Jordan curve theorem
--------------------
TODO

.. figure:: media/connectedness.png
	 :alt:



----------------------
background distraction
----------------------

What if objects are of similar colour as the background?

.. figure:: media/hand_segmentation.png
	 :alt:

A easy solution for this problem can be to take a picture of the background in advance.
Then you simply need to take the difference between the new Frame and the default background, to extract what’s new.
This solution only works if you have a constant not changing background.

.. figure:: media/backgroud_distraction_cars.png
	 :alt:

You can use the process to count cars crossing an intersection for example.
The difference Image between the background image and a image with cars will show blobs, which are the cars and some noise due to lighting changes.
You remove the noise by blurring the image and you can threshold it to get a binarized image for further processing.


optical flow
------------

SIFT-scale-invariant feature transform
--------------------------------------

low pass filter
---------------
