=======================================
Image processing using machine learning
=======================================



Number recognition with knn classification
------------------------------------------

Using the kinect you can register handwriting on a desk or another flat surface.
By mounting the kinect to a tripod and pointing it downwards you can monitor interactions with the table.
In order to extract people’s hands, you first take a picture from the background and distract it from the incoming frames of the camera. Now you have an image with the moving objects.
To extract noise, you first blur the image and then threshold it. Because we are processing a depth image this thresholding effectively determines the depth of the objects you want to detect.
Because a hand or fist is about 3-5 cm thick, you need to calibrate the threshold accordingly, so that hands are detected if they are close enough to the surface.
Now you have an image with a white blob, which shows the hands position. To determine the centre of the hand, you can use the openCV method cv::fitEllipse.

To enable people to write you draw circles at the centre of their hands position in a new image. Now you can draw lines.

.. figure:: media/number_classification.png
	 :alt:

For the classification you have to normalize the image for example using min-max normalization: normalized(x) = (x-min(x))/(max(x)-min(x))
This transfers the letter in a 1 by 1 range.
For faster classification and better generalization, you need to extract a couple of points from the number, and should not use all the registered points as a feature vector.
You extract 8 equidistant points in the order of detection and use these to classify your number.

For knn-classification you need a trainings dataset. Knn checks out the class labels from the k closest data points and determines the class label of the new point using a majority voting.
For k = 7 you usually have a good compromise between overfitting and generalisation.

Principle component analysis
----------------------------
.. figure:: media/principle_component_analysis.png
	 :alt:

Random forests
--------------

Random forests are constructed by multiple decision trees. Random forests correct for decision trees' habit of overfitting to their training set.
Random forests are essentially bagging with multiple decision trees.

Improve classification
----------------------

For elongated clusters (ellipsis) the mahalanobis distance is a better distance function.

.. figure:: media/mahalanobis_distance.png
	 :alt:

feature extraction
------------------

In feature extraction, you transform given input data into a set of features. It is important to extract meaningful features which contain the relevant information to perform the desired task.
