Face detection
--------------
To detect people’s faces you need to create a general model to define what pattern to look for, when you try to detect a face.
The following image shows a basic version of these models.

.. figure:: media/face_detection1.png
	 :alt:

The model defines a constellation of light and dark pixels as the feature which define a face.
Now you need to go through the entire image to look for such patterns. But how can you do this fast?

In a pre-processing step, you
This has a complexity of

.. math:: \mathcal{O} (n)
