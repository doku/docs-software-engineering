Depth detection using the XBox Kinect
=====================================

How does it work?
-----------------


The XBox kinect consists of three main parts:

 - normal RGB-camera (2)
 - infrared projector (1) on the left side
 - infrared camera (1) on the right side next to the RGB-camera

 It has a motorized stand (4) for tilt and some microphones (3) as well.

 .. figure:: media/kinectSensors.png
 	 :alt: kinect sensors

**The depth detection works as follows:**

The infrared projector projects a pattern of points, that paint the scene with (for the human eye) invisible markers.

.. figure:: media/kinectPointPatterns.png
  :alt: Point Patterns from the kinect

The points are laid out in a specific way, so that parts of the pattern uniquely identifies this part.

.. figure:: media/kinectInfraredProjectorFunctionality.png
  :alt: Point Patterns from the kinect

So from any patch of points big enough you can reconstruct its location in the projection.
By "mapping" the image from the camera to the projection you can calculate the depth from the objects.

.. figure:: media/kinectParallax.png
  :alt: parallax

This example shows, that the "point pattern" on the black rectangle matches the one from the bottom left corner of the projection.
Therefore, both are marked green. By getting the reference to the original projection, you know the angle *a* between the projector and the object.
You also can calculate the angle *b* from the captured image. Because the distance between the camera and the projector is fixed and known, you are now able to calculate the depth of the object.

.. figure:: media/kinectParallax2.png
  :alt: parallax


Essential for parallax is the distance between the projector and the camera. That's the reason for the layout of the sensors.

Can you use multiple kinects at the same time?
-----------------------------------------------

If the projected infrared-point patterns from different kinects overlap each other the patches can no longer be identified,
because the patterns the camera sees don't match the ones emitted by the projector.
Therefore, you can't simply combine multiple kinects, if their field of view overlap. But there is a workaround:

.. figure:: media/kinectVibrationMotor.png
  :alt: change in computer interaction over time

By mounting a vibration motor to the top of each kinect you can blur out the projected points by the other kinects.
This works as follows: The vibration from the motor forces the kinect to move slightly in a high frequency. Because infrared projector and camera both move with the fixes structure of the kinect, there is no respective movement between the two.
Therefore the camera shows a crisp image of the projected infrared points of the projector. But the other kinects move differently and therefore points form a different kinect appear blurred.
Now they can be easily extracted by standard computer vision methods like blur and thresholding.
