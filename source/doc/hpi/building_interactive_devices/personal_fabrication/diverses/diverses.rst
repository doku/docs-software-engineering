Diverses
==========
This is a collection of many different topics

srews and bolts
---------------

Bolts (Maschinenschrauben) are threaded in an external male thread.

.. figure:: bolts.png
	 :alt: bolts

.. figure:: screw.png
	 :alt:    screw

.. figure:: screwsAndBolts.png
	 :alt: screws and bolts

.. figure:: screwsandbolts2.png
	 :alt: screwsAndBolts2

.. figure:: Locknut.png
	 :alt: 	 locknut


frustrated total internal reflection ftir
--------------------------------------------


.. figure:: ftir1.png
	 :alt: ftir1

.. figure:: ftir2.png
	 :alt: ftir2

.. figure:: ftir3.png
	 :alt: ftir3
