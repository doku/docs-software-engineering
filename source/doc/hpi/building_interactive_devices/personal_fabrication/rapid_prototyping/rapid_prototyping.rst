Personal Fabrication Technologies
=================================

Arts & crafts
-------------

any of a wide variety of types of work where useful and decorative objects are **made completely by hand or by using only simple tools**
(like wood chisels, knifes, files)

Rapid prototyping
-----------------

The automatic construction of physical objects using manufacturing technology

.. figure:: rapid_prototyping.png
	 :alt: rapid_prototyping

Instead of each lifecycle taking the same time, updates and changes can be made rapidly after the first basic design. There are different techniques to achive this but the general idea is that you can replicate and build the prototypes fast and easy.

Techniques:

- 3DPrinting
- milling
- laisercutter

The basic concept is: take a robot arm and mount a drill(milling machine), a glue gun(3D printer) or a laiser(laiser cutter) to it and let them do the manufacturing.

3d Modelling
------------

Software:
	- tinkercad (simple)
	- Autodesk CAD (powerfull)

sundrises
