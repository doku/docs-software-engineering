Arduino
========

Arduino is an open source, computer hardware and software company, project and user commuity that designs and manufacturers single-board microcontrollers for building digital devices and interactive objects that can sense and controle objects in the physical world.

Arduino nano
-------------

.. figure:: arduino_nano.png
	 :alt: arduino_nano

pulse width modulation (pwm)
-----------------------------
PWM is a techniwue using a rectangular pulse wave whose pulse width is modulated resulting in the variation of the average value of the waveform. It serves to create an analog signal from a digital one.


It is also used to modulate sinus form "Wechselspannung" so that there is no need to add a resistor, which would waste a lot of energy.

.. figure:: pwm.png
	 :alt: pwm
	 
Arduino commands
----------------

analogWrite(pin_number)
_______________________

writes a PWM wave to a pin. It makes the pin generate a steady square wave of the specified duty cycle(the cycle of operation). It can be used to vary the brightness if a LED, or the speed of a motor. Its effects end/ are overritten with the next call of analogWrite(), digitalRead() or digitalWrite() on the same pin. The PWM signal on the most pins is rund about 490 Hz.
analogWrite() has **nothing to do with analog pins or analogRead()**


analogRead(pin_number)
______________________

reads the voltage on the pin and returns values between 0 (0V) and 1023 (5V) -> discretized 0...1023
analog pins are **input only**

digitalRead(pin_number)
_______________________

reads the voltage on the pin and returns 0(voltage > 3V) or 1(voltage < 3V) -> thresholded

Sensors
---------

photodiode
__________

.. figure:: photodiode.png
	 :alt: photodiode

piezoelectric cell
___________________

.. figure:: piezoelectric_cell.png
	 :alt:    piezoelectric_cell

build your own pressure sensor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   conductive foam (normally used als shipping material for electronics to remove electrostatic charges by connecting all pins)


.. figure:: conductive_foam.png
	 :alt:    conductive_foam

.. figure:: conductive_foam2.png
	 :alt: conductive_foam2

.. figure:: Electrode.png
	 :alt: 	 electrode

potentiometer
______________

.. figure:: potentiometer.png
	 :alt: potentiometer

Hall sensor
____________

.. figure:: hallsensor.png
	 :alt: hallsensor

rotary encoder
_______________

**Using light**:

.. figure:: rotary_encoder.png
	 :alt: rotary_encoder

You can also use graycode to add some bits, with low propability of high errors if you .

.. figure:: rotary_encoder_graycode.png
	 :alt: rotary_encoder_graycode

**Using magnets**

.. figure:: magnet_encoder.png
	 :alt: magnet_encoder



Electronics
------------

.. math:: R = U/I \\
				  P = U*I

Resistors
_________

	 - ohmic resistor -> resistance by differing voltage constant
	 - non ohmic resistor -> resistance differs (like LED)

.. figure:: resitstortable.png
	 :alt: resitstortable

series connection / voltage devider
____________________________________

current all the same
devides voltage
resistance adds up

parallel connection
____________________________________

voltage all the same
current adds up
Rges = 1/R1 + ... + 1/Rn

relay
____________

.. figure:: relay.png
	 :alt: relay

H-bridge
___________

A h-bridge is a circuit that enables a voltage to be applied across a load **in either direction**. It is often used in robotics to allow DC motors to run forwards and backwards. They are available as integrated circuits or can be built from discret components.

.. figure:: hbridge.png
	 :alt: hbridge
