Mechanics
=========
A mechanism is a device designed to transform input forces and movement into a desired set of output forces and movement.

simple machines
---------------
There are four distinct basic machines, which can't be simplified further in underlying principles:

.. todo::

	- **rope and rod**: they move the point where a force is applied
	- **lever**: changes "ansatzpunkt" and size of a force and changes it's direction
	- **role**: 1

statics
-------


friction
--------

friction is a force that resists relative motion of solid surfaces, fluid layers and material elements sliding against each other. Friction is caused by bonds between the two objects on the atomic level.

.. figure:: friction.png
	 :alt: friction

.. figure:: Friction2.png
	 :alt: 	 friction2

There are three properties of friction:

.. figure:: frictionLaws.png
	 :alt: friction laws

.. figure:: FrictionTable.png
	 :alt: 	 friction table

- **static friction**: Friction between two solid objects which are not moving relative to each other.
- **dynamic friction**: Friction between two object which are moving relative to each other.

**Static friction is typically larger then the dynamic friction**.

.. figure:: frictionGraph.png
	 :alt: friction graph

A wheel creates less friction, because rolling breaks only a part of the molecular bonds between the surfaces at a time instead all of them all the time when pushing.

.. figure:: frictionWheel.png
	 :alt: friction wheel

ball bearings
_____________
The purpose of a ball bearing is to reduce rotational friction and support radial and axial loads. Because the balls are rolling they have a much lower coefficient of friction than if two flat surfaces were sliding against each other.

.. figure:: ballBearing.png
	 :alt: ball bearing

.. only:: html

	.. figure:: BallBearing.gif
		 :alt: ball bearing

This technique can also be applied in different mechanisms.

.. figure:: BallScrew.png
	 :alt: 	 ball screw

ball transfer table
___________________

A Ball transfer unit allows movement in all direction and is used to move heavy cargo around easily (used in planes...). They have the issue though that this is not a sealed system (like the ball bearings) so dirt can get into the inside over the big ball and cause friction inside.

.. figure:: ballTransfereTable.png
	 :alt: ball transfere table

.. figure:: BallTransfereUnit.png
	 :alt: 	 ball transfere unit

omni wheel
__________

The omni wheel is a wheel made from wheels

.. figure:: omniWheel.png
	 :alt: omni wheel

coupling
--------

capstan and bowstring
_____________________

A capstan is a reel(Rolle/Spule) or pully(a wheel with a grooved rim) around which the bowstring is wound several turns. This mechanism  converts rotary motion into linear motion and vice versa.

.. figure:: capstan.png
	 :alt: capstan

.. figure:: bow.png
	 :alt: bow

The capstan (big yellow wheel) is driven, but only grips the rope if there is tensionin it. Therefore you need to pull a bit, to create tension(bow), then the rope gets pulled by the capstan. A small holding force exerted on one side can carry a much larger loading force on the other side;

.. figure:: capstan2.png
	 :alt: capstan2

.. figure:: dreckselbank.png
	 :alt: dreckselbank

The cassette player uses a capstan in order to ensure the correct speed of the unwinding tape. If you would drive the tape rolls themselves the speed woud vary depending on the circumference of the tape roll. Therefore a capstan(the small metal wheel) drives the tape.

.. figure:: kassette.png
	 :alt: kassette

how much friction does a capstan produce?
###########################################

.. figure:: capstanfriction.png
	 :alt: capstanfriction

.. math:: T_{load} = T_{hold} * e^{µ \phi}

where :math:`T_{load}` is the applied tension on the line, :math:`T_{hold}` is the resulting force exerted at the other side of the capstan, :math:`T_{\mu}` is the coefficient of friction between the rope and capstan materials, and :math:`T_{\phi}` is the total angle swept by all turns of the rope, measured in radians.


Transmitting forces
-------------------

Transmission
------------

A transmission is a system which provides controlled application of the power. Often the term transmission refers simply to the gearbox that uses gars and gear trains to provide speed and torque conversions from a rotating power source to another device.

Gear train
__________

A geartrain is formed by mounting gears on a frame so that the teeth of the gears engage.

.. figure:: gearTrain.png
	 :alt: gear train

worm drive
___________

.. figure:: wormDrive.png
	 :alt: worm drive

rack and pinion
_______________

.. figure:: rackAndPinion.png
	 :alt: rack and pinion

jack screw
__________

.. figure:: jackScrew.png
	 :alt: jack screw

planetary gear
______________

.. figure:: planetaryGear.png
	 :alt: planetary gear

jaming
______

backlash
#########


.. figure:: backlash.png
	 :alt: backlash

Engineering fit
###############

.. figure:: engineeringFit.png
	 :alt: engineering fit

block and tackle
________________


.. figure:: blockAndTackle.png
	 :alt: block and tackle
