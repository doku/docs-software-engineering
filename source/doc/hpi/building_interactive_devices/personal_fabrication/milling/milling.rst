Milling
========

Milling maschines can either cut in 2D or 3D depending on the degrees of freedom of the robotic arm.

.. figure:: milling2d.png
	 :alt:  milling2d

.. figure:: Milling3d1.png
	 :alt: 	 milling3d1

.. figure:: Milling3d2.png
	 :alt: 	 milling3d2

Professional milling maschines can change the tools automaticly.

.. figure:: milling_tools.png
	 :alt: milling_tools


You can also use your own arm insteed of a robotic one:

.. figure:: dremel.png
	 :alt: dremel

benefits of milling
-------------------

 - you can process metal
 - beautiful, high-precision results, typically to make maschine parts



disadventages of milling
------------------------

 - you can't create vertical edges in a plane, if the milling maschine lacks the degree of freedom in the respective perpendicular plane

.. figure:: milling_problems.png
	 :alt: milling_problems

.. figure:: milling_fingerjoints1.png
	 :alt: milling_fingerjoints1

.. figure:: milling_fingerjoints2.png
	 :alt:    milling_fingerjoints2

milling examples
----------------


.. figure:: clip_tenons.png
	 :alt: clip_tenons
