3d Printing
===========
3d Printing is an additive manufacturing (AM) technique creating a part by adding layer by layer of material. Early 3d printers offered terrible print quality, but todays 100k+ industrial printers yield fantastic print quality.

different printing techniques
-----------------------------

fused deposition modelling
__________________________

In fused deposition modeling, the model or part is produced by extruding small beads or streams of material which harden immediately to form layers. A filament of thermoplastic, metal wire, or other material is fed into an extrusion nozzle head (3D printer extruder).

.. figure:: fdmlayers.png
	 :alt: fdmLayers

.. figure:: Fdmprinter.png
	 :alt: 	 fdmPrinter

The nozzle head heats the material and turns the flow on and off. Typically stepper motors or servo motors are employed to move the extrusion head and adjust the flow. The printer usually has 3 axes of motion.

With a polyjet printer (a printer with multiple extrusion nozzels) you can print with multiple materials at the same time and mix them to create unique structures and textures.


sintering
__________
Selective laser sintering (SLS) is an additive manufacturing (AM) technique that uses a laser as the power source to sinter(heat(but beneth the main melting point) under high pressure, so that particles connect) powdered material (typically nylon/polyamide), aiming the laser automatically at points in space defined by a 3D model, binding the material together to create a solid structure. So layer by layer th 3d object is created.

.. figure:: sintering.png
	 :alt: sintering

This technology can also be used to produce metal parts and is called Direct metal laser sintering (DMLS). This method is wide spread in aerospace. Its also used to print turbo turbines for racecars.

.. figure:: spacex3dprint.png
	 :alt: spacex3dprint

stereolithography
_________________

Stereolithography (SLA or SL; also known as stereolithography apparatus, optical fabrication, photo-solidification, or resin printing) is a form of 3-D printing technology creating parts in a layer by layer fashion using photopolymerization, a process by which light causes chains of molecules to link, forming polymers. Photopolymers are sensitive to ultraviolet light, so the resin is photochemically solidified and forms a single layer of the desired 3D object. This process is repeated for each layer of the design until the 3D object is complete. In models featuring an elevator apparatus,an elevator platform descends a distance equal to the thickness of a single layer of the design (typically 0.05 mm to 0.15 mm) into the photopolymer vat.

.. figure:: stereolithography.png
	 :alt: stereolithography


.. figure:: stereolithography2.png
	 :alt: stereolithography2


Continuous Liquid Interface Production CLIP
____________________________________________

Harnessing UV light and oxygen to rapidly grow a solid parts continuosly from a liquid polymer. The light can convert a resin to a solid and oxygen inhibits this process. By spacialy controlling the light and oxygen you can controll to process of building the part.

.. figure:: cLIP.png
	 :alt: CLIP

.. figure:: Bevorclip.png
	 :alt: 	 bevorCLIP

With oxygen comming from the bottom it inhibits the reaction and forms a deadzone(10µm thick).So right at the window interface it remains a liquid. By changing the oxygen content you can change the deadzone thickness.

This process is 25 - 100x faster than traditional 3d printers. Also by growing continiously this process removes the layers from the object. Therefore the surface gets is very smooth and the mechanical properties depend no longer on the print orientation.

.. figure:: surfaceclip.png
	 :alt: surfaceCLIP

.. figure:: Mechanical_properties_clip.png
	 :alt: 	 mechanical_properties_clip

https://www.ted.com/talks/joe_desimone_what_if_3d_printing_was_25x_faster#t-217991

Desingning 3d printed parts
---------------------------
3d prints can be designed with 3d modelling software and are then exported.
Then the printer (ususally by it self) decides where to add support material and which pattern to print inside the object. To save material 3d prints usualy aren't printed solid, but rather with a crossing pattern inside.

.. figure:: 3dprint_inner_structure.png
	 :alt: 3dprint_inner_structure

To enhance the structural regidity of your object perpendicular edges should be avoided if possible by adding fillets.

.. figure:: add_fillets.png
	 :alt: add fillets

The general rule of thumb for stiffness of your parts:

.. figure:: stiffness_thickness.png
	 :alt: stiffness = thickness ^ 3

What can you print and what not?
--------------------------------

Modern 3d printers are incredible capable and can print a wide variety of objects. You can print moving and interlooking parts in one piece, so no assembly is nessecary.

.. figure:: 3dprint1.png
	 :alt: 3dprint1

.. figure:: 3dprint_chain.png
	 :alt: 	 3dprint_chain

.. figure:: Ballandsocket.png
	 :alt: 	 ballandsocket

.. figure:: 3DExample.png
	 :alt: 3DExample

Multi material printers can print with support material which can later be removed or even desolves in water. They can also print with multiple colors or different material properties like stiffness, flexibility (ruber in the plastik), transparancy... This enabeles you to mix materials

.. figure:: desolvingprint.png
	 :alt: desolvingPrint

.. figure:: Mulitmaterialprint.png
	 :alt: 	 mulitmaterialPrint
