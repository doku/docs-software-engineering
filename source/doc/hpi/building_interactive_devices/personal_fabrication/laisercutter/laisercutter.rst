Laiser Cutter
=============

Laiser Cutters are using high power, focused laisers to cut the material, by vaporising it.

.. figure:: laisercutter1.png
	 :alt: laisercutter1

.. figure:: lasisercutter2.png
	 :alt: lasisercutter2

inner structure
---------------
The basic idea behind the laiser cutter is, to have a big fixed laiser and a moveable mirrors to project the light on the right point.
By moving the lense up and down you can ajust the focus of the laiser cutter to alter between cutting and engraving, by reducing the energy per area of the material.

.. figure:: inner_workings.png
	 :alt: inner_workings

Industrial laiser cutters can have multiple kilo watt of power and 5+ degrees of freedom.

laiser cutting joints
---------------------

3 axis laisers are limited to parts that are extruded 2d. Therefore joints are needed to break out of 2D abd build more complex structures. They also enable us to build larger objects or moving parts.


.. figure:: joints1.png
	 :alt: joints1

.. figure:: Verkeiltersteg.png
	 :alt: 	 verkeiltersteg

.. figure:: Joints2.png
	 :alt: 	 joints2

notch joints
____________

.. figure:: Notchjoints.png
	 :alt: 	 notchjoints

gears
------


.. figure:: gearWithBearings.png
	 :alt: gear with bearings


.. figure:: PlanetaryGear.png
	 :alt: 	 planetary gear


.. figure:: RackAndPinion.png
	 :alt: 	 rack and pinion


bending
-------

Acrylics can be bend using a heat gun.

.. figure:: bend1.png
	 :alt: bend1

you can also cut flexible materials to bend and interlock them into structures:


.. figure:: bendlamps.png
	 :alt: bendlamps

lattice hinge aka living hinge
______________________________

.. figure:: livingHinge.png
	 :alt: living hinge

.. figure:: LivingHinge2.png
	 :alt: 	 living hinge2

precision bend
_______________


perforated
__________

.. figure:: origami.png
	 :alt: origami


building curcit boards
-----------------------

First spray conductive material on base plate. By laisering lines of you can design a circuit.

Another way of building a circuit board is, to spraypaint upon a conductive material. Then laser parts of the paint of. Th
