Actuation
=========

solonoid
--------

A solenoid (Spule) is a is a coil wound into a tightly packed helix. The direction of the current determines the direction of the magnetic field created by it. Therefore it pull and push an ferromagnetic shaft in and out, thus converts electric energy in linear motion.

.. figure:: solenoidMagneticField.png
	 :alt: solenoid magnetic field

.. figure:: solonoid.png
	 :alt: solonoid

electric motor
--------------

brushed electric motor
______________________

.. figure:: brushedElectricMotor.png
	 :alt: brushed electric motor

brushless electric motor
_________________________

A brushless electric motor has permanenet magnets which rotate around a fixed armature, eliminating problems associated with connecting  current to the moving armature.

.. figure:: brushlessElectricMotorExample.png
	 :alt: brushless electric motor example


// todo omniwheel

pneumatics
----------

In pneumatics compressed air is used for actuation. Pneumatics are a transmission, they do not gererate a force. The transmition ratio is determined by the area of the inlel and the area of the cylinder.

.. figure:: pneumatics.png
	 :alt: pneumatics

.. todo:: FIX GIFS

.. only:: html

	.. figure:: einfachwirkender_zylinder_funktionsprinzip.gif

	.. figure:: doppelwirkender_zylinder_funktionsprinzip.gif

hydraulics
----------

Hydraulics use the same concept as pneumatics, but instead of using air as compression material they use a liquid (usually oil). Air is compressible, therefore

fish

.. figure:: hydraulicsVsPneumatics.png
	 :alt: hydraulics vs pneumatics

piezo electric effect
---------------------

Piezo elements are materials that expand or contract when you apply voltage. In reverse it produces voltage under compression

electroactive polymers


.. figure:: piezo1.png
	 :alt: piezo1



interesting mechanisms
----------------------

hexapod or Stewart platform
___________________________

A Stewart platform ihas six prismatic actuators, commonly hydraulic jacks or electric actuators, attached in pairs to three positions on the platform's baseplate, crossing over to three mounting points on a top plate. Devices placed on the top plate can be moved in the six degrees of freedom. These are the three linear movements x, y, z (lateral, longitudinal and vertical), and the three rotations pitch, roll, & yaw.

.. figure:: hexapod.png
	 :alt: hexapod

.. only:: html

	.. figure:: Hexapod_general_Anim.gif

This construction is often used ad base for flight/car simulators.

https://www.youtube.com/watch?v=oB0xv_sHA-U

step robots
___________

.. figure:: stepRobots.png
	 :alt: step Robots

2D treadmill
____________


.. figure:: 2DTreadmill.png
	 :alt: 2D treadmill


steam engine
____________

.. figure:: steamEngine1.png
	 :alt: steam engine1

.. figure:: steamEngine2.png
	 :alt:   steam engine 2

The lateral movement of the piston is translated into rotary movement.


compressor
__________

The compressor consists of to rotaing inerliefed helical screws which camptures air in a pocket between them. The space in which the air is trapped becomes smaller as it moves down the axis of the screws

.. figure:: compressor.png
	 :alt: compressor


electric multi-turn actuator
____________________________


.. figure:: electricMulti-turnActuator.png
	 :alt: electric multi-turn actuator


.. figure:: electricMulti-turnActuator2.png
	 :alt:    electric multi-turn actuator2

The pneumatics had an build in transmission, here we need to use a worm drive.

ultrasound
__________


.. figure:: ultraSound1.png
	 :alt: ultra sound 1


.. figure:: ultraSound2.png
	 :alt: ultra sound 2
