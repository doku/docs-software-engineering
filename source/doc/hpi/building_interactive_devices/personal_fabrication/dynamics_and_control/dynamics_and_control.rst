Dynamics and control
====================

Dynamics, statics and kinematics
--------------------------------

- dynamics is a branch of applie mathematics concerned with th study of forces and their effect on motion
- statics which is concerned with bodies at rest and forces in equilibrium)

control systems
---------------
A control system manages, commands, directs or regulates the behaviour of other devices or systems. It can range from a home heating controller using a thermostat controlling a domestic boiler to large Industrial control systems which are used for controlling processes or machines.
There are two common classes of control systems, open loop control systems, and closed loop control systems.With a control system you can replace a expensive actuator with a cheap one plus a cheap sensor.

.. figure:: controlSystems.png
	 :alt: control systems

open loop
_________

In an open loop control system, the control action from the controller is independent of the "process output". A good example of this is a central heating boiler controlled only by a timer, so that heat is applied for a constant time, regardless of the temperature of the building. (The control action is the switching on/off of the boiler. The process output is the building temperature).

.. figure:: openLoopExample.png
	 :alt: open loop example

.. figure:: openLoop.png
	 :alt:    open loop

Other examples: toaster, 3d Printers

closed loop
___________

In a closed loop control system, the control action from the controller is dependent on the **desired and actual process output values**. In the case of the boiler analogy this would utilise a thermostat to monitor the building temperature, and thereby **feed back a signal** to ensure the controller output maintains the building temperature to that set on the thermostat.
A closed loop controller therefore has a **feedback loop** which ensures the controller exerts a control action to give a process output the same as the "Reference input" or "set point". For this reason, closed loop controllers are also called feedback controllers.

.. figure:: closedLoopController.png
	 :alt: closed loop controller

.. figure:: closedLoopExample1.png
	 :alt: closed loop example 1


.. figure:: closedLoopExample2.png
	 :alt:    closed loop example 2


.. figure:: closedLoopExample3.png
	 :alt:    closed loop example 3

on/off control aka bang bang
#############################

.. code-block:: cpp

  if(measured value < low_threshold) turn on
  if(measured value > high_threshold) turn off


.. figure:: onOffController.png
	 :alt: on off controller

Examples: fridge, medium price soldering irons, and many more

proportional controller
#######################

The controller output is proportional to the error signal:

.. figure:: proportionalController.png
	 :alt: proportional controller

.. math::  P_{out} = K_p e(t) + p_0

- :math:`P_{out}`: Output of the proportional controller
- :math:`K_p`: Proportional gain
- :math:`e(t)`: Instantaneous process error at time t. :math:`e(t) = target - measuredValue`
- :math:`p_0`: Controller output with zero error.

The problem with the proportional controler is, that the actual target vaule is never reached only ever closer approximated, if the :math:`K_p` is to low. It has to be high enougth for the system to *overshoot* while correcting, so the system starts oszilating in a declining fassion around the desired value. The problem here is that we try to calibrate a system with a single constant, which is prone to errors in changing outer conditions.

.. figure:: pcExample.png
	 :alt:    pc example

proportional - integral control PI
##################################

The PI controller tales the past error values into account. So a small error from the desired output will accumulate over time and the controller will respond by applying a stronger action. This corrects the stady state error but tends to overshoot. Here the time window of the integral has to be choosen carefully to prevent overshooting as a reaction of a long corrected error.

proportional - integral - differential control PID
##################################################

A proportional–integral–derivative controller (PID controller) is a control loop feedback mechanism (controller) commonly used in industrial control systems. A PID controller continuously calculates an error value e(t) as the difference between a desired setpoint and a measured process variable and applies a correction based on proportional, integral, and derivative terms (sometimes denoted P, I, and D respectively) which give their name to the controller type.

- **P accounts for present values of the error**. For example, if the error is large and positive, the control output will also be large and positive.
- **I accounts for past values of the error**. For example, if the current output is not sufficiently strong, the integral of the error will accumulate over time, and the controller will respond by applying a stronger action.
- **D accounts for possible future trends of the error**, based on its current rate of change. For example, continuing the P example above, when the large positive control output succeeds in bringing the error closer to zero, it also puts the process on a path to large negative error in the near future; in this case, the derivative turns negative and the D module reduces the strength of the action to prevent this overshot.

.. figure:: dController.png
	 :alt: d controller

.. math:: u(t)=K_{p} e(t)+ K_{i} \int_{t-a}^{t} \! e(\tau )\, \mathrm{\tau}x + K_{d} \frac {de(t)}{dt}

where

- :math:`K_{p}` s the proportional gain, a tuning parameter
- :math:`K_{i}` is the integral gain, a tuning parameter
- :math:`K_{d}` is the derivative gain, a tuning parameter
- :math:`e(t)= SP - PV(t)` is the error (SP is the setpoint, and PV(t) is the process variable)
- :math:`t` is the time or instantaneous time (the present)
- :math:`\tau`  is the variable of integration (takes on values from time 0 to the present :math:`t`


.. figure:: pidControlSystem.png
	 :alt:    pid control system

The controller system needs to be calibrated:

.. figure:: pidCallibration.png
	 :alt:    pid callibration

holonomicity
------------

.. figure:: holonomicity.png
	 :alt: holonomicity
