==================================
Historie, Begriffe und Architektur
==================================

Kernaufgabe der meisten IT-Systeme ist die Verarbeitung von Daten. Daher ist effizientes speichern und zugreifen auf diese von essentieller Bedeutung.
So fallen zum Beispiel für einen Onlineversandhandel Kundendaten, Produktinformationen und Rechnungsdaten an. Ohne Datenbank kommt es hier oft zu Datenredundanzen.

Historie - Datenbanksysteme
===========================
 - Anfang 60er Jahre: anwendungsspezifische Datenorganisation (geräteabhängig, redundant, inkonsistent)
 - Ende 60er Jahre: Dateiverwaltungssysteme und Information Management System (IMS) vom IBM noch heute auf Mainframes im Einsatz (> 1 Mrd $ Umsatz)
 - 70er: Datenbanksystme (Geräte-  und Datenunabhängig, redundanzfrei, konsistent)
 - 1970: Ted Codd(IBM) Relationenmodell als konzeptionele Grundlage relationaler DBS
 - 1974: System R (IBM) erster Prototyp eines RDBMS
 - 1975: Ingres, Anfragesprache QUEL(Vorgänger von Postgres) University Berkley
 - 80er und 90er:

  - DBMS auf immer kleinern Systemen
  - immer größere Datenbanken mit Verteilung und Parallelität
  - Objektoreintierte Datenbanksysteme

 - 2000er:

  - speziallisierte Multimediadatenbanken
  - XML Datenbanken mit semistrukturierten Daten (XML Dokumente)
  - Verteilte Datenbanksysteme

 - 2010er:

  - Web-scale data (Google, Facebook...)
  - Spezialisierte DBMS

    - OLTP Online Transactional Processing
    - OLAP Online Analytical Processing

  - neue Hardware (SSD, Main-Memory, FPGA, GPGPU)

 - weitere Entwicklungen:
 
  - Data Warehouses
  - Data Mining
  - Data Streams
  - Information Retrieval / Search

Heute gibt es Datenbanken die viele Terrabyte oder bis zu mehreren Petabyte an Daten verwalten.

Genealogy of Relational Database Management Systems
===================================================

.. figure:: genealogydbms.png
	 :alt: GenealogyDBMS


Architektur
===========

Die Systemarchitektur gibt eine Beschreibung aller Komponenten eines Datenbanksystems, sowie eine Standardisierung der Schnittstellen zwischen den Komponenten an.

ANSI-SPARC-Architektur
----------------------

ANSI: American National Standards Institute
SPARC: Standards Planning and Requirements Committee

ANSI-SPARC-Architektur ist ein **DBMS Architekturvorschlag** von 1975, der die 3 Schichtenarchitektur verfeinert.
Wesentlich wurde hier die interne Ebene verfeinert und die Schnittstellen normiert.

.. figure:: ansi_sparc_architektur.png
	 :alt: ansi_sparc_architektur

Komponenten:
  - Definitionskomponente: DDL, Sichten, Dateiorganisation, Indizes
  - Programmierkomponente: Integration von DB-Operationen
  - Benutzerkomponente: Anfrageinterface + DB-Anwendungen
  - Transformationskomponente: Anfrageausführung und Darstellung der Ergebnisse
  - Data Dictionary: Metadaten über Datenbank (untersucht z.B. bei jeder Anfrage ob Attribute vorhanden sind)







.. figure:: datenbanken_schichtenmodell.png
	 :alt:


Anforderungen an DBMS nach Codd 1982
=====================================
- Integration
  - Einheitliche, nichtredundante Datenverwaltung
- Operationen
  - Definieren, Speichern, Abfragen, Ändern
  - Deklarativ
- Benutzersichten
  - verschiedene Anwendungen, Zugriffskontrolle, Umstrukturierung
- Integritätssicherung
  - Korrektheit und Konsistenz des Datenbankinhalts gewährleisten
- Transaktionen
  - Mehrere DB-Operationen als eine Funktionseinheit ausführen
- Synchronisation
  - Parallele Transaktionen koordinieren
- Datenschutz
  - Datenzugriff nur für autorisierte Nutzer
- Datensicherung
  - Wiederherstellung von Daten nach Systemfehlern
  - Persistenz -> Daten nicht im Hauptspeicher, sondern auf Disk
- Katalog
  - speichert Datenbankbeschreibungen, Metadaten und Statistiken

Aktuelle DBMS
==============

- Drei-Ebenen-Architektur nach ANSI-SPARK
- Einheitliche Datenbanksprache: SQL (Structured Query Language)
- Einbettung der Anfragesprache in andere Programmiersprachen (Embedded SQL, Prepared Statements Bsp. JDBC)
- kontrollierter Mehrbenutzerbetrieb, Transaktionsmanagement, Zugriffskontrollen
- Tools für Backup, Optimierung etc.
