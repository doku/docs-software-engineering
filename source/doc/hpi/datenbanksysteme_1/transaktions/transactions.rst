============
Transactions
============

Eine Transaktion ist eine **Folge von Operationen**, die eine Datenbank von einem **konsistenten Zustand** in einen konsistenten Zustand überführt, wobei das **ACID-Prinzip** eingehalten werden muss.

ACID
======

  - **Atomicity** (Atomarität): eine Transaktion wird entweder ganz oder gar nicht ausgeführt
  - **Consistency** (Konsistenz oder auch Integritätserhaltung): Eine Datenbank befindet sich vor Beginn und nach Beendigung einer Transaktion jeweils in einem konsistenten Zustand
  - **Isolation**: Die Transaktion, die auf der Datenbank arbeitet soll den "Eindruck" haben, dass die alleine auf der Datenbank arbeitet (wenn mehrere Transaktionen gleichzeitig bearbeitet werden)
  - **Durability** (Dauerhaftigkeit/ Persistenz): Nach erfolgreichem Abschluss einer Transaktion muss das Ergebnis dieser Transaktion "dauerhaft" in der Datenbank gespeichert werden

Probleme beim Mehrbenutzerbetrieb
=================================

- Abhängigkeiten von nicht freigegebenen Daten: **Dirty Read**
- Inkonsistentes Lesen: **Nonrepeatable Read**
- Berechnungen auf unvollständigen Daten: **Phantom Problem**
- Verlorengegangene Änderung: **Lost Update**


.. figure:: dirty_read.png
	 :alt: dirty_read


.. figure:: nonrepeatable_read.png
	 :alt:    nonrepeatable_read

.. figure:: phantom_problem.png
	 :alt: phantom_problem


.. figure:: lost_update.png
	 :alt:    lost_update

Transaktionen in SQL
====================

Die Idee ist es, **mehrere Operationen zu gruppieren**, deren **Ausführung atomar (per Definition) und serialisierbar(per SQL Standard)** sind. Ein einfacher SQL Befehl entspricht einer Transaktion und ausgelöste ``TRIGGER`` werden innerhalb der Transaktion ausgeführt.
Eine Transaktion wird mit ``START TRANSACTION`` begonnen und ein erfolgreiches Ende mit ``COMMIT`` signalisiert. ``ROLLBACK`` oder ``ABORT`` signalisiert ein Scheitern der Transaktion.**Bricht eine Transaktion ab werden alle erfolgten Änderungen Rückgängig gemacht**

Isolationsebenen
================

Isolationsebenen beschreiben wie sich Transaktionen verhalten, wenn parallel andere Transaktionen auf die Daten zugreifen. Man könnte eine Transaktion arbeiten lassen und alle Daten für die restlichen Transaktionen sperren, doch dies verhindert Parallelität und verlangsamt die Ausführung.

.. figure:: isolationsebenen.png
	 :alt: isolationsebenen

Standardeinstellungen: ``set transaction read write, isolation level serializable``

- **read uncommitted**: schwächste Stufe: Zugriff auf nicht fertig geschriebene Daten, kein sperren der Daten ist nötig, keine anderen Transaktionen werden behindert. Dies kann für Transaktionen, die Statistische Untersuchungen vornehmen verwendet werden, wenn ein ungefährer Überblick nötig ist und keine perfekten Werte gefordert werden
- **read committed**: Nur lesen endgültig geschriebener Werte, aber ``Nonrepeatable Read`` möglich.
- **repeatable read**: kein ``Nonrepeatable Read`` möglich, aber ``Phantomproblem`` kann auftreten
- **serializable**: garantierte Serialisierbarkeit, Transaktion sieht nur Änderungen, die zu Beginn der Transaktion committet waren.

.. figure:: isolationsebenen_folgen.png
	 :alt: isolationsebenen_folgen

Seriell vs. Parallel
====================

Man könnte alle Transaktionen seriell ausführen, doch dies wäre unglaublich ineffizient. Lange Transaktionen können sich über mehrere Stunden hinwegziehen und den seriellen Betrieb blockieren. Daher werden korrekte Pläne, sogenannte **Schedules** gesucht, die eine Struktur angeben, wie Transaktionen parallel ausgeführt werden können und zum Gleichen Ergebnis kommen wie seriell ausgeführte Transaktionen.

Schedules
===========

Ein Schedule ist eine geordnete Abfolge von wichtigen Aktionen, die von einer oder mehrerer Transaktionen durchgeführt werden. Wichtige Aktionen sind dabei ``READ`` und ``WRITE``.

- **Schedule** einen **Ablaufplan** für Transaktionen, bestehen aus einer Abfolge von Transaktionsoperationen
- **serieller Schedule**: Schedule in dem die **Transaktionen hintereinander** ausgeführt werden
- **serialisierbarer Schedule**: Es existiert ein serieller Schedule mit identischem Effekt

.. figure:: schedules1.png
	 :alt: schedules1

Nicht alle Schedules sind serialisierbar.

.. figure:: schedules2.png
	 :alt: schedules2

.. figure:: schedules3.png
	 :alt:    schedules3

Es kann auch sein, dass die Reihenfolge, in der Transaktionen ausgeführt werden einen Unterschied im Ergebnis hervorruft. Dies kann nicht berücksichtigt werden. Das DBMS nimmt sich die Freiheit die Reihenfolge zu Optimierungszwecken zu wählen

.. hint:: Ist die Reihenfolge von Transaktionen entscheidend, müssen diese in einer Transaktion festgehalten werden.

Konflikserialisierbarkeit
=========================

Die Konflikserialisierbarkeit ist eine Bedingung für die Serialisierbarkeit und wird von den meisten DBMS verlangt. Konflikte herrschen zwischen zwei Aktionen eines Schedules, falls die Änderung ihrer Reihenfolge das Ergebnis verändern *kann* (nicht muss).

.. figure:: konflikte.png
	 :alt: konflikte

.. note::

  Ein Konflikt herrscht falls zwei Aktionen das **gleiche Datenbankelement** betreffen und **mindestens eine der beiden Aktionen** ein ``WRITE`` ist.

Die Idee der Konflikserialisierbarkeit ist es nicht-konfligierende Aktionen zu tauschen, bis ein serieller Schedule vorliegt. Falls dies klappt ist der Schedule serialisierbar. Zwei Schedules heißen **konfliktäquivalent**, wenn die Reihenfolge aller konfligierender Aktionen in beiden Schedules gleich ist. Ein Schedule ist genau dann konfliktserialisierbar, wenn er konfliktäquivalent zu einem seriellen Schedule ist.

.. note::

  Aus Konfliktserialisierbarkeit folgt Serialisierbarkeit.

Graphenbasierter Test
---------------------

  Man kann einen Konfliktgraphen für einen Schedule anlegen. Hier entsprechen die Knoten den einzelnen Transaktionen und die gerichteten Kanten einem Konflikt. Enthält dieser **Graph einen Zyklus** ist der schedule **nicht konfliktserialisierbar**. Enthält er keinen Zyklus kann man durch **topologisches Sortieren der Knoten** (Transaktionen) einen **konfliktäquivalenten seriellen Schedule** bestimmen.

.. figure:: graph_test.png
	 :alt:   graph_test

.. figure:: graph_test2.png
	 :alt:    graph_test2

Sperren (Locking)
------------------

- **legaler Schedule**: Schedule, der kein gesperrtes Objekt erneut sperrt
- **konsistente Transaktionen**: Transaktionen, die alle Aktionen nur auf korrekt gesperrten Objekten ausführen und gesperrte Objekte nach Verwendung wieder freigeben.
- **2-Phase-locking(2PL)**: Alle Sperren einer Transaktion erfolgen vor der ersten Freigabe; ermöglicht Konfliktserialisierbarkeit, **2PL Gild für Transaktionen nicht Schedules**

.. figure:: konzeptionelles_schaubild.png
	 :alt: konzeptionelles_schaubild


.. figure:: konfliktserialisierbarkeit1.png
	 :alt: konfliktserialisierbarkeit1


.. figure:: konfliktserialisierbarkeit2.png
	 :alt: konfliktserialisierbarkeit2


.. figure:: opti_pessi_locking.png
	 :alt: opti_pessi_locking

Tritt beim Locking ein Konflikt auf, wird die Abarbeitung dieser Transaktion pausiert und andere Weiterbearbeitet.

.. figure:: konflikt_locking.png
	 :alt:    konflikt_locking

Kommt es zu einem wirklichen Deadlock (zwei Transaktionen warten gegenseitig auf ein unlock) bricht das DBMS die Transaktion ab.

Sperrkonzepte
--------------

  - **Schreibsperre**: *Exclusive Lock*, Sperrt zugriff aller anderen Transaktionen ``xl(x)``
  - **Lesesperre**: *Shared Lock*, erlaubt lesen, sperrt schreiben durch andere Transaktionen ``sl(x)``

Für ein Objekt darf es nur eine Schreibsperre und mehrere Lesesperren geben.
Der Unlock ``u(x)`` gibt alle Arten von Sperren frei.


.. figure:: weitere_sperrarten.png
	 :alt: weitere_sperrarten
