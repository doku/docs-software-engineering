==============
Normalisierung
==============

Bisher wurde eine **direkte Übersetzung vom ER-Diagramm in das relationale Modell** vorgenommen. Dabei sind jedoch **Verbesserungen möglich**

Funktionale Abhängigkeit(FA) / functional dependencies (fd)
===========================================================

:math:`X \rightarrow A` ist eine Aussage über eine Relation, die besagt, dass zwei Tuple, die in den Werten der Attributmenge :math:`X` übereinstimen auch im Attributwert A übereinstimmen.

.. figure:: fd_regeln.png
	 :alt: fd_regeln

Funktionale Abhängigkeiten sind
  - **trivial**, wenn die Attribute rechts eine Teilmenge der Attribute links sind
  - **nicht-trivial**, wenn wenigstens ein Attribut rechts, auf der Linken Seite nicht vorkommt
  - **völlig nicht-trivial**, wenn die Attribute rechts und links disjunkt sind

Funktionale Abhängigkeiten sind **Aussagen über das Schema und nicht eine Instanz**. In der Regel werden Funktionale Abhängigkeiten von Domainexperten bestimmt. Sie können auch aus ER-Diagrammen abgelesen werden.

Schlüssel
---------
Schlüssel sind eine Menge von Attributen, die alle anderen Attribute funktional bestimmen. Auch darf keine echte Teilmenge der Schlüsselattribute ebenfalls alle anderen Attribute funktional bestimmen, der Schlüssel muss also **minimal** sein. Eine Relation kann mehr als einen Schlüssel haben, hier muss ein Primärschlüssel gewählt werden. Ein **Superschlüssel** ist eine Obermenge des Schlüssels und damit **nicht minimal**.

Ist :math:`X` ein Schlüssel, dann gilt :math:`X \rightarrow R \setminus X`, jedoch folgt aus :math:`X \rightarrow R \setminus X` nicht, dass :math:`X` ein Schlüssel ist, da die linke Seite einer Funktionalen Abhängigkeit doppelte Werte erlaubt. Ist :math:`X` kein Schlüssel folgt, dass es keinen Schlüssel für die FD gibt.



Das Ziel des Datenbankentwurfs ist durch Normalisierung alle

Hüllenbildung
==============

Hülle von :math:`{A}` = :math:`{A}^+`

Vorgehen: Füge so lange Attribute in {A} mittels funktionaler Abhängigkeiten hinzu bis keine Erweiterung mehr möglich ist.

gesucht: alle Minimalen Schlüssel, unter Angabe der Funktionalen Abhängigkeiten. Dazu müssen alle Teilmengen von Attribunten auf Schlüsseleigenschaft überprüft werden. Kommt ein Attribut auf keiner rechten Seite vor, wird es von nichts funktional bestimmt und muss Teil des Schlüssels sein.
