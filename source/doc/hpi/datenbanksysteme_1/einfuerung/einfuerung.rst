Einführung
============

Kernaufgabe der meisten IT-Systeme ist die Verarbeitung von Daten. Daher ist effizientes speichern und zugreifen auf diese von essentieller Bedeutung.
So fallen zum Beispiel für einen Onlineversandhandel Kundendaten, Produktinformationen und Rechnungsdaten an. Zunächst wollen wir uns einige Probleme verdeutlichen, die ohne Nutzung eines Datenbankmanagementsystems(DBMS) entstehen. Die oft vielfältig in Beziehung stehenden Daten müssten dann in isolierten Datein gespeichert werden.

- **Redundanz und Inkonsistenz**: Durch isoliertes speichern müssen Daten die an mehreren Stellen benötigt werden, redundant gespeichert werden. Änderungen an einer Instanz der Daten kann dann zu Inkonsistenz führen. In einem globalen, integrierten DBMS wird diese Art von unkontrollierter Redundanz vermieden.
- **Beschränkte Zugriffsmöglchkeiten**: Das Verknüpfen der isoliert gespeicherten Daten ist schwer bis unmöglich. In einem DBMS wird die gesamte Datenstruktur einheitlich modelliert.
- **Problem Mehrbenutzerbetrieb**: In heutigen Dateisystemen ist der Mehrbenutzerbetrieb kaum möglich (Problem des "Lost Update"). DBMS bieten Mehrbenutzerkontrolle, die solche Anomalien verhindert.
- **Verlust der Daten**: Wenn die Daten in isolierten Dateien gehalten werden, wird die Wiederherstellung eines konsistenten Gesamtzustandes im Fehlerfall sehr schwierig. Dateisysteme bieten oft nur periodische Backups an, Datenverluste während der Bearbeitung führen so zu Problemen. Mit dem Konzept der Transaktionen bieten DBMS eine Lösung dieses Probems.
- **Integritätsverletzung**: Je nach Anwendungsgebiet gibt es vielfältige Integritätsbedingungen, die sich über mehrere isolierte Datein erstrecken können. Oft möchte man die Konsistenz erzwingen, d.h. Änderungen, die diese verletzen, unterbinden. In Datenbanken werden Zustände als konsistent bezeichnet, wenn sie die Integritätsbedingungen erfüllen, sie also der Modellierung nach eine "richtige" Instanz darstellt. In DBMS werden Transaktionen(ein Verarbeitungsvorgang) nur dann vollzogen, wenn die die Datenbasis in einen Konsistenten Zustand überführen.
- **Sicherheitsprobleme**: Hoch spezifizierte Zugriffsbeschränkungen sind in Dateisystemen kaum möglich, in DBMS ist es sogar möglich Zugriffsrechte Attributsweise zu vergeben.
- **Entwicklungskosten**: Ohne DBMS müsste man für jede Anwendung das "Rad neu erfinden" und eine Dateiverwaltung entwickeln. DBMS stellen eine komfortable Schnittstelle dar, die Entwicklungszeiten verkürzt und die Fehleranfälligkeit reduziert.

Datenabstraktion / Schichtenmodell
----------------------------------

Man unterscheidet drei Abstraktionsebenen:

.. image:: schichtenmodell.png
	 :scale: 30 %
	 :align: right
	 :alt: Schichtenmodell

1. **interne Schicht / physische Ebene**: legt fest, wie die Daten gespeichert werden (meist Plattenspeicher, aber auch In-Memory)
2. **konzeptionelle Schicht / logische Ebene**: hier wird in dem Datenbankschema definiert, welche Daten abgespeichert sind. Sie ist der Bezugspunkt für die interne und externe Schicht.
3. **externe Schicht / Sichten**: Das Datenbankschema der logischen Ebene stellt die gesamte Informationsmenge dar, Sichten nur eine Teilmenge und können damit auf Bedürfnisse zugeschnitten werden. Kann bereits Aggregationen und Transformationen enthalten.

.. image:: schichten_beispiel.png
	 :alt: schichten beispiel
	 :scale: 30 %
	 :align: right

Die physische Ebene ist für den normalen Benutzer einer Datenbank irrelevant und obliegt der Pflege eines Datenbankadministrators(DBA). Änderungen hier werden zur Leistungssteigerung vorgenommen. Möchte man das Datenmodell ändern, wird dies in der logischen Ebene vorgenommen. Auf der Ebene der Sichten können die im Datenbankschema (logische Ebene) festgelegten Strukturen auf Nutzerbedürfnisse zugeschnitten werden, z.B. können geheime Daten ausgeblendet werden.


Datenunabhängigkeit
-------------------

Das Ziel der Schichtenarchitektur ist die **Entkopplung der Schichten** um Änderungen an einer Schicht zu ermöglichen, ohne dass dies zu Problemen bei anderen Schichten führt.

 - Portierbarkeit: interne Schicht soll einfach ausgetauscht werden können (Bsp.neue Server, neue DB-Engine )
 - Standardisierte Schnittstellen (SQL)
 - Stabilität der Anwendungsschnittstellen gegen Änderungen

Die drei Ebenen des DBMS ermöglichen einen Grad der Datenunabhängigkeit. Durch die wohldefinierten Schnittstellen wird die darunterliegende Implementierung verdeckt. Aufgrund der drei Schichten ergeben sich zwei Stufen der Datenunabhängigkeit.

- **Physische Datenunabhängigkeit**: Die Modifikation der physischen Speicherebene (z.B. Dateiorganisation) belässt die logische Ebene invariant, z.B. hat das Nachträgliche anlegen eines Indexes keinen Einfluss auf die Anwendungen, bis auf schnellere Suchen
- **logische Datenunabhängigkeit**: Zwar wird in den Anwendungen Bezug auf die logische Struktur der Datenbasis genommen, jedoch können kleine Änderungen wie Umbenennung von Attributen, vor den Sichten verborgen werden.

Heutige Datenbanken erfüllen meist nur die physische Datenunabhängigkeit, die logische Datenunabhängigkeit kann bereits rein konzeptionell nur für marginale Änderungen gewährleistet werden. So können Änderungen des Datenmodells, wie z.B. das Einfügen eines neuen Attributs auch auf physischer Ebene zu Problemen führen, da nun eine ganz neue Spalte eingefügt werden muss. Da die Daten jedoch in der Regel zeilenweise hintereinander abgespeichert sind, müssten die Daten komplett neu angeordnet werden, was bei großen Datenmengen sehr teuer sein kann, und bei laufenden Systemen großen Schwierigkeiten verursacht.


Datenmodelle
-------------

DBMS basieren auf einem Datenmodell, das die Datenobjekte beschreibt und die anwendbaren Operationen, sowie deren Wirkung festlegt. Das Datenmodell besteht demnach aus zwei Teilsprachen:
1. **Data Definition Language (DDL)**: definiert die Struktur der abzuspeichernden Datenobjekte. Die **Strukturbeschreibung aller Datenobjekte** des betrachteten Anwendungsbereiches nennt man das **Datenbankschema**
2. **Data Manipulation Language (DML)**: besteht aus einer Anfragesprache(Query Language, Bsp. SQL) und der eigentlichen Datenmanipulationssprache, zum Bearbeiten, Löschen, Einfügen der Daten. Sie kann zum einen als DML-Kommandos im Terminal genutzt werden oder als eingebettete DML-Kommandos in höheren Programmiersprachen.

Datenbankschema und Ausprägung
------------------------------

Datenbankschema und Ausprägung gilt es zu unterscheiden. Das Datenbankschema legt die Struktur der abspeicherbaren Datenobjekte fest, sagt aber nichts über die individuellen Datenobjekte aus. Bei einer Ausprägung handelt es sich um einen gültigen Zustand der Datenbasis. Im Sinne der ER Modellierung könnte man sagen, dass das Datenbankschema die Entitätstypen festlegt, wohingegen die Ausprägung eine Menge konkreter Entitäten ist.

Datenmodellierung
-----------------

Aufgaben des DBMS
-----------------

- Unterstützung des Datenmodells (in der Regel relationale Daten)
- Bereitstellung einer Anfragesprache
- Effiziente Anfragebearbeitung
- Robustheit (Wahrung der Datenintegrität -> Konsistenz)
- Speicherverwaltung (RAM & Disk)
- Transaktionsmanagement (auch im Mehr-Benutzer-Betrieb)
- Nutzerverwaltung und Zugangskontrolle

.. figure:: datenbanken_schichtenmodell.png
	 :alt:

In der Datenbank selbst sind Daten und Metadaten(Beschreibungen der Daten, sowie Statistiken über diese) gespeichert.
Ein Datenbankmanagementsystem(DBMS) ist eine Softwarekomponente zum Zugriff auf eine oder mehrere Datenbanken.
