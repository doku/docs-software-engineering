=============================
Relationaler Datenbankentwurf
=============================

Modelle des konzeptionellen Entwurfs
====================================

Ziel des Datenbankentwurfes ist es, einen Anwendungsrelevanten Ausschnitt der "realen" Welt, die sogenannte Miniwelt, konzeptuell zu modellieren. Dazu gibt es mehrere Datenmodelle:

  - **Entity-Relationship-Modell**: Konzepte der Welt werden in Gegenstandsmengen und Beziehungen zwischen diesen strukturiert, am weitesten verbreitetes Modell
  - **semantisches Datenmodell**:
  - **objektorientierte Entwurfsmodelle**

Konzeptuelle Datenmodelle verfügen im Allgemeinen nur über eine DDL und haben keine DML, da die nur die Struktur der Daten beschreiben. Dies ist auch nicht nötig, da es sich um Konzeptuelle Modelle handelt und auf eine Datenbankausprägung verzichtet wird.

Datenbankentwurf
================

Der Entwurf ist von besonderer Bedeutung, in der Regel die Datenerhaltung für mehrere Anwendungssysteme über viele Jahre gewährleistet werden muss. Die Daten müssen daher möglichst **effizient** und **nicht redundant** gespeichert werden.

.. image:: datenbankentwurf.png
  :alt: datenbankentwurf
  :scale: 50 %
  :align: right

- **Anforderungsanalyse** : informelle Beschreibung(Texte, Tabellen, etc.), Trennen von Informationen über Daten (Datenanalyse ->"klasischer" DB-Entwurf) und Informationen über Funktionen (Funktionsanalyse)
- **Konzeptioneller Entwurf**: formale Beschreibung häufig mit ER-Diagrammen, Vorgehensweise: Modellieren der Sichten (z.B. für verschiedene Fachabteilungen) und Analyse der Sichten im Bezug auf Konflikte(Namens-, Typ-, Bedingungs-, Strukturkonflikte: z.B. Speicherung männlich weiblich) und anschließende Integration der Sichten in ein Konzeptionelles Gesamtschema (z.B. ER- Diagramm)
- **Verteilungsentwurf**: Partitionierung, Sollen die Daten auf mehreren Servern verteilt vorliegen, muss die Art und Wiese der verteilten Speicherung festgelegt werden(horizontale/vertikale Partitionierung)
- **Logischer Entwurf**: Relationenschemata, Konzeptionelles Modell(ER) in relationales Modell überführen (häufig automatische Transformation) und verbessern (Normalisierung, Redundanzvermeidung); Ergebnis: logisches Schema (z.B. Sammlung von Relationenschemata)
- **Datendefinition**: Übersetzung des logischen Schemas in ein konkretes Schema; Sprachmittel SQL (DDL, DML) eines DBMS(Oracle, DB2,...): Datendeklaration in der DDL eines DBMS (``CREATE TABLE``), Realisierung der Integritätssicherung(Schlüssel, Fremdschlüssel, Nebenbedingungen, Datentypen), Definition von Benutzersichten (``CREATE VIEW``)
- **Physischer Entwurf**: wird von DBMS automatisch vorgenommen,  Parameter und Indizes
- **Implementierung und Wartung**: Installation, Optimierung, Backups, etc. (kostenaufwändigste Phase)


Relationale Modell
==================
Eine Relation ist eine Menge von Tupeln und eine Teilmenge des Kartesischen Produktes der n-Trägermengen.
Im Sinne von Datenbanken bilden die Trägermengen die Domänen(Wertebereiche, z.B. Integer, String, ...) und entsprechen den Spalten der Tabelle. Ein Tuple ist eine Menge von Datenwerten und entspricht einer Zeile. Damit ist Konzeptionell eine Datenbank eine Menge von Tabellen, wobei die Tabellen Relationen zischen den Werten der Attributdomänen entsprechen.

.. figure:: relationales_modell.png
	 :alt: relationales_modell

Logischer Entwurf
=================

Ziel des logischen Entwurfs ist die Abbildung des ER-Modells in ein relationales Modell. Dabei soll das Datenbankschema *genau so viel Instanzen* wie das ER-Diagramm darstellen, nicht mehr, und nicht weniger.


.. figure:: kapazitaetserhoehende_abbildung.png
	 :alt: kapazitätserhöhende_abbildung

Genau ein Vorsitzender leitet ein Studio. Damit ist sowohl der Name des Vorsitzenden, als auch des Studios ein Schlüssel der Relation. Würde man beide Schlüssel zu einem zusammenfassen ``{SName, Vname}`` wäre dies kapazitätserhöhend, da nun potentiell ein Vorsitzender zwei Studios leiten kann, solange diese unterschiedliche Namen haben.

.. figure:: kapazitaetsreduzierende_abbildung.png
	 :alt:    kapazitätsreduzierende_abbildung

Wäre der Name des Schauspielers der Schlüssel der Relation, könnte jeder Schauspieler in nur einem Film mitspielen.

Transformationsalgorithmus
--------------------------

**1. Entitätstypen in Relation umwandeln**

.. figure:: entitiys_umwandeln.png
	 :alt: entitiys_umwandeln

**2. Relationstypen in Relation umwandeln**

Dazu die Attribute der Relation selbst und die Schlüsselattribute der beteiligten Entitätstypen hinzufügen. Bei doppelten Attributnamen ist eine Umbenennung nötig. Falls ein Entitätstyp an mehreren Rollen beteiligt ist müssen entsprechend oft die Schlüsselattribute übernommen und entsprechen umbenannt werden.

.. figure:: relationen_umwandeln.png
	 :alt: relationen_umwandeln

.. figure:: relationen_umwandeln2.png
	 :alt:    relationen_umwandeln2

**3. Entwurf verfeinern:**

**3.1 Zusammenlegen von Relationen**

Man kann Relationen für einen Entitätstypen E oder Relationen eines 1:n Relationshiptypen R falls E auf der n-Seite liegt kombinieren. Die neue Relation enthält alle Attribute von E und R(inkl. Schlüssel des anderen Entitytypen)

.. figure:: zusammenlegen_1n.png
	 :alt: zusammenlegen_1n


.. figure:: zusammenlegen_11.png
	 :alt:    zusammenlegen_11

Das Zusammenlegen von m:n Relationen ist nicht möglich, da dies zu unkontrollierter Redundanz führt, was beim Updaten eines Filmes zu Inkonsistenzen führt.

.. figure:: zusammenlegen_mn.png
	 :alt:    zusammenlegen_mn

.. figure:: schwache_entitytypen.png
	 :alt: schwache_entitytypen

.. figure:: schwacheEntitytypen2.png
	 :alt:    schwache entitytypen2

**3.2 Normalisierung**

Konvertierung von Spezialisierungen
-----------------------------------

  - ER-Still: Für jeden Entitytypen E der Hirarchie erzeuge eine Relation mit den Schlüsselattributen des Wurzel-Entity
