==================================
Anfragebearbeitung und Optimierung
==================================

.. figure:: ablauf.png
	 :alt: ablauf

SQL Anfragen werden in einen Parsebaum umgewandelt.

.. figure:: parse_baum.png
	 :alt: parse_baum

Anfragebearbeitung-Transformationsregeln
========================================

Anfragen können äquivalent umgeformt werden.

.. figure:: umformungs_regeln1.png
	 :alt: umformungs_regeln1

.. figure:: umformungsregeln2.png
	 :alt:    umformungsregeln2

Optimierung
===========

Anfragen können nach zwei Modellen optimiert werden:

  - **Regelbasierte Optimierung**: Fester Regelsatz schreibt Transformationen vor, die Priorität der Umformungen beruht auf Heuristiken.
  - **Kostenbasierte Optimierung**: Es besteht ein Kostenmodell für Operationen und Transformationsregeln, die die Kosten verringern; damit wird der optimale Plan bestimmt

.. hint:: Im Allgemeinen wird nicht die optimale Auswertung gesucht, sondern es gilt: **Avoid the worst case**

  - **logische Optimierung**: jeder Ausdruck kann semantisch äquivalent umgeformt werden; es soll ein best möglicher Ausdruck gewählt werden
  - **physische Optimierung**: jede relationale Operation kann verschieden implementiert werden (Bsp. Zugriff auf Tabellen: Scan, Index, sortierter Zugriff); (Bsp. Joins: Nested loops, sort-merge, hash,...); es soll die für diesen Fall beste Implementierung gewählt werden.

  Beide Optimierungen hängen voneinander ab.

logische Optimierung - regelbasiert
-----------------------------------

  - Selection so weit wie möglich im Baum nach unten schieben
  - Selectionen mit ``AND`` können aufgeteilt und separat verschoben werden
  - Projektionen so weit wie möglich im Baum nach unten schieben, bzw. wo möglich(Endergebnis wird nicht verändert) neue Projektionen einfügen
  - Duplikateliminierung kann manchmal entfernt oder verschoben werden
  - Kreuzprodukt mit geeigneten Selektionen zu einem Join zusammenfassen
  - Joinreihenfolge optimieren


.. figure:: anwendung_transformationsregeln1.png
	 :alt: anwendung_transformationsregeln1


.. figure:: anwendung_transformationsregeln2.png
	 :alt: anwendung_transformationsregeln2

.. hint:: Im Allgemeinen sollen große Zwischenergebnisse vermieden werden.

Kostenmodel
-----------
Konzept: Generieren aller denkbaren Anfrageausführungen und Bewertung der Kosten anhand eines Kostenmodells, basierend auf Statistiken zu den Daten, kalibriert nach verwendetem Rechner, abhängig vom verfügbaren Speicher und nach einem Aufwandskostenmodell (nicht Antwortzeit, sondern Durchsatz optimiert). Der billigste Plan soll ausgeführt werden. Dabei muss darauf geachtet werden **nicht zu lange zu optimieren**.

Die Größe des Suchraumes wächst überexponentiell, das Opimierungsproblem ist NP-hart.

Kosten von Operationen
######################

  - **Projektion**: Keine Kosten falls mit anderen Operatoren ausgeführt
  - **Selektion**: Ohne Index: linear; mit Baumindex: logarithmisch; bei Pipelining: fast keine Kosten
  - **Join**: hängt von Joinalgorithmus ab

  .. hint:: Entscheidend ist oft, ob Zwischenergebnisse in Hauptspeicher passen!

Daher werden die Anzahl der Ausgabetupel für jede Operation bestimmt.

``#Ausgabetuple = #Eingabetuple * Selektivität``


Selektivität
############

.. figure:: selektivitaet.png
	 :alt: selektivitaet


.. figure:: uebung1.png
	 :alt:  uebung1

.. figure:: loesung1.png
	 :alt:    loesung1
