===========================================
Entity-Reationship Modelling (ER-Modelling)
===========================================

ER-Modellierung
===============

- Entität: Instanz eines Entitätstypen
- Entitätstypen: Klasse gleichartiger Objekte; der Typ legt in der Modellierung fest welche Instanzen möglich sind
- Relationship: Beziehung zwischen zwei Entitäten, meist durch Beziehungen zwischen zwei Entitätstypen modelliert
- Relationshiptyp: Klasse gleichartiger Beziehungen
- Attribut: repräsentiert eine Eigenschaft einer Entität oder einer Relationship


.. figure:: er1.png
	 :alt: er1


.. figure:: er2.png
	 :alt:    er2

B ist eine Spezialisierung von A.
Mehrfachvererbung ist nicht erlaubt, d.h. Ist-Beziehungen müssen eine Baumstruktur haben.
Eine Entity kann aus mehreren Komponenten des Ist-Baumes bestehen, also Eigenschaften aus mehreren Entitäten der Vererbungshierarchie übernehmen (Im Unterschied zur Objekt orientierten Modellierung, in der ein Objekt immer genau in einer Klasse ist)

.. figure:: er3.png
	 :alt:    er3

Das Modellierungskonstrukt schwacher Entitäten ist in der Regel nur zum konstruieren schwacher Schlüssel, die pro starker Entität eindeutig sin notwendig (Bsp. Die Hausnummer ist pro Straße eindeutig).

.. figure:: er4.png
	 :alt:    er4

Nebenbedingungen (Constraints)
==============================

Schlüssel
---------
Menge von einem oder mehreren Attributen, deren Werte eine Entität eindeutig identifizieren.
Der Schlüssel sollte die **minimale Menge von Attributen** sein, für die gilt, das keine zwei Entitäten die gleichen Werte in allen Schlüsselattributen haben. Es kann mehr als einen Schlüssel für einen Entitätstyp geben, hier muss dann ein *Primärschlüssel angegeben werden*. Bei der Ist-Beziehung muss die Wurzel-Superklasse sämtliche Schlüsselattribute enthalten.

Referentielle Integrität
------------------------


.. figure:: referentielle_integritaet1.png
	 :alt:  referentielle_integritaet1

.. figure:: referentielle_integritaet.png
	 :alt: referentielle_integritaet

Existenz der referenzierten Entitäten

Allgemeine Nebenbedingungen (assertions)
----------------------------------------
Nebenbedingungen sind Teil des Schemas, sie sollen nicht aus den Daten abgeleitet werden. Häufig werden sie als Prädikatenlogische Ausdrücke formuliert.
Bsp. Der Chef darf nicht mehr als 150% des Durchschnittseinkommens der Mitarbeiter verdienen.
