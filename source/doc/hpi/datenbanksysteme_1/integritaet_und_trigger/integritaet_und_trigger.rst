======================
Integrität und Trigger
======================

Bei aktiven Datenbanken können Updates zu logischen und anderen Fehlern in den Daten führen. Dies könnte man zum einen durch das Schreiben besserer Anwendungen lösen, jedoch ist die Konsistenzprüfung, aufgrund der komplexen Bedingungen, abhängig von bereits bestehenden Daten, sehr schwer. Zum anderen könnten **Integritätsbedingungen** im DBMS spezifiziert und sichergestellt werden, dies ist in der Regel die bessere Wahl.

Schlüssel
=========
Ein oder mehrere Attribute bilden den Schlüssel, sodass sich irgendzwei Tuple der Relation in mindestens einem der Attributwerte der Schlüsselmenge unterscheiden müssen.
Diese Eigenschaft wird im ``CREATE TABLE`` spezifiziert
- **Primärschlüssel**: ``PRIMARYKEY`` (darf keinen Nullwert haben)
- **Schlüssel**: ``UNIQUE``

.. figure:: primarykey_bsp.png
	 :alt: Primary Key

.. figure:: unique_bsp.png
  :alt: Unique

Die Schlüsselbedingungen müssen immer gelten, können aber nur bei ``INSERT`` und ``UPDATE`` verletzt werden, nicht bei ``DELETE``. Zur effizienten Prüfung der Eigenschaft legen DBMS meist automatisch **Indizes für Primärschlüssel** an. Optional kann dies auch für ``UNIQUE`` gemacht werden: ``CREATE UNIQUE INDEX JahrIndex ON Filme(Jahr);`` Beim EInfügen und Ändern wird nun überprüft ob der Schlüsselwert bereits vorhanden ist. Ist kein Index vorhanden ist die Prüfung sehr ineffizient (binäre Suche auf sortierten Daten, sequentielle auf unsortierten)

Fremdschlüssel
==============
Ein Fremdschlüssel, ist der Schlüssel einer anderen Relation um auf diese zu verweisen.
Ein 
Bei Fremdschlüsseln möchte man **Referentielle Integrität** gewährleisten.
