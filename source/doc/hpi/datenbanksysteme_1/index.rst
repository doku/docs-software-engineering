Datenbanken
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   einfuerung/*
   historie/*
   relationaler_datenbankentwurf/*
   entity_relationship_modelling/*
   normalisierung/*
   relationale_algebra/*
   sql/*
   integritaet_und_trigger/*
   transaktions/*
   anfrageoptimierung/*
