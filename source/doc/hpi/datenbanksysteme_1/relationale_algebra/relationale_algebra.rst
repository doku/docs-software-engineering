===================
Relationale Algebra
===================

Kriterien einer Anfragesprache
==============================

- **Ad-Hoc-Formulierung**: Anfragen sollen ohne "vollständiges Programm" gestellt werden können
- **Eingeschränktheit**: keine vollständige Programmiersprache
- **Deskriptiv/Deklarativ**: Nutzer formuliert was er haben will, nicht wie dies berechnet werden kann
- **Optimierbar**: Sprache besteht aus wenigen Operationen, für die es Optmierungsregeln gibt
- **Effizienz**: Jede Operation ist effizient ausführbar(im relationalen Modell hat jede Operation eine Komplexität von  :math:`<= O(n^2)` mit n als Anzahl der Tuple)
- **Abgeschlossenheit**

// TODO Folie 5-6

Anfragealgebra
==============
Die Anfragealgebra definiert **Operationen auf Relationen** und ist so die Anfragesprache der relationalen Modells.

Mengen vs. Multimengen
========================
Multimengen sind Mengen, die Duplikate erlauben. Datenbanktabellen sind Multimengen von Tupeln. Die Operationen auf Multimengen sind effizienter(Bsp. Vereinigung Multimengen: :math:` O(m+n)`, Mengen :math:` O(m*n)` )

Operatoren
==========

- Mengenoperatoren: **Vereinigung, Schnittmenge, Differenz**
- Entfernende Operatoren: **Selektion, Projektion**
- Kombinierende Operatoren: **Kartesisches Produkt, Join, Joinvarianten**
- Umbenennung: Verändert nicht die Tuple, sondern das Schema

.. figure:: operatoren.png
	 :alt: operatoren

Vereinigung
-----------
Attributmenge der beiden Relationen muss identisch sein(d.h. Schemata müssen gleich sein). Als Mengenoperation entfernt die Vereinigung Duplikate, wenn diese in beiden Relationen vorhanden sind.

Outer Union
###########

Ermöglicht eine Vereinigung auch bei inkompatiblen Schemata. Das resultierende Schema entspricht der Vereinigung der Attributmengen und fehlende Werte werden mit Nullwerten ergänzt.

.. figure:: outer_union.png
	 :alt: outer_union


Differenz
---------
Schemata müssen gleich sein.

Schnittmenge
------------
Schemata müssen gleich sein und liefert die Tuple zurück, die in beiden Relationen gemeinsam vorkommen.

Projektion
----------
Erzeugt eine neue Relation mit einer Teilmenge der Attribute :math:`\pi_{L}(R)`. Dabei muss beachtet werden, dass durch das entfernen von Attributen Duplikate entstehen können, welche dann implizit entfernt werden. Der Ausdruck :math:`L` kann hierbei Attribute Umbenennen und neue Attribute aus Konstanten erstellen.

Selection
---------
Ist ein unärer Operator und erzeugt eine neue Relation, die Teilmenge der alten ist und das gleiche Schema hat. Nur Tuple die die Selektions-Condition erfüllen werden in die neue Relation aufgenommen. Als Operanten der Selektionsbedingung stehen nur Konstanten oder Attribute der Relation zur Verfügung.

Kartesisches Produkt
--------------------
Entspricht dem Kreuzprodukt zweier Relationen und kombiniert jedes Tuple aus der einen Relation mit jedem Tuple aus der anderen. Bei Namensgeleichheit wird kein Attribut weggelassen, stattdessen findet eine Umbenennung statt.

Join
----
Statt wie im Kreuzprodukt sollen nicht alle Paare von Tupeln gebildet werden, sondern nur solche, die in bestimmten Eigenschaften übereinstimmen.


.. figure:: join1.png
	 :alt: join1


.. figure:: join2.png
	 :alt:    join2


.. figure:: join3.png
	 :alt:    join3


.. figure:: join4.png
	 :alt:    join4

Unterschiedliche Join Varianten
###############################

.. figure:: join_varianten.png
	 :alt: join_varianten

Theta-Join
##########

.. figure:: join5.png
	 :alt: join5

Ist eine Verallgemeinerung des Joins, in der die Verknüpfungsbedingung selbst bestimmt werden kann. Das Ergebnis wird durch eine Selektion mit der Verknüpfungsbedingung aus dem Kreuzprodukt der beiden Relationen ermittelt.

Semi-Join
#########

.. figure:: semi_join.png
	 :alt: semi_join

Outer-Join
##########

Der Outer-Join übernimmt auch partnerlose Tuple der beiden Relationen und füllt leere Stellen mit Nullwerten.

.. figure:: outer_join.png
	 :alt: outer_join


Umbenennung
--------------

Durch Umbenennung der Attribute können Schemata angeglichen werden. Dies ist oft bei Operationen(alle Mengenoperationen, Joins) nötig, die gleiche Schemata voraussetzen.

.. figure:: umbennung.png
	 :alt: umbennung

.. figure:: Umbenennung_bsp.png
	 :alt: 	 umbenennung_bsp

Duplikat Eliminierung
---------------------
Wandelt eine Multimenge durch löschen aller Duplikate in eine Menge um. Die Duplikat Eliminierung ist streng genommen unnötig, da die Mengensemantik der relationalen Algebra dies bereits implizit macht.

Aggregation
-----------
Aggregationen fassen die Werte eine Spalte zusammen. Sie ist eine **Operation auf einer Menge oder Multimenge von atomaren Werten nicht Tupeln**  Dabei gehen Nullwerte in der Regel nicht ein. Es gibt eine Vielzahl von Operationen:

	- SUM
	- AVG (gibt auch STDDEV, VARIANCE)
	- MIN / MAX
	- COUNT : doppelte Werte gehen doppelt ein, Nullwerte werden in der Regel mitgezählt; Angewandt auf ein beliebiges Attribut einer Relation gibt es die Anzahl der Tuple zurück

Gruppierung
-----------
Die Gruppierung liefert eine Partitionierung der Tuple einer Relation gemäß ihrer Werte in einem oder mehreren Attributen. Sie wird hauptsächlich zur Aggregation auf Teilen einer Relation verwendet.

.. figure:: gruppierung.png
	 :alt: gruppierung

.. figure:: gruppierung_bsp1.png
	 :alt: gruppierung_bsp1

.. figure:: gruppierung_bsp2.png
	 :alt: 	 Gruppierung_bsp2

Sortierung
-----------

.. figure:: sortierung.png
	 :alt: sortierung

Kardinalitäten
--------------
Sei R Relation mit m Tupeln, S Relation mit n Tupeln. Die minimale und maximale Anzahl von Tupeln in den folgenden Ausdrücken, sind wie folgt.

.. figure:: kardinalitaeten.png
	 :alt: kardinalitaeten

Division
--------
.. figure:: division.png
	 :alt: division

Typischerweise wird die Division nicht als primitiver Operator unterstützt, sondern kann wie folgt berechnet werden

.. figure:: division_formel.png
	 :alt: division_formel

Nullwerte
----------
generell sind Abfragen wie `` = NULL `` nicht möglich, diese müssen über die Differenz bestimmt werden

Unabhängigkeit und Vollständigkeit
==================================

.. figure:: minimale_algebra.png
	 :alt: minimale_algebra

Optimierung
===========

.. figure:: optimierung.png
	 :alt: optimierung

Komplexe Ausdrücke
==================

Durch Kombination oder Schachtelung der Ausdrücke können komplexe Anfrage formuliert werden. Da die relationale Algebra abgeschlossen ist, ist der Output eines Ausdruckes immer einer Relation, die weiterverarbeitet werden kann. Der komplexe Ausdruck kann entweder mittels Klammerung oder über eine Baumstruktur dargestellt werden.

.. figure:: relationale_algebra1.png
	 :alt: relationale_algebra1

Multimengen Operationen
=======================

- Vereinigung: Relationen direkt aneinander hängen, Tuple t erscheint n mal in R und m mal in S, nach Vereinigung n+m
- Schnittmenge: Tuple t erscheint n mal in R und m mal in S, nach Schnitt min(n,m)
- Differenz: Tuple t erscheint n mal in R und m mal in S, jedes Vorkommen von t in S eliminiert ein t in R
- Projektion: Attributwerte einfach abschneiden, dabei können neue Duplikate entstehen
- Selektion: wird für jedes Tuple einzeln angewendet, Duplikate bleiben erhalten
- Kreuzprodukt: Tuple t erscheint n mal in R und Tuple u erscheint m mal in S, im Kreuzprodukt erscheint das Tuple tu n*m mal

Joins auf Multimengen
---------------------

.. figure:: joins_multimengen.png
	 :alt: joins_multimengen
