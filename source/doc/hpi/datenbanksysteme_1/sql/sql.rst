==========
SQL
==========

Historie
========

.. figure:: standardisierung.png
	 :alt: standardisierung

Motivation
==========

Erfüllt Anforderungen an eine relationale Anfrage Sprache.

- Ad-hock und einfach
- Deklarativ und optimierbar
- an relationale Algebra angelehnt (Hinzu kommen DDL (Data definition language), DML (Data manipulation language))

.. hint:: Die SQL Syntax und Funktionalität kann sich von System zu System unterscheiden.

Basics
======

.. figure:: basics.png
	 :alt:  basics

.. note::

  - Groß und Kleinschreibung wird in SQL nicht beachtet
  - Per Konvention sollen Schlüsselworte "all -caps" geschrieben werden, Schemaelemente klein
  - Bei Konstanten spielt die Groß und Kleinschreibung natürlich eine Rolle

Projektion in SQL: SELECT
-------------------------

.. figure:: select.png
	 :alt: select

.. hint:: ``SELECT`` entspricht der Projektion **nicht der Selection**

Selection in SQL: WHERE
-----------------------

Die Selection wird in der WHERE Klausel spezifiziert und entspricht einer Bedingung in anderen Programmiersprachen. Sie liefert immer einen Boolschen Wert zurück und wird für jedes Tuple evaluiert.
Ist dieser ``TRUE`` erscheint das entsprechende Tuple im Ergebnis. Wird die ``WHERE`` Klausel zu ``UNKNOWN`` evaluiert, erscheint das Tuple nicht im Ergebnis. Aussagen können mit ``AND``, ``OR`` oder ``NOT`` verknüpft werden, Klammerung ist erlaubt. Es können Konstanten, Attributnamen (auch solche, die nicht im SELECT vorkommen) und Arithmetische Ausdrücke für numerische Attribute verwendet werden.

.. figure:: where_bsp.png
	 :alt: where_bsp

Stringvergleiche
----------------

Es gibt mehrere String Datentypen: Arrays fester Länge, Buchstabenlisten variabler Länge und Konstanten.
SQL erlaubt viele Vergleiche über Datentypen hinweg ``foo_ _ _ _ _ = foo = 'foo'``

**Lexikographische Vergleiche** ``<,>,<=,>=``, Sortierreihenfolge upper-case/lower-case hängt vom DBMS ab

.. hint:: Ungleichheit kann durch ``<>`` überprüft werden

.. figure:: vergleiche_bsp.png
	 :alt: vergleiche_bsp

Mit ``LIKE`` bzw. ``NOT LIKE`` können String pattern verglichen werden. Mit ``%`` kann eine Sequenz von 0 bis beliebig vielen Zeichen dargestellt werden, ``_`` steht für ein beliebiges Zeichen. String Vergleiche mit ``%`` sind jedoch **sehr teuer** da viele Kombinationen überprüft werden müssen.

Strings können mit ``||`` verknüpft werden: ```Star` || `Wars``` entspricht ````StarWars```. Dies is auch mit Attributen möglich ```Vorname` || ` ` || `Nachname``` entspricht für ein bestimmtes Tuple ```Max Mustermann```

Datum und Uhrzeit
-----------------
Es gibt spezielle Datentypen für Zeitrepräsentation:

- Datumskonstante: ``DATE `YYYY-MM-DD``` Bsp. ``DATE `1790-11-21```
- Zeitkonstante: ``TIME `HH:MM:SS.S` Bsp. ``TIME `08:29:21.1```
- Zeitstempel: ``TIMESTAMP `YYYY-MM-DD HH:MM:SS.S```
- Zeitvergleiche: Vergleiche sind innerhalb eines Datentypen möglich ``TIME `08:29:21.1`  <  TIME `18:29:21.1`` liefert true zurück

Nullwerte
-----------

Mögliche Interpretationen: Unbekannter Wert, unzulässiger Wert, unterdrückter/geheimer Wert

Regeln für den Umgang mit Nullwerten

  - **Arithmetische Operationen** mit ``NULL`` ergeben ``NULL``
  - **Vergleiche** mit ``NULL`` ergeben ``UNKNOWN``
  - ``NULL`` ist keine Konstante sondern erscheint nur als Attributwert
  - **logische Operationen** mit ``NULL``: Für ``AND``, ``OR``, ``NOT`` gilt folgende Regel: Sei ``TRUE = 1``, ``FALSE = 0``, ``UNKNOWN = 1/2`` dann folgt ``AND`` ist das Minimum der beiden Werte, ``OR`` ist das Maximum der beiden Werte, ``NOT`` entspricht :math:`1 - Wert`
  - Wird die ``WHERE`` Klausel zu ``UNKNOWN`` evaluiert, erscheint das Tuple nicht im Ergebnis

Sortierung
----------

``ORDER BY <Attributlist> DESC/ASC`` (``ASC`` ist default und kann weggelassen werden)

.. figure:: sortierung_bsp.png
	 :alt: sortierung_bsp

FROM
-------

``FROM`` ermöglicht es Anfragen über mehrere Relationen zu stellen, in dem alle beteiligten Relationen einer Anfrage in der ``FROM`` Klausel genannt werden.

.. figure:: from_bsp.png
	 :alt: from_bsp

Uneindeutige Attributnamen
--------------------------
Bei gleichen Attributnamen aus mehreren beteiligten Relationen, muss der Relationsname als Präfix vorangestellt werden. Dies ist auch sonst erlaubt, um die Lesbarkeit der Anfragen zu verbessern.

.. figure:: praefix_bsp.png
	 :alt: praefix_bsp

Tupelvariablen (Alias)
----------------------
Relationen kann eine eindeutige Kennzeichnung gegeben werden, was insbesondere bei mehrfacher Verwendung einer Relation in einer Anfrage notwendig ist. Ohne explizites Angeben der Tupelvariablen wird der Relationsname implizit als Tupelvariable verwendet.

.. figure:: alias_bsp.png
	 :alt: alias_bsp

Joins
-----

.. figure:: joins_sql.png
	 :alt: joins_sql

Kreuzprodukt ``CROSS JOIN``
###########################

``Film CROSS JOIN spielt_in``
Doppelte Attributnamen werden mit Präfix der Relation aufgelöst.
Die beiden Varianten sind äquivalent:

.. figure:: kreuzprodukt.png
	 :alt: kreuzprodukt

Theta-Join ``JOIN ... ON ...``
##############################

.. figure:: theta_join_bsp.png
	 :alt: theta_join_bsp

Natural Join ``NATURAL JOIN``
#############################

Bsp.: ``Schauspieler NATURAL JOIN MANAGER`` redundante Attribute werden eliminiert

Outer Join
##########

.. figure:: outer_joins.png
	 :alt: outer_joins



CREATE TABLE
------------
.. code-block:: sql

	CREATE TABLE table_name (
	    column1 datatype,
	    column2 datatype,
	    column3 datatype,
	   ....
	);

The possible datatypes differ form DBMS to DBMS. Check out this guide for further information https://www.w3schools.com/sql/sql_datatypes.asp

Interpretation von Anfragen
===========================

Anfragen mit mehreren Relationen können auf drei verschiedene Arten interpretiert werden.

- Nested Loops(geschachtelte Schleifen): Bei mehreren Relationen/Tuplevariablen gibt es je eine Schleife pro Relation
- Parallele Zuordnung: Alle Kombinationen werden parallel bezüglich der Bedingung geprüft
- Relationale Algebra: Bilde Kreuzprodukt und wende Selektionsbedingung auf jedes Resultat-Tuple an.

.. figure:: interpretation_anfragen_bsp.png
	 :alt: interpretation_anfragen_bsp
