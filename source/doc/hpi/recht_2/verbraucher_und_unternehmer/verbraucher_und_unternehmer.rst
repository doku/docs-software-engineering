Verbraucher und Unternehmer
===========================

Verbraucher §13 BGB
-------------------

**natürliche Personen**, die ein Rechtsgeschäft zu Zwecken abschließen, die
überwiegend weder ihrer gewerblichen noch ihrer selbstständigen beruflichen
Tätigkeit zugerechnet werden können.

Unternehmen §14 BGB
-------------------

- Eine natürliche oder juristische Person(unnatürliche aber formalisierte Personen, von Rechtswegen in vielerlei Hinsicht wie natürliche Person gestellt) oder eine rechtsfähige Personengesellschaft, die bei Abschluss eines Rechtsgeschäftes in Ausübung ihrer gewerblichen oder selbstständigen beruflichen Tätigkeit handelt.
- Eine rechtsfähige Personengesellschaft ist eine Personengesellschaft, die mit der Fähigkeit ausgestattet ist, Rechte u erwerben und Verbindlichkeiten einzugehen


Unterscheidung:

  - natürliche Personen
  - Rechtsfähige Personengesellschaften
  - juristische Personen
  - Kaufleute

Bsp. Peugeut handelt sich nicht um ein physisches Objekt sondern eine juristische Person.
Bleibt ohne jede Sache(Arbeiter, Fabriken etc.) bestehen, kann jedoch zerschlagen
werden durch Gericht obwohl alle Sachen Bestehen bleiben.

Gesellschaften
--------------

Eine Personengesellschaft ist ein Zusammenschluss von mindestens 2 Personen(natürliche oder juristische), die sich zusammengeschlossen haben um einen gemeinsamen bestimmten Zweck(relativ beliebig) nachzugehen


Gesellschaft/Unternehmen hat einen Unternehmensbetrieb
Der Unternehmensbetrieb wird unternommen in einer bestimmten Rechtsform
?Firma eines Kaufmannes ist der Name unter dem er seine Geschäfte betreibt

Jedes Unternehmen, das nicht eine reine BGB Gesellschaft ist, ist Kaufmann und hat demnach eine Firma. HGB §17
Firma hat vor und Nachname. Nachname ist Rechtsform andeutender Zusatz. Bsp. Apple AG

Unternehmenstragende Gesellschaften


Teilung in innen und außen Gesellschaft

interessant im außen Verhältnis


Gesellschaft Rechtsformen
-------------------------

Es gibt unterschiedliche Gesellschaftsformen, aber nur zwei generelle typen

Körperschaften: Personenunabhängig

- GmbH: Gesellschaft mit beschränkter Haftung
- AG. :Aktiengesellschaft
- Gen : Genossenschaften
- e.V. : eingetragener Verein

Kapitalgesellschaften sind Teilmenge der Körperschaften. Körperschaften sind juristische Personen

Personengesellschaft: Verbund von min. 2 Personen die sich gemeinsam zur Erreichung eines Zieles unterstützen

- GbR/BGB Gesellschaft : BGB §705ff (bsp. Lerngruppe, )
- OHG : HGB §105 sind Personenhandelsgesellschaffen
- KG : Kommandit gesellschaftHGB §161 sind Personenhandelsgesellschaffen
- stilleG: Stille Gesellschaft
- PartG: Partnerschaftsgesellschaft
- PartG: Partnerschaftsgesellschaft



Nur die Gesellschaftsrechtsformen, die vom Gesetzgeber gegeben werden dürfen verwendet werden

Wie entsteht eine Personengesellschaft vs
Da die PG durch die Personen manifestiert ist, ist es viel einfacher diese zu erzeugen. Eine BGB Gesellschaft entsteht mit zwei Personen und einem Zweck. Für Körperschaften muss das abstrakte Konstrukt erst erzeugt werden.
PG entstehen

Rechtsfähige Personengesellschaften: GBR, OHG, KG
Diese werden durch Verleihung §124 HGB Rechtfähig.



GBR / BGB Gesellschaft
______________________

Wann ist sie ein Rechtssubjekt ? Kann sie also Unternehmer am Rechtsverkehr teilnehmen?
Wird

Wann ist eine BGB Gesellschaft ein Rechtsubjekt?

rechtsfähig sind natürliche und juristische Personen.

OHG
___
§124 wichtig für die Klausur, sind keine Juristischen Personen, werden gleich gestellt und erhalten Rechtsubjektivität.

Rechtssubjekt, darf verklagt werden
GBR ist Grundform der OHG
jeder einzelne Gesellschafter haftet für die Verbindlichkeiten der Gesellschaft.§128, §124
die
jeder einzelne persönlich Haftende Gesellschafter kann Gesellschaft vertreten

typische Familiengesellschaftsformen

KG
___

KG ist Spezialform von OHG und damit transitiv GBR

ist dann KG, wenn die Haftung gegeben
§161
es gibt immer mindestens einen persönlich haftenden Gesellschafter vie bei OHG
es gibt immer mindestens einen beschränkt haftenden Gesellschafter, geben Haftungslimit in der Handelskammerbuch an

häufig Gesellschaften mit einem persölichhaftenden und vielen beteiligen, bsp. Haus kaufen
Gleiche wie eine OHG nur gibt es hier noch Kommanditisten
Einbringen von Einlage, kann Geld, aktien, patente ( wert wird zum Stichtag bestimmt)
Gesellschaft hat aus Vertrag recht auf Leistung der Einlage
Eingebracht wird zu einem Vereinbarten Zeitpunkt


Kalusurfrage: Wie haftet der Komanditist? Beschränkt in höhe der noch nicht geleisteten Einlagen
Personengesellschaften
______________________

Hier haften die Gesellschaften
Gesellschaft wird von Gesellschafter vertreten
Vertrag mit einer Gesellschaft schließen: mit Mindestens einem persönlich haftenden Gesellschafter, bei BGBG alle


//TODO
innen und außengesellschaften

erst wenn die leute nach außen hin in erscheinung treten weird der gbgg nach außen hin in erscheinun treten, haften die gesellschafter

wenn eine GBR nach außen hin auftritt wie eine OHB ist sie zu behandeln wie eine OHG

§128

Personenhandelsgesellschaffen
_____________________________

Zweck nach außen ist Geld zu verdienen

Aufbauschema Verbrauchsgüterkauf
---------------------------------

 1. Gewährleistungsrecht standen


Gewährleistungsrechte


  Sachmangel §434




  §437 Recht des Käufers bei Mangel: Nacherfüllung, Rücktritt, Minderung, Wandlung,

Fallbeispiel

Grundlegende Frage: Kann V von K die Bezahlung des Porsches verlangen
  + Ist der Fall wenn ein KV zustande gekommen ist.
  + Durch Anmeldung des Porsches ist es bereits zum Gefahrenübergang gekommen. Eigentumsübergang ist
  + Da der Porsche überwiegend privat genutzt wird liegt die Konstellation Verbraucher - Unternehmer vor

Abwandlung
  Verkauf von K an S ohne Gewährleistung
  + Rechtsgeschäft zwischen Verbraucher und Unternehmer ? Gesetz ist dazu gedacht Ungleichgewicht zwischen Professionellem Unternehmer mit rechtsgeschäftspezifischem Fachwissen und unwissenden Verbraucher
  + nach §475 kann K als Unternehmerin als S nicht ohne Gewährleistung verkaufen (Rechte des Käufers dürfen nicht nachträglich beeinträchtigt werden)
  + Mangel liegt nur dann vor, wenn er bei Gefahrübergang bestand. Nach §476 innerhalb 6 Monate Beweislastumkehr

insolvent
---------
zahlungsunfähig, bzw. droht zahlungsunfähig zu werden
gang zum Amtsgericht ist verpflichtend sonst anzeige wegen Insolvenzverschleppung
Gründe für isolvenz
-zahlungsunfähig
- droht zahlungsunfähig zu werden
- überschuldung


Paragraphensammlung
-------------------

BGB
___
§§

HGB
___
§17 Firma
§124 Rechtfähig durch Verleihung
§125
§126
§127
§128
§160 ausgetetener haftet 5 Jahre nach austreten
§161
§171 Haftungslimit Kommanditisten
