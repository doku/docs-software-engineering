===============
Fallbearbeitung
===============

Methoden
--------

1. Verständnis des Sachverhalts
2. Erfassen der Fallfrage
3. Finden der richtigen Anspruchsgrundlagen

  * Anspruchsarten: Recht auf ein Tun oder Unterlassen (§194 I)
  * Anspruchsgrundlage: eine Norm, die eine Person dazu berechtigt ein Tun oder Unterlassen zu fordern (Bsp.: § 433 I), In Betracht kommen aber nur solche Anspruchsgrundlagen, die genau das gewähren, was dem Begehrten in der Fallfrage entspricht. Das von einer Anspruchsgrundlage Gewährte nennt man **Rechtsfolge**.

4. Subsumtion: Deckung zwischen Fall und Anspruch
   Jede Anspruchsgrundlage hat Voraussetzungen, den sog. Tatbestand. Der Sachverhalt wird auf Erfüllung der Voraussetzungen untersucht, d.h. der konkrete Lebenssachverhalt wird mit der abstrakten Gesetzesnorm verglichen(eigentliche Hauptaufgabe von Juristen)


Aufbau einer Falllösung
-----------------------

1. **Fragesatz**
2. **Voraussetzungssatz**
3. **Definitionssatz**
4. **Subsumtionssatz**
5. **Folgesatz**

Beispiel: A kauft bei B ein Auto für 900€. Welche Ansprüche hat A gegen B und welche hat B gegen A?

1. **Fragesatz**: B könnte gegen A den Anspruch auf Zahlung des Kaufpreises nach §433 II haben.
2. **Voraussetzungssatz**: Dazu müsste ein kaufvertag zustande gekommen sein.
3. **Definitionssatz**: Ein Kaufvertrag kommt bei zwei kongruenten Willenserklärungen(Angebot und Annahme) zustande.
4. **Subsumtionssatz**: Ist hier ein Kaufvertrag zustande gekommen?
5. **Folgesatz**: B hat (k)einen Anspruch auf Zahlung des Kaufpreises gegen A aus § 433 II.

Jede Falllösung beginnt mit der Frage:

.. attention:: **Wer will was von wem woraus?**

Wer/von wem?
~~~~~~~~~~~~
Wer: Gläubiger
Wem: Schuldner

Hier ist die Frage, ob die Beteiligten Rechtssubjekte sind, also Rechtsfähigkeit besitzen, d.h. Träger von Rechten und Pflichten sein zu können:

  - Natürliche Personen sind alle Menschen. Ihre Rechtsfähigkeit beginnt mit der Vollendung der Geburt, § 1 BGB.
  - Juristische Personen sind die von der Rechtsordnung als selbständige Rechtsträger anerkannten Personenvereinigungen oder Vermögensmassen.
  - Personengesellschaften ist ein Zusammenschluss mehrerer Personen zur Verfolgung eines gemeinsamen Zwecks.

Was
~~~~~~~~~~

Die Frage nach dem „was" ist die Frage nach dem Anspruchsinhalt, der für das Auffinden der
richtigen Anspruchsgrundlage entscheidend ist. Der Inhalt des Anspruchs richtet sich danach, was von dem Anspruchsteller begehrt wird.

Diese Begehren müssen jedoch juristisch korrekt benannt werden. A möchte das Eigentum und
den Besitz am Auto erlangen. B möchte Eigentümer und Besitzer des Geldes werden bzw. im
Falle der Überweisung einen Auszahlungsanspruch gegenüber der Bank.

Woraus
~~~~~~~~~~

Hier wird nach den Anspruchsgrundlagen gesucht. Welche Anspruchsgrundlage geeignet ist, hängt davon ab, ob die in der betreffenden Norm angeordnete Rechtsfolge der Richtung der Fallfrage entspricht.
Wenn z.B. nach Schadensersatz gefragt ist, kommen als Anspruchsgrundlage von vornherein nur
solche in Betracht, die auch tatsächlich Schadensersatz gewähren.
Ob die so gefundene Anspruchsgrundlage dann tatsächlich einschlägig ist, hängt davon ab, ob der Sachverhalt unter die Tatbestandsvoraussetzungen der Norm subsumiert werden kann.
So scheiden nach und nach diejenigen Anspruchsgrundlagen aus, die zwar von der Rechtsfolge her passen, deren Voraussetzungen aber nicht vorliegen.




Prüfung eines Einzelanspruchs
_____________________________

ist ein Anspruch entstanden?
  keine rechtshindernden Einwenden
ist der Anspruch erloschen?
  keine rechtsvernichtenden Einwenden
ist der Anspruch durchsetzbar? (Bsp. bei Insolvenz)
  keine rechtshemmenden Einreden

im Gerichtsverfahren muss ein Anspruch geltend gemacht werden und Verjährung angesprochen werden. Der Richter darf nur beachten, was im Prozess angesprochen wurde.

Entstehen eines Anspruches
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: rechtsgeschaefte.png
	 :alt:    Rechtsgeschäfte




Trennungs und Abstraktionsprinzip
---------------------------------

In § 433 l 1 heißt es, dass der „Verkäufer (lediglich) verpflichtet ist, dem Käufer die Sache zu
übergeben und das Eigentum an der Sache zu verschaffen". Daraus ist zu schließen, dass der
Käufer nicht allein durch den Kaufvertrag Eigentümer werden kann. Der Kaufvertrag begründet
lediglich die Verpflichtung für den Verkäufer, das Eigentum auf den Käufer zu übertragen.
Er ist das sog. Verpflichtungsgeschäft (schuldrechtlicher Vertrag).
Es stellt sich dann aber die Frage, wie A tatsächlich Eigentümer des Autos werden kann.
Dazu muss B seine Verpflichtung erfüllen. B muss das Eigentum auf A übertragen, also über das
Eigentum verfügen.

Die Übertragung des Eigentums ist das sog. Verfügungsgeschäft (oder Erfüllungsgeschäft).
Die Einigung nach §929 S.1 BGB ist ebenso wie der Kaufvertrag ein Vertrag. Da er aber nicht auf
die Schaffung von Verpflichtungen, sondern auf die unmittelbare Rechtsänderung an einer Sache
(einem Ding) gerichtet ist, spricht man bei der Einigung von einem dinglichen Vertrag.
Das Trennungsprinzip besagt nun, dass Erfüllungs- und Verpflichtungsgeschäft streng
voneinander zu unterscheiden sind.


Begriffsdefinitionen
--------------------

**Besitz**: tatsächliche Gewalt über eine Sache, getragen von einem Herrschaftswillen (§ 854)
**Eigentum**: dahinterstehende, totale Recht über eine Sache zu Verfahren (§ 903)

**Vertragsschluss**: Einigung durch Angebot/Annahme §145ff. -> zwei kongruente Willenserklärungen
  - Äußerer Erklärungsbestand(objektiv)
  - innerer Erklärungsbestand (subjektiv): Handlungswille (darf nicht erzwungen sein), Erklärungsbewusstsein/ Rechtsbindungswille

.. figure:: vertragsschluss.png
	 :alt:  vertragsschluss
