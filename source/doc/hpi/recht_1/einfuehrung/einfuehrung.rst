==========
Einführung
==========

Recht bezeichnet die Gesamtheit gesellschaftlich institutionalisierter, genereller Rechtsnormen, also Regeln mit allgemeinem Geltungsanspruch. Diese Regeln entstehen entweder als Gewohnheitsrecht durch andauernde Anwendung von Rechtsvorstellungen oder Regeln, die von den Beteiligten als verbindlich akzeptiert werden, oder als (gesetztes) positives Recht, das von staatlichen oder überstaatlichen Gesetzgebungsorganen oder von satzungsgebenden Körperschaften geschaffen wurde.

.. figure:: rechtsordnung.png
	 :alt: Rechtsordnung

Gerichtsarten
-------------

Die Rechtsprechung, und damit die Interpretation der Gesetzestexte wird von Gerichten durchgeführt, welche in mehrere Stufen unterteilt sind, welche je nach Art und Bedeutung des Falles zum Einsatz kommen.

1. **Bundesverfassungsgericht**: bei Verletzungen der Grundrechte
2. **Bundesgerichtshof**: oberstes Zivilrechtgericht, oberstes Landesgericht
3. **Oberlandesgericht**: 24 Stück, in Berlin Kammer Gericht genannt
4. **Langerichte**: 115 Stück, Kammern mit drei Richtern
5. **Amtsgericht**: lokale Gerichte, unterste Instanz, ein Richter, 639 Stück

.. figure:: gerichtsZustaendigkeiten1.png
	 :alt: Gerichts Zustaendigkeiten1

.. figure:: gerichtsZustaendigkeiten2.png
	 :alt: Gerichts Zustaendigkeiten2

In der Regel beginnen Prozesse bei den unteren Instanzen und gehen bei Berufung zur nächst höheren Instanz.
Der BGH entscheidet dann über komplexe Fragestellungen und seine Rechtsprechungen haben eine hohe Bedeutung, sie geben einen Orientierungsrahmen für ähnliche Fälle vor. Dabei ist der BGH niemals erste Instanz, sondern in der Regel die 3. Instanz.


BGB - Bürgerliches Gesetzbuch
-----------------------------

Das BGB ist eine Zusammenfassung von Rechtsnormen, die die Rechtsbeziehungen der Bürger untereinander regelt.

Das BGB ist in fünf Bücher aufgeteilt: Allgemeiner Teil, Schuldrecht, Sachenrecht, Familienrecht und Erbrecht.
Der Allgemeine Teil enthält Normen, die, wie der Name schon sagt, für alle anderen Bücher ebenfalls gelten.
Es gibt aber in den anderen Büchern Spezialregelungen, die der jeweiligen Materie besser gerecht werden. Dann müssen die Regeln des Allgemeinen Teils hinter diesen zurückstehen.
Es gilt der Grundsatz: **Die speziellere Norm verdrängt die allgemeinere / sog. Spezialitätsgrundsatz**.

.. figure:: bGB.png
	 :alt: BGB
