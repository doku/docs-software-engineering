===============
Cloud Platforms
===============

.. toctree::
  :maxdepth: 2
  :glob:

  cloud_native_apps/*
  kubernetes/*
  kubernetes_vs_serverless/*
  aws/*
