.. _ref-kubernetes:

==========
Kubernetes
==========

Kubernetes is a platform for automating deployment, scaling and operations of
application containers across clusters of hosts. Kubernetes comes from Google
and is the open source offering of Borg the in house system they've used to run
systems over the last decade. This uses a model based approach wherein you specify
the application and the specification for the desired number of instances available.
Kubernetes reads in the model and creates the system to that spec.

A pod is one or more containers and pods are the smallest deployable units of
computing that can be created and managed in Kubernetes

The deployment controller changes the actual to the desired state at a controlled
rate for you.

- ``containers`` run programs
- ``pods`` group containers together
- ``services`` make pods available to others
- ``lables`` are used for very advanced service discovery

Kubernetes uses the ``kubectl`` command for scripting large operations and gives
you very flexible overlay networking.

Platform
========

A platform abstracts away a messy problem so you can build on top of it. For example
Linux and Java are platforms. Platforms enable portability across a wide variety
of environments and interfaces and provide stable APIs to enable extensibility.

Cluster
=======

A kubernetes cluster consists of a **master** which coordinates the cluster and
**nodes** which are the workers that run applications.

.. figure:: clusterdiagramm.png
	 :alt: clusterdiagramm

The master is responsible for managing the cluster. The master coordinates all
activities in your cluster, such as scheduling applications, maintaining
applications' desired state, scaling applications, and rolling out new updates.

A node is a VM or physical computer that serves as a worker machine in a Kubernetes
cluster. Each node has a Kubelet, which is an agent for managing the node and
communicating with the Kubernetes master.

Kubernetes Deployment
=====================

One you have a running Kubernetes cluster, you can deploy containerized applications
on top auf it using a Kubernetes **Deployment**. The Deployment is responsible
for creating and updating instances of your application. Once you have created a
Deployment, the Kubernetes master schedules the application instances that the
Deployment creates onto individual Nodes in the cluster.

.. figure:: deployment_structure.png
	 :alt: deployment_structure

Kubernetes Pods
===============

A Pod is a Kubernetes abstraction that represents a group of one or more application
containers (such as Docker or rkt), and some shared resources for those containers.

The containers share storage, as volumes, networking (as a unique cluster IP address)
and basic configuration information (such as image version and specific ports to use).

A Pod models an application-specific *local host* and can contain different application
containers which are relatively tightly coupled.

The containers in a pod share an IP Address and port space are always co-located and
co-scheduled, and run in a shared context on the same Node.

Pods are the atomic unit on the Kubernetes platform. When we create a Deployment
on Kubernetes, that Deployment creates Pods with the containers inside them (the
containers are not created directly).

Each Pod is tied to the Node where it is scheduled and remains there until
termination (according to restart policy) or deletion. In case of a nNode failure,
identical Pods are schedules on other available Nodes in the cluster.

.. figure:: pods_overview.png
	 :alt: pods_overview

Kubernetes Pods are mortal. Each Pod in a Kubernetes Cluster has a unique IP address,
even Pods on the same Node.

Pods can be creates using a **pod definition** file.

.. code-block:: YAML

	apiVersion: v1
	kind: Pod
	metadata:
	  name: nginx
	spec:
	  containers:
	  - name: nginx
	    image: nginx:1.7.9
	    ports:
	    - containerPort: 80

A pod definition is a declaration of a **desired state**.
The **desired state** is presented to the system and it is Kubernetes' responsibility
to make sure that the current state matches the desired state.

.. code-block:: bash

	kubectl create -f /path/to/POD_DEFINITION_FILE.yaml

	kubectl delete pod nginx #delete pod by name

Volumes
-------
The containers file system only lives as long as the container does.
Volumes enable persistent storage.

The ``volumes`` have to be defined and then mounted in the pods definition.

Define a volume:

.. code-block:: yaml

	volumes:
		- name: redis-persistent-storage
		  emptyDir: {}

Define a volume mount within a container definition. The ``volumeMounts name`` is
a reference to a specific ``volumes name``

The ``volumeMounts mountPath`` is the path to mount the volume within the container.

.. code-block:: yaml

	volumeMounts:
		# name must match the volume name below
    - name: redis-persistent-storage
      # mount path within the container
      mountPath: /data/redis

In this example a helper job polls a git repository for new updates

.. code-block:: YAML
	:linenos:

	apiVersion: v1
	kind: Pod
	metadata:
	  name: www
	spec:
	  containers:
	  - name: nginx
	    image: nginx
	    volumeMounts:
	    - mountPath: /srv/www
	      name: www-data
	      readOnly: true
	  - name: git-monitor
	    image: kubernetes/git-monitor
	    env:
	    - name: GIT_REPO
	      value: http://github.com/some/repo.git
	    volumeMounts:
	    - mountPath: /data
	      name: www-data
	  volumes:
	  - name: www-data
	    emptyDir: {}

Controllers
===========

.. glossary::

	ReplicaSets
		makes sure the specified number of pods is available

	Deployments
		A Deployment controller provides declarative updates for pods and ReplicaSets

	DaemonSets
		DaemonSets ensure that all odes run a copy of a specific pod. As Nodes
		are added or removed from the cluster, a DaemonSet will add or remove the required
		pods.

	Service
		Allow the communication between one set of deployments with another, use a
		service to get pods in two deployments to talk to each other.

Multi-Tenancy
=============

.. glossary::

	Labels
		Labels are key-value pairs that are attached to objects like pods, services
		and deployments. Labels are for users of Kubernetes to identify attributes
		for objects.

	Selectors

	Namespaces
		Namespaces allow you to divide a cluster to multiple virtual sections

Nodes
=====

A Pod always runs on a **Node**, but one Node can have multiple Pods.
A Node is a worker machine Kubernetes ans may be
either a virtual or a physical machine, depending on the cluster. Each Node is
managed by the Master.

Every Kubernetes Nodes runs at least

- **Kubelet** which is a process responsible for communication between the Kubernetes
	Master and the Nodes, it manages the Pods and the containers running on the machine
- a container runtime (Docker, rkt) which is responsible for pulling the container
  image form a registry, unpacking it and running the application

.. figure:: node_overview.png
	 :alt: node_overview

Services
========
A Service in Kubernetes is an abstraction which defines an logical set of
Pods  and a policy by which to access them. Services enable a loose coupling
between dependent Pods. Services are the stable endpoint for pods.

A Service is defined using YAML (preferred) or JSON, like all Kubernetes objects.

The set of Pods targeted by a Serivce is usually determined by a ``LableSelector``

Although Pods each have a unique IP address, those IPs are not exposed outside the
cluster without a Service. Services allow your applications to receive traffic.
Services can be exposed in different ways by specifying a ``type`` in the
ServiceSpec.

.. figure:: services.png
	 :alt: services

A Service routes traffic across a set of Pods. Services are the abstraction that
allow pods to die and replicate in Kubernetes without impacting your application.

Discovery and routing among dependent Pods (such as frontend and backend components)
is handled by Kubernetes Services.

Services match a set of Pods using labels and selectors.

Labels are key/value pairs attached to objects.

.. figure:: services_lables.png
	 :alt: services_lables

Labels can be attached to objects at creation time or later on. They can be
modified at any time

To apply a new label we use the label command followed by the object type, object
name and the new label:

.. code-block:: bash

	kubectl describe pods $POD_NAME

	kubectl get pods -l app=v1 #querry for list of pods using a label

Scaling an application
======================
**Scaling** is accomplished by changing the number of replicas in a Deployment.

Scaling up a Deployment will ensure new Pods are created and scheduled to nodes
with available resources. Scaling down will reduce the number of Pods to the new
desired state. Kubernetes supports **autoscaling** Pods.

Services have an integrated load-balancer that will distribute network traffic
to all Pods of an exposed Deployment. Services will monitor continuously the running
Pods using endpoints, to ensure the traffic is sent to available Pods.

.. figure:: pod_scaling.png
	 :alt: pod_scaling

Updating an application
=======================

Rolling updates allow deployments to be updated with zero downtime by incrementally
updating Pods instances with new ones. The new Pods will be scheduled on Nodes with
available resources.

In Kubernetes, updates are versioned and any Deployment update can be reverted to
a previous (stable) version.

In a Deployment is exposed publicly, the Service will load-balance the traffic
only to available Pods during the update.

.. figure:: rolling_updates.png
	 :alt: rolling_updates

Use the ``set image`` command to start the update. The command notifies the
Deployment to use a different image for your app and initiate a rolling update.

.. code-block:: bash

	kubectl set image $Deployment_Name $New_Image_Version

The update can be confirmed by running a ``rollout status`` command.

.. code-block:: bash

	kubectl rollout status $Deployment_Name

The ``rollout`` command reverts the deployment to the previous known state.
Updates are versioned and can be reverted to any previously known state of a
Deployment.

.. code-block:: bash

	kubectl rollout undo $Deployment_Name

Minikube
========
Minikube is a lightweight Kubernetes implementation that creates a VM on your local
machine and deploys a simple cluster containing only one node.

.. code-block:: bash

	minikube start	# start minikube vm

	minikube stop	  # shutdown minikube vm

Minikube Setup
--------------

#. Install kubectl: https://kubernetes.io/docs/tasks/tools/install-kubectl/
   and add path to environment variables
#. Install Minikube: https://github.com/kubernetes/minikube/releases (rename to minikube.exe)
   and add path to environment variables

kubectl
=======

Commands
--------

.. code-block:: bash

	kubectl action resource #basic format

  kubectl cluster-info #show cluster details

  kubectl get nodes #show nodes that can be used to host your application


Use the ``run`` Command to create a new deployment.

.. code-block:: bash

	kubectl run kubernetes-bootcamp --image=docker.io/jocatalin/kubernetes-bootcamp:v1 --port=8080

To execute commands directly on the container, you can use the ``exec`` command and
use the name of the Pod as a parameter.

.. code-block:: bash

	kubectl exec -ti $POD_NAME bash

	exit #to leave container

Debugging
=========

Attatchen in einen Pod:

.. code-block:: bash

	kubectl exec -it ecadiaserver-postgres-deployment-6f66f9d4c9-ph4rv --container ecadiaserver -- bash

Logs eines

.. code-block:: bash

	kubectl logs pod-name

Dashboard
=========
The Kubernetes Dashboard is a web UI for monitoring and configuring your cluster.

To run the dashboard locally you have to start a ``proxy``.

.. code-block:: bash

	kubectl proxy

The dashboard can be accessed at ``localhost:8001/ui``.

.. code-block:: bash

	kubectl proxy -w . -p 8080

Monitoring and Health Check
===========================

.. code-block:: yaml

	readinessProbe:
		httpGet:
			path: /
			port: 80
		initialDelaySeconds: 1
		timeoutSeconds: 1
	livenessProbe:
		httpGet:
			path: /
			port: 80
		initialDelaySeconds: 1
		timeoutSeconds: 1

Secrets and Configmaps
======================

Kubernetes offers Configmaps and secrets to allow you to distribute sensitive
data to your containerized application.

 .. code-block:: bash

 	kubectl create secret generic tls-certs --from-file=tls/ #create a secret form a from-file

	kubectl create -f pods/secure-monolith.yaml #create pod that uses secret

A Pod gets created and the secret is mounted to the container as a secret.
In this way all the configs are available before the container starts. Once the
volume is mounted, the containing files can be exposed to the file system.

The pods get online and the container starts up.

.. figure:: secret_diagramm.png
	 :alt: secret_diagramm

Konfigmaps allow you to change Environmet variables at deployment time.

Jobs
====

Jobs are a construct that run a pod once and then stop, unlike pods in deployments,
the output of the job is around until you remove it.

Using a ``CronJob`` with a ``schedule`` attribute you can periodically execute
jobs.

DeamonSet
=========

A deamon set ensures that all nodes run a copy of a specific pod.

Stateful set
============

Logging for kubernetes
======================

Your applications should write logs to ``stdout`` so you are able to see them
with ``kubectl logs``.

.. figure:: logging_architecture.png
	 :alt: logging_architecture

Monitoring
==========

Kubernetes Health
-----------------

You can use ``cAdvisor`` and ``heapster`` to monitore node and kubernetes health.
Allows you to see cluster memory and cpu usage

.. code-block:: bash

	minikube addons enable heapster

heapster aggregates monitoring data across all nodes in a cluster. It runs in a
pod in the cluster.

.. figure:: heapster_architecture.png
	 :alt: heapster_architecture


App Metrics
-----------

You can visualize app metrics with prometheus.

You can instrument your application to save application monitoring data at a
``/metrics`` endpoint that prometheus queries in a timely manner.

Prometheus, Heapster and cAdvisor are typically linked to Grafana, a tool to
visualize monitoring data.


Tools
=====

Helm
----
Helm is a tool that streamlines the installation ad management of Kubernetes
applications. Ist is like a package manager for Kubernetes.

Charts are packages of preconfigured kubernetes resources.

- Helm introduces templating to your applications.
- applications can be versioned and rolled back to an earlier version.
-

Ingress
=======

https://hackernoon.com/setting-up-nginx-ingress-on-kubernetes-2b733d8d2f45

.. code-block:: bash

	minikube addons enable ingress
