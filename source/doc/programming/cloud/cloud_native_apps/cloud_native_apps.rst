=================
Cloud native apps
=================

Principles
==========
- Build and compose as microservices
- Packaged and distributed in containers
- dynamically executed in the cloud

Design Príncipes
================

- Design for performance: Responsive, concurrency, efficiency
- Design for automation: Automate dev tasks and ops tasks
- Design for resiliency: Fault tolerant and self healing
- Design for elasticity: Dynamically scale up and down and be reactive
- Design for delivery: Short round trips and automated delivery
- Design for diagnosability: Cluster-wide logs, traces and metrics

.. figure:: cloud_native_stack.png
	 :alt: cloud_native_stack
