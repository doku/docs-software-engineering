=====
Notes
=====

Basic Notes
===========

- Beamer mit sehr leisem Lüfter!

Extensive Load balancing is especially in the early days of a business.
When you get a sudden exposure, a lot of potential future customers will check
out your website and it will go down leaving you with a lot of missed opportunity

Angemessene Code Komplexität: Einfache Probleme dürfen nicht unnötig komplex
programmiert sein. Eine Überlegung der Schwierigkeit des Problems lässt so leicht
die Codequalität beurteilen. Code sollte so leicht und verständlich wie möglich sein.

When deploying a application which heavily depends on data security, make sure
to have the ability to shutdown or lock of the application even when deployed on
the customer's infrastructure, in case of critical bug or possible exploit. This
enables you to prevent any possible data breaches as soon as the bug is noticed
without adding additional latency due to slow response time from your customers
it department. This emergency shutdown  must be disclosed to the customer and
should be tested prior to production release.


If something is important to you do it as early in the morning as possible.
At that time there are absolutely no excuses not to do it. Make it part of your
daily routine.

If you start working early in the morning you will be ahead of everyone else through
out the day. Starting at 6am will allow you to work completely focused with minimal
distractions for almost 3 hours before everyone else arrives at the office. Working
early and going to bed early will make sure you get the most out of your day.
In the evening your are usually not that productive anyways.

Try to work with a **one-day-off** policy. Take one day per week off from regular
work and focus on learning new processes and technologies. Especially in
Softwareengineering it is curtail to stay ahead of the game both in terms of
knowledge and technology stack. Teaching your self, reading books or participating
in workshops and trainings should be a weekly routine. Do not interrupt this
training and learning session by any other kind of work or distractions. Make sure
to track your training process and make notes about the things you learn.

When geo blocking auf feature make sure to inform the user not just in Wiki but
also right where The Feature is expected. Otherwise googleing will result in
differing Versions with no explanation. Your User will think it is a bug.

For Software Configuration Files: don't force the user to write them from scratch, offer
a command line tool which will generate the Configuration file containing all important
Information and commented out configuration options. In that way the user can
make his config just by reading the comments. He will be able to see what even
is possible to configure

Minimalism: Eliminating distractions so you can focus on what is truly important

Measurement boosts awareness. Awareness contributes to quality.

.. hint:: The perfect is the enemy of the good - Voltaire

Backup
======
Configure a Backup Server to be pulling the backup files from the client, which
should be backed up. If the client is compromised and is able to push to your
backup server the intruder can damage your backups.


Company Structure
=================

You should have a monthly meeting with all team members discussing organizational
problems concerning the company. Everyone should say what he/she likes and possible
improvements (Food, Time policy, equipment). This makes sure everyone is heard and
everyone is comfortable.

A software company is nothing more than a collection of talented people working on one mission
and idea. The people define a company and should feel as comfortable as possible.

A company should give everyone the opportunity to express their feelings, criticize
current practices and state ideas for new features.

Teammanagement
--------------
Every employee should be given an annual budget to customize and personalize his
workspace. Goal is to improve efficiency and bind talented employees to your company.
