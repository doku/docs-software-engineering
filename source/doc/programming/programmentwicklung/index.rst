===================
Programmentwicklung
===================

- :ref:`oo-programming`
- :ref:`ref-uml`
- :ref:`architecture-principles`
- :ref:`ref-design-pattern`
- :ref:`refactoring`
- :ref:`android`
- :ref:`processes-threads`
- :ref:`xml`
