=============
Suchmaschinen
=============

**Information Retrieval** beschäftigt sich mit

- der Struktur
- der Analyse
- der Organisation
- dem Speichern
- der Suche
- dem Wiederfinden

von Informationen.

Das Problem beim Information Retrieval ist, dass man meist nicht genau weiß,
was man überhaupt sucht.

Suchmaschinen lassen sich an Datentypen (Bild, Text etc.) und Datenquellen (Web,
Desktop etc.) unterscheiden.

In der Implementierung gibt es indexbasierte, meta und föderierte Suchmaschinen.

.. glossary::

  Informationsbedürfnis
    spiegelt den konkreten Bedarf an Informationen eines Nutzers bezüglich eines
    Themas wider. Es beschreibt was der der Nutzer *nicht* weiß und ist daher
    subjektiv und nicht klar definiert.

  Suchanfrage
    Eine Suchanfrage ist das, was der Nutzer dem Computer mitteilt, um sein
    :term:`Informationsbedürfnis` auszudrücken.

  Relevanz
    Ein Dokument ist **relevant** im Bezug auf ein :term:`Informationsbedürfnis`
    falls der Nutzer meint, dass es
