==========
App Basics
==========

Activity Livecycle
==================

.. figure:: activity_livecycle.png
	 :alt: activity_livecycle

Activity
========

An activity is the android component responsible for most app user interactions.

An activity is the single focused thing that the user can do.

Activities are responsible for creating the window that your application uses
to draw and receive events form the system

An activity creates ``views``, which are ``xml`` files stored in ``res/layouts``.

Fragment
========

Fragments sind losgelöste Oberflächenteile, die von einer Activity gestartet werden.

Weitere Komponenten
===================

Handler
-------
Durch einen Handler können Nachrichten/Runnables an einen Thread geschickt werden.

Services
--------
Services sind für langlaufende Hintergrundprozesse gedacht.

Broadcast Receiver
------------------
- Reagiert auf die vom Android System global versandten Nachrichten
- Activity ohne Benutzerschnittstelle
- Erhalten alle Messages der registrierten intents

Content Provider
----------------
- für den Zugriff auf anwendungsübergreifende Datenspeicher

JobScheduler
------------
- ermöglicht es die Ausführung von Programmen von genau definierten Bedingungen
	abhängig zu machen (keine Nutzer Interaktion nötig)

Asynchron
=========

Example
=======

.. code-block:: java

	public class MainActivity extends AppCompatActivity {

	    TextView text;
	    Button add2;
	    Button add3;

	    int value = 0;

	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main);

	        text = (TextView) findViewById(R.id.valueText);
	        add2 = (Button) findViewById(R.id.add2button);
	        add3 = (Button) findViewById(R.id.add3button);

	        text.setText(String.valueOf(value));

	        add2.setOnClickListener((View v) -> {
	            value += 2;
	            text.setText(String.valueOf(value));
	        });

	        add3.setOnClickListener((View v) -> {
	            value += 3;
	            text.setText(String.valueOf(value));
	        });

	    }
	}
