=========
Typsystem
=========

Der Datentyp legt die Menge von möglichen Werten (und ihre Darstellung) und die
Menge von Operationen, die auf die Werte angewendet werden können.


- Notwendigkeit der Typisierung von Werten
- Wunsch nach Typisierung von Variablen u. ä.

Typbindung
==========

- statisch vor der Laufzeit (durch Deklaration)
- semi-dynamisch bei der Erzeugung (durch 1. Zuweisung)
- dynamisch während Laufzeit (durch jede Zuweisung)

Typdeskriptoren zur Laufzeit ermöglichen :

- Garbage Collecion
- Polymorphie

Eigene Datentypen
=================

- Portabilität (bei eigenen numerische Datentypen)
- Zuverlässigkeit

Typkonversion
=============

Wir sprechen von kompatiblen Typen, wenn zwei Typen äquivalent oder aber implizit
konvertierbar sind.

Äquivalenz
==========
- Strukturäquivalenz (Problem bei rekursiven Typdefinitionen)
- Namensäquivalenz

Strong, Weak und Duck Typing
============================

.. figure:: arten_typing.png
	 :alt: arten_typing

Typsysteme
==========

Monomorphes Typsystem: Eine Variable/Parameter/Komponente/Zeigerziel kann nur
Werte genau eines Typens enthalten.

Polymorphes Typsystem: Eine Variable/Parameter/Komponente/Zeigerziel kann
Werte unterschiedlicher Typen beinhalten:

    - offene Polymorphie der objektorienterten Programierung
    - statisch aufgelöste Polymorphie der generischen Programmierung
    - dynamische Typbindung
