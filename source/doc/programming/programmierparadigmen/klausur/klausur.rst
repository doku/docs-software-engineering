===========================================
Propra Klausur Gedankenprotokoll 31.07.2018
===========================================

Deallocierung

Dangeling References
  - Reference Counting, Problem Zyklische Pointer
  - Lock and Key
  - Tumbstone

Namensbindung
  - Typbestimmung Namen
  - Beuglouis Effekt

Wertesemantik vs. Referenzsemantik

Fragmentierungs Problem Halde:
  - Interessiert Programmierer nur, wenn Programm sehr lange läuft
  - egal, wenn Programm nur kurz läuft

Call By name Parameter, Algol vs Normal

Redefinition von Methoden
  - Sichtbarkeit
  - Typ Bindung bei

.. code-block:: java

  class C {
    public void M(){}
  }
  class C2 extends C {}
  class NC extends C {
    public void N(){}
    public void M(){} // redefinition
  }
  class NNC extends NC {}


  C my_object = new NC();

  my_object.N(); //illegal
  my_object.M(); //dispatching call in redefinition

Funktionsweise Garbage Collection
  - wie kann garbage collector feststellen welche Objekte nichtmehr referenziert sind
  -

Es wurde nichts gefragt zu:

- Funktionale Programmierung
- Logik Programmierung
- Exceptions
