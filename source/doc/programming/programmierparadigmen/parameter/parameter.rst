=========
Parameter
=========

Functions return values, Procedures don't.

Procedures change state, by changing global Variables or Out-Parameters

Mechanismen zur Parameter Übergabe
==================================

call-by-copy
------------

Formale Parameter werden als lokale Variable betrachtet

call-by-value
'''''''''''''
The value of the actual parameter is used to initialize the corresponding
formal parameter, which then acts as a local variable in the subprogram, thus
implementing in-mode semantics.

implementation:

- by copy
- by access path to write-protected cell

call-by-result
''''''''''''''

Beim Austritt wird der Wert des formalen Parameters in den aktuellen
Parameter kopiert.

Calling a procedure with two identical actual parameters can lead to problems:

- the time of evaluation of the address of the actual parameters: time of the call or time of the return
- whats the order in which outparameters are saved?

call-by-value/result
''''''''''''''''''''
implementation of in-out Parameters

combination of call-by-value und call-by-result

The value of the actual parameter is used to initialize the corresponding formal parameter,
which then acts as a local variable.
A subprogram termination, the value of the formal parameter is transmitted back
to the actual parameter

call-by-reference
-----------------

in-out parameters

Rather than copying data values back and forth, as in pass-by-value/result, the pass-by-reference
method transmits an access path, usually just an address to the called
subprogram.

call-by-reference creates multiple aliasing problems.

.. glossary::

  aliasing
    refers to the situation where the same memory location can be accessed using different names.

  aliasing-effect
    Eine Zuweisung mit einem Namen beeinflusst den Wert, der unter anderem
    Namen gelesen wird


call-by-name
------------

inout parameters

When parameters are passed by name, the actual parameter is, in effect, textually
substituted for the corresponding formal parameter in all its occurrences in
the subprogram.


Algol-60-Regel
''''''''''''''
Bei der Algol Regel merkt man sich woher der Parameter kam.

.. figure:: algol-regel.png
	 :alt: algol-regel

Unterprogramme als Parameter
============================
