====================================
Beschreibung von Programmiersprachen
====================================

Syntax
======
Syntax: Beschreibt die Eigenschaften einer Sprache, die sich durch kontextfreie
Grammatiken ausdrücken lassen

Syntax Beschreibung mit BNF, EBNF, Syntaxdiagramme

Semantik
========

Semantik: legt  die Bedeutung einzelner Sprachbestandteile fest

- Statische Semantik: kann (muss aber nicht) bereits zur Übersetzungszeit ausgewertet
  werden
- dynamische Semantik: beschreibt den funktionalen Zusammenhangs zwischen Ein-
  und Ausgabewerten

Attributierte Grammatiken
-------------------------
Die Beschreibung der Semantik eines Programms setzt sich aus den Beschreibungen
der Semantik seiner syntaktischen Bestandteile zusammen.

Dadurch im wesentlichen aus statische Semantik beschränkt.



- Operationale Semantik
- Denotationale Semantik
- Axiomatische Semantik
- nichtdeterministische Semantik
