============
Introduction
============

Interpreter
===========

.. figure:: interpreter.png
	 :alt: interpreter

- Keine Erzeugung von Maschinencode
- Das Programm ist nicht eigenständig ausführbar
- Programmtext wird bei jeder Ausführung analysiert (Schleifen)
- Es ist keine  Codeoptimierung möglich

Compiler
========

.. figure:: compiler.png
	 :alt: compiler

- Analyse und Übersetzung finden nur einmal statt. Code kann in Ruhe optimiert werden
- generierte Maschinenprogram kann beliebig oft ausgeführt werden

Eigenschaften von Programmiersprachen
=====================================

Eigenschaften von Programmen
----------------------------

- writeability
- readability
- modifyability
- reliability
- portability
- efficiency
- maintainability

Eigenschafen von Programmiersprachen
------------------------------------

1. Abstraktion
2. Einfachheit
3. Orthogonalität (Spracheigenschaften können miteinander vermischt werden)
   The meaning of an orthogonal language feature is independent of the context of its appearance in the program.
4. Erweiterbarkeit, Flexibilität
5. Einfache Implementierung
6. Maschinenunabhängigkeit
7. Sicherheit (statische und dynamische Prüfung)
8. Standard-/Formale Definition
9. Prinzip der geringsten Verwunderung

Wartung
=======

In der Wartung wird in aller Regel nur das Symptom beseitigt und nicht die Ursache.

Nicht jede Programmiersprache ist geeignet um gewisse Eigenschaften in code zu
erzeugen.

Semantik
========

Orthogonale Semantik: die Semantik eines zusammengesetzten Teiles, setzt sich
zusammen durch die Semantiken der Einzelteile.

Notes
=====

Der Typ eines Objekts kann sich nach dem es allociert wurde nie wieder ändern.

Wenn Rekursive Aufrufe möglich sind ist ein Stapel zwingend nötig.


Fragrementierung führt zu immer langsamer werdenden allocator.


Reference counting
==================
