=================
Logic Programming
=================

Es sollen die Eigenschaften des zu lösenden Problems in prädikatenlogischer
Formulierung spezifiziert werden, also "was soll gelöst werden". Das "Wie" der
Lösung soll vom Ausführungsmodell bestimmt werden.


Prolog
======

Großschreibung = Variablen
Kleinschreibung = Konstanten, Prädikatnamen


Endlos Rekursion: kann bei Linksrekursion auftreten

NOT Problem: Negierung einer nicht beweisbaren Aussage wird als wahr interpretiert.


Implementierung
---------------

- depth-first im Alternativenbaum -> Backtracking nötig
- sequentiell über den Regeln
