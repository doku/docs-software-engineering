======================
Functional Programming
======================

The meaning of expressions are independent of their context. (referential transparency)

Because mathematical functions have no side effects and cannot depend on any
external values, they always map a particular element of the domain to the
same element of the range.

lambda calculus
===============

Die im :math:`\lambda`-Kalkül dargestellte Funktion ist anonym.

A lambda expression specifies the parameters and the mapping of a function.

Functional forms
================

currying: Rückführung von lambda Ausdrücken mit mehreren gebundenen Variablen
auf lambda Ausdrücke mir genau einer gebundenen Variable

Higher-order Functions
======================

A higher-order function is one that either takes one or more functions as
parameters or yields a function as its result, or both.


A programming language is strict if it requires all actual parameters to be fully
evaluated, which ensures that the value of a function does not depend on the order in
which the parameters are evaluated. Nonstrict languages can use **lazy evaluation**.
Lazy evaluation supports the modularization of programs into generator units and 
selector units, where the generator produces a large number of possible results
and the selector chooses the appropriate subset.

Haskell Examples
================

.. code-block:: haskell

  sort [] = []
  sort (h:t) =  sort [b | b <- t,  b <= h]
                ++ [h] ++
                sort [b | b <- t,  b > h]
