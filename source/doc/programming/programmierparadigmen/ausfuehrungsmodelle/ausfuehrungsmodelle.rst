==================
Ausführungsmodelle
==================

von-Neumann-Modell
==================
Daten und Befehle werden in einem gemeinsamen Speicher abgelegt

Von-Neumann-Sprachen (auch **prozedurale oder imperative Programmierung**) nehmen
auf abstrakten Ebene Bezug auf das Modell:

- Variablen sind symbolische Bezeichner für Speicherzellen
- Kontrollfluss kann durch Kontrollstrukturen beeinflusst werden

Simulationsmodell
=================

- Beschreibung der Bestandteile des realen Systems mit Objekten
- Beschreibung des Zusammenwirkens der Bestandteile durch Nachrichten

Ausführung besteht aus dem Austausch von Nachrichten zwischen Objekten und deren
entsprechender Reaktionen.

Dieses Modell wurde durch viele objektorientierte Programmiersprachen aufgegriffen
und imperativ-prozedural ausgedrückt.

Funktionales Modell
===================

Programmierung ist die Entwicklung von Funktionen und Funktionalen, für die
beabsichtigte Transformation von Eingabe- in Ausgabewerte.

Datenfluss-Modell
=================

Darstellung von Datenabhängigkeiten durch einen gerichteten Abhängigkeitsgraph

- Knoten: die Operationen des Programms
- Kanten: die Datenabhängigkeiten

Der Programmausführung besteht im Transport von Datenwerten entlang der Kanten.
Sobald alle Operanden einer Operation vorliegen wird diese ausgeführt und sendet
ihr Ergebnis ab. (Gut für die Ausnutzung von Parallelität stellt aber
Single-Assignment Forderung)

Logik-Modell
============

.. figure:: logik_modell.png
	 :alt: logik_modell

Beim Logik Modell ist Programmierung die logische Herleitung einer Lösung aus einer
Wissensbasis. Die Aufgabe des Programmierers ist die Erstellung der Wissensbasis
und die Formulierung des Problems.
