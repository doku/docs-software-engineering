================
Bindungskonzepte
================

A binding is a association between an attribute and an entity.

Statische Bindung: Bindungen werden vor der Ausführung des Programms festgelegt
dynamische Bindung: Bindungen werden während der Ausführung des Programms festlegt

Wertebindung
============

variables Object vs. konstantes Object

Namensbindung
=============

Falls ein Objekt mehrere Namen hat, sprechen wir von Aliasing

Unterscheidung zwischen statischer und dynamischer Namensbindung

statische Namensbindung
-----------------------

Gültigkeitsbereich ergibt sich aus der Programmaufschreibung

Homographen = zwei Deklarationen für den gleichen Bezeichner (mit gleicher Signatur)
Homographen in einem umschließenden Gültigkeitsbereich werden von der Deklaration des
geschachtelten Gültigkeitsbereichs für die direkte Sichtbarkeit verdeckt.

Dynamische Namensbindung
------------------------
Gültigkeitsbereich ergibt sich aus der Ausführungsreihenfolge

Sichtbarkeiten
==============

.. glossary::

  Beaujolais-Effect
    Das Hinzufügen einer zusätzlichen Deklaration verändert still-schweigend
    die Laufzeit-Semantik der Implementierung eines anderen bislang bereits
    übersetzbaren Moduls.

Typbindung
==========

Bei Wertänderung muss eine Typprüfung erfolgen.

.. figure:: wertesemantik_referenzsemantik.png
	 :alt: wertesemantik_referenzsemantik
