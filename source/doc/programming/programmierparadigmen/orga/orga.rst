====
Orga
====

cosettacode.org


.. code-block:: haskell

  T I = If 0 != I then put(I) $ T(I-1)
