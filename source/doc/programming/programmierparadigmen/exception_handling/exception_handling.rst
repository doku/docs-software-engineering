==================
Exception Handling
==================

Auslösen
========

- implizites Auslösen
- propagierte Exceptions
- explizites Auslösen

Continuation Semantik
=====================

- "Resume"-Semantik
- "Recovery"-Semantik: Kontrolle wird an das Ende eines umschließenden Konstrukts transferiert
