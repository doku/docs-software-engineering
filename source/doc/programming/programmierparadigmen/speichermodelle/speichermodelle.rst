===============
Speichermodelle
===============

Wir benötigen Speicher für:

- globale Daten
- Codesegmente
- lokale und administrative Daten dieser Konstrukte (Aktivierungsblöcke)
- Daten, deren Lebensdauer nicht an Kontrollstrukturen gebunden sind (Halde)

.. figure:: speicher.png
	 :alt: speicher

Aktivierungsblöcke
==================

Statische Verwaltung von Aktivierungsblöcken
--------------------------------------------

- keine Rekursion möglich


Dynamische Verwaltung von Aktivierungsblöcken
---------------------------------------------

- Aktivierungsblöcke werden auf einem Laufzeitstapel verwaltet (möglich, da das
  Unterprogramm endet, bevor die Aufrufer enden können)
- Neue Aktivierungsblöcke werden an der Spitze des Laufzeitstapel angelegt

Größe der Aktivierungsblöcke bei jeder Aktivierung gleich
  - Größe alle lokalen Daten müssen statisch bekannt sein

Größe der Aktivierungsblöcke von Aktivierung zu Aktivierung unterschiedlich, wird jedoch
zum Zeitpunkt der Aktivierung bestimmt

.. figure:: aktivierungsblock2.png
	 :alt: aktivierungsblock2

Größe von Aktivierungsblöcken kann sich während der Aktivierung ändern

  Alternative: Anlage des Objekts auf der Halde und Verweis im Stapel, damit
  bleibt Aktivierungsblock statisch

  Aktivierungsblock von dynamischer Größe ist einfacher und schneller und vermeidet
  Fragmentierung auf der Halde

Heap
====
Organisation des Heaps mit Blöcken fester oder variabler Größe

Feste Größe
-----------

Variable Größe
--------------

Fragmentierung der Halde

- Best-fit-Methode: Auswahl des Blockes mit dem kleinsten Überschuss
- first-fit-Methode: Auswahl des ersten Blocks mit ausreichender Größe
- next-fit-Methode: Freispeicherliste ist Ringliste, mit rotierendem Anfang: sonst wie first fit
- buddy-Methode: suche nach genauer Größe, wenn nicht vorhanden, zerteile einen Block
- worst-fit-Methode: Auswahl des Blocks mit größtem Überschuss

next-fit und first-fit empirisch am besten

Sichere Freigabe
----------------

Zu frühe Freigabe führt zu dangling references. Um die Gefahr unschädlich zu machen,
gibt es die Methoden "tombstones", "key and lock" und "reference counting".

.. figure:: tombstones.png
	 :alt: tombstones

.. figure:: key_and_lock.png
	 :alt: key_and_lock

Reference Counting
''''''''''''''''''

Jedes Heap-Objekt enthält einen Zähler mit der Anzahl der darauf gerichteten Zeiger.

Der Speicherblock wird erst wenn und sobald der Zähler 0 erreicht freigegeben.

Verhindert Dangling References und führt zur impliziten Freigabe von Abfall.
Unerreichbare Ringlisten können aber nicht freigegeben werden.


Garbage Collection
''''''''''''''''''
1. Markierung aller erreichbaren Haldeobjekte
2. Freigabe der nicht markierten Haldeobjekte und evtl. Kompaktifizierung

Jedes Heap-Objekt enthält ein Markierungsbit.

Markierungsalgorithmus entsprechen dem Durchlaufen eines gerichteten Graphen
(Tiefensuche, Breitensuche...).

.. figure:: markierung.png
	 :alt: markierung

Voraussetzung:
	Zeiger außerhalb des Heaps müssen erkennbar kein
	und Zeiger aus Heap-Objekte auf andere Heap-Objekte müssen erkennbar sein,
	das setzt Kenntnis der Typen gespeicherter Werte zur Laufzeit voraus.

Alle Objekte, die das Programm zwar nicht mehr baucht, die aber immer noch über
Referenzketten erreichbar sind, werden nicht eingesammelt.
