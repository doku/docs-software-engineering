=======
Ranking
=======

Why ranked retrieval?
=====================


Jaccard coefficient
-------------------

.. figure:: jaccard_coefficient.png

Problems with Jaccard

- does not consider term Frequency
- does not factor in that: rare terms are mor informative than frequent terms
- does not rormalize lenght of document

Term Frequency
==============

Bag of words model: we don't consider the order of words in a document. 

:math:`tf_{d,t}` = Termfrequency of Term :math:`t` in document :math:`d`

document frequency is the number of documents in the collection that the term occurs in.

The relevance does not increase proportionally with term frequency, so we use: 

.. figure:: log_frequency_weighting.png

TF - idf
========

:math:`df_t` is the document frequency, the number of documents that :math:`t` occurs in.
this is an inverse measure of the informativeness of the term :math:`t`

:math:`idf_t = \log{10}{\frac{N}{df_t}}` is the inverted document frequency and an measure of 
informativeness. where :math:`N` is the number of documents in the collection. The :math:`log` 
dampens the effect. 

Rare terms are more informative than frequent terms. We want high weighs for rare terms.

:math:`idf` affects the ranking of documents for queries with at least two terms. Single 
term queries are not affected (idf only functions as a scaler but has no effect on ranking)

The :math:`tf-idf` weight of a term is the product of its :math:`tf` weight and its 
:math:`idf` weight. 

:math:`w_{t,d} = (1 + log tf_{t,d}) * log{\frac{N}{df_t}}`

Relation between df and cf: 

Summary
-------

- Assign a tf-idf weight for each term :math:`t` in a document :math:`d`.
- The tf-idf weight increases with the number of occurences within a document (Termfrequency)
- The tf-idf weight increases wtih the rarity of the term in the collection (inverse documet frequency)

.. figure:: tf_vs_stopwords.png

Vector Space model
==================

.. figure:: vector_space_model.png

Each document is now represented as a real-valued vector of tf-idf weights :math:`\in \mathbb{R}^{|V|}`

- Represent the query as a weighted tf-idf vector
- compute the cosine similarity between the query vector and the document vector
- rank documents according to their proximity to the query
- return the top :math:`K` to user

Euclidean distance does not work to measure difference because it would be large for vectors of different lenght. 
Instead we rank documents according to angle wtih query.

Rank documents according to cosine(query, document) in increasing order.

.. figure:: cosine_similarity.png

.. figure:: cosine_similarity_formula2.png

.. figure:: cosine_similarity_formula.png

.. figure:: cosine_for_normalized_vectors.png

Importance of Ranking
=====================

.. figure:: importance_of_ranking.png


Compute Ranking
===============
There is no need for a complete ranking. We just need the top :math:`k` for a small :math:`k`.

- Have query independent measure heuristik for "goodness", like pagerank, and sort postings list by that
- define composite score fo a document: : `net-score(q,d) = pagerank(d) + cos(q,d)`

document-at-a-time
------------------
Both docID-ordering and PageRank ordering impose a consistent ordering on documents in postings lists.

We complete computation of the query-document similarity score of document :math:`d_i` before starting
to compute the query-document similarity score of :math:`d_{i+1}`.



term-at-a-time
--------------

Weight sorted postings:

.. figure:: weight_sorted_postings.png

Term at a time processing: 

.. figure:: term_at_time_processing.png

.. figure:: term_at_time_processing_code.png


You create your index differently than before now. You sort them by 

.. note::

    For DAAT processing, it suffices to have the postings list in the inverted index 
    sorted by document id or some global measurement like page rank. 

    For TAAT processing however, the postings list should be ordered by weighted score
    like tf-idf. 