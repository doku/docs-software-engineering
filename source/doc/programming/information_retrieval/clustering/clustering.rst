==========
Clustering
==========

Clustering documents is the process of grouping a set of documents into clusters of simular documents.

Documents within a cluster should be similar, Documents from different clusters should be dissimilar.

Clustering is the most common for of **unsupervised** learning. (Unsupervised = no labled data used in training)

.. figure:: clustering-classification.png

.. note::

    **Cluster Hypothesis**

        Documents in the same cluster behave similar with the respect to relevance to information needs. 

        "closely associated documents thend to be revelant to the same requests"

.. figure:: scatter-gather.png

.. figure:: clustering_for_better_recall.png

Goals for Clustering
--------------------

.. figure:: goals_for_clustering.png

- Flat vs. Hirachical Clustering
    - Flat: start with random (partial) partitioning of docs into groups and refine iteratively (K-Means)
    - Hierachical: create hierarchy (bottom-up / top-down)
- Hard vs. soft clustering
    - hard clustering: each document belongs to exactly one cluster
    - soft clustering: a document can belong to more than one cluster.


Flat Algorithms
---------------

.. figure:: flat_clustering.png


K-Means
=======
- best known clustering algorithm, simple and works well
- uses Vector space model, same simularity measures as before, common for clustering: Euclidean distance

**K** is the number of Clusters you have. 

.. figure:: kmeans_basic_idea.png


1. select some documents as your centroids
2. Assign points to closest centroids (each document has the class of the centroid that is closest to it)
3. Calculate the center of all documents in one cluster and move new centroid there
4. Repeat 2 and 3 until centroid doesn't move. 

K-Means Convergence
-------------------

.. figure:: kmeans_converge.png

Recomputation of centroids decreases average distcance: 

.. figure:: kmeans_converge_2.png

K-Means is guaranteed to converge. Commonly, convergence is fast (if you are not taking into account every instance. Just have a threshold of elements that is allowed to swap during reasissignmet steps and stop when you've reached it)

Optimality is a great weaknes of K-Means. The set of seeds has a big impact on the result. So in practice you repeat 
k means with different sets of seeds. 

Seeds
-----

suboptimal clustering: 

.. figure:: suboptimal_clustering.png

.. figure:: seed_selection.png

Time Complexity
---------------

.. figure:: kmeans_time_complexity.png

Evaluation
==========

We take a gold standard data set with classification. Our Clustering should reproduce the classes in the gold standard .


Purity
------

.. figure:: purity_1.png

.. figure:: purity_2.png

If you have a higher K it is more likely to have a greater purity.
 

Rand-Index
----------

.. figure:: rand_index_1.png

.. figure:: rand_index_2.png

.. figure:: rand_index_3.png


Other Evaluation measures
-------------------------

- Normalized mutual information (entropy based approach)
- F - measure 
- 

Hierarchical Clustering
=======================

.. figure:: hierarchy_clustering_1.png

Hierarchical Agglomerative Clustering (HAC)
-------------------------------------------
- creates a hierarchiy in form of a binary tree
- uses a similarity measure for determining the similarity of two clusters
- it's bottom up


.. figure:: hac_1.png

Dendogram
.........

.. figure:: dendogram.png

Cluster similarity
------------------

.. figure:: cluster_similarity.png

.. figure:: cluster_similarity_1.png

.. figure:: cluster_similarity_2.png

.. figure:: cluster_similarity_3.png

.. figure:: cluster_similarity_4.png

.. figure:: cluster_similarity_5.png

.. figure:: cluster_similarity_6.png

Singel & Complete Link Clustering
---------------------------------

for single link you compare the distance between the closest elements of different clusters

for complete link you compare the distance between the farthers away elements of different clusters

We then use this similarity score an join the clusters with the best score. 

.. figure:: single_complete_link_clustering_1.png

.. figure:: single_complete_link_clustering_2.png


.. figure:: complete_link_1.png

.. figure:: complete_link_2.png


Centroid HAC
------------

.. figure:: centroid_hac_1.png

.. figure:: centroid_hac_2.png

.. figure:: centroid_hac_3.png

.. figure:: centroid_hac_4.png

two different clustering approaches can lead to the same clustering but will have different dendograms. 

Group Average Agglomerative Clustering (GAAC)
---------------------------------------------

- No Inversions!
- similarity of two clusters is the average **intrasimilarity** (the average similarity of all document pair - including those from the same cluster.)

.. figure:: clustering_selection.png

Labeling
========
We need a lable for each cluster.

.. figure:: labeling.png

Bisecting K-Means
=================

.. figure:: bisecting_kmeans.png

Bisecting K-means is more efficient than HAC when not a full hierarchy is needed
It is less efficient when a full hierarchy is needed. 