==============
Classification
==============

Text  Classification
====================

Training
--------

.. figure:: text_classification_definition.png

Application/Testing
-------------------

.. figure:: text_classification_application.png

Supervised Learning
-------------------

.. figure:: supervised_learning.png

.. figure:: other_learning.png

Input
-----

Instance
    one example we want to classify

Attribute or Feature
    representing characteristics of an Instance
    
Class or Prediction
    one specific feature we want to predict 


.. figure:: attributes.png

Training Sets
-------------

- Training set: Data used to optimize the parameters of a model
- Validation set: data intermediate models can be tested on to optimize meta-parameters
- Test set: Datat never seen while building a model, for reporting final results. 

Cross Validation
----------------

k-fold cross Validation

.. figure:: cross_validation.png


Topic Classification
--------------------

.. figure:: topic_classification.png

Usecases in IR
--------------

How search engines use classification

.. figure:: usecases.png

Classification Methods
======================

Manual
------
- done my Yahoo in beginning of web
- scaling is difficult and expensive

Rule-Based
----------


Statistical/Probabilistic
-------------------------

.. figure:: probabilistic_classifier.png


Naive Bayes Classifier
======================

The Naive Bayes classifier is a probabilistic classifier.

Independence Assumption: We assume that all 

.. figure:: naive_bayes_1.png

.. figure:: naive_bayes_2.png

.. figure:: naive_bayes_3.png

- Each conditional parameter :math:`\log{P(t_k | c)}` is a weight that indicates how good an indicator :math:`t_k` is for :math:`c`
- the prior :math:`\log{P(c)}` is a weight that indicates the relative frequency of :math:`c`
- The sum of log prior and term weights is then a measure of how much evidence there is for the document being in the class
- we select the class wtih the most evidence.

.. figure:: naive_bayes_4.png

.. figure:: naive_bayes_example.png

.. figure:: naive_bayes_example2.png

Smoothing in Naive Bayes
------------------------

.. figure:: naive_bayes_5.png

By definition we now just say, that every term is in every document.

.. figure:: naive_bayes_6_smoothing.png


example
-------

.. figure:: time_complexity_nb.png

Time Complexity is linear in Training and Testing. 

Two Independence Assumptions

- If I have one word in a document it doesn't tell me anything about what other words are in the document
- Where in the document a term is has no effect on the probability of a beeing in a class

.. figure:: naive_bayes_independence_assumptions.png

NB can work surprisingly we 

.. figure:: naive_bayes_independence_assumptions2.png

.. figure:: naive_bayes_evaluation.png


Overcounting
------------
Bayes can't handle correlated features.  

Evaluation of Text Classification
=================================

.. figure:: precision_recall.png

.. figure:: fmeasure.png


Averaging
---------
.. figure:: averaging.png

Macroaveraging calculates the f measure and then averages.

Microaveraging avearegs the TP, TN, FP, FN for all classes and then calculates the F measure. 

Macroaveraging ignores the differeces in class distribution. 
With Microaveraging a class wiich is dominating the dataset it contributes more to TP, TN etc..
After you calculated the F measure in Macroaveraging you don't even know how frequent a class is. 

It is easier to get a high value for Microaveraging in for example unbalanced data sets. 

.. figure:: exercise_1.png

.. figure:: exercise_solution.png 


Maximum Entropy Classifier
==========================

.. figure:: maxentro_class.png

:math:`f`: function that is 1 if term is in document 
:math:`\lambda` feature weight


example
-------

.. figure:: example_1.png

.. figure:: example_2.png

.. figure:: example_3.png

.. figure:: example_4.png


features
--------

.. figure:: features.png



Features in Text Classification

- Words
- number of sentences
- good choice is application/data specific

Learning: Parameter Estimation
------------------------------

(Slide 15)

.. figure:: iterative_optimization.png

.. figure:: parameter_estimation.png

.. figure:: derivative.png

.. figure:: derivative_example_1.png

.. figure:: derivative_example_2.png

Feature Selection
=================

.. figure:: bias_variance_dilemma.png


Wrapper
    - use the learning procedure
    - change feature sets
    - evaluate model using development set/cross Validation

    - Pro: Can lead to very good results
    - Con: slow, depending on feature space not feasible, might lead to overfitting. 

Filters
    - Estimate impact of each feature
    - select feature set (based on threshold) 

    - Pro: Preprocessing & Efficient
    - Con: not necessarily the best results as classifier agnostic 

Combination between Filter and Wrapper are Possible. 

Forward Search: You start with the feature that has the highest Mutual information and add it. Calculate the model and add new features until doesn't improve
Backward Search: Start with all features and remove the ones with the lowest mutual information one by one. This is better at detecting correlation between features, but slower

Mutual Information
------------------

This is a filter 

.. figure:: mutual_information.png

When I tell you a value for x how much do you learn about a value for y. 

.. figure:: mutual_information_formula.png

The Nominator and the Demonimanor would be exactely the same if x and y would be independent of each other. 
Last Part is a independence measure.

We weight this by the frequency of the feature class combination. 

example
.......

.. figure:: mi_example_1.png

.. figure:: mi_example_2.png

Pointwise Mutual Information
............................

Pointwise Mutual Information (PMI) allows you to get an insight into the documents of a class without looking at all of them

.. figure:: pmi_formula.png

This shows PMI for Reuters

.. figure:: pointwise_mutual_information.png



Vector Space Classification
===========================
- every document is a vector, one component for each term 

.. figure:: vector_space_classification.png

- Premise 1: Documents in the same class form a contiguous region
- Premise 2: Documents from different classes don't overlap

kNN Classification
==================
kNN = k nearest neighbors 

Classifier: 
    - Lazy: Generalization during Prediction (kNN)
    - Eager: Generalization during learning (Bayes, MaxEntropy)

kNN is often more accurate than Naive Bayes. But it is lazy and therefore pretty slow at run time.

kNN classification rule for :math:`k=1` (1NN): Assign each test document to the class of its **nearest neighbor** in the training set. 1NN is not very robust: one document can by mislabled or atypical.

kNN classification rule for :math:`k>1` (kNN): Assign each test document to the **majority class of its k nearest neigbors** in the training set. 


Rationale of kNN: contiguity hypothesis:
    We expect a tesd docmuent :math:`d` to have the same lable as the training documents located in the local region surrounding :math:`d`. 


Probabilistic Interpretation of kNN: :math:`P(c|d) =` fraction of :math:`k` neigbors of :math:`d` that are in :math:`c`

**kNN classificatio rule for probabilistic kNN**: Assign :math:`d` to class :math:`c` with the highest :math:`P(c|d)`


.. figure:: knn_example.png

.. figure:: knn_example_2.png


Distance Weighted kNN
---------------------
- we use all instances instead of just k
- weight distance to the query instance (called **shepard's Method**)
- 


Time Complexity of kNN
----------------------

.. figure:: knn_complexity.png

With kNN no training is necessary. But linear Preprocessing of documents is as expensive as training Naive Bayes. 
We always preprocess the training set, so in reality training time of kNN is linear. 

kNN is very accurate if training set is large. kNN can be very inaccurate if training set is small. 

.. note::

    Test time is proportional to training set size.


kNN with the inverted Index
---------------------------
Use test document as query: finding :math:`k` nearest neigbors is the same as determining the :math:`k` best retrievals.
 
we can use the standard vector space inverted index methods fo find the :math:`k` nearest neighbors.


Linear Classifiers
==================

.. figure:: linear_classifiers_definition.png

XOR is for exmaple not linear classifyable

.. figure:: xor_classify.png


Linear Classifier in 1D
-----------------------

.. figure:: lin_classifier_1d.png

Linerar Classifier in 2D
------------------------

.. figure:: lin_classifier_2d.png

Linear Classifier in 3D
-----------------------

.. figure:: lin_classifier_3d.png

 
Naive Bayes as linear Classifier
--------------------------------

.. figure:: linear_classifier_naive_bayes.png

.. figure:: linear_classifier_naive_bayes_example.png

kNN is not a linear classifier
------------------------------

.. figure:: no_linear_classfier_knn.png

    
Precepton Rule
--------------

.. figure:: percepton_rule.png

.. figure:: hyperplane_example.png

For linearly seperatebale training sets there are infinetely many separating hyperplanes.

Support Vector Machine (SVM)
============================
It's a large margin classifier and uses the vector space classification (like kNN and linear classifiers).

- Aim to find a separating hyperplane (decision boundary) that is **maximally far** from any point in the training data. 
- In case of non-linear-separability: We may have to discount some points as outliers/noise.

.. figure:: support_vector_machine.png

- Points near the decision surface are **uncertain classification decisions**
- A classifier with a large margin makes **no low certainy classification decisions** (on the training data)
- Gives classification safety margin with respect to errors and random variations. 
- this is also a unique solution: **there is only one line with a maximum margin**

.. figure:: hyperplane.png

.. figure:: svm_example.png

.. figure:: svm_optimization_1.png

.. figure:: svm_optimization_2.png

.. figure:: svm_optimization_3.png

R is always a positive value for correct predictions. 
With SVM we don't optimize so that we make a correct prediction. But we optimize to have a hyperplane that is far away from all instances.
In the optimization process we only consider those hyperplanes that are correct. 

.. figure:: svm_optimization_4.png

for multiple classes
--------------------

.. figure:: multi_classes.png

- classes are mutually exclusive
- each document belongs to exacly one class.

Combine two-class linear classifiers as follows for one-of classification:
    - run each classifier separately
    - Rank classifiers (e.g. according to score)
    - Pick the class with the highest score
    - **An element can only be in one class.**

Any-of or multilable classification
    - A document can be a member of 0, 1, or many classes
    - A decision on one class leaves decisions open on all other classes
    - Example: topic classification
    - Usually: make decisions on one region, on the subject area, on industry and so on "independently"
    - simply run each two classifier separately on the test document and assign document accordingly



Classifier Overview
===================

.. figure:: classifier_overview.png


Neural Networks
===============
Percepton is a neural network.

.. figure:: neural_network_motivation.png

.. figure:: simple_neural_network.png

the 1 is :math:`-w_0`.  :math:`w_0` is the threshold

.. figure:: activation_functions.png

OR Neuron
---------

.. figure:: or_neuron.png

And-Neuron
----------

.. figure:: and_neuron.png

It's not possible with one line (one perceptron) possible to get a XOR.

.. figure:: xor_neuron.png

Learning Parameters in Feed Forward Neural Network
--------------------------------------------------

.. figure:: neural_network_1.png


Backpropagation
---------------

- **Inference**: Forward Algorithm: calculate results from each neuron layer-by-layer
- **learning**:   weight adaption by backpropagation calculate derivative layer by layer backwards

.. figure:: backpropagation.png


Learning How To Rank
====================

We want to learn weights 

Given a set of training examples, each of which is a tuple of a query d, a document d, a relevance judgment for d on q
 - simplest case: R(d,q) is either relevant (1) or not relevant (0)
 
.. figure:: learn_to_rank_concept.png


Features
--------

- Vector space cosine similarity between query and document (denoted :math:`\alpha`)
- The minimum window width within wich the query terms lie (denoted :math:`\omega`)


This can solve the problem that vector space ranking is not considering phrases and term disctances. 

.. figure:: learn_to_rank_example.png

Each element in this vector space is a document, query pair. 

.. figure:: learn_to_rank_example_2.png

.. figure:: learn_to_rank_2.png

With this approach we can also generalize to a large number of features:

- Document specific features: document length, domain name, page rank etc. 
- document-query features: term frequency, 

Shortcommings of Learn to rank
------------------------------
- classification: a categorical variable is predicted
- regression: a real number is predicted 

In between is the field **ordinal regression** where a ranking is predicted.

Ranking SVMs
------------

Reformulation of classification problem: given two documents to a querry, which one is better?

This a simpler problem than predicting an exact score. 

Documents can be evaluated relative to other candidates, rather than beeing maped to a global scale of goodness.

.. figure:: ranking_svms_1.png

.. figure:: ranking_svms_2.png

.. figure:: ranking_svms_3.png
