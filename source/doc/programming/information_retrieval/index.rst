=====================
Information Retrieval
=====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   overview/*
   summary/*
   information_retrieval/*
   terms/*
   ranking/*
   evaluation/*
   search_system/*
   page_results/*
   probabilistic_ir/*
   classification/*
   clustering/*
   link_analysis/*

