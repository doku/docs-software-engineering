Terms 
=====

- Token: Character sequence in document (closely related to Word)
- Type: Equivalence class of tokens (related to Term: normalized Type), as it occurs in the IR systems dictionary

We need to normalize words in the indext text as well as in the
query terms into the same form. (U.S.A and USA) Normalization
and language detection interact (MIT vs mit)

Detecting terms can be very difficult depending on the language (Chinese, 
in Arabic text with numbers you have biderectional text). 

You should reduce all letters to lowercase, even if semantically meaningfull
(MIT vs mit) as the user usually writes lower case regardless of correct 
capitalization. That's called **case folding**.

Stop words
----------
Stop words are extremely common words which would appear to be little value
in helping select documents matching user need. For example ``and, the, with, ...``.

Stop word elimination used to be standard in older information retrieval systems.
Most web search engines index stop words. 

Equivalence Classing
--------------------

.. glossary::

    Soundex
        phonetic Equivalence (Muller = Mueller = Miller)

    Thesauri/Ontologies
        semantic Equivalence or similarity (car = automobile)

Lemmatization
-------------
Reduce variant forms to base form - implies doing proper reduction to the dictionary form. 
You diffenentiate between **Infletional Morphology** (cutting => cut) and 
**derivational Morphology** (destructino => destroy).

- am, are, is -> be
- the boy's cars are different colors -> the boy car be different color

Stemming
--------
Stemming is a crude heuristic process that **chops off the end of words**  
in the hope of achieving what *principled* Lemmatization attemts to do
with a lot of linguistic knowledge. 

Stemming increases the effectiveness for some queries and decreases it for others.
But sometimes equivalence classes are pretty big.

Porter Algorithm
    - Most common algorithm for stemming in english
    - uses conventions and 5 phases of reductions that are applied sequentially
    - sample command: remove final 'ement' from end of word
    - sample convention: of the rules in a compound command , select the one that applies to the longest suffix
    - rules: `sses -> ss`, `ies -> i`, `ss -> ss`, `s -> `


Accents 
-------
How are users likely to write their queries for these words? 
Event in languages that standardly have accents, users often don't
type them (Polish).