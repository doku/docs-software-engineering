===================================
Information Retrieval & Text Mining
===================================

Information Retrieval (IR) is finding material (usually document) of an 
unstructured nature (usually text) that satisfies an information need 
from within large collections.

Text Mining is the derivation of information (usually in structured form) from unstructured text. 
In contrast, data mining is typically applied on structured data. Typically, methods from information
Retrieval are used. 

Boolean Retrieval
=================
Queries are Boolean expressions (`Brutus AND Caesar`) and the search engine returns all documents
that satisfy the Boolean expression.

On Google the default interpretation of a query is (w_1 And w2 And ...).

Simple Boolean retrieval returns matiching documents in no particular order.


Inverted Index
==============
Saving a matrix of words over documents results in a huge matrix (incidence matrix). 
Mostly filled with zeros. 

With the inverted index we save for each term :math:`t` the list of all document that contain :math:`t`.

#. Collect the documents to be indexed
#. Tokenize the text, turning each document into a list of tokens
#. Do linguistic preprocessing, producing a list of normalized tokens, which are the indexing terms (all to lowercase etc.)
#. Index the documents that each term occurs in by creating an inverted index, consisting of an dictionary and postings

.. figure:: inverted_index.png

The runtime of the inverted index search is linear in the sum length of the
posting lists (only if posting lists are sorted).

To optimize a boolean search you can start with the shortest list
to limit the required comparisons. 

.. figure:: optimized_intersection_algorithm.png

Skip Pointers
-------------

Stik pointers enable to skip postings that will not figure in the
results. 

- tradeoff between number of items skipped and frequency skip can be used
- more skips result in each skip pointer only skipping a few items but we can use it frequently.
- fewer skips reslut in large jumps that can not be used that often. 

.. note::

    Simple heuristic
        For posting list of length :math:`P` we use :math:`\sqrt{P}` evenly-spaced
        skip pointers. 

Skip pointer are pretty easy in a static index and harder in dynamic environment as they have to be updated. 

Using Skip pointer is a Memory/Computation trade-off.


Phrase Queries
==============
We want to answer a querry like `stanford university` as a phrase. 
"The inventor Stanford Ovhinsky never vent tu university" should not be a match. 

.. hint:: 
    10% of all web queries are phrase queries.

It no longer suffies to store just docIDs in posting list. 


Biword Index
------------
We **index every consecutive pair of terms** as a phrase. 

example: "test code today" would generate "test code" and "code today"

Each of these biwords are now a vocabulary term. Two-word pharses can now be answerded. 

For longer phrases we split it in multiple biword pairs and run a boolean query. Post filtering
is then needed to check if we actually hit the longer phrase.

Biword Indices result in a lot of false positives and blow up the indext due to a large term vocabulary. 

Positional Index
----------------
Positional Indices are more efficient than the Biword index. 

Postings lists in a **non positional index**: each posting is just a document id. 

Postings lists in a **positional index**: each posting is a document id and a list of positions.

.. figure:: positional_index_example.png

We can use the positional index for proximity search as well. (find all documents that contain employment
and place within 4 words of eachother). You can do this by just looking at the 
cross-product of positions of emplyoment and place in the document.

This is very inefficient for frequent words, especially stop words. But it is important for 
dynamic summaries. 

Combination
    Include frequent biwords (like Michael Jackson) as vocabulary terms in the index and 
    do all other phrases by positional intersection. 

For websearches positional queries are much more expensive than regular boolean queries. 

Dictionaries
============

Dictionary: the data structure for storing the vocabulary terms. 
Term vocabulary: the data

Can be implemented with both Hashes or Trees

Hashes
------
- lookup is faster than in tree and lookup time is constant
- no way to find minor variants in terms
- lookup time could be higher depending on the hash functions
- no prefix search possible (all terms starting with auto)

.. figure:: polynomial_rolling_hash.png

Trees
-----
- trees solve prefix problem
- lookup in :math:`O(log M)` where :math:`M` is the size of the vocabulary
- rebalancing binary trees is expensive (B-trees adress that)


Wildcard Queries
================
.. todo::
    Lecture 4

Permuterm Index
---------------

.. figure:: Permuterm.png

Permuterm more than **quadruples** the size of the dictionary compared to regular tree (empirical number).

.. figure:: termmapping.png

k-gram Index
------------

.. figure:: kgram.png



Edit Distance
=============

You need to do spell correction on the documents being indexed and the user queries.

You can do isolated word spelling correction or Context sensitive spelling correction


Correcting documents
--------------------
The general philosophy in IR is : don't cange docmunets. 


Correcting Queries
------------------

Isolated Word spelling correction:

- we have a list of correct words
- we compute the distance between a correct and misspelled word 
- we return the correct word that has the smallest distance to the misspelled word


The edit distance between string :math:`s_1` and string :math:`s_2` is the 
minimum number of basic operations that convert :math:`s_1` to :math:`s_2`.

For the Levenshtein distance the basic operations are insert, delete and replace. 

Damerau-Levenshtein includes transposition as fourth possible operation.

.. figure:: levenshtein_distance_computation.png


.. figure:: levenshtein_distance_computation_2.png

.. figure:: levenshtein_distance_computation_3.png


Weighted Edit Distance:
  You can also account for the weight of an edit operation, depending on character invovement.


Spelling
========


k-gram indexes for spelling correction
--------------------------------------

.. figure:: kgram-spell-correction.png

Context-sensitive spelling correction
-------------------------------------

.. figure:: context-sensitive-spelling-correction.png



Indexing
========

Reuters RCV1 Dataset
--------------------

.. figure:: rcv1_stats.png

We can no longer keep all postings in memory to sort them. We have to store intermediate results 
on disk. **We need an external sorting algorithm.**


Blocked sort-based indexing (BSBI)
----------------------------------

We split postings in block that can fit into memory. 

BSBI keeps one dictionary and sorts each block into it. 


.. figure:: merging_blocks.png

Sinle-pass in-memory indexing (SPIMI)
-------------------------------------

SPIMI build full index for each block and merges later

- split postings in blocks that can be stored in memory
- Key Idea 1: generate separate dictionaries for each block - no need to maintain term-termID mapping
  across all blocks.
- Key Idea 2: Don't sort (across blocks/termID-DocID pairs) Accumulate postings in postings list as they occur. 
- This allowes us to generate a complete inverted index for each block
- the seperate indexes can then be merged into one big one

Distributed indexing
====================
We use a master machine that breaks up indexing into sets of parallel task
and assigns each task an idle machine from a pool.

.. figure:: data_flow.png

what information does the task description contain that the master gives to a parser? Set of Document IDs and # of segments

What information does the parser report back to the master upon completion of the task? Position of the prefix-based term partitions for the documents 

What information does the task description contain that the master gives to an inverter? position of prefix-based term partitions for the documents. 

What information does the inverter report back to the master upon completion of the task?
position of the complete postings list for the prefix. 

Number of segmenets have to be told to parserf

Dynamic Indexing
================

Up to now we have asumed that collections are static. But they are not. Documents are added, deleated and modified. 

- Maintain a big main index on disk
- new docs go into small auxiliary index in memory
- search across both an merge results
- periodically merge auxiliary index into main one
- Deletions: Invalidation bit-vector for deleted docs

logarithmic merge
-----------------

- use logarithmic merge to amortize the merge cost over time
- maintain series of indexes, each twice as large as the previous one. 
- keep smallest :math:`Z_0` in memory
- larger ones :math:`I_0, I_1, ...` on disk
- if :math:`Z_0` gets to big :math:`(>n)`, write to disk as :math:`I_0`
- or if :math:`I_0` already exists merge with :math:`I_0` and write merger to :math:`I_1` etc.

.. figure:: logarithmic_merge.png

Lucene
======

.. figure:: lucene.png

Compression
===========
 
Term Statistics
===============

Heaps Law
----------

.. figure:: heaps_law.png

Zipfs Law
---------

Key Insight: Few frequent terms, many rare terms

.. figure:: zipfs_law.png

Postings Compression
--------------------
The postings file is much larger than the dictionary (factor of at least 10)
Instead of storing the docID, just store the gaps.

.. figure:: posting_compression.png

Variable Lenght Encoding (07)

- variable Byte code

.. figure:: variable_byte_code.png

Gamma Code
----------
- Binary value of : Length + Offset
- Offset is the value to be saved with the leading bit removed
- length is the unary represantation of the lenght of the offset (number of letters) with a trailing 0

.. figure:: gammacode.png


.. figure:: gammacode2.png

The gamma code values refer to the gab of the doc ids compared to the previous one.