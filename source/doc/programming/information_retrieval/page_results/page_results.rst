============
Page Results
============

Summaries
=========

**static summary**
    static summary of a document is always the same, regardless
    of the query 

**dynamic summary**
    are query dependent. they attempt to explain why the document was
    retrieved for the query at hand

    Prefer snippets in which query terms occure as a phrase or jointly in a small window

    To generate these snippets we need to cache documents 