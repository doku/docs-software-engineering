=============
Link Analysis
=============

- citation frequency = inlink count
- weighted citation frequency

Page Rank
=========

.. figure:: page_rank_1.png

.. figure:: page_rank_2.png

.. figure:: page_rank_3.png

The web graph must correspond to an **ergodic Markov chain**.

That means tha now deadends are allowed.

Teleporting
-----------

.. figure:: teleporting.png

With teleporting we cannot get stuck in a dead end. 

Ergodic Markov Chains
---------------------

.. figure:: ergodic_markov_chain_1.png

.. figure:: ergodic_markov_chain_2.png

Compute Page Rank
-----------------

.. figure:: compute_page_rank_example_2.png

Page Rank Summary
-----------------

.. figure:: page_rank_summary.png

.. figure:: page_rank_summary_2.png


HITS: Hubs & Authorities
========================

.. figure:: hits_1.png 


Crawling
========

Web search engines must crawl their documents.


Anchor Text
===========

 