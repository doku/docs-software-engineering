=================
Probablilistic IR
=================

.. figure:: probablilistic_approach.png

Document Ranking
================

.. figure:: document_ranking_problem.png

Probability Ranking Principle (PRP)
-----------------------------------

if the retrieved documents are ranked decreasingly on their probability of relevance,
then the effectiveness of the system will be the best that is optainable.

Binary Independence Model (BIM)
===============================

.. figure:: bim_assumptions.png

Baysche Regel: 

.. figure:: bim_formula.png

.. figure:: bim_formula_2.png

.. figure:: bim_formula_3.png

.. figure:: bim_formula_4.png

You might not see one document representation in your collection very often. 

Solves Sparsity Problem because we now observe words rather than documents. 


Extensions
==========

Okapi BM25
----------
- is a probabilistic model that incoporates term frequency (nonbinary) and length normalization.
- BM25 is none of the 

Simplest score for document :math:`d`: `idf` weighting for query terms which are in the document. 
(RSV: retrieval status value)

.. figure:: bm25_basic_weighting.png


- :math:`k_1 = 0`: only idf value, we ignore the tf
- :math:`k_1 = 1` and :math:`b=0`: we dont consider the length
- :math:`k_1` very large values:  idf is ignored
- :math:`b = 1` longer documents get lower score


Language Models (LM)
====================

(Lesson 11)

We view the document as a generative model that generates the query.

We need to

- define the precise generative model we want to use
- estimate the parameters (different parameters for each document's model)
- smooth to avoid zeros
- **apply to the query and find documents most likely to have generated the query**
- present most likely documents to user.

We have different language models for each document.

.. figure:: lm_for_document.png


We rank according to the Probability of a query.

.. figure:: lm4ir_1.png

.. figure:: lm4ir_2.png

`^` just means that we do an estimate.  

.. figure:: lm4ir_3.png

Jelinek-Mercer smoothing
------------------------

.. figure:: jelinek-mercer-smoothing_1.png

- with a high :math:`\lambda` all the words of the query have to occure in the document
- low :math:`\lambda` words can be missed in a document

.. figure:: jelinek-mercer-smoothing_2.png

.. figure:: jelinek-example.png


Dirichlet smoothing
-------------------

.. figure:: dirichlet-smoothing.png

Dirichlet proforms better for keyword queries, Jelinek-Mercer preforms better for verbose queries. 

Both models are sensitive to the smoothing parameters. You shouldn't use these models without parameter tuning. 

All of these probalbilities are precalculated and can be stored in our inverted index.

Models Comparison
=================

.. figure:: model_comparison.png