========
Overview
========

Topics
======

- Boolean Retrieval
- Term Vocabularies
- Postings Lists
- Dictionaries
- Tolerant Retrieval
- Spelling Correction
- Index Construction
- Compression
- Scoring
- Ranking
- Evaluation
- Query Expansion
- Probabilistic IR (BIM, BM25, Language Models)

- Text Classification
- Naive Bayes
- MaxEnt
- Vector Space Classification
- Support Vector Machines
- Neural Networks
- Learning to Rank
- Clustering
- IR on the web
- Crawling
- Link Analysis


Aufgaben
========

- build inverted index with skip pointers
- Heaps Law (6)
- Compression: Variable Byte Code, Gamma Code
- Kappa (annotation evaluation)

- calculate tf-idf & jaccard score (8 Slide 22)
- create precision recall graph
- cosine similarity
- LM4IR Jelinek Smooting 
- Naive Bayes Classification
- calculate Micro and macro F Measures
- Max entropy Classification
- Max Entropy Parameter Estimation (8 Slide 19)
- Single & Complete Link Clustering (19 End)



Exam
====

- 60 Minutes
- Read Exam without time Preasure
- After Exam read you can ask questions and they'll be answered for everyone


Formulas
========

- Heaps Law: :math:`M = kT^b` M is the size of the vocabulary (terms), T is the Number of tokens in the collection

TF idf
------

:math:`idf_t = log(N/df_t)` with :math:`N` number of documents

Precision recall
----------------

Precision: P(relevant|retieved) = #(relevant items retieved) / (total retrieved items)
Recall: P(retrieved|relevant) = (relevant items retrieved) / ( total relevant items)