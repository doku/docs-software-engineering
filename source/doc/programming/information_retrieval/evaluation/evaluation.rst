==========
Evaluation
==========

User happiness is equated with the relevance of search results to the query.

query vs. information need

Precision :math:`P` is the fraction of th eretrieved documents that are relevant.

Recall :math:`R` is the fraction of the relevant documents that are retrieved.

Unranked Evaluation
===================

.. figure:: recall_precision.png

.. figure:: recall_precision2.png


.. figure:: f_measure.png

:math:`\beta` tells us if Precision or recall are more important. F is the harmonic mean. 

increase beta to get more results and higher recall.

Accuracy can't really be used in IR. If you want to maximize accurayc: always say no and return nothing.. Not usefull.

.. figure:: example_f.png

Arithemic Mean can't be used. so we use harmonic mean. We want to punish really bad performance on either precision or recall. 

The red line is the F value: 

.. figure:: f_measure_vis.png

Ranked Evaluation
=================

Precision recall curve
----------------------

Precision/recall/F are measures for unranked sets.

To evaluate ranked lists we just compute the set measure for each "prefix" (top 1, top 2, top 3, top 4 etc.)

Doing this for precision and recall gives you the **precision-recall curve**.

.. figure:: precision_recall_curve_example.png

.. figure:: precision_recall_curve_interpolation.png

ROC curve
---------

.. figure:: roc_curve.png


Benchmark
=========
variance of the same system across queries >> variance of different systems on the same query

What we need for a Benchmark
----------------------------

- collection of documents
- collection of information needs 
- Human relevance assessments (hired judges, judges must be representative of your real users)

Validity of relevance assessments
---------------------------------
assesments by judges are only usable if they are consistent.

Kappa Statiskik um Konsistenz zu testen: 

.. figure:: kappa_measure2.png

.. figure:: kappa_measure.png

.. figure:: kappa_measure_example.png


.. figure:: kappa_measure_example2.png

- A/B Testing


Marginal Relevance
------------------
The marginal relevance of a document at posision :math:`k` is the result list is the additional information
it contributes over the above information that was contained in the documents :math:`d_1, ... d_{k-1`

- Good measure for user happyness: Early in result list, more information. 
- precision recall is misleading, but marginal relevance is good measure: top results have same content and they are all relevant
- difficulty of marginal measures instead of non-marginal measures: How to calculate that two documents have same information


Query Expansion
===============
Query Expansion: improve the retrieval results by adding synonyms/related terms to the query
to improve recall.

- Local

Global
------
- Do a global analysis once (of collection) to produce thesaurus

A database that collects (near-)synonyms is caled a thesaurus.

.. figure:: thesaurus_expansion.png

Automatic thesaurus generation
    - two words are similar if they co-occur wtih similar words (more robust)
    - two words are similar if they occur in a given grammatical relation wtih the same words (more accurate)
