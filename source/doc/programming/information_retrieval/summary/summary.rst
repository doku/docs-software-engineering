=======
Summary
=======

- Inverted Index with lenght, Skip Pointers in Wurzel Länge abständen, Positional Index
- Terms: Lemmatization, Stemming
- phrase Queries: Biword index, 
- Wildcard Queries: Permuterm (rotate so that wildcard is at end) K-gram index

- Levenstein distance


- Block sort-based index (BSBI) : Keeps one dictionary and sorts each block into it
- Single-pass in-memory indexing (SPIMI): build full index for each block and merge it 
- Distributed indexing
- Dynamic indexing logarithmic Merge 
- Term statistics: 
    - Zipfs Law: i th most frequent term has the frequency cf proportional zu 1/i
    - Heaps Law: M = kT^b (M=number of vocabel, T number of tokens)
- Posting Compression: 
    - store gabs of documents, rather than doc id
    - Variable Byte Code: 
    - Gamma Code: 
        - Binary value of : Length + Offset
        - Offset is the value to be saved with the leading bit removed
        - length is the unary represantation of the lenght of the offset (number of letters) with a trailing 0
- Querry Expansion: Thesaurus

Ranking
-------
- TF-idf
    - :math:`idf_t = log_{10} \frac{N}{df_t}`
    - tf: :math:`w_{t,d} = 1 + log_{10}(tf_{t,d})`
    - tf-idf match score: :math:`score(d,q) = \sum_{t\in q \cap d} (1 + log_{10} tf_{t,d}) * log ( \frac{N}{df_t})`
- cos: :math:`cos(q,d) = \frac{q*d}{|q| |d|}`

- document-at-a-time (sort postings by page rank, compute cosine similarity for each document)
- term-at-a-time ?


Evaluation
----------

- Precision: (# relevant items retrieved) / # (retrieved items)
- Recall: (# relevant items retrieved) / # (relevant items) 
- :math:`F = \frac{2PR}{P+R}`
- Precision Recall Curve
- Annotator Konsistenz mit Kappa: :math:``

Prob IR
-------

Classification
--------------

- Naive Bayes: Problem correlated Features
- Evaluation    
    - Macro: compute F value for each class then average
    - Micro: compute F value as aggregat eof TP.. for all classes
    
- Max Entropy

- Feature Selection: Mutual Information

- Vector Space Classification

- kNN - k nearest Neighbor

- Support Vector Machines

- Learn to score

Clustering
----------
- K-means   

Images:
-------

.. figure:: levenstein.png