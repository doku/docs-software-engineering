============================
Introduction to Data Science
============================

.. note::

	Data is what we collect and store. It becomes information once we analyze it.
	It becomes knowledge once we understand it.

Data Science is the analysis of diverse data. Data Science gives you the ability
to take unstructured data and find order, meaning and value. This provides insight
and competitive advantage.

Data Science combines the fields of programming (ability to manipulate data),
math/statistics (ability to make sense of data) and Substantive expertise
(familiarity of working in any particular applied domain)

.. figure:: datascience_fields.png
	 :alt: datascience_fields

The programming gathers and prepares the data. The data has to bee cleaned and
defect datasets have to be removed. With statistics you can choose useful
procedures and diagnose problems.

Data with variability and uncertainty requires statistics.



Ethical Issues of Data Science
==============================

- Privacy
- Anonymity
- Copyright

.. glossary::
  Anonymity
    potential identifiers have been removed from the data.

  Privacy
    data is not shared without consent of the people described in the data.

Algorithms could potentially be bias. The Algorithms are only as neutral as the rules
and data that they are given.

Don't be to confident in your results. Data science analyses are limited simplifications.
So personal judgments is always needed.

Data Science vs. Big Data
=========================
Big Data and Data Science differ. They share some goals and techniques.

Machine Learning
================

The goal of machine Learning is to create a "black box" predictive model.
