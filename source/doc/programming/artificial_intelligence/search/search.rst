==================
Search Algorithmen
==================

Motivation
==========

Search is a core method in AI. 

- Finding a decision sequence towards a goal
- Probabilistically estimating effects of decisions

Decision trees formalize the space of possible future decisions and state transitinos. 

Wie die Knoten in der Fringe sortiret sind, entscheidet, ob es ein 
Tiefensuche, Breitensuche & co ist. 

S_0 = A

fringe <- {A}

loop {
    n <- pop (fringe)
    if (goal_test(n)) -> return n
    expand(n, fringe)
}

Search Strategies
=================
A strategy is defined by picking the **ordering of the fringe**

- Complete: if there is 

- Breadth-First: Fringe is FIFO
- Depth-First: Fringe is LIFO
- Iteratitve Deepening Search: erpeat depth-first for increasing depth limit
- Uniform-Cost: sort fringe by :math:`g`
- A*: sort by :math:`f=g+h`

.. figure:: complexity_search.png

Breadth-first Search
====================

The Fringe is a **FIFO** queue, new children go to the end of queue.

- Complete: Yes if :math:`b` is finite
- Time: :math:`O(b^{d+1}`
- Space: :math:`O(b^{d+1}` (has to keep every node in Memory)
- Opitmal: Yes if cost per step is 1, not Opitmal otherwise

Depth-First Search
==================
The `fringe` is a **LIFO** queue.

- Complete: Yes for m not infinity, (problems with loops)
- Time: :math:`O(b^m)`: terrible if :math:`m` is much larger than :math:`d`
- **Space**: :math:`O(bm)` - linear Space
- Optimal: No

Iterative Deepening Search
==========================

Depth-first search wtih depfh limit :math:`l` - asuming nodes at depth 

.. figure:: iterative_deepening_search.png

Graph Search
============

Bildet man einen Zyklinchen Graphen als Baum ab, so kommt es 
vor, dass ein Zustand durch mehrere Knoten abgebildet wird

A* Search
=========

.. code::

    while (fringe != {}) {
        n = fringe.pop()
        if(goalCheck(n)) return n;
        fringe.insert(Expand(n))
    }

**cost-so-far** and **cost-to-go** (:math:`h(n)`)

Your heuristik :math:`h(n)` should always underestimate the **cost-to-go**.
You can use `h(n) = 0` (:ref:`dijkstra-algorithm`).


.. figure:: a_star_heuristik.png

.. figure:: a_star_optimality_proof.png

.. figure:: a_star_optimality_proof_lemma.png

.. figure:: a_star_optimality_proof_2.png