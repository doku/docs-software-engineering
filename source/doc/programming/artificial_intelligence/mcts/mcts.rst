=======================
Monte Carlo Tree Search
=======================

Bandits
=======

Multi-armed Bandits: There are :math:`n` machines. Each machine :math:`i` returns a reward
:math:`y ~ P(y; \Theta_i)`. The machine's parameter :math:`\Theta_i` is unknown. Your goal is
to maximize the reward collected over the first :math:`T` trials.


- Exploration: You collect more data about the machine (knowledge)
- Exploitation:  You collect reward


Mittelwert 

.. math:: 
    
    Sdv{\mu} = \frac{1}{\sqrt{N}} Sdv{x}

Upper Confidence Bound (UCB)
============================
(in Übung und Klausur)

The UCB algorithm is based on the principle of **optimism in the face of uncertainty**,
which is to choose your actions as if the environment (in this care bandit) is as nice
as plausible possible.

Either the optimism was justified, in which case the learner is acting optimally, 
or the optimism was not justified. In the latter case the agent takes some action 
that they believed might give a large reward when in fact it does not. 
If this happens sufficiently often, then the learner will learn what is the true 
payoff of this action and not choose it in the future.

.. figure:: bandits_formal_definition.png

.. figure:: ucb1.png

UCB over-estimates the rewart-to-go (under-estimates cost-to-go), just like `A*`
but does so in the probabilistic setting of bandits. 

In tree search, the decision which branch/actions to explore is itself a decision
problem.


Monte Carlo Tree Search (MCTS)
==============================

MCTS is very sucessful on Computer Go and other games, rather simple to implement
and very general (applicable to many discrete domains).

The goal of MCTS is to estimate teth utility (e.g. expected payoff :math:`\Delta`)
depending on the action :math:`a` chosen - the **Q-function** where expectation is 
taken with respect to the whole future randomized actions (including a potential opponent)

.. math::

        Q(s_0,a) = E\{\Delta | s_0, a\}



.. figure:: generic_mcts_scheme.png

Upper Confindenc Tree (UCT)
===========================

UCT uses UCB to realize the `TreePolicy` i.e. to decide where to expand the tree. 

Given infinite time an memory UCT converges to MinMax. 

MinMax Algorithmus
==================
Min-Max Prinzip: Der andere Spieler wird optimal spielen 
und unser Gewinn immer versuchen zu minimieren.

Perfect play for deterimnistic, perfect-information games. We 
choose a move to position with highest **minmax values**, the 
best achivable payoff against best play. 

.. figure:: minmax_tree.png

The algorithm uses recursive fuction calls, which effectively do 
DFS (:ref:`depth-first-search`)

.. figure:: minmax_algorithm.png

- **Complete:** Yes if tree is finite (chess has specific rule for this)
- **Optimal:** Yes, against an optimal opponent. Otherwise?
- **Time complexity:** :math:`O(b^m)`
- **Space complexity:** :math:`O(bm)` (depth-first exploration) 

    For chess :math:`b \approx 35`, :math:`m \approx 100` for reasonable games is an exact solution complexity infeasible.
    **But do we even need to explore every path?**

a-b pruning
-----------
We can guarantee that we find the optimal path before we find any
other non optimal path. 

Um einen Wert am Knoten zu berechnen muss man einmal komplett
durch auswerten bis zu einem Blatt. Wenn man einen Knoten 
findet, der kleiner als das bisherige min ist, kann man den 
gesamten Baumteil in "prunen" und weglassen.

Beachte den Unterschied zwischen Min-Max Knoten.

Bei UCT für 2 Spieler Spiele das Vorzeichen flippen.
