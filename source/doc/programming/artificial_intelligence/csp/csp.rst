======================================
CSP (Constraint Statisfaction Problems
======================================

CSPs are **not** sequential dicition Problems. Basic methods address them
however by sequentially making partial decitions. 

We have :math:`n` **variables** :math:`x_i`, each with **domain** :math:`D_i`, :math:`x_i \in D_i`

We have :math:`K` constraints :math:`C_k`, each of which determines the feasible 
configurations of a subset of variables.

The goal is to find a configuration :math:`X = (X_1, ..., X_n)` off all variables that 
satisfies all constraints.


A CSP  corresponds to a **constraint graph**. A constraint graph is a bi-partite graph 
where nodes are variables and boxes are constraints. Constraints may constrain 
several (or one) variables. 

Varieties of CSPs
=================

- discrete variables
- continuous variables
- real world examples

Varieties of constraints
------------------------

- Unary
- Pair-wise
- Higher-order

Methods for solving CSPs
========================

Sequential Assignment Approach
------------------------------


Backtracking sequential Assignment
----------------------------------

Backtracking search is the basic uniformed algorithm for CSPs.

.. figure:: backtracking_pseudo_code.png


Backtracking Efficieny Improvemnts

Variable order
    - Minimum remaining values (MRV): choose the variable with the fewest legal values
    - Degree Heuristic: choose the variable wtih the most constraints on remaining variables
    
Value Order: Least constraining value
    - given a variable, choose the least constraining value: the one that rules out the fewest
      values in the remaining variables. 


Inference
=========

Wissenschaftliche Daten: Problem Annahme welche Variablen es in einer Welt gibt..

Bayesian Networks
=================

- directed acyclic Graph (DAG) 
- gibt aussagen über die Abhängigkeiten von 
- Graphische modelle definieren unabhängigkeiten!

Eine Variable kann immer nur von ihren 

:math:`\pi(i)` sind die Eltern

:math:`P(A|B,C):`
:math:`\sum_A P(A|B,C)=1` (ist normiert gegen A)
:math:`\sum_B P(A|B,C)=?`
:math:`\sum_C P(A|B,C)=?`

:math:`P(A,B)=P(A) P(B|A) = P(B) P(A|B)`

A,B sind conditional unabhängig, wenn :math:`P(A,B) = P(A) P(B)` (Im graphinschen
model 2 knoten A,B unabhängig und ohne Kante.

Gleichungen können immer kondidioniert werden und stimmen dann immer noch: 

.. figure:: konditionierung.png


head-to-tail:
- markov kette: wenn Z fix ist sind x und y unabhängig

indep(B,S|T) ist true

Mit modell versuchen wir zu inferieren, was wir üben den nicht-beobadeten 


Algorithmus Value elimination
=============================
Man bekommt eine liste von Fakturen als input

Man kann auch 
So kann man die Randwahrscheinlichkeit über alle Messages berechenen