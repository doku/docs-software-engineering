=======================
Artificial Intelligence
=======================

.. toctree::
  :glob:

  summary/*
  artificial_intelligence/*
  search/*
  mcts/*
  dynamic_programming/*
  reinforcement_learning/*
  csp/*
  bayes_inference/*
  machine_learning/*
  deep_learning/*
  med/*


Exam
====

Time: 90min

Aufgabentypen
-------------

- A*
- Tree Search complexity
- Tiefensuche & Breitensuche

- Bayes
- Bandits

- MDP
- Value Iteration
- Q Learning

- CSP & Constraint Propagation
- Arc Consistency

- Hidden Markov Modelle


- Bayesian Inference
- Bayes Theorem
- UCB
- Naive Bayes Classifier
- MCTS


Aufgaben die Immer Kommen
-------------------------

- A*
- CSP
- Bayes Inferenz

