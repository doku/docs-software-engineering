=========================
Dynamische Programmierung
=========================

Markov Decision Process
=======================

P(a_0, s_0) = P(a_0 | s_0) P(s_0)
P(s_1, a_0, s_0) = P

Beim Discounding haben wir ein exponential leaking. 
:math:`\gamma` ist der Discounting factor. Discounting is der 
besondere teil des MDP. Bei regelungstechnik wird kein Discounting
verwendet.


Wir makimieren: (das ist unser Return)

.. math:: 
    \sum_{t=0}^{\infy} \gamma^t r_t


Wenn der Zustandsraum diskter ist ist es super einfach die Funktion
als LGS aufzustellen und so die ideale Wertefunktion zu bestimmen.


Wertefunktion
-------------

- :math`V^{\pi}` : value function of :math:`\pi`
- :math`V^*`: optimal value function

:math:`argmax_i v_i` gibt den index :math:`i` des maximalen wertes zurück.


Bellman Optimality Equation
============================

Wenn der Pfad nach A optimal ist 

Wenn man schon weiß wie das opitmale verhalten 

Man nimmt an, dass man sich in s' optimal verhält und 
kann sich dan zurückhangeln, und im schritt zuvor 
den optimalen schritt bestimmen.

Bellman Optimality Equation ist kein LGS.



Value Interation:

4 nested loops

iterieren über k
    über s
        über max a
            über s'

Die Funktion konvergiert da \gamma echt kleiner 1. Wir haben 
eine Kontrahierendefunktion. 

Wertefunktion schreibt jedem Zustand einen Wert zu. 

Der expected Return wenn ich in einem Zustand S bin,
die Aktion A durchführ 


Optimaler Wert = Erwartungswert über alle möglichen Zustaände * 

V^*(s) = max_a


Reinforcement Learning
======================

Q-Learning

Bei Reinforcement Learning hat man nur eine einzele erfahrung (s, a, r, s'). 
Jetzt kann mann die Bellman Equation approximieren


Bei Q-Learning ohne Eligabilitie verfahren ist die Konvergenz echt schlecht. 

Eligabilitie Tabellen lassen sich super schlecht mit neuronalen Netzen kombinieren. 
