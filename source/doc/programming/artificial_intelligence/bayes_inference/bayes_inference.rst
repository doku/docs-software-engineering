===============
Bayes Inference
===============

A Bayesian Network is :

- directed acyclig Graph (DAG)
- where each node represents a random variable :math:`X_i`
- for each node we have a conditional probability distribution :math:`P(X_i | Parents(X_i))`

.. figure:: conditional_independence_1.png

.. figure:: conditional_independence.png

.. figure:: conditional_independence_2.png


Variable Elimination
====================

.. figure:: variable_elimination.png



Inference
=========

.. figure:: inference_1.png
