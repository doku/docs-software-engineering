=======================
Artificial Intelligence
=======================

What is AI
==========





Human-compatible AI
===================

#. The robot's only objective is to maximize the realization of human values
#. The robot is initially uncertain about what those values are
#. Human behavior provides information about human values.

A machine with this principles is provable beneficial to society.
