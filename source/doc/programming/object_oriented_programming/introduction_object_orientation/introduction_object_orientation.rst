Object Oriented Programming
===========================

Object-oriented design focuses more on the communication between objects than on
the objects themselves.

We try to think about objects in terms of roles, responsibilities and collaborators.
An object is an implementation of one or more **roles**. A role is a set of related
*responsibilities*  and a responsibility is an obligation to perform a task or
know a information. A *collaboration* is an interaction of objects or roles (or both).

We grow our systems a slice of functionality at a time. As code scales up we need to
structure functionality in objects, objects into packages, packages into programs
and programs into systems. We use these principles to guide this structuring.

Three mag

.. glossary::

  Separation of concerns
      we gather together code that will change for the same reason

  Higher levels of abstraction
      The only way we can deal with complexity is to avoid it, by working at a higher
      level of abstraction.

  Ports and adapters architecture
      code from the business domain is isolated from its dependencies on technical
      infrastructure, such as databases and user interfaces.

  Encapsulation
        Ensures that the behavior of an object can only be affected through its API.
        We can ensure that there are no unexpected dependencies between unrelated components.

  Information hiding
          Conceals how an object implements its functionality behind the abstraction of its API.
          It lets us work on a higher abstraction by ignoring lower-level details
          that are unrelated to the task at hand.

.. warning::
    Objects should only contain logic that is necessary for their specific behavior.
    It should usually **not** contain any logic acting on other objects. This logic
    belongs into their class and you should just be able to call a method.

    You should never act on the implementation of an other object.

OO gives you the ability to create one module calling another and yet have the
compile time dependency point against the flow of control instead of with the
flow of control. This gives you absolut control over your dependency structure.

.. figure:: oo_dependency.png
	 :alt: oo_dependency

.. hint::

  OO is about managing dependencies by selectively inverting certain key dependencies
  in your architecture so that you can prevent fragility, rigidity and non-reusability.


Features for an object
----------------------

.. hint:: No and's, Or's or But's

Every object should have a single, clearly defined responsibility (**single responsibility principle**).
We should be able to describe what an object does without using any conjunctions (and, or).
Otherwise the object should probably be broken up into collaboration objects.

.. _OOP_Context:

Object Peer
-----------

The objects an object communicates with directly are its *peers*. We categorize an object's
peers into three types of relationships.

Dependencies
  Services that the object requires from its peers so it can perform its responsibilities.
  The object cannot function without these services.

Notifications
  The object will notify interested peers whenever it changes state or performs a
  significant action. Notifications are *fire and forget*.

Adjustments
  Peers that adjust the object's behavior to the wider needs of the system.
  This includes policy objects that make decisions on the object's behalf (the Strategy Pattern)

Notifications are one-way. A notification listener my not return a value, call back the caller or
through an exception, since there may be other listeners further down the chain.

A dependency  or adjustment, on the other hand, may do any of these since it is a
direct relationship.

.. attention::

  Composite simpler than the sum of its parts
    The composite object's API must hide the existence of its component paers and
    the interactions between them, and expose a sompler abstraction to its peers.

A system is easier to change if its objects are *context-independent*, that is if
each object has no built-in knowledge about the system in which it executes.

A caller wants to know what an object does and what it depends on, but not how it works.

In OOP we view a system as a web of communicating objects, so we focus our design effort on
how the objects collaborate to deliver the functionality we need. We can use interfaces
to define the available messages between objects.

.. hint::
  **Interface and Protocol**
    an interface describes whether two components will *fit* together, while a
    protocol describes whether they will *work* together.

Values and Objects
------------------

*Values* are immutable, so they are simpler and have no meaningful identity.
*Objects* have state, so they have identity and relationships with each other.

**Breaking out**
  When the code in an object becomes to complex, that's often a sign that we should
  break it out coherent units of behavior into helper types.

**Bundling up**
  If a group of values is always used together, it is often a hint that there is a
  missing structure. We should introduce a new type and hide its behaviour behind a clean interface.

**Budding of**
  When implementing an object we discover that it need a sevice to be provided by another
  object. We gove the new service a name and mock it out in the client object's unit tests,
  to clarify the relationship between the two. Then we write an object to provide that service and in
  doing so, discover what services that object needs. We follow this chain pf collaborator relationships until
  we connect up to existing objects.

  This is called "on-demand" design: we "pull" unterfaces and their implemantaions
  into existance from the needs of the client, rather than pushing out the features that we think a class
  should provide.


.. hint:: Identify Relationships with Interfaces


We organize our code in two layers: an *implemention layer* which is the graph of objects,
its behavior is the combined result of how its objects respond to events, and a *declarative layer*
which build up the objects in the implementation layer, using small helper methods.
The declarative layer describes what the code will do, while the implementation layer
describes how the code does ist.

In the declarative layer readability is king. In the implementing layer we use standard
OOP technices.

We aspire to rise ourselves from programming in terms of control flow and data manipulation,
to composing programs from smaller programs, where objects form the smallest unit of behavior.

An object's type is defined by the roles it plays.

Building on Third-Party-Code
----------------------------

The critical point about third-party code is that we don't control it, so we cannot
use our OOP process to guide its design. Instead, we must focus on the integration
between our design and the external code. We must find the best balance between elegance and
practical use of someone else's ideas.

You should always provide your own wrapper hiding all third-party code from your own
implementation. This protects you from future changes of the library code and enables
you to change the library in use, just by modifying your wrapper implementation.

Best Practices
--------------
One goal of object orientation as a technique for structuring code is to make the
boundaries if an object clearly visible.
An object should only deals with values and instances that are either local - created
and managed within its scope - or passed in explicitly.

.. hint:: Clients should not be forced to depend upon interfaces that they do not use.

.. danger:: There are always uncomfortable but necessary compromises. The longer we
      leave them in the code, the more likely it is that some brittleness in the design
      will cause us grief.

.. attention:: Make clear which methods have side-effects or are destructive! Never give them a
      nondestructive method name like get, find etc.

.. attention:: Domain Types are better than Strings!

Bloated Constructors
~~~~~~~~~~~~~~~~~~~~
Bloated Constructors are a hint that a couple of the argument objects should be
grouped as a new Object. When extracting implicit components, we start by looking for two
conditions: arguments that are always used together in the class and those that
have the same lifetime. Once we have found a coincidence, we have the harder task
of finding a good name that explains the concept. One sign that a design is developing
nicely is that this kind of a change is easy to integrate.

Another diagnosis for a bloated constructor might be that the object itself is too
large because it has too many responsibilities.

Such classes are confusing to use because unrelated features interfere with each other.
If we can break up the class into slices that do not share anything, it might be best
to go ahead and slice up the object too.

A constructor should only require necessary dependencies. Notifications (such as
listeners) and adjustments should be set to save defaults and then be adjusted through
the objects interface.
