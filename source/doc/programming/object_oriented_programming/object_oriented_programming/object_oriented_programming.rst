.. _oo-programming:

=================================
Objekt Orientierte Programmierung
=================================

Kurzdefinitionen
================

.. glossary::

  Objektorientierung
    Unter Objektorientierung versteht man eine Sichtweise auf komplexe
    Systeme, bei der ein System durch das Zusammenspiel kooperierender
    Objekte beschrieben wird.

  Objekte und Nachrichten
    Ein Objekt repräsentiert aus fachlicher Sicht einen Gegenstand der Anwendung mit
    allen seinen Eigenschaften. Technisch werden die Dienste, die das Objekt
    anbietet, durch Methoden beschrieben; die Daten, die das Objekt und seinen
    Zustand charakterisieren, werden in Attributen gespeichert. Objekte kommunizieren
    miteinander, indem sie Nachrichten versenden und empfangen. Hat ein Objekt eine
    Nachricht empfangen, wird die Methode ausgeführt, die die Klasse dieses Objekts
    für diese Nachricht bereithält.

  Objekte/Instanzen
    Instanzen sind lebendige Objekte, die nach dem Bauplan einer Klasse
    erstellt wurden. Von jeder Klasse kann es beliebig viele Instanzen
    geben, die alle die gleichen Eigenschaften haben und das gleiche
    Verhalten aufweisen, aber unterschiedliche Daten enthalten können.

  Klassen
    Eine Klasse ist die Abstraktion, also der Datentyp eines Objekts. Sie
    definiert welche Eigenschaften/ Attribute und welches Verhalten ein Objekt besitzt.

    Jedes Objekt ist ein Exemplar (eine Instanz) genau einer Klasse. Sie definiert
    die Attribute der Objekte und ihre Methoden. Klassen können sich selbst aber
    auch wie Objekte verhalten, d.h. sie können Nachrichten empfangen und versenden.
    Dementsprechend besitzen auch Klassen eigene Attribute und Methoden.
    Wir unterscheiden zwischen Objekt- und Klassenattributen sowie zwischen
    Objekt- und Klassenmethoden.

  Generalisierung
    Die Generalisierungsbeziehung besteht zwischen Klassen, die damit hierarchisch
    organsiert werden. Ist die Klasse A eine Generalisierung der Klasse B, dann
    wird A als Ober- und B als Unterklasse bezeichnet. Die Unterklasse hat
    grundsätzlich alle Eigenschaften, die die Oberklasse besitzt. Sie kann
    weitere (spezielle) Eigenschaften hinzufügen und auch Eigenschaften der
    Oberklasse überschreiben (redefinieren).

  Vererbung
    Vererbung ist eine Beziehung zwischen zwei Klassen. Die erbende Klasse
    übernimmt dabei die Eigenschaften der beerbten Klasse und kann diese
    anschließend verfeinern. Die Vererbung ist eine ``is a`` Beziehung
    (Generalisierungsbeziehung) oder besser eine ``is substitutable for``
    Beziehung.. Die Kindklasse stellt eine Spezialisierung
    dar, die Vaterklasse ist eine Generalisierung.

  Methoden (Routinen)
    Methode ist der Überbegriff für Prozeduren und Funktionen, die Teil
    einer Klasse sind. Die Methoden definieren das Verhalten der Objekte.

  Funktionen
    Funktionen sind Methoden mit Rückgabewert.

  Prozeduren
    Prozeduren sind Methoden mit Seiteneffekten. Sie liefern kein Ergebnis.
    Der Aufruf einer Prozedur stellt also kein Ausdruck dar, sondern eine
    Anweisung. Eine Prozedur hat daher immer den Rückgabewert ``void``.

  Attribute
    Attribute sind Variablen, die zu einem Objekt gehören und seinen Zustand
    beschreiben.

  Polymorphie
    Polymorphie (griechisch für Vielseitigkeit) ist ein Konzept in der
    objektorientierten Programmierung, das ermöglicht, dass ein Bezeichner
    abhängig von seiner Verwendung verschiedene Datentypen annimmt. Eine
    Zuweisung ist Polymorph, wenn Variable und Wert verschiedene Datentypen
    haben (Subklasse statt Superklasse).

  Dynamisches Binden
    Dynamisches Binden ist die Eigenschaft, dass eine Ausführung eines
    Methodenaufrufes diejenige Version der Methode verwendet, die am besten
    auf die Typ des Zielobjekts angepasst ist.

  Interfaces
    Ein Interface ist eine von der Implementierung **losgelöste**
    Schnittstelle. Eine Klasse kann zusätzlich zu einer Superklasse beliebig
    viele Interfaces implementieren und damit etwas ähnliches wie
    Mehrfachvererbung darstellen.

  Datenkapselung
    Bei der Datenkapselung wird der Zugriff von außen auf Attribute und
    Methoden von Objekten durch Sichtbarkeitsmodifizierer beschränkt.

    -  Datenstrukturen sind vor Fehlinterpretation geschützt und Missbrauch
       wird erschwert.
    -  Die gekapselte Datenstruktur kann ohne Auswirkungen auf andere
       Klassen geändert werden.

  Abstrakte Klassen
    Wird die :term:`Generalisierung` konsequent eingesetzt, dann entstehen
    Klassen, die nur abstrakte Konzepte oder Begriffe repräsentieren. Diese
    werden als abstrakte Klassen bezeichnet.

    Charakteristisch für abstrakte  Klassen sind Methoden, die nicht *definiert*
    (realisiert), sondern nur durch Angabe der :term:`Methodensignatur` spezifiziert
    sind. Diese werden :term:`abstrakte Methoden` genannt.

    Eine abstrakte Klasse kann nicht instanziiert werden und dient als
    Strukturelement in einer Klassenhierarchie.

    Besitzt eine Klasse eine abstrakte Methode, so muss diese Klasse
    ebenfalls abstrakt sein. Es können jedoch abstrakte Klassen ohne
    abstrakte Methoden existieren.

  Abstrakte Methoden
    Eine abstrakte Methode bietet keine Implementierung und definiert damit
    nur die Schnittstelle. In der **nicht abstrakten** Kindklasse muss diese
    dann zwingend implementiert werden.

  Statische Methoden
    Statische Methoden können direkt auf der Klasse aufgerufen werden.
    Nicht-Statische Methoden benötigen ein Objekt, um aufgerufen werden zu
    können.

    Statische Methoden sollten keine Eigenschaften der Objekte ändern und
    nur von den Parametern abhängig sein.

    Statische Methoden brechen eigentlich das objektorientierte Paradigma,
    dass alle Daten und alles Verhalten durch Objekte repräsentiert wird.

  Aggregationsbeziehung
    Die Aggregationsbeziehung modelliert eine Teile-Ganzes-Beziehung zwischen
    Objekten. Mit der Aggregationsbeziehung können Objekte hierarchisch strukturiert
    werden.

    Man unterscheidet bei Teile-Ganzes-Beziehungen, ob ein Teil in mehreren
    Ganzen enthalten sein kann, oder ob ein Teil ausschließlich einem Ganzen zugeordnet.
    Den letzteren Fall, die restriktive Form der Aggregation, bezeichnet man auch als
    **Komposition**.

  Sichtbarkeiten

    .. figure:: Sichtbarkeiten.png
       :alt: Sichtbarkeiten Java

       Sichtbarkeiten Java

  Klassenschnittstelle
    Als Klassenschnittstelle bezeichnet man die Menge aller als ``public``
    gekennzeichneten Methoden und Attribute einer Klasse.

  Kontrakte
    Kontrakte von Objekten beschreiben, was das Objekt vom Aufrufer fordert
    und was es dafür als Leistung zusichert. Kontrakte können prinzipiell
    aus drei Teilen bestehen: **Vorbedingung**, **Nachbedingung** und
    **Invarianten**. Vor- und Nachbedingung werden dabei für Methoden
    definiert, Invarianten für die ganze Klasse.

    Die **Vorbedingung** einer Methode beschreibt, was die Methode erwartet,
    dass vor ihrer Ausführung gilt. Die **Nachbedingung** beschreibt, was
    die Methode zusichert, dass nach ihrer Ausführung gilt, falls die
    Vorbedingungen eingehalten werden. Die **Invariante** muss immer erfüllt
    sein.

  Entitäten
    Man definiert eine **Entität** als einen Bezeichner, der Laufzeitwerte
    repräsentiert. Eine Entität ist entweder ein Attribut, eine lokale
    Variable, ein Parameter oder das Schlüsselwort ``this``.

  Methodensignatur
    Zur Methodensignatur gehört der Name, der Rückgabetyp und die
    Parameterliste der Methode. Zusätzlich gehört auch die Sichtbarkeit,
    ``static`` und die geworfenen Exceptions zur Signatur.

  Override vs. Overload
    Beim überladenen Methoden gibt es mehrere Methoden mit gleichem Namen
    aber verschiedener Signatur. Ihre Parameterlisten müssen sich
    unterscheiden. Es kann auf jede der überladene Methoden zugegriffen
    werden. Je nach Parameterliste des Aufrufes wird die richtige Methode
    ausgewählt.

    Beim Überschreiben wird eine geerbte Methode durch eine neue
    Implementierung ersetzt. Diese hat die gleiche Methodensignatur und den
    gleichen Rückgabewert. Überschreibende Methoden werden mit ``@Override``
    gekennzeichnet.


Besonderheiten
~~~~~~~~~~~~~~

-  Diamantproblem
