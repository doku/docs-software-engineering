===========
Programming
===========


.. hint::

  **This 1500 page software engineering and startup guide is maintained by Fabio Schmidberger.**
  
  **It covers everything I learned during my software engineering and entrepreneurship study**

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   object_oriented_programming/index
   search_engine/*
   web/index
   development_tools/index
   programming_languages/index
   project_management/index
   embedded_systems/index
   database/index
   big_data/index
   data_science/index
   security/index
   artificial_intelligence/*
   cloud/index
   notes/*
   systemkonzepte_programmierung/index
   programmentwicklung/index
   modellierung/*
   aas/*
   programmierparadigmen/*
   system_administration/index
   android/index
   performance_tuning/*
   blockchain/*
   information_retrieval/*
   mci/*
