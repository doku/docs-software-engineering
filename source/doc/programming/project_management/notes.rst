=====
Notes
=====

Nur Aufgaben verteilen, bei denen man die Lösungsmöglichkeit kennt und helfen kann.
Hat man selbst keine Lösungsidee sollte man zuerst selbst nachforschen oder offen
sagen dass man keine eigene Idee hat.

If you can avoid doing work that doesn't directly result in business value, it's
to your organizations benefit not to do that work.

Neue Aufgaben starte bevor die alten fertiggestellt wurden?

Email antworten wenn dies nur dazu dient um zu signalisieren dass Mail gelesen
wurde und bearbeitet wird
