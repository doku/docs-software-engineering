================
Embedded Systems
================

.. toctree::
  :glob:

  introduction/*
  automotiv/index
  iot/index
