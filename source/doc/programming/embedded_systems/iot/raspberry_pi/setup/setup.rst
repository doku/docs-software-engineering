Raspberry Pi Setup
==================

1.  Download Raspbian
2.  Extract the ``.zip``
3.  Use Win32Diskimager: Select your extracted Raspbian Folder and your SD Card
    Device Letter. Then click write

Connect the Pi to Power, a Monitor and a Keyboard

Activate SSH and VNC under the ``Raspberry Pi Configuration / Interfaces``
and change the keyboard settings to your locale.

Remote Desktop einrichten
-------------------------

Siehe auch hier http://www.gieseke-buch.de/raspberrypi/vom-windows-pc-per-remotedesktop-den-raspberry-pi-desktop-steuern

Vorinstallierten RealVNC entfernen

.. code-block:: bash

    sudo apt-get purge realvnc-vnc-server

XRDP Instalieren

.. code-block:: bash

  sudo apt-get install xrdp


Remote Server starten

.. code-block:: bash

  Starting Remote Desktop Protocol server: xrdp sesman

Damit ist der

Putty einrichten
-----------------
Raspberry PI Installationen

Git und JDK sind bereits vorinstalliert

- Synergy https://cdn-learn.adafruit.com/downloads/pdf/synergy-on-raspberry-pi.pdf


GPIO Pins verwenden

Dateien in Autostart



Diskpart
--------

.. code::

    list disk

    select disk 7

    clean

    create partition primary


Raspberry Pi Zero over USB
---------------------------

http://blog.gbaman.info/?p=791

You can connect with putty or if remote desktop is installed with that.


Connect via SSH with ``raspberrypi.local``
