=============
Basissoftware
=============

Komponenten werden als Schichtenmodell

Service Layer

Communications-Stack
====================

Typischer Pfad aus Applicationssicht
------------------------------------

RTE übersetzt den ``RTE_WRITE`` Call und ``Com`` Modul puffert die Daten.

Watchdog
========

Watchdogmanager ermöglicht Modellierung von Checkpoints.

Memory Services
===============

Diagnose
========

Anfragen von Extern auf ein Steuergerät und es werden Antworten erwartet.

Reprogrammierung und Updates der Steuergerätssoftware

Flashbootloader entfernt die alte Version und updated zum aktuellsten Stand.

Hardware
========

I/O Hardware Abstraction


OS
===

Sehr einfaches aber extrem schnelles (ideal für kritische Echtzeitanwendungen) auf
OSEK/OS Spezifikation.

Das OS ist dadurch sehr kein und kann auf bis zu unter 16kB gebracht werden.
