================
Autosar Adaptive
================

Automotive Megatrends

- Automated Driving
- Connectivity (IoT)
- Electrification

.. todo:: SAE Level für  Fahrassistenz

Koexistenz von Classic AUTOSAR und Adaptive AUTOSAR beider Architekturen im
Fahrzeug. Classic ist für Echtzeitanwendungen zuständig.
