==================
Einführung AUTOSAR
==================

AUTOSAR (AUTomotive Open System ARchitecture) ist ein internationaler Standard
der Automobilindustrie. Er beschreibt eine offene und standardisierte Softwarearchitektur
für die Fahrzeugentwicklung, die gemeinsam von Automobilherstellern,
Automobilzulieferern und Werkzeugherstellern entwickelt und getragen wird.

AUTOSAR möchte einen Paradigmenwechsel in der Automotivsoftwareentwicklung herbeiführen:
weg von einem steuergerätezentrierten Ansatz und hin zu einem systembasierten Ansatz.

Ziel ist die Entwicklung einer Referenzarchitektur für Steuergerät-Software, mit der
die wachsende Komplexität der Software in modernen Fahrzeugen beherrscht werden kann.

Basiselemente der AUTOSAR-Architektur sind unter anderem formal definierte
Softwarekomponenten (SWC) mit eindeutig spezifizierten Schnittstellen zur
Basissoftware (BSW), die wiederum grundlegende Standarddienste bereitstellt,
wie z.B. Buskommunikation, Speicherverwaltung, IO-Zugriff, System- und
Diagnosedienste. Ein weiteres Basiselement ist die Ablaufumgebung RTE
(Runtime-Environment), die die SWCs mit der BSW verbindet.

Der von AUTOSAR spezifizierte Virtual Functional Bus (VFB) liefert den
konzeptionellen Unterbau für die Kommunikation von SWCs untereinander und die
Nutzung von BSW-Diensten. Die Entwicklung der SWCs basiert auf dem VFB, dadurch
sind die SWCs unabhängig von der Steuergeräte-Hardware. Sie lassen sich damit
einfacher wiederverwenden und in verschiedenen Projekten auf unterschiedlichen
Plattformen einsetzen. Realisiert wird der VFB für ein konkretes Fahrzeug, indem
für jedes Steuergerät eine spezifisch konfigurierte RTE in Verbindung mit der
passend konfigurierten BSW zum Einsatz kommt.

Die Entwicklung von Steuergeräten erfolgt nach der AUTOSAR-Methodik:

Beim Systementwurf wird die Architektur der Funktionssoftware festgelegt. Dies
geschieht durch die Definition von SWCs und deren Verteilung auf die
Steuergeräte. Auch die Netzwerkkommunikation wird in diesem Schritt festgelegt.
Das Ergebnis ist die System Description – eine AUTOSAR XML Datei, aus der für
jedes Steuergerät ein spezifischer ECU Extract of System Description erzeugt wird.

Während der Steuergeräteentwicklung werden die SWCs entworfen und implementiert,
sowie die BSW und die RTE konfiguriert. Durch die Konfiguration bestimmt der
Entwickler den für sein Projekt benötigten Umfang der Basissoftware. Dadurch
kann er die gesamte Steuergeräte-Software optimieren. Als Ergebnis der Konfiguration
erhält er eine ECU Configuration Description (AUTOSAR XML Datei), die auf den
ECU Extract of System Description abgestimmt ist.

Basierend auf der ECU Configuration Description wird durch den Einsatz von
Codegeneratoren die Basissoftware für die Steuergeräte-Software erzeugt bzw.
angepasst. Auch die RTE wird steuergeräte-spezifisch erzeugt.

Durch diese in AUTOSAR definierte Methodik wird die Integration der
Anwendungssoftware in ein Steuergerät wesentlich erleichtert. Eine manuelle
Anpassung der Software entfällt.

Basisfunktionen werden wiederverwendet und jeder Zulieferer kann sich auf
seine Spezialgebiete konzentrieren. Die Basisfunktion wurde ausgelagert und
man kann sich auf die Wettbewerbsfunktionen fokussieren.

.. attention::
    **Reusability** of functions across vehicle networks and across OEM boundaries.

Autosar standardisiert Interfaces und Schnittstellen

Fokus auf Funktionsorientierte Entwicklung (Was soll mein Fahrzeug leisten?) und
erst dann hardwarenahe Entwicklung.

.. note:: Cooperate on standards - compete on implementation

Moderne Steuergeräte sind permanent an die Batterie angeschlossen und werden nicht mehr
erst durch die **Klemme15** (Zündschlüssel) gestartet.

.. glossary::

  Basis Software
    Autosar spezifizierte Basislayer

  Software Components
    SWC

  Virtual Function Bus
    Abstrahiertes Bussystem

  Composition Components
    Container for many SWC

Ports
=====

Sender/Reciever Port (S/R)
--------------------------

Nach Fire and Forget, Informationsverteilung ohne großes
Feedback und Komplexität, Auf Serverseite muss für jeden Client genau eine Runnable
laufen, welches den Trigger empfängt.

Client/Server Port (C/S)
-------------------------
Möglichkeit von Rückgabewerten/Rückmeldung. Entspricht einem
Funktionsaufruf

Communication auf :math:`n:1` Basis.

Der Client ruft mit dem ``RTE_CALL`` den Server auf.

Serverrunnables müssen nicht immer gemappt werden und können so im Kontext des
Aufrufers ausgeführt werde.

Bei asynchronen Aufrufen setzt der Client

Asynchronität besonders für extrem langlaufende Aufrufe (Cryptographie)

Autosar RTE
===========

Die RTE verbindet die klar definierten Schnittstellen von Applikationslayer und den
Basissoftwareschickten.

Die RTE ist sehr spezifisch für jedes Steuergerät, sogar für jeden Entwicklungsstand
des Steuergeräts.

Die RTE wird komplett konfiguriert, der Code selbst wird aber vollständig automatisch
generiert.

Die RTE abstrahiert Betriebssystem, Kommunikationssysteme und Hardwareinterfaces.

Die Runmables werden durch die RTE getriggert.

Die RTE stellt die Kommunikationsimplementierung zwischen den Komponenten dar.

Eventbasierte Entwicklung kann Probleme bei der Vorhersagbarkeit des Systems hervorheben.

RTE bietet einen Communications mechanismus zwischen SWCs und zu der BSW

Da die RTE zum Zeitpunkt der genierierung sehr genaue Kenntnisse über Hardware etc.
hat lässt sich eine Automatisierte Optimierung durchführen.

Complex Device Driver
======================

Möglichkeit Spezialfunktionen abzubilden die mit dem bisherigen Stack nicht abgebildet
werden können (Funktionen die in Autosar nie so vorgesehen war)

Sehr zeitkritische Funktionen können hier ausgelagert werden (Motorsteuerung).

Software-Component (SWC)
========================

Beschreibung der Komponente als **Software-Component Description**.

Dadurch paralleles arbeiten von Integrator, Entwickler und Tester.
