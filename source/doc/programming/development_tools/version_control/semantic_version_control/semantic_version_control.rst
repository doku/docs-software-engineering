========================
Semantic Version Control
========================

A semantic version control tool saves for example the process of renaming a
variable instead of the pure textual difference of the files. Therefore
it is able to apply these semantic changes to other code when merging. 
