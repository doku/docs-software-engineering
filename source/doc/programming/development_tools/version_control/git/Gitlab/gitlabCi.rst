GitLab CI
==========

GitLab CI mit Submodulen
------------------------

Im Gitlab CI Prozess kann nur ``HTTPS`` Authentifikation verwenden. Da man aber
meist lokal ``SSH`` verwendet, muss der Link zu den git-Submodulen **relativ**
eingebunden werden. Damit ist es möglich beide Authentifikationsmechanismen
für das Submodul zu verwenden.

.. code::

  git submodule add /relativePath/on/GitLab /subfolder/Submodule/willBe/placedIn

Möchte man beispielsweise das Repository ``sphinx-custom-theme`` der Gruppe ``doku``
in dem Unterordner ``source`` einfügen, muss dieses Commando ausgeführt werden:

.. code::

   git submodule ../../doku/sphinx-custom-theme source/sphinx-custom-theme

Git legt dann eine ``.gitmodules`` Datei an, die folgendermaßen aussehen kann:

.. code::

  [submodule "source/sphinx-custom-theme"]
	path = source/sphinx-custom-theme
	url = ../../doku/sphinx-custom-theme

Ist das Submodule relativ eingebunden muss die ``.gitlab-ci.yml`` nur minimal
angepasst werden.

Bei den ``variables`` muss angegeben werden, dass die Submodule geklont werden
sollen.

.. code::

  variables:
    GIT_SUBMODULE_STRATEGY: recursive


Script Ausführung
-----------------

Um Script Dateien in der Gitlab CI auszuführen muss mit ``./scriptfile.sh`` gekennzeichnet
werden, dass es sich um eine Datei handelt die aufgerufen werden soll
und nicht um ein Command.

Der vorangestellte ``.`` bei ``. ./scriptfile.sh`` ist ein alias für ``source``.
Dadurch wird Zeile für Zeile aus der Datei interpretiert, als ob sie von Hand
auf der Console eingegeben worden wären.

.. code-block:: yaml
  :linenos:
  :emphasize-lines: 4

    maven-build:
      image: maven:3-jdk-8
      stage: build
      script: . ./maven_runner.sh
      artifacts:
        paths:
          - Uebungsblaetter/Uebungsblatt[0-9][0-9]/projects/Ex*/target/*.jar


Ausführrechte für Skripte:

.. code-block:: bash

    git update-index --chmod=+x *.sh


Um die Test coverage in Gitlab anzeigen zu können muss dieser Guide befolgt werden:

https://medium.com/@kaiwinter/javafx-and-code-coverage-on-gitlab-ci-29c690e03fd6

Auto Merge Requests
-------------------
CI Process can automatically create WIP Merge Requests when pushing to a new branch.

https://about.gitlab.com/2017/09/05/how-to-automatically-create-a-new-mr-on-gitlab-with-gitlab-ci/a

Gitlab Pages
------------

Um Gitlab pages zu verwenden muss im CI Prozess eine ``index.html`` in den ``public``
Ordner gefügt werden. Diese wird dann angezeigt, wenn man die gitlab pages url
aufruft.

Der Job der ``pages`` heißt, triggert dann automatisch einen weiteren von Gitlab
vor konfigurierten Job ``pages:deploy`` welcher all diejenigen Dateien auf
Gitlab-pages veröffentlicht, die sich beim Fertigstellen des Jobs ``pages``
im ``public`` Verzeichnis befinden.


Eigene GitLab Ci Runner
-----------------------

For installation: 

- install docker: https://docs.docker.com/install/linux/docker-ce/ubuntu/
- install runner: https://docs.gitlab.com/runner/install/linux-repository.html


Eigene Gitlab CI Runner können leicht installiert werden. Dazu wurde eine vor konfiguriere
VM erstellt. Es muss lediglich die ``config.toml`` angepasst werden. Diese findet
man unter debian bei ``etc/gitlab-runner/config.toml``

.. code-block:: bash

  sudo gitlab-runner start

  sudo gitlab-runner stop

Android Emulator
^^^^^^^^^^^^^^^^

``privleged = true`` to give containers access to kvm.

.. code-block:: yaml

  concurrent = 3
  check_interval = 0

  [[runners]]
    name = "server-s-native"
    url = "....."
    token = "......"
    executor = "docker"
    [runners.docker]
      tls_verify = false
      image = "alpine"
      privileged = true
      disable_cache = false
      volumes = ["/cache"]
      shm_size = 0
    [runners.cache]


Gitlab CI Docker build
----------------------

https://docs.gitlab.com/ce/ci/docker/using_docker_build.html


Gitlab CI mit Selenium
----------------------

https://codereviewvideos.com/course/your-own-private-github/video/running-selenium-with-docker-during-virtualbox-test-runs

https://codereviewvideos.com/course/your-own-private-github/video/how-to-use-virtualbox-as-a-gitlab-ci-test-runner


Tests in Gitlab CI
-------------------

Split Performance and Unit Tests

https://semaphoreci.com/community/tutorials/how-to-split-junit-tests-in-a-continuous-integration-environment


Guides
------

Auto Create Merge request from new Branch:

https://rpadovani.com/open-mr-gitlab-ci
