==============
Advanced Usage
==============

Short Cuts
==========

https://docs.gitlab.com/ce/workflow/shortcuts.html

Press ``Strg Enter`` to submit your comment on a issue

Commands
========

Slash Commands
==============

``/estimate 1d 2h`` for a time estimate in an issue. Do not surround the time value
with ``< >``.
