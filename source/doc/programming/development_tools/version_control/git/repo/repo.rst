====
Repo
====

Repo is a repository management tool build on top of git.

.. figure:: cheatsheet.png
	 :alt: cheatsheet

Installation
------------

https://source.android.com/source/downloading

Setup
-----

http://www.instructables.com/id/Using-Googles-repo-command-in-your-own-projects/
