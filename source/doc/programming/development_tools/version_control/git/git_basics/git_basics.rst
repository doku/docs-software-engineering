===
git
===

- Revert
- Branching
- Ignore File
- git Casesensitivity documents -> Changing file names?

great guide: http://learngitbranching.js.org/

Git Commands
============

Git Rename
-----------

Da Windows nicht Casesensitiv ist kann es einige Schwierigkeiten machen casesensitive
Änderungen in ein Repository zu commiten. Mit dem ``git mv`` ist das möglich.

.. code-block:: bash

  git mv -f OldFileNameCase newfilenamecase

Besonders vorsichtig muss man sein, wenn man eine Sphinx doku auf einem Windowssystem
lokal hostet. Da hier nicht auf casesensitivity geachtet wird werden im Browser auch
Bilder angezeigt bei denen eigentlich der Dateipfad falsch angegeben wurde (falsche
Groß-Kleinschreibung). Werden die Änderungen dann gepushed fehlen meist die Bilder,
da die Linuxumgebung casesensitiv ist.


git add
-------

You can add files using Patterns:

This Example adds content from all ``*.txt`` files under Documentation directory and its subdirectories:

.. code-block:: bash

    $ git add Documentation/\*.txt

New Git repo form subfolder
---------------------------

https://help.github.com/articles/splitting-a-subfolder-out-into-a-new-repository/



Git Submodule
-------------

Um ein Neues Submodul hinzuzufügen muss folgender Befehl verwendet werden.

.. code-block:: bash

  git submodule add <Submodule-URL> <path/to/saveLocation/submoduleFolder>

Alle Dateien aus dem Submodul werden dann im Ordner ``submoduleFolder`` eingefügt.
Es bietet sich daher an den ``submoduleFolder`` so zu nennen wie das Repository
des Submodules selbst.

Beim clonen eines Repositories mit Submodulen müssen 2 Befehle ausgeführt werden
um auch das Submodul zu clonen.

.. code-block:: bash

  git submodule init

  git submodule update


Git submodule update from remote

.. code-block:: bash

  git submodule update --recursive --remote


Git initial clone with submodules

.. code-block:: bash

  git submodule update --init --recursive


submodul updaten
~~~~~~~~~~~~~~~~

Im Hauptrepo folgenden Befehl durchführen:

.. code-block:: bash

  git submodule update --remote subModulName


Pullen eines Repositories, in dem die verweise auf geupdate Submodule enthalten ist
führt nicht dazu dass die submodule direkt geupdated werden, dazu muss dann noch separat
nach ``git pull`` der Befehl ``git submodule update`` aufgerufen werden.

Der Befehl ``git submodule update`` updated das Submodul auf die aktuelle Version, die
im Mutter-Repository als letztes Commited wurde. Mit dem Zusatz ``--remote``
lässt sich das Submodul auf den aktuellen Stand des submodul remotes setzen.
So lassen sich Änderungen vom upstream nachziehen.

Undo last commit
----------------
Undo last commit and keep the changes of this commit in the working directory.

.. code-block:: bash

  git reset HEAD~


submodule entfernen
~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

  git submodule deinit asubmodule

  git rm asubmodule

Git Branch
-----------

Branch löschen

.. code-block:: bash

  $ git push origin --delete <branch_name>

  $ git branch -d <branch_name> #delete locally

Neue Branch anlegen, die auf älterem commit basiert:

.. code-block:: bash

  git checkout -b oldState a9c146a09505837ec03b

Dieser Command legt eine neue Branch ``oldState`` an, die auf dem Commit ``a9c146``
basiert und wechselt direkt.

.. code-block:: bash

  git branch oldState a9c146a09505837ec03b

Legt die gleiche Branch wie oben an, wechselt aber nicht zu dieser.

Git Development Enviorment
==========================

git fancy logg
---------------
.. code-block:: bash

    git config --global alias.logg "log --graph --decorate --oneline --abbrev-commit --all"


``cmd> cls`` to clear screen


Git SSH
-------

Mit SSH Agent muss man nur einmal beim Starten der Bash das Passwort zum Key eingeben
man bleibt dann eingeloggt.

Eine Anleitung findet man hier:
https://help.github.com/articles/working-with-ssh-key-passphrases/

.. attention::
   Wird das Script aus unerklärlichen Gründen nicht erkannt, ist es ratsam andere
   git Config Files zu entfernen, da diese möglicherweise die ``ssh-agent`` Änderungen
   überschreiben.
