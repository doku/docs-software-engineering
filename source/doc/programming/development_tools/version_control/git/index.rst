git
====

Git is a free and open source distributed version control system designed to
handle everything from small to very large projects with speed and efficiency.

Here you can find great git-Documentation_.

.. _git-Documentation: https://git-scm.com/book/en/v2

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   git_basics/*
   repo/*
   Gitlab/index
   github/*
