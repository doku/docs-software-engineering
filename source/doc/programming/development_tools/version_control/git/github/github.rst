======
Github
======


Issue management
================

Waffle.io
---------
connect PR to Issue in a single Ticket using ``connect to`` Keyword.
For more details: https://help.waffle.io/faq/waffle-workflow/use-waffles-connect-keyword-to-connect-prs-to-issues

Zenhub
------
https://www.zenhub.com/
