=======
Eclipse
=======


Shortcuts
=========

.. csv-table::

	"``Alt Arrow``", "um Zeilen hoch oder runter zu verschieben"
	"``CTRL T``", "bei ausgewählter Interface/Vaterklassen Methode, zeigt alle überschreibenden Implementierungen an."
	"``CTRL O``", ""
	"``CTRL SHIFT O``", "Organize Imports"


https://shortcutworld.com/en/Eclipse/win/all

http://www.vogella.com/tutorials/EclipseShortcuts/article.html

Eclipse for multiple Monitors
=============================

Mit ``window -> new window`` lässt sich ein neues Fester des gleichen Eclipse
Workspaces öffnen. Die Dateien beider Ansichten sind synchronisiert. Man kann dann
zwischen den Ansichten wechseln.

.. figure:: newwindow.png
	 :alt: newwindow
