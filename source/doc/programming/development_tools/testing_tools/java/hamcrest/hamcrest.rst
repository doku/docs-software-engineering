Hamcrest
========

.. attention::
  ``assertThat`` is the **only** assertion that should be used.

``assertThat()`` and jMock expectation syntaxes are designed to allow developers
to compose small features in (more ore less) readable descriptions and assertions.

The failure reports give great detail about what went wrong.

.. code-block:: java
  :linenos:

  assertThat(instruments, hasItem(instrumentWithPrice(greaterThan(81))));

  private Matcher <? super Instrument> instrumentWithPrice(Matcher<? super Integer> priceMatcher) {
    return new FeatureMatcher<Instrument, Integer>(priceMatcher, "instrument at price", "price") {
      @Override
      protected Integer featureValueOf(Instrument actual) {
        return actual.getStrikePrice();
      }
    };
  }

This may create more program text in the end, but we are prioritizing expressiveness
over minimizing the source lines.

The only caution with factoring out test structure is that we have to be careful
not to make a test so abstract that we cannot see what it does any more.

https://www.javacodegeeks.com/2015/11/hamcrest-matchers-tutorial.html

http://www.vogella.com/tutorials/Hamcrest/article.html

Predefined Matchers
-------------------

Test for ``null``:

.. code-block:: java
  :linenos:
  :emphasize-lines: 3

  import static org.hamcrest.Matchers.nullValue;

  assertThat(list, is(nullValue()));

.. code-block:: java

    assertThat(s, not(containsString("mangos")));


.. code-block:: java

  @Test
	public void testAddInt() {
		assumeTrue("Fail", System.getProperty("os.name").startsWith("Windows"));

		assertThat(calculator.add(1, 1), is(2));
		assertThat(calculator.add(3, 2), is(not(5.0)));

	}

	@Test
	public void testAddDouble() {
		double result = 5.0;
		double delta = 0.0001;
		assertThat(calculator.add(4.0, 1.0), is(closeTo(result, delta)));
	}
