Junit 4
=======

Junit is a Java test framework. In essence, JUnit uses reflection to walk the
structure of a class and run whatever it can find in that class that represents a
test.

.. figure:: junit_setup.png
   :alt: Setup Prozess bei Junit

Junit treats any method annotated with ``@Test`` as a test case. The method must be
of the return type `void` and have no parameters.


inner Test classes
------------------

To organize your Tests better you can split a Testclass into multiple inner test
classes. All inner classes have to be set to ``static`` and the surrounding needs
the annotation ``@RunWith(Enclosed.class)``.

.. code-block:: java
  :linenos:
  :emphasize-lines: 1-2, 4, 14-15

  @RunWith(Enclosed.class)
  public class SorterTest {

  	public static class EdgeTest {
  		public SimpleList<Integer> list = null;

  		@Test
  		public void testSelectionSort_whenListIsNull_listShouldStillBeNull() {
  			Sorter.selectionSort(list);
  			assertThat(list, is(nullValue()));
  		}
  	}

  	@RunWith(Parameterized.class)
  	public static class paramSorterTest {

  		@Parameters
  		public static Iterable<Object[]> testDate() {
  			return Arrays.asList(new Object[][] { { 0, 0 }, { 2, 4 }, { 1, 2 } });
  		}

  		@Parameter(0)
  		public int value;

  		@Parameter(1)
  		public int doubleValue;

  		@Test
  		public void testSelectionSort() {
  			assertThat(double(value), doubleValue);
  		}

  	}
  }


Theory Methoden
---------------

Mit Test Theorien kann eine Test-Methode überprüfen ob Methoden Invarianten (welche
völlig unabhängig von spezifischen Testdaten gelten sollten) tatsächlich eingehalten
werden. Solche Test Theorien werden mit ``@Theory`` annotiert.

Die Testdaten als sogenannten Datapoints ``@Datapoints`` werden dann automatisch
von Junit den ``@Theory`` Methoden übergeben und der Test wird mit diesen Testdaten
ausgeführt.

Dabei können die Testdaten einzeln also als Datapoint ``@Datapoints`` oder in einer
Iterable Datenstrukturen ``@Datapoints`` welche wiederum die einzelnen Testdaten enthält geliefert
werden.

Diese Testdaten müssen als ``public static`` gekennzeichnet sein.

Vorsicht bei mehreren ``@Theory`` Methoden mit den gleichen Testdaten.
Nach üblichen Java Vorgehen werden nur die Referenzen zu den Testobjekten an die
``@Theory`` Methode weitergegeben. Nur die erste Testmethode die ausgeführt wird
hat also saubere Testdaten. Werden in der ersten Testmethode die Objekte bearbeitet
aber nicht zuvor eine harte Testkopie erstellt, so arbeiten alle anderen Testmethoden
auf den bearbeiteten Testdaten. Je nach Ausführungsreihenfolge kann sich so ungewollter
nichtdeterminismus in die Testing Suite einschleichen und dazu führen, dass Builds
fehlschlagen. Die Testdaten sollten daher in der ``arange`` Phase der Testmethode
auf jeden Fall kopiert werden.

Exception Testing
-----------------

.. code-block:: java
  :linenos:
  :emphasize-lines: 2, 8-9

  @Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void iterator_preorder_whenRemoveIsCalled_shouldThrowUnsupportedOperationException () {
		Iterator<Integer> preorderIterator = tree.iterator(TreeTraversalType.PREORDER);

		expectedException.expect(UnsupportedOperationException.class);
		expectedException.expectMessage("The remove operation is not supported");

		preorderIterator.remove();
	}

jMock
-----

jMock is designed to make the expectation descriptions as clear as possible and
therefor uses some unusual Java coding practices.

The core concepts of the jMock API are the *mockery*, *mock objects* and *expectations*.

- A mockery represents the context of the object under test, its neighboring pbjects
- mock objects stand in for the real neighbors of the object under thest while the test runs
- expectations describe how the object under test should invoke its neighbors during the test


.. code-block:: java
  :linenos:

  @RunWith(JMock.class)
  public class AuctionMessageTranslatorTest {
    private final Mockery context = new JUnit4Mockery();
    private final AuctionEventListener listener = context.mock(AuctionEventListener.class);
    private final AuctionMessageTranslator translator = new AuctionMessageTranslator(listener);

    @Test
    public void notifiesAuctionClosedWhenCloseMessageRecieved() {
      Message message = new Message();
      message.setBody("SOLVersion: 1.1; Event: CLOSE;");

      context.checking(new Expectations() {{
        oneOf(listener).auctionClosed();
      }});

      translator.processMessage(UNUSED_CHAT, message);
    }
  }

In jMock we assemble a description of the expected calls for a test in the context object
called a ``Mockery``. During the test runt , the ``Mockery`` will pass calls made to any of
its mocked objects to its ``Expectations``, each of which will attempt to match the call.
If an ``Expectation`` matches, that part of the test succeeds. If none matches, than each
``Expectation`` reports its disagreement and the test fails.


jMock supports more varied checking of how often a call is made than just ``allowing()``
and ``oneOf()``. The number of times a call is expected is defined by the "cardinality"
clause that starts the expectation.
