Maven
=====

pom.XML
-------

.. todo:: default dependencies

.. todo:: build section



Maven Web App
-------------

generate Project mit maven-archetype-web


Workspace Setup
~~~~~~~~~~~~~~~
Servers add Tomcat Server

Properties -> Targeted Runtimes -> Apache Tomcat v8

Project -> Run as -> Run on Server


http://www.java2blog.com/2015/09/how-to-create-dynamic-web-project-using.html


Compilation Problems Setup
--------------------------

.. figure:: compilationerror.png
	 :alt: compilationError

Go to ``Java Build Path`` -> ``Libraries`` and double click ``JRE System Library``.
Select a current Java Version.


Maven installieren: http://agilerule.blogspot.de/2016/06/how-to-install-maven-on-raspberry-pi.html
