Selenium
========

Selenium IDE
------------

Selenium IDE is a Firefox Plugin.

Selenium WebDriver
------------------

Selenium WebDriver is a library that will allow us to control a browser form a
programming language, such as Java, C#, Python, etc.

WebDriver enables one to automate the testing of your web application and to find
any regressions before one deploys a new version of your application.

Web driver will allow you to automate the testing of your application using various
browses. Automatic testing with WebDriver can be done in various OS.

Page Objects
~~~~~~~~~~~~

Separate Navigation and Verification in your Selenium tests with the Page Object Pattern.
This highly increases maintainability.

A page object is an abstraction of a page in a web application. We should represent
each page, or sometimes a smaller part of a page, with a Java object.

This Java object knows how to interact with the page. It can return elements and
values from the page so that they can be asserted in a test. The Java object
contains logic that is used for interaction with the webpage. It will, therefore,
effectively remove any page-specific knowledge from a test that implements the flow
in the web app. The next test doesn't have to be aware of implementation details,
such as the name of a link that should be followed to find another page.

The low level knowledge is abstracted away from the test. This allows the application
do develop and reduces the cost of maintaining the test suite. If all the navigation detail
is hidden only the page objects have to be updated, when the page changes-

The page objects should never perform assertions about the web application. The
page objects should be able to return the status of the page.

WebDirver API
~~~~~~~~~~~~~

https://sites.google.com/a/chromium.org/chromedriver/downloads
chromediver.exe speichern. Auf Version Achten und Chrome Kompatibilität!

Latest Release: ChromeDriver 2.28
Supports Chrome v55-57

Selenium Server
---------------

Run Tests on a different Computer and Operating System.

Software Setup
--------------
Selenium and Cucumber

Kubernetes Cluster:

https://github.com/kubernetes/kubernetes/tree/release-1.7/examples/selenium


Semantic verification
---------------------
Regularly Simulating a typical path through the application, to make sure your service
is up and running. Smoke Test.
