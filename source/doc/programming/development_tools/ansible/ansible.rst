=======
Ansible
=======

Ansible does not require agents on the remote hosts it controlls. Ansible uses
SSH to controll the remote hosts. 

Ansible Playbook files are YAML based text files that describe the desired end
state.

Inventories is the list of targets on which to automate.

Playbooks contain plays. Plays contain tasks. Tasks call modules. 

Tasks run sequentially. Handlers are triggered by tasks and are run once, 
at the end of plays.

Ansible Roles are special kind of playbook that are fully self-contained
with tasks, variables, configuration templates and other supporting files.

In Checkmode ansible checks what it would do in case it actually ran
the playbook. This allows you to verify your changes prior to rolling it out.

