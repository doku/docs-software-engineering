===============
Advanced Docker
===============

volumes & Permissions
=====================
if you mount volumes and they don't exist yet on the host system the folder will be created with root Permissions
on the host by the container. this causes a lot of headachs, so create your folders before you mount them.

With ``--user`` you can set the Permissions of the user in the docker container to match with your current user on the host that runs the command.
This is super important if you want to share files: 

.. code::

  docker run -ti --rm \
  -v "${MODEL_DIR}":/opt/model \
  -v "${RECORDINGS_DIR}":/opt/data/recordings \
  -v "${OUTPUT_DIR}":/opt/output  \
  --user "$(id -u):$(id -g)" \
  ${TAG} bash
