===============
Docker for Java
===============

Maven
=====

Using a **docker-maven-plugin** you can package your application in a container
right from the maven build command.

https://github.com/fabric8io/docker-maven-plugin

Gradle
======

https://github.com/bmuschko/gradle-docker-plugin
