=====================
Persistent Containers
=====================

Implicit Per-Container Storage:

- implicit sandbox for each container
- Directory is in ``/var/lib/docker/volumes`` on the host

This per container storage can not be shared with other containers and is
unavailable if container or host crashes or is deleted.

Explicit Per-Container Storage
==============================

- Volume that is created using ``VOLUME`` command


Per Host Storage
================

Data is stored in a named directory on the host. The dir is available even if the
container crashes or is deleted. Multiple containers can read/write to the
container.

Shared-Host Storage
===================

Distributed file s

.. figure:: persistent_containers_overview.png
	 :alt: persistent_containers_overview
