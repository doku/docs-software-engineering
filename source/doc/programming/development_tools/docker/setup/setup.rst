=====
Setup
=====

Windows 10
==========

Install `Docker-Toolbox`_ for Windows.

.. _Docker-Toolbox: https://www.docker.com/products/docker-toolbox

.. figure:: dockerstructure.png
	 :alt: dockerstructure

When running ``docker Quickstart terminal`` and an error like this occurs try to restart:

.. code::

	Starting "default"...
	(default) Check network to re-create if needed...
	Unable to start the VM: C:\Program Files\Oracle\VirtualBox\VBoxManage.exe startvm default --type headless failed:
	VBoxManage.exe: error: The virtual machine 'default' has terminated unexpectedly during startup with exit code 1 (0x1).  More details may be available in 'C:\Users\Fabio Schmidberger\.docker\machine\machines\default\default\Logs\VBoxHardening.log'
	VBoxManage.exe: error: Details: code E_FAIL (0x80004005), component MachineWrap, interface IMachine
	Looks like something went wrong in step ´Checking status on default´... Press any key to continue...

Start the terminal again. Everything should work now.

Install ``netcat`` Programm form the docker Quickstart commandline:

.. code-block:: bash

   docker run --net=host -ti ubuntu:14.04 bash

Share Folder with Docker
========================

Select Shared Folder:

.. figure:: select_shared_folder.png
	 :alt: select_shared_folder

And add a Folder

.. figure:: add_folder.png
	 :alt: add_folder

Docker to use Minikube registry
===============================

If you are locally working in a Windows environment with docker-toolbox and
kubernetes minikube you want docker to use the same container registry as
kubernetes so that your kubernetes instance can use newly build docker images.

With docker and minikube running execute in the docker-quickstart-terminal:

.. code-block:: bash

	eval $(minikube docker-env)
