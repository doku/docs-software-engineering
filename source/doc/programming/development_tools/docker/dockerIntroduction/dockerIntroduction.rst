Docker
=======

What is Docker?
----------------

Docker is a Container Technology.

A Container is a construct based on cgroups and LXC that allows you
to run segmented mini-systems on your Linux or Windows host system. These systems
are smaller and more light weight and transparent than a virtual machine but are
segmented and isolated more than just running an application on the host machine.

A container is a self-contained sealed unit of software. It contains everything
required to run the software. A Container includes the code, configs, processes
and networking configurations to enable communication between containers.

Containers begin to bluer the line between systems and applications.

The beauty of Docker is that each container is intended to be one service, one
or a very small number of processes with a minimal system environment customized
to itself.

Docker is a application deployment and delivery technology.
It builds, runs and deploys software container. Docker is a standard way to package,
deploy and run the software.

The mission of Docker is to build ship and run distributed applications anywhere.

.. figure:: dockermission.png
	 :alt: dockerMission

Docker defines a standard docker image. This contains your configuration and installed
software.

A docker image is the build time component. You then can run  the application as
a Docker container. You can run multiple instances of the containers on multiple hosts,
to avoid a single point of failure. This follows the principle: **Write once, Run Anywhere**
(WORA).

In an docker image, all of your application configuration data is put together. The
Idea is to **package once, deploy anywhere** (PODA).

Docker is a nativ linux technology.

How is docker different from VMs?
---------------------------------

Docker is not a virtualization technology but a application delivery technology.

For VMs you have a Host Operating Systems and use a Hypervisor (Type 2 like Virtual box)
to run a VM with a full blown operating system. On a given infrastructure there is
a limit on how many vms you can run because each operating system has its own
hardware requirements. This leads to low density.

.. figure:: dockervmbare.png
	 :alt: dockervmbare

For docker you need only a basic operating system to run your docker engine.
The docker images uses minimal operating systems and leads to much smaller images
improving scalability.

.. figure:: dockervm.png
	 :alt: dockerVm

docker workflow
----------------

Docker Client makes a request to run a docker container. The docker host downloads
the docker image form docker hub (Registry) and the docker host runs the image
in a container. All this happens behind the scenes.

.. figure:: dockerworkflow.png
	 :alt: dockerWorkflow

The client makes a request to the host, which downloads an image from the registry
and runs the container.

``-ti``
	terminal interactive

.. code-block:: bash

	docker run -ti ubuntu:latest bash #start new ubuntu container

	# STRG+D to stop container

	docker ps --all #show all containers including stopped

With the ``docker commit`` command you can save a given container as a new image.

.. figure:: dockercommit.png
	 :alt: dockercommit

.. code-block:: bash

	docker commit CONTAINER-NAME IMAGE-NAME

Running containers
------------------

When starting a container you give **one** command. The container runs as long as
this entry point process runs.

.. code-block:: bash

	docker run --rm -ti ubuntu bash #container will be removed when exited

.. code-block:: bash

	docker run -d -ti ubuntu bash #run container in the background (detached mode)

	docker attach CONTAINER-NAME #jump in an detached container

If you are in a container, hit ``CTRL P`` and ``CTRL Q`` to detatch from a container
but leave the container running in the background. You can jump back in it by using
``docker attach``.

To start another process in an already running container use ``docker exec`` command.

docker-machine
--------------

On Windows/Mac a Linux VM is used to host your docker containers. Through the
docker terminal you can reach with ``docker-machine``

.. code-block:: bash

	docker-machine ssh	#login to vm

	docker-machine ip #get ip of vm

	docker-machine stop #shutdown the vm

Manage containers
-----------------

You can look at your containers output by using ``docker logs``. Docker will keep
the output if your container even after they have exited.

.. code-block:: bash

	docker logs CONTAINER-NAME

.. code-block:: bash

	docker kill CONTAINER-NAME	#container goes to the stopped state

	docker rm CONTAINER-NAME 		#container will be gone

	docker rm -f CONTAINER-NAME #stopps and removes the container in one step

	docker rm -f $(docker container ls -aq)

Stopped Container still exists until you explicetly remove them.

You cann limit the resources of each container.

.. code-block:: bash

	docker run --memory MAX-MEMORY IMAGE-NAME

	docker run --cpu-shares RELATIVE-TO-OTHER-CONTAINER

.. hint::

	Don't let your conainers fetch dependencies when they start, make your containers
	include their dependencies inside the container themselve.

	Don't leave important things in unnamed stopped containers.

logs
~~~~

.. code-block:: bash

	docker container logs CONTAINER-NAME #show logging infos for a specific container


Networking between containers
-----------------------------

Docker provides a private network for us by the containers on your system. You
can group your containers into these private networks. You have to explicitly
expose specific ports and link containers.

You can specify the ports using the ``-p 127.0.0.1:$HOSTPORT:$CONTAINERPORT``

.. code-block:: bash

	docker run -p OUTSIDE-PORT:INSIDE-PORT/PROTOKOL(tcp/udp)

Using the ``-P`` option gives the container a predefined outside port:

.. code-block:: bash

	docker run -d --name web -P jboss/wildfly

	docker container -ls #to check which port the container uses

To make port ``8080`` in a container accessible from the internet on port ``80``
run:

.. code-block:: bash

	docker run -p 80:8080

.. code-block:: bash
	:linenos:
	:emphasize-lines: 1

	docker run --rm -ti -p 45678:45678 -p 45679:45679 --name echo-server ubuntu:14.04 bash

	nc -lp 45678 | nc -lp 45679 #simple connection forwarding

	#start 2 additional docker clients and connect them
	docker run --rm -ti ubuntu:14.04 bash
	nc 192.168.99.100 45678

	docker run --rm -ti ubuntu:14.04 bash
	nc 192.168.99.100 45678

Run the ``docker-machine ip`` command to get the ip of your virtual linux machine
hosting docker (when using docker on mac or Windows).

You can expose ports dynamically. The port inside the container is fixed but the
outside port is set dynamically (depending on which are available). This allows
you to run many containers with the same fixed inside ports without having to
worry about conflicting with each other.

You can do this by simply not specifying a outside port.


.. code-block:: bash

	docker run --rm -ti -p 45678 -p 45679 --name echo-server ubuntu:14.04 bash

To get the list of outside ports run ``docker port``.

.. code-block:: bash

	docker port CONTAINER-NAME

.. figure:: linkcontainer.png
	 :alt: linkcontainer

When usig the basic networking all traffic goes through the host

Link container
~~~~~~~~~~~~~~~

You can link your containers so the data goes directly from the client container
to the server container while staying within docker.

.. figure:: linkcontainer_directly.png
	 :alt: linkcontainer_directly

Direct linking is generally used with orchestration to keep track of things. All
ports will be linked, but only one way. You are connecting from the client to the
server, but the server does not know when a client connects. This should only be
used for services that are really meant to be run on the same machine.

You can add a link by using the option ``--link CONTAINER-NAME``.

.. code-block:: bash

	docker run -ti --rm --name server ubuntu:14.04 bash

	docker run -ti --rm --link server --name client ubuntu:14.04 bash

Docker automatically assigns a hostname in the client container. The link will
break if the IP of the server changes.

Docker has private networks so you can keep track of the names at the network level.

You can create a network using ``docker network create NETWORKNAME``.

.. code-block:: bash

	docker network create my-network

	docker run --rm -ti --net=my-network --name server ubuntu:14.04 bash

	docker run --rm -ti -- link server --net=my-network --name client ubuntu:14.04 bash

Now even if the server container is stopped and restarted the link can be reestablished.

Services that listen locally by default are only available in the container
itself. To allow connections from outside the container, you need to use the
*bind address* ``0.0.0.0`` inside the container. You can use Docker to limit access
to only the host.

.. code-block:: bash

	docker run -p 127.0.0.1:1234:1234/tcp

This listens only for connections from the localhost address of the host ``123.0.0.1``
and only if it's coming from this host should you forward it on port ``1234`` into
the container on port ``1234`` using ``tcp``.

Docker images
-------------

To list all the images that are already downloaded you can use the command
``docker images``.

You can tag a image when using the ``commit`` command:

.. code-block:: bash

	docker commit CONTAINER-ID IMAGE-NAME:VERSION

Removing images:

.. code-block:: bash

	docker rmi IMAGE-NAME

Docker Volumes
--------------

Docker Volumes are virtual discs in which you can store data and share them	between
containers and the host. Volumes are not part of images.

.. glossary::

	persistent volumes
		data will still be available even if the container is gone

	ephemeral volumes
		They exist as long as a container is using them. If no container uses the volume
		they will be deleted.

To connect to your docker host on windows/mac use:

.. code-block:: bash

	docker-machine ssh

You can share a folder from the docker host with the container.

.. code-block:: bash

	 	docker run -ti -v /home/docker/example:/shared-folder ubuntu bash

The folder ``example`` (on the host) will be available with the name ``shared-folder``
from inside the container. All data stored within this folder will be available
even after the container is stopped.

To share a file with a container you use the same command, but enter the path to
the file.

.. note::
	The file must exist **before** the container is started or it will be assumed to
	be a directory.

For sharing data between containers you use the option ``volumes-from``.
These are shared *disks* that exist only as long as they are being used.

.. code-block:: bash

	docker run -ti -v /shared-data ubuntu bash --name docker1

	docker run -ti --volumes-form docker1 ubuntu bash

The volume is still available as long as at least one container is using it, even
if the original container which created the volume is stopped. If no container uses
the volume it will be deleted. These volumes are :term:`ephemeral volumes`.

.. hint::
	You should avoid using shared folders with the host in containers. Because
	the container will only work on your machine properly.

Docker Registries
-----------------

Docker registries manage and distribute images. You can search in the official
docker registry form docker using the ``search`` command.

.. code-block:: bash

	docker search IMAGE-NAME

All images are also available on  `docker-hub`_.

.. _docker-hub: http://hub.docker.com

If you have an account on `docker-hub`_ you can login via the command line:

.. code-block:: bash

	docker login

	docker tag debian:sid fabioschmidberger/test-image:v1.0 #create new image

	docker push fabioschmidberger/test-image:v1.0 #publish your image

	docker pull fabioschmidberger/docker-latex #pull one of your own images

.. danger:: Don't push images containing passwords.

.. danger:: Be aware of how much you are trusting the containers you fetch

Dockerfiles
-----------

Dockerfiles are small programms designed to describe how to build a docker image.
You can run these dockerfiles with the ``build`` command.

.. code-block:: bash

	docker build -t name-of-result /pathTo/myDockerfile

The path to the Dockerfile should be the directory containing the ``Dockerfile``.
The entire content of the directory will be send to the docker host, so it is
recommended having the ``Dockerfile`` in an empty directory. If that is not possible
you can exclude unwanted files using an ``.dockerignore`` file.

When the command finishes running the resulting image (in this case with the tag
``name-of-result``) will be in your local Docker registry.

Each step inside a dockerfile is cached. Therefor docker can skip lines that have
not changed since the last build.

.. tip::
	Put the parts of your dockerfile that changes the most at the end of your file.
	So docker can use its cache effectively when you change your docker image and
	reuse most of the previous image.

.. hint::
	Be careful of doing operations on large files in multiple lines. If you download
	a file in one line, than work with the data in a new line and delete the files in
	a third line, all data will be cached and your docker images can get pretty big.

Dockerfiles look like shell scripts but are very different. Processes you start on
one line will **not** be running on the next line. You run them, they run for the
duration of the container, then the container gets shut down and saved into an image.

For the next line you start with a fresh container form that image.

.. attention::
	Each line in a dockerfile is its own call to ``docker run`` and then ``docker command``

Basic Dockerfiles
~~~~~~~~~~~~~~~~~

Your Dockerfile has to start with a ``FROM`` statement.

.. code-block:: Dockerfile

	FROM debian:sid
	RUN apt-get -y update
	RUN apt-get install nano
	CMD ["/bin/nano", "/tmp/notes"]

	#build this docker image
	docker build -t example/nanoer .

.. code-block:: Dockerfile

	FROM examples/nanoer
	ADD notes.txt /notes.txt
	CMD ["/bin/nano", "/notes.txt"]

Dockerfile Statements
~~~~~~~~~~~~~~~~~~~~~

.. glossary::

	``FROM``
		Defines which image to download and start from. It must be the first command
		in your Dockerfile. You can have multiple ``FROM`` Statements, which will
		produce multiple images.

		.. code-block:: Dockerfile

			FROM java:8

	``Maintainer``
		Defines the author of the Dockerfile.

		.. code-block:: Dockerfile

			MAINTAINER Firstname Lastname <email@example.com>

	``RUN``
	  Runs the commandline through the shell, waits for it to finish, and saves the result.

		.. code-block:: Dockerfile

			RUN unzip install.zip /opt/install
			RUN echo "hello Docker"

	``ADD``

		- Adds local files

			.. code-block:: Dockerfile

				ADD run.sh /run.sh

		- Adds the contents form tar archives and uncompresses them

			.. code-block:: Dockerfile

		 		ADD project.tar.gz /install/

		- Downloads files from URLs

			.. code-block:: Dockerfile

		  	 ADD https://project.example.com/download/1.0/project.rpm /project/

	``ENV``
		Sets environment variables both during the build and when running the resulting
		image

		.. code-block:: Dockerfile

			ENV DB_HOST=db.production.example.com
			ENV DP_PORT=5439

	``ENTRYPOINT``
		specifies the start of the command to run. So if your container has an ``ENTRYPOINT``
		of ``ls``, then anything you type when you run the image would be treated as
		Arguments to the ``ls`` command.

		``ENTRYPOINT`` gets added to when you add commands when starting a container.

		The Default Entrypoint is ``/bin/sh -c``

	``CMD``
		specifies the whole command to run. ``CMD`` gets replaced when you add a command
		when starting a container. Only the last ``CMD`` Command in the image/cli chain
		gets executed.

	Shell and Exec Form
		``ENTRYPOINT``, ``RUN`` and ``CMD`` can run either in shell or in exec form:

		- Shell

			.. code-block:: shell

				nano notes.txt

		- Exec

			.. code-block:: shell

				["/bin/nano", "notes.txt"]

	``VOLUME``
		defines shared or :term:`ephemeral volumes`.

		.. code-block:: Dockerfile

			VOLUME ["/host/path/", "/container/path"]
			VOLUME ["/shared-data"]

	``WORKDIR``
		Sets the directory the container starts in and changes the directory for the
		rest of the docker file.

		.. code-block:: Dockerfile

			WORKDIR /install/  #all the rest of your RUN statements will now happen in the install directory

	``USER``
	  Sets which user the container will run as

	``HEALTHCHECK``
		performs a health check on the application inside the container

		.. code-block:: bash

			HEALTHCHECK --interval=5s --timeout=3s CMD curl --fail http://localhost:8091/pools || exit 1

Tips for docker images
~~~~~~~~~~~~~~~~~~~~~~

- include all installers in your project
- have a canonical build that builds everything completely from scratch
- use small base images, such as ``Alpine``
- Build images you share publicly from Dockerfiles, always
- Don't ever leave passwords in layers. delete files in the same step

Docker: Under the hood
----------------------
Docker is a program written in Go (an upcoming system language) that manages
several features of the kernel. Docker uses ``cgroups`` to contain processes and
prevent containers from colliding with each other. It uses ``namespaces`` to contain
networks and allows it to split the networking stack for each container. Docker
uses ``copy-on-write`` file systems to build images. None of these things are new.
What Rocker really does is making scripting distributed systems *easy*.

Docker is divided in two programs: the client and the server.

The server receives commands over a socket.

.. figure:: runningdockerlocal.png
	 :alt: runningdockerlocal

You also can run the client inside one of the containers and share the socket into
this container.

.. figure:: clientasdocker.png
	 :alt: clientasdocker

Run your Docker client inside docker:

.. code-block:: bash

	docker run -ti -v /var/run/docker.sock:/var/run/docker.sock docker sh

Docker uses bridges to create virtual networks in your computer. They are essentially
software switches and control the Ethernet layer.

You can turn off this protection with

.. code-block:: bash

	docker run --net=host options image-name command

Namespaces allow processes to be attached to private network segments. These
private networks are bridged into a shared network with the rest of the containers.
Containers have virtual network cards and geht their own copy of the networking
stack.

In Docker, your container starts with an ``init`` process and vanishes when that
process exits.

.. code-block:: bash

	docker inspect

Docker can limit resources, schedule cpu time and memory allocation. These limits
are inherited, so you can't escape your limits by starting more processes.

Docker uses ``copy-on-write``. Instead of writing directly on an image, it writes
to a new layer, leaving the original image untouched. These layers are then composed
when running an actual container.

.. figure:: copyonwrite.png
	 :alt: copyonwrite

The content of layers are moved between containers in ``gzip`` files. Containers
are independent of the storage engine.

.. hint::
	On some storage engines it is possible to run out of layers.

Mounting volumes always mounts the host's filesystem over the guest.

The Docker Registry is just a network program which can be run in a local docker
container.

.. code-block:: bash

	docker run -d -p 5000:5000 --restart=always --name registry registry:2

	docker tag ubuntu:14.04 localhost:5000/mycompany/my-ubuntu:99

	docker push localhost:5000/mycompany/my-ubuntu:99

You can save your images locally, make backups and migrate between storage types :

.. code-block:: bash

	docker save -o	my-images.tar.gz debian:sid busybox ubuntu:14.04

	docker load -i my-images.tar.gz

This is also a great way to ship your images to others.

There are many :term:`Orchestration` systems available for docker. They start your
containers and restart them if they fail. They also offer service discovery and
resource allocation.

``Docker Compose`` enables single machine coordination. It is designed for testing
and developing (but not for large scale deployment). It brings up all your containers
volumes, networks etc. with one command. It is already contained in docker toolbox.

:ref:`ref-kubernetes` are meant for large scale deployment.
