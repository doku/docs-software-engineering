==============
Docker Compose
==============

Docker Compose allows you to define and run a multicontainer application
very easily.

The default configuration file name is ``docker-compose.yml``.

.. code-block:: yaml

  version: '3'
  services:
    web:
      image: jboss/wildfly
      volumes:
        - ~/deployments:/opt/jboss/wildfly/standalone/deployments
      ports:
        - 8080:8080

To start the service use ``docker-compose up``

.. code-block:: bash

  docker-compose up -d #bring up all services and run in detached mode

  docker-compose down #shutdown service and remove containers

  docker-compose -p my_app -d #create service in separate network

  docker-compose \
    -f docker-compose.yml \
    -f docker-compose.db.yml \ #overrides first file partly
    up -d

.. figure:: comand_use_cases.png
	 :alt: comand_use_cases


The ``depends_on`` option of a service only makes on a container level sure that
the other services a container depends on are started first. 
