docker hub
==========

automative docker images
------------------------

docker images can be created by linking a github repository to your docker hub
account. In your repository you define a ``Dockerfile`` and hub.docker.com will
automatically create your docker image form the given definition.
