=======
Classes
=======

A class is a definition and an object is an instance of a class.

The first argument for a method inside a class is always ``self``, which is a
reference to the object when the class is used to create an object.

.. code-block:: python

  class Duck:
    sound = 'Quack quack.'
    movement = 'Walks like a duck.'

    def quack(self):
        print(self.sound)

    def move(self):
        print(self.movement)

  def main():
      donald = Duck()
      donald.quack()
      donald.move()

  if __name__ == '__main__': main()

Constructor
===========

``__init__`` is a special name for a class function which operates as a constructor.

Object variables that are initialized for the first time when the object is
created are called object variables. Object variables are bound to the object
itself and not to the class.

A class variable is the same object in every instance of a class (like static).

Traditional Object variables have a
underscore at the beginning of the name.

Python does not have private variables. The underscores are tradition to discourage
the direct access.

.. code-block:: python

  class Animal:

    classVariable = [1, 2, 3]

    def __init__(self, type, name, sound):
        self._type = type
        self._name = name
        self._sound = sound

    def type(self, t = None):
        if t: self._type = t
        return self._type

    def name(self, n = None):
        if n: self._name = n
        return self._name

    def sound(self, s = None):
        if s: self._sound = s
        return self._sound

    # string representation of object
    def __str__(self):
        return f'The {self.type()} is named "{self.name()}" and says "{self.sound()}".'

  def main():
      a0 = Animal('kitten', 'fluffy', 'rwar')

  if __name__ == '__main__': main()

Inheritance
===========

.. code-block:: python

  class Superclass:
    def __init__(self, **kwargs):


  class Subclass(Superclass):
      def __init__(self, **kwargs):
          super().__init__(**kwargs)

Iterator
========

An iterator is a class that provides a sequence of items, generally used in a
loop. :ref:`generator-functions` are a easier way to implement a iterator.

Exceptions
==========

.. code-block:: python

  try:
    ...
  except ExceptionType as e:
    print(e)
    ...
  except: # catches all exceptions
    ...
  else
    # only gets executed if no exception was thrown

.. code-block:: python

  raise ExceptionType("...")
