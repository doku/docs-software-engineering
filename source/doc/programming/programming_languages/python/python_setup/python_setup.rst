============
Python Setup
============

.. code-block:: bash

  pip install matplotlib



jupyter notebook
================

.. code-block:: bash

  pip install jupyter

In dem Verzeichnis, in dem man mit jupyter arbeiten möchte führt man dann folgendes
Kommando aus:

.. code-block:: bash

  jupyter notebook # start application
