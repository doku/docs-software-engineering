======
Python
======

.. toctree::
  :glob:

  python_setup/*
  basics/*
  classes/*
  files/*
  matplotlib/*
  unittest/*
