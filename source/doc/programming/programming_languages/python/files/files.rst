========
File I/O
========

``open()`` returns a file object. The file object is an iterator. By default
files are opened in read mode and text mode.

.. code-block:: python

  def main():
    f = open('lines.txt')
    for line in f:
        print(line.rstrip())

  if __name__ == '__main__': main()
