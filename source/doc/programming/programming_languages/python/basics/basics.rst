======
Python
======

Python is an **interpreted high-level programming language**. Python focusses on
readability and helping programmers
to express concepts in fewer lines of code. Python features a **dynamic  type system**
and **automatic memory management**.
It supports **multiple programming paradigms**, including object-oriented,
imperative, functional and procedural,
and has a large and comprehensive standard library.
Python interpreters are available for many operating systems.
CPython, the reference implementation of Python, is **open source** software

Python 3
========
.. hint:: Everything is a object (even functions)

The Shebang Line ``#!/usr/bin/env python3`` allows execution from the commandline
on Unix based Systems. Because it starts with a comment character it will be
ignored on non-Unix Systems.

Python requires that a function is **defined before it is called**. The condition
at the end forces the interpreter to read the entire script before it executes
any of the code

.. code-block:: python

  import platform

  def main():
      message()

  def message():
      print('This is python version {}'.format(platform.python_version()))

  # __name__ will return the name of the current module, the value is __main__ if
  # it was not imported
  if __name__ == '__main__': main()

If you put a string as the first line of any function, method or class, that
serves as documentation for that function, method or class. It is traditionally
used with tripple quoted strings. 

If you have two variables and assign the same immutable value to both of them have
the same id.

When you assign a mutable type to two variables, you are actually assigning a
reference to the mutable. If you change on element the other will change to.
For mutable datatypes python is **call-by-reference**.

.. code-block:: python

  x = [2]
  y = x
  y[0] = 1
  print(id(x) == id(y)) # true
  print(x) # [1]
  print(y) # [1]

interpreter
===========

There are two main versions of python in use right now, version 3.x and 2.7.
They need different Interpreters and have
some differences, which make them in general incompatible

.. code-block:: python

  #the use of the print function was unified with how other functions are used.
  print "hi" # how to print stuff in python 2.7
  print("hi") # how to print stuff in python 3.x

You can install and run both interpreter indepentent and in parallel. Usually
one Interpreter is configured as main one
and is used if you use the command `python my_python_programm.py` to interpret
the code. But you can also call one interpreter specifically

.. code-block:: python

  py -2.7 my_python_programm.py
  py -3 my_python_programm.py

Statements and Expressions
==========================
A Statement is a unit of execution and an expression is a unit of evaluation.

In Python, an expression is any combination of literals, identifiers, and
operators. Generally, this means anything that returns a value is an expression.

A statement is a line of code. It may be a expression or an import etc. A statement
doesn't require a semicolon at the end of the line.

Blocks and Scope
================
Whitespace is significant in python. A block is delimited by indentation. Elements
that are indented at the same level are part of the same block.

Blocks do **not** define the scope in python. Even if a variable is defined
inside a block, it's scope is still the same as the code outside of it.

Functions, Objects and modules do define scope.

Comments
========

A comment ends at the end of a line. There is no specific multiline comment.

.. code-block:: python

  print("Test")   # comment

Arithmetic
==========

.. code-block:: python

  2 ** 3 # 2^3

Python types
============

Python uses a form of dynamic typing, where the type of a value is determined
by the value itself. In python3 all types are classes.

.. code-block:: python

  type(x) #get type of x
  id(x) # unique identifier for each object
  isinstance(x)

  str(x) #convert x to string
  int(x) #to int
  float(x) #to float
  bool(x) #to boolean


Strings
-------
Strings are immutable.

A string can be wrapped in ``"string"`` or ``'string'``. They are exactly the same.
There is no difference between single quotes and double quotes in python.

Using 3 quotes you can make a multiline string.
You can use single quotes in a double quotes string (and the other way round)
without escaping them.

When formating strings you can use ``:`` followed by formatting instructions.

.. code-block:: python

  print("{0:<5}, {2}, {1}, {0}".format(1, 2, 3))

In python 3.6 you can use f-Strings (which as a shortcut for the format method)

``join`` is a string method, but the string is the separator this it's going to
be joined with. ``join`` takes a list or tuple as argument.

Numeric Types
-------------

.. code-block:: python

  x = 7 / 3   # 2.3333
  y = 7 // 3  # 2

  # Exponent **
  z = 2**3 # 8

  # Modulo %
  a = 5 % 3 # 2

bool type
---------

A bool can be ``True`` or ``False``.

The following things evaluate to ``False``

- numeric zero
- empty string
- ``None``

The following things evaluate to ``True``

- non zero number
- non empty string

Sequence Types
--------------

In Sequences any element can be any object or type.

List
^^^^
A Python lists can contain multiple different datatypes at the same time. A list
is a mutable sequence.

.. code-block:: python

  list = ["str", 1.32, "ab"]

  listOfLists = [["name1", 1.2],
                 ["name2", 1.6]
                ]

Elements can be accessed with their index. Additionally there is a negative
index, starting with ``-1`` for the last element and ending with ``- legth of list`` at the
first element

List slicing:

.. code-block:: python

  list[start_inclusive : end_exclusive]

  list[:endIndex] #from index 0 to endIndex
  list[start:] #from start to last Index

  list[start:end:step]

  x = ["1", "test", "aei"]

  i = x.index("test") # returns index of test

Tupel
^^^^^
A Tuple is exactly like a list, but immutable and indicated by ``()``.

.. code-block:: python

  x = ( 1, 2, 3, 4, 5 )
  for i in x:
    print('i is {}'.format(i))

Range
^^^^^
You can create a sequences using ``range(start, end_noninclusive, stepby)``. A range is
immutable.

Dictionary
^^^^^^^^^^
A dictionary is a searchable sequence of key value pairs. Dictionaries are
mutable. Keys must me immutable.

.. code-block:: python

  y = dict(a = 1, b = 2, c = 3)
  x = {"a" : 1, "b" : 2, "c" : 3}

Set
^^^
A set is an unordered list of unique values.

.. code-block:: python

  x = {1, 2, 3, 4}

List Comprehension
^^^^^^^^^^^^^^^^^^

.. code-block:: python

  seq = range(10)

  seq2 = [x**2 for x in seq if x%2 != 0 ]

Conditionals
============

.. code-block:: python

  x = 12
  y = 42

  if x < y:
    print("1")
  elif x == y:
    print("2")
  else:
    print("3")

Python does not have a ``switch-case`` Statement.

With the ``is`` comparator you can check if two elements are exactly the same
object. ``isInstance(x, type)`` returns true if x is of type ``type``.

- logical operators ``and``, ``or``, ``not``.
- identity operators ``is``, ``is not``
- membership operators ``in``, ``not in`` (tests if element is in collection)

.. figure:: operator_precedence.png
	 :alt: operator_precedence

Tenary Operator
---------------

.. code-block:: python

  hungry = True
  x = 'Feed the bear now!' if hungry else 'Do not feed the bear.'
  print(x)

Loops
=====

.. code-block:: python

  # fibonacci
  a, b = 0, 1
  while b < 1000:
    print(b, end = ' ', flush = True)
    a, b = b, a + b

.. code-block:: python

  words = ['one', 'two', 'three', 'four', 'five']

  for i in words:
    print(i)

.. figure:: while_loop_controls.png
	 :alt: while_loop_controls

The else clause only gets executed if the loop ends normally.

Functions
=========
A function is defined with the ``def`` keyword, then the name and parenthesis,
even if it doesn't take any arguments. Multiple Arguments are separated by commas.
The arguments become variables in the scope of the function.

You can give arguments of a function a default value. If no argument is provided
the default value will be used. Any arguments with defaults must be after arguments
without defaults.

.. code-block:: python

  def function(n = 3):
    print(n)

  function() # prints 3
  function(23) # prints 23

In python all functions return a value. If you don't specify the return value,
``None`` (the absent of value) will be returned.

Argument Lists
--------------

You can have functions with variable length argument lists.
The argument will be a tuple. It is traditional to call the tuple ``args``.

.. code-block:: python

  def main(*args):
    if len(args):
      for s in args:
        print s

  main('meow', 'grrr', 'purr')

  x = ('meow', 'grrr', 'purr')
  main(*x)

Keyword Arguments
-----------------
Keyword Arguments are like list arguments that are dictionaries instead of tuples.
This allows your function to have a variable number of named arguments. This
is a dictionary as arguments. It is tradition to call the keyword arguments
``kwargs``.

.. code-block:: python

  def main():
    kitten(Buffy = 'meow', Zilla = 'grr', Angel = 'rawr')

    x = dict(Buffy = 'meow', Zilla = 'grr', Angel = 'rawr')
    kitten(**x)

  def kitten(**kwargs):
      if len(kwargs):
          for k in kwargs:
              print('Kitten {} says {}'.format(k, kwargs[k]))
      else: print('Meow.')

  if __name__ == '__main__': main()

.. _generator-functions:

Generator
---------
A generator is i special class of function that serves as an iterator. Instead of
returning a single value the generator returns a stream of values.

You use ``yield`` to "return" a value and continue the execution of the function
after the that.

.. code-block:: python

  def main():
      for i in inclusive_range(25):
          print(i, end = ' ')
      print()

  def inclusive_range(stop):

      # generator
      i = 0
      while i <= stop:
          yield i
          i += 1

  if __name__ == '__main__': main()

Decorator
---------
A decorator is a special type of function that returns a wrapper function.
Because functions are objects you can use :ref:`closure` in python.

You can use this to define scope, because block don't describe scope but functions
do.

.. code-block:: python

  import time

  def elapsed_time(f):
      def wrapper():
          t1 = time.time()
          f()
          t2 = time.time()
          print(f'Elapsed time: {(t2 - t1) * 1000} ms')
      return wrapper


  @elapsed_time
  def big_sum():
      num_list = []
      for num in (range(0, 10000)):
          num_list.append(num)
      print(f'Big sum: {sum(num_list)}')

  def main():
      big_sum()

  if __name__ == '__main__': main()

For this short annotation you have to have the decorator, followed directly by
the function definition. It takes that function and passes it into the annotated
function (the decorator function) as an argument and returns and assigns the name
of the function to the wrapper itself.
