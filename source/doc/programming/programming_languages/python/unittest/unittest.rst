=====================
Unittesting in Python
=====================

Basics
======

.. code-block:: python

  import unittest
  import rsa_helper

  class Test_rsa_helper(unittest.TestCase):

      def test_isPrime(self):
        self.assertEqual(rsa_helper.isPrim(0), False)
        self.assertEqual(rsa_helper.isPrim(1), False)
        self.assertEqual(rsa_helper.isPrim(2), True)
        self.assertEqual(rsa_helper.isPrim(17), True)
        self.assertEqual(rsa_helper.isPrim(3242), False)
        self.assertEqual(rsa_helper.isPrim(997), True)


  if __name__ == '__main__':
      unittest.main()
