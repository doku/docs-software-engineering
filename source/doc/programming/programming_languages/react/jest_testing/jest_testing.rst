============
Jest Testing
============

jasmine

jest has automated dependency moking

react virtual dom makes testing easy

istanbul test coverage


.. glossary::

  Mocking
    The process of replacing a module with a generic and mostly empty object,
    called a mock.

``describe()`` defines a group of tests

``it()`` defines a single test


Mock Functions
==============

- Mock Functions add functionality to mocks
- Mocked versions of modules don't have methods
- calling a non-existing function always results in error


Stuff
=====

express supertest: https://github.com/visionmedia/supertest


Test Strategy
=============

