=====
React
=====

React is a javascript library for user interfaces.

- describe UIs with components
- tree reconsiliation

Virtual DOM
===========

.. figure:: dom.png
	 :alt: dom

We never read from the DOM and we only write to it when a change is required.

Main Concepts
=============
JSX components are just objects and can be treated as such. 

.. warning::
  Always start component names with a capital letter.

  React treats components strating with lowercase letters as DOM tags (div, h1) and
  elements with capital letter like a component.


All React components must act like pure functions with respect to their props.

Do Not modify state directly. Use `setState()`. State Updates may be Asynchronous. 
State Updates are merged (shallow)

Data flows down. 

Be careful about the meaning of `this` in JSX callback.

You can render multiple items with the js `map` function. To identify which 
items have changed, are added or removed you should add keys. A key should
uniquely identify an item among its siblings. A good rule of thumb is that 
elements inside the map() call need keys.


.. code-block:: js

  const listItems = numbers.map((number) =>
    <li key={number.toString()}>
      {number}
    </li>
  );

If two components need the same state data, you should lift it up to the closest ancestor. 

In React a more specific component renders a more "generic" component and configures it with
props. You should use Composition instead of Inheritance. 

Thinking in React
-----------------

#. Start with a Mock
#. Break The UI into a Component Hierarchy
#. Build a stitic Version in react 
#. Identify the minimal (but complete) Representation of UI state
#. Identify where your state should live
#. Add Inverse Data Flow

Hooks
-----
Hooks don't work inside classes, they let you use react without classes. 
Hooks allow you to reuse stateful logic without changing your component hierarchy.
Hooks are a way to reuse stateful logic, not state itself. 

Hooks let you split one component into smaller functions based on what pieces are
related (such as setting up a subscripiton or fetching data). 

Only call Hooks at the top level. Don't call Hooks inside loops, conditions or nested functions. 
Only call Hooks from React function components. 

Hook funtions start with ``use`` and follow the ``useSomething`` naming pattern. 

Components
==========

function components
-------------------

.. figure:: func_component.png
	 :alt: func_component


Class Component
---------------

.. figure:: class_component.png
	 :alt: class_component

state can be changed while probs is fixed.

jscomplete.com/repl

Compononts require capital letter


.. code-block:: js

  class Button extends React.Componont {

    state = { counter: 0}

    handleClick = () => {
      this.setState((prevState) => {
        return {
          counter: prevState.counter + 1
        }
      })
    };

    render() {
      return (
        <button onClick={this.handleClick}> this.state.counter </button> //jsx
      );
    }
  }

jsx

Any ``js`` can be written in ``{}``. React event handlers use a js function.

setState is async, so when changing state based on prev state use prevState func

the state of a component can only be accessed by that component

render can only return one element, so you need to wrap them


only use class components, when you need to manage state and personalized events

controlled component

- default probs
- probtypes

bit.ly/psreact4
