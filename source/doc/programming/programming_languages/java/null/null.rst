========
``null``
========

If a reference points to null, it simply means that there is no value associated
 with it.

Technically speaking, the memory location assigned to the reference contains
the value 0 (all bits at zero), or any other value that denotes null in the
given environment.

null operations are exceedingly fast and cheap.

The concept of null exists only for reference types. It doesn't exist for
value types.

If a reference points to null then it always means that there is no value
associated with it.

If we need to know why a reference points to null then additional
data must be provided to differentiate the possible cases.

Allow null only if it makes sense for an object reference to have 
'no value associated with it'.

Don't use null to signal error conditions.

null pointer error is the most frequent bug in many software applications and
has been the cause for countless troubles in the history of software development.
Whenever possible, use a language that supports compile-time null-safety.
