Java Platforms Compared: SE vs EE vs ME
=======================================

Java is both a programming language and a platform.

The platform consists of two essential components, the JRE (Runtime Environment)
and JDK (Development Kit).
Code written in Java can run on any operating system, platform or architecture.
Their JRE sits on top of the machine's operating system and acts as an intermediate
layer between the underlying operating system and the application.
The JRE contains a virtual processor calls JVM, which interprets the java byte code

.. figure:: javaplattforms.png
	 :alt: javaplattforms

Standard Edition (SE)
-----------------------
- develop and deploy java desktop applications

.. figure:: searchitecture.png
	 :alt: seArchitecture

Enterprise Edition (EE)
-----------------------
- builds on top of the SE
- designed for large-scale, multi-tiered, scalable, reliable, secure network applications
- used fro serverside applications

- includes enterprise javabeans and the java persistence api

.. figure:: javaeetiers.png
	 :alt: javaEETiers

Java EE applications are made up of components, which are self contained functional
software units.

.. figure:: eearchitecture.png
	 :alt: eearchitecture

Micro Edition (ME)
------------------
- Subset of SE with added special libraries for small devices
- toolset for developing mobile and embedded applications
- provides device emulations
- ME applications are portable across many devices
