Programmierung
==============

Grundlagen
----------

Java
----

Einfache Datentypen
~~~~~~~~~~~~~~~~~~~

Fließkommazahlen
^^^^^^^^^^^^^^^^

Vergleich Double Werte:

.. code-block:: java

    if(abs(d1-d2)<eps)

Strings
^^^^^^^

Möchte man Strings vergleichen macht man das mit ``s1.equals(s2)``.

Strings mit dem gleichen Inhalt zeigen auf das selbe Objekt.

.. code-block:: java
  :linenos:

  String s1 = "Hallo";
  String s2 = "Hallo";
  System.out.println(s1 == s2); //liefert true

  String s3 = new String(s1);

  System.out.println(s1 == s3); //liefert false

  System.out.println(s1.equals(s3)); //liefert true

Bezeichner
~~~~~~~~~~

JVM
~~~

Collections
~~~~~~~~~~~

List
^^^^

-  Elemente können doppelt vorkommen
-  Feste Ordnung

Set
^^^

-  Ungeordnete Menge verschiedener Elemente
-  kann keine Duplikate enthalten

Queue
^^^^^

-  Eine Warteschlange nach den FIFO-Prinzip (First in - first out)

Dequeue
^^^^^^^

Stack
^^^^^

Auch Stapel oder Kellerspeicher mit dem LIFO-Prinzip (last in - first
out)

Map
^^^


Enums
^^^^^

.. code-block:: java

    public enum Status {
      PASSED(1, "Passed", "The test has passed."),
      FAILED(-1, "Failed", "The test was executed but failed"),
      DID_NOT_RUN(0, "Did not run", "The test did not start");
    }

Casting
~~~~~~~

Implizites Casting
^^^^^^^^^^^^^^^^^^

Typumwandlung funktioniert problemlos, da der Zieldatentyp größer wird.

.. figure:: ImplizitCasting.png
   :alt: Implizites Casting

   Implizites Casting

.. attention::
  Informationsverlust
    Beim impliziten Casting eines
    ``int`` hat man trotzdem Informationsverlust. Das liegt daran, wie
    Fließkommazahlen gespeichert werden, wodurch man einen
    Genauigkeitsverlust hat.

Explizites Casting
^^^^^^^^^^^^^^^^^^

Explizite Typumwandlung kann Nebeneffekte haben.

-  Zieldatentyp kann zu klein sein
-  Nachkommastellen werden abgeschnitten

.. code-block:: java
  :linenos:
  :emphasize-lines: 3

  int a = 4;
  double b = 4.1;
  a = (int) a + b;


Es gelten folgende Besonderheiten:

.. code-block:: java

    Integer.MAX_VALUE + 1 = Integer.MIN_VALUE;

    Integer.MAX_VALUE = abs(Integer.MIN_VALUE) - 1;

    (int) (Integer.MAX_VALUE * 2 + 3) = 1;
    (int) (Integer.MAX_VALUE * 4) = -4;

Boolesche Operatoren
~~~~~~~~~~~~~~~~~~~~

Boolsche Logik - Bitweise
^^^^^^^^^^^^^^^^^^^^^^^^^

Fehlerbehandlung
~~~~~~~~~~~~~~~~

Exceptions
^^^^^^^^^^

Mit einem ``try-catch``-Block kann man Exceptions abfangen. Im
``try``-Block ist der überwachte Code. Es wird versucht den Inhalt des
``try``-Blocks auszuführen. Wird hierbei eine Exception geworfen bricht
der ``try``-Block ab (es werden möglicherweise nicht alle Statements
ausgeführt).

Je nach Exception Klasse wird dann in den jeweiligen ``catch``-Block
gesprungen. In dem Bei den Exceptiontypen der ``catch``-Blöcke sollte
man so spezifisch wie möglich sein, denn man sollte verhindern das
Exceptions gefangen werden, die nicht vorgesehen waren. Das kann
unschöne Nebeneffekte haben.

Optional kann man nach einem ``try-catch`` noch einen ``finally``-Block
anfügen. Dieser wird ausgeführt, egal ob es eine Exception geworfen
wurde oder nicht. Der ``finally``-Block wird auch dann ausgeführt, wenn
der geworfene Exceptiontyp nicht gefangen werden kann. Dieser Block wird
vor allem dafür verwendet Ressourcen wieder freizugeben und I/O-Streams
zu beenden.

.. code-block:: java

    try {

    } catch(IllegalArgumentException e) {

    } catch(ArrayIndexOutOfBoundsException e) {

    } finally {

    }

+----------------------------------+-----------------------------------------+
| Exceptions                       | RuntimeException                        |
+==================================+=========================================+
| Tool zur Fehler Behandlung,      | Programmiertechnisch vermeidbar         |
| müssen explizit behandelt werden | (Auftauchen nur durch                   |
|                                  | Programmierfehler)                      |
+----------------------------------+-----------------------------------------+

Streams und Lambda Expressions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lambda Ausdrücke
^^^^^^^^^^^^^^^^

Syntax:

.. code:: java

    parameter -> body

Vier wichtige Regeln
''''''''''''''''''''

-  Typen deklarieren ist optional
-  Klammer um Parameter optional, sofern nur ein Parameter
-  Geschweifte Klammeroptional, sofern body aus einer Anweisung besteht
-  return Keyword optional, sofern es nur eine Anweisung gibt, die einen
   Typ zurückgibt

Folgende Lambda Ausdrücke sind gleichwertig (bis auf Datentypen):

.. code-block:: java

    (int x, int y) -> {return x * y;}

    (x, y) -> x*y

Funktionales Interface
''''''''''''''''''''''

.. code-block:: java

    @FunctionalInterface
    public interface TriFunction<R, A, B, C> {
      R apply(A a, B b, C c);
    }

    //ohne Lambda
    TriFunction triOld = new TriFunction() {
      @Override
      public int apply(int a, int b, int c) {
        return a + b + c;
      }
    };

    //Aufruf
    triOld.apply(1, 2, 3); //liefert 6;

    //mit Lambda
    TriFunction triLambda = (a, b, c) -> a + b + c;

    //Aufruf
    triLambda.apply(1, 2, 3); //liefert 6

Methodenreferenz
^^^^^^^^^^^^^^^^

Methodenreferenz sind Referenzen auf statische Methoden,
Instanzmethoden, Methoden einer bestimmten Instanz oder Konstruktoren

+-------------------------------------+---------------------------+
| Typ                                 | Syntax                    |
+=====================================+===========================+
| Referenz zu statischer Methode      | ``Class::staticMethod``   |
+-------------------------------------+---------------------------+
| Referenz zu Methode einer Instanz   | ``object::method``        |
+-------------------------------------+---------------------------+
| Referenz zu (empty) Konstruktor     | ``Class:new``             |
+-------------------------------------+---------------------------+

+-------------------------------------+-----------------------+--------------------------------+
| Typ                                 | Methodenreferenz      | Lambda                         |
+=====================================+=======================+================================+
| Referenz zu statischer Methode      | ``String::valueOf``   | ``(s) -> String.valueOf(s)``   |
+-------------------------------------+-----------------------+--------------------------------+
| Referenz zu Methode einer Instanz   | ``s::toString``       | ``(s) -> s.toString()``        |
+-------------------------------------+-----------------------+--------------------------------+
| Referenz zu (empty) Konstruktor     | ``Foo::new``          | ``() -> new Foo()``            |
+-------------------------------------+-----------------------+--------------------------------+

Stream
~~~~~~

Ein ``stream`` repräsentiert eine Sequenz von Objekten (vergleichbar mit
Iterator), kann aber im Gegensatz zum Iterator parallel ausgeführt
werden.

Generics
~~~~~~~~

Comparator
~~~~~~~~~~

.. figure:: ExplizitCasting.png
   :alt:
