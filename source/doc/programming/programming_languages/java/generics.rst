Generics
========

Bei Generics wird der Datentyp abstrahiert. Bei der Deklaration muss dann der
Datentyp angegeben werden.

Mehrfachtypbeschränkung:

.. code-block:: java

  <T extends B1 & B2 & B3>

Dabei kann B1 eine Klasse oder Interface sein.

Generics in Classes
-------------------

.. code-block:: java

  class Node<T> {
    private T object;
    private Node<T> next;
    public T getElement() {
      return object;
    }
  }

.. code-block:: java

  class Node <T extends Collection<?>> {

  }

Generics in Methoden
--------------------

Um einen Generischen Parameter Wert zu akzeptieren, muss der Generische Typ vor dem
Rückgabewert angegeben werden.

.. code-block:: java

  	private static <T extends Comparable<T>> boolean isFirstElementSmallerOrEqualToSecond (T firstElement, T secondElement) {
  		if (firstElement.compareTo(secondElement)>= 0) {
  			return true;
  		}
  		return false;
	}

Generic Wildecards
------------------

The wildecard declaration of ``<? extends parent>`` fits any subclass of parent or
``parent`` it self.

.. code-block:: java

  List<? extends Number> foo1 = new ArrayList<Number>();
  List<? extends Number> foo2 = new ArrayList<Integer>();

You can read a ``Number`` Object but when writing you can not add any object to
``List<? extends T>`` because you cant guarantee what kind of list it is really
pointing to, so you cant guarantee that the object is allowed in that list. The
only *guarantee* is that you can only read from it and you get ``T`` or a subclass
of ``T``.

The wildcard declaration of ``<? super child>`` fits any type which is parent in the
inheritance hierarchy of ``child``.

.. code-block:: java

  List<? super Integer> foo3 = new ArrayList<Integer>();  // Integer is a "superclass" of Integer (in this context)
  List<? super Integer> foo3 = new ArrayList<Number>();   // Number is a superclass of Integer
  List<? super Integer> foo3 = new ArrayList<Object>();   // Object is a superclass of Integer

When reading from a ``list<? super T>`` the only guarantee is that you will get
an instance of an ``Object`` or a subclass ``Object`` (but you don't know which subclass).

Producer Extends, Consumer Super (**PECS**)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Producer Extends
  If you need a ``List`` to produce ``T`` values (you want to read ``T`` from
  the list), you need to declare it with ``? extends T``. But you cannot add to
  this list.

Consumer super
  If you need a ``List`` to consume ``T`` values (you want to write ``T`` into
  the list), you need to declare it with ``? super T``. But there are no guarantees
  what type of objects you may read from this list.

If you need to both read form and write to a list, you ned to declare it exactly
with no wildcards ``List<T>``

.. code-block:: java

  public class Collections {
    public static <T> void copy(List<? super T> dest, List<? extends T> src) {
        for (int i=0; i<src.size(); i++)
          dest.set(i,src.get(i));
    }
  }

In this Example ``src`` is the producing list and ``dest`` is the consuming list.
