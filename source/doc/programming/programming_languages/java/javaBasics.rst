Java Basics
===========

Variablen vor Serialisierung schützen.

.. code-block:: java

  private volatile int a;

Ternärer Operator

.. code-block:: java

  boolean test = true;

  n = (test ? val1 : val2);

Der Ternäre Operator ist eine verkürze ``if else`` Verzweigung. Ist die Bedingung
``true`` so wird ``val1`` gewählt. Ist die Bedingung ``false`` wird der Wert hinter
dem ``:`` genommen.

Methods
-------

A Method can accept multiple Elements of a given Datatype:

.. code-block:: java

  	/**
	 * insert multiple Elements at once
	 * @param elements
	 */
	public void insertAll(T... elements) {
		for (T element : elements) {
			insert(element);
		}
	}

CompareTo
~~~~~~~~~

.. code-block:: java

  // compares two Integer objects numerically
      Integer obj1 = new Integer("25");
      Integer obj2 = new Integer("10");
      int retval =  obj1.compareTo(obj2);

      if(retval > 0) {
         System.out.println("obj1 is greater than obj2");
      } else if(retval < 0) {
         System.out.println("obj1 is less than obj2");
      } else {
         System.out.println("obj1 is equal to obj2");
      }

This method returns the value 0 if this Integer is equal to the argument Integer,
a value less than 0 if this Integer is numerically less than the argument Integer
and a value greater than 0 if this Integer is numerically greater than the
argument Integer.
