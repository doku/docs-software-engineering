.. _xml:

===
XML
===

XML ist eine Auszeichnungssprache zur Strukturierung von Daten. Ein XML-Dokument
besteht aus Elementen, welche durch Tags gekennzeichnet werden: ``<element> ... </element>``
Elemente können Text oder weitere Elemente enthalten. Sie können zudem
Attribute besitzen.

.. code-block:: xml

  <element attribute="value">
    <innerElement>Text</innerElement>
  </element>

Genau ein Wurzelelement enthält alle weiteren Elemente.


DOM-basierte APIs
=================
Das XML-Dokument wird als Baumstruktur (DOM) komplett um Speicher repräsentiert.

.. figure:: xml_dom.png
	 :alt: xml_dom

Push-APIs
=========
Der XML-Parser verarbeitet das XML Dokument sequentiell und ruft Methoden auf, wenn
bestimmte Bestandteile des XML Dokuments gelesen werden (Callback Prinzip).

.. figure:: xml_push_api.png
	 :alt: xml_push_api

Pull API
========
Einzelne Teile des XML Dokuments werden "auf Anfrage" sequentiell vom XML Parser
geholt (Iterator Prinzip).

.. figure:: xml_pull_api.png
	 :alt: xml_pull_api

Mapping API
===========

XML Daten werden auf Java-Objekte abgebildet, die Struktur des XML Dokuments
bleibt verborgen.


.. figure:: xml_mapping_api.png
	 :alt: xml_mapping_api
