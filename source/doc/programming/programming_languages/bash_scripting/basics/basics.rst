======
Basics
======

Scripts are interpreted.

The first two characters should be ``#!`` (shebang), followed by the path to
``Bash`` or ``env``. The kernel checks for ``#!`` and passes the path to the
original program as a command-line argument.

So ``./myscript.sh`` with ``#!/bin/bash`` would have the kernel execute:

.. code-block:: bash

  /bin/bash ./myscript.sh

 This general technique of having the kernel run the interpreter and passing the
 path to the script to the interpreter works a lot of scripts like python, perl
 etc.

 To execute scripts they need execute permission:

 .. code-block:: bash

   chmod u+x

If the directory that contains the script, like the current directory (.), is
not in your path, then you normally run the scripts with: ``./thescript.sh``

You can use ``time`` to execute a command and measure how long it took.

.. code-block:: bash

  time ./thescript.sh

- ``real`` is the measured time from start to finish
- ``user`` and ``sys`` are CPU times, with ``user`` being the time in the program
  itself and ``sys`` being the time in the kernel doing something for the process.

Variables
=========

You assign variables with ``=``. You should not put spaces before or after the
``=``. If the value has special characters, including spaces, put the value in
quotes.

To remove variables use the ``unset`` command. To reference the value of a
variable with the dollar sign in front of the name.

.. code-block:: bash

  echo myvar is $myvar

The shell keeps variables in two different areas.

If you say ``export var2="value"`` then the shell puts the variable in your
shells environment. Whenever it starts a new process, that process gets a copy
of those variables. To export functions use ``export -f``, to see all exported
things run ``export``.

When you group in ``()`` the content starts in a now process. ``{}`` groups
run in the same process.

.. code-block:: bash

  a=1
  (
  a=2
  )
  echo $a #prints 1

.. code-block:: bash

  a=1
  {
  a=2
  }
  echo $a #prints 2

Functions run with ``{}`` therefore they don't get a copy of the variables, but
share them and value changes in the function are effective outside of the function.

Variables can be created in a function that will not be available outside of it.
``typeset`` makes variables lokal, it can provide a type or formatting.

.. code-block:: bash

  # x must be an integer
  typeset -i x

  # declare a readonly variable
  declare -r y="value"

Bash Startup
============

``.bash_profile`` is read when Bash in invoked as a login shell.

``.bashrc`` is executed when a new shell is started.

Sourcing Scripts
================

.. code-block:: bash

  source example.sh

  # or

  . example.sh

When sourcing a script, the shell executes the script in the shell's own process
instead of in a now process.

echo
====

``echo`` is the way to print a message.

.. code-block:: bash

  echo 'Warning' >&2

While loops
===========

.. code-block:: bash

  #loops while command list 1 succeeds
  while
    command list 1
  do
    command list
  done

For loops
=========

For loops iterate over elements separated by spaces.

.. code-block:: bash

  for var in list
  do
    command list
  done

You can use ```command``` or ``$(command)`` to run another commend:

.. code-block:: bash

  # loops over 1 2 3 4 5
  for num in `seq 1 5`
  do
    ...
  done

You can generate sequences with ``{A..Z}, {1..10}`` etc.

Find starts in the current directory and searches in all subdirectories and paste
a list with all filepaths with ``.c`` as list for the for loop. This works
only properly unless there are no spaces in the file names.

.. code-block:: bash

  for f in $(find . -name *.c)
  do
    ...
  done

Functions
=========

Functions are a way to give a name to a sequence of statements that will execute
within the same shell, not in a new process.

.. code-block:: bash

  function name(parameter) {
    #statements
  }

  name

Functions produce results by writing output, like commands do. The output can be
captured

.. code-block:: bash

  hvar=$(printhello)

``exit`` is a function terminates the whole shell program, not just the function.
``exit Value`` sets the exit status represented by ``$?`` to ``Value``. ``0``
means successful, and an exit code non zero means not successful.

Redirections and Pipes
======================

Processes normally have three files open.

.. code-block:: bash

  0 => stdin
  1 => stdout
  2 => stderr

When we run a commend ad we use a ``>``, we are telling the shell to redirect
the stdout of the command into the specific file. If the file doesn't exist
it will be created, otherwise overwritten.

.. code-block:: bash

  command > stdout-here 2> stderr-here < stdin-from-here

  # file gets stdout and stderr from the command
  command &> file


Pipe ``|`` changes stdin and stdout of its assosiated commands.

.. code-block:: bash

  # command2 stdin comes from command1 stdout
  command1 | command2

  # 2>&1 redirects stderr to the same place stdout goes
  # command2 gets stdout and stderr from command1
  command1 2>&1 | command2

  # append stdout of command to the end of file
  command1 >> file

  # append stdout and stderr to end of file
  command1 &>> file

Here Document

.. code-block:: bash

  sort <<END
  2
  3
  1
  END
