=======
Haskell
=======

http://learnyouahaskell.com

Haskell is a purely functional programming language. It is statically typed
and uses a type system with type inference.

If a function takes two parameters, we can also call it as an infix function by surrounding it with backticks.
Functions can't begin with uppercase letters.

If statements are expressions (they always return something) so the else part is
mandetory.

Lists
=====

The lists within a list can be of different lengths but they can't be of different types

Setup
=====

Disable Avira Prior to running:

.. code-block:: bash

  stack ghci
