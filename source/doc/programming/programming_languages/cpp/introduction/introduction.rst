================
C++ Introduction
================

C++ was developed in the 1970'a by the Danish computer scientist Bjarne Stroustrup
and was originally called *C with classes*.

C++ is based on the C language, adding many poweful features while retaining as much
of C's syntax, efficiency and utility as possible.

Most programmin languages including Java, PHP, Python and Perl are either written
in C or C++.

Basic Syntax
============

.. glossary::

  statement
    A statement is a unit of code terminated by a semicolon.

  expression
    anything that returns a value. An expression my be part of a statement or it
    may be the entire statement.

  function
    A function is a larger unit of code that may contain many statements or expressions.
    Functions are designed to be reused or to be called by another statement.

  variable
    A variable holds values for later use in other statements and expressions. It
    represents both a value and its type. A variable has to be defined and can be
    initialized (assign a value) after that.

  identifiers
    tokens that provide readable names for variables, functions, labels and defined
    types. They consist of letters and numbers or an underscore ``_``. Identifiers
    may not begin wit a numeral. Identifiers are case sensitive. Identifiers should
    be kept under 31 characters long (only first 31 characters are guaranteed to
    be checked for uniqueness among external identifiers).

    An initial underscore character ``_private_identifier`` is commonly used for
    private identifiers.

.. code-block:: cpp

  //single line comment

  /*
  multi line comment
  */

.. code-block:: cpp

  const int x = 3; //read only variable, can not be changed

Pointers
--------

A variable is a typed and named location in memory. The name of a variable is used
as an index to map to a memory address and a particular associated data type.

.. code-block:: cpp

  //memory is allocated for a value the size of an integer and associated with name x
  int x;

  //the integer value one is copied into the memory location associated with the variable x
  x = 1;

  /* definition and assignment: memory is allocated (size of int) and the value of the
   * variable x is copied into the memory of variable y. y now contains a separate integer
   * in a separate memory location with the same value as x
   */
  int y = x

In C++ provides the ability to create a variable that is a pointer to a value as
opposed to carrying the value itself.

.. code-block:: cpp

  int *ip; //pointer definition

The variable ``ip`` is of the type **pointer to int**. Memory is allocated in the
size of a pointer. The pointer is also strongly typed (integer pointer).

To place the address of a variable in a pointer variable the ``&`` operator can
be used. ``&`` is called the **reference operator** or the **address of operator**
and returns the address of an object suitable for assigning to a pointer

.. code-block:: cpp

  int *ip;

  // address of x is placed in the pointer variable
  // ip now points to the integer variable x
  ip = &x;

To copy the value pointed to by a pointer you can use the ``*`` operator. In this
context the ``*`` is also called **de-reference operator**. It is used to get at
the value pointed to by the pointer.

.. code-block:: cpp

    //copy value of ip pointer to y
    y = *ip;

.. note::

  The value of a pointer changes if the data in the memory address it points to
  changes.

.. code-block:: cpp

  int x = 7;
  int y = 42;
  int *ip = &x;

  printf(x); // 7
  printf(y); // 42
  printf(*ip); // 7

  x = 63+;

  printf(x); // 63
  printf(y); // 42
  printf(*ip); // 63

References
----------

References and Pointers are very similar.

.. attention::

  You can not define a reference without also initializing it.

Use ``&`` to create a reference

.. code-block:: cpp

  int &y = x; //y is a reference and is initialized with the value of x

Throughout its life, ``y`` will always be a reference to ``x``. And every
