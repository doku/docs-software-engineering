==============
Best Practices
==============



Declare all variables at the top of the function.

Declare all functions before you call them.

Avoid global variables, when using them, use all UPPER_CASE names

Constructor functions (and nothing else) should be named with InitialCaps.

Use ``x += 1`` instead of ``x++`` to avoid post and preincrement errors

- Use ``"`` for external

- Only use ``undefined`` and not null (for consistency)
- don't relay on falsiness (companisons should be expressive)