==========
ECMAScript
==========

The goal of the ECMAScript 5 project was to make a batter JavaScript.


const and let
=============

``const`` and ``let`` are two new key words for declaring variables,
which behave different than ``var``.

.. figure:: var_const_let_overview.png
	 :alt: var_const_let_overview

``const`` isn't actually an immutable variable, it can be changed.

.. code-block:: JavaScript
	
    const array = ["element1", "element2"];

    array.push("element3"); // const is not immutable 


    array = ["3", "5"]; // ERROR: but it cant be reasigned.


If you need a immutable object use ``Object.freeze(yourObject)`` .

For nested objects this only freezes the outer one. There is no 
deep freeze in the language.

importing and exporting
=======================

.. code-block:: JavaScript

    export default class MyClass {

    }

    import OtherName from './file'

When using To use ``export default`` with ``const`` 

.. code-block:: JavaScript

    const test = "Hallo";

    export default test;

``const`` can be directly exported as non ``default`` modules.

.. code-block:: JavaScript

    export const test = "Hallo";

When importing non ``default`` submodules they have to be in brackets
and have the same name they were exported with.

.. code-block:: JavaScript

    import defaultThing, { nonDefault } from './export';

    import defaultThing as someThingElse, { nonDefault as other } from './export'

.. code-block:: JavaScript

    export * from './file'

Arrow Fuctions
==============

Destructing Assignment
======================

.. code-block:: JavaScript

    let { attribute1, attribute2 } = objectWithManyAttributes;

Destructing can also be used for Arrays.

Object Assembly shorthand
=========================

Object Assembly shorthand allows us to take variables 
and then insert them into objects as key value pairs in a succinct way

Computed Property Names
=======================

.. code-block:: JavaScript

    let keyName = 'myKey';
    let value = 'myValue';

    const specialKeys = {
        [keyName]: value
    } 

Spread Operator
===============

 The spread operator allows you to pull apart the values of an array 
 and make use of them as though they were instead a series or 
 collection of values, rather than a traditional array.

.. code-block:: JavaScript

    const array1 = ['element1', 'element2'];
    const array2 = ['element3', 'element4'];

    const combined = [... array1, ... array2];

The spread operator can also be used with objects.

.. code-block:: JavaScript

    const object1 = {
        attribute1: 'value',
        attribute2: 'value2'
    }

    const object2 = {
        attribute3: 'test',
        ... object1
    }

Template Literal
================

Template Literals are a easy way to pass variables into strings. 

.. code-block:: JavaScript

    let word = 'my word';

    const text = `"my text including ${word}"`

Function Arguments
==================

.. code-block:: JavaScript

    function getEmAll(... args) {  }

    function setSomeDefault(myString = "default", aList = []) { }

    function objectDestructuring({attribute1, attribute2}) { }    

Classes
=======

A ``class`` is a function that returns a special opject which has all
the specified methods and types set on it by another function.

.. code-block:: JavaScript

    class MyClass {

        constructor() {
        }

        typeList = ["hex", "rgb"]

        get types() {
            return this.typeList;
        }

        set types(types = ["hex", "rgb"]) {
            if(Array.isArray(types)) { 
                this.typeList = types.map(type => type);
            }
        }
    
    }

    const myInstance = new MyClass()

    const values = myInstance.types

getter and setter
-----------------

A getter is an automatically invoked function.

Promises
========

Promises allow you to make asyncronous actions in JavaScript.
Typically you have a function that returns a promise. 

A new Promise expects a function as its argument, and that 
function should have two arguments, which are ``resolve`` and
``reject``. And ``resolve`` and ``reject`` are themselves functions
that we'll invoke inside of our promise. 

To invoke a promise function we use the ``.then()`` syntax.

Fetch
=====

One of the most common usecases for a promise is to promisify a function 
that makes an asyncronous request through an remote API

.. code-block:: JavaScript

    fetch(url, options).then(resp => { ... })

Async and Try/Catch
===================

.. code-block:: JavaScript

    async function () {...}

    try {...} catch (e) {...}

Tail recursion
==============

This enables **continuation passing style**.

Other Features
==============

- WeakMap
- Fat arrow functions

