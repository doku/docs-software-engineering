========
JS Intro
========

Resources
=========
https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript

http://crockford.com/javascript/javascript.html

https://gist.github.com/gaearon/683e676101005de0add59e8bb345340c


Overview
========

JavaScript is an object oriented language. In JS an object is simply a
louse collection of key value pairs. It is a loosely language.

.. hint:: All values are objects, except null und undefined.

.. code-block:: javascript

  object.name = value;
  object[expression] = value;

Where ``expression`` is a string, so you can get things dynamically without
using a reflection api.

Prototypes

Delegation (Differential Inheritance)

Datatypes
=========

Everything in JavaScript is an Object, so numbers, booleans, strings, arrays,
dates, regular expressions and functions.

.. code-block:: js

  NaN === NaN //false
  NaN !== NaN //true
  isNaN(NaN) //true


Strings
-------

- There is no separate Type for characters, they are Stings with length 1.
- Strings are immutable.
- Similar Strings are equal (===)

Array
-----

Js doe not have real arrays. There is the array data type, which inherits from
Object, that really is hashtable. Therefore arrays dont have a fixed size.

``delete`` leaves a undefined hole in the array. Use ``splice`` to remove
elements from array. This is really slow because it changes the index of each
following element.

Use objects when the names are arbitrary strings. Use arrays when the names
are seqential integers.

undefined
=========

``undefined`` is the default value for variables and parameters and the value
of missing members in objects and arrays.

References
==========
Objects are passed by reference and not by value

The ``===`` Operator compares object references not values. ``True`` only  if both operands
are the same object.


function expression
===================

We make functions using the ``function`` expression. It will then return a new function
object which could then be invoked.

Function objects are first class, which means they may be:

- passed as an argument to a function
- returned from a function
- assigned to a variables
- stored to in an object or array


var statement
=============

``var`` declares and initializes variables within a function.
A variable declared anywhere within a function is visible everywhere
within the function.

.. code-block:: JavaScript

    var myVar = 0, myOtherVar;

    //expands to
    var myVar = undefined;
    var myOtherVar = undefined;

    ...

    myVar = 0;

A var statement gets split in declaration und initialisation. The
declaration part gets hoisted to the top of the function, initialising with
undefined. The initialisation part turns into a ordinary assignment.

The function statement is a short hand for a ``var`` statement with a
function value.

If the first token in a statement is ``function``, then it is a function
statement, else it is a function expression.

.. code-block:: JavaScript

  //immediately invoked function

  (function () {
    return expression;
  }())

Scope
=====

Because the ``var`` declaration gets hoisted to the top of the function
blocks don't have scope, only functions have scope.

Invocation
==========

If a function is called with to many arguments the extra ones are ignored.
If a function is called with to few arguments the missing values will be ``undefined``.

On Invocation functions additionally get the two parameters ``arguments`` and ``this``.
With ``arguments`` you have access to an array-like object containing all arguments
passed into the function. ``this`` is a reference to the object of invocation.

There are four ways to call a function.

Method form
-----------

.. code-block:: JavaScript

  thisObject.methodName(arguments)
  thisObject[methodName](arguments)

When a function is called in method form, ``this`` is set to
``thisObject``, the object containing the function. This allows
methods to have a reference to the object of interest.

Function form
-------------

.. code-block:: JavaScript

  functionObject(arguments)

When a function is called in the function form, ``this`` is set
to the global object.

Constructor form
----------------

.. code-block:: JavaScript

  new FunctionValue(arguments)

When a function is called with the ``new`` operator, a new object is created
and assigned to this.
If there is not an  explicit return value, then ``this`` will be returned.

Apply form
----------

.. code-block:: JavaScript

  functionObject.apply(thisObject, arguments)
  functionObject.call(thisObject, arguments)

A function's apply or call method allows for calling the function,
explicitly specifying ``thisObject``. It can also take an array of parameters
or a sequence of parameters.

.. _closure:

Closure
=======

The context of an inner function includes the scope of the outer function.

.. figure:: function_scope.png
	 :alt: function_scope

An inner function enjoys that context even after the parent functions have returned.

.. code-block:: javascipt

  // inner survives the outer

  function green() {
    let a;
    return function yellow() {
      let b;
      ... a ...
      ... b ...
    }
    ... a ...
  }

To make this work you don't allocate the activation record on the stack.
You allocate them on the heap and use a good garbage collector.

.. code-block:: JavaScript

  var digit_name = (function () {
    var names = ['zero', 'one', 'two', 'three'];

    return function (n) {
      return names[n];
    };
  }());

  alert(digit_name(3)); //three

The inner function will still have access to the ``names`` variable, even after
the outer function has returned.

.. hint:: This is the good stuf.


Prototypes
==========

Module Pattern
==============

.. code-block:: javascipt

  var singleton = (function () {
    var privateVariable;
    function privateFunction(x) {
      ... privateVariable ...
    }

    return {
      firstMethod: function (a,b) {
        ... privateVariable ...
      },
      secondMethod: function (c) {
        ... privateFunction ...
      }
    };
  }());

In ``singleton`` in not the outer function stored but the return value
which is a object containing two methods, because we invoke the outer
function immediately. Those two methods have the same private state.

Constructor Pattern
===================
The Module Pattern can easily be transformed into a powerful constructor
pattern.

#. Make an object (object literal, ``new``, ``Object.create``, call another power constructor)
#. Define some variables and functions (these become private members)
#. Augment the object with privleged methods (will be public)
#. Return the object

.. code-block:: javascipt


  function constructor (spec) {
    var that = otherMaker(spec),
      member,
      method = function () {
        // spec, member, method
      };
    that.method = method;
    return that;
  }


.. hint:: We take advantage of colsure in order to provide private state in the object.

Functional Inheritance
======================

.. code-block:: javascipt

  function gizmo(id) {
    return {
      id : id,
      toString: function () {
        return "gizmo" + this.id;
      }
    };
  }

  function hoozit(id) {
    var that = gizmo(id);
    that.test = function (testid) {
      return testid === this.id;
    };
    return that;
  }

With private id

.. code-block:: javascipt

  function gizmo(id) {
    return {
      toString: function () {
        return "gizmo" + id;
      }
    };
  }

  function hoozit(id) {
    var that = gizmo(id);
    that.test = function (testid) {
      return testid === id;
    };
    return that;
  }

Higher Order functions
======================

Higher Order Functions are functions that receive other functions as
parameters, and return other functions as result.

.. code-block:: JavaScript

  function liftf(binary) {
    return function (first) {
      return function (second) {
        return binary(first, second);
      };
    };
  }

  //usage

  function mult (first, second) {
    return first*second;
  }

  liftf(mult)(5)(2) // =7

The process of taking a function with multible arguments and
turning it into a function multiple functions that take a
take a single argument, is called ``currying``.

.. code-block:: JavaScript

  function curry(func, ...first){
    return function(...second) {
      return func(...first, ...second);
    };
  }
