======
Golang
======

.. toctree::
  :glob:
  :maxdepth: 2

  basics/*
  go_workspace/*
  concurrency/*
  setup/*

https://leanpub.com/golang-tdd/read

https://fullstack.network/session-18-test-driven-development-in-golang-d6b37460e9df

https://github.com/gunjan5/go-test-driven-development
