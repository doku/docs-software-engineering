=========
Go Basics
=========

Go is a compiled Language and statically typed.
The compiled executable is operating system specific.

Go Applications have a statically linked runtime.

Go has some object-oriented features:

- you can define custom Interfaces
- custom Types can implement one or more interfaces
- custom types can have member methods
- custom structs (data structures) can have member fields

Go doesn't support:

- type inheritance (no classes)
- method or operator overloading
- structured exception handling
- implicit numeric conversions

Syntax Rules
============

- Go is case sensitive
- Variable and package names are lower and mixed case
- Exported functions and fields have an initial upper-case character (this is equivallent
  to the ``public`` keyword in other languages)

  - initial upper-case character on fields or methods: available to the rest of the application
  - initial lower-case character on fields or methods: Not available to the rest of the application

- no Semicolons required: Semicolons are part of the formal language spec. but
  the lexer adds them as needed, everytime a statement is complete and the
  lexer encounters a line feed.
- Code blocks are wrapped with braces, the starting brace is on the same line
  as preceding statement
- Go has a set of built-in functions that are always available (without import)
- Package Names should be all lower case and a single word, you should not use camelcase
- All declared Variables have to be used
- If you pass an object in a function a copy will be made rather than a reference
- Go has no conventional structured exception handling syntax.

Functions
=========

Go functions are allowed to return more than one value simultaneously.

.. code-block:: go

  func main() {
  	string1 := "test1"
  	string2 := "test2"
  	string3 := "test3"

  	stringLength, err := fmt.Println(string1, string2, string3)

  	if err == nil {
  		fmt.Println("Stringlength:", stringLength)
  	}
  }

Variables can be passed into a function by reference rather than by value
using ``&``:

.. code-block:: go

  var str string
  fmt.Scanln(&str)
  fmt.Println(str)

There is no function-overloading. All functions with a lowercase first character
are private to the current package.

.. code-block:: go

  func main() {
    sum := add(32, 43)
  }

  func add(a int, b int) int {
    return a + b
  }

  // as long as all parameters are of the same type, the type can be declared at the end
  func subtract(a, b int) int {
    return a - b
  }

By using ``values ...type`` you can except any number of parameters. In the function
``values`` will be a ``slice``.

.. code-block:: go

  func addAll(values ...int) int {
    sum := 0
    for i:= range values {
      sum += values[i]
    }
  }

A go function can return multiple values.

As a convention you don't add "get" to getters.

.. code-block:: go

  func main() {
    name1, length1 := FullName("Max", "Mustermann")
  }

  func FullName(first, last string) (string, int) {
    full = first + " " + last
    length = len(full)
    return full, length
  }

Naked Return
------------
You can name the return variables in the function declaration. Then you only need
``return`` to automatically return the variables with those names in the function.
This syntax has the advantage that you don't have to worry about the order
in which you are returning them.

.. code-block:: go

  func FullNameNakedReturn(first, last string) (full string, length int) {
    full := first + " " + last
    length := len(full)
    return
  }

variables
=========

All variables have assigned types. You can set types **explicitly** or
**implicitly**.

Explicitly Typed Declaration
----------------------------

When using explicit typing, setting the initial value is optional. Use the ``var``
keyword and ``=`` assignment operator.

.. code-block:: go

  var myInteger int = 42

  var myInteger int

  var myString string = "Hello"

Implicitly Typed Declarations
-----------------------------

Use ``:=`` assignment operator without ``var``.

.. code-block:: go

  myInteger := 42

  myString := "Hello"

.. hint:: The type is still static, and  can't be changed.

Constants
---------

A constant is a simple, unchanging value

.. code-block:: go

  // explicit typing
  const myInteger int = 42

  // implicit typing
  const myString = "Hello"

Predeclared Types
-----------------

All datatype identifiers are in all lowercase and a single word.

- Boolean: ``bool``
- String: ``string``
- Fixed integer types:

  - unsigned: ``uint8``, ``uint16``, ``uin32``, ``uint64``
  - signed: ``int8``, ``int16``, ``in32``, ``int64``

- Aliases:  ``byte``, ``uint``, ``int``, ``uintptr`` (OS dependent whether they
  are ``64`` or ``32``)
- Floating Values: ``float32``, ``float64``
- Complex Numbers:  ``complex64``, ``complex128``

Data collections

- ``Arrays``
- ``Slices``
- ``Maps``
- ``Structs``

Language organization

- ``Function``
- ``Interfaces``
- ``Channels``

Data Management

- ``Pointers``

Arithmetic Operations
=====================

Numeric types don't implicitly convert. Therefore you can't add an ``int`` to a
``float`` without explicit conversion.

Pointer
=======

If you want to share the value with a function or method, then use a pointer. If
you don't want to share it, then use a value (copy).

If you want to share a value with it's method, use a pointer receiver. Since
methods commonly manage state, this is the common usage, but it is not safe for
concurrent access.

.. code-block:: go

  var p *int

  var v int = 42

  // connect pointer to value
  p = &v

  // reference value throug *
  fmt.Println("Value of p:", *p)


  var value1 float64 = 20.3
  pointer1 := &value1
  fmt.Println("Value 1:", *pointer1)
  *pointer1 = *pointer + 2 // *pointer == value1 is true

Array
=====

.. code-block:: go

  var colors [3]string
  color[0] = "Red"


  // array literal
  var numbers = [5]int{5,5,4,1}

  length_of_array := len(numbers)

Slices
======

A ``slice`` is an abstraction layer that sits on top of an array. The runtime
creates the underlying array. All element in a Slice have to be of the same type.
Slices are resizable and can be sorted.

You use ``[]type`` to create a slice, to create a array you use ``[size]type``.

.. code-block:: go

  var colors = []string{"red", "green"}

  colors = append(colors, "blue")

Map
===
A map is an unorderd collection of key-value pairs.
A maps keys can be of any type that is comparable.


.. code-block:: go

  myMap := make(map[string]string)

  myMap["key1"] = "value1"


  for key, value := range myMap {
    fmt.Printf("%v: %v", key, value)
  }

  delete(myMap, "key1")

A maps iteration order is **not** guaranteed.

Structs
=======

Structs

.. code-block:: go

  type Dog struct {
    Name string
    Age int
  }

  func main() {
    poodle := Dog{"dog", 2}
  }

Custom Types
============

.. code-block:: go

  type Dog struct {
  	Name string
  	Age  int
  }

  // pointer, to be able to change object
  func (dog *Dog) AgeBy(increment int) {
  	dog.Age += increment
  }

  func main() {
  	poodle := Dog{"dog", 2}
  	poodle.AgeBy(5)

  	fmt.Println(poodle.Age)
  }

Interfaces
==========

Interface and Type relationships are implied by the presents of the methods.

.. code-block:: go

  type Animal interface {
    Speak() string
  }


  type Dog struct {
  }

  // Dog is automatically of type Animal
  func (dog Dog) Speak() string {
    return "wuof"
  }

  type Cat struct {
  }

  // Cat is automatically of type Animal
  func (cat Cat) Speak() string {
    return "miau"
  }

  func main() {
    poodle := Animal(Dog{})

    animals := []Animal{Dog{}, Cat{}}

    for _, animal := range animals {
      fmt.Println(animal.speak)
    }

  }

If a custom types method has a name that matches an interfaces method name, then
it must implement that method with the same signature.

Every value is an instance of an type.

Every type in go is some implementation of at least one interface. Every type is also an
implementation of an  no-method interface called ``interface``.

Error Handling
==============

Go doesn't support classic exception handling syntax.

``Error`` is an interface.

An error is an instance of an interface that defines a single method, named
error and that returns a string (the error message).

.. code-block:: go

  myError := errors.New("My error string")

comma-ok-syntax
---------------

.. code-block:: go

  attendance := map[string]bool{
    "Ann": true,
    "Mike": true
  }
  // ok will be true if element was found, false otherwise
  attended, ok := attendance["Mike"]
  if ok {

  }

Deferring function calls
========================

Statements labeled with ``defer`` will wait until everything in the current function
is finished and execute after that. The ``defer`` statement only works within
a function.

Multiple ``defer`` Statements are added to a stack and handled in LIFO order at
the end of the function.

.. code-block:: go

  func main(){
    defer fmt.Println("2. Line")
    defer fmt.Println("1. Line")
    fmt.Println("undeferred statement")

    x :=
  }

Defered Statements are evaluated at the moment of deferring, rather than wait until
the execution happens.

.. code-block:: go

  func main(){
    x := 1;
    defer fmt.Println(x) // will print 1, not 2
    x += 1
  }

With ``defer`` you can immediately call the file or db-connection close functions
after opening them.

Control structures
==================

if-statement
------------
The ``if`` Statement does not require ``()`` around the condition.

.. code-block:: go

  var x := 32
  var result string

  if x < 0 {
    result = "hallo"
  } else if x == 0 {
    result = "x is 0"
  } else {
    result = "test"
  }

.. hint::

  Due to the lexers sensitivity to line feeds you must place the ``else`` in
  the same line as the preceding closing brace ``}``.

You can include an initial statement as part of the if declaration. Any variables
you declare will be local to the ``if`` block.

.. code-block:: go

  var result string

  if x:= 31; x < 0 {
    result = "hallo"
  } else if x == 0 {
    result = "x is 0"
  } else {
    result = "test"
  }

switch-statements
-----------------

You can evaluate any simple type with switch statements. Codeflow automatically
jumps past additional cases after finding a match, therefore no ``break``
statements are required.

.. code-block:: go

  result := ""

  switch x := 1; x {
    case 1:
      result = "its one"
    case 2:
      result = "its two"
    default:
      result = "default"
  }


  // alternative to a extended if statement
  x := -42
  switch {
    case x < 0:
      result = "test"
    case x == 0:
      result = "its zero"
    default:
      result = "default"
  }

To have fallthrough behavior in switch statements you have to explicitly add a
``fallthrough`` statement.

.. code-block:: go

  // fallthrough
  x := -42
  switch {
    case x < 0:
      result = "test"
      fallthrough
    case x == 0:
      result = "its zero"
    default:
      result = "default"
  }

Loops
=====

You can iterate with ``for`` statements, there is no ``while`` loop in go.

.. code-block:: go

  sum := 0

  for i:= 0; i<10; i++ {
    sum += 10;
  }

  colors := []string{"red", "green", "blue"}

  for i := 0; i < len(colors); i++ {
    fmt.Println(colors[i])
  }

  for i := range colors {
   fmt.Println(colors[i])
  }


You can mimic while loop behavior:

.. code-block:: go

  sum := 0

  for sum < 1000 {
    sum += sum
  }

``break`` and ``continue`` work like in Java.

There is support for ``lables`` and ``goto``.

.. code-block:: go

  sum := 0

  for sum < 1000 {
    sum += sum

    if sum > 200 {
      goto endofprogramm
    }

    endofprogramm : fmt.Println(sum)

  }

Memory Management
=================

Memory is allocated and deallocated automatically.

The ``new()`` function allocates but does not initialize memory.
Results in zeroed storage, but returns a memory address.

The ``make()`` function allocates and initializes memory.
It allocates non-zeroed storage and returns a memory address.

You must initialize complex objects before adding values. Declarations without
``make()`` can cause a **panic**.

.. code-block:: go

  var m = map[string]int
  m["key"] = 41 // panic: assignment to entry in nil map

  var m := make(map[string]int)
  m["key"] = 41 // no panic

Memory is deallocated by the garbage collector.

Commandline Tools
=================

godoc
-----

like java doc

gofmt
-----

formatter
