===========
Concurrency
===========

A ``goroutine`` is a lightweight synchronized thread managed by the runtime.

A ``channel`` is a typed conduit for messages between goroutines.

``select`` lets a goroutine wait for multiple communication operations.

Data structures are not safe for concurrent access.
