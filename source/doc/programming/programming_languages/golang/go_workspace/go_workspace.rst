============
Go Workspace
============

.. attention::

  Unlike other programming languages, in go typically keep all your Go code in
  a single workspace.

  You add the path to this workspace to your environment variables as ``GOPATH``.

The workspace is a directory hierarchy
