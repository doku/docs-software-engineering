===================
Programmiersprachen
===================

.. toctree::
  :caption: Contents:
  :maxdepth: 2
  :glob:

  java/index
  c/index
  cpp/index
  ada/index
  python/index
  javascript/index
  react/index
  xml/*
  golang/index
  bash_scripting/*
  haskell/*
