====
HTTP
====

.. glossary::

  HTTP
    Hypertext Transfer Protocol is the set of rules, servers and browsers use to
    transfer web documents.

    HTTP is plain language and human readable. It is a stateless protocol which
    means that each individual request sent over the protocol is unique and no
    request is connected to another request (memoryless).

    Every action performed over HTTP starts with a request using one of the HTTP
    methods and ends with a response containing an HTTP status code saying what
    happened to the request along with the data like headers and content.

  Sessions
    To overcome the statelessness HTTP has Sessions, which are stored states
    shared between the browser and server. The browser and server can exchange
    information in form of cookies.

  User Agent
    An application acting on behalf of the user (literally an agent of the user),
    typically the browser

  TCP
    Short for Transmission Control Protocol, one of the main internet protocols
    used by the World Wide Web, email, FTP and remote administration

  IP
    Short for Internet Protocol, IP is used to actually transfer data between
    computers over a network. Every device connected to the internet has an
    IP address.

  URL
    Short for Uniform Resource Locator, an address pointing to a resource on the
    web.

  .. figure:: url_breakdown.png
  	 :alt: url_breakdown

  URN
    Short for Uniform Resource Name

    If we request a folder without a file specification the server looks for a
    file named ``index.html`` or ``default.html`` or ``index.php`` or simular. If
    the file is named anything other than ``index`` or ``default`` the resource
    path needs to list the filename specifically.

    URL queries start with a ``?`` and then each query has an argument and a value
    like ``u=1234``. Multiple queries can be strung together using ``&``.

    .. figure:: urn_breakdown.png
    	 :alt:     urn_breakdown

  DNS
    Short for Domain Name Server, DNS catalogs all domain name URLs and points
    them to the IP address of servers.

  Proxy
    Software or hardware service acting as a middle person between clients and
    servers

  Request-Response Pairs
    Clients and servers communicate over HTTP using request-response pairs. A
    request is sent, and a response is returned.

  Header
    Request and responses use HTTP headers to pass information back and forth.
    They always contain a Request Method

  HTTP Request Method/Verb
    Every HTTP request contains a method (also called verb) explaining what action
    the sender wants to perform on the resource.

    - ``GET``
    - ``POST``: Create a new resource and add is to the collection
    - ``PUT``: Update an existing singleton resource based on ID
    - ``DELETE``: Delete a singleton resource based on ID.
    - ``CONNECT``
    - ``HEAD``: Get just the response header from the resource
    - ...

  Status Response Code
    Numerical code in the 100 to 500 range describing what type of response the
    server sent back to the client.

  Cookie
    String of data passed back and forth between the client and server to create
    a stateful session.

  HTTPS
    TLS certificates are exchanged to ensure that only the computer and the
    server can encrypt and decrypt the transmitted data.

Default Ports
=============

- HTTP: 80
- HTTPS: 443

HTTP Status Codes
=================

- ``1xx``: Information
- ``2xx``: Success
- ``3xx``: Redirection
- ``4xx``: Client error
- ``5xx``: Server error

HTTP Header
===========

Cache Busting
=============

Each file has a unique name. Therefore anytime you upload a now version of a
file, you upload a new version of a file you actually are uploading a unique file
that has not yet been cached.
