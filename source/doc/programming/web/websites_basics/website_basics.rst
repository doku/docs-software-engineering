Website Basics
==============

Static vs Dynamic Websites
--------------------------

Static Websites show the same content for all visitors.

Dynamic Websites show different content depending on the visitors identity.
