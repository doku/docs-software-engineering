===================
Progressive Web App
===================

#. Load Quickly (through App shell)
#. Responsive
#. Offline
#. Installable (manifest)
#. Secure

Service Worker
==============

.. figure:: service_worker.png
	 :alt: service_worker

- requires https

The Service Worker is a background script that a browser can run even while a
user isn't on your page. It's the thing that provides offline support, and wakes
up when a notification is triggered.

- "native" experience
- offline Usage
- Notifications (even when website not running)

Notifications
=============

Notify on the following events

- time sensitive
- it has specific info that's good to know or act upon
- it is from people or sources that matter to me, which makes it personal.

Client Side
-----------

#. Get permission to send notifications
#. Subscribe and get ``PushSubscription``
#. Send ``PushSubscription`` to your server

Sending a Message
-----------------

#. Create message on our server
#. Use Web Push Protocol to send
#. Push Service delivers message
#. Message arrives on device

Handling the Incoming Message
-----------------------------

#. Message arrives on the device
#. Browser wakes up the service worker
#. Handels push event and handles notifications

WebPush Api
