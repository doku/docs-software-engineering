Web
============

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   http/*
   rest/*
   progressive_web_app/*
   websites_basics/*
