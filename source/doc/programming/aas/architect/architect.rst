=========
Architekt
=========

Der Architekt überwackt die Entwicklung und stellt sicher, dass die gewünschte
Architektur umgesetzt wird. Der Architekt ist für die technische Umsetzung
verantwortlich.

Durch die geeignete Kombination von Komponenten erzeugt man neue Eigenschaften
im System, die keines der Komponenten hat.


.. note::

  If the size and complexity of a software system increase, the global structure
  of the system becomes more important than the selection of specific algorithms
  and data structures.

Bei Architektur hat man verschiedene Sichten auf die gleichen Gegenstände, die
für verschiedene Stakeholder wichtig sind.


An architect designs an IT Strategy which supports the business strategy.

Enterprise Architecture should be technology independent.

Abstraktion ist

.. hint::
  Architekturentscheidungen werden immer in einem Kontext getroffen.

- Archetektur ist auf viel höherem Level als Design.
- The ability to move quickly between levels of abstraction and audiences while adjusting communitcation style accordingly
- 




Views
=====

A software system is represented by different models (views) each of which focuses
on an individual aspect of the overall system (function blocks, communication, presistence, layering, module structure, deployment)

Coherence between these views is required.

Structural & Behavioral views
-----------------------------

.. figure:: levels_and_views.png


.. figure:: layers_and_stages.png


