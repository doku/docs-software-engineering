================
rpc & Middleware
================

.. figure:: rpc_1.png

Nichtfunktionale Anfordenungen werden durch Middelware repräsentiert

Middelware ist eine schicht zwischen Anwendung und Betriebssystem


- EXEC SQL CALL is an RPC mechanism

Converting data structures into messages:
    - marshalling, de-marshalling (serialization)

- Standard Data respresentaiton 
- IDL: Interface definition language
- WSDL: IDL for Services


- RPC is synchronous?

.. figure:: wsdl.png

CORBA
=====

(RPC for Objects)

Corba is an OMG (Object Management Group) specification for an Common ORB Architecture

Corba defines a metamodel for destributed objects an da corresponding IDL (Interface definition Language)

.. figure:: corba_architecture.png

Object Request Broker ORB
-------------------------

.. figure:: orb.png


Messaging
=========

- Messaging enables send and forget approach to communication (sender only has to cait for the message system acceptation)
- async communication allows sender to batch requests. 
- more reliable communication than RPC, because messaging system can store and forward & guarantee delivery
- async messaging requires more complex eventdriven programming model (hard to develop & debug)
- not all applications can operate in a send and forget mode: *Request-Reply Pattern* bridges th  gab between synchronous and async solutions.


Message Queuing
---------------

- Message Queue Mananger (MQM) 
- Message Queuing Interface (MQI)

.. figure:: mqi_1.png

Message Queue Manager
----------------------

A **mover** processes messages in a transmission queue jointly with its partner mover at the other end 
of the channel, thus ensuring reliable transmission to remote MQM (Guaranteed delivery)

- You use 2 Phase commit protokol between two pmovers

Publish-Subscribe
-----------------

Loose Coupling
--------------
core principle: reduce the number of assumptions two parties make about each other when they exchange information.

RPC simply does **not* have same semantic of a local call!