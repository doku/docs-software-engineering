===
MDA
===

.. figure:: mda_overview.png
	 :alt: mda_overview

- **CIM (computation independent model)**: Focus on domain without implementations. Define the domain logic technology independent. This helps you understand what the system has to do. 
- **PIM (Plattform independet model)**: view of the system from platform independent perspective. PIM can be used with number of different plattforms
- **PSM (Plattform Specific Model)**: combines specifications in PIM & details on how specific platform is used
- **PM (Plattform Model)**: technical concepts that describe platform. 

MDA Pattern
===========

Model transformation is the process of converting one model to another model of the same system. 

.. figure:: mda_transform.png

Model transformation is the process of converting one model to another model of
the same system.


Meta Model Mapping
------------------
- the mapping gives rules how types of PIM metamodels are transformen into types of PSM metamodels
- not applicable if PSM has no metamodel specified (like in Model-to-text/code transformation where PSM = code)


Model Marking
-------------
- a mark represents a concept in the psm, which can be applied to an element of the PMI to indicate how that element has to be transformed.
- marks contain plattform specific information, that should not pollute the PIM (you create a reference copy)


.. figure:: model_marking.png


Transformation Templates
------------------------
- templates are parameterized models that sepcify particular kinds for transformations

Model Merging
-------------

Mulit-stage Transformation
--------------------------
Apply MDA pattern in cascade (several times in succession)

Multi-platform transformation
-----------------------------
- MDA transformation can use marks for several PMs to transform a PIM into a PSM with parts of the system on several different plattforms


Advantages of MDA
==================
- each model is independet of the rest
- software development becomes model transformation

MDA process
===========

#. A CIM is prepared
#. PIM is built
#. Architect chooses a Plattform
#. PIM is marked
#. Transforme the marked PIM into a PSM
#. the resulting PSM can be the PIM for the next MDA iteration


Eine Anatomy beschreibt wie man mit dem Bestandteil einer Beschreibung umgeht.

Der Designer gibt mit marking

Ein Marking steuert die Transformation vom PIM zum PSM

Ein Model eines Modells ist ein Meta-Model

Dadurch das Beide Meta-Model im gleichen Sprach Raum ist können wir eine
Mapping Transformation angeben.


Das Platformmodell wird mit Markierungen annotiert.

MOF ist ein Meta-Meta-Model


MDA Standards
=============

Meta object facility (MOF)
--------------------------

- Meta-Meta Modelf for the construction of meta-models in MDA
- uml is an instance of MOF



DSL (Domain Specific Lanugage)
==============================

.. figure:: 4layer_metamodel.png


UML
===

Extending UML
-------------
- Heavyweight: completely new meta-model based on MOF
- lightweight: extension based on UML metamodel (inherit from uml constructs an M2, use of stereotypes and tagged values)
- lightweight: extension with UML Profiles (sets of seterotypes and tagged values, modelling tools support profiles)


.. figure:: uml_extension.png


.. figure:: uml_extension_metamodel.png

	UML extension with meta model

UML Profile
-----------

- profiles specialize uml for specific domains 
- profiling is the standard, built-in extension mechanism for UML
- profile consists of: stereotypes, tagged values (attributes of stereotypes) OCL constraints
