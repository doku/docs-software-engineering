===================
Quality of Services
===================

Scalability
===========


Scalability is the ability to

  - endure increasing workload
  - without decreasing an agreed service level
  - if underlying resources are also increased


Availability
============
- Mean Time Between Failures
- Mean Time To repair

A system is called available if it is up and running and
produces correct results. The availability of a system is
the fraction of time it is available.

Reducing the meant time to repair is easier.

The only way to prevent failure, is to fail constantly. (Netflix)


Stability
=========

.. note::

  An application system is called **stable** if it keeps processing business transactions 
  even when there are transient impulses, persistent stresses or component failures disrupting
  normal processing.


CAP, Base and Eventuually Consistency
=====================================

CAP Theorem
-----------


Consistency Models
------------------

Base Properties
---------------


Resilience
==========

.. figure:: failure.png



Isolation - Bulkheads