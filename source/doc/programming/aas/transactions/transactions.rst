============
Transactions
============

- Das transaction program bildet die real durchgeführten business transaktionen durch.
- Vor der Ausführung der Transaktion ist die Daten Konsistent, nach der Ausführung 
  muss die Transaktion wieder konsistent sein. Während der Ausführung können Daten
  inkonsistent sein.
- The Recource Manager may decide to ABORT transacions unilaterally at any point in time.

.. hint:: 

	Never trust uncommitted Data!

	Thus, COMMIT is even important for read-only transactions.

ACID
====

Atomicity
	all actions performed within a transaction either all succeed or are all undone

Consistency
	A transaction transforms resources from one consistent state to another consistent state

Isolation
	Although multiple transactions may execute concurrently, each transaction can be unaware of other concurrently executing transacions
	The program realizing a transaction is isolated from all other actions within the system

Durability
	Once successfully completed the effects of a transaction will survive any failure

Transaction State
=================

- Active
- Done
- Failed
- Aborted
- Committed

.. figure:: transaction_state_diagramm.png
	 :alt: transaction_state_diagramm

.. figure:: transaction_operations.png



Serializability
===============

A possibly concurrent schedule is serializable if it is equivalent to a
serial schedule.

.. figure:: schedule_conflicts.png

.. figure:: schedule_swaps.png

If a schedule :math:`S` can be transformed into a schedule :math:`S'` by a series of swaps of 
non-conflicting instructions, we say that :math:`S` and :math:`S'` are conflict equivalent: :math:`S \equiv S'`

We say that a schedule :math:`S` is conflict 

Serialsierbare Schedules verhindern:

- Inconsistent Database,
- inconsistent Views
- lost update.

Recoverability
--------------

.. figure:: recoverability.png


Jeder Scheduler muss den Commit jeder Read operationen von daten commiten bis
alle schreiber auf diesen Daten auch committet haben, bevor der Reader committet.

Cascading Abort
---------------
A single transaction failure leads to a series of transaction rollbacks. 

- ACA Property: avoid cascading aborts

ACA Schedules: "Never read uncommitted data"

For each pair of transactions :math:`T_i` and  :math:`T_j` such that :math:`T_j` reads a data item
previously written by :math:`T_i`, the commit operation of the writing transaction :math:`T_i` appears
befor the read operation of :math:`T_j`.

Eac cascadeless schedule is also recoverable. 

Testing for Serializability
---------------------------

.. figure:: testing_for_serializability.png

.. figure:: testing_for_serializability_1.png

.. figure:: testing_for_serializability_2.png

.. figure:: testing_for_serializability_3.png


.. hint:: 
	A schedule is conflict serializable if and only if its precedence graph is acyclic.

If precedence graph is acyclic, a serialibability order can be obtained by a
topological sorting (:ref:`topological_sorting`) of the graph.

Distributed Transaction
=======================

You have multiple resource manager.

Schwierigkeit: Atomares commitment

- atomicity and two-phase-commit

Transactionalität ist infectiös.

Startet eine Transaction einen anderen befehl, so

Transaction Bracking über transaction manager.

Atomic Commitment Protocol
==========================

Das 2 Phasen Commitment Protokoll ist ein ACP.
Prepare Phase und Commitment Phase.

.. figure:: two_phase_commit.png

Sendet ein Recource Manager das Prepraed Statement, so speichert er die Daten
in einem Separaten Datentopf zwischen.
Hat der Recource manager ein Prepared Statement geschickt, so sind diese
Daten blockiert und keine andere Transaktion kann auf diesen Daten arbeiten.

.. hint::

	Theorem: In the presence of communication failures, **no** atomic commitment
	protocol (e.g. 2PC) can avoid blocking of participants or remove dependency
	of recovery of failed participants.

X/Open DTP (Distributed Transaction Protocol)
---------------------------------------------

DTP Reference Model

.. figure:: dtp_reference_model.png


Transaction Process Monitor
===========================

- business transaction: all steps necessary to perform a function from a particular business area
- transaction program: The execution of a transaction that implements a business transaction

.. figure:: purpose_tp.png

- TP Loop

Request:
	- User name
	- Device ID
	- Request Type
	- Request Parameters


Savepoints
----------

.. figure:: savepoints.png



Queued TP
---------


.. figure:: problems_direkt_tp.png


Multi-step Transactions
-----------------------


Transaction Model Metaphor
--------------------------


Extended Transaction Models
---------------------------