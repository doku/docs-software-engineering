======================
Architectural Diagrams
======================

utitity computing

Architecture Description Languages (ADLs)
=========================================


Basic Elements:
    - **Components** (represent the primary computational elements and data stores of a system)
    - **Connectors** (represent interactions between components)
    - **Systems** (configurations of components and connectors, the overall topology of a system is defined independently from internal details of components and connectors that make up the system)
    - **Properties** (semantic information about a system and it's components that goes beyond structure)
    - **Constraints** (claims about the architectural design that should remain true even if as it evolves over time, like allowed value properites, topology & design vocabulary)
    - **Styles** (represent families of related systems, like Pipes and filters, 3-tierd client server, service-based systems)
    - **Rationale** (ADR)




ACME
----

.. figure:: acme_example_1.png

- Components can have multiple interfaces, each of which is called a **port**
- Representations
- Bindings
- Properties
- Constraints: invatiants & heuristics


Architectural Styles
====================

.. figure:: architectural_styles_list.png

Pipes & Filters
---------------

- Pipes: provide the output of a filter as input to another filter

Filters
    - read the input data stream and transform it into an output data stream
    - don't share status with other components
    - don't know the identity of their neighbors


Events Based
------------

- functions are not executed through direct method call
- components raise an event (publishers)
- other components (subscribers) are notified and act accordigly
- the relation between events and event handling is unknown to the components
- if an event is published it is not assured that it's handled by someone
- no processing sequence is clearly defined
- behavior of components 

SOA
---

.. figure:: soa_1.png


- **Service**: A function provided at a network address, it's always on (the user doesn't have to deal with construction & deconstruction)
- **Service Oriented Computing (SOC)**: compute paradigm behind services
- **Service oriented Architecture (SOA)**: architecture style to realize SOC
- **Web Service Technology (WS)**: standard- and technology stack to support SOA
 


Workflow-Based Application structure
------------------------------------

Large applications use special "control programs" to ensure the appropriate & correct sequencing of business functions


- BPMN

.. figure:: workflow_based_application.png 