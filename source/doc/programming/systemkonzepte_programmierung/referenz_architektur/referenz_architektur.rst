===================
Referenzarchitektur
===================

.. figure:: referenz_architektur_modell.png
	 :alt: referenz_architektur_modell

Das Betriebssystem ist zuständig für das Management von Rechnerressourcen
(CPU, Speicher, I/O) und die Vergabe von Ressourcen an Prozesses.

Das Rechnernetz ist zuständig für das Management von Netzressourcen und die
Nachrichtenkommunikation zwischen Prozessen und Threads.

Die Middleware beschreibt höhere Konzepte und Funktionen zur Kommunikation und
Synchronisation (für spezielle Programmiersprachen oder sprachenunabhängig).

Die Anwendung sind nebenläufig interagierende Prozesse/Threads. Sie können auf
einem oder über mehrere Knoten verteilt sein.

Die Modelle der Nebenläufigkeit sind eine allgemeine Abstraktion zur Beschreibung
von Nebenläufigkeit. Sie ist gültig auf allen Ebenen der Architektur.
