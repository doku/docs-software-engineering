=====================
Globale Eigenschaften
=====================

.. glossary::

  Globaler Zustand
    Menge von Variablen unterschiedlicher Prozesse

  Globale Eigenschaften
    Prädikat auf globalem Zustand. Die Prüfung eines Prädikats erfordert
    "Zusammenführung" der lokalen Anteile des globalen Zustands.


Der Globale Zustand bestimmt sich aus einer Menge lokaler Knotenzustände. Eine
inhärente Eigenschaft von verteilten Systemen ist, dass das Erfassen der lokalen
Zustände zum gleichen Zeitpunkt unmöglich ist, bedingt durch unterschiedliche
Nachrichtenlaufzeit. Die Nachrichten können noch im Transit sein.

Durch diese Eigenschaften können inkonsistente globale Zustände beobachtet werden.


Verteilte Verklemmungserkennung
===============================

(Distributed Deadlock Detection)

.. figure:: wartegraph.png
	 :alt: wartegraph

In verteilten Systemen ist der Wartegraph häufig über mehrere Knoten verteilt.

.. figure:: verteilte_verklemmung_bsp.png
	 :alt: verteilte_verklemmung_bsp


Verteilte Terminierung
======================

Bedingung für die  Terminierung

- Alle Prozesse sind passiv und
- es sind keine Nachrichten in den Kommunikationskanälen
