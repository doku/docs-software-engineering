======================
Globaler Schnappschuss
======================

Ein Schnappschuss ist konsistent wenn:

#. Aufgezeichneter Globaler Zustand entspricht einem :term:`Konsistenter Schnitt` und
#. für jeden Kanal :math:`C_j` wird berücksichtigt die Sequenz von Nachrichten, die
   während des Schnitts in :math:`C_j` in Transat sind.

	.. figure:: Schnappschuss_transit.png
		 :alt: 	 schnappschuss_transit

.. figure:: schnappschuss_konsistenz.png
	 :alt: schnappschuss_konsistenz

.. figure:: globalerschnappschuss_verklemmungserkennung.png
	 :alt: globalerschnappschuss_verklemmungserkennung

	 Verklemmungserkennung

Algorithmus von Chandy und Lamport
==================================

Systemmodell
------------

.. figure:: systemmodell.png
	 :alt: systemmodell

Prinzip
-------

.. figure:: prinzip1.png
	 :alt: prinzip1

.. figure:: regeln.png
	 :alt: regeln

Marker-Sonderregel:
	Prozess sendet Marker, nachdem er seinen Zustand aufgezeichnet hat und bevor er
	irgendeine weitere Nachricht sendet.

Marker-Empfangsregel:
	- Empfang des ersten Markers: Prozess zeichnet seinen Zustand auf und beginnt
	  mit dem Aufzeichnen für seine eingehenden Kanäle.
	- Empfang weiterer Marker über irgendeinen eingehenden Kanal :math:`C`: Beendet
	  die Aufzeichnung von Kanal :math:`C`.

Prinzipiell kann jeder Prozess den Algorithmus starten. Falls man die Marker
unterschiedlicher Aktivierungen unterscheiden kann, können sogar mehrere Prozesse
den Algorithmus gleichzeitig starten.

.. figure:: korrektheit.png

Beispiel
--------

.. figure:: beispiel.png
	 :alt: beispiel

	 Beispiel

Algorithmus
-----------

Jeder Prozess hat drei Threads.

- compute-Thread
- receive marker-Thread
- record state-Thread

.. figure:: algorithm_1.png
	 :alt: algorithm_1

.. figure:: algorithm_2.png
	 :alt: algorithm_2

Der Algorithmus erfasst in endlicher Zeit einen konsistenten Schnappschuss.
