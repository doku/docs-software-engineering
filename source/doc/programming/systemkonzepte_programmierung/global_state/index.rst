================
Globaler Zustand
================

.. toctree::
  :glob:

  globale_eigenschaften/*
  konsistenter_globaler_zustand/*
  verteilte_terminierung/*
  globaler_schnappschuss/*
