=============================
Konsistenter Globaler Zustand
=============================

Systemmodell
============

.. figure:: systemmodell.png
	 :alt: systemmodell

Kausale Abhängigkeit
====================

Die Relation "passierte davor" (:math:`\rightarrow`) auf der Menge der Ereignisse
erfüllt die folgenden drei Bedingungen

#. Sind :math:`e` und :math:`f` Ereignisse im selben Prozess, und :math:`e` wird
   vor :math:`f` ausgeführt, dann gilt :math:`e \rightarrow f`
#. Repräsentiert :math:`e` das Senden einer Nachricht und :math:`f` das Empfangen
   dieser Nachricht, durch einen anderen Prozess, dann gilt :math:`e \rightarrow`
#. Sind :math:`e,f` und :math:`g` Ereignisse in beliebigen Prozessen und
   :math:`e \rightarrow f` und :math:`e \rightarrow g`, dann gilt :math:`e \rightarrow g`

Zwei Ereignisse :math:`e` und :math:`f` sind **nebenläufig (concurrent)** oder
**kausal unabhängig** notiert mit :math:`e || f`, wenn weder
:math:`e \rightarrow f` noch :math:`f \rightarrow e`

Konsistenter Schnitt
====================

.. figure:: historie.png
	 :alt: historie

.. figure:: globale_historie.png
	 :alt: globale_historie

.. figure:: consistenc_cut.png
	 :alt: consistenc_cut

.. glossary::

  Konsistenter Schnitt
      Ein Schnitt :math:`C` ist konsistent, wenn für jedes Ereignis :math:`e`
      in :math:`C` alle Ereignisse, die kausal vor :math:`e` passiert sind,
      ebenfalls in :math:`C` enthalten sind.

      Für alle Ereignisse :math:`e \in C: f \rightarrow e \Rightarrow f \in C`

  Konsistenter globaler Zustand
    Ein konsistenter globaler Zustand entspricht einem konsistenten Schnitt.
