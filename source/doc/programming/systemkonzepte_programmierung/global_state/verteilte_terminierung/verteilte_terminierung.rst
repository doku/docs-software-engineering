=====================
Verteile Terminierung
=====================

Problem Definition
==================

.. figure:: problem_definition.png
	 :alt: problem_definition

.. figure:: verhalten.png
	 :alt: verhalten

Algorithmus von Dijkstra-Scholten (DS)
======================================

Fur jede Kante existiert eine  Rückkante zur Signalisierung von Kontrollnachrichten
des Terminierungsalgorithmus.

.. figure:: prozess_zustaende.png
	 :alt: prozess_zustaende

The difference between the number of messages received on an incoming edge
:math:`E` of node :math:`i` and the number of signals sent on the corresponding back
edges is denoted :math:`inDificit_i[E]`.

The difference between the number of messages sent on outgoing edges of node
:math:`i` and the number of signals received on back edges is denoted
:math:`outDeficit_i`

.. figure:: datenstruktur.png
	 :alt: datenstruktur

Korrektheit
-----------

.. figure:: lemma1.png
	 :alt: lemma1

Terminiert das System, so meldet die Umgebung schließlich Terminierung (Liveness).

Spannbaum
---------

Spannbaum aufbauen, um Safety Eigenschaft zu erreichen.

.. figure:: spannbaum.png
	 :alt: spannbaum

.. figure:: spannbaum_aufbau.png
	 :alt: spannbaum_aufbau

Algorithmus
-----------

.. figure:: ds_algo.png
	 :alt: ds_algo

.. figure:: ds_algo_signal.png
	 :alt: ds_algo_signal

A node sends its last signal only, when :math:`outDeficit_i` has become zero, so
when it becomes a leaf node in the spanning tree of non-terminated nodes.

Komplexität
-----------

.. figure:: ds_complexity.png
	 :alt: ds_complexity

Kreditmethode
=============

Prinzip
-------

.. figure:: kredit_prinzip.png
	 :alt: kredit_prinzip

Die Berechnung wird durch mehrere Prozessoren ausgeführt:

- Umgebungsprozess
- mindestens ein (Arbeiter-) Prozess

Die Umgebung hat 2 Threads:

#. ``compute``-Thread, der die Berechnung initiiert und nach Empfang aller Kreditanteile
   Terminierung feststellt.
#. ``receive signal``-Thread, der die empfangenen Kreditanteile nebenläufig zum
   ``compute``-Thread akkumuliert.

Jeder Prozess hat zwei Threads:

#. ``compute``-Thread, der einen Teil der eigentliche Berechnung durchführt. Er
   kommuniziert mit anderen Prozessen mittels ``send message`` und ``receive message``
#. ``send signal``-Thread, der die Kreditabteile bei Terminierung an die Umgebung
   zurückgibt.

Algorithmus
-----------

Umgebung:

.. figure:: kreditmethode_algo_umgebung.png
	 :alt: kreditmethode_algo_umgebung

Prozess:

.. figure:: kreditmethode_algo_prozess.png
	 :alt: kreditmethode_algo_umgebung

Kredite
-------

.. figure:: rekombination.png
	 :alt: rekombination

	 Kreditrekombination

Komplexität
-----------
.. figure:: credit_complexity.png
	 :alt: credit_complexity
