===============================
Transportprotokolle im Internet
===============================

Der Transportdienst ermöglicht Interprozesskommunikation.

User Datagram Protocol (UDP)
============================

UDP ist ein :term:`Verbindungsloser Dienst`.

UDP hat nur minimale zusätzliche Funktionalität im Vergleich zu IP:

- Adressierung von Ports, d.h. Adressierung von Diensten
- Prüfsumme zur Sicherung von Bitfehlern

.. figure:: udp_nachrichtenformat.png
	 :alt: udp_nachrichtenformat

Transmission Control Protocol (TCP)
===================================

TCP ist ein fehlerfreier Kommunikationskanal zur Übertragung von Byte-Strömen.

TCP baut auf IP auf. TCP ist ein :term:`Verbindungsorientierter Dienst`

- Reihenfolge gesichert
- zuverlässig (keine Bitfehler, Datenverlust, Duplikate)

.. figure:: tcp_segmente.png
	 :alt: tcp_segmente

Three-Way-Handshake
-------------------

.. figure:: three_way_handshake_1.png
	 :alt: three_way_handshake_1

.. figure:: three_way_handshake_2.png
	 :alt: three_way_handshake_2

.. figure:: three_way_handshake_3.png
	 :alt: three_way_handshake_3

.. figure:: three_way_handshake_4.png
	 :alt: three_way_handshake_4

.. figure:: three_way_handshake_5.png
	 :alt: three_way_handshake_5
