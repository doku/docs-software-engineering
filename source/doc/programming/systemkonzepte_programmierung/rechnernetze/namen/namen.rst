=====
Namen
=====

The ``name`` of a resource indicates **WHAT** we seek, an ``address`` indicates
**WHERE** it is, and a ``route`` tells **HOW TO GET THERE**.

.. figure:: namen.png
	 :alt: namen

Domain Name System (DNS)
========================

DNS ermöglicht die Abbildung von Domänennamen auf Internet-Adressen.
