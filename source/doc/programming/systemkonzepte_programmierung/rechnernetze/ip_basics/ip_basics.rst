=================
Grundlagen von IP
=================


.. glossary::

  Vermittlungsdienst
    Der Vermittlungsdienst ermöglicht die Kommunikation zwischen beliebigen
    Rechnern in einem Netz oder Netzverbund

IP ist verbindungslos (:term:`verbindungsloser Dienst`)

IP Adressierung
===============

Hierarchisches Addressierungskonzept mit ``Netznummer.GeräteNummer``. Dabei ist
die Netznummer global eindeutig, die Gerätenummer aber nur im jeweiligen Netzwerk.
Somit ist jede IP-Adresse global eindeutig.

IP ist eine Datagramm-orienteriete Kommunikation und ermöglicht die Kommunikation
zwischen Endgeräten. Endgeräte werden mit IP-Adressen identifiziert.

IPv4
----
32 Bit-Adressen

IPv6
----

- 128 Bit Adressraum
- Durch ``:`` getrennte Hex-Darstellungen eines 16-Bit-Teils der Adresse


Routing
=======

Die Aufgabe ist die Bestimmung eines Pfades für Pakete vom Sender zu dem
Empfänger bzw. den Empfängern.

Routing Tabellen
================

Allgemeines Optimierungsprinzip
-------------------------------

.. figure:: optimierungsprinzip.png
	 :alt: optimierungsprinzip

Die Optimalen Pfade von allen Quellen zu einem bestimmten Ziel bilden einen Baum.

.. figure:: spann_tree.png
	 :alt: spann_tree

Jeder Router hat eine Routing Tabelle, wobei zu jedem Ziel ein kürzester Pfad
gespeichert wird.

Man kann dann verschiedene Metriken (Distanz, Anzahl der Teilstrecken etc.) als
Kantengewichte nutzen.

Shortest-Path-Routing
---------------------

Mit Dijkstra lassen sich die kürzesten Pfade bestimmen.

.. figure:: dijkstra_min_tree.png
	 :alt: dijkstra_min_tree

Distanzvektor-Routing
---------------------

Router tauschen mit Nachbarn Routing-Informationen (Distanzvektoren) aus und
berechnen auf dieser Grundlage neue Routing Tabellen.

- jeder Router ermittelt die Entfernung :math:`e^i` zu jedem (direkten) Nachbar :math:`R^i`
- Jeder Router sendet jedem Nachbar seinen Distanzvektor
- der Router berechnet auf der Grundlage der Empfangenen Distanzvektoren seine
  neue Routing-Tabelle

.. figure:: distanz_vektor.png
	 :alt: distanz_vektor
