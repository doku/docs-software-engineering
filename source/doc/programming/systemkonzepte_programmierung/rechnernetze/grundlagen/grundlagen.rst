===========================
Grundlagen der Rechnernetze
===========================

.. glossary::

  Rechnernetz
    Ein Rechnernetz ist eine Menge autonomer Computer, die über ein Netzwerk
    miteinander verbunden sind.

    Zwei Computer gelten als verbunden, wenn sie Informationen austauschen können.

    Netzwerke können unterschiedliche Größen und Topologien (Formen) haben.

Netzwerke
=========

Direkte Konnektivität
---------------------

- Punkt-zu-Punkt Verbindung
- Mehrfachzugriffsverbindungen (Ring, Bus)

Unabhängige Netzwerke werden zu einem Verbund zusammengeschlossen.

.. figure:: netzwerk_typen.png
	 :alt: netzwerk_typen

Schichtenarchitektur
====================
Kommuniktationssysteme haben einen hohen Komplexitätsgrad, daher nutzt man ein
geschichtetes System zur Abstraktion. Zur schichtspezifischen Kommunikation sind
Protokolle definiert.

.. figure:: kommunikations_schichten.png
	 :alt: kommunikations_schichten

.. glossary::

  Service
    Ein Service oder Dienst ist eine Menge von (abstrakten) Primitiven, die eine
    Schicht der darüber liegenden Schicht anbietet. Die Implementierung bleibt
    transparent.

    .. figure:: service_and_protokoll.png
    	 :alt: service_and_protokoll

  Protokoll
    Ein Protokoll regelt die Kommunikation zwischen Partnerinstanzen einer Schicht.
    Es definiert Regeln, wie das Format der Nachrichten, die erlaubte Abfolge der
    Partner Interaktionen und die durch Sende- und Empfangsereignisse ausgelösten
    Aktionen.

  Verbindungsorientierter Dienst
    Dreiphasige Kommunikation mit Verbindungsaufbau, Datentransfer und Verbindungsabbau.
    Meist zuverlässige Kommunikation

  Verbindungsloser Dienst
    Übertragung isolierter Dateneinheiten, meist unzuverlässige Kommunikation
