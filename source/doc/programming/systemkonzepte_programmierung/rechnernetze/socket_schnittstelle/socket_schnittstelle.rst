====================
Socket-Schnittstelle
====================

.. figure:: socket_api.png
	 :alt: socket_api

Sockets sind Kommunikationsendpunkte.

Verbindungsorientiertes Paradigma
---------------------------------

Verbindungsloses Paradigma
--------------------------

Server-Schablone
================

.. figure:: session_orientiert.png
	 :alt: session_orientiert

Java Sockets
============

Verbindungslos
--------------

- DatagramSocket
- DatagramPacket

.. code-block:: java

	public class Client {

		public static final String SERVERNAME = "localhost";
		public static final int SERVERPORT = 1234;

		public void sendRequest(byte[] request) throws IOException {
			InetAddress serverAddress = InetAddress.getByName(SERVERNAME);

			DatagramSocket s = new DatagramSocket();
			DatagramPacket req = new DatagramPacket(request, request.length, serverAddress, SERVERPORT);
			s.send(req);

			DatagramPacket res = new DatagramPacket(new byte[65535], 65535);
			s.receive(res);
			System.out.println(new String(request) + new String(res.getData()));

		}

		public static void main(String[] args) throws IOException {
			Client c = new Client();
			c.sendRequest("Hallo".getBytes());
		}
	}

.. code-block:: java

	public class Server {

		public static void main(String[] args) throws IOException {
			DatagramSocket s = new DatagramSocket(1234);
			DatagramPacket packet = new DatagramPacket(new byte[65535], 65535);

			while (true) {
				s.receive(packet);

				byte[] data = packet.getData();
				int port = packet.getPort();
				InetAddress address = packet.getAddress();

				Thread t = new Thread(new ServerThread(data, address, port));
				t.start();
			}

		}

	}

.. code-block:: java

	public class ServerThread implements Runnable{

		byte[] data;
		InetAddress address;
		int port;

		public ServerThread(byte[] data, InetAddress address, int port) {
			this.data = data;
			this.address = address;
			this.port = port;
		}

		@Override
		public void run() {
			byte[] result = doCalculation(data);

			DatagramSocket socket = new DatagramSocket();
			DatagramPacket out = new DatagramPacket(result, result.length, address, port);
			socket.send(out);
		}

		private byte[] doCalculation(byte[] data2) {
			return "Server".getBytes();
		}

	}


Verbindungsorientiert
---------------------

- Socket
- ServerSocket
