===============================
Synchronisation auf Systemebene
===============================

Im Gegensatz zu Atomaren Primitiven gibt es hier kein Aktives Warten, da das
Betriebssystem die Schlange blockierter Threads verwaltet und diese erst aktiviert,
wenn Konflikt nicht mehr besteht.

Semaphor
========

.. figure:: semaphor.png
	 :alt: semaphor

.. figure:: semaphor_wait.png
	 :alt: semaphor_wait

.. figure:: semaphor_signal.png
	 :alt: semaphor_signal

.. figure:: critical_section_semaphor.png
	 :alt: critical_section_semaphor

- wechselseitiger Ausschluss
-

Selbst bei nicht fairer Ausführung gibt es keine Verklemmung.

Um Aushungern zu vermeiden, benötigt man faire Ausführung (Thread mit hoher
Priorität im nicht kritischen Abschnitt kann sonst Fortschritt verhindern) und
eine Faire Implementierung des Semaphores (Warteschlange in Implementierung muss
fair sein).

Implementierung
---------------

Das Betriebssystem muss atomare Veränderung der Synchronisationsvariablen unterstützen.
(zum Beispiel mit :ref:`atomare-primitive`)

.. code-block:: java

  // pseudo binäre Semaphore allerdings value>1 möglich
  static Semaphor semaphore = new Semaphor(1);

.. code-block:: java

	import java.util.concurrent.Semaphore;

	public class SemaphoreDemo {
		static Semaphore semaphore = new  Semaphore(1);

		static class Demo implements Runnable {
			public void run() {
				while (true) {
				try {
					semaphore.acquire();
					try {// Kritischer Abschnitt
					} finally {semaphore.release();}
				} catch (InterruptedException e) {}
			}}
		};
		public static void main(String[] args) {
			new Thread(new Demo()).start();
			new Thread(new Demo()).start();
		}
	}


Monitor
=======
Monitor ist ein objektorientierter Ansatz zur Synchronisation auf Systemebene.
Die gemeinsamen Date werden im Zustand des Monitor-Objekts gekapselt und immer
nur **ein** Thread kann zu einem Zeitpunkt Methoden des Objektes ausführen.

.. figure:: monitor_1.png
	 :alt: monitor_1

.. figure:: bedingungsvariablen.png
	 :alt: bedingungsvariablen

.. figure:: monitor_prio.png
	 :alt: monitor_prio
