.. _atomare-primitive:

=================
Atomare Primitive
=================

auch Hardware Synchronisation

UBR abschalten
==============

Abschalten der UBR bewirkt, dass keine Verschachtelung mit anderen Threads möglich
ist.

.. figure:: ubr_abschalten.png
	 :alt: ubr_abschalten

Dadurch verliert das Betriebssystem aber Kontrolle über das Programm.
Das Verfahren ist ungeeignet für den wechselseitigen Ausschluss auf Multiprozessoren.

Die folgenden Verfahren sind anwendbar auf beliebig viele Threads, effizient für
Mehrprozessorsystemen und einer geringen Anzahl von Konflikten. Sie lösen aber
nur den :term:`Kritischer Abschnitt (schwache Formulierung)`. Bei nicht fairer
Ausführung ist Verklemmung möglich.

Test and Set
============

.. figure:: test_set.png
	 :alt: test_set

.. figure:: critical_section_testset.png
	 :alt: critical_section_testset

Exchange
========

Exchange führt den atomaren Austausch zweier Speicherreferenzen durch.

.. figure:: exchange.png
	 :alt: exchange

.. figure:: critical_section_exchange.png
	 :alt: critical_section_exchange

Compare and Swap
================

Atomarer Vergleich und Setzen einer Speicherreferenz

.. figure:: compare_and_swap.png
	 :alt: compare_and_swapl

.. figure:: critical_section_compare_swap.png
	 :alt: critical_section_compare_swap

Bewertung
=========

.. figure:: bewertung.png
	 :alt: bewertung
