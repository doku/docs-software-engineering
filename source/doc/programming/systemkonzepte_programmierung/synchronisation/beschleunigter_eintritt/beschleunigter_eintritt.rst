=======================
Beschleunigter Eintritt
=======================

.. figure:: beschleunigter_eintritt_algo.png
	 :alt: beschleunigter_eintritt_algo

.. figure:: beschleunigter_eintritt_faelle.png
	 :alt: beschleunigter_eintritt_faelle

.. figure:: kein_konflikt.png
	 :alt: kein_konflikt

.. figure:: konflikt_1.png
	 :alt: konflikt_1

.. figure:: konflikt_2.png
	 :alt: konflikt_2
