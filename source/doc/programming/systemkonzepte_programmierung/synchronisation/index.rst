===============
Synchronisation
===============

.. toctree::
  :maxdepth: 2
  :glob:

  kritischer_abschnitt/*
  model_checking/*
  dekkers_algorithmus/*
  bakery_algorithmus/*
  beschleunigter_eintritt/*
  atomare_primitive/*
  synchronisation_auf_systemebene/*
  synchronisations_probleme/*
