===================
Dekkers Algorithmus
===================

Der Dekkers Algorithmus funktioniert nur für die Synchronisation von 2 Prozessen.

.. figure:: dekkers_algorithmus_code.png
	 :alt: dekkers_algorithmus_code

Invarianten
===========

- wenn ``p`` im KA ausführt gilt: ``wantp=true``
- wenn ``q`` im KA ausführt gilt: ``wantq=true``

Eigenschaften
=============

- Wechselseitiger Ausschluss
- Keine Verklemmung
