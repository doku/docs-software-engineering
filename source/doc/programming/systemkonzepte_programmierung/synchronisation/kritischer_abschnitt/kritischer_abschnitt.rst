====================
Kritischer Abschnitt
====================

Beim Kritischen Abschnitt Problem wollen beliebig viele Threads in einem
Programmabschnitt, bezeichnet als **kritischer Abschnitt**, mit nicht atomar
ausführbaren Befehlen auf gemeinsame Daten (Ressourcen) zugreifen.

Ziel einer Lösung des Kritischen Abschnitts ist es Verschachtelung im kritischen
Abschnitt vermeiden.

.. code::

  Loop forever
      Nicht kritischer Abschnitt
    Präprotokoll
      Kritischer Abschnitt
    Postprotokoll

Das Präprotokoll stellt sicher, dass immer nur ein Thread im kritischen
Abschnitt ist. So ist keine Race-Condition über gemeinsame Ressourcen möglich
Sodass andere Threads den Kritischen Abschnitt betreten können, müssen sie bis
zur Ausführung des **Postprotokolls** warten.

.. glossary::

  Kritischer Abschnitt (schwache Formulierung)

    Jede Lösung zum Problem des **kritischen Abschnitts** muss folgenden Eigenschaften
    genügen:

      #. **Wechselseitiger Abschluss** (Mutual Exclusion): Keine Verschachtelung
         zweier Threads bzgl. Befehle des kritischen Abschnitts.
      #. **keine Verklemmung** (Deadlocks): Falls mehrere Threads versuchen den kritischen
         Abschnitt zu betreten, wird ein Thread letztendlich erfolgreich sein.

  Kritischen Abschnitts (starke Formulierung)

    Jede Lösung zum Problem des kritischen Abschnitts erfüllt zusätzlich zum
    **wechselseitigen Ausschluss** und **Freiheit von Verklemmung** folgende
    Eigenschaft:

    3. **Keine Aushungerung**: Wenn ein Thread den kritischen Abschnitt betreten
       möchte, so wird er ihn letztendlich betreten.

Ein Deadlock (Verklemmung) entsteht immer, wenn die Threads eine zyklische Wartebeziehung haben.
Deadlocks treten bei schlechten Präprotokollen ein.

Das Problem bei der schwachen Formulierung des Kritischen Abschnitts, ist, dass
kein individueller Fortschritt eines (bestimmten) Threads garantiert wird.

Optimistisches Verfahren
========================
Man nimmt an, dass es keine Nebenläufigkeit gibt und führt den
Algorithmus aus. Ist man mit der Berechnung fertig, so überprüft man, ob bereits
ein anderer Thread die geteilten Variablen bearbeitet hat. Wurden die Variablen
nicht bearbeitet, so kann man die Ergebnisse des Algorithmus speichern, falls
sie verändert wurden, führt man die Berechnung erneut mit den aktualisierten
Werten aus.
