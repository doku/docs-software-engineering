========================
Synchronisationsprobleme
========================

Philosophenproblem
==================

Koordiniert die Zuweisung geteilter Ressourcen

.. glossary::

  Philosophenproblem
    Jede Lösung zum Philosophenproblem muss folgenden Eigenschaften genügen

    - Keine Verklemmung
    - Kein Aushungern

Consumer/Producer
=================

Koordiniert Interaktion entkoppelter Tasks

.. figure:: producer_consumer.png
	 :alt: producer_consumer

Readers/Writer
==============

Koordiniert Lesen und Schreiben auf gemeinsamen Speicherbereich

Ähnlich zum kritischen-Abschnitt-Problem, aber mehrere Leser können den KA nun
gleichzeitig betreten. Sobald ein Schreiber sich im KA befindet, kann kein anderer
Thread den KA betreten.

Die Anzahl der Leser im KA wird in gemeinsamer Variable :math:`rc` gespeichert.

.. figure:: readers_writers_withsemaphore.png
	 :alt: readers_writers_withsemaphore
