==================
Bakery-Algorithmus
==================

Lamports Bakery-Algorithmus erlaubt den wechselseitigen Ausschluss für
beliebige Anzahl von Threads.

Idee
====

- Jeder Thread erhält eine Nummer, bevor er den Kritischen Abschnitt betritt
- Der Besitzer der kleinsten Nummer darf den kritischen Abschnitt betreten.

Mehrere Prozesse können dieselbe Nummer teilen.
Dann Totalordnung :math:`>>` durch ``(Nummer, process id)`` Tupel sicherstellen.
Die Process ID ist eindeutig.

Algorithmus
===========

.. figure:: bakery_algorithm.png
	 :alt: bakery_algorithm

Invarianten
===========

#. Wenn :math:`p_i` und :math:`p_j` die gleiche Nummer ziehen, wartet :math:`p_i`
   auf das Update von :math:`p_j` vor einem Vergleich der gezogenen Nummer (Zeile 6)
#. Nur ein Thread mit minimaler Nummer (gemäß Totalordung) :math:`\neq 0` tritt in den KA ein.
#. Nach einem Update der gezogenen Nummer sieht jeder nachfolgende Thread eine
   größere  Nummer

Korrektheit
===========

- Wechselseitiger Ausschluss, da nur Thread mit kleinster Nummer im KA ausführt
- keine Verklemmung, da Thread mit kleinster Nummer (totale Ordnung) schafft es dennoch
- kein Aushungern von :math:`p`, da nur eine endliche Anzahl von Threads mit
  kleinerer Nummer als :math:`p` existieren
