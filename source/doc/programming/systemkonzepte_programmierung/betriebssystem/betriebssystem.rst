==============
Betriebssystem
==============

Das Betriebssystem ermöglicht und kontrolliert insbesondere verschachtelten
Ablauf von Prozessen.

Es übergibt Kontrolle über den Prozessor an andere Programme und ist verantwortlich
für die Granularität nebenläufiger Ausführung.
