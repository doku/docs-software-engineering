===============
Geschichte
===============

Serielle Verarbeitung
=====================
Nur ein Programm kann auf der Rechnerarchitektur ablaufen.

Batch (Stapel) System
=====================
Ermöglicht automatisierte Unterstützung der Ausbringung mehrerer Programme.

Bei der Uniprogrammierung werden Programme in sequentieller Reihenfolge abgearbeitet.
Das ist sehr ineffizient selbst für Uniprozessor-Systeme, da in der meisten Zeit
auf I/O Operationen gewartet werden muss.

Multiprogrammierung
===================

Multiprogrammierung ermöglicht effizientere Auslastung des Prozessors durch
:term:`Quasiparallelität` bei der Ausführung von Programmen.

- Möglichst hohe Hardwareauslastung

Time Sharing Systeme
====================
Time Sharing Systeme unterstützen zusätzlich die Interaktion einer Anwendung
mit seiner Umwelt.

Das Ziel ist kurze Antwortzeit für alle Prozesse.

Echtzeit Systeme
================
Die Korrektheit des Systems hängt nicht nur vom logischen Resultat ab,
der Zeitpunkt der ausgeführten Berechnung ist wichtig.

Jede Berechnung hat eine Deadline. Das Ergebnis muss rechtzeitig vorliegen
oder gefährdet die Sicherheit der Ausführung.
