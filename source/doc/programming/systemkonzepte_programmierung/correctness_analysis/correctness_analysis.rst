=================================
Korrektheitskriterien und Analyse
=================================

Unterschiedliche Ausführungen desselben Programms resultieren in unterschiedlichen
Ausführungssequenzen und durchlaufenen Zuständen.

Zusätzliche Korrektheitskriterien und Invarianten sind nötig um
Nebenläufige-Programme analysieren zu können.

Safety
======

.. note::
  Eine Sicherheitseigenschaft stellt sicher, dass niemals etwas Schlechtes
  passieren wird.

.. glossary::

  Sicherheitseigenschaft
    Für eine **Sicherheitseigenschaft** eines Programms gilt: Die Sicherheitseigenschaft
    gilt in jedem möglichen Ausführungszustand des Programms.

Sobald das Programm einen Zustand erreicht, der die Sicherheitseigenschaft verletzt,
so ist diese auch in jeder möglichen Fortsetzung des Programms verletzt.

Beispiele für Sicherheitseigenschaften einer Kinokartenreservierung
-------------------------------------------------------------------

- Pro Veranstaltung werden maximal :math:`n` Karten vergeben
- Jede Antwort des Systems enthält eine Karte oder eine Ablehnung
- Eine Anfrage wird nur abgelehnt, wenn es keine freien Karten gibt

.. _liveness:

Liveness
========

.. note::
  Eine Lebendigkeitseigenschaft stellt sicher, dass letztendlich (eventually)
  etwas "Gutes" passiert.

.. glossary::

  Lebendigkeitseigenschaft:
    Für eine **Lebendigkeitseigenschaft** gilt: Für jeden Zustand eines Programs gibt es
    eine Fortsetzung der Ausführung, welche die Eigenschaft in endlich vielen
    Schritten erfüllt.

Eine Voraussetzung für Lebendigkeit ist, dass jeder Befehl letztendlich ausgeführt
wird.

Beispiel Kartenreservierung
---------------------------

- Jede Anfrage wird von System letztendlich beantwortet.

.. hint::
   Eine Lebendigkeitseigenschaft definiert keine feste obere Zeitschranke. Z.b.
   "Jede Anfrage wird in 20 Sekunden beantwortet" ist eine :term:`Sicherheitseigenschaft`

.. _fairness:

Fairness
========

Jede Anfrage wird vom System letztendlich beantwortet.

Schwache Fairness
-----------------
Eine schwach faire Ausführung eines Programms garantiert, dass jeder ständig
ausführbereite Befehl eines Prozesses letztendlich ausgeführt wird.

Bei schwacher Fairness hat ein Befehl *nur* eine Wahrscheinlichkeit :math:`>0`
pro Ausführungsschritt.

Starke Fairness
---------------
Eine stark faire Ausführung eines Programms garantiert, dass jeder unendlich oft
(nicht notwendigerweise ständig) ausführbereite Befehle eines Prozesses unendlich
oft ausgeführt wird.

Man muss sich merken wer in der Warteschlange war (Bookkeeping), da die Befehle zwischen
zeitig auch nicht ausführungsbereit sein können.
