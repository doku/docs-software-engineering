=================
Parallele Abläufe
=================

Die Unterstützung paralleler Abläufe benötigt Programme, in denen verschiedene
Aktivitäten gleichzeitig ausgeführt werden können. Programme/ Systeme dieser Art
werden als **nebenläufig** bezeichnet.

Nebenläufigkeit
===============

.. glossary::

  Concurrency (Nebenläufigkeit)
    Eigenschaft eines Systems, in dem verschiedene Aktivitäten gleichzeitig
    ausgeführt werden können.

  Quasiparallelität
    Muss das Programm aus I/O warten, kann der Prozessor ein anderes Programm ausführen.
    So lässt sich der Prozessor besser ausnutzen, da er bei den langen I/O
    Wartezeiten trotzdem weiter arbeiten kann.

    .. figure:: quasiparallelitaet.png
    	 :alt: quasiparallelitaet

  Parallelität
    In mehreren CPU Kernen werden Programme parallel ausgeführt.

    .. figure:: paralleitaet_diagramm.png
    	 :alt: paralleitaet_diagramm

Aktivitäten können nur gleichzeitig ausgeführt werden, wenn zwischen ihnen keine
kausale Abhängigkeiten bestehet.

Sequentielle und Nebenläufige Programme
=======================================

Bei Sequentiellen Programmen werden alle Instruktionen des Programms sequentiell
ausgeführt.

Nebenläufige Programme können viel höhere Ressourcen ausnutzen.

Nebenläufige Programme erfordern Kommunikation und Synchronisation zwischen
Prozessen. Fehlerfreie und effiziente Synchronisation und Kommunikation sind
schwierig. Probleme sind häufig situations- und zeitabhängig und daher schwierig
zu reproduzieren, diagnostizieren und korrigieren.

.. glossary::

  parallel
    bezeichnet typischerweise echte :term:`Parallelität`

  nebenläufig
    bezeichnet mögliche Parallelität, d.h. echte Parallelität und
    :term:`Quasiparallelität`

Systemarchitekturen
===================

Einzelkernprozessoren
---------------------
Es kann höchstens eine Instruktion zu jedem Zeitpunkt durchgeführt werden.
Es lässt sich Quasiparallelität durch Multitasking erreichen. Durch
Quasiparallelität lässt sich selbst in Einprozessor-Systemen er Durchsatz
deutlich erhöhen.

Mehrkernprozessoren
-------------------
Mehrkernprozessoren haben mehrere Prozessoren auf einem Chip und teilen sich
einen gemeinsamen Speicher.

.. figure:: mehrkernprozessoren_diagramm.png
	 :alt: mehrkernprozessoren_diagramm

Multiprozessor Systeme
----------------------
Mehrere Prozessoren haben je eigenen lokalen Speicher, nutzen aber oft einen
globalen Speicher gemeinsam.

.. figure:: multiprozessorsysteme_diagramm.png
	 :alt: multiprozessorsysteme_diagramm

Mit einem Cash-Koherenzprotokoll lässt sich sicherstellen, dass die verschiedenen
Cashes die gleichen Werte enthalten

Verteilte Systeme
-----------------
Verteilte Systeme haben eine Menge von Knoten die sich keinen Speicher teilen.
Rechnernetze ermöglichen die Kommunikation zwischen Knoten. Bei den Knoten handelt
sich dann wieder um Multiprozessor- oder Ein-/Mehrkernprozessor-Systeme

.. figure:: verteiltes_system.png
	 :alt: verteiltes_system

So kann man Parallelität zwischen den Knoten, zwischen Prozessoren eines Knotens
und zwischen den Kernen eines Prozessors erreichen.
