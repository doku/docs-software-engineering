=================================
Systemkonzepte und Programmierung
=================================

.. toctree::
  :maxdepth: 2
  :glob:

  parallele_ablaeufe/*
  verarbeitungs_modelle/*
  concurrency_problems/*
  referenz_architektur/*
  maschinen_modelle/*
  modeling_concurrent_programms/*
  correctness_analysis/*
  betriebssystem/*
  history_systems/*
  prozesse_threads/*
  scheduling/*
  synchronisation/index
  rechnernetze/index
  kommunikation/index
  global_state/index

Übersicht
=========

- Rechnernetze

  - Three-Way-Handshake
  - DNS
  - Sockets

- Nachrichtenaustausch

  - Erlaubnisbasierter Algorithmus (Ricard und Agrawala)
  - Token-Passing (Ricard und Agrawala)
  - RPC/RMI

- Globaler Zustand

  - DS Algorithmus
  - Kreditmethode
  - Globaler Snapshot CL
