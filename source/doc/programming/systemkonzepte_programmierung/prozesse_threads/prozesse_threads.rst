.. _processes-threads:

====================
Prozesse und Threads
====================

Prozesse kümmern sich nicht selbst um das Management der Rechnerarchitektur. Das
System verwaltet die Ressourcen und entscheidet, welcher Prozess abläuft.

Die Systemsoftware muss Kontrolle über die Rechnerarchitektur erhalten.


Es werden unterschiedliche Modi für das Ausführen von Befehlen benötigt, sodass nur
die Systemsoftware privilegierte Befehle ausführen kann. Mit Unterbrechungen (UBR)
auch Interrupts (INT) können Programme gewechselt werden.

Durch einen UBR wird ein Prozess am :math:`i` ten Befehl unterbrochen. Die
UBR-Behandlung übernimmt die Kontrolle und der Prozess wird danach wieder an
Stelle :math:`i+1` fortgesetzt.

.. figure:: interrupt_cycles.png
	 :alt: interrupt_cycles


Dispatcher
==========

Der Dispatcher entscheidet welcher Prozess auf einer Rechnerarchitektur abläuft.

#. Erfasse und sichere Ausführungszustand des Prozesses
#. Wähle neuen Prozess
#. Weise Prozess benötigte Ressourcen zu
#. Initialisiere Prozessor
#. Übergebe Kontrolle

Der Dispatcher übernimmt die Ausführung nach einer UBR.

Prozesse arbeiten auf einem virtuellen Adressraum, der teils im Hauptspeicher und
teils auf der Platte liegt.
Die UBR signalisiert einen Seitenfehler, wenn eine Adresse auf welche ein Prozess
zugreifen will nicht im Hauptspeicher ist

Prozesskonzepte
===============

- Prozesskontrollblock (PCB)

.. figure:: pcb.png
	 :alt: pcb

Der Zustand des Betriebssystems besteht aus der Menge der Kontrollblöcke.

.. figure:: modell_5_zustaende.png
	 :alt: modell_5_zustaende

Bei vielen Prozessen ist nicht genügend Speicher pro Prozess vorhanden, wodurch
viele Seitenfehler entstehen. Daher werden Prozesse die nicht ausführbar sind
ausgelagert (swapping). Der Prozess befindet sich dann im Zustand **suspendiert**,
wenn er auf die Platte ausgelagert wurde.

.. figure:: modell_7_zustaende.png
	 :alt: modell_7_zustaende



.. figure:: thread_states.png
	 :alt: thread_states

Erstellen eines Neuen Prozesses
===============================

Prozesswechsel
==============



Threads
=======

.. glossary::

	Multithreading
		Bei Multithreading können mehrere Entitäten in der gleichen Ressourcenumgebung
		ausgeführt werden und auf den gleichen Ressourcen kooperieren

.. figure:: prozesse_und_threads.png
	 :alt: prozesse_und_threads

- Einfache und leichtgewichtige Kommunikation, da sich Threads Speicher und Dateien teilen.

Die Suspendierung und Terminierung eines Prozesses betrifft alle Threads.

Vorteile von Threads
--------------------

#. Überlappung
#. Asynchrone Verarbeitung
#. Höhere Performanz
#. Modularer Programmaufbau

Die Verwaltungs

Thread Implementierung
----------------------

- auf Benutzerebene: ermöglicht benutzerdefiniertes Scheduling, aber Problem mit blockierenden Zuständen
- im Kern: Kern verwaltet Kontext von Prozessen und Threads, aber deutlich weniger effizient
- Hybride Implementierung: Multiplexing von Threads der Benutzerebene auf Threads im Kern

Java Threads
------------

.. code-block:: java

	public class Runnable_impl implements Runnable {
		private static int n = 0; // shared variable

		public void run(){
			...
		}
 	}

	public class Thread_impl extends Thread {
		public void run() {
			try {
				...
			} catch(InterruptedException e) { // Thread Implementierung muss Interrupt selbst behandeln
				...
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Thread t1 = new Thread(new Runnable_impl());
		Thread t2 = new Thread_impl();

		t1.start();
		t2.start();

		t1.join();
		t2.join(40);
		t2.interrupt();
	}
