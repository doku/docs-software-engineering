====================================
Modellierung nebenläufiger Programme
====================================

.. glossary::

  Modelle
    Modelle mit geeigneten Abstraktionen unerlässlich zur Bewältigung und
    Verständnis komplexer Systeme und Abläufe.

  Atomarer Befehl
    Ein Atomarer Befehl wird vollständig ohne Unterbrechung ausgeführt. Dadurch
    können keine Verschachtelung mit anderen Befehlen auftreten. Ein atomarer
    Befehl wird ganz oder gar nicht ausgeführt.

    Nach Intel spec: 32-Bit Werte Lesen und Schreiben sind atomar.

Der CPU-Zyklus besteht aus einer Ladephase und einer Ausführungsphase.
Wir laden den Befehl aus dem Hauptspeicher in den Befehlsregister (Ladephase)
und dann führt der Prozessor den Befehl aus (Ausführungsphase).

.. figure:: cpu_zyklus.png
	 :alt: cpu_zyklus

Unterbrechungen des Programms sind nur am Ende der Ausführungsphase möglich.

Es ist unterschiedliche Granularität von Atomarität möglich. Atomare Operationen
beziehen sich immer auf ein bestimmtes Maschinenmodell(Stapel, Register etc.).
Bei falscher Granularitätsannahme ist inkorrekte (nebenläufige) Programmausführung
möglich.

.. hint::
  Grundsätzlich gilt: Ein Befehl wird atomar ausgeführt, genau dann wenn der
  Befehl maximal einen Zugriff auf gemeinsame Ressourcen verwendet.

  - :math:`x = n+1` ist atomar, falls :math:`x` lokal und :math:`n` geteilt
  - :math:`x = ++n` ist nicht atomar,  falls :math:`x` lokal und :math:`n` geteilt
  - Lesen und Schreiben auf gemeinsamen Speicher ist atomar für grundlegende
    Datentypen (Integre, Double), nicht aber für Datenstrukturen (Array, Struct)

Zur Modellierung von Nebenläufigkeit betrachten wir nur Zustandsübergänge.
Nach Ausführung eines atomaren Schrittes ändert sich der Zustand.

Ein Programm definiert Befehle, ein Prozess ist ein Programm, in der Ausführung.

Prozess
=======
Ein Prozess beschreibt den Ausführungszustand eines Programms.

.. glossary::

  Prozess
    Ein Prozess :math:`p` besteht aus einem Tupel :math:`(P,Z)` mit

    - :math:`P = \{p_1, ..., p_n\}`: eine Zerlegung des Programmtextes in atomare Befehle
    - :math:`Z = \{z_1, ..., z_m\}`: die Menge vor Kontroll- und Zustandsinformationen

    Insbesondere definiert :math:`c_p=p_i` den in einer Ausführung als nächstes
    ausführbaren Befehl.

    .. figure:: prozess_definition.png
    	 :alt: prozess_definition

Prozesse benötigen Ressourcen (CPU, Speicher etc.) zur Ausführung.

Die Kontroll- und Zustandsinformationen ermöglichen die Sicherung des
Ausführungszustands eines Programms. Das ermöglicht :term:`Quasiparallelität` in
Uniprozessorsystemen und die Migration von Prozessen zwischen Prozessoren auf
Mehrkernprozessorsystemen. Dafür wird aber Scheduling benötigt.

Interaktion von Prozessen
=========================
Mehrere Prozesse können sich Ressourcen teilen. Das ermöglicht die Interaktion
zwischen Prozessen.

Alternativ können Prozesse durch Kommunikation interagieren. Hier ändert sich
der Zustand aufgrund des Austauschs von Nachrichten. Das ermöglicht die Interaktion
von Prozessen auf unterschiedlichen Systemen und Systemen ohne gemeinsamen Speicher.

Nebenläufigkeit
===============
Ein nebenläufiges Programm beschreibt die gleichzeitige Ausführung mehrerer interagierender
Prozesse auf einem oder mehrerer Prozessoren.

.. glossary::

  nebenläufiges Programm
    Ein nebenläufiges Programm besteht aus einer endlichen Menge von interagierenden
    Prozesse :math:`\Pi = \{p^1, ..., p^l\}`, die auf einer Menge von Ressourcen
    :math:`R_{\Pi}` ausgeführt werden. Hierbei gilt:

    #. Die Ausführung kann durch eine Sequenz von atomaren Befehlen beschrieben
       werden, in der jeweils genau ein Prozess voranschreitet.
    #. Paare von Prozessen können sich Ressourcen aus :math:`R_{\Pi}` teilen und
       deren Belegung durch Ausführung eines Befehls jeweils ändern.
    #. Nach Ausführung eines atomaren Befehls auf eine gemeinsame Ressource
       :math:`r \in R_{\Pi}` sind Veränderungen der Belegungen von :math:`r`
       im nächsten Ausführungsschritt sichtbar.

Zustand
=======

Der Zustand ist  eindeutig über Ausführungssequenz definiert. Der Zustand eines
nebenläufigen Programms ist eine Menge von Kontroll- und Zustandsinformationen
sämtlicher beteiligter Prozesse.

.. figure:: zustand_beispiel.png
	 :alt: zustand_beispiel

Jeder atomare ausgeführte Befehl eines nebenläufigen Programms beschreibt einen
Zustandsübergang.


.. figure:: zustandsuebergaenge.png
	 :alt: zustandsuebergaenge

Ohne Kontrollinformation und Koordination sind beliebige Verschachtelungen der
Prozesse möglich.

Bei den Verschachtelungen muss die Sequentielle Anordnung der atomaren Programmschritte
eines Prozesses respektiert werden. (:math:`p_2, p_1, q_2, q_1` ist daher keine
mögliche Verschachtelung)

.. note:: Die Ausführungssequenz beeinflusst die Ausführungssemantik.

.. figure:: zustandsuebergaenge_concurrent.png
	 :alt: zustandsuebergaenge_concurrent

Szenario
========
Ein Szenario wird durch eine Folge von Zuständen definiert.


.. figure:: szenario.png
	 :alt: szenario

Multitasking Systeme
====================
Ein Multitaskingsystem ist Software, die den Ablauf mehrere Prozesse auf einer
CPU unterstützt.

Ein Prozesswechsel erfordert:

#. System sichert den Zustand des aktiven Prozesses
#. System wählt nächsten Prozess :math:`P_k` aus
#. Weist :math:`P_k` die benötigten Ressourcen zu
#. Übergibt Kontrolle an :math:`P_k`

Auf der Systemsicht hat man einen Kontextwechsel zwischen Betriebssystem und
Prozessen.

Bei unabhängigen Prozessen führen beliebige Verschachtelungen atomarer Befehle
zum selben Endzustand.

Multiprozessor Systeme
----------------------

.. glossary::

  Ausführung auf Multiprozessor System
    In einem Multiprozessorsystem führen je zwei Sequenzen von Zustandsübergängen
    in einem nebenläufigen Programm zum selben Endzustand, wenn sie folgende
    Eigenschaften erfüllen:

    #. Die sequentielle Ordnung der Befehle eines Prozesses bleibt erhalten
    #. Die Ausführungsordnung von Befehlen auf atomar veränderbaren Ressourcen
       bleibt erhalten.

Das erlaubt für jeden Zustand die Betrachtung von nur einer repräsentativen
Ausführungssequenz, die Eigenschaften 1 und 2 erfüllt.

Verteilte Systeme
-----------------

Der Zustand des Programms ist abhängig von Interaktionen der Kommunikation.

.. glossary::

  Ausführung im Verteilten System
    In einem Verteilten System führen je zwei Sequenzen von Zustandsübergängen in
    in einem nebenläufigen Programm zum selben Endzustand, wenn sie folgende
    Eigenschaften erfüllen:

    #. Die sequentielle Ordnung der atomaren Befehle eines Prozesses bleibt erhalten
    #. Die Ausführungsordnung von Befehlen unter Einhaltung der Sende/Empfangs-Relationen
       bleibt erhalten
