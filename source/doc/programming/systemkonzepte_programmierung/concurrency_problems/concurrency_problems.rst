====================
Concurrency Problems
====================

Nebenläufige Programme erfordern Kommunikation und Synchronisation zwischen
Prozessoren. Die Fehlerfreie und effiziente Synchronisation und Kommunikation
sind schwierig. Probleme sind häufig situations- und zeitabhängig und daher
schwierig zu reproduzieren, diagnostizieren und korrigieren.

Race Condition (Lost Update Problem)
====================================

Eine Race Condition liegt vor, wenn das Ergebnis einer Berechnung vom zeitlichen
Verhalten einzelner Operationen abhängt.

.. figure:: race_condition_example.png
	 :alt: race_condition_example

:math:`n` kann nun sowohl :math:`10`, :math:`100` als auch :math:`110` sein.

Lost Updates

Deadlock (Verklemmung)
======================

Ein Deadlock liegt vor, wenn sich eine Menge von Prozessen zyklisch gegenseitig
blockieren.

.. figure:: deadlock_example.png
	 :alt: deadlock_example
