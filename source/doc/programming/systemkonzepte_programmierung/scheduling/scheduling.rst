==========
Scheduling
==========

Ein gutes Scheduling sollte möglichst effizient  sein und geringen Speicherverbrauch
haben (kaum overhead)

Drei Ebenen des Schedulings
===========================

.. figure:: scheduling_ebenen.png
	 :alt: scheduling_ebenen

Das Langfristige Scheduling entscheidet welche Programme auf einem Rechnersystem
ablaufen dürfen.

Mittelfristiges Scheduling entscheidet, welche Prozesse suspendiert/fortgesetzt
werden.

Das Lang- und Mittelfristige Scheduling kontrollieren den Grad der Multiprogrammierung
auf einem Rechner.

Kurzfristiges Scheduling entscheidet welche Prozesse/ Threads auf dem Prozessor
ablaufen. Dabei gibt es unterschiedliche Verfahren für Uni-, Multiprozessor oder
Echtzeitsysteme.

.. figure:: scheduling_prozesszustaende.png
	 :alt: scheduling_prozesszustaende

- Je mehr Prozesse, desto effizientere Ausnutzung von I/O
- Je mehr Prozesse, desto weniger Lokalität pro Prozess im System

Trashing tritt bei zu vielen Seitenfehlern wegen fehlender Lokalität auf.

.. figure:: trashing.png
	 :alt: trashing

Durch Suspendierung von Prozessen, können die restlichen effizienter Laufen.

Leistungskriterien für Scheduling
=================================

- Auslastung: Prozessor möglichst immer beschäftigen
- Durchsatz: Anzahl der erledigten Tasks pro Zeiteinheit
- Antwortzeit: Ausführung und Warten im ablauffähigen Zustand
- Durchlaufzeit: Zeit einen bestimmten Prozess auszuführen (Ausführung + Wartezeit inkl. I/O)
- Fairness: Jeder Prozess macht Fortschritt (wichtig für :ref:`liveness`)

Entscheidungsmodi
=================

.. glossary::

	nicht präemptiv
		Ein ablaufender Prozess führt so lange auf dem Prozessor aus, bis er blockiert
		oder terminiert. Dies wird häufig für Batch-Systeme verwendet.

	präemtiv
		Ein ablaufender Prozess kann unterbrochen werden, auch wenn er noch bereit ist.
		Dies ist nötig für interaktive Anwendungen.

Nicht Präemtives Scheduling
===========================

FIFO Scheduling
---------------
Die Priorität eines Prozesses wird durch die Wartezeit in der
Bereitwarteschlange bestimmt. Der Prozess mit größter Wartezeit
wird als nächstes aktiv. Die Prozesse werden nach Einfüge Reihenfolge in die
Bereitwarteschlange ausgeführt.

Beim FIFO Scheduling gibt es geringen Aufwand für den Dispatcher.

Prozesse mit langen CPU-Bursts behindern Prozesse mit kurzen
CPU-Bursts. Dadurch entsteht der Konvoy-Effekt und viele
Prozesse warten in der Bereitwarteschlange. Das ist besonders ungünstig für den
Durchsatz.

.. figure:: fifo_scheduling_beispiel.png
	 :alt: fifo_scheduling_beispiel

Shortest Process Next Scheduling (SPN)
--------------------------------------

Die Priorität von Prozessen wird durch die Länge des CPU-Bursts gestimmt.
Der Prozess mit kürzestem CPU-Burst wird als nächstes aktiv.

Eine mögliche Implementierung ist ein Min-Heap mit Sortierung nach der
geschätzten Dauer des CPU-Bursts.

SPN minimiert **durchschnittliche Antwortzeit** und maximiert **Durchsatz**.
Es ist beweisbar optimal für :term:`nicht präemtiv` Uniprozessor Scheduling.

Schätzung: mit exponential moving average

.. math:: t = \alpha lastExpected + (1-\alpha) thisTimeActual

So lässt sich die Historie in einer Zahl speichern.

Werte die Lange in der Vergangenheit liegen, bekommen nur exponentiell wenig
gewicht.

FIFO vs. SPN
------------

.. figure:: fifo_spn_vergleich.png
	 :alt: fifo_spn_vergleich

Problem von FIFO und SPN
------------------------
Prozesse mit niedriger Priorität werden niemals ausgeführt und verhungern. Damit
kann keine :ref:`fairness` und somit auch keine :ref:`liveness` gewährleistet
werden

Präemptives Scheduling
======================

Unterbrechung von aktiven Prozessen möglich. Dadurch Übergang ``Aktiv -> Bereit``
möglich. So lasst sich die Ausführungszeit eines Prozesses begrenzen und
:ref:`fairness` in der Ausführung erreichen

Round Robin Scheduling
----------------------
Ein Faires Scheduling wobei jeder Prozess eine Zeitscheibe der Prozessorzeit
erhält. Ohne Betrachtung von I/O  bekommt so jeder Prozess den gleichen Anteil an
der Gesamtprozessorzeit.

.. figure:: round_robin_scheduling.png
	 :alt: round_robin_scheduling

Das Zeitquantum entscheidet über den Dispatcher Aufwand. Bei einem großen
Zeitquantum ist das Verhalten ähnlich zu FIFO und bei einem kleinen Zeitquantum
hat man hohen Aufwand durch Kontextwechsel.

Multilevel Feedback Queue
-------------------------

Es gibt mehrere Bereit Warteschlangen mit verschiedenen Prioritäten.

Es wird zuerst die Warteschlange mit der höchsten Priorität abgearbeitet. Kann ein
Prozess innerhalb des Zeitquantums der jeweiligen Prioritätslevel nicht fertiggestellt
werden, so wird er in die nächst niedrigere Prioritätswarteschlange verschoben.

.. figure:: multilevel_feedback_queue.png
	 :alt: multilevel_feedback_queue

Die niedrig priorisierten Queues haben ein höheres Zeitquantum als die hoch
priorisierten Queues (:math:`RQ_0`). Das ermöglicht eine Annäherung an SPN.

Mulitprozessor Scheduling
=========================

.. glossary::

  Semetric Multiprocessing (SMP)
    Alle Prozess kann auf allen Cores ausgeführt werden.

Das Gleichzeitige Ablaufen interaktiver Threads verbessert die Durchlaufzeit eines
Prozesses. Interagierenden Threads wird eine Partition exklusiv zugewiesen und
gleichzeitig gescheduled.

Threads führen erst aus, wenn die entsprechende Partition verfügbar ist (dadurch
lange Wartezeiten bis entsprechend große Partition verfügbar ist).

Load-Sharing
------------
Es gibt eine globale Warteschlange für Threads. Sobald ein Prozessor frei wird,
wird diesem ein Thread aus der Warteschlange zugewiesen.

.. figure:: loadsharing.png
	 :alt: loadsharing

Space Sharing
-------------
Die Prozessoren werden partitioniert. Interagierenden Threads wird eine Partition
exklusiv zugewiesen. Threads einer Partition werden gleichzeitig gescheduled.

Da Threads erst ausgeführt werden, wenn eine entsprechende Partition verfügbar ist,
haben große Partitionen lange Wartezeiten.

Gang Scheduling
---------------
Interagierende Threads werden zu einer Gang gruppiert. Eine Gang wird in einem
Schritt auf mehreren Prozessoren ausgebracht (Space-Sharing) und die
Ausführungszeit wird durch ein Zeitquantum beschränkt (Time-Sharing). Das
Zeitquantum beginnt und endet für alle Prozessoren zum gleichen Zeitpunkt.

Die Gruppierung von Threads führt zu nicht genutzter Prozessorzeit. Zur Optimierung
kann man Thread-Gruppen gemäß ihrer Ausführungszeit gewichten.
