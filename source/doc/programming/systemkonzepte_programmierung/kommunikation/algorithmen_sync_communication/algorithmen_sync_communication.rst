========================================
Algorithmen mit synchroner Kommunikation
========================================

Synchroner Kanal
================

.. figure:: sync_kanal.png
	 :alt: sync_kanal

Lauflängenkodierung
-------------------

.. figure:: lauflaengenkodierung.png

   lauflaengenkodierung


Matrix Multiplikation
=====================

.. figure:: matrix_mult.png
	 :alt: matrix_mult
