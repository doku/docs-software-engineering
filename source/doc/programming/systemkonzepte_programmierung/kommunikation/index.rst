=============
Kommunikation
=============

.. toctree::
  :glob:

  nachrichten/*
  algorithmen_sync_communication/*
  algorithmen_async_communication/*
  remote_procedure_call/*
  remote_method_invocation/*
