.. _remote-procedure-call:

===========================
Remote Procedure Call (RPC)
===========================

Beim synchronen RPC wird der Client während der Dauer der Ausführung der entfernten
Prozedur blockiert. Damit gibt es aus Sicht der einzelnen Anwendung/Thread
eine rein sequentielle Ausführung.

.. figure:: syncron_rpc.png

   syncron_rpc

.. figure:: rpc_architecture.png
	 :alt: rpc_architecture

Im Client-Server-Modell: Der Klient ruft Prozeduren einer Schnittstelle auf, und
der Server implementiert diese Schnittstelle.

IDL
===

Schnittstellen können über **Interface Definition Language (IDL)** definiert werden.

Der IDL-Compiler erzeugt die Klient- und Server-Rumpfprozeduren. Rumpfprozeduren
sind programmiersprachenunabhängig.

.. figure:: idl.png
	 :alt: idl

Binden
======

Als Binden bezeichnet man den Vorgang der Zuordnung eines Servers an einen
Klienten.

Transparenz
===========

Bei entfernten Aufrufen ist die Art der Parameterübergabe ein kritischer Faktor
hinsichtlich des Kommunikationsaufwands.

Parameterübergabe
=================

Call-by-Value
-------------

.. figure:: call_by_value.png
	 :alt: call_by_value

Ideal hinsichtlich Kommunikationsaufwand bei kleinen Datenstrukturen und vielen
Zugriffen innerhalb der Prozedur.

Call-by-Reference
-----------------

.. figure:: call_by_reference.png
	 :alt: call_by_reference

Ideal hinsichtlich Kommunikationsaufwand bei wenigen Zugriffen durch Prozedur und
großen Datenstrukturen, von denen nur kleine Teile gelesen/geschrieben werden.

Call-by-Copy/Restore
--------------------

.. figure:: call_by_copy_restore.png
	 :alt: call_by_copy_restore

Ideal hinsichtlich Kommunikationsaufwand bei kleinen Datenstrukturen und vielen
Zugriffen innerhalb der Prozedur.

Fehlersemantik
==============

Wenn keine Fehler auftreten, garantieren alle RPC-Systeme, dass eine entfernte
Operation genau einmal (exactly once) ausgeführt wird.

.. glossary::

  Maybe
    Eine Anfrage wird entweder überhaupt nicht oder einmal ausgeführt.

    .. figure:: maybe_rr.png
    	 :alt: maybe_rr

  At-Least-Once
    Eine Anfrage wird mindestens einmal ausführt. Bei Knotenfehlern wird die
    Ausführung nicht garantiert.

    Da mehrfache Ausführung einer
    Operation möglich ist, ist diese Fehlersemantik nur für idempotente Operationen
    geeignet

    .. figure:: at_least_once_rr.png
    	 :alt: at_least_once_rr

  At-Most-Once
    Eine Anfrage wird höchstens einmal ausgeführt. Treten keine Knotenfehler auf,
    so wird die Anfrage sogar genau einmal (exactly once) ausgeführt.

    .. figure:: at_most_once_rra.png
    	 :alt:     at_most_once_rra

    Der Client-Stub speichert die Anfrage in flüchtigem Speicher und sendet diese
    periodisch bis eine Antwort ankommt. Wenn die Antwort ankommt, verwirft er die
    Anfrage und sendet ``ACK`` an Server.

    Der Sever-Stub hat eine Duplikats Erkennung basierend auf der Anfrage-ID. Er
    speichert die Antwort nach Ausführung der Prozedur und sendet diese Antwort
    solange, bis ``ACK`` ankommt. Er verwirft die gespeicherte Antwort, wenn
    ``ACK`` ankommt.

.. figure:: fehlersemantik.png
	 :alt: fehlersemantik

Protokoll
=========

.. figure:: protokoll_klassen.png

	 protokoll_klassen
