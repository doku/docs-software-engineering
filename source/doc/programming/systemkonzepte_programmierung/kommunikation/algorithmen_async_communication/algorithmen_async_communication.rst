=========================================
Algorithmen mit asynchroner Kommunikation
=========================================


.. figure:: async_notation.png
	 :alt: async_notation

Eventbasierter Algorithmus (Ricard und Agrawala)
================================================

.. figure:: regeln.png
	 :alt: regeln

.. figure:: prinzip.png
	 :alt: prinzip

Prozess hat Sicht auf eine virtuelle Warteschlange ``W``.

.. figure:: ricart_agarwala_main.png
	 :alt: ricart_agarwala_main

.. figure:: ricart_agarwala_receive.png
	 :alt: ricart_agarwala_receive

Wechselseitiger Ausschluss gilt für den Algorithmus.
Der Algorithmus ist frei von Aushungern und somit frei von Verklemmung.

Pro Eintritt in den kritischen Abschnitt gibt es bei :math:`N` Prozessen
:math:`(N-1)` requests und :math:`(N-1)` replies. Der Aufwand ist unabhängig
davon, ob ein Wettstreit existiert oder nicht.

Token Passing
=============

.. figure:: token_passing_prinzip_1.png
	 :alt: token_passing_prinzip_1

.. figure:: token_passing_prinzip_2.png
	 :alt: token_passing_prinzip_2

.. figure:: token_passing_prinzip.png
	 :alt: token_passing_prinzip

.. figure:: token_passing_prinzip_3.png
	 :alt: token_passing_prinzip_3

``requested`` und ``granted`` dienen dazu um in einem Prozess festzustellen welche
Tokenanfragen noch abgearbeitet werden müssen, und welche bereits von anderen
Prozessen ``granted`` wurden.


Algorithmus
-----------

.. figure:: token_passing_main.png
	 :alt: token_passing_main

.. figure:: token_passing_receive.png
	 :alt: token_passing_receive

.. figure:: token_passing_send.png
	 :alt: token_passing_send

Beispiel
--------

.. figure:: token_passing_bsp.png
	 :alt: token_passing_bsp

Aufwand
-------
Falls der Prozess das Token nicht besitzt macht er :math:`(N-1)` requests und bekommt
eine Tokennachricht. Diese enthält das Token und ``granted``, sodass die Länge der
Nachricht von :math:`N` abhängt.

Korrektheit
-----------
Der Algorithmus ist verklemmungsfrei.

Es wurde angenommen, das Präprotokoll, Postprotokoll und receive atomar ausgeführt
werden. Ist das nicht gegeben, so wechselseitiger Ausschluss nicht gegeben.

.. figure:: atomaritaet.png
	 :alt: atomaritaet

Durch nicht deterministische Auswahl des wartenden Prozesses welcher als nächstes
das Token bekommt kann es zur Aushungerung kommen.
