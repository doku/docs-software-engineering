===========
Nachrichten
===========

send and receive
================

.. glossary::

  nichtblockierendes Send
    Der Sender ist nur für die Zeit des Kopiervorgangs blockiert.

  blockierendes Send
    Der Sender wird solange blockiert, bis die dazugehörige receive Operation
    beendet ist.

  blockierendes receive
    Ein Prozess, der receive aufruft, wird solange blockiert, bis eine Nachricht
    eingetroffen ist.

  nichtblockierendes receive
    Ein Prozess, der receive aufruft, wird nie blockiert:
    Wenn eine Nachricht vorhanden, so wird die Nachricht zurückgegeben, sonst
    negativer Rückgabewert.

.. figure:: send_receive_1.png
	 :alt: send_receive_1

In der Praxis ist ``send`` nicht blockierend und ``receive`` ist blockierend.
