==============================
Remote Method Invocation (RMI)
==============================

RMI ist eine Erweiterung des :ref:`remote-procedure-call` zur Unterstützung von
objektorientierter Technologie.

.. figure:: object_modell.png
	 :alt: object_modell

.. figure:: architecture_rmi.png
	 :alt: architecture_rmi

.. figure:: parameter_uebergabe.png
	 :alt: parameter_uebergabe

RMI kennt lokale und remote Objekte. Es gibt keine Lokationstransparenz.

Ein entferntes Objekt implementiert entfernte Schnittstelle (Remote Interface).
Jede Methode der Schnittstelle deklariert ``RemoteExceptions``.

- UniCastRemoteObject

- RMIRegistry

.. figure:: rmi_registry.png
	 :alt: rmi_registry

Java
====

.. code-block:: java

	public interface Hello extends Remote {
    	String sayHello() throws RemoteException;
	}

.. code-block:: java

	public class Server implements Hello {

	    public Server() {}

	    public String sayHello() {
	        return "Hello, world!";
	    }

	    public static void main(String args[]) {

	        try {
	            Server obj = new Server();
	            Hello stub = (Hello) UnicastRemoteObject.exportObject(obj, 0);

	            // Bind the remote object's stub in the registry
	            Registry registry = LocateRegistry.getRegistry();
	            registry.bind("KEY", stub);

	            System.err.println("Server ready");
	        } catch (Exception e) {
	            System.err.println("Server exception: " + e.toString());
	            e.printStackTrace();
	        }
	    }
	}

.. code-block:: java

	public class Client {

	    private Client() {}

	    public static void main(String[] args) {

	        String host = "localhost";
	        try {
	            Registry registry = LocateRegistry.getRegistry(host);
	            Hello stub = (Hello) registry.lookup("KEY");
	            String response = stub.sayHello();
	            System.out.println("response: " + response);
	        } catch (Exception e) {
	            System.err.println("Client exception: " + e.toString());
	            e.printStackTrace();
	        }
		   }
	}
