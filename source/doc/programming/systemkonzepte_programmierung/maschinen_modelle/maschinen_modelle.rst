================
Maschinenmodelle
================
Das Maschinenmodell bestimmt die Repräsenation des Programmzustands. Beim
Registermodell definieren Registerinhalte den Zustand und beim Stapelmodell
definiert der Stapelzustand den Zustand.

Bei nebenläufigen Prozessen hat jeder Prozess seinen eigenen Stapel und
Register. Wird ein Befehl von Prozess ``p`` ausgeführt und danach ein
Befehl von Prozess ``q`` so wird der Stapel und Register von ``p`` extern
zwischen gespeichert, der aktuelle Stapel/Register von ``q`` geladen und
erst dann der Befehl von ``q`` ausgeführt.

Zwei Prozesse arbeiten also nicht auf dem gleichen Stapel oder Register.

Registermaschine
================
Die Prozessorbefehle sind die atomaren Bestandteile eines Programms.

Der Prozessor kann mit einer begrenzten Zahl an Registern rechnen.

.. figure:: registermaschine_example.png
	 :alt: registermaschine_example

- ``LOAD R1, a``: Lade den Wert von ``a`` aus dem Speicher in Register ``R1``
- ``ADD R1, b`` Addiere den Wert von ``b`` zu ``R1``
- ``STORE R1, c`` Schreibe den Wert von ``R1`` in den Speicher, der ``c`` enthält

Beim Registermodell wird der Programmzustand durch den Registerinhalt repräsentiert.

Stapelmaschine
===============
Operationen auf dem Stapel sin die atomaren Bestandteile eines Programms.

.. figure:: stapelmaschine_example.png
	 :alt: stapelmaschine_example

Stapelmaschinen können auf mehreren, sehr unterschiedlichen Rechnerarchitekturen
implementiert werden, da sie auf einer höheren Abstraktionsebene sind als die
registerbasierte Ausführung.

Beim Stapelmodell ist der Programmzustand durch den Stapelzustand bestimmt.
