====================
Verarbeitungsmodelle
====================

Produzent/Konsument-Modell
==========================

.. figure:: produzen_konsument_modell.png
	 :alt: produzen_konsument_modell

Der Produzent erzeugt die Daten und sendet diese zum Konsumenten. Der Konsument
erhält die Daten und verarbeitet sie. Die Kommunikation ist **unidirektional**.

Pipeline-Modell
===============

.. figure:: pipeline_modell.png
	 :alt: pipeline_modell

Die Pipeline ist eine Verallgemeinerung des Produzent/Konsument-Modells. Die Prozesse
sind sowohl Produzenten als auch Konsumenten. Die Prozesse sind über unidirektionale
Kommunikationskanäle verbunden.

Client/Server-Modell
====================

.. figure:: client_server_modell.png
	 :alt: client_server_modell

Der Server bietet seine Dienste einer vorher unbekannten Anzahl von Klienten an.
Die Interaktionen beruht auf einem Auftrag/Antwort System.

Server können Klienten anderer Dienste sein.
