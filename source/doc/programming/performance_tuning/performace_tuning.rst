==================
Performance Tuning
==================

.. danger::
  Premature optimization is the root of all evil. (Donald Knuth)

Most of the code has negligible impact on performance. Only optimize
the code that is taking time.

You should not attempt to optimize code until you have measured it. You should
then do the optimization and measure again. If we did not achieve a significant
improvement we should reverse the change.

.. hint:: optimization without good performance data is a waste of time.

Algorithm replacement is vastly more effective than code fiddling.

- Schichtenübergreifende Algorithmen Optimierung

Größten Performance Verbesserungen durch Algorithmen Verbesserung auf fachlicher
Ebene.


Optimierungs Ziel
-----------------

- Responsetime
- CPU kosten

DB Explain
----------
Statistische Strategie für SQL Path:

Algorithmen Optimierung
-----------------------

- häufig benötigte Operationen: suchen, maximum, Sortieren
- Laufzeit der Operationen
- Erwartete Anzahl der Elemente

Zusätzliches Wissen über Fachlichkeit ermöglicht häufig bessere Lösungen.
(z.b. Großteils vorsortierte Liste, Hashing)


Initialer Entwurf:
- Effiziente Datenstrukturen verwenden und Algorithmen
- Aber keine aufwändigen/schwer lesbaren Performance Maßnahmen auf Verdacht

Fortlaufend:
- Messen und kritische Stellen identifizieren
- Probleme verstehen und gezielt verbessern
