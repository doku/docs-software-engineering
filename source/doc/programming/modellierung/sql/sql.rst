===
SQL
===

SQL (Structured Query Language)

SQL is a declarative query language (not a procedural, imperative language).
You use SQL to describe what you want and let the DBMS handle how that's actually
done.

.. figure:: sql_key_word_types.png
	 :alt: sql_key_word_types

Keywords
========

- Select
- From
- Where
- Order by
- Group by
- Join
- Insert into
- Update
- Delete
- Having
- in
- Distinct

.. code-block:: SQL

  SELECT columns FROM tableName;

If you want multiple columns you separate them by comma:

.. code-block:: SQL

  SELECT FirstName, LastName FROM Employee;

Select all columns with ``*``:

.. code-block:: SQL

  SELECT * FROM Employee;

With the ``WHERE`` clause you get only the rows that meet this condition.

When you have multiple databases you add the database name:

.. code-block:: SQL

  SELECT * FROM databaseName.Employee WHERE condition;

SQL is non sensitive to whitespace. So statements can go across multiple lines.

``WHERE`` Clause
================

Predicate

If you match against text you need to write that in quotes.

.. hint::
  String values in SQL are surrounded in **single quotes**.

.. code-block:: SQL

  Select *
  FROM Employee
  WHERE LastName = 'Green';

A single equals is used for comparison.

Numeric comparisons can be made with: ``<,>, >=, <=, <>``, where ``<>`` stands
for not equal.

With ``AND`` and ``OR`` you can combine multiple conditions.

.. code-block:: SQL

  SELECT * FROM Employee WHERE Department IN ('Marketing', 'Sales');

You can use ``LIKE`` instead of ``=`` to use wildcards in comparisons.

Use ``IS`` to check for ``NULL``:

.. code-block:: SQL

  WHERE MiddleName IS NULL;

  WHERE MiddleName IS NOT NULL;

Sorting Query Results
=====================

Sort query results using ``ORDER By``. By default the data is sorted in
ascending order. With ``DESC`` you can make it sort descending.


Aggregate Function
==================

.. code-block:: SQL

  SELECT COUNT(*) FROM PRODUCT;

  SELECT MAX(ListPrice) FROM PRODUCT;

MIN, AVG, SUM

.. code-block:: SQL

  SELECT COUNT(*), Color
  FROM Product
  GROUP BY Color;

.. figure:: sql_result_group_by.png
	 :alt: sql_result_group_by

JOIN
====

With Join you can select from multiple tables


By default a inner join is performed.

.. figure:: join_example.png
	 :alt: join_example


.. figure:: left_outer_join_example.png
	 :alt: left_outer_join_example


Insert
======

.. code-block:: SQL

  INSERT INTO table
    (column1, column2, ...)
    VALUES (value1, value2, ...)


Update
======

.. code-block:: SQL

  UPDATE table
  SET column = value
  WHERE condition

DELETE
======

.. code-block:: SQL

  DELETE FROM table
  WHERE CONDITION

Delete works on rows.

CREATE
======

.. code-block:: SQL

  CREATE Employee
  (EmployeeID INTEGER PRIMARY KEY,
   FirstName VARCHAR(35) NULL,
   LastName VARCHAR(100) NOT NULL
  );


ALTER
=====
Change a table with ``ALTER``.

.. code-block:: SQL

  ALTER TABLE Employee
  ADD Email VARCHAR(100);

DROP
====

Destroy a table:

.. code-block:: SQL

  DROP TABLE Employee;


Referential Integrity
=====================

.. figure:: ref_integrity_1.png

.. figure:: ref_integrity_2.png