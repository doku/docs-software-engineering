===============================
Entity-Relationship Model (ERM)
===============================

Ziel ist es die Objekte und die Operationen beschreiben zu können.

Entity
======

A entity is a thing that has real or individual existence in reality or in
mind. A entity is distinguishable from other objects.

Entities are dishinguishable through keys.

Entity Sets
===========

An entity set is a set of entities of the same type that share same
properties.

A predicate decides on the membership of an

A database contains a finite number of enitiy sets :math:`E_i`. These
entity sets are not necessarily disjoint.

Weak Entity Sets
================
Entity Set that does not have a primary key

Relationship
============
A relationship is a association among several entities.

Entity sets particiating an a relationsihp set :math:`R` do not have
to be disjoint.

The role name signigies the role that a participating entity from the
entity set plays

Relationship sets
=================

Cardinalities
=============
Cardinalities express the number of entities to which another entity can be
aoosciated via a relationship set.

- :math:`1 : 1`: one-to-one
- :math:`n : 1`: many-to-one
- :math:`n : m`: many-to-many

Relationship types define

Domains
=======

Attributes
==========

An entity is represented by a set of attributes, that is, descriptive properties
possessed by all members of an entity set.

- Derived Attributes

Key
===

A super key of an entity set is a set of one or more attributes whose values
uniquely determine each entitiy.

A cantidate key of an entity set is a minimal super key.

Primary keys
============
A primary key is one of the candidate keys that is distinguished as "the key" of
an entity set.

In ER Diagram mehrere Attribute unterstreichen, sodass die Kombanation aus beiden
der Primary Key ist.

ER-Diagramme
=============


.. figure:: er_elemente.png
	 :alt: er_elemente

Indicate Primary Key by underlining.


.. figure:: er_diagrams_1.png

.. figure:: er_diagrams_2.png

Component Aggregation: **component-of** - relationship (**co**)