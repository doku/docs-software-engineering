=========================
Introduction to databases
=========================

Having Data is not the problem. But it introduces new problems such as data size,
ease of updating, accuracy of the data, security, redundancy and importance.

Databases are not about providing a place to put your data. They are designed to
make data scalable, accessible, accurate, secure, consistent and permanent.

The true power of a database is its invisibility.

Databases are a structured system to put data.

The database management system (DBMS) is the program that manages the database.
(such as Oracle, SQL Server, DB2 etc.) The database is your data
and your rules about that data. DBMS fall into different categories. The most
common are the **relational database management systems** (RDBMS).

Additionally there are hierarchical database systems, network database systems,
object-oriented database systems and NoSQL database systems.

Tables
======

A database is constructed of one or more tables. The colums of a table define the
structure of the data in this table. Each column describes one piece of data, with
a name and a data type. Every row must follow this structure. By defining columns,
we are imposing rules on the data they store.

**Tables** and **columns** are defined up front. Day-to-day use is creating und
updating **rows**.

Key
===

A **key** is a way to identify just one particular row in any table.
It is created as one column that will contain a guaranteed unique value for each
row.

The **primary key** is the one piece of data that will identify one and only one
row in a table.

Generated primary keys are also called synthetic keys or surrogate keys.

Table Relationships
===================

These types of possible table relationships are also called cardinality.

one-to-many
-----------

.. figure:: table_relationships.png
	 :alt: table_relationships


many-to-many
------------
To create a many-to-many relationship we add another table (junction or linking table).

.. figure:: table_many_to_many.png
	 :alt: table_many_to_many

The junction table is used to setup two one-to-many relationships.

one-to-one
----------
One-to-one Relationships are not very common because they can just be combined
to one table.

.. figure:: table_one_to_one.png
	 :alt: table_one_to_one
