=========
DB Design
=========

A Database Design needs to be of high Quality:

-

.. figure:: db_design.png
	 :alt: db_design

Das Ziel von ERM ist es ein konzeptuellen DB entwurf zu machen und
Das Conzeptuelle schema ist unabhöngigt von der logichen und physischen
Abbbildung.

From the conceptual schema you can generate the logical schema and
physical schema.


- Superschlüssel: K bestimmt relation R
- Candidate Key: ist minimaler Super Key für R
- alle Attribute die in einem Candidate Key vorkommen sind Prim Attribute


Functional Dependencies
=======================


n-m modellierung: seperate relation machen


Closure Algorithm
-----------------

- you can use the closure algorithm to check if elements are super key. 
- just add elements that are implied by the potential keys step by step.
- check for minimality of super keys afterwards to determine minimality 

.. figure:: closure_1.png

.. figure:: closure_2.png

Normalization
=============

1NF
---
- each tuple contains **exactly one value** for each attribute
- Bereits in definition des Relationenmodells enthalten


.. figure:: 1nf_example.png


2NF
---
- Every non prime Attribute in R is fully dependent on every Candidate key in R

3NF
---

- 3NF prevents transitiv dependencies


BCNF
----

Sei R eine 3NF Relation die keine überlappenden Kanditatenschlüssel hat. Dann ist R in BCNF.	

- An attibut from wich oters are fully functionally dependent is called a **determinatte**
- A relation schema R is in BCNF if every determinate is a canditate key of R.