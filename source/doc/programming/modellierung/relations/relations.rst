=========
Relations
=========

https://bigdata.uni-saarland.de/datenbankenlernen/


.. figure:: relations_1.png

.. figure:: relations_2.png

.. figure:: relations_3.png

.. figure:: relations_4.png


Mapping of ERM to RM
====================

.. figure:: mapping_1.png


Mapping Generalizations
-----------------------


Relational Algebra
==================

Bei relationarler Algebra haben wir **Mengen-Semantik** also keine duplikate und keine Ordnung.


- Selection sigma

.. figure:: selection.png

- Projection

.. figure:: projection.png

- Vereinigung
- Differenz
- Kreuzprodukt

.. figure:: kreuzprodukt.png

- Umbenennung

.. figure:: umbenennung_1.png

.. figure:: umbenennung_2.png


Theta Join
    - Kreuzprodukt mit auswahl

.. figure:: theta_join.png    

join werden genutzt um an Fremdschlüssel entlang 

.. figure:: equi_join.png

.. figure:: natural_joint.png

Rewrite Rules for Relational Algebra
====================================

.. figure:: rewrite_rules.png

.. figure:: rewrite_rules_2.png

.. figure:: rewrite_rules_3.png

.. figure:: rewrite_rules_4.png