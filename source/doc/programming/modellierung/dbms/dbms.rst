====
DBMS
====

Datastructures for persistent storage need to be modeled differently than transient
Datastructures.

The notation of model is a set of language features to describe an universe of
discourse.

Databases make data application independent.

A Transaction is a unit of work.

Fault Tolarnce

.. glossary::

  database system (DBS)
    Is comprised by a database, which as a collection of stored data that is used
    by applications, and a database management system (DBMS) managing the
    database.

  DBMS
    A DBM is a tool for creating and managing large amounts of data efficiently
    and allowing it to persist over long periods of time, safely.

.. figure:: classical_data_models.png
	 :alt: classical_data_models

``XML`` is a hierarchical model.

Nur über die Kombination mit Metadaten können wir Datenwerten eine Semantik geben.

Transactions
============

A transaction is a combined unit of work. Either all things happen or non.
All changes will be reversed instantly if any part of the transaction fails.

ACID Properties
---------------

A transaction must be **atomic, consistent, isolated and durable** (ACID).

- atomic: the transaction must happen completely or not at all
- cosistency: any transaction must take the database from one valid state to another valid state
- isolation: the data in the transaction is locked for that moment in  which the transaction is occuring
- durable: if the the database says this transaction has happend successfully, then the transaction is guaranteed.

Transaction Processing
----------------------

A (on-line) transaction is the execution of a program that uses

A transaction is a non-interruptible sequence (atomic) of DB operations that
transforms the given logically consistent database state to new logically
consistent database state

Data Warehouse System
=====================

.. glossary::

  Data mining
    Data mining is the process of discovering hidden, previously unknown and
    useable information from a large amount of data. The data is analyzed without
    any expectation on the result. Data mining delivers knowledge that can be used
    for a better understanding of the data.
