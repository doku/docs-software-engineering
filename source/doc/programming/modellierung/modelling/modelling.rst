=========
Modelling
=========

A model is a concrete or mental illustration/description of an existing artefact or a
prescription of an artifact to be build.

Models are always influenced by the perception of the modeling person.

.. glossary::

  Model
    A description of (part of) a system written in a well-defined language.

    A description or specification of a system and its environment for some
    certain purpose. A model is often presented as a combination of drawings
    and text.

Characteristics of Models
=========================

- Abstract (Emphasize important aspects, hide irrelevant ones)
- Understandable (Expressed in a form readily understood by users)
- Accurate (Faithfully represent the modelled system)
- Predictive (Can be used to derive correct conclusions about the system)
- Inexpensive (Cheaper to construct and study than the system)
