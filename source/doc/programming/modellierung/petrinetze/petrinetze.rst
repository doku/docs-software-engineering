===========
Petri Netze
===========



Fallen
------
Wer etwas aus einer Falle Q herausnimmt, der legt auch etwas hinein.

Deadlocks
---------

Wer etwas in einen Deadlock Q hineinlegt, der nimmt (vorher) auch etwas raus.

