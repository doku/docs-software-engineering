=========================
Digitale Grundschaltungen
=========================

Transitoren
===========

n-Kanal-MOSFET
--------------

.. figure:: n-kanal-mosfet-aufbau.png
	 :alt: n-kanal-mosfet-aufbau

.. figure:: n-kanal-mosfet-schaltsymbol.png
	 :alt: n-kanal-mosfet-schaltsymbol

Wie geschlossener Schalter bei positiver Gate-Source-Spannung :math:`U_{GS}>U_{th}>0`
Legt man dann eine positive Drain-Source-Spannung :math:`U_{DS}` an, fließ ein Strom.

Eignet sich als pull-down Transistor.

p-Kanal-MOSFET
--------------

.. figure:: p-kanal-mosfet-aufbau.png
	 :alt: p-kanal-mosfet-aufbau

.. figure:: p-kanal-mosfet-schaltsymbol.png
	 :alt: p-kanal-mosfet-schaltsymbol

Wie geschlossener Schalter bei negativer Gate-Source-Spannung :math:`U_{GS}<U_{th}<0`
Legt man dann eine negative Drain-Source-Spannung :math:`U_{DS}` an, fließ ein Strom.

Eignet sich als pull-up Transistor.

Komplementäre Logik
===================

.. glossary::

  CMOS
    Complementary Metal-Oxide-Semiconductor


CMOS Inverter
-------------

CMOS NAND
---------

CMOS NOR
--------
