=======================
Elektrische Schaltungen
=======================

Strom
=====

:math:`I=\frac{\Delta Q}{\Delta t}`

Widerstand
==========

- :math:`U=RI`
- Reihenschaltung: :math:`R_{ges} = \sum R_k`
- Parallelschaltung: :math:`\frac{1}{C_{ges}} = \sum \frac{1}{C_k}`
- Leistung: :math:`P=UI`

Kondensator
===========

- Kapazität: :math:`C = \frac{Q}{U}`
- Parallelschaltung: :math:`C_{ges} = \sum C_k`
- Reihenschaltung: :math:`\frac{1}{C_{ges}} = \sum \frac{1}{C_k}`
- Energie: :math:`W = \frac{1}{2}CU^2`
