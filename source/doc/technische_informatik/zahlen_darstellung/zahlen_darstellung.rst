=================
Zahlendarstellung
=================

Die Darstellung der Basis in einem Beliebigen b-adischen Zahlensystem ist immer
:math:`10`.



Konvertierung
=============

- Zum 10er System: :math:`z = \sum_{i=-m}^n z_i*b^i`


B-Komplement
============
Man nimmt eine Folge von 9er und subtrahiert die Zahl.

Statt subtrahieren kann man auch das Komplement bilden und dieses Addieren.

FFFFFFFF ist (-1)

Bilden des B-Komplements zu einer Zahl :math:`a`:

.. math:: B^n-a=((B^n-1)-a)+1

Für das Zweier Komplement muss man die Binärzahl (mit führender 0) nur invertieren
und 1 addieren.

Ist eine negative Dualzahl im Zweierkomplement gegeben, so muss man diese
invertieren und 1 addieren um die positive Dualzahl zu erhalten.
