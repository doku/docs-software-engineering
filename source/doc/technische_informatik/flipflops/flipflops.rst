========
Flipflop
========

Asynchrone Flipflops
====================

- Setze Eingang :math:`S`
- Rücksetze Eingang :math:`R`

Asynchron: :math:`Q^t \rightarrow Q^{t+\Delta t}`


Synchrone Flipflops
===================


synchron: :math:`Q^t \rightarrow Q^{t+1}`

:math:`Q^t` ist der Zustand nach dem zur Zeit t ausgeführten Schaltvorgang.
Er bleibt bis zum nächsten Schaltvorgang zur Zeit t+1 erhalten.

RS-Flipflop
===========
asynchrones Flipflop

.. figure:: rs_flipflop_funkrion.png
	 :alt: rs_flipflop_funkrion

NOR-Flipflop
------------

.. figure:: nor-flipflop.png
	 :alt: nor-flipflop

D-Flipflop
==========

.. figure:: d_flipflop.png
	 :alt: d_flipflop

JK-Flipflop
===========

.. figure:: jk_flipflop.png
	 :alt: jk_flipflop

T-Flipflop
==========

.. figure:: t_flipflop.png
	 :alt: t_flipflop

Übersicht
=========

.. figure:: flipflops_wahrheitstafeln.png
	 :alt: flipflops_wahrheitstafeln

.. figure:: uebersicht_flipflops.png
	 :alt: uebersicht_flipflops
