================
Boolsche Algebra
================

Dualität
========

Ersetzt man in einem Axiom der Boolesche Algebra gleichzeitig :math:`\vee`
durch :math:`\wedge` und :math:`\wedge` durch :math:`\vee`
sowie 1 durch 0 und 0 durch 1, so erhält man das zu diesem Axiom gehörige
duale Axiom. Führt man diese Ersetzung in einem Theorem aus, so erhält man das
zu diesem Theorem gehörige duale Theorem.

Negationstheorem
================
Ersetzt man in einem (komplexen) booleschen Ausdruck gleichzeitig :math:`\vee`
durch :math:`\wedge` und :math:`\wedge` durch :math:`\vee` und
negiert man alle vorkommenden Konstanten und Variablen,
so erhält man die Negation dieses Ausdrucks.
