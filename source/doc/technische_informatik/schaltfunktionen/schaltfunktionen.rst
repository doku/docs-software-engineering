================
Schaltfunktionen
================

.. glossary::

  Minterm
    Ein Minterm (Vollkonjunktion) zu einer Schaltfunktion ist eine Konjunktion,
    in der jede Variable dieser Schaltfunktion genau einmal vorhanden ist und zwar
    entweder nicht-negiert (bejaht) oder negiert (verneint).

  Maxterm
    Ein Maxterm (Volldisjunktion) zu einer Schaltfunktion ist eine Disjunktion,
    in der jede Variable dieser Schaltfunktion genau einmal vorhanden ist und
    zwar entwerder nicht-negiert (bejaht) oder negiert (verneint).


	Kanonische disjunktive Normalform
		Eine disjunktive Normalform heißt kanonische disjunktive Normalform, wenn in
		jeder Konjunktion alle Variablen vorhanden sind, jede Variable genau einmal
		vorkommt und alle Konjunktionen von einander verschieden sind.

		Eine Schaltfunktion f(xn-1,...,x1,x0) ist in der kanonischen disjunktiven Normalform, wenn f
		eine Disjunktion von Mintermen ist, und wenn kein Minterm mehrfach auftritt.

	Kanonische konjunktive Normalform
		Eine konjunktive Normalform heißt kanonische konjunktive Normalform, wenn in
		jeder Disjunktion alle Variablen vorhanden sind, jede Variable genau einmal
		vorkommt und alle Disjunktionen von einander verschieden sind.

		Eine Schaltfunktion f(xn-1,...,x1,x0) ist in der kanonischen konjunktiven Normalform, wenn f
		eine Konjunktion von Maxtermen ist, und wenn kein Maxterm mehrfach auftritt.


.. figure:: schaltsymbole.png
	 :alt: schaltsymbole

Minimalformen
=============

Ein Monom zu einer Schaltfunktion ist eine Konjunktion gewisser Variable oder
ihrer Komplemente. (Auch eine einzige bejahte oder negierte Variable ist ein Monom.)

Ein Monom ist ein Implikant einer Schaltfunktion f, wenn für jede Wertekombination,
für die das Monom den Wert 1 annimmt, auch die Schaltfunktion f den Wert 1 annimmt.

Streicht man aus einem Monom Q gewisse Variable, so erhält man ein Teilmonom Q' von Q.

Ein Monom Q heißt Primimplikant einer Schaltfunktionen f, wenn Q Implikant von f
ist und es kein Teilmonom Q' von Q gibt, das ebenfalls ein Implikant von f ist.

Arten von Primimplikanten
-------------------------

Ein Primimplikant heißt Kern-Primimplikant, wenn es einen Minterm gibt, der nur
diesen Primimplikanten impliziert.

Ein Primimplikant heißt wesentlicher Primimplikant, wenn er in jeder minimalen
Form einer Schaltfunktion enthalten ist und kein Kernprimimplikant ist.

Ein Primimplikant heißt unwesentlicher Primimplikant, wenn er in keiner minimalen
Form einer Schaltfunktion enthalten ist.

Ein Primimplikant heißt relativ wesentlicher Primimplikant, wenn er mindestens
in einer minimalen Form aber nicht in allen minimalen Formen einer Schaltfunktion
enthalten ist. (Eine Schaltfunktion hat entweder keine oder mehrere relativ
wesentliche Primimplikanten.)

Statischer Hasard
=================

Statische Hasards können auftreten, wenn bei der strukturgleichen Realisierung
einer Schaltfunktion im Karnaugh-Diagramm zwei benachbarte Einsen (bzw. Nullen)
nicht gemeinsam in einem Block liegen.

Statische Hasards können durch Verwendung zusätzlicher Blöcke beseitigt werden.
