=====================
Technische Informatik
=====================

Eine Definition des Begriffs "Information" ist nicht möglich.

.. glossary::

  Nachricht
    Als Nachricht wird die Darstellung der Information unter Einschluss der
    Information selbst bezeichnet.

  Daten
    Daten sind maschinen-lesbare und -bearbeitbare digitale Repräsentation
    von Informationen.

Die gleiche Information kann durch verschiedene Nachrichten dargestellt werden.

.. glossary::

  Bit
    Ein Bit (Binary Digit) ist ein Speicherelement, das genau einen von zwei
    möglichen Werten aufnehmen kann.

  Byte
    Eine Folge von 8 Bits bezeichnet man als Byte.

  
