Computer Hardware
=================

von-Neumann-Rechner
-------------------

.. figure:: von_neuman_architektur.png
	 :alt: von_neuman_architektur

Steuerwerk
~~~~~~~~~~

Aufgaben des Steuerwerks
^^^^^^^^^^^^^^^^^^^^^^^^

-  Laden der Befehle des aktuellen Programms (aus der Quelle) aus dem
   Speicher in der richtigen Reihenfolge
-  Decodierung der Befehle
-  Interpretation der Befehle
-  Das Steuerwerk versorgt alle Funktionseinheiten, die an der
   Ausführung des Befehls beteiligt sind, mit den nötigen Steuersignalen

Verarbeitungsschritte des Steuerwerks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Das aktuelle Programm wird immer an Speicherstelle 0 geladen.
2. Über das Speicherwerk wird der Befehl, der am Befehlszählersteht,
   gelesen
3. Der Befehl wird analysiert.
4. Der Befehlszähler wird um 1 erhöht.
5. Die Operanden des aktuellen Befehls werden (über das Steuerwerk)
   gelesen
6. Das Rechenwerk wird mit der Ausführung des Befehls beauftragt (manche
   Befehle kann das Steuerwerk aber selber ausführen)
7. Das Ergebnis des Befehls wird an die Stelle geschrieben, die das
   Ergebnisregister vorgibt.
8. Der nächste Befehl wird gelesen (durch den zuvor um 1 erhöhten
   Befehlszähler) - dh. man fängt wieder bei Schritt 2 an

.. figure:: SteuerwerkAufbau.png
   :alt: Aufbau des Steuerwerks

   Aufbau des Steuerwerks

Rechenwerk (**ALU** - Arithmetic Logical Unit)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Aufgaben des Rechenwerks
^^^^^^^^^^^^^^^^^^^^^^^^

-  (Serielle oder parallele) Berechnung durch Basisoperatoren (Addition,
   Subtraktion, Konjunktion, Disjunktion, Vergleiche)
-  Schreiben und Lesen von Zwischenergebnissen im Register
-  Schreiben des Endergebnis in Akkumulator

.. figure:: RechenwerkAufbau.png
   :alt: Aufbau des Rechenwerks

   Aufbau des Rechenwerks

Steuer- und Rechenwerk fasst man unter dem Namen CPU zusammen.

Die Funktionseinheiten einer CPU sind Rechenwerk, Steuerwerk, Register
und Adresswerk.

Es gibt einen Datenbus zwischen dem Steuerwerk und Rechenwerk, da das Steuerwerk
die Ergebnisse von Berechnungen benötigt um bei Verzweigungen im Programm den
richtigen Pfad zu wählen.

Speicherwerk
~~~~~~~~~~~~

Der Speicher ist für die "Aufbewahrung" von Daten zudtändig. Heute wird
quasi nur binärer Speicher verwendet. Dieser speichert in ``bit`` und
hat ``byte`` als Einheit der Speicherkapazität.

Die Zugriffszeit ist die Zeit, die zum Lesen oder Schreiben einer
Speicherzelle benötigt wird. Sie setzt sich aus der Zeit zur
Lokalisierung und Ansteuerung der Speicherzelle und der Schaltzeit der
Speicherelemente zusammen. Je nach Speichertyp liegt diese zwischen
wenigen Nanosekunden und mehreren Sekunden.

von-Neumann-Prinzipien
----------------------

von-Neumann-Prinzipien 1
~~~~~~~~~~~~~~~~~~~~~~~~

Ein Rechner besteht aus 5 **Funktionseinheiten**:

-  Steuerwerk
-  Rechenwerk (**ALU** - Arithmetic Logical Unit)
-  Speicher
-  Eingabewerk
-  Ausgabewerk

von-Neumann-Prinzipien 2
~~~~~~~~~~~~~~~~~~~~~~~~

-  Die Struktur des Von-Neumann-Rechners ist unabhängig von dem zu
   bearbeitenden Problem (**Universalmaschine**)
-  Zur Lösung eines Problems muss von außen ein **Programm** eingegeben
   und im Speicher abgelegt werden.
-  Ohne dieses Programm ist die Maschine nicht arbeitsfähig

von-Neumann-Prinzipien 3
~~~~~~~~~~~~~~~~~~~~~~~~

-  Programme
-  Daten
-  Zwischenergebnisse
-  Endergebnisse

werden in demselben Speicher abgelegt.

von-Neumann-Prinzipien 4
~~~~~~~~~~~~~~~~~~~~~~~~

-  Der Speicher ist in gleichgroße Zellen unterteilt, die fortlaufend
   durchnummeriert sind.
-  Über die Nummer (**Adresse**) einer Speicherzelle kann deren Inhalt
   abgerufen oder verändert werden.

von-Neumann-Prinzipien 5
~~~~~~~~~~~~~~~~~~~~~~~~

-  Aufeinanderfolgende Befehle eines Programms werden in
   aufeinanderfolgenden Speicherzellen abgelegt
-  Das Ansprechen des nächsten Befehls geschieht vom Steuerwerk durch
   Erhöhung der Befehlsadresse um Eins

von-Neumann-Prinzipien 6
~~~~~~~~~~~~~~~~~~~~~~~~

Durch **Sprungbefehle** kann von der Bearbeitung der Befehle in der
gespeicherten Reihenfolge abgewichen werden

von-Neumann-Prinzipien 7
~~~~~~~~~~~~~~~~~~~~~~~~

Es gibt zumindest:

-  arithmetische Befehle
-  logische Befehle
-  Transportbefehle
-  bedingte Sprünge

von-Neumann-Prinzipien 8
~~~~~~~~~~~~~~~~~~~~~~~~

-  alle Daten (Befehle, Adressen) werden binär codiert
-  Geeignete Schaltwerke sorgen für die richtige Entschlüsselung
   (**Decodierung**)
