=====================
Technische Informatik
=====================

.. toctree::
  :glob:

  introduction/*
  zahlen_darstellung/*
  boolsche_algebra/*
  computer_hardware/*
  elektrische_schaltungen/*
  digitale_grundschaltungen/*
  schaltfunktionen/*
  flipflops/*

Auf das E oben links des headers Klicken um alles auszugrauen, was nicht teil
der Klausur ist.

Für die Klausur nur die rot umrandeten Formeln mit gelbem Hintergrund.
