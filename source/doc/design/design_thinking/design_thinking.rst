===============
Design Thinking
===============

By going to the field and learning about the people we wish to design for we bring
their needs and desires early on in the problem solving process. This will ensure
that the solution will create real value.

Design research is focused on learning things that are beyond our own assumptions.

In order to understand the users hidden needs we need to watch their workflow. 
