Setup DSA
=========

Setup eines Neuen Aufgaben Blatts
---------------------------------

Init Script
~~~~~~~~~~~

1. Das template Script aufrufen ``./templates/template.sh newexersice <number>``
2. Umbennenen des ``projects`` Ordners
3. Projekt Namen in der ``pom.xml`` anpassen
4. Project im Gitlab CI Prozess einbinden
5. Java Kopfkommentare einfügen

Das Template Script
-------------------
Das Script ist im Template Repository verfügbar template.sh.

Man kann es beim aufrufen direkt ein Subcommand mit Argumenten angeben:

``template.sh <subcommand> [arguments]``

Oder man wird nach einem subcommand gefragt, wenn keins angegeben wurde, daduch kann man das Script auch ohne eine Shell, im Explorer ausführen.

Verfügbare Subcommands
~~~~~~~~~~~~~~~~~~~~~~
init
   Mit dem init Subcommand, kann man die wichtigsten Dateien und Verzeichnisse in ein neues Projekt kopieren.
   
   Der gesammte Inhalt des ``initRessouces/`` Verzeichnisses wird in das Wurzelverzeichnisskopiert.
   Dabei ist auch  eine ``.gitignore``, welche die wichtigsten Einträge enthält.
   Außerdem wird ein Verzeichniss für die Übungsaufgaben erstellt.
newexercise [number]
   Erstellt eine neues Aufgabenverzeichniss im Übungsaufgebenverzeichniss, mit der angegebenen Nummer bzw. 0 wenn keine Nummer angegeben wurde.
   Die Verzeichnisse, die erstellt werden, werden der Datei ``exercisetemplate`` entnommen
clean
   Entfernt alle  ``.gitkeep`` Dateien, wenn diese nicht mehr gebraucht werden.
   Außerdem werden alle Datein, die während des compelierens von latex Dokumenten entstehen und in der .gitignore enthalten sind, entfernt.
   
   Es werden die Pfade der gelöschten Dateien aufgegeben.
getexercisefolders
   Gibt die Pfade aller Aufgaben Verzeichnisse zurück.
   Zum Beispiel:
   
   $ templates/template.sh getexercisefolders
   Uebungsblaetter/Uebungsblatt00/ Uebungsblaetter/Uebungsblatt01/ Uebungsblaetter/Uebungsblatt02/ Uebungsblaetter/Uebungsblatt03/

help
   Zeigt eine Hilfe für die verwendung des Scripts an.
