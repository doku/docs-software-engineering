===============
String-Matching
===============

Das Ziel beim **String-Matching** ist es ein Teilwort in einem (langen) Wort zu
finden.

Man kann nach allen Vorkommen oder nur nach dem ersten Vorkommen des Patterns
im Wort suchen.

- Alphabet :math:`\Sigma`
- Wort :math:`a \in \Sigma^*` der Länge n
- Gesuchtes Teilwort :math:`p \in \Sigma^*` der Länge :math:`m<n`

Bruteforce bei Text-Patter-Matching
===================================

.. figure:: brute_force_example.png
	 :alt: brute_force_example

.. code::

	public int bruteForceSearch(String text, String pattern) {
		int textLength = text.length();
		int patternLength = pattern.length();

		for (int textPos = 0; textPos <= textLength - patternLength; textPos++) {
			for (int patternPos = 0; patternPos < patternLength;  patternPos++) {
				if (!(pattern.charAt(patternPos) == text.charAt(textPos+patternPos)) {
					break; // Verlasse Schleife bei Mismatch
				}
				if (patternPos >= patternLength -1) {
					return textPos;
				}
			}
		}
		return  -1;
	}

Die **Worst-Case** Laufzeit ist :math:`(n - m + 1)*m \in O(n*m)` wobei n die Länge des Textes ist
und m die Länge des Pattern

Der Worst-Case tritt ein, wenn das Pattern sich immer erst im letzten Buchstaben
vom Wort unterscheidet. Zum Beispiel ``000001`` suchen im Wort ``00000000000000000000000``

Finden des Nullstrings?

Knuth-Morris-Pratt (KMP)
========================

Läuft ähnlich wie der Brute-Force-Ansatz nutzt aber die beim Vergleich gesammelten
Informationen und verschiebt das Muster :math:`p` gegebenenfalls um mehr als eine
Stelle nach rechts.

:math:`i` ist Index des Worts :math:`a` und :math:`j` ist Index des Patterns :math:`p`.
Bei einem Mismatch an der Stelle :math:`j` von :math:`p` bei Vergleich ab der
Stelle :math:`i` in :math:`a \in \Sigma^*` ist bekannt, dass :math:`a_{i+j} \neq p_i`
und :math:`(a_i, a_{i+1}, ..., a_{i+j-1}) = (p_0, ... p_{j-1})`

Algorithmus besteht aus 2 Schritten: der Präfixsuche im Pattern und dem eigentlichen
Vergleich mit dem Text.

Das ``next`` Feld enthält für jede Posotion des Musters die Distanz für eine
sichere Verschiebung.

Präfixsuche
-----------
Das Pattern wird nach Wiederholungmustern durchsucht die dann in der Präfixtabelle
gespeichert werden. Diese Informationen werden genutzt um größere Sprünge bei
der Verschiebung zu machen.

Man bestimmt für jedes :math:`k` die Länge :math:`f[k]` des längsten Präfix von
:math:`p`, das Suffix von :math:``

.. figure:: praefix_patterns_example.png
	 :alt: praefix_patterns_example

Die Präfixtabelle wird von links nach rechts gefüllt.

.. figure:: knuth_morris_pratt_next.png
	 :alt: knuth_morris_pratt_next

Danach wird anhand der Wiederhohlumgsmuster das Pattern im Wort gesucht.

Missmatch bei Pattern vorschieben, da die Information über Vergleichsstatus mit
Wort nicht in der Patternanalyse gespeichert ist.


Algorithmus
-----------

.. figure:: knuth_morris_pratt_algorithm.png
	 :alt: knuth_morris_pratt_algorithm

.. figure:: knuth_morris_pratt_example.png
	 :alt: knuth_morris_pratt_example

Bei Missmatch zwischen Pattern und Text an Patternposition :math:`j` und
Textposition :math:`i` kann das Pattern mit Position :math:`next(j-1)` auf :math:`i`
verschoben werden.

Aufwandsanalyse
---------------

- :math:`O(n)` für die Eigentliche Suche
- :math:`O(m)` für die Musteranalyse

Der Algorithmus ist Stream fähig, da der Text in eine Richtung gelesen wird.

Der Algorithmus ist zwar theoretisch  schneller aber in der Praxis kaum effizienter
bei Texten mit geringer Selbstähnlichkeit.

Boyer-Moore-Algorithmus
=======================

Pattern wird von hinten mit dem Wort verglichen. Dadurch können deutlich
größere Sprünge gemacht werden. Wird am Ende des Patterns ein Mismatch gefunden
kann das ganze Pattern übersprungen werden.

Dazu wird die **Bad Character** und **Good suffix** Heuristik berechnet und im
Falle eines Mismatch wird das Pattern um den Maximalwert der beiden Heuristiken
verschoben. Da Beide Heuristiken konservativ sind, kann das Maximum der
Verschiebungen verwendet werden.

Dadurch erhält man im Best-Case :math:`O(\frac{n}{m})`, wobei :math:`n` die Länge des Wortes
ist und :math:`m` die Länge des Patterns.

Bad Character Strategie
-----------------------
Gibt es einen Mismatch mit einem Zeichen im String, welches im Pattern gar nicht
vorkommt, kann das Pattern um die ganze Länge verschoben werden.

.. figure:: boyer_moore_jumps.png
	 :alt: boyer_moore_jumps

	 bad character

Tritt das Zeichen im Pattern auf, so wird das Pattern bis zum letzten Auftreten
des Zeichens verschoben.

.. figure:: boyer_moore_occurence.png
	 :alt: boyer_moore_occurence

	 occurrence

Der Boyer-Moore Algorithmus benötigt eine ``last``-Tabelle und eine ``shift``-Tabelle.

Last Tabelle
^^^^^^^^^^^^

In der ``last``-Tabelle wird für jedes Zeichen des Textes die sichere Verschiebedistanz
gespeichert, also die letzte Position im Muster.

Für ein Muster :math:`p` der Länge :math:`m` mit :math:`p = p_0 p_1 ... p_{m-1}` und
einem Alphabet :math:`\Sigma` kann ``last[c]`` für alle :math:`c \in \Sigma` wie
folgt bestimmt werden.

.. math:: last[c] = \max \{j | p_j = c\}

Lasttabelle starten mit null

.. figure:: boyer_moore_example.png
	 :alt: boyer_moore_example

Good suffix oder Gutes Ende Strategie
-------------------------------------

.. figure:: boyer_moore_good_suffix.png
	 :alt: boyer_moore_good_suffix

	 good suffix

.. figure:: boyer_moore_suffix_match.png
	 :alt: boyer_moore_suffix_match

	 Ein Präfix des Musters stimmt mit einem Teil des Suffixes überein (match)

.. figure:: boyer_moore_suffix_not_in_pattern.png
	 :alt: boyer_moore_suffix_not_in_pattern

	 Der Suffix tritt im Muster nicht wieder auf.

Shift-Tabelle
^^^^^^^^^^^^^
Die shift Tabelle enthält für jedes Suffix des Musters die sichere Verschiebedistanz.
Konkret bedeutet dies, das nach der Übereinstimmung zwischen dem Teilmuster
:math:`p[j+1 ... m-1]` und dem Text und einer Nichtübereinstimmung von :math:`p[j]`
der Wert :math:`shift[j]` die mögliche Distanz zur Verschiebung liefert.

Suffix Tabelle beim letzten Element steht immer die Länge des Strings.

Die Suffix Tabelle wird benötigt wenn man die Shifttabelle bestimmen möchte.
Die Shifttabelle gibt die sichere Verschiebedistanz an.

.. math::
	\max(posInPattern - last (stringChar), shift(patternChar))


Berechnung der Shifttabelle ohne Suffix Tabelle
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Dazu baut man eine Tabellenstruktur auf, wobei im unteren Abschnitt das Pattern
immer um einen Schritt weiter verschoben ist. Diese Zeilen sind seitlich nummeriert.
In allen Feldern die fehlen, steht das Wildcard Symbol ``*``, für das man in jedem
Schritt entscheiden kann, ob es ein bestimmtes Zeichen ist oder nicht ist.

.. figure:: shift_tabele_structure.png
	 :alt: shift_tabele_structure

Der Shiftwert ist immer die Nummer der ersten Zeile, die an der aktuellen Stelle
nicht übereinstimmt.

.. figure:: shift_table_calculation_1.png
	 :alt: shift_table_calculation

Man markiert dann alle Zeilen, bei denen das Zeichen übereinstimmt, inklusive der
Wildcard.

.. figure:: shift_table_calculation_2.png
	 :alt: shift_table_calculation_2

Wieder wird die erste Zeile gewählt, in der das Zeichen nicht übereinstimmt. Dabei
wird nur unter den markierten Zeilen ausgewählt. Auch neu markieren kann immer
nur auf bereits markierten Zeilen gemacht werden.

.. figure:: shift_table_calculation_3.png
	 :alt: shift_table_calculation_3

Alle bereits markierten Zeilen bleiben weiterhin markiert, wenn das Zeichen
übereinstimmt. Der Wildcard Wert kann alle Buchstaben repräsentieren

.. figure:: shift_table_calculation_4.png
	 :alt: shift_table_calculation_4

.. figure:: shift_table_calculation_5.png
	 :alt: shift_table_calculation_5

Suffix Tabelle
^^^^^^^^^^^^^^
Man vergleicht den Anfang mit dem Ende des Patterns. Stimmen diese überein,
so wird ein ``counter`` erhöht und man geht an beiden Seiten um eine Stellen nach
links.

Der Suffix Wert der Position 0 im Pattern kann nur 0 oder 1 sein.

.. figure:: suffix_table_1.png
	 :alt: suffix_table_1

Man geht dann vorne um eins nach rechts

.. figure:: suffix_table_2.png
	 :alt: suffix_table_2

.. figure:: suffix_table_3.png
	 :alt: suffix_table_3

.. figure:: suffix_table_4.png
	 :alt: suffix_table_4

.. figure:: suffix_table_5.png
	 :alt: suffix_table_5

Der letzte Eintrag in der Suffix Tabelle ist immer die Länge des Patterns,
da die Suffix vom Ende her mit sich selbst verglichen werden.

.. figure:: suffix_table_6.png
	 :alt: suffix_table_6

Shifttabelle aus Suffix Tabelle bestimmen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Der Algorithmus um Shifttabelle aus der Suffixtabelle zu berechnen hat 2 Phasen:

Phase 1:

Der Präfix wird so verschieben, das es mit dem eigenen Suffix übereinstimmt

.. figure:: shift_from_suffix_1.png
	 :alt: shift_from_suffix_1

Der Shiftwert in der Tabelle ist die Anzahl, wie oft geshifted werden muss.

.. figure:: shift_from_suffix_2.png
	 :alt: shift_from_suffix_2

.. figure:: shift_from_suffix_3.png
	 :alt: shift_from_suffix

Phase 2:

Sofern ein Suffix doppelt existiert, müssen die Shiftwerte angepasst werden.

.. figure:: shift_from_suffix_4.png
	 :alt: shift_from_suffix_4

Algorithmus
-----------

.. figure:: boyer_moore_example.png
	 :alt: boyer_moore_example

Für die Heuristik rechnet man nun immer:

.. math::
	\max(posInPattern - last (stringChar), shift(patternChar))

.. figure:: boyer_moore_algorithm.png
	 :alt: boyer_moore_algorithm


Reguläre Ausdrücke
==================
Reguläre Ausdrucke bieten ein einheitliches Modell für die Mustererkennung.

Notation
--------

- Concatenation: ``ab``, ``a`` gefolgt von ``b``
- Disjunktion: ``a|b``, ``a`` oder ``b``
- Wildcard: beliebiger Buchstabe ``.`` (außer neue Zeile)
- Beliebige Anzahl von ``a``: ``a*``
- Beliebige Anzahl ``a`` größer 0: ``a+``
- ``a?`` genau 0 oder 1 mal ``a``
- ``[abc]``: irgendein Buchstabe der Aufzählung
- ``^`` und ``$``: Spezialzeichen für Zeilenbeginn und Zeilenende
- Gruppierung mit ``()``

.. hint:: https://jex.im/regulex/ for visualisation

.. note::
	DSA Anwendung: Jeder Regulärer Ausdruck muss folgenden Aufbau haben:
	``^(RegEx)$``

	Start und Endzeile müssen markiert werden.

Endliche Automaten erkennen die regulären Ausdrücke.

.. todo:: Link to teo Artikel

Ähnlichkeit von Zeichenketten
=============================

Die Ähnlichkeitsuntersuchung von Strings erlaubt Fehlertoleranter Vergleich von
Strings.

Levenshtein-Distanz
-------------------
Die Levenshtein-Distanz oder auch Editierdistanz genannt, misst die minimale
Anzahl von Editieroperationen um :math:`s_1` in :math:`s_2`

Mögliche Editieroperationen:

- ``insert(c)``: Einfügen des Zeichens ``c``
- ``update(a -> b)``: Ersetzte ``a`` durch ``b``
- ``delete(c)``: Lösche ``c``

Jeder Operation ist gleich teuer.

.. figure:: levenshtein_kosten_definition.png
	 :alt: levenshtein_kosten_definition

Anordnung in Matrix mit Kosten

Der Aufwand für die Berechnung der Matrix beträgt :math:`\mathcal(O)(n*m)`

Algorithmus mit **dynamischer Programmierung**

.. figure:: levenshtein_kosten_example.png
	 :alt: levenshtein_kosten_example


.. figure:: levenshtein_algorithmus.png
	 :alt: levenshtein_algorithmus
