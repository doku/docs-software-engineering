.. _merge-sort:

=========
MergeSort
=========

Anwendung des Teile-und-Herrsche-Grundsatzes (**devide and conqure**)

  - Aufteilung (Split) in kleine Teile, die im Speicher sortierbar sind
  - Zusammenfügen (Merge) der Teile nach dem Sortieren in eine vollständig
    sortierte Datei

- Teile die zu sortierende Folge in zwei Teilfolgen.
- Sortiere diese (rekursiv)
- führe die sortierten Ergebnisse zusammen

 - vergleiche jeweils die beiden kleinsten Einträge
 - "Schiebe" den kleineren ans Ende der neuen Folge

.. figure:: mergesortexample.png
	 :alt: mergesortExample

.. code::

    algorithm MegeSort(F) -> F_s

      if F einelementig then
        return F
      else
        teile F in F_1 und F_2
        F_1 := MergeSort(F_1);
        F_2 := MergeSort(F_2);
        return Merge(F_1, F_2);
      fi

    procedure Merge(F_1, F_2) -> F
      Eingabe: 2 sortierte Folgen F_1, F_2
      Ausgabe: eine sortierte Folge F

      F := leere Folge

      while F_1 und F_2 nicht leer do
        Entferne das kleinere Anfangselement aus F_1 bzw. F_2;
        Füge dieses Element an F an;
      od;
      Füge die verbleibende nichtleere Folge F_1 oder F_2 an F an.
      return F;

Aufwandsabschätzung: :math:`n \log_2 n`

Der algorithmische Aufwand steckt im Schritt des ``Merge``.

Merge
    Durchlaufen der beiden bereits sortierten Listen und Überprüfung welche der Elemente
    zuerst kommen müssen.

Der Mergesort ist stabil, aber nicht in-place. Es wird daher ein Zwischenspeicher
für die :math:`n` Elemente benötigt. Der Aufwand beim MergeSort ist unabhängig
von der Eingabereihenfolge.
