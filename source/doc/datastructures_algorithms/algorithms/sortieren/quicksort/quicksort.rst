.. _quicksort:

=========
QuickSort
=========

Bei Quicksort wird immer ein Pivot-Element ausgewählt und die anderen Elemente
werden dann mit dem Pivot Element verglichen. Der Quicksort ist ein rekursives
Sortierverfahren nach dem Prinzip **Devide and Conquer**.

Links vom Pivot Element sind nur Elemente die kleiner sind als das Pivot-Element.

.. figure:: quicksort_pivotelement.png
	 :alt: quicksort_pivotelement

Idee der Partitionierung:

- Arbeite mit drei Bereichen: „< Pivot“, „> Pivot“ und „ungeprüft“.
- Schiebe die linke Grenze nach rechts, solange das zusätzliche Element < Pivot ist.
- Schiebe die rechte Grenze nach links, solange das zusätzliche Element > Pivot ist.
- Tausche das links gefundene mit dem rechts gefundenen Element.
- Fahre fort, bis sich beide Grenzen treffen.

.. figure:: quicksort_partitionierung.png
	 :alt: quicksort_partitionierung

Algorithmus
===========

.. figure:: quicksort_algorithm.png
	 :alt: quicksort_algorithm

.. figure:: quicksort_algorithm_partition.png
	 :alt: quicksort_algorithm_partition

.. code-block:: java

  public static void quicksort(int[] array) {
    quicksort(array 0, array.lenght -1 )
  }

  public static void quicksort(int[] array, int left, int right) {
    if (left >= right) {
      return;
    }
    int pivot = array[(left+right)/2];
    int index = partition(array, left, right, pivot);
    quicksort(array, left,  index-1);
    quicksort(array, index, right);
  }

  public static int partition(int[] array, int left, int right, int pivot) {
    while (left <= right) {
      while (array[left] < pivot) {
        left++;
      }
      while (array[right] > pivot) {
        right--;
      }

      if(left <= right) {
        swap(array, left, right);
        left++;
        right--;
      }
    }
    return left;
  }


Quicksort ist ein inplace Verfahren.
Quicksort ist nicht stabil.

.. figure:: quicksortexample.png
	 :alt: quicksortExample

.. figure:: quicksortvisualisierung.png
	 :alt: quicksortVisualisierung

Komplexität
===========

Worstcase
---------
Schlechtester Fall tritt ein, wenn Liste sortiert ist und man das ``Pivot`` Element
immer als das größte oder kleinste Element wählt. In diesem Fall ist die worst  case Laufzeit
:math:`\Theta(n^2)`. Dieser Fall tritt ein, wenn z.B. der Array bereits auf oder absteigend sortiert ist.
Dadurch entartet der Rekursionsbaum zum Pfad mit n-1 Ebenen. Laufzeit: :math:`\sum_{i=1}^n {i-1} = \frac {n*(n-1)}{2} `


Optimierungen
=============

.. todo::
	Median der Mediane
	Dual pivot quicksort
