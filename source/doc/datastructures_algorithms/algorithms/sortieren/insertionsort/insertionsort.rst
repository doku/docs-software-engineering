.. _insertion-sort:

=============
InsertionSort
=============
Die Folge wird von vorne nach hinten durchlaufen. Das erste unsortierte Element
wird im vorderen bereits sortierten Teil an der richtigen Stelle eingefügt.

.. figure:: insertionsortexample.png
	 :alt: insertionSortExample

Invariante: Linke Seite von der ausgewählten Zahl ist immer schon sortiert.

Der Insertionsort ist ein stabiles Sortierverfahren und inplace.

.. figure:: insertionsortvisualisierung.png
	 :alt: InsertionSortVisualisierung

.. figure:: insertionsort_algorithm.png
	 :alt: insertionsort_algorithm

Aufwandsanalyse
===============
.. csv-table::

  "best case", "Die Liste ist bereits sortiert, dann ist der Aufwand linear: :math:`n`"
  "average case", "Bei jedem der :math:`(n-1)` Rückwege Faktor :math:`i/2`, Gesamtzahl der Verglieche :math:`\frac{n^2}{4}`"
  "worst case", "Die Liste ist umgekehrt sortiert, dann ist die Gesamtzahl der Vergleiche :math:`\frac{n^2}{2}`"
