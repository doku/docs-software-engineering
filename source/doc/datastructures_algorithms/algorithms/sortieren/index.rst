=========
Sortieren
=========

.. toctree::
  :maxdepth: 2
  :glob:

  sort_overview/*
  selectionsort/*
  insertionsort/*
  bubblesort/*
  mergesort/*
  quicksort/*
  heapsort/*
  binarytreesort/*
  countingsort/*
