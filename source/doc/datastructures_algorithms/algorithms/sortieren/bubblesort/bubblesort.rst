==========
BubbleSort
==========

Sortieren durch aufsteigen/vertauschen.

Es werden immer nur benachbarte Element getauscht.
Die Größten Elemente steigen am schnellsten auf.

Invariante: Größte Element ist nach dem ersten Durchlauf an letzter Stelle.
Nach dem zweiten Durchlauf ist das zweitgrüßte Element an vorletzter Stelle ...

.. figure:: bubblesortexample.png
	 :alt: bubbleSortExample

.. figure:: bubblesortvisualisierung.png
	 :alt: bubblesortVisualisierung

Unsere größten Zahlen wandern am schnellsten an ihre sortierte Position und
bleiben dann dort.

Der Bubblesort ist stabil und in-place.

Algorithmus
===========

.. code::

  algorithm BubbleSort(F)
    Eingabe: zu sortierende Folge F der Länge n

    do
    //letzter wert muss noch angeschaut werden da dieser in der
    //Folge schon direkt angeschaut wird
      for i:=0 to n-2 do
        if F[i] > F[i+1] then
          Vertausche Werte von F[i]  und F[i+1]
        fi
      od
    until keine Vertauschungen treten mehr auf

Aufwandsanalyse
===============

.. csv-table::

  "best case", ":math:`n`"
  "worst case", ":math:`n^2`"
  "average case", ":math:`n^2`"
