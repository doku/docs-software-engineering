=========
Sortieren
=========

Grundlegende Unterteilung
  - Internen Verfahren: Sortierung im Hauptspeicher (bei Arrays und Listen)
  - Externe Verfahren: Datensätze auf externen Speichermedien (Festplatten)

In-Place
  nur konstante Speichermenge wird benötigt (unabhängig von Eingabedaten),
  Eingabedaten werden mit Ausgabedaten überschrieben.


Ein Sortierverfahren heißt **stabil**, wenn es die relative Reihenfolge gleicher
Schlüssel beibehält.

Suchverfahren im Vergleich
==========================

.. figure:: laufzeitvergleich.png
	 :alt: LaufzeitVergleich


.. figure:: visualisierungvergleich.png
	 :alt: visualisierungVergleich


.. figure:: komplexitaetvergleich.png
	 :alt: komplexitaetVergleich

Bestes Verhalten im **worst-case**: Merge-Sort :math:`O(n \log n)`
Schlechtestes Verhalten im **best-case**: Selectionsort :math:`O(n^2)`

- Bei einer aufsteigend sortierten Folge ist der Insertionsort und Bubblesort
  am besten.
- Bei einer absteigend sortierten Folge hat der MergeSort die beste Laufzeit
  Komplexität (Quicksort ist hier :math:`\mathcal{O}(n^2)`)
- bei einer unsortierten Daten ist MergeSort am besten. (Quicksort hat
  eine Vorfaktor von :math:`1,38`)

.. figure:: komplexitaettable.png
	 :alt: komplexitaettable

Komplexitätsanalyse
===================
Betrachten wir das Problem des Sortierend von :math:`n` Objekten, wobei die
Objekte nur verglichen werden dürfen.

Jeder Sortieralgorithmus liefert eine obere Schranke für :math:`T(n)`

- Bubblesort: :math:`T(n) \in O(n^2)`
- Mergesort: :math:`T(n) \in O(n \log n)`

Wir können beweisen, dass :math:`T(n) \in \Omega(n \log n)`.
Damit folgt :math:`T(n) \in \Theta(n \log n)`.

Es sind :math:`\Omega( n \log n)` Vergleiche nötigt, um :math:`n` Elemente
zu sortieren. Wir betrachten einen beliebigen Algorithmus :math:`A` zum Sortieren.

:math:`A` vergleiche :math:`e_i` mit :math:`e_j`

.. figure:: vergleichsbaum.png
	 :alt: vergleichsbaum

In dem Baum stehen alle möglichen Programmabläufe.

Ein Blatt in diesem Baum bedeutet, dass der Algorithmus fertig sortiert hat und
somit herausgefunden hat, was die Permutation der Eingabe war.

Es gibt :math:`n!` viele verschiedene Sortierungen und man muss zwischen :math:`n!`
verschiedenen Eingaben unterscheiden.
Es muss daher :math:`n!` Blätter im Baum geben. Da der Algorithmus auf :math:`n!`

Die Mindestanzahl der Vergleiche ist die Tiefe des Baums.
Die Worstcase Laufzeit von Algorithmus :math:`A` ist also die Tiefe des Baums.

Für die minimale Tiefe eines Binärbaums mit :math:`n!` Blättern folgt:

.. math::

  2^x &= n! \\
  n! &\approx (\frac{n}{e})^\frac{n}{e} \\
  x &= log_2((\frac{n}{e})^\frac{n}{e}) = n \log n

Das vergleichsbasierte Sortieren braucht also :math:`\Omega(n \log n) Schritte.`
