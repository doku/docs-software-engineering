.. _sort-heapsort:

========
Heapsort
========

Wird ein MinHeap mit dem Heapsort sortiert, sind die Elemente danach absteigend
sortiert.

Ein MaxHeap wird durch den Heapsort aufsteigend sortiert.

Der Heapsort ist In-Place und instabil.

Algorithmus
===========

.. figure:: heapsort_algorithm.png
	 :alt: heapsort_algorithm

Durch das Erstellen des Heaps wird immer das größte/kleinste Element gefunden.
Dieses wird dann an die letzte Stelle des Arrays getauscht und Heap entfernt.


.. code-block:: java

  void heapSort(int[] array) {
    for (int i = array.length / 2; i >= 0; i--) {
      heapify(array, array.length, i); // creates max heap
    }
    for (int  i = array.lenght -1; i >= 0; i--) {
      swap(array[0], array[i]); // swaps first and last node, because first node now sorted
      heapify(array, i, 0); //creates max heap on the reduced array
    }

  }

.. figure:: heapsort_example.png
	 :alt: heapsort_example

.. figure:: heapsort_visualisierung.png
   :alt: heapsort visualisierung

   Heapsort Statische Visualisierung

Komplexität
===========

Der Heapsort besteht im Wesentlichen aus 2 Teilen:

1. Aufbau des Heaps
    Hier werden :math:`\frac{n}{2}` Elemente durchgesickert. Der Pfad davon ist im
    ungünstigsten Fall die Höhe des Baumes :math:`\log_2 n`
2. Entfernen der Elemente zur eigentlichen Sortierung
    Entfernen aller :math:`n` Elemente und Wiederherstellen der Heap-Eigenschaft
    durch durchsickern entlang der Pfade. Damit ist der Aufwand :math:`n \log_2 n`

Der Gesamtaufwand ist mit :math:`\frac{3}{2} n \log_2 n` in
:math:`\mathcal{O} (n \log_2 n)` im worst case.
