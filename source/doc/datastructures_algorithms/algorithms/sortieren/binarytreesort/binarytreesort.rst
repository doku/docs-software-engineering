================
Binary Tree Sort
================

Sortieren mittels eines Suchbaums: Alle Elemente werden in einen leeren Suchbaum
eingefügt. Man gibt dann den Baum in inorder-Reihenfolge aus.

Komplexität
===========

- Einfüge: :math:`n*O(\log n)` best case :math:`O(n)`
- Ausgabe:
