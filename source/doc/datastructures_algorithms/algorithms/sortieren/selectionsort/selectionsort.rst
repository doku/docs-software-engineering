.. _selection-sort:

=============
SelectionSort
=============

Beim SelectionSort wird das größten Element der Liste gesucht und an das Ende der
unsortierten Folge gesetzt.
Es wird dann mit der um 1 kleineren Liste fortgefahren.

Der SelectionSort ist ein **inplace** Verfahren (intern) und nicht stabile.

.. figure:: selectionsortexample.png
	 :alt: selectionSortExample

.. figure:: visualisierungselectionsort.png
	 :alt: visualisierungSelectionSort

Algorithmus
===========

.. code::

  algorithm selectoinSort(F)
    Eingabe: zu sortierende Folge der Länge n

    p:=n-1;
    while p>0 do
      g:= Index des größten Elements aus F im Bereich 0 .. p;
      Vertausche Werte von F[p] und F[g];
      p:=p-1;
    od

Alle Elemente rechts von ``p`` sind bereits sortiert. Links von ``p`` ist der unsortierte
Teil.

Aufwandsanalyse
===============

Gesamtzahl der Vergleiche

.. math::

    (n-1) * (n-2) + ... + 1 = \frac{n(n-1))}{2} \approx  \frac{n^2}{2}

    O(n) = n^2


Anzahl der Vergleiche ist identisch für beste, mittlere und schlechtesten Fall.
Der SelectionSort benötigt immer :math:`n` Vertauschungen
