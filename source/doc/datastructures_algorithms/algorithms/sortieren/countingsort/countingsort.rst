=============
Counting Sort
=============

Idee
====
Sei jedes der n zu sortierenden Elemente eine ganze Zahl zwischen 0 und k-1.
Nun bestimmt man für jedes  Element x die Anzahl der Elemente die gleich x sind.
Jedes Element kann dann direkt an seine korrekte Position im Ausgabearray gebracht werden

.. figure:: countingsort_example.png
	 :alt: counting_sort_example

Algorithmus
===========

.. code-block:: C++

  void countingSort(int[] a, int k) {
    int[] c = new int[k];
    int i, j, index;
    for (i = 0; i < k; i++) c[i] = 0;
    for (i = 0; i < a.length; i++) c[a[i]]++;
    // c[i] enthält jetzt die Anzahl Elemente = i
    index = 0;
    for (i = 0; i < k; i++)
    for (j = 1; j <= c[i]; j++) {
      a[index] = i;
      index++;
    }
  }
