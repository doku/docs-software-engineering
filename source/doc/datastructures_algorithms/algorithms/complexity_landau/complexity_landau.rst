===========
Komplexität
===========

In der Regel existieren viele Algorithmen, um dieselbe Funktion zu realisieren.
Welche Algorithmen sind besser?

Komplexitätsanalyse ist die Quantifizierung von der Schwierigkeiten von Algorithmen.

Asymptotische Komplexität
=========================

Aufwandsfunktion
----------------
:math:`T_\pi(n)` ist die maximale Zeit, die ein Programm
:math:`\pi` für eine Eingabe :math:`w` der Länge :math:`n` bis zum Anhalten
benötigt.

.. attention::
    :math:`T_\pi(n)` ist nur eine totale
    Funktion, wenn das Programm :math:`\pi` stets terminiert.

Bei der Aufwandsanalyse wird die asymptotische Komplexität der Aufwandsfunktion
mit den Komplexitätsklassen verglichen.

.. figure:: LandauKomplexitaetsklassen.png
   :alt: Komplexitätsklassen

   Komplexitätsklassen

Landau Symbole
--------------
Landau Symbole teilen Funktionen in Komplexitätsklassen ein.

.. figure:: LandauSymbole.png
   :alt: Landau Symbole

.. figure:: landau_symbole_informell.png
	 :alt: landau_symbole_informell

.. math::

    f(n) \in O(g(n)) : \Leftrightarrow \exists c, n_0 \forall n \geq n_0 : f(n) \leq c*g(n)

:math:`f` wächst asymptotisch nicht schneller als :math:`g`

:math:`o(n^2)`: Menge aller Funtkionen die echt langsamer wachsen als :math:`n^2`
:math:`n^2` ist nicht in :math:`o(n^2)`

:math:`\Omega(n^2)` ist die Menge aller Funtkionen, die mindestens so schnell wie
:math:`n^2` wachsen (asymtopisch)

Beispiele in :math:`\Omega(n^2)`: :math:`n^2 \log n`

:math:`\omega (n^2)` ist die Menge aller Funktionen, die echt schneller wachsen als
:math:`n^2`

:math:`f(n) \in \omega(g(n)) \Leftrightarrow g(n) \in (o(f(n)))`


:math:`f(n) \in \Theta(n^2) \Leftrightarrow f(n) \in O(n^2) \wedge f(n) \in \Omega(n^2)` 

Wir ignorieren endlich viele Anfangswerte und auch konstante Faktoren an den
Funktionen.

.. csv-table:: Beispiele der Komplexitätsklassen
  :header: "Aufwand", "Problemklasse"

  ":math:`\mathcal{O}(1)`", "Hashing"
  ":math:`\mathcal{O}(\log n)`", "Baum-Suchverfahren"
  ":math:`\mathcal{O}(n)`", "Sequenzielle Suche, Suche in Text, syntaktische Analyse von Programmen (Typ2)"
  ":math:`\mathcal{O}(n*\log n)`", "Sortieren"
  ":math:`\mathcal{O}(n^2)`", "Matrix-Vektor Multiplikation"
  ":math:`\mathcal{O}(n^3)`", "Matrizen-Multiplikation"
  ":math:`\mathcal{O}(2^n)`", "viele Optimierungsprobleme"


Übersicht
=========

.. figure:: BigOLaufzeit.png
   :alt: Laufzeit Komplexität

   Laufzeit Komplexität

.. figure:: DataStructureOperations.png
   :alt: Datenstrukturen

   Datenstrukturen

.. figure:: ArraySortierAlgorithem.png
   :alt: Sortieralgorithmen

   Sortieralgorithmen

.. figure:: Cheatsheat.png
   :alt:

`Bildquelle <http://bigocheatsheet.com/>`__
