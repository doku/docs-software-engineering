=====================
Verteilte Algorithmen
=====================

Modell der kommunizierenden Prozesse
====================================

Dadurch können Prozesse teilweise unabhängig laufen. So lassen sich große Probleme
aufteilen und nebenläufig bearbeiten. Jeder Prozess hat eine eigene Ausführungssequenz.

Kritischer Abschnitt
--------------------

.. figure:: petri_netz_kritische_abschnitt.png
   :alt: petri_netz_kritische_abschnitt
   :scale: 60%

Bereich im Programm in dem nur 1 Prozess/Thread parallel ausgeführt werden
darf.

Er besteht aus mehreren Einzelanweisungen, deren Ergebnisse inkonsistente Zustände
darstellen.

Semaphor
--------

.. glossary::

  Semaphor
    Konstrukt zur Kontrolle des Zugriffs auf gemeinsame Ressourcen.
    Ganzzahlige Variable plus Warteschlange (Queue) von Prozessen

Semaphor Operationen

- ``down`` Beginn eines kritischen Abschnitts
- ``up`` Verlassen eines kritischen Abschnitts

Ein Zähler repräsentiert die Anzahl der verfügbaren Ressourcen und wird mit
dem Maximalwert initialisiert. Vor dem Zugriff auf Ressourcen müssen diese
reserviert werden, indem der Zähler um 1 verringert wird. Nach dem Zugriff
auf Ressourcen werden diese wieder freigegeben und der Zähler um 1 erhöht.
Wenn der Zähler 0 ist, werden die Prozesse in einer Warteschlange gespeichert.

Bei binären Semaphoren ist nur eine Ressource verfügbar und der Zähler ist 1.

Multi-Threading
===============

Amdahlsches Gesetz


.. figure:: amdahlsches_gesetz.png
	 :alt: amdahlsches_gesetz

Petri-Netze
===========

Petri-Netze sind ein Modell für kommunizierende Prozesse. Das Konzept basiert auf
den endlichen Automaten und wurde 1962 von Carl Adam Petri entwickelt.

Ein Petri Netz ist ein gerichteter Graph.

Er besitzt 2 Arten von Knoten:

  - Stellen: Zwischenlager von Daten (im Graph Kreise)
  - Transitionen: Verarbeitung von Daten (im Graph Balken)

Kanten verbinden immer nur 2 Knoten unterschiedlicher Art. Der Graph ist also
:term:`bipatiter Graph`

.. figure:: petri_netz_beispiel_1.png
	 :alt: petri_netz_beispiel_1

Der Zustand des Systems wird durch Markierung der Stellen definiert. Durch schalten/feuern
der Transitionen bewegen sich die Marken.

.. figure:: petri_netz_marker_int.png
	 :alt: petri_netz_marker_int

Dabei gibt es mehrere Arten von Petri-Netzen

- Bedingungs-Ereignis-Netz: Stelen sind boolesche Variablen
- Stellen-Transitions-Netz: Stellen nehmen Integer-Werte an, ggf. mit "Kapazitäten"
- Höhere Petri-Netze: Stellen sind Container für (strukturierte) Werte, z.B. "gefärbte"
  Marken

Schaltregeln
------------

Eine Transition :math:`t` kann schalten (bzw. feuern), wen jede Eingabestelle
von :math:`t` mindestens eine Marke enthält. Schaltet eine Transition, ddann
wird aus jeder Eingabestelle eine Marke entfernt und zu jeder Ausgabestelle der
Marke hinzugefügt.

Bei Stellen mit Kapazität darf das Hinzufügen nicht die Kapazitätsbegrenzung verletzen.

.. figure:: gegenseitiger_ausschluss_zweier_prozesse.png
	 :alt: gegenseitiger_ausschluss_zweier_prozesse

:math:`s_2` und :math:`s_4` können nie gleichzeitig belegt sein, da entweder
:math:`t_1` oder :math:`t_3` eine Marke and der Stelle :math:`s_3` nutzen.

Bei Erweiterung um gewichtete Kanten können auch mehrere Marken auf einmal bewegt
werden.

.. figure:: petri_netz_gewichtete_kanten.png
	 :alt: petri_netz_gewichtete_kanten

Definition
----------

Ein Petri-Netz (Stellen-Transitions-Netz) ist ein 5-Tupel:

.. math:: P=(S,T,A,E,M)

- :math:`S` ist nicht leere Menge von Stellen
- :math:`T` ist nicht leere Menge von Transitionen mit :math:`S \cap T = \emptyset`
- :math:`A \subset S \times T` - Ausgangskanten von Stellen; Eingangskanten von Transitionen
- :math:`A \subset T \times S` - Eingangskanten von Stellen; Ausgangskanten von Transitionen
- :math:`M : S \rightarrow \mathbb{N}_0` Startmarkierung


.. figure:: petri_netz_formales_beispiel.png
	 :alt: petri_netz_formales_beispiel

.. figure:: petri_netz_formale_schaltregeln.png
	 :alt: petri_netz_formale_schaltregeln

.. figure:: modellierungsprimitive.png
	 :alt: modellierungsprimitive

Deadlock

Versuche Kommunikation möglichst lokal zu halten
