======
Suchen
======

Sequenzielle Suche
==================

Bei der sequentiellen Suche wird die Folge von Eingabewerten, nacheinander nach
dem Schlüssel durchsucht.

.. figure:: sequential_search_algorithm.png
	 :alt: sequential_search_algorithm

.. csv-table::
  :header: "case", "number of comparisons"

  "best case", ":math:`1`"
  "worst case", ":math:`n`"
  "average (successful search)", ":math:`\frac{n+1}{2}`"
  "average (unsuccessful search)", ":math:`n`"

Ist die Folge von Eingabewerten sortiert, kann die Suche abgebrochen werden, sobald
aktuelle Wert größer als der gesuchte Wert ist. Dadurch werden die Average Laufzeiten
halbiert.

.. _binary-search:

Binäre Suche
============

Für die binäre Suche wird eine sortierte Folge benötigt. Der Schlüssel wird mit dem
Pivotelement in der Mitte der Folge verglichen. So kann die zu große/ zu kleine
Hälfte an Elementen für weitere Suche weggelassen werden. Man benötigt daher eine
Datenstruktur mit wahlfreiem Zugriff.

.. figure:: binary_search_ecample.png
	 :alt: binary_search_ecample

.. figure:: binary_search_algorithm_recursive.png
	 :alt: binary_search_algorithm_recursive

.. figure:: binary_search_algorithm_iterative.png
	 :alt: binary_search_algorithm_iterative

.. csv-table::
  :header: "case", "number of comparisons"

  "best case", ":math:`1`"
  "worst case", ":math:`\log_2(n)`"
  "average (successful search)", ":math:`\log_2(n)`"
  "average (unsuccessful search)", ":math:`\log_2(n)`"

Bei Folgen mit ungerader Elemente Anzahl, wird in der Regel abgerundet.

.. _breadth-first-search:

Breitensuche
============
Bei der Breitensuche werden die Knoten eines Graphen nach der Entfernung von
einem Startknoten geordnet durchlaugen. Zuerst werden alle von diesem Startknoten
direkt erreichbaren Knoten bearbeitet, danach die nur durch mindestens zwei Kanten,
dann die durch drei etc.

Das Verfahren ist im Englischen als **breadth-first-search** (BFS) bekannt.

Ausgehend vom Startknoten werden die einzelnen Level des Graphen nacheinander
durchlaufen.

Die Breitensuche im Graphen ist analog zur Breitensuche im Bäumen. Diese entspricht
einer Traversierung des Graphen in **Levelorder**.

.. figure:: breitensuche_example.png
	 :alt: breitensuche_example

.. only:: html

	.. figure:: breitensuche_animated_example.gif
		 :alt: breitensuche_example_gif

.. code::

	Breadth-First-Search(Graph, start):

		create empty set S
		create empty queue Q

		add start to S
		Q.enqueue(start)

		while Q is not empty:
			current = Q.dequeue()
			if current is the goal:
				return current
			for each node n that is adjacent to current:
				if n is not in S:
					add n to S
					Q.enqueue(n)

		return goal not found;

Der Graph des Breitensuchlauf ist ein aufgespannter Baum mit dem Startknoten
der Breitensuche als Wurzel.

.. figure:: breitensuche_spannbaum_examle.png
	 :alt: breitensuche_spannbaum_examle

Der konkret berechnete aufgespannte Baum hängt von der Reihenfolge ab, in der die
ausgehenden Kanten eines Knotens bearbeitet werden. Wird diese Reihenfolge nicht
durch die Datenstruktur oder die Verfeinerung dieser Auswahl vorgegeben, ist
``BFS`` nichtdeterministisch.

Die Breitensuche kann eingesetzt werden um alle Knoten innerhalb einer
Zusammenhangskomponente zu bestimmen und um bei ungewichteten Kanten den kürzesten
Pfad zwischen zwei Knoten zu finden.


.. _depth-first-search:

Tiefensuche
===========

Bei der Tiefensuche (im Englischen: depth-first-search DFS) geht man zuerst
in die Tiefe bis man nicht mehr weiterkommt und dann in die Breite. Die Tiefensuche
versuch einen gewählte Pfad so weit wie möglich entlang zu gehen.

.. only:: htmml

	.. figure:: tiefensuche_animated.gif

Hier wird ein Stack benötigt.

1. Bestimme den Knoten, an dem die Suche beginnen soll
2. Speichere den kleinsten/größten (optional) noch nicht erschlossenen
   Nachfolger in einem Stack.
3. Rufe rekursiv für jeden der Knoten im Stack die Tiefensuche auf

   - Falls der Stack leer ist, liefere ``nicht gefunden`` zurück
   - Falls es keine nicht erschlossenen Nachfolger mehr gibt, lösche den
     obersten Knoten aus dem Stack und rufe für den jetzt oberen Knoten
     im Stack die Tiefensuche auf.
   - Falls ds gesuchte Element gefunden wurde, brich die Suche ab und liefere das Ergebnis.

.. code::

	algorithm DFS(G)
		Eingabe: ein Graph G

		for each Knoten u in V[G] do
			farbe[u] = weiß;
			pi[u] = null;
		od;
		zeit = 0;
		for each Knoten u in V[G] do
			if farbe[u] = weiß then
				DFS-visit(u)
			fi
		od

	algorithm DFS-visit(u)
		Eingabe ein Knoten u

		farbe[u] = blau;
		zeit = zeit + 1;
		d[u] = zeit;

		for each v in ZielknotenAusgehenderKanten(u) do
			if farbe[v] = weiß then
				pi[v] = u;
				DFS-visit(v)
			fi
		od;
		farbe[u] = schwarz;
		zeit = zeit + 1;
		f[u] = zeit;

.. figure:: tiefensuche_beispiel_1.png
	 :alt: tiefensuche_beispiel_1

Die Tiefensuche in Graphen ist analog zur Traversierung von Bäumen in **Preorder**.

Test auf Zyklenfreiheit
-----------------------
Mit der Tiefensuche lässt sich auch die Zyklenfreiheit von Graphen überprüfen.
Bei der DFS werden Back-Edges erkannt.

Ein gerichteter Graph :math:`G` ist genau dann zyklenfrei, wenn er keine
Back-Edges besitzt.

.. _topological_sorting:

Topologisches Sortieren
=======================

Das Topologische Sortieren ermöglicht es eine Reihenfolge zwischen kausalen oder
zeitlichen Abhängigkeiten in einem azyklischen gerichteten Graphen zu bestimmen.
Bei der Topologischen Sortierung wird eine Reihenfolge der Knoten bestimmt, sodass
jeder Knoten nach all seinen Vorgängern kommt.

Werden die gerichteten Kanten als kausale oder zeitliche Abhängigkeit aufgefasst,
so spricht man auch vom **Scheduling-Problem**.

Mathematisch gesehen, konstruiert die Topologische Sortierung eine totale Ordnung
aus einer Halbordnung.
Das Topologische sortieren mit Totalordnung funktioniert nur bei Graphen ohne Zyklen.


Vorgehen:
	- Suche nach möglichen Startknoten (Knoten mit dem Eingangsgrad 0)
	- Tiefensuche ausgehend von jedem  Startknoten
	- Wurde ein Knoten vollständig

.. todo:: Abschnitt fertigstellen

Man findet in dem Graphen immer eine Startknoten (Knoten ohne eingehende Kanten),
da er azyklisch ist. Ist der Graph zyklisch kann keine Topologische Sortierung
gefunden werden. Auf Graphen mit zyklischen Abhängigkeiten kann es kein
Scheduling geben :math:`A \rightarrow B \land B \rightarrow A`.

Die Tiefensuche traversiert den Graphen nur. Das Topologische Sortieren sortiert,
basierend auf der Tiefensuche, die Knoten nach ihren Abhängigkeiten zueinander.
Zum Schluss entsteht ein Scheduling.

.. figure:: topologische_sortierung_beispiel_1.png
	 :alt: topologische_sortierung_beispiel_1

.. figure:: topologische_sortierung_beispiel.png
	 :alt: topologische_sortierung_beispiel
