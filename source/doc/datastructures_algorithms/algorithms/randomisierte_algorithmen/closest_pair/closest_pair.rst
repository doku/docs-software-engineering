============
Closest Pair
============

Man möchte das Punktepaar mit minimaler Distanz aus einer Punktmenge bestimmen.

1. Versuch
----------
- Betrachte :math:`p_1` und berechne die Distanzen zu :math:`p_2, p_3, ..`
- Betrachte :math:`p_2` und berechne die Distanzen zu :math:`p_3, p_4, ..`
- ...
- Gebe Minimum aller Distanzen aus

.. math::
  \mathcal{O}(\sum^{n-1}_i=1)
  = \mathcal{O}(\frac{(n-1)*n}{2})
  = \mathcal{O}(n^2)

Laufzeit von :math:`\mathcal{O}(n^2)` macht den Algorithmus völlig unpraktikabel
für Nicht-Spielzeugprobleme.

Das Closest Pair Problem kann deterministisch in :math:`\Omega(n*\log n)`
berechnet werden. Man kann zeigen, dass das Closest Pair vergleichsbasiert
deterministisch mindestens :math:`\Omega(n \log n)`.

Man weiß, dass Element uniqueness (gegeben :math:`n` Zahlen, entscheide, ob eine
Zahl doppelt vorkommt) :math:`\Omega (n \log n)` braucht.
Falls CP deterministisch, vergleichsbasiert mit Laufzeit :math:`o(n \log n)`
gelöst werden könnte, könnte man auch Element uniqueness auch in dieser Zeit
lösen (Zahl :math:`i \rightarrow Punkt (i,i) \in \mathbb{R}^2`).
Wir nutzen allerdings Randomisierung und Abrunden und können so eine schnellere
Laufzeit erreichen.

Bei großen Datenmengen sind nur solche **fast-linearen** Algorithmen sinnvoll
einzusetzen.

Randomisierter Algorithmus
--------------------------
Wir entwerfen einen einfachen randomisierten Algorithmus mit erwarteter Laufzeit
von :math:`\mathcal{O}(n)`. Die Variation der Laufzeit hängt nicht von der Eingabe
ab, sondern nur von dem Glück bei dem Zufallswurf. Wenn :math:`X` die
Zufallsvariable der Laufzeit ist, so ist :math:`E(X) = n`.

Der CP Algorithmus berechnet immer das korrekte Resultat, nur die Laufzeit hängt
vom Zufall ab (**Las Vegas Algorithmus**).

Inkrementeller Algorithmus für Closest Pair
-------------------------------------------
Betrachte die Punktmenge :math:`P` in Reihenfolge :math:`p_1, p_2, ... p_n`.
Sei :math:`\delta_i` die CP-Distanz der Punkte :math:`\{p_1, ... p_i\}`.

Angenommen wir kennen :math:`\delta_i`, wie können wir :math:`\delta_{i+1}` bestimmen?

- Naiv: Vergleiche :math:`p_{i+1}` mit :math:`p_1, p_2, .. p_i` und setze
  :math:`\delta_{i+1} = \min(\delta_i, \min_{j=1 , .. i}(p_{i+1}, p_j))` wieder
  :math:`\mathcal{O}(n^2)`

Angenommen wir haben :math:`\delta_i` bestimmt und auch ein Gitter mit Maschenweite
:math:`\delta_i` erzeugt, in welche alle Punkte :math:`\{p_1, .. p_i\}` eingeordnet
sind.

.. figure:: closest_pair_gitter.png
	 :alt: closest_pair_gitter

- lokalisiere :math:`p_{i+1}` im Gitter
- inspiziere die Punkte in der Zelle von :math:`p_{i+1}` und den 8 Nachbarzellen.
- falls :math:`p_{i+1}` mit einem dieser ein neues CP bildet setzte :math:`\delta_{i+1}`
  entsprechend, sonst :math:`\delta_{i+1} = \delta_i`

Es gilt: in einem Kästchen können nur konstant :math:`\mathcal{O}(1)` viele Punkte liegen.
Für gegebenes Gitter kostet Bestimmung von :math:`\delta_{i+1}` :math:`\mathcal{O}(1)`, falls man in
:math:`\mathcal{O}(1)` auf die Gitterzellen/inhalte zugreifen kann.

- Falls :math:`\delta_{i+1} = \delta_{i}` füge :math:`p_{i+1}` hinzu
- Falls :math:`\delta_{i+1} < \delta_i`, dann muss man das Gitter mit neuer
  Maschenweite :math:`\delta_{i+1}` neu aufbauen in :math:`\mathcal{O}(i+1)`

Bei unglücklich gewählter Punktmenge und Reihenfolge kann es sein, dass in jedem
Schritt das Gitter neu konstruiert werden muss. In diesem Fall hat der CP Algorithmus
eine Laufzeit von :math:`\mathcal{O}(n^2)`.

Wir wählen eine **zufällige Permutation** der Punktmenge, dh. jede Permutation
mit der Wahrscheinlichkeit :math:`\frac{1}{n!}`.

Die Wahrscheinlichkeit, dass :math:`\delta_{i+1} < \delta_{i}` (genau dann muss
das Gitter neu aufgebaut werden) ist höchstens :math:`\frac{2}{i+1}`.

Das Gitter muss genau dann neu aufgebaut werden, wenn :math:`p_{i+1}` einer der
beiden Punkte ist, welche das CP in der Menge der ersten :math:`i+1` Punkte bestimmen.
Jeder der ersten :math:`i+1` Punkte ist mit gleicher Wahrscheinlichkeit der
:math:`p_{i+1}`.

Daraus folgt, dass :math:`P(\text{Gitter muss neu aufgebaut werden}) = \frac{2}{i+1}`
Unter der Annahme dass das CP eindeutig ist. Falls CP nicht eindeutig, gilt
:math:`\leq \frac{2}{i+1}`

Dann sind die erwarteten Kosten des Einfügens von :math:`p_{i+1}`

.. math:: \leq \frac{2}{i+1} \mathcal{O}(i+1) + k*\mathcal{O}(1) = O(1) + O(1) = O(1)

Erwartete Gesamtlaufzeit (mit Linearität des Erwartungswerts)

.. math::
  E(\sum^n_{i=1}(\text{Kosten für Einfügen von } p_i))

  = \sum^n_{i=1} E(\text{Kosten des Einfügen von } p_i )

  = \sum^n_{i=1}(\mathcal{O}(1)) = \mathcal{O}(n)
