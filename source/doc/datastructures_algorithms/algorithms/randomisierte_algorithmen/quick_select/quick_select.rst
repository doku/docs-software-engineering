============
Quick-Select
============

Quick-Select (Randomisiert)
===========================

Der Quick-Select kann das :math:`k`-kleinste Element in einem unsortierten
Array finden.

Sei :math:`A` zum Beispiel :math:`[3,5,1,4,7]` und :math:`k=2`, so soll der
Algorithmus :math:`3` zurückgeben, was dem 2. kleinsten Wert entspricht.

Der naive Ansatz wäre das Array mit einem Quicksort oder Mergesort zu sortieren und
dann den Wert :math:`A[k]` zurückzugeben. Man kann dann für jedes :math:`k` aus
dem sortierten Array in :math:`\mathcal{O}(1)` das :math:`k` kleinste Element
bestimmen. Das bedeutet, dass sich dieser Algorithmus nicht auf das Nötigste
beschränkt und zu viel macht.

Besser ist es einen Quicksort durchzuführen, wobei aber immer nur diejenige
Hälfte weiter sortiert wird, in der sich das gesuchte Element befindet.

.. figure:: quickselect_recursion.png
	 :alt: quickselect_recursion

.. code::

  Select(A,k):
    Wähle Pivotelement p zufällig gleichverteilt
    A1 := Array-Teil bis Index p
    A2 := Array-Teil ab Index p

    if k <= A1.length
      Select(A1, k)
    elif k> A2.length
      Select(A2, k-A1.length)

Der Randomisierte Quick-Select ist ein :ref:`las-vegas-algorithm`.

Laufzeit von Quick-Select (Randomisiert)
----------------------------------------

Falls immer der Median getroffen wird, so ergibt sich
:math:`n+\frac{1}{2}+\frac{1}{4} ... \leq 2n`

Mit :math:`X_{i,j}` wie bei Quicksort: :math:`E[X_{i,j}] = \frac{2}{j-i+1}`.
Hier gibt es nur die drei Fälle:

- :math:`k<i<j`
- :math:`i<k<j`
- :math:`i<j<k`

Die Laufzeit ist :math:`\mathcal{O}(n)`. Schneller ist nicht möglich, da jedes
Element betrachtet werden muss.

Quick-Select (Deterministisch)
==============================
In einigen Anwendungsszenarien ist es wichtiger, dass ein Ergebnis konstant
schnell vorliegt, als *wahrscheinlich schnell*.

Wir wählen daher das Pivotelement nicht randomisiert, sondern durch median-Berechnung.

Wahl des Pivotelements
----------------------

1. Man gruppiere die Elemente der Eingabestruktur in 5er Blöcke.
2. Man berechnet in jeder Gruppe den Median (geht in sechs Vergleichen, damit
   kann man die Mediane aller Gruppen in :math:`\mathcal{O}(n)` bestimmen).
   Es bleiben dann :math:`\frac{n}{5}` Gruppenmediane
   (:math:`M[i] := \text{Median der Gruppe } i`)
3. Man berechnet rekursiv den Median der Gruppenmediane: :math:`Select(M,\frac{\frac{n}{5}}{2})`
4. Der Median der Gruppenmediane wird als Pivotelement verwendet.

Das Pivotelement ist gut genug
------------------------------
Sei :math:`M` der Median der Gruppenmediane. Von den :math:`\frac{n}{5}`
Gruppenmedianen sind die Hälfte, also :math:`\frac{n}{10}`, größer bzw. kleiner als :math:`M`.
In jeder Gruppe :math:`i`, deren Gruppenmedian :math:`M[i] < M` ist, sind
:math:`3` Elemente :math:`<M` bzw. in jeder Gruppe deren Gruppenmedian
:math:`M[i] > M` ist, sind :math:`3` Elemente :math:`>M`.

Zusammen ergibt das, dass

- :math:`3*\frac{n}{10} \text{Elemente} <M`
- :math:`3*\frac{n}{10} \text{Elemente} >M`

Wählt man also nach dieser Methode das Pivotelement, so erhält man mindestens einen
30%-70% Split.

Das Pivotelement wird schnell genug gefunden
--------------------------------------------

Für die Laufzeit ergibt sich nach dem :ref:`master-theorem`:

.. math::

  T(n) \leq \underbrace{T(\dfrac{n}{5})}_{\substack{\text{Median} \\ \text{der} \\ \text{Mediane}}}
        + \underbrace{T(\dfrac{7}{10} \cdot n)}_{\substack{\text{Quick-Sort-} \\ \text{Rekursion}}}
        + \underbrace{\mathcal{O}(n)}_{\substack{\text{Pivotieren} \\ \text{+} \\ \text{Verwaltung}}}


Per Induktion kann nun gezeigt werden, dass :math:`T(n) \leq \gamma * n`. Im
Induktionsschritt zeigt sich dann:

.. math::

  T(n) & \overset{\text{IV}}{\leq} \gamma \cdot \frac{n}{5} + \gamma \cdot \frac{7}{10} \cdot n + c \cdot n\\\nonumber
       &= n \cdot (\frac{9}{10} \cdot \gamma + c)\\\nonumber
       &\leq \gamma \cdot n

Dies ist der Fall für :math:`\gamma =  10*c` Damit folgt:

.. math:: T(n) \in \mathcal{O}(n)

.. note::
  Somit lässt sich der Median in Linearzeit berechnen.

.. figure:: select_deterministic_example.png
	 :alt: select_deterministic_example


Concentration Bounds
====================
Bisher wurden hauptsächlich Analysen mithilfe des Erwartungswerts durchgeführt.
Der Erwartungswert macht jedoch keine Aussage über die Verteilung.

Man kann die Verteilung eines Algorithmus mit Erwartungswert :math:`L` generisch
analysieren. Der Algorithmus läuft :math:`\alpha * L` Schritte. Ist er danach
nicht fertig, wird wer neu gestartet.

Die Wahrscheinlichkeit, in einem Durchlauf nicht fertig zu werden ist
:math:`< \frac{1}{\alpha}`.

Die Wahrscheinlichkeit, in :math:`k` Durchläufen nicht fertig zu werden ist
:math:`\leq \frac{1}{\alpha}*\frac{1}{\alpha}*... = \frac{1}{\alpha^k}`

Beispiel 1
----------
Gegeben ist begrenzte Zeit :math:`t \underbrace{>>}_{\text{viel größer}} L`

Wie soll :math:`\alpha` gewählt werden?

Für gegebenes :math:`\alpha` können maximal :math:`\frac{t}{\alpha*L}` Runden
durchlaufen werden.

Die optimale Wahl von :math:`\alpha` ist :math:`\alpha = e`.

Beispiel 2
----------
Seien :math:`\alpha = 2` und :math:`k= \log(n)`, dann beträgt die erwartete Laufzeit
:math:`\frac{1}{n}`

Seien :math:`\alpha = 2` und :math:`K = 2\log(n)`, dann beträgt die erwartete Laufzeit
:math:`\frac{1}{n^2}`. Für diesen Fall liefert die :ref:`markov-ungleichung`:

.. math::
  P(\text{Algorithmus braucht länger als }2*\alpha*L*\log(n)) \leq
    \frac{1}{2*\alpha*\log(n)}

Anwendung des Quickselect
=========================

.. code::

	// returns the smallest element that is at least (n/9 aufgerundet) times in the array
	// returns NULL if there is no such element
	findaElementThatIsOftenInArray(array)
		for (i := 0; i < 9; i++)
				candidate := Quickselect(array, (i*n)/9 )
				candidateCounter := Count(array, candidate)
				if (candidateCounter >= (n/9))
						return candidate
		return NULL
