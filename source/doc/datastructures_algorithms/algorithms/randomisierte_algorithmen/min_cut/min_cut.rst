==============
MinCut-Problem
==============

In einem ungerichteter Graph :math:`G(V,E)` induziert eine Teilmenge
:math:`\emptyset \neq A \subsetneq V` einen **cut** wenn:

.. math:: cut(A, G) =  \{\{u, w\} \in E | v \in A, w \notin A \}

.. figure:: cut_beispiele.png
	 :alt: cut_beispiele

Der Wert/Größe des :math:`cut(A,G)` ist :math:`|cut(A,G)|`

Das MinCut Problem: Finde :math:`\emptyset \neq A \subsetneq V` sodass
:math:`|cut(A, G)|` minimal ist.

.. figure:: mincut_beispiel.png
	 :alt: mincut_beispiel

Der MinCut eines Graphens ist gleich seinem Maximalen Fluss (:ref:`maximaler-fluss`).

Wir wollen die Knoten in 2 Gruppen unterteilen, sodass die Anzahl der Kanten zwischen
den Gruppen minimal ist.

Naive Lösung
============
Wir probieren alle möglichen Teilmengen für :math:`A` aus, Laufzeit :math:`\mathcal{O}(2^n)`.

.. _krager-mincut-algorithmus:

Karger MinCut Algoritmus
========================
Der Algorithmus nimmt den Graph, sucht sich eine Kante aus und kontrahiert diese.
Die zentrale Operation des Algorithmus ist die Kanten Kontraktion.

.. figure:: kanten_kontraktion_example.png
	 :alt: kanten_kontraktion_example

.. code::

  Karger MinCut

  for i=0 to n-2
    kontrahiere zufällige Kante (gleichverteilt)

  return Knotenmenge, die einen der beiden verbleibenden Knoten entspricht.

:ref:`krager-mincut-algorithmus` ist ein :ref:`monte-carlo-algorithm`.

Der Algorithmus gibt zunächst nur die Knotenmenge zurück. Die Kantenmenge lässt
sich in Linearzeit bestimmen.

Die Anzahl der Kanten zwischen den verbleibenden 2 Knoten entspricht dem berechneten
MinCut (dieser kann falsch sein).

Abschätzung der Erfolgswahrscheinlichkeit
-----------------------------------------

Wir zeigen, dass Wahrscheinlichkeit für das richtige Ergebnis
:math:`\ge \frac{1}{n^2}` ist.
Wir nehmen an, das der MinCut eindeutig ist. Das ist er in der Regel zwar nicht,
aber die Wahrscheinlichkeit, eine korrekte MinCut Zerlegung zu bestimmen ist
höher falls der MinCut nicht eindeutig wäre.

.. hint::

	Der Algorithmus berechnet genau dann das richtige Ergebnis, wenn er nie eine
	der MinCut Kanten kontrahiert.
	Sobald man eine der MinCut Kanten kontrahiert, liefert er das falsche Ergebnis.

Sei :math:`k` die Größe des Mincuts (Anzahl der Kanten). Die Wahrscheinlichkeit
im ersten Kontraktionsschritt einen Fehler zu machen ist genau :math:`\frac{k}{m}`.
Wobei :math:`m` die Anzahl der Kanten im Graph ist und :math:`n` die Anzahl der
Knoten.
Die Wahrscheinlichkeit keinen Fehler zu machen ist :math:`1-\frac{k}{m}`.

--------------------------------------------------------------------------------

Lemma
^^^^^
Betrachte einen Multigraph (:term:`Multigraphen`) :math:`G(V,E)` mit einem MinCut mit Wert
:math:`k`. Dann gilt :math:`G` hat mindestens :math:`\frac{k*n}{2}` Kanten.

Wenn wir einen MinCut von :math:`k` haben, muss jeder Knoten :math:`A_i` mindestens den Grad
:math:`k` haben, da sonst :math:`|cut(A_i, G)| < k`, wodurch :math:`k` kein
MinCut wäre.

.. math:: m = \frac{ \sum_{v \in V} grad(v)}{2} \geq \frac{k*n}{2}

--------------------------------------------------------------------------------

Sei :math:`E_i` das Ereignis, dass im :math:`i`-ten Kontraktionsschritt keine
MinCut-Kante erwischt wurde.

Wir wissen :math:`Pr(E_1) = 1-\frac{k}{m} \overset{m \geq \frac{kn}{2}}\geq 1 - \frac{2}{n}`.

Falls :math:`E_1` eingetreten ist, existieren vor dem zweiten Schritt mindestens
:math:`\frac{k(n-1)}{2}` Kanten. Sei diese Anzahl :math:`m'`.
Dadurch ist die Wahrscheinlichkeit im zweiten
Schritt keinen Fehler zu machen, falls :math:`E_1` eingetreten ist:

.. math:: Pr(E_2 | E_1) \geq 1 - \frac{k}{m'} \geq 1 - \frac{2}{n-1}

Falls :math:`E_1, E_2` eingetreten sind, existieren vor dem dritten Schritt
mindestens :math:`\frac{k*(n-2)}{2}` Kanten

.. math:: Pr(E_3 | E_1 \wedge E_2) \geq 1 - \frac{2}{n-2}

Falls :math:`E_1 ... E_i` eingetreten sind existieren vor dem :math:`i+1` ten Schritt
mind. :math:`\frac{k*(n-i)}{2}` Kanten

.. math:: Pr(E_{i+1} | E_1 \wedge ... \wedge E_i) \geq 1-\frac{2}{n-i}

Uns interessiert:

.. math::

  Pr(\bigcap^{n-1}_{i=1} E_i)
  &= Pr(E_1) * Pr(E_2 | E_1) * Pr(E_3 | E_1 \wedge E_2) * ... *
      Pr(E_i | E_1 \wedge E_2 \wedge ... \wedge E_{i-1}) \\
  &\geq \prod^{n-2}_{i=1} (1-\frac{2}{n-i+1}) \\
  &= \prod^{n-2}_{i=1} (\frac{n-i+1-2}{n-i+1}) \\
  &= \prod^{n-2}_{i=1} (\frac{n-i-1}{n-i+1}) \\
  &= \frac{n-2}{n} * \frac{n-3}{n-1} * \frac{n-4}{n-2} * \frac{n-5}{n-3} * ... \\
  &= \frac{2}{(n-1)n} \\
	&\leq Pr(\text{Algorithmus macht das richtige})

Wir können die Erfolgswahrscheinlichkeit beliebig erhöhen, in dem wir den Algorithmus
:math:`r` mal ausführen:

.. code::

    RepeatMinCut(G, r)
      bestVal = infinite
      bestSol = null

      for i = 1 to r do
        A = KargurMinCut(G)

        if val(A) < bestVal
          bestVal = val(A)
          bestSol = A
      od

      return bestSol


Abschätzung der Fehlerwahrscheinlichkeit
----------------------------------------

Wir brauchen eine obere Schranke für die Wahrscheinlichkeit, dass der Algorithmus
immer das falsche macht.

Die Wahrscheinlichkeit, in jedem der :math:`r` Versuche **nicht** den MinCut zu
finden ist:

.. math::

  &\leq \left( 1- \frac{2}{n*(n-1)} \right)^r \\
	&< (e^{\frac{-2}{n*(n-1)}})^r \\
	&= e ^{ \frac{-2r}{n*(n-1)}}

mit :math:`1-x < e^{-x}`: wähle z.B. :math:`r = \frac{n(n-1)}{2}`
Wahrscheinlichkeit dafür immer den falschen MinCut zu bestimmen ist dann:
:math:`\leq \frac{1}{e} \approx 0,36`

Für :math:`r \approx n^2` bekommen wir also **konstante** Erfolgswahrscheinlichkeit.

Für :math:`r \approx n^2 \log n` ist das Ergebnis mit hoher Wahrscheinlichkeit
korrekt. D.h. Fehlerwahrscheinlichkeit ist :math:`\leq \frac{1}{n^c}`

Für :math:`r \approx n^3` ist das Ergebnis mit sehr hoher Wahrscheinlichkeit korrekt
Die Fehlerwahrscheinlichkeit ist dann: :math:`\leq \frac{1}{c^n}`
