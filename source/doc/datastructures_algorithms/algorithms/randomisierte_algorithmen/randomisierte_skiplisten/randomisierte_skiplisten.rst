========================
Randomisierte Skiplisten
========================

Randomisierte Skiplisten sind eine randomisierte Datenstruktur zur Verwaltung
von geordneten Mengen.

Ziel ist es die Operationen **Suchen**, **Einfügen** und **Löschen** in
erwarteter Laufzeit von :math:`\mathcal{O}(\log n)` zu unterstützen.
Die Suche nach :math:`x` liefert :math:`x`, falls :math:`x` nicht in der
Liste enthalten ist, wird das maximale Element :math:`x'` mit :math:`x'<x`
zurückgegeben.

Alternative Datenstrukturen werden aus verschiedenen Gründen nicht verwendet:

- Binärer Suchbaum und sortierte verkettete Liste: Suche in :math:`\mathcal{O}(n)`
- Balancierter Suchbaum zu kompliziert zu implementieren
- Sortiertes Array: Einfügen und Löschen in :math:`\mathcal{O}(n)`

.. figure:: skipliste_example.png
	 :alt: skipliste_example

Durch zusätzliche lange Pointer kann man sich in der Liste schneller Bewegen.

Jedes Element hat einen Turm von Pointern. Die Höhe dieses Turms wird zufällig
bestimmt.

.. math::

  P(\text{Height} =i) &= \frac{1}{2^{i+1}} \\
  P(h=0) &= \frac{1}{2} \\
  P(h=1) &= \frac{1}{4} \\
  P(h=2) &= \frac{1}{8} \\

Jede Turmetage wird mit dem nächsten Element verbunden, dessen Turm mindestens
genauso hoch ist.

Die Erwartete Größe einer Skipliste mit :math:`n` Elementen ist in :math:`\mathcal{O}(n)`
da die Turmhöhen der einzelnen Elemente erwartete in :math:`\mathcal{O}(1)` sind.
Der Erwartungswert der Höhe eines Turmes ist :math:`2`.

Search
======

.. figure:: skiplist_search.png
	 :alt: skiplist_search

.. code::

  Search(x):
    v := -infty-Turm
    h := v.height

    while (h > 0):
      while (x <= v.forward[h]->key):
        v <- v.forward[h]
    return v

Randomisierte Skiplisten sind Datenstrukturen nach dem Las-Vegas Verfahren
(:ref:`las-vegas-algorithm`).

Delete
======

.. code-block:: java

	delete(deleteKey):

		currentElement = head;

		for(h = currentElement.height; h>0; h--) {

			// go to the last element with a value smaller than
			// the deleteKey at height h
			while(currentElement.forward[h].key < deleteKey) {
				currentElement = currentElement.forward[h];
			}

			// if currentElement at height h points the deleteKey
			// replace the pointer in currentElement to the next
			// element at height h (deleteKey[h] points to that)
			if(currentElement.forward[h].key == deleteKey) {
				deleteElement = currentElement.forward[h];
				currentElement.forward[h] = deleteElement.forward[h];
			}
		}

Insert
======

.. code-block:: java

	insert(insertKey):

		// create Skiplistelement with random height
		insertElement = new SkipListElement()
		currentElement = head;

		// start height has to be the highest, so that you are able to use long jumps
		for(h = head.height; h>0; h--) {

			// go to the last element with a value smaller than
			// the insertKey at height h
			while(currentElement.forward[h].key < insertKey) {
				currentElement = currentElement.forward[h];
			}

			//pointer routing for new element
			insertElement.forward[h] = currentElement.forward[h]
			currentElement.forward[h] = insertElement

		}

Laufzeit von Search(x)
======================

Beweis mit :math:`X_{i,h}` möglich

.. math::

  X_{i,h} =
        \begin{cases}
               1 & \text{Falls Turm } i \text{ auf Höhe  } h \text{ besucht} \\
               0 & \text{sonst}
        \end{cases}

Besser wäre es jedoch eine Routine zu bauen, die dieselbe Zellen besucht
wie die Suche, jedoch einfacher zu analysieren ist.

Rückwartsanalyse von Search
---------------------------

.. code::

  v := x
  h := 0

  while (v != -infty && h != h_max):
    if(v.height > h):
      h = h + 1
    else:
      v = v.backward[h]
  return v

Man muss sich also den ``Search(x)`` Algorithmus rückwärts vorstellen. Damit
lässt sich beobachten, dass die Wahrscheinlichkeit, dass sich über der
aktuellen Zelle noch eine weitere Zelle befindet :math:`\frac{1}{2}` ist.

Im ``if-else`` Zweig entscheidet sich, ob wir nach oben :math:`WS = \frac{1}{2}`
oder nach links :math:`WS = \frac{1}{2}` weitergehen. D.h. die erwartete
Anzahl der Linksschritte ist gleich der erwarteten Anzahl an Hoch-Schritte.

Die :math:`h_i` sind unabhängige (:ref:`unabhaengige-zufallsvariablen`),
geometrisch verteilte Zufallsvariablen (:ref:`geometrische-verteilung`):

.. math::

  &P(h_i \geq h) = 1-P(h_i<h) = 1-P(h_i \leq h-1) = 1-F^{h_i} = 1-(1-(\frac{1}{2^h})) = 2^{-h} \\

Mit hoher Wahrscheinlichkeit, ist den Turm :math:`h_i` nicht höher als :math:`\log_2(n)`, da:

.. math::	P(h_i \geq 2*\log_2(n)) = \frac{1}{n^2}

Die Wahrscheinlichkeit, dass ein Turm höher als :math:`h` ist, ist gleich,
der Wahrscheinlichkeit, dass der Turm genau :math:`h` hoch ist.

Die Erwartete Höhe eines Turmes ist konstant. Das folgt aus dem Erwartungswert
der ZVs :math:`h_i` (mit der ersten Ableitung der :ref:`geometrische-reihe`):

.. math::
	E(h_i) &= \sum_{h=0}^{\infty} h*P(h_i = h) \\
	&= \sum_{h=0}^{\infty} h* \frac{1}{2^{h+1}} \\
	&= \frac{1}{2} \sum_{h=0}^{\infty} h* \frac{1}{2}^h \\
	&= \frac{1}{2} \frac{1}{(1- \frac{1}{2})^2} = 2

Für die Wahrscheinlichkeit, dass irgendein Turm Höhe :math:`h_i \geq 2 \log_2(n)`
ist (unter Verwendung der Unabhängigkeit der :math:`h_i`):

.. math::

  &P(h_1\geq 2*\log(n) \cup h_2\geq 2*\log(n) \cup ... \cup h_n\geq 2*\log(n)) \\
  &=\sum_{i=0}^n P(h_i \geq 2*\log_2(n)) \\
	&=\underbrace{\frac{1}{n^2} + \frac{1}{n^2} + ... + \frac{1}{n^2}}_n \\
  &=\frac{n}{n^2} = \frac{1}{n}

Damit können wir mit hoher Wahrscheinlichkeit damit rechnen höchstens
:math:`\mathcal{O}(\log n)` viele Hoch-Schritte
und damit auch bei :math:`\mathcal{O}(\log n)` viele Links-Schritte der
Rückwärtsanalyse zu machen.

Mit hoher Wahrscheinlichkeit ist die Suche damit in :math:`\mathcal{O}(\log(n))`,
da wir bei der Suche die selben Schritte wie bei der Rückwärtsabalyse machen.

Die Laufzeit von ``Search(x)`` lässt sich mit dem :ref:`master-theorem`
untersuchen.
