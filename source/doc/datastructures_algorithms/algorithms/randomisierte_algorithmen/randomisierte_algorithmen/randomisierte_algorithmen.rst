

=========================
Randomisierte Algorithmen
=========================

Ein randomisierter Algorithmus ist ein Algorithmus der unter Nutzung einer Zufallsquelle
(Münzwurf, Zufallsgenerator) ein Problem löst.

Oft sind die rand. Algorithmen deutlich einfacher (und manchmal auch effizienter)
als die entsprechenden deterministischen Algorithmen

Typischerweise analysierte man Algorithmen bzgl. Platz- und Zeitbedarf.
rand. Algorithmen kann man auch bzgl. "Verbrauch von Zufall" analysieren.

Die Randomisierten Algorithmen laufen auch bei identischer Eingabe verschieden ab.

Die Randomisierten Algorithmen und **Probabilistische Analyse** stehen im Kontrast.
Bei der probabilistischen Analyse liegt die Zufallskomponente in der Eingabe und
der Algorithmus läuft bei gleicher Eingabe immer gleich ab.
Bei Randomisierten Algorithmen enthält der Algorithmus selbst die Zufallskomponente,
sodass diese auch bei gleicher Eingabe nicht immer gleich ablaufen.

.. _monte-carlo-algorithm:

Monte Carlo Algorithmen
=======================

Bei Monte Carlo Algorithmen hängt die Laufzeit nicht vom Zufall ab, aber die
Korrektheit hängt vom Zufall ab. Die Korrektheit ist unabhängig von der Eingabe.

Monte-Carlo-Methode
-------------------

Die Monte-Carlo-Methode ist ein Berechnungsverfahren zur Berechnung numerischer
Resultate durch das randomisierte Abtasten. Hier wird das Gesetz der großen Zahlen
(Konvergenz des Mittelwerts der relativen Häufigkeit zum Erwartungswert) verwendet.

.. _las-vegas-algorithm:

Las Vegas Algorithmen
=====================

Las-Vegas-Algorithmen berechnen stets das korrekte Ergebnis oder geben aus,
dass das korrekte Ergebnis nicht gefunden wurde. Die Zufallskomponente beeinflusst
die Laufzeit.

Las Vegas Umwandlung zu Monte Carlo
===================================

Um aus einem Las Vegas Algorithmus einen Monte Carlo Algorithmus zu machen,
lässt man den Las Vegas Algorithmus eine bestimmte Anzahl von Schritten laufen,
und bricht ihn dann ab.

Sei :math:`A` ein LV-Algorithmus mit erwarteter Laufzeit :math:`f(n)`. Lasse :math:`A`
für maximal :math:`\alpha f(n)` mit :math:`\alpha \geq 1` viele Schritte laufen
und breche ihn dann ab.

Falls der :math:`A` zu diesem Zeitpunkt bereits sowieso
fertig ist, so liefert der resultierende Monte Carlo Algorithmus das korrekte
Ergebnis.

Ist :math:`A` zum Zeitpunkt des Abbruchs noch nicht fertig, dann
liefert der resultierende Monte Carlo Algorithmus ein falsches Ergebnis (er
könnte beliebiges ausgeben).

Dieser modifizierte Algorithmus hat immer die Laufzeit :math:`< \alpha f(n)`

Die Wahrscheinlichkeit, dass er beliebiges ausgibt, ist gleich der Wahrscheinlichkeit,
dass :math:`A` länger als :math:`\alpha f(n)` Zeit benötigt.

In unserem resultierenden Monte Carlo Algorithmus, der nach :math:`\alpha f(n)`
abbricht, bekommen wir nach :ref:`markov-ungleichung` eine
Fehlerwahrscheinlichkeit :math:`\leq \frac{1}{\alpha}`.

Monte Carlo Umwandlung zu Las Vegas
===================================

Es ist allgemein nicht möglich jeden Monte Carlo Algorithmus in einen Las Vegas
Algorithmus umzuwandeln.

Für manche Probleme ist Verifikation des Ergebnisses einfacher als die Berechnung.
z.B. Sortieren überprüfen geht in Linearzeit und kürzeste Wege Verifikation in
:math:`n \log (n+m)` vs :math:`O(m)`

Es ist möglich einen Monte-Carlo Algorithmus in einen Las Vegas Algorithmus zu
überführen, wenn man einen effizienten Verifizierer hat.

Sei :math:`A` ein Monte Carlo Algorithmus mit Laufzeit :math:`f(N)`. Sei :math:`C`
ein Verifikationsalgorithmus mit Laufzeit :math:`g(n)`. Die
Erfolgswahrscheinlichkeit von :math:`A` sei :math:`p(n)`

Dann lässt sich ein Las Vegas Algorithmus wie folgt konstruieren:

#. Lasse :math:`A` laufen
#. überprüfe Ergebnis mit :math:`C`: falls korrekt fertig, sonst goto 1)

Die erwartete Laufzeit :math:`R` ist:

.. math::

  R &= p(n) * (f(n)+g(n)) \\
    &\quad + ((1 - p(n)) * p(n) * (f(n) + g(n))) * 2 \\
    &\quad + ((1 - p(n))^2 * p(n) * (f(n) + g(n))) * 3 \\
    &\quad +\ ... \\
    &=f(n) + g(n)) * p(n) * \sum^{\infty}_{i=1}((1-p(n))^i \\
    &<\frac{f(n) + g(n)}{p(n)}) * (i+1)

Bei diesem entstanden Algorithmus kann jede Beliebige hohe Laufzeit auftreten, die Wahrscheinlichkeit,
dafür wird beliebig klein.
