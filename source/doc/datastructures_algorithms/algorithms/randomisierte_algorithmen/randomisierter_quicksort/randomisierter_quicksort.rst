========================
Randomisierter Quicksort
========================

Pivot Element
=============

Ein :ref:`quicksort` mit randomisierter Wahl des Pivotelements ist ein :ref:`las-vegas-algorithm`.

.. code::

  RandomisierterQuicksort(A[1, ..., n])

  	wähle p aus A zufällig gleichverteilt
  	sortiere A sodass p an finaler Position steht

.. figure:: quicksort_aufteilung.png
	 :alt: quicksort_aufteilung

Der Algorithmus wird dann für die beiden Seiten :math:`A_L` und :math:`A_R` rekursiv
aufgerufen.

Idealerweise ist p immer genau der mittlere Wert, da er dann :math:`A_L` und :math:`A_R`
gleichmäßig aufteilt. Dafür lässt sich :math:`p` via Medianberechnung in :math:`O(n)`
bestimmen. Dieser Aufwand würde sich gegenüber der randomisierter Pivotwahl oder
den anderen Möglichkeiten ein Pivotelement zu bestimmen nicht lohnen.

Laufzeit Analyse
================

1. Versuch einer Analyse (FALSCH)
---------------------------------
Was ist die erwartete Länge von :math:`A_L` und :math:`A_R`?

.. math::
	E(|A_L|) = E(|A_R|) = \sum_{i=0}^{n-1} \frac{1}{n}*i = \frac{1}{n} * \frac{(n-1)*n}{2}
	= \frac{n-1}{2}

Die Tiefe des Rekursionsbaums ist erwartet :math:`\mathcal{O}(\log n)`.

.. figure:: random_quicksort_binarytree.png
	 :alt: random_quicksort_binarytree

Dieser Ansatz der Analyse kann nicht richtig sein, den folgender Quicksort-Algorithmus
hätte nach selber Folgerung eine Laufzeit von :math:`\mathcal{O}(n \log n)`, obwohl
dieser offensichtlich in :math:`\mathcal{O}(n^2).`

.. code::

  QuicksortRandomWorst(A[1, ..., n])
    wähle Pivot p
      mit 50% Wahrscheinlichkeit das kleinste Element in A
      mit 50% Wahrscheinlichkeit das größte Element in A

    Füge p an die richtige Stelle ein.

In diesem Fall gilt für die Länge von :math:`A_L` und :math:`A_R`:

.. math::
  E(|A_L|) = E(|A_R|) = \frac{1}{2} * 0 + \frac{1}{2} (n-1) = \frac{n-1}{2}


Korrekte Analyse
----------------

Angenommen, die zu sortierenden Elemente sind :math:`s_1, s_2, s_3, ...` mit
:math:`s_1 < s_2 < s_3 < ... < s_n`

Wir definieren die Zufallsvariable :math:`X_{i,j}`:

.. math::
  X_{i,j} =
    \begin{cases}
      1 \text{ falls während RQS } s_i \text{ mit } s_j \text{ verglichen werden, } \\
      0 \text{ sonst}
    \end{cases}


Gesamtlaufzeit der RQS ist:

.. math:: \sum_{i<j} X_{i,j} = \sum_{i=1}^{n-i} \sum_{j=i+1}^{n} X_{i,j}

Der Erwartungswert der Gesamtlaufzeit ist somit:

.. math::

  E\left(\sum_{i<j}X_{ij}\right)  &= \sum_{i<j}E(X_{ij}) \text{ (Linearität des Erwartungswerts)} \\
                                  &= \sum_{i<j}(0 \cdot P(X_{ij} = 0) + 1 \cdot P(X_{ij} = 1)) \\
                                  &= \sum_{i<j}\underbrace{P(X_{ij}=1)}_{P_{ij}} \\
                                  &= \sum_{i<j} p_{ij}

Beim Quicksort werden zwei nebeneinander liegende Elemente immer miteinander
verglichen.
Die Wahrscheinlichkeit, dass :math:`s_1` und :math:`s_n` miteinander verglichen
werden ist :math:`\frac{2}{n}` da es 2 Möglichkeiten gibt: wähle :math:`p = s_1`
oder :math:`p = s_n`

Die Wahrscheinlichkeit, dass 2 Werte miteinander verglichen werden, hängt stark
davon ab, wieweit die Werte der Elemente voneinander entfernt sind.

Elemente mit großer Rangdifferenz werden nur mit geringerer Wahrscheinlichkeit
verglichen, da viele Chancen bestehen, dass sie im Rekursionsbaum getrennt werden.

Elemente mit kleiner Rangdifferenz werden eher verglichen bzw. müssen sogar
verglichen werden (bei Rangdifferenz = 1).

Wir stellen den Ablauf des Quicksorts als Rekursionsbaum dar, wobei ein Knoten
das Pivotelement im entsprechenden Aufruf darstellt.

.. figure:: quicksort_permutationen_levelorder.png
	 :alt: quicksort_permutationen_levelorder

.. hint::
  Permutation :math:`\pi` ist nicht zufällig gleichverteilt, da manche Permutationen
  gar nicht vorkommen können.

:math:`s_i` und :math:`s_j` werden genau dann nicht miteinander verglichen, wenn ein Element
:math:`s_k` mit :math:`s_i < s_k < s_j` als Pivotelement vor :math:`s_i` und vor :math:`s_j` in
:math:`\pi` auf.
(denn dann werden :math:`s_i` und :math:`s_j` auf verschiedene Mengen :math:`A_L`
und :math:`A_R` gesplittet)

:math:`s_i` wird mit :math:`s_j` verglichen genau dann, wenn :math:`s_i` oder
:math:`s_j` das erste Element aus :math:`\{s_i, s_{i+1}, .. s_j\}` in :math:`\pi` ist.

Mit gleicher Wahrscheinlichkeit ist jedes der Elemente :math:`\{s_i, ... , s_j\}`
das erste, welches in :math:`\pi` auftaucht. Die Wahrscheinlichkeit, dass
:math:`s_i` mit :math:`s_j` verglichen werden, beträgt also (da
:math:`|\{s_i, ... , s_j\}| = j-i+1`):

.. math:: P_{i,j} := Pr(s_i \text{ wird mit } s_j \text{ verglichen }) = \frac{2}{j-i+1}


Damit kann nun die zu erwartende Laufzeit bestimmt werden.

.. math::

  E\left(\sum_{r<j}X_{ij}\right)
    &= \sum_{i+1}^{n-1} \sum_{j=i+1}^{n}E(X_{ij}) \\
    &= \sum_{i+1}^{n-1} \sum_{j=i+1}^{n}P_{ij}  \\
    &= \underbrace{\sum_{i+1}^{n-1} \sum_{j=i+1}^{n}\frac{2}{j-i+1}}_{\frac{2}{2}+\frac{2}{3}+\frac{2}{4}+\frac{2}{5}+\ ...} \\
    &\leq \sum_{i=1}^{n-1} \sum_{l=2}^{n}\frac{2}{l} \\
    &= \sum_{i=1}^{n-1}2 * \sum_{l=2}^{n} \frac{1}{l} \\
    &= \sum_{i=1}^{n-1}2 * \mathcal{O}(H_n) \\
    &= \sum_{i=1}^{n-1}2 * \mathcal{O}(\log(n)) \\
    &= \mathcal{O}(n * \log(n))

Dabei beschreibt :math:`H_n` die :math:`n`-te Harmonische Zahl
(:term:`Harmonische Zahlen`).

Somit ist die zu erwartende Laufzeit des Randomisierten Quicksorts
:math:`\mathcal{O}(n \log n)`.
