.. _master-theorem:

==============
Master Theorem
==============

Rekursionsgleichungen für *Divide & Conquer* Algorithmen (:ref:`divide-and-conquer`)
lassen sich mit dem Master-Theorem lösen.

.. math:: T(n) = a * T(\frac{n}{b}) + \Theta(n^c)

- :math:`a`: Anzahl der Teilprobleme (:math:`a \geq 1`)
- :math:`b`: Verkleinerung der Teilprobleme (:math:`b>1`)
- :math:`n^c`: Aufwand für Divide & Merge (:math:`c\geq 1`)

.. figure:: master_theorem.png
	 :alt: master_theorem

.. math::

  \sum^{\log_b(n)}_{l=0} n^c \cdot \underbrace{\left( \frac{a}{b^c}\right)^l}_{q}
  = n^c \cdot \sum^{\log_b(n)}_{l=0} q^l \quad \text{(Geom. Reihe)}

.. math::

  T(n)
  \begin{cases}
         \leq n^c \cdot \frac{1}{1-q} \in \Theta(n^c), & q < 1 \Leftrightarrow \log_b(a) < c \\
         = n^c \cdot log_b(n) \in \Theta(n^c \cdot \log(n)), & q = 1 \Leftrightarrow \log_b(a) = c \\
         > \in \Theta(n^{\log_b(a)} \cdot \log(n)), & q > 1 \Leftrightarrow \log_b(a) > c
  \end{cases}


oder Vereinfacht:

Seine :math:`a,b \in \mathbb{N}` mit :math:`b>1` und es gelte die Rekursionsgleichung:

.. math:: t(n) \in a*t(n/b) + \Theta(n^c)

Dann gilt:

.. math::

  t(n) \in
  \begin{cases}
         \Theta(n^c), & \text{ falls } a < b^c \\
         \Theta(n^c \cdot \log(n)), & \text{ falls } a = b^c \\
         \Theta(n^{\log_b(a)}), & \text{ falls } a > b^c
  \end{cases}


Master Theorem Allgemein
========================

.. math:: T(n) = \sum_{i=1}^m T(\alpha_i *n) + \Theta(n^k)

mit :math:`0 < \alpha_i <1, m>0, k \leq 0` gilt:

.. math::

  T(n) =
  \begin{cases}
         \Theta(n^k), & \text{ falls } \sum_{i=1}^m \alpha_i^k <1 \\
         \Theta(n^k \log(n)), & \text{ falls } \sum_{i=1}^m \alpha_i^k =1 \\
         \Theta(n^c) \text{ mit } \sum_{i=1}^m \alpha_i^c=1, & \text{ falls } \sum_{i=1}^m \alpha_i^k >1
  \end{cases}


Beispiel Quick-Sort
===================

(mit Pivot-Median)

.. math:: T(n) = 2 T(\frac{n}{2}) + \Theta(n)

Quick-Sort mit :math:`a=2`, :math:`b=2` und :math:`c=1` entspricht also der
zweiten Klasse: :math:`\log_2(2)=1=c`. Damit ist der Quick-Sort in
:math:`\Theta(n \log n)`.
