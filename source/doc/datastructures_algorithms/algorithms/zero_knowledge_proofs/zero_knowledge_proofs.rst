====================
Zero-Knowledge-Proof
====================

Anwendungsbeispiel:
Wie könnte Bank (Website) ihren Nutzern sicherstellen, dass sie wirklich zur Bank
gehört?

Highlevel Idee: Die Bank (Alice) soll den Nutzern (Bob) beweisen, ein Geheimnis
zu kennen, welches nur die Bank (Alice) kennen kann, allerdings ohne das Geheimnis
(Bob) zu verraten.

Gewünschte Eigenschaften des Protokolls:
========================================

- Falls Alice das Geheimnis nicht kennt, wird das mit hoher Wahrscheinlichkeit
  von Bob erkannt
- Alice gibt nichts über das Geheimnis Preis, was Bob nicht allein rausfinden könnte.

Graphisomorphie Problem
=======================

Das Protokoll des Zero-Knowledge-Proofs basiert auf dem Graphisomorphie Problem.
Gegeben sind zwei Graphen :math:`G_1(V_1, E_1)` und :math:`G_2(V_2, E_2)` und man
stellt sich die Frage ob :math:`G_1` und :math:`G_2` :term:`isomorph` sind.

.. hint::
	Klausuraufgabe Isomorphie: Bestimmen einer Isomorphie zwischen 2 Graphen
	oder bei gegebener Funktion bestimmen, ob diese eine Isomorphie ist.

.. figure:: graph_isomorphie.png
	 :alt: graph_isomorphie

Bis 2015 hatte man die Vermutung, dass Graphisomorphie NP-hart ist. Nun ist bekannt,
dass Graphisomorphie in Quasipolynomzeit lösbar ist: :math:`O(2^{{O(\log n)}^2})`.

Im folgenden wird angenommen, dass das Problem der Graphisomorphie für große Graphen
praktisch nicht lösbar ist.

Geheimnis von Alice
===================
Das Geheimnis von Alice ist ein Isomorphismus zwischen zwei öffentlichen Graphen
:math:`G_1` und :math:`G_2`. Alice kann :math:`G_1` und :math:`G_2` wie folgt erzeugen:

#. wähle :math:`G_1` zufällig
#. wähle zufällige Permutation :math:`\phi` der Knoten, um :math:`G_2` zu generieren.
#. :math:`\phi` bleibt geheim, :math:`G_1, G_2` werden veröffentlicht.

Protokoll
=========
Folgendes Protokoll erlaubt es, Alice Bob davon zu überzeugt, dass sie :math:`\phi`
kennt.

Alice permutiert :math:`G_j` wobei :math:`j \in \{1,2\}` zufällig mit einer zufälligen
Permutation :math:`\pi` zu einem Graphen :math:`H` (und stellt sicher :math:`H \neq G_1, G_2`)
und schickt :math:`H` zu Bob.

Bob möchte sich davon überzeugen, dass Alice die Isomorphie zwischen :math:`G_1` und
:math:`G_2` kennt indem er :math:`k \in \{1,2\}` zufällig wählt und Alice bittet die
Isomorphie zwischen :math:`G_k` und :math:`H` offenzulegen.

Falls Alice das Geheimnis :math:`\phi` kennt, kann sie immer :math:`\pi` bzw.
:math:`\pi \circ \phi` zurückgeben.

.. figure:: zkp_permutationen.png
	 :alt: zkp_permutationen

Falls Alice :math:`\phi` nicht kennt und Bob den Graph wählt aus dem Alice
:math:`H` generiert hat, kann sie leicht antworten, falls jedoch Bob den anderen
Graph wählt, müsste Alice das Graphisomorphieproblem praktisch lösen, um die
korrekte Antwort zu geben (was wir als nicht möglich angenommen hatten).

Bei :math:`f`-maliger Wiederholung ist die Wahrscheinlichkeit, dass Bob immer den
Graph erwischt, aus dem :math:`H` erzeugt wurde :math:`= \frac{1}{2^f}`.

Die Wahrscheinlichkeit, dass Bob erkennt, dass Alice :math:`\phi` nicht kennt
ist somit :math:`1 - \frac{1}{2^f}`.

Theorem
=======
Alice verrät nichts über :math:`\phi`.

Bob lernt Isomorphie zwischen z.B :math:`G_1` und einer zufälligen Permutation
von :math:`G_1`.
Das hätte er auch selber basteln können.
