======================================
Shortest Path: Contraction Hierarchies
======================================

Gegeben ist ein Graph :math:`G(V, E, c)`. Das Ziel ist es kürzesten
(schnellsten) Weg von Knoten :math:`s` nach Knoten :math:`t` zu finden.

Das einfache Standardverfahren für das Shortest Path Problem ist Dijkstra
(:ref:`dijkstra-algorithm`).
Allerdings benötigt Dijkstra auf Deutschland mit :math:`|V| \approx 20` Millionen
und :math:`|E| \approx 40` Millionen (ordentlich implementiert) etwa
5s auf Desktop Hardware.

.. figure:: shortest_path_algorithms_comp.png
	 :alt: shortest_path_algorithms_comp

Bei relativ statischen Graphen (wie das Straßennetz) ist es sinnvoll in einem
recht teuren Vorverarbeitungsschritt Hilfsinformationen zum Graphen zu berechnen,
um dann Shortest Path Anfragen (um Faktor :math:`10^5`) schneller beantworten
zu können.

Idee
====

Vorverarbeitung
---------------

- Erweiterung des Graphs um sogenannte "Shortcuts". Das sind zusätzliche Kanten,
  die kürzeste Wege repräsentieren
- Zuordnung eines sog. **Level** für jeden Knoten. (Level zwischen :math:`0, n-1`)

Anfrage
-------

Eine Anfrage für einen Shortest Path von :math:`s` nach :math:`t`, bearbeitet man
dann nach folgendem Muster:

#. Lasse Dijkstra von :math:`s` laufen und berücksichtige nur Kanten, die zu höher
   leveligen Knoten führen.
#. Lasse Dijkstra von :math:`t` laufen und berücksichtige nur Kanten, die zu
   höherlevligen Knoten führen.
#. Betrachte alle Knoten :math:`v`, die sowohl von :math:`s` als auch von :math:`t`
   besucht wurden. Die Kürzeste-Wege Distanz ist bestimmt dadurch, dass :math:`v`
   mit :math:`d_S(v) + d_T(v)` minimal.

Vorverarbeitung
===============
Die zentrale Operation bei Vorverarbeitung ist Knotenkontraktion eines Knotens :math:`v`.
Wir möchten einen Knoten aus dem Graphen löschen, aber so, dass sich die Kürzeste
Wegedistanz zwischen den Nachbarknoten nicht ändert.

Löschen wir einen Knoten :math:`v`, so fügen wir eine neune Kante (Shortcut)
zwischen jedem Nachbarpaar von :math:`v` mit Kosten "Weg über v" ein.

.. figure:: node_contraction.png
	 :alt: node_contraction

Manche dieser Shortcuts sind überflüssig, da bessere Pfade, als der über :math:`v`
existieren.

.. hint::
  Füge Shortcut zwischen Nachbarn :math:`x` und :math:`y` ein, genau dann, wenn
  Kürzester Weg zischen :math:`x` und :math:`y` muss über :math:`v` gehen.

.. code::

  Vorverarbeitungs loop
    counter = 0
    while (|V| > 1) do
      wähle ein v in V und kontrahiere es, füge Shortcuts zu E hinzu
      level[v] = counter++;
    od
    return level[] und alle kreierten Shortcuts

Korrektheit
===========
Betrachte einen kürzesten Pfad von :math:`s` nach :math:`t`, der

.. figure:: contraction_example_1.png
	 :alt: contraction_example_1

Die Kontraktionsreihenfolge entspricht der Ordnung der Knotenlevel.

Als erstes wird also Knoten :math:`v_2` kontrahiert:

.. figure:: contraction_example_2.png
	 :alt: contraction_example_2

Danach werden :math:`v_7` gefolgt von :math:`v_3`, usw. kontrahiert.

.. figure:: contraction_example_3.png
	 :alt: contraction_example_3


Kontraktionsreihenfolge
=======================

Man möchte möglichst wenige Shortcuts neu einfügen. Es gib daher verschiedene
Ansätze, die Anzahl der neu eingefügten Shortcuts durch die Kontraktionsreihenfolge
zu beeinflussen.

Edge Difference
---------------

Man berechnet vor der Kontraktion für jeden Knoten seine **Edge Difference**.

Die Edge Difference ist die Differenz der *Anzahl der neu einzufügenden Shortcuts*
und der *Anzahl der durch Kontraktion wegfallenden Kanten*

Man kontrahiert dann immer einen Knoten mit minimaler Edge-difference.

Nur das Minimieren der Shortcut Anzahl ist nicht ideal.

Zufällige Reihenfolge
---------------------

Kontrahieren in zufälliger Reihenfolge

Praktisch sehr gut
------------------

Wir erlauben, dass nicht benachbarte Knoten den selben Level haben dürfen.
In einer Kontraktionsrunde werden dann folgende Schritte durchlaufen:

#. Berechne eine :term:`Unabhängige Menge` :math:`I` von :math:`G` (mit eher keinem Grad)
#. Berechne Edge difference für alle :math:`v \in I`
#. Kontrahiere alle :math:`v \in I` mit den 75% kleinsten Edge-differences
#. alle Knoten in dieser Runde bekommen das selben Level.

Anmerkungen
===========

Die Vorverarbeitung benötigt ungefähr 4min für Deutschland, mit einigen
Optimierungen benötigt man dann für eine Anfrage weniger als 1ms

.. hint::
  Die Konkatenation zwischen zwei kürzesten Pfaden ist nicht zwingend wieder
  ein richtiger Kürzester Pfad.

Customizable Contraction Hierarchies: alle Shortcuts rein: dadurch schnelleres Updaten bei
Änderungen der Kartendistanzen, da nur Abbildung der Änderungen auf alle shortcuts

Resultat ist Pfad mit Shortcuts, die jedoch rekursiv entpackt werden können.


Noch schnellere Verfahren
=========================

- Transit Nodes ca. 10 :math:`\mu s`
- Hub Lables 10 :math:`\mu s`
- CRP :math:`< 1ms`


Bei Öffentlichen Netzen sind Kürzeste Wege suchen viel schwieriger, da
viele Verschiedene Verkehrsmittel zur Verfügung stehen. Hier arbeitet man mit
Transfer Patterns.
