=============
Shortest Path
=============

.. _dijkstra-algorithm:

Dijkstra Algorithmus
====================

Der Dijkstra Algorithmus berechnet den kürzesten Pfad von einem Startknoten zu allen
anderen Knoten.

Wir folgen immer derjenigen Kante die den kürzesten Streckenabschnitt vom Startknoten
aus verspricht.

.. code::

  algorithm Dijkstra (G,s)
    Eingabe: Graph G mit Startknoten s

    for each Knoten u in V[G] without s do
      D[u] := infinite; //the value D[u] is the distance from s to u
    od;

    D[s] := 0;
    PriorityQueue Q := V;

    while not isEmpty(Q) do
      u := extractMinimal(Q); //der minimale Distanzwert der noch nicht abgearbeitet wurde
      for each v in ZielknotenAusgehenderKanten(u) and v in Q do
        if D[u] + weight((u, v)) < D[v] then
          D[v] := D[u] + weight((u, v));
          adjustiere Q an neuen Wert D[v];
        fi
      od
    od

Der Dijkstra Algorithmus ist ein :ref:`greedy-algorithm`.
Man bearbeitet beim nächsten Durchlauf der Schleife immer den Knoten :math:`u` mit dem
Minimalen Distanzwert zum Startknoten :math:`s` aus der Prioritätswarteschlange. Dieser
Wert ist dann schon immer der endgültige Distanzwert von :math:`u` zu :math:`s`,
da jeder andere Pfad zu :math:`u` über einen anderen Knoten :math:`v` gehen müsste,
der aber bereits mindestens die gleiche Distanz zu :math:`s` habt wie :math:`u`.

Funktioniert nicht bei negativen Gewichtungen der Kanten, da dann die Annahme, dass
minimale Distanzwert bereits endgültig ist nicht stimmt.

A* Algorithmus
==============

Der A* Algorithmus ist eine Verallgemeinerung des Dijkstra Algorithmus mit einer
optimistischen Heuristik (Heuristik darf nicht überschätzen).

**Konsistenz der Heuristik**: Die Heuristik darf nicht größer geschätzt
werden als die tatsächlichen Kosten zum Nachbarn plus der geschätzte Wert des Nachbarn
(Dreiecksungleichung).

Für jeden Knoten gibt es drei Kostenfunktionen:

- :math:`g(u)` sind die bisherigen Kosten vom Startknoten bis :math:`u`
- :math:`h(u)` sind die geschätzten Kosten von :math:`u` bis zum Ziel
- :math:`f(u)` sind die Kosten vom Start zum Ziel über :math:`u` mit :math:`f(u) = g(u)+h(u)`

Die Knoten lassen sich in drei Gruppen einteilen:

- OPEN: Alle Knoten zu denen ein Weg gefunden wurde (dieser ist nicht notwendigerweise
  optimal)

  - Speichere :math:`f` Wert für jeden Knoten
  - Im nächsten Schritt wird der Knoten mit dem kleinsten :math:`f` Wert entnommen.

- CLOSED: Alle Knoten zu denen der kürzeste Weg bekannt ist
- Alle anderen Knoten: noch gar nicht betrachtet.

.. code::

  algorithm A*-Suche (G, start, ziel)
    Eingabe: Graph G mit Startknoten start und Zielknoten ziel

    PriorityQueue OPEN := <>;
    List CLOSED := <>;
    enqueue(OPEN, start);
    while not istEmpty(OPEN) do
      n := extractMinimal(OPEN); //Knoten mit kleinstem f-Wert
      if n = ziel then
        return WegGefunden;
      fi;
      expand(n);
      enqueue(CLOSED, n);
    od;
    return WegNichtGefunden;

    /*
      Überprüft alle Nachfolgeknoten und fügt sie der Open List hinzu, wen entweder
      der Nachfolgeknoten zum ersten Mal gefunden wird oder ein besserer Weg zu
      diesem Knoten gefunden wird.
    */

    procedure expand (Knoten u in V[G])
    begin
      for each v in Successor(u) and v not in CLOSED do
        tf := g(u) + gamma(u,v) + h(v);
        if v in OPEN and tf >= f(v) then
          continue;
        fi;
        v.pred := u;
        Aktualisiere f- und g-Werte von v;
        if v not in OPEN then
          enqueue(OPEN, v);
        fi;
      od;
    end


Der Algorithmus strebt der Heuristik entsprechend Richtung Ziel.

- A* ist vollständig (existiert eine Lösung, so wird sie gefunden)
- A* ist findet optimale Lösung (bei Einsatz konsistenter Heuristik)
- A* ist optimal effizient (Es existiert kein Algorithmus, der bei gleicher Heuristik
  die Lösung schneller findet.)

Dijkstra ist ein Spezialfall von A* ohne Heuristik (:math:`h=0; f=g`)
Dijkstra und A* funktionieren nur bei positiven Kantengewichten.
Auch der A* ist wie der :ref:`dijkstra-algorithm` ein :ref:`greedy-algorithm`

Bellman-Ford Algorithmus
========================

Iterative Untersuchung. Bei jedem Iterationsschritt wird die Länge der Pfade um 1
erhöht. Der :math:`i`-te Durchlauf des Algorithmus berechnet korrekt alle Pfade vom Startknoten
der Länge :math:`i`.

Problem: Zyklen mit negativem Gesamtgewicht: Hier landet der Algorithmus in einer
Endlosschleife

Der längste Pfad ohne Zyklus hat eine Länge von :math:`|E| -1`, sodass man diesen
Prozess nach :math:`|E| -1` Durchläufen abschließen kann.

In jedem Schritt werden alle Kanten angeschaut. Dabei wird immer der aktuelle
Wert der Knoten verwendet, der zu Begin des Schrittes eingetragen war und nicht
der Knotenwert der im selben Schritt aktualisiert wurde.

.. code::

  algorithm BF (G,s)
    Eingabe: ein Graph G mit einem Startknoten s
    Sei D[x] wieder mit infinite initialisiert

    D[s] := 0
    for i:= 1 to |E| -1 do
      for each (u, v) in E do //gerichtete Kanten
        if D[u] + edgeWeight((u,v)) < D[v] then
          D[v] := D[u] + edgeWeight((u,v));
        fi;
      od
    od

.. figure:: bellman_ford_algorithm_with_cycle_recognition.png
	 :alt: bellman_ford_algorithm_with_cycle_recognition

Bei einem Graphen mit einem negativen Zyklus erzeigt jeder Durchlauf durch den
Zyklus Gewinn, es gibt also keinen günstigsten Pfad endlicher Länge.
