.. _divide-and-conquer:

==================
Divide-and-Conquer
==================

Das Problem wird in mehrere getrennte Teilprobleme aufgeteilt.

Diese Teilprobleme sollten strukturell gleich sein wie das Startproblem. Dadurch
lässt sich der Algorithmus sehr gut rekursiv lösen.

Die Rekursion endet, wenn die Probleme so klein sind, das die Lösung trivial ist.

Beispiele sind die :ref:`binary-search` und der :ref:`merge-sort`.

Nach dem Divide-and-Conquer Verfahren lässt sich die Komplexität der
Matrizen Multiplikation verringern.

.. figure:: matrizen_multiplikation.png
	 :alt: matrizen_multiplikation

.. figure:: matrizen_multiplikation_rechentrick.png
	 :alt: matrizen_multiplikation_rechentrick
