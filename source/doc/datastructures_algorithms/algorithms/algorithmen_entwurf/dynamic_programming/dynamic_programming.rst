=========================
Dynamische Programmierung
=========================

Bei der **dynamischen Programmierung** werden abhängige Teilprobleme gelöst, wobei
mehrfach auftretende Teilprobleme nur einmal berechnet werden.

Es bietet sich daher an eine *Bottom-up* Realisierung der Backtracking-Strategie.

Bei der dynamischen Programmierung werden kleinere Teilprobleme zuerst gelöst, um
aus diesen größere Teillösungen zusammenzusetzen.

Bei der Levenshtein Distanz werden Abstände von Teilwörtern bestimmt und die
partiellen Lösung wird weiterverwendet. Daher handelt es sich bei der Levenshtein
Distanz um ein Beispiel der Dynamischen Programmierung.

Nicht immer ist es möglich, die Lösungen kleiner Probleme so zu kombinieren, dass
sich die Lösung eines größeren Problems ergibt.
