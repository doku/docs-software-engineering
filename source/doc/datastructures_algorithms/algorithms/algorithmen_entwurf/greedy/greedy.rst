.. _greedy-algorithm:

==================
Greedy-Algorithmus
==================
Greedy-Algorithmen (gierige Algorithmen) zeichnen sich dadurch aus, dass sie immer
denjenigen Folgezustand auswählen, der zum Zeitpunkt der Wahl den größten Gewinn
bzw. das beste Ergebnis verspricht. Der Folgezustand wird durch eine
Bewertungsfunktion bestimmt.

Zum Beispiel Geldwechselproblem: Bestimmung des Rückgelds mit so wenigen Münzen wie
möglich. In diesem Beispiel liefert das Greedy Verfahren mit normalen Münzen
das ideale Ergebnis. Wählt man das 11, 5, 1 als ct, so liefert Greedy ``11+1+1+1+1``
obwohl das Optimum ``5+5+5`` wäre.

Greedy-Algorithmen berechnen in jedem Schritt das **lokale Optimum**, dadurch
kann das globale Optimum verfehlt werden. In vielen Fällen entsprechen lokale
Optima den globalen oder es reicht ein lokales Optimum aus.

Auch der :ref:`dijkstra-algorithm` ist greedy.

Das np-Harte Rucksachproblem lässt sich durch greedy-Verfahren approximativ lösen
