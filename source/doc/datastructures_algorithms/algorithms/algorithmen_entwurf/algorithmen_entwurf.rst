===================
Algorithmen-Entwurf
===================

Der Algorithmenentwurf ist eine kreative Tätigkeit. Dabei sollte man sich an
*best practices* orientieren.

Man sollte möglichst lange abstrakte Notation verwenden, so abstrakt wie möglich
und so detailliert wie nötig.

Oft bietet es sich an bewährte **Algorithmenmuster** zu verwenden.
Diese generischen Algorithmenmuster müssen dann für die bestimmte
Problemklassen an die konkrete Aufgabe angepasst werden.

Verfahren
=========

.. toctree::
   :maxdepth: 2
   :glob:

	 backtracking/*
	 devide_and_conquer/*
	 greedy/*
	 dynamic_programming/*

Inkrementelles Vorgehen
=======================
Schrittweise bearbeiten eines zu lösenden Problems in kleinen Einheiten.

Beispiel: :ref:`insertion-sort`


Scanline- und Sweep-Verfahren
=============================

Schrittweises Vorgehen entlang einer strukturierten Richtung
