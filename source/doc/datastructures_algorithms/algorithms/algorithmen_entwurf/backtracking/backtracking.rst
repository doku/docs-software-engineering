============
Backtracking
============

Backtracking ist ein Verfahren des **systematische triel and error** und daher
eine **systematische Suchtechnik**.

Es werden systematisch alle möglichen Lösungen betrachtet, dadurch kann nichts
übersehen werden. Die Lösung wird entweder gefunden (unter Umständen nach sehr
langer Laufzeit) oder es existiert definitiv keine Lösung.

.. note::
	Teillösungen werden schrittweise zu einer Gesamtlösung ausgebaut. Falls Teillösungen
	nicht zu einer Lösung führen, werden die letzten Schritte rückgängig gemacht
	und ein alternativer Weg probiert.

.. figure:: backtracking_pseudo_code.png
	 :alt: backtracking_pseudo_code

Zu dem Problem muss eine Graphenrepräsentation gefunden werden.

.. figure:: backtracking_graph_represenation.png
	 :alt: backtracking_graph_represenation

Eine *Terminierung* ist nur dann gegeben, wenn des Lösungsraum endlich ist und es
kein wiederholtes Betreten einer bereits getesteten Konfiguration gibt.

Auch die Tiefensuche ist ein Backtracking Algorithmus.

Eine rekursive Implementierung ist hier sehr natürlich.

Die Laufzeit is meist exponentiell. Bei maximal :math:`k` möglichen Verzweigungen
in jeder Teillösung und der Tiefe :math:`n` des Baumes erhält man die Laufzeit von
:math:`O(k^n)`.

Beim **Branch-and-Bound** werden nur diejenigen Zweige verfolgt, die prinzipiell
eine Lösung zulassen. Hier werden Sackgassen frühzeitig erkannt und weggelassen.
Konfigurationsbäume, die unmöglich die Lösung erreichen können werden abgeschnitten.

Für das Rucksackproblem liefert Backtracking die ideale Lösung.

.. figure:: backtracking_rucksackproblem_beispiel.png
	 :alt: backtracking_rucksackproblem_beispiel
