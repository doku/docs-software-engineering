===========
Algorithmen
===========

.. glossary::

  Algorithmus
    Ein Algorithmus ist eine präzise endliche Beschreibung eines allgemeinen Verfahrens
    unter Verwendung ausführbarer elementarer Verarbeitungsschritten.

Struktogramme (:ref:`ref-struktogramm`) bieten eine standardisierte graphische
Notation zum Entwurf von Algorithmen.
