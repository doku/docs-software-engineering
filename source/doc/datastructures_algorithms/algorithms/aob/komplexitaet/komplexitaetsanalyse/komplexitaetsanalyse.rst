===================
Komplexitätsanalyse
===================

Adversary-Argument
==================

Man kann mittels einem AdversaryArgument
(also einem Gegenspielerargument) untere Schranken für Worstcase-
Laufzeiten, wie folgt, argumentieren: Sei ein Algorithmus gegeben. Um eine Ausgabe
in Bezug auf die Eingabe zu erzeugen, wird sich der Algorithmus die Eingabe
beziehungsweise Teile davon irgendwann einmal angucken. Das können wir uns als
eine Folge von Suchanfrage an die Eingabe vorstellen. Wir sind nun ein Gegenspieler
(Adversary) und versuchen diese Suchanfragen zwar korrekt zu beantworten, aber
so ungeschickt, dass der Algorithmus möglichst lange benötigt, um sicherzugehen,
dass seine Ausgabe korrekt ist. Man kann das also als Spiel zwischen Adversary
und Algorithmus betrachten, bei dem der Algorithmus möglichst wenig Anfragen
benötigen möchte, der Adversary diese Zahl allerdings hochtreiben will.
Um fur einen gegebenen Algorithmus eine Laufzeitschranke von :math:`\Omega(t)` zu argumentieren,
konstruieren wir Antworten auf Anfragen dieses Algorithmus2 und zeigen,
dass er frühestens nach :math:`\Omega(t)` Schritten eine korrekte Antwort geben kann. Davor
gibt es mehrere Möglichkeiten dafür, welche Lösung tatsächlich korrekt ist.
Betrachte das folgende Problem: Gegeben sind n Politiker in einem Array A.
Du darfst für zwei gegebene Indizes i und j fragen, ob A[i] und A[j] zum Thema
Bildungspolitik die gleiche Meinung haben. Eine solche Anfrage wird in konstanter
Zeit beantwortet. Keine anderen Anfragen sind erlaubt. Gefragt ist, ob es zwei
(verschiedene) Politiker im Array gibt, die die gleiche Meinung zu dem Thema
haben.
Zeige mit einem Adversary-Argument, dass jeder Algorithmus zur korrekten
Beantwortung der Frage im Worstcase :math:`\Omega(n^2)` Anfragen benötigt. Gib außerdem
noch einen Algorithmus an, der mit :math:`\Omega(n^2)` Anfragen auskommt (womit dann
sowohl die untere Schranke als auch der Algorithmus asymptotisch bestmöglich
sind). Vergiss im zweiten Fall nicht, die Korrektheit und die Laufzeit zu beweisen.
