=============================
Datastructures and Algorithms
=============================

Zusammenfassungen und Vorlesungsnachbereitung zur Vorlesung "Datenstrukturen und
Algorithmen" der Universität Stuttgart.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   datastructures/index
   algorithms/index
   uebung/index
