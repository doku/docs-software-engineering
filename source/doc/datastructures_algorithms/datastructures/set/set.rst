===
Set
===

Sets sind ungeordnete/geordnete Datenstrukturen ohne Duplikate.

Beim Hinzufügen eines Elements ``add``, der Vereinigung vom Mengen ``unite`` und
Bilden der Schnittmenge ``intersect`` muss sichergestellt werden, dass keine
Duplikate auftreten. Das kann durch einen binären Suchbaum effizient umgesetzt
werden.
