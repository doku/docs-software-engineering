======
Listen
======

Listen sind lineare Anordnungen von Daten.

Mit Listen können abstrakte Datentypen, wie Stack und Queue umgesetzt werden.

Listen bestehen aus Knoten, Zeiger und Head und Tail.

Der Head bietet einen Einstieg in die Listenstruktur. Löscht man den Head
geht die Referenz auf die Liste verloren, da kein Einstiegspunkt mehr gibt.

Einfügen
	1. Erstellen des neuen Knoten
	2. Zeiger umlegen

Löschen an beliebiger Stelle
	1. Zeiger auf das Element nach dem zu löschende Element speichern
	2. Zeiger nun umlegen.

Einfach verkettete Liste
========================

Einfach verkettete Listen haben nur Pfeile vom Head aus in Richtung Tail.

.. figure:: einfach_verkettete_liste.png
	 :alt: einfach_verkettete_liste

Jeder Knoten hat zwei Einträge, den Wert des Knotens selbst und einen Zeiger auf
den nächsten Knoten.

Doppelt verkettete Liste
========================

Eine einfache Verkettung erlaubt nur Vorwärtsnavigation. Die zusätzlichen "Rückwärtszeiger"

.. figure:: doppelt_verkettete_liste_example.png
    :scale: 60%
    :align: center
    :alt: doppelt_verkettete_liste_example

Array Liste
===========

Besteht im Hintergrund aus einem Array. Die Arrayplätze entsprechen den Knoten.
Dadurch gibt es wahlfreiem Zugriff auf die Elemente.

Beim Einfügen und Löschen von Elementen müssen immer die anderen
Elemente verschoben werden.

Stack
=====
Einfache Datenstruktur mit beschränktem Zugriff nach dem *LIFO-Prinzip*
(Last-In-First-Out-Speicher).

.. figure:: stack_example.png
   :scale: 60 %
   :align: center
   :alt: stack_example


- ``push``: legt das Objekt als oberstes Element auf dem Stack ab
- ``pop``: nimmt das oberste Element vom Stack und gibt es zurück
- ``top`` bzw. ``peek``: gibt das oberste Element des Stack zurück, ohne es zu
  entfernen.
- ``isEmpty`` liefert ``true``, wenn keine Elemente auf dem Stack liegen,
  andernfalls ``false``
- ``Empty`` leeren der Liste

Es bietet sich an eine Linked List zu verwenden.

Queue
=====
Einfache Datenstruktur mit beschränktem Zugriff nach dem *FIFO-Prinzip*
(First-In-First-Out-Speicher).

.. figure:: queue_example.png
   :scale: 60%
   :align: center
   :alt: queue_example

Aufträge werden in Eingangsreihenfolge abgearbeitet.

Es gibt klassisch nur 3 Methoden:

- ``enter`` bzw. ``enqueue``: fügt das Objekt am Ende der Schlange zur Warteschlange hinzu
- ``leave`` bzw. ``dequeue``: nimmt das "älteste" Element aus der Warteschlange
  heraus und gibt es zurück
- ``front`` bzw. ``peak``: liefert das "älteste" Element aus der Warteschlange, ohne es zu entfernen.

Zudem gitb es meist:

- ``isEmpty`` liefert ``true``, wenn keine Elemente auf dem Queue liegen, andernfalls ``false``

Am besten verwendet man eine Linked-List mit Tail-Referenz oder als Ringpuffer mit
Array.

Iterator
========
Der Iterator durchwandert die Collection.

Ein Iterator ist eine spezielle Datenstruktur, die über alle Elemente der Liste
iterieren kann, aber nicht immer zum ersten Element zurückspringen muss.

Implementierungsunabhängiges und einheitliches durchwandern von Datenstrukturen.

Ein Iterator bietet 3 Methoden:

- ``hasNext()`` prüft ob noch weitere Elemente in der Datenstruktur verfügbar sind.
- ``next()`` liefert das aktuelle Element zurück und setzt den internen Zeiger des
	Iterators auf das nächste Element.
- ``remove()`` ermöglicht es, das zuletzt von ``next()`` gelieferte Element zu löschen

In einer ``foreach`` Schleife kann man keine Elemente aus der Liste löschen. Nur
unter Verwendung des normalen Iterators ist bei manschen Datenstrukturen das löschen
möglich.

Mit einem Methodenaufruf in der jeweiligen Collection-Klasse lässt sich der
richtige Iterator erzeugen:

.. code-block:: java

	class LinkedList {
		...
		public Iterator iteratro(){
			return new ListIterator();
		}
	}

	// Nutzung des Iterators

	List users = new LinkedList();
	...
	Iterator userIterator = users.iterator();
	while (userIterator.hasNext()) {
		User user = userIterator.next();
		...
	}

Operationen auf Listen ohne Iterator kann Probleme geben:

.. code-block:: java

	ArrayList list = new ArrayList();

	for (int = 0; i < list.size(); i++) {
		if(list.get(i)<10) {
			list.remove(i);
			--i; //index zurücksezten da Element gelöscht wurde
		}
	}

Daher sollte besser der Iterator verwendet werden.

Der Java-Iterator auf dem Stack gibt die Elemente von unten nach oben aus.
