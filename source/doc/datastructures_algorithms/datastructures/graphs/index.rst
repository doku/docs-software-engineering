=======
Graphen
=======

.. toctree::
  :maxdepth: 2
  :glob:

  graphs/*
  graph_definitions/*
  graphen_darstellungen/*
  kreise_wege/*
  minimaler_spannbaum/*
  triangulierung/*
  graphen_visualisierung/*
