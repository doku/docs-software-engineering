==============
Triangulierung
==============

Um allgemeine Oberflächen darstellen zu können, werden diese als Punktmenge
dargestellt und die benachbarten Punkte werden zu Flächen verbunden.

Bei der Triangulierung (triangulation) wird ein Netz von Dreiecken im Raum verwendet.

.. figure:: triangulierung3d_example.png
	 :alt: triangulierung3d_example
	 :scale: 60%

Dieses Dreiecksnetz lässt sich als Graph modellieren.

Definition
==========

Eine **Triangulierung** von Datenpunkten :math:`P = \{p_o, ... , p_n\}` aus
:math:`\mathbb{R}^2` besteht aus einem Knoten an Position :math:`P` und Kanten,
die je zwei Knoten verbinden. Je drei adjazente Knoten bilden ein Dreieck.

.. figure:: 2d_triangulierung.png
   :scale: 40%
   :alt: 2d_triangulierung

Die Vereinigung aller Dreiecke entspricht der konvexen Hülle aller Datenpunkte
:math:`P`.

Der Schnitt zweier Dreiecke ist entweder leer, ein gemeinsamer Knoten oder eine
gemeinsame Kante.

.. figure:: triangulierungen_ungueltig.png
	 :alt: triangulierungen_ungueltig

	 Ungültige Triangulierungen

Aus einer Menge an 2D Positionen soll eine Triangulierung berechnet werden.
Dafür existieren viele verschiedene Lösungen.

Wir definieren daher eine Qualität der Triangulierung. Hierbei sollen lange,
dünne Dreiecke vermieden werden. Die Dreiecke sollen möglichst gleichseitig sein.

Delaunay-Triangulierung
=======================

Bei der Delaunay-Triangulierung wird ein Voronoi-Diagramm für die Punktmenge :math:`P`
erstellt. Man verbindet dann die Mittelpunkte benachbarter Voronoi Zellen zu einer
Delaunay Triangulierung.

.. figure:: voronoi_to_delaunay.png
	 :alt: voronoi_to_delaunay

Diese Triangulierung ist eindeutig (unter bestimmten Annahmen) und vermeidet
lange dünne Dreiecke.

Voronoi Diagramm
----------------

Jedem Punkt :math:`p_i` aus :math:`P` wird eine **Voronoi-Region** :math:`Vor(p_i)`
zugewiesen, für die gilt: Jeder Punkt innerhalb der Voronoi Region liegt näher
zu :math:`P_i` als zu jedem anderen Punkt aus :math:`P`.

.. math:: Vor(p_i) = \{ p | dist(p, p_i) <dist(p, p_j) \forall j \neq i\}

.. figure:: voronoi_diagramm.png
	 :alt: voronoi_diagramm
	 :scale: 40%

Hat ein Punkt zu mehr als drei Voronoi Punkten die exakt gleiche Distanz, spricht
man von einem Degenerierten Diagramm. Diese Punkte werden dann minimal um Zufallswerte
:math:`\epsilon << 1` verschoben.

.. figure:: degeneriertes_voronoi_diagramm_korrektur.png
	 :alt: degeneriertes_voronoi_diagramm_korrektur

Delaunay Graph
--------------

Der **Delaunay-Graph** :math:`Del(P)` ist der **duale Graph** eines Voronoi-Diagramms
:math:`Vor(P)`. Die Datenpunkte :math:`P` in den Voronoi Regionen bilden die Knoten.
Zwei Knoten :math:`p_i` und :math:`p_j` sind genau dann verbunden, wenn die
Voronoi-Regionen :math:`Vor(p_i)` und :math:`Vor(p_j)` eine Kante teilen.

Ist das Voronoi-Diagramm nicht degeneriert, so sind die Delaunay Zellen (die
Flächen zwischen dien Kanten) Dreiecke. Der Delaunay Graph ist dann eindeutig
und stellt eine Triangulierung dar.

Drei Datenpunkte :math:`p_i, p_j, p_k` aus :math:`P` bilden ein Dreieck in
:math:`Del(X)` genau dann, wenn keine weiteren Datenpunkte aus P im Kreis
um :math:`p_i, p_j, p_k` liegen.

.. figure:: delaunay_eigenschaft_example.png
	 :alt: delaunay_eigenschaft_example

.. hint::

	Bei der Delaunay Triangulierung enthält der Umkreis jedes Dreiecks der Triangulierung
	keine anderen Datenpunkte.

Zwei Datenpunkte :math:`p_i, p_j` aus :math:`P` bilden eine Kante genau dann,
wenn es einen Kreis um :math:`p_i, p_j` gibt, der keinen dritten Datenpunkt
aus :math:`P` enthält.

Dadurch maximiert die Delaunay Triangulierung den kleinsten Winkel aller Dreiecke
der Triangulierung und vermeidet lange, dünne Dreiecke.

.. glossary::

	lokale Delaunay Eigenschaft
		Eine Kante zwischen zwei Datenpunkten :math:`p_i, p_j` erfüllt die lokale Delaunay
		Eigenschaft genau dann wenn sie zu nur einem Dreieck gehört oder sie zu
		Dreiecken :math:`p_i p_j p_a` und :math:`p_i p_j p_b` gehört und :math:`p_b`
		außerhalb des Umkreises von :math:`p_i p_j p_a` liegt und :math:`p_a` außerhalb
		des Umkreises von :math:`p_i p_j p_b` liegt

		.. figure:: delaunay_eigenschaft.png
			 :alt: delaunay_eigenschaft

		Eine Triangulierung ist genau dann eine Delaunay-Triangulierung, wenn alle Kanten
		die lokale Delaunay Eigenschaft erfüllen.

Verletzt eine Kante die lokale Delaunay Eigenschaft, so kann diese durch Kantendrehung
wieder hergestellt werden.

.. figure:: delaunay_edge_flip.png
	 :alt: delaunay_edge_flip

Die Kanten der involvierten Dreiecke könne die lokale Delaunay Eigenschaft in Folge
des Edge-Flips verletzten. Es sind dann weitere Drehungen nötig, bis alle Kanten die
lokale Delaunay-Eigenschaft erfüllen.

.. figure:: delaunay_edge_flip_sideeffects.png
	 :alt: delaunay_edge_flip_sideeffects

Mit dem Edge-Flip Algorithmus lässt sich aus einer beliebigen Triangulierung eine
Delaunay Triangulierung herstellen.

.. figure:: edge_flip_algorithm.png
	 :alt: edge_flip_algorithm

Plane-Sweep-Algorithmus
=======================

Der Plane-Sweep-Algorithmus erzeugt eine initiale Triangulierung, welche dann
mittels Edge-Flip in eine Delaunay Triangulierung gewandelt werden kann.

Eine imaginäre vertikale Linie (sweep line) wird von links nach rechts bewegt.

Invariante: Links der Plane-Sweep Line ist die Punktmenge bereits Trianguliert

.. figure:: plane_sweep_algo.png
	 :alt: plane_sweep_algo


.. figure:: siluette_add_point.png
	 :alt: siluette_add_point

Bowyer-Watson-Algorithmus
=========================

Mittels des Bowyer-Watson-Algorithmus lässt sich inkrementell eine Delaunay Triangulierung
erzeugen.

Die Annahme ist, dass :math:`p_1, ..., p_{n-1}` bereits in Delaunay-Triangulierung sind
und :math:`p_n` liegt innerhalb von dieser Triangulierung.
Man möchte nun :math:`p_n` hinzufügen und die Delaunay Triangulierung aktualisieren.

#. Bestimme die Dreiecke, deren Umkreis :math:`p_n` enthält.
#. Lösche alle Kanten, die in zwei der gefunden Dreiecke vorkommen (innere Kanten)
#. Trianguliere das entstandene Loch durch sternförmiges verbinden aller Eckpunkte
	 des Lochs mit :math:`p_n`

.. figure:: bowyer_watson_incremental_triangulierung.png
	 :alt: bowyer_watson_incremental_triangulierung
	 :scale: 70%

.. figure:: bowyer_watson_algorithm.png
	 :alt: bowyer_watson_algorithm

Fortunes Algorithmus
====================
