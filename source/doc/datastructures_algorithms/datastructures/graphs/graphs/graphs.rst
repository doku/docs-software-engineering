=======
Graphen
=======

Graphen sind ein Netzwerk aus Knoten und Kanten. Listen und Bäume sind
Spezialfälle von Graphen.

Es gibt ungerichtete und gerichtete Graphen. Die Kanten können zudem gewichtet
sein.

Ungerichteter Graph
===================
Ein Graph ist ein Zwei-Tupel :math:`G = (V, E)`.
Mit einer endlichem Menge :math:`V` von Knoten (vertices) und einer
Menge :math:`E` von Kanten. :math:`e \in E` ist eine zweielementige Teilelemente
der Knotenmenge :math:`V`. Die Kanten sind ungerichtet, denn für 2 Mengen
gilt :math:`\{a, b\} = \{b, a\}`

.. figure:: graph_beispiel.png
	 :alt: graph_beispiel

In ungerichteten Graphen können 2 Knoten nur durch maximal eine Kante verbunden
werden.

Ein Teilgraph (Subgraph) ist definiert durch Teilmengen von :math:`V` unf :math:`E`.

Ein gerichteter Graph ist **symmetrisch**, wenn zu jeder Kante :math:`(i, j) \in E`
auch die entgegengesetzte Kante :math:`(j, i) \in E` existiert.

Eine Folge von Knoten :math:`v_1, v_2, ... , v_n`, die durch Kanten verbunden sind,
(also :math:`(v_i, v_{i+1}) \in E`) heißt **Pfad**.

Ein Zyklus ist ein Pfad, bei dem Start- und Endknoten gleich sind.

Ein gerichteter Graph heißt zyklisch, wenn er einen Zyklus enthält.


Gerichteter ungewichteter Graph
-------------------------------

Es sind mehrere *EntryPoints* möglich.

.. code-block:: java

  public class DynamicGraph implements Graph {
    public class GraphNode implements Comparable<GraphNode> {
      int key;
      Set<GraphNode> edges = null;

      GraphNode(int k) {
        this.key = k;
        edges = new TreeSet<GraphNode>();
      }
    }

    Set<GraphNode> t = null;
  }


Knoten einfügen
~~~~~~~~~~~~~~~

Wird ein neuer Knoten erstell muss dieser Zwangsläufig ein entryPoint sein,
da dieser noch keine eingehenden Kanten hat.

.. code-block:: java

  public int addNode() {
    entryPoints.add(new GraphNode(++maxKey));
    return maxKey;
  }

Kante einfügen
~~~~~~~~~~~~~~

Zuerst muss Anfangs und Zielknoten gesucht werden. Es kann dann die Kante
gezogen werden. Da der Zielknoten nun indirekt erreichbar ist, kann er aus
``entryPoints`` entfernt werden.

Vorsicht bei Zyklen: kann der Startknoten nicht mehr gefunden werden nachdem der
Zielknoten aus entryPoints entfernt worden ist, muss dieser zu den entryPoints
hinzugefügt werden.


Planare Graphen
===============

Eulerformel

  In endlichen, zusammenhängenden, planaren Graphen mit :math:`n \geq 1` Knoten
  :math:`m` Kanten und :math:`f` Facetten gilt:

  .. math:: n - m + f = 2


Traversierung von Graphen
=========================
Die Traversierung von Graphen ist eine Verallgemeinerung der Traversierung von
Bäumen (:ref:`tree_traversal`).

.. _maximaler-fluss:

Maximaler Fluss
===============

Korrekter Fluss
---------------

Ein Durchfluss :math:`F` legt für jede Kante einen aktuellen Fluss fest, der in
einem korrekten Fluss unter der maximalen Kapazität liegt, In einem Fluss kennen
wir für jede Kante den Wert :math:`c` der maximalen Kapazität und den Wert :math:`f`
des aktuellen Flusses. So lässt sich mit :math:`c - |f|` auch die verbleibende/verfügbare
Kapazität bestimmen.

Ein Durchfluss muss für Korrektheit folgende Eigenschaften erfüllen:

#. Einschränkungen der Kapazität der Kanten werden eingehalten (auch bei
	 negativem Fluss): :math:`|f(u, v)| \leq c((u,v))`
#. Konsistenz des Flusses: :math:`f(u,v) = -f(v,u) \forall (u,v) \in E` wobei
	 :math:`(v,u)` nicht zwangsläufig :math:`\in E`
#. Erhaltung des Flusses: Für jeden Knoten :math:`v \in V` außer Quelle :math:`s`
	 und Ziel :math:`t` gilt: :math:`\sum_{u \in V} f(v, u) = 0`

Rückkapazität wird auf Kanten in entgegengesetzter Richtung aufaddiert.

Die austretende Fluss des Startknoten muss mit dem eintretenden Fluss im Zielknoten
übereinstimmen. (sonst gab es einen Rechenfehler)


Ford Fulkerson
--------------

Der Ford-Fulkerson-Algorithmus ist ein Verfahren zur Berechnung des maximalen Flusses.

Man fügt solange wie möglich verfügbare Pfade zum Gesamtfluss hinzu.

Einzeichnen von Rückflusspfeilen mit minimalem Fluss des Pfades.

Die Vorgehensweise des Ford Fulkerson-Algorithmus ist eine Mischung aus Greedy und
Zufallsauswahl.

Aufgrund der nicht festgelegten Auswahlstrategie der Pfade ist der Ford Fulkerson
formal eigentlich kein Algorithmus.

Minimaler Schnitt
-----------------

Beim **s-t-Schnitt** werden die Knoten *senkrecht* um Fluss in zwei disjunkte
Teilmenten :math:`S` und :math:`T` aufgeteilt.

.. figure:: s_t_schnitt_example_1.png
	 :alt: s_t_schnitt_example_1

Die **Kapazität** eines s-t-Schnittes :math:`(S, T)` ist die summe aller
Kantenkapazitäten von :math:`S` nach :math:`T`

.. math:: c(S, T) = \sum_{u \in S, v \in T | (u, v) \in E} c(u,v)

.. glossary::

	Max-Flow/Min-Cut-Theorem
		Der maximale Fluss im Netzwerk ist identisch mit der Kapazität des minimalen
		Schnitts.
