=====================
Graphen Darstellungen
=====================

Kantenliste
===========

Ein Graph lässt sich als Kantenliste repräsentieren:

.. math::

  v, e, x_1, y_1, ..., x_n, y_n

Hierbei sind :math:`v` die Knotenanzahl, :math:`e` die Kantenanzahl und :math:`x_i, y_i`
Knotenpaare die eine Kante repräsentieren.

.. figure:: kanten_liste_example.png
	 :alt: kanten_liste_example

Bei gerichteten Graphen müssen Hin- und Rückkanten eingefügt werden.

Der Graph wird beschrieben als Knoten- und Kantenanzahl gefolgt von Knotentupeln
für jede Kante.

Die Knotenanzahl wird benötigt um Knoten ohne Kanten zu erkennen. Die Kantenanzahl
ist redundant, da sie dem Betrag der Tupelliste entspricht.

Knotenliste
===========

Ein Graph lässt sich als Kantenliste repräsentieren:

- Zahl der Knoten
- Zahl der Kanten
- Alle Knoten durchgehen und die Anzahl der ausgehenden Kanten gefolgt von den
  Zielknoten angeben.

.. figure:: knotenliste_example.png
	 :alt: knotenliste_example

Eine Knoten ohne Kanten wird durch eine 0 in der Knotenliste repräsentiert.

Bei einem ungerichteten Graph wird eine ausgehende Kante nur dann gezählt, wenn
sie nicht bereits in der Liste eingetragen wurde.

.. figure:: knotenliste_ungerichtet_example.png
	 :alt: knotenliste_ungerichtet_example

Man weiß nicht wie viele ausgehende Kanten der Knoten hat.

Adjazenzmatrix
==============

Zwei Knoten :math:`x, y` heißen **adjazent**, wenn gilt :math:`\{x,y\} \in E`


.. figure:: adjazentmatrix_ungewichtet.png
	 :alt: adjazentmatrix_ungewichtet

Trägt man an den Zeilen und Spalten die Knoten ein, so liest sich die Matrix
wie folgt: Vom Zeilenknoten geht eine Kante zum Spaltenknoten.

.. figure:: adjazentmatrix_gewichtet.png
	 :alt: adjazentmatrix_gewichtet

Adjazenzmatrizen sind für ungerichtete und symmetrische (gerichtete) Graphen
symmetrisch. Es genügt daher nur die obere oder untere Dreiecksmatrix zu speichern.

.. figure:: adjazenzmatrix_symmetrisch.png
	 :alt: adjazenzmatrix_symmetrisch

Bei Graphen ohne Schleifen (azyklisch) kann die Diagonale der Matrix weggelassen
werden, da hier dann nur Nullen auf der Diagonale stehen.

Möchte man Knoten hinzufügen, so wird die Adjazenzmatrix jeweils um eine
zusätzliche leere Spalte und Zeile vergrößert.

Um eine neue Kante einzufügen oder zu läschen muss man lediglich die richtigen Einträge
der Adjazenzmatrix anpassen, Komplexität :math:`O(1)`.

In der Matrix lassen sich sehr schnell neue Kanten einfügen.

Es bietet sich besonders bei sehr dichten Graphen an eine Matrix zu verwenden.

Adjazenzliste
=============

Eine Adjazenzliste ist die Repräsentation des Graphen als eine Art verkettete
Liste.

.. figure:: adjazentliste_example.png
	 :alt: adjazentliste_example

Die Adjazenzlisten sind besonders bei dünnen Graphen (wenig Kanten bei vielen
Knoten) von Vorteil gegenüber den Matrizen, da nicht so viel Speicherplatz
verschwendet wird.

Komplexität Kante einfügen/löschen: :math:`O(n)`

Übersicht
=========

.. figure:: operationen_komplexitaet_uebersicht.png
	 :alt: operationen_komplexitaet_uebersicht

Das löschen eines Knotens erfordert das Löschen aller zugehörigen Kanten.
