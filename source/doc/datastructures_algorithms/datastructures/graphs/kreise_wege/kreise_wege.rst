===============
Kreise und Wege
===============


Eulerscher Kreis
================

Ein Pfad :math:`(u_0, u_1, ..., u_k)` heißt **Eulerscher Weg**, wenn jede Kante
des Graphen genau einmal in seinem Pfad vorkommt. Er heißt **Euler'scher Kreis**,
wenn zusätzlich gilt :math:`u_k = u_0`

Ein (ungerichteter zusammenhängender) Graph enthält genau dann einen Eulerweg,
wenn zwei oder keiner seiner Knoten von ungeradem Grad sind; hat kein Knoten
ungeraden Grad, handelt es sich bei dem Eulerweg um einen Eulerkreis.

Ein zusammenhängender ungerichteter Graph besitzt genau dann einen Eulerschen
Kreis, wenn alle seine Knoten einen geraden Grad haben. Dies gilt auch für Graphen
mit Mehrfachkanten.

Für den Eulerpfad sollte man an einem Knoten mit ungeradem Grad anfangen und am
anderen enden.

Das Königsberger Brückenproblem beschäftigt sich mit der Frage, ob es möglich ist
einen Rundgang durch Königsberg zu machen und jede der sieben Brücken genau einmal
zu besuchen.
Das Königsberger Brückenproblem lässt sich auf die Suche nach einem Eulerkreis im
Graph herunterbrechen.

.. figure:: koeningsberger_brueckengraph.png
	 :alt: koeningsberger_brueckengraph

Der Königsberger Brückengraph ist *nicht* Euler'sch, da die vier Knoten alle einen
ungeraden Grad haben.

.. _hamilton-kreis:

Hamiltonkreis
=============

Beim Hamiltonweg muss jeder Knoten genau einmal besucht werden. Ein Hamiltonweg
ist ein Hamiltonkreis wenn der Startknoten gleich dem Endknoten ist.

Ein Pfad :math:`(u_0, u_1, ..., u_n-1)` in einem Graphen :math:`G` mit :math:`n`
Knoten heißt **Hamilton'scher Weg**, wenn jeden Knoten genau einmal enthält.

Ein Pfad :math:`(u_0, u_1, ..., u_n-1, u_0)` heißt **Hamilton'scher Kreis**, wenn
:math:`(u_0, u_1, ..., u_n-1)` ein Hamilton'scher Weg ist.

Das Finden von Hamilton'schen Kreisen ist NP-vollständig.
