==================
Graph Definitionen
==================

.. glossary::

  gerichteter, ungewichteter Graph
    :math:`G = (V, E)`

  gewichteter Graph
    :math:`G = (V, E, \gamma)` mit :math:`\gamma: E \rightarrow \mathbb{N}`,
    wobei :math:`\gamma` die Kantengewichte als natürliche Zahlen auffasst.


  Standard Graphen

    .. figure:: graph_kn_cn_pn.png
      :alt: graph_kn_cn_pn
      :scale: 70%

  Ausgangsgrad
    Maximale Anzahl der Ausgangskanten an einem Knoten

  Eingangsgrad
    Maximale Anzahl an Eingangskanten in einem Knoten

  Grad
    Maximale Anzahl aus Eingangskanten und Ausgangskanten in einem Knoten

  Pfad
    Ein Pfad durch eine Graph :math:`G` ist eine Liste von aneinander anschließenden
    Kanten:

    .. math:: P = \bigl( (v_1, v_2), (v_2, v_3), (v_3, v_4), ..., (v_{n-1}, v_n) \bigr)

  Gewicht eines Pfades
    Das Gewicht eines Pfades (:term:`Pfad`) wird beschrieben als Aufsummierung der einzelnen
    Kantengewichte.

    .. math:: w(P) = \sum_{i=0}^{n-1} \gamma((v_i, v_{i+1}))

  Distanz
    Die Distanz zweier Punkte :math:`d(u,v)` ist das Gesamtgewicht, des kürzesten
    Pfades on :math:`u` nach :math:`v`

  Planare Graphen
    Ein Graph ist **planar** wenn er sich ohne Kantenkreuzungen darstellen lassen.
    Nur Graphen die :math:`K_5` oder :math:`K_{3,3}` enthalten sind nicht planar.

    .. figure:: k5_and_k33.png

    Ein Graph ist **planar**, wenn eine zweidimensionale Einbettung des Graphs ohne
    Kantenkreuzungen existiert.

  Gerichtete azyklische Graphen
    (DAG "directed acyclic graph") Ein gerichteter Graph ohne Zyklus.

  Multigraphen
    Verbindung von zwei Knoten durch **mehr** als eine Kante. Ein einfacher
    Graph hat hingegen keine Multikanten.

  Hypergraphen
    Bei Hypergraphen verbindet eine Kante mehr als zwei Knoten gleichzeitig.
    Eine Kante ist somit eine Teilmenge verbundener Knoten.

    .. figure:: hypergraphen.png
      :scale: 50 %
      :alt: hypergraphen

  isomorph
    Zwei Graphen :math:`G_1(V_1, E_1)` und :math:`G_2(V_2, E_2)` heißen **isomorph**,
    falls sie bis auf Umbenennung gleich sind.

    D.h. es gibt eine bijektive Umbenennungs-Funktion :math:`\phi` sodass:

    .. math:: \forall e = (v,w) \in E_1 \Leftrightarrow (\phi(v), \phi(w)) \in E_2

  Komplementgraph
    :math:`\Bigl( V, \bigl( \begin{array}{c} V \\ 2 \end{array} \bigl) \backslash E \Bigl)` heißt
    Komplementgraph von :math:`G = (V,E)`.

    Der Komplementgraph enthält also die gleichen Knoten und genau die anderen
    Kanten.

  Clique
    Eine Menge :math:`C \subseteq V` ist eine **Clique** in :math:`G`, falls
    :math:`( \begin{array}{c} C \\ 2 \end{array}) \subseteq E`

    Eine Menge ist eine Clique, falls zwischen den Knoten in :math:`C` **alle**
    Kanten vorhanden sind.

  Unabhängige Menge
    Eine Menge :math:`U \subseteq V` ist eine **unabhängige Menge** in :math:`G`,
    falls :math:`( \begin{array}{c} U \\ 2 \end{array}) \cap E = \emptyset`

    Also falls zwischen den Knoten in :math:`U` **keine** Kanten vorhanden sind.

  bipatiter Graph
    Ein **bipatiter Graph** kann in 2 Teile geteilt werden, sodass alle Kanten
    genau einmal die Trennlinie schneiden.
