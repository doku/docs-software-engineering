===================
Minimaler Spannbaum
===================

Sei :math:`G=(V,E)` ein zusammenhängender (gerichteter oder ungerichteter) Graph.

Der **Spannbaum** von G ist der (gerichtete bzw. ungerichtete) Teilgraph
:math:`B=(V, E_B)` der ein Baum ist und in :math:`B` alle Knoten von :math:`G`
vorkommen.


Die :ref:`breadth-first-search` ergibt einen Spannbaum, allerdings keinen
minimalen.
Der :ref:`dijkstra-algorithm` kann modifiziert werden, um den minimalen Spannbaum
zu finden. Diese Variante ist unter dem Algorithmus von Prim bekannt und nutzt
einen Fibonacci-Heap für die effiziente Implementierung.

Der Spannbaum beinhaltet alle Knoten aber möglichst wenige Kanten (Kantengewichten).

Ein Baum ist ein Graph ohne Zyklen.

Das Gewicht eines Graphen ist die Summe aller Gewichte (der Kanten).

Der **Minimale Spannbaum** :math:`B` von :math:`G` ist derjenige Spannbaum mit dem
geringsten Gewicht.

Algorithmus von Kruskal
=======================

Der Algorithmus von Kruskal bestimmt den minimalen Spannbaum für ungerichtete
Graphen und arbeitet Kantenbasiert. Er iteriert über die kleinsten Kante und ist
somit ein :ref:`greedy-algorithm`.

Im Algorithmus werden folgende Schritte durchgeführt:

#. Auflisten aller Kanten nach Gewichten geordnet
#. starten bei Kante mit geringstem Gewicht, schrittweises aufnehmen von Kanten zum
   Minimalen Spannbaum, nur dann wenn durch die Kante neue Knoten zum Baum verbunden
   werden.

.. code::

  algorithm Kruskal(G)

  sort(E);
  A := {};
  while size(A) < n-1 do
    e := takeFirst(E);
    if isAcyclic(G(A.add(e)) then
      A.add(e);
    fi;
  od;

Da ``isAcyclic()`` nicht trivial ist bietet sich auch die Implementierung unter
Verwendung von Mengen an:

.. code::

  algorithm Kruskal(G, v)

  F := sort(E);
  A := {};
  while size(A) < n-1 do
    e := takeFirst(E);

    // Endknoten von E, Abfrage, ob beide Wurzeln derselbe Knoten sind.
    M := FIND(e.origNode());
    N := FIND(e.destNode());

    if M != N then
      UNION(M, N);
      A.add(e);
    fi;
  od;

.. figure:: kruskal_algorithm_example_1.png
	 :alt: kruskal_algorithm_example_1

Jeder Knoten als Trivialbaum.
Schrittweises einfügen der Kanten (immer nur um neue Teilbäume zu verbinden)

Sobald man :math:`n-1` Kanten hat, kann man aufhören, da der Baum fertig aufgebaut
wurde. Bei :math:`n` oder mehr Kanten auf :math:`n` Knoten hätte man zwingend einen
Zyklus und somit keinen Spannbaum mehr.
