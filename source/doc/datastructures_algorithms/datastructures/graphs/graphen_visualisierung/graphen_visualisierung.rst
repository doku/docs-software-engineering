=====================
Graphenvisualisierung
=====================

Die Graphen Visualisierung beschäftigt sich mit der lesbaren visuellen Darstellung
von Graphen zur Datenanalyse.

Der Graph ist eine visuelle Repräsentation abstrakter Daten und die Position der
Knoten wird von einem Layoutalgorithmus bestimmt.

Layoutalgorithmen
=================
Ein Layoutalgorithmus für :term:`planare Graphen` legt die genaue Knotenpositionen
fest, ohne Kantenkreuzungen zu erzeugen.

Schwerpunktmethode (barycenter method) nach Tutte
-------------------------------------------------

.. figure:: algorithm_barycenter_method.png
	 :alt: algorithm_barycenter_method

.. figure:: barycenter_method_example.png
	 :alt: barycenter_method_example

Bäume
-----

#. Zeichne rekursiv den linken Unterbaum
#. Platziere den aktuellen Wurzelknoten eine Einheit über dem Unterbaum und eine
   Einheit rechts vom linken Unterbaum.
#. Zeichne rekursiv den rechten Unterbaum und platziere ihn eine Einheit unter und
   eine Einheit rechts vom aktuellen Wurzelknoten.

.. figure:: layout_binary_tree.png
	 :alt: layout_binary_tree

Allgemeine Graphen
-------------------

Lesbarkeitskriterien oder **ästetische Kriterien**

- Anzahl der Kantenkreuzungen minimieren
- Kantenlängen ausgleichen
- Knoten gleichmäßig verteilen
- Symmetrien maximieren
- Zeichnfläche minimieren
- Ähnliche Knoten nebeneinander platzieren

.. figure:: layoutalgorithmen.png
	 :alt: layoutalgorithmen

Kräftebasiertes Layout (force-directed layout)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Der Graph wird mittels eines physikalischen Mosells abgebildet. Die Kanten werden als
Federn zwischen den Knoten betrachtet und die Knoten sind Partikel, die sich gegenseitig
anziehen und abstoßen.

Die Knoten die miteinander verbunden sind ziehen sich an, wenn sie verbunden sind,
und stoßen sich ansonsten ab.

Layout nach Eades
~~~~~~~~~~~~~~~~~
Die Kanten zwischen Knoten entsprechen Federn. Auf jeden Knoten wirken Kräfte:

- Anziehung durch Federn zu adjazenten Knoten
- Abstoßung durch nicht-adjazente Knoten

.. figure:: layout_eades_algorithm.png
	 :alt: layout_eades_algorithm

Layout nach Kamada-Kawai
~~~~~~~~~~~~~~~~~~~~~~~~
Annahme: Ein Layout ist gut, wenn die geometrische Distanz zwischen Knoten
mit deren Distanz im Graph korrespondiert.

- Füge Federn auch zwischen nicht-adjazenten Knoten ein
- Bezeichne :math:`d_{i,j}` die Länge des kürzesten Weges zwischen zwei Knoten :math:`i`
  :math:`j`. Dann ist :math:`I_{i,j} = L d_{i,j}` die ideale Länge der Feder zwischen
  den Knoten, wobei :math:`L` die ideale Länge einer einzelnen Kante ist.

.. figure:: layout_kamada_kawai.png
	 :alt: layout_kamada_kawai
