========
Arrays
========

Arrays bieten wahlfreien Zugriff auf ein beliebiges Element. Sie haben eine
feste Größe, die nicht verändert werden kann.

Um die Größe eines Arrays anzupassen, müssen alle Einträge in ein größeres
Array umkopiert werden. 
