=========
AVL-Bäume
=========

AVL steht für **Adelson-Velskii und Landis** (russische Mathematiker, 1962).

AVL Bäume sind Binärbäume, die aber nur annähernd ausgeglichen sein müssen.

AVL Kriterium
  Für jeden (inneren) Knoten ist der Betrag der Differenz der Höhen des linken
  und rechten Teilbaums maximal :math:`1`

	.. math::
		T(UB_{links}) - T(UB_{rechts}) \in \{-1, 0, 1\}

.. figure:: avl_hoehendifferenz.png
	 :alt: avl_hoehendifferenz

.. code-block:: java
  :linenos:

  public class AVLTree<K extends Comparable <K>> {
    static class AVLNode <K extends Coparable<K>> {
      int balance; // height(left) - height (right)
      K key;
      AVLNode<K> left = null;
      AVLNode<K> right = null;

      public AVLNode (K e) {
        key = e;
        balance = 0;
      }
      public int getBalance() {
        return balance;
      }
      public int setBalance(int b) {
        balance = b;
      }
    }

    private AVLNode<K> head;
    private AVLNode<K> nullNode;
    private boolean rebalance = false;
  }

Einfügen in AVL Bäume
=====================

Einfügen eines neuen Blattknotens erfolgt mit dem üblichen Einfüge-Algorithmus
(:ref:`binary-tree-insert`).
Vor dem Einfügen ist nach der AVL Eigenschaft die Balance entweder :math:`-1, 0, 1`.

Nach dem Einfügen kann die Balance die Werte :math:`-2` und :math:`3` annehmen,
was die AVL Eigenschaft verletzt.

.. figure:: avlinsert.png
	 :alt: avlinsert
	 :width: 50%
	 :align: center


Rotationen
----------
Das Wiederherstellen des AVL Kriteriums erfolgt mittels **Einfachrotationen** und
**Doppelrotationen**.

Eine Einfachrotation wird durchgeführt, wenn in den linken Teilbaum des linken Kindes
ein Element eingefügt wird.
Oder wenn in den rechten Teilbaum des rechten Kindes ein Element eingefügt wird.
Dann wird mit dem rechten Kind rotiert

Eine Doppelrotation wird durchgeführt, wenn in den rechten Teilbaum des linken Kindes
ein Element eingefügt wird. Oder wenn in den linken Teilbaum des rechten Kindes
ein Element eingefügt wird.

.. figure:: rotationen_uebersicht.png
	 :alt: rotationen_uebersicht

Einfachrotation
^^^^^^^^^^^^^^^

.. figure:: einfachrotation1.png
	 :alt: einfachRotation1

X, Y, Z haben die gleiche Höhe

Das Einfügen eines neuen Elements führt dazu, dass das AVL Kriterium verletzt wird.
Das :math:`k_1` Element rotiert nach oben. Die Teilbäume bleiben unverändert.

.. figure:: einfachrotation2.png
	 :alt: einfachrotation2

Das Suchbaum Kriterium wird durch diese Umsortierung nicht verletzt.

Doppelrotation
^^^^^^^^^^^^^^

.. figure:: doppelrotation1.png
	 :alt: doppelrotation1


**Rotationsbeispiele**

.. figure:: rotationen_beispiel.png
	 :alt: rotationen_beispiel

.. figure:: rotationen_beispiel2.png
	 :alt: rotationen_beispiel2

Problem im linken des rechten Kinds das Problem, dann macht man eine rechts-links
Rotation.

Ist das AVL Kriterium durch das rechte Kind des linken Teilbaums verletzt, muss
man eine links-rechts Rotation machen

Algorithmus
-----------

1. Einfügeposition suchen
2. Neuen Knoten auf Blattebene erzeugen
3. Balance-Kriterium auf dem "Rückweg" testen
4. Bei Verletzung Ausgleich durch Rotation oder Doppelrotation

Rekursive Methoden für Suchen, Einfügen, Re-Balancieren

Baumstruktur ändert sich von den Blättern aufwärts, der Restrukturierte Knoten als
Rückgabewert.

Löschen im AVL Baum
===================

Gleicher Ansatz wie beim Einfügen:

1. Knoten suchen
2. Knoten entfernen
3. Balance des jeweiligen Teilbaums durch Rotationen wiederherstellen
4. Wenn Rotationen die Ausgeglichenheit des Elternknotens verletzt, muss man
   den Pfad bis zur Wurzel verfolgen und die Balance gegebenenfalls für jeden
   Knoten wieder herstellen.

In den Blättern können Element einfach gelöscht werden.

Löscht man ein Element im Wurzelknoten, werden die Unteren Kindelemente verschmolzen.
Überschreitet ein Knoten dann die 3 Elemente, wird dieser gesplitet und ein Element
wird hochgezogen.

Komplexität des AVL Baums
=========================

Höhe eines AVL-Baums mit :math:`n` Knoten:

.. math::
   \log_2(n+1) \leq height_{AVL} (n) \leq 1,44 \log_2 (n+2) - 0,328

Worst-case von Suchen, Einfügen und Löschen ist also :math:`O(\log n)`
