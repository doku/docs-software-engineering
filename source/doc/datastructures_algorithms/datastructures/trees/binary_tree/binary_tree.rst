==========
Binärbäume
==========

Im Binärbaum hat jeder Knoten genau zwei Unterbäume (die leer sein können).

Binärbaumtransformation
=======================

Jeder geordnete Baum lässt sich eindeutig in einen binären Baum umwandeln, indem
der linke Zeiger L stets auf das erste Kind und der rechte Zeiger R stets auf den
nächsten Geschwisterknoten zeigt.

.. figure:: binary_tree_transformation.png
	 :alt: binary_tree_transformation

Suchbäume
=========

Ein Binärbaum heißt Suchbaum, wenn für jeden Knoten :math:`n` gilt:

-  Alle Schlüssel im linken Unterbaum von :math:`n`  sind echt kleiner als der
   Schlüssel von :math:`n`
-  Alle Schlüssel im rechten Unterbaum von :math:`n` sind echt größer als der
   Schlüssel von :math:`n`
-  Dürfen Schlüssel mehrfach vorkommen, so werden diese einheitlich dem
   linken oder rechten Unterbaum zugeordnet

.. hint::
  Durchläuft man einen Suchbaum in
  **inorder-Reihenfolge**, so besucht man die Knoten in aufsteigend
  sortierter Reihenfolge (bezüglich ihrer Schlüsselwerte)

Typische Operatoren für Suchbäume sind:

-  FIND
-  INSERT
-  DELETE

Invariante nach jeder Operation das Linke Element jedes Kinds ist kleiner als die
Wurzel und jedes Kind rechts ist größer als die Wurzel

Suchen im Suchbaum
------------------

Der Suchschlüssel wird mit dem Schlüssel der Wurzel verglichen. Wenn der Schlüsselwert
**kleiner** ist, dann sucht man im **linken** Teilbaum weiter. Wenn der Schüssel
**größer** ist, dann wird im **rechten** Teilbaum weitergesucht. Sind die Schlüsselwerte
gleich, hat man das Element gefunden.

.. code::
	
	algorithm binaryTreeSearchIterative(node, searchKey)

		while node != null && node.key != searchKey do
			if searchKey < node.key
				node := node.left
			else
				node := node.right
			fi
		od
		return node

Um das kleinste Element in einem Suchbaum zu finden, muss das Blatt ganz links
bestimmt werden. Das größte Element ist ganz rechts.

.. _binary-tree-insert:

Einfügen im Suchbaum
--------------------
Wir nehmen vor dem Einfügen an, dass ein Suchbaum vorliegt, und müssen sicherstellen,
dass auch nach dem Einfügen ein Suchbaum vorliegt.

Wie suchen zunächst die Einfügeposition d.h. den Blattknoten mit der nächst kleineren
oder nächst größeren Schlüssel. Man erzeugt dann den neuen Knoten und verlinkt diesen als
Kind Knoten des Knotens an der Einfügeposition. Wenn der Schlüssel bereits existiert,
fügen wir diesen nicht erneut ein.

Knoten werden immer als Blattknoten eingefügt. Liefert ``false`` wenn es den Schlüssel
schon im Baum gibt.

Löschen in Suchbaum
-------------------
Zuerst sucht man das zu löschende Element: dieses sei Knoten :math:`k`. Man muss nun
den nachrückenden Knoten bestimmen und den Baum anpassen. Hier gibt es drei Fälle:

  1. Knoten :math:`k` ist ein Blatt (hat keine Kinder): Löschen des Knotens
  2. Knoten :math:`k` hat genau ein Kind: Kind in die Lücke des gelöschten Elements
     hochziehen.
  3. Knoten :math:`k` hat zwei Kinder: Ersetze durch weitest links stehenden Knoten
     rechten Teilbaums (**Inorder-Nachfolger**)

     Durch diesen Schritt kann der Baum mit der Zeit entarten, was sich negativ auf
     die Suchzeit auswirkt. Deswegen wird in der Praxis ein zufälliger Wechsel
     zwischen Inorder-Vorgänger (am weitesten rechts stehender Knoten des linken
     Teilbaums) und  Inorder-Nachfolger gewählt.

.. figure:: search_tree_delete_example.png
	 :alt: search_tree_delete_example

Komplexität
-----------
Die Komplexität der Operationen ist :math:`O(h)` für einen Baum der Höhe :math:`h`
Für die Höhe eines ausgeglichenen Baumes gilt :math:`h=\log_2 n` bei :math:`n` Knoten.

Ein ausgeglichener oder balancierter Baum ist ein Baum mit minimaler Höhe.
Für jeden Knoten unterschieden sich die Höhe des linken und des rechten Teilbaums
um höchstens 1. Dies erfordert Ausgleichen beim Einfügen und beim Löschen.

.. figure:: balancierentree.png
	 :alt: balancierenTree

Bei einem maximal entarteden Baum ist die Höhe :math:`n`. Es folgt also lineare
Komplexität.
