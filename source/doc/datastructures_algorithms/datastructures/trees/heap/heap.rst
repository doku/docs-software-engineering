====
Heap
====

Der vollständige Binärbaum lässt sich auch als Feld darstellen.
Hier ist der Baum in Levelorder traversiert und Kinder des :math:`i-ten` Knotens
an der Stelle :math:`2i` (linke Kind) und :math:`2i+1` (rechte Kind).
Elemente mit höchster Priorität an vorderster Stelle.

Heaps haben eine eindeutige Abbildung vom Array zum Binärbaum und umgekehrt.

.. figure:: heap_array.png
	 :alt: heap_array

Min Heap
	Jeder Knoten immer kleiner als seine Kinder

Max Heap
	Jeder Knoten immer größer als seine Kinder

Heaps sind **keine** Suchbäume, da das linke Element nicht kleiner als das
rechte Element sein müssen.

Heap-Strukturen sind besonders dann geeignet, wenn keine vollständig sortierte
Reihenfolge festgelegt ist, sondern nur das jeweils größte bzw. kleinste
Element ausgewählt wird. Das wäre eine Prioritätswarteschlange.

Einfügen
========

Das Element wird an letzter Stelle des Feldes eingefügt. Danach muss durch Sortierung
die Heap Eigenschaft wieder hergestellt werden. Das Element steigt entsprechend
seines Gewichtes hoch und wird immer dann wenn die Ordnungsbedingung verletzt ist
mit dem Elternknoten vertauscht.

Löschen
=======

#. Wurzel löschen
#. dann das letzte Element aus Levelorder als Wurzel setzen.
#. Durchsickern lassen (immer mit kleineren Kindknoten vertauschen) um die
	 Heap-Eigenschaft zu erhalten.

.. figure:: delete_root.png
	 :alt: delete_root

Aufbau eines Heaps
==================

Man betrachtet ein unsortiertes Feld als Baum ohne Heapeigenschaft.

Um die Heapeigenschaft herzustellen genügt es die ersten :math:`\frac{n}{2}` Elemente
vom letzten Element her kommend zu betrachten, da die zweiten :math:`\frac{n}{2}`
Elemente Blätter sind und somit automatisch die Heapeigenschaft erfüllen.
Man lässt das gerade betrachtete Element in den Heap einsickern.

.. figure:: aufbau_heap.png
	 :alt: aufbau_heap

Sortierung des Heaps: :ref:`sort-heapsort`
