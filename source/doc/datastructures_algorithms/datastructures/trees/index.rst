=====
Bäume
=====

.. toctree::
  :maxdepth: 2
  :caption: Contents
  :glob:

  trees/*
  binary_tree/*
  avl_tree/*
  234_tree/*
  red_black_tree/*
  b_tree/*
  digital_prefix_tree/*
  heap/*
  raeumliche_datenstrukturen/*
