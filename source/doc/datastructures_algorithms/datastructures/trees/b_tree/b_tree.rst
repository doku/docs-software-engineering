=======
B-Bäume
=======

AVL- und Rot-Schwarz-Bäume erlauben effizientes Einfügen und Löschen (in
:math:`O(\log n)`). Es müssen aber sehr viele Datenblöcke (Knoten) gespeichert werden.
Das ist besonders auf externen Speichern (z.B. Festplatten) ineffizient.

Der B-Baum ist **kein** Binärbaum. Die Grundidee ist, dass die Baumhöhe vollständig
ausgeglichen ist, aber der Verzweigungsgrad variiert. Dieser Suchbaum wurde von
Bayer und McCreight entwickelt.

B-Bäume sind **Mehrwegebäume**: Ein Knoten kann mehrere Elemente/Schlüsselwerte
enthalten (wie in 2-3-4 Bäumen).

Mehrwegbäume sind völlig ausgeglichen, wenn ale Wege von der Wurzel bis zu den
Blättern sind gleich lang, und jeder Knoten gleich viele Elemente hat.

.. glossary::

	B-Baum-Kriterium
		Jeder Knoten außer der Wurzel enthält zwischen :math:`m` und :math:`2m`
		Schlüsselwerten.

		:math:`m` heißt **Ordnung** des B-Baums.

Bei B-Bäumen spricht man auch von "Seiten" statt von "Knoten"

Für einen B-Baum der Ordnung :math:`m` gilt:

#. Jede Seite enthält höchstens :math:`2m` Knoten.
#. Jede Seite, außer der Wurzelseite, enthält mindestens :math:`m` Elemente :math:`e_i`
#. Jede Seite mit :math:`i` Elementen hat entweder keine Nachfolger oder :math:`i+1`
   Nachfolger
#. Alle Blattseiten liegen auf der gleiche Stufe. (B-Bäume sind immer ausgeglichen)

Einfügen in B-Baum
==================

.. figure:: bbaum_beispiel_1.png
	 :alt: bbaum_beispiel_1

	 Beispiel mit :math:`m=1`:

Der Baum wächst immer nur an der Wurzel. Ändert sich die Gesamthöhe, ändert sich
diese immer nur durch Einfügen eines neuen Wurzelknotens. Dadurch ist die Invariante
sichergestellt, dass alle Blätter immer auf dem gleichen Level sind.

Wird durch Einfügen eines neuen Elements eine Seite zu groß, so hat die mit
:math:`2m+1` Elementen damit immer eine ungerade Anzahl. Das mittlere Element
wird dann nach oben gezogen. Dieser Prozess muss eventuell rekursiv bis zur
Wurzel wiederholt werden.

.. figure:: bbaum_einfuegen_2.png
	 :alt: bbaum_einfuegen_2

Suchen
======
Das Suchen ist analog zu anderen Baumstrukturen.

.. figure:: bbaum_suchen.png
	 :alt: bbaum_suchen

Löschen
=======
Zum Löschen eines Elements wird die Seite des Elements gesucht.

Ist das zu löschende Element auf einer Blattseite:

- Element löschen
- Unterlauf (bei Seite mit nun weniger als :math:`m` Elementen) beheben

Ist das zu löschende Element nicht auf einer Blattseite:

- Element löschen
- Durch nächstkleineres Element von einer Blattseite ersetzten
- Unterlauf der Blattseite behandeln

Unterlaufbehandlung
-------------------
Eine Seite hat nach Löschen eines Elements Unterlauf, wenn diese nun :math:`m-1`
Elemente besitzt. (Noch weniger Elemente sind nicht möglich, da vor dem Löschen
die B-Baum Eigenschaft gelten musste)

Wenn die benachbarte Seite mehr als :math:`m` Elemente besitzt, kann man die beiden
Seiten ausgleichen.

.. figure:: bbaum_loeschen_2.png
	 :alt: bbaum_läschen_2

Besitzt die benachbarte Seite genau :math:`m` Elemente, so können die beiden
Seiten zusammengelegt werden. Man fügt das passende "mittlere" Element des Elternknotens
hinzu und muss eventuellen Unterlauf des Elternknotens behandeln.

.. figure:: bbaum_loeschen_1.png
	 :alt: bbaum_löschen_1


Anwendung
=========
B-Bäume werden besonders in Datenbanksystemen (Oracle, DB2, SQLite) und Dateisystemen
wie XFS und NTFS verwendet.
