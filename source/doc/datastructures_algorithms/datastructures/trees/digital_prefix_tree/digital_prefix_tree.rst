========================
Digital- und Präfixbäume
========================

Digitalbäume sind Suchbäume über einem festen Alphabet. Das Alphabet kann sich
auch binär kodieren lassen.

Sie sind nur bei Gleichverteilung der Einträge ungefähr ausgeglichen.

Tries
=====
Der Name "Trie" leitet sich aus "Text Re\ **trie**\ val" ab.

Schlüsselwerte sind nur in den Blattknoten und innere Knoten haben nur
Wegweiserinformation.

Ein Trie ist nach Schlüssel sortiert und unabhängig von der Einfügereihenfolge der
Schlüssel.

Ein Trie ist eindeutig durch die Menge der einzufügenden Schlüssel bestimmt.

Auf Tries lässt sich eine Präfixsuche durchführen.

Die Schlüssel müssen nicht explizit gespeichert werden, sondern sind über die
Konstruktion gegeben.

Tries haben oft Lange Folgen ohne Verzweigungen, die zur Liste entarten. Zudem sind
Tries in der Regel sehr unausgeglichen

Patricia Bäume
==============

Der Name Patricia bildet sich aus dem Akronym von
"Practical Algorithm To Retrieve Information Coded In Alphanumeric"

Grundidee der Patricia Bäume ist es, Teile der Zeichenketten, die für die
Vergleiche bzw. das Verzweigen irrelevant sind, werden übersprungen.

Jeder Knoten enthält die Anzahl der zu überspringenden Zeichen. Dazu lässt
sich vor allem bei langen Wörtern Suchaufwand reduzieren und die
Darstellung des Baums komprimieren.

.. figure:: digital_trees_overview.png
	 :alt: digital_trees_overview

Präfix-Baum
===========
In den Knoten wird das zu überspringenden Teilwort und seine Länge gespeichert.
Für die Verzweigungen werden die zu testenden Zeichen angegeben.

.. figure:: prefix_tree.png
	 :alt: prefix_tree

.. figure:: prefix_b_tree.png
	 :alt: prefix_b_tree
