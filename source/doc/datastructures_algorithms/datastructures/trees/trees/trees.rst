=====
Bäume
=====

Ein Baum ist eine mehrdimensionale, hierarchische Datenstruktur.

Ein Baum besteht aus einer Menge von **Knoten** (**nodes**) und einer Menge
von **Kanten** (**edge**) mit besonderen Eigenschaften.

Jeder Baum besitzt einen **Wurzel** Knoten (**root**)

.. figure:: baumterminologie.png
	 :alt: baumTerminologie

Jeder Kindknoten (child node) - d.h. außer der Wurzel - ist genau durch eine Kante mit
einem Eltern Koten verbunden

Ein Knoten ohne Kinder heißt Blatt (leaf). Alle Anderen bezeichnet man als innere
Knoten (inner nodes).

Ein Pfad in einem Baum ist eine Folge von unterschiedlichen Knoten, in der die aufeinander
folgenden Knoten durch Kanten verbunden sind.

Die Länge des Pfades von der Wurzel zu einem Knoten wird als dessen **Niveau**
bezeichnet. Der Wurzelknoten hat das Niveau 0. Die Höhe des Baum entspricht dem
größten Niveau +1.

.. figure:: aufbaubinaerbaum.png

.. glossary::

	n-ärer Baum
	  Knoten in einem **n-ären** Baum haben höchstens n Kinder. Im Binärbaum hat jeder
	  Knoten maximal 2 Kinder

	voller Baum
	  Bei einem vollen n-ären Baum haben alle Knoten 0 oder n Knoten.

	vollständiger Baum
		Ein vollständiger Baum ist ein voller Baum, bei dem alle Blätter auf dem
		gleichem Niveau sind.

	geordneter Baum
	  Kinder jedes Knotens sind in einer bestimmten Reihenfolge geordnet.

.. figure:: entarteterbaum.png
	 :alt: entarteterBaum

Die Höhe des entarteten Baums ist :math:`h = n`.

.. _tree_traversal:

Baumtransversierungen
=====================

-  **preorder**: W-L-R (Wurzelknoten zuerst in Traversierung)
-  **inorder**: L-W-R
-  **reverse inorder**: R-W-L
-  **postorder**: L-R-W (Wurzelknoten zuletzt in Traversierung)
-  **levelorder**: Die Knoten werden geordnet nach deren Höhe (von oben
   nach unten) und von links nach rechts durchlaufen (Der Wurzelknoten ist
   das erste Element bei Traversierung.)

.. figure:: baumtransversierungen.png
   :alt: Baumtransversierungen

Die Baumtraversierungen preorder, inorder und postorder sind Tiefensuche.
Levelorder ist eine Breitensuche und sucht immer auf den selben Niveau alle Knoten.

.. figure:: baumtraversierung_beispiel.png
	 :alt: baumtraversierung_beispiel

.. code-block:: java

	public void printPreOrder (TreeNode<T> n) {
		if (n != null) {
			System.out.println(n.getKey());
			printPreOrder(n.getLeft());
			printPreOrder(n.getRight());
		}
	}

	public void printInorder (TreeNode<T> n) {
		if (n != null) {
			printInorder(n.getLeft());
			System.out.println(n.getKey());
			printInorder(n.getRight());
		}
	}

	public void printPostOrder(TreeNode<T> n) {
		if (n != null) {
			printPostOrder(n.getLeft());
			printPostOrder(n.getRight());
			System.out.println(n.getKey());
		}
	}

	//Levelorder

	public void printLevelorder(Queue<TreeNode<K>> queue) {
		while(!queue.isEmpty()) {
			TreeNode<K> node = queue.remove();
			if (node.getLeft() != nullNode) {
				queue.add(node.getLeft());
			}
			if (n.getRight != nullNode) {
				queue.add(node.getRight());
			}
			System.out.println(node.toString());
		}
	}

	Queue<TreeNode<K>> queue = new ArrayDeque<TreeNode<K>>();
	queue.add(head.getRight());
	printLevelOrder(queue);

Für Iterator Implementierung muss bei Instanziierung des Iterators ein Stack
mit der Traversierung erzeugt werden. Dazu können auch die rekursiven
Implementierungen der Traversierungen verwendet werden.

.. code-block:: java

	public class PostorderIterator<T extends Comparable<T>> extends TreeTraversalIterator<T> {

		private Queue<IBinaryTreeNode<T>> nodeQueue = new LinkedList<>();


		public PostorderIterator(BinarySearchTree<T> tree) {
			getNodeStackInPreOrder(tree.getRootNode());
		}

		/**
		 * recursive implementation that saves all tree nodes in postorder the nodeQueue
		 * @param rootNode
		 */
		private void getNodeStackInPreOrder(IBinaryTreeNode<T> rootNode) {
			if(rootNode != null) {
				getNodeStackInPreOrder(rootNode.getLeftChild());
				getNodeStackInPreOrder(rootNode.getRightChild());
				nodeQueue.offer(rootNode);
			}
		}

		@Override
		public T next() {

			if (!this.hasNext()) {
				throw new NoSuchElementException();
			}

			return nodeQueue.poll().getValue();
		}

		@Override
		public boolean hasNext() {
			return !nodeQueue.isEmpty();
		}

	}
