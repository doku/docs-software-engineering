===========
2-3-4-Bäume
===========

Es werden mehrere Schlüssel pro Knoten verwendet. Neben binären Knoten (2-Knoten
mit 1 Schlüssel) gibt es auch 3-Knoten mit 2 Schlüssel und 4 Knoten mit 3 Schlüssel.

Die Anzahl der Nachfolger entspricht der Anzahl der Schlüssel + 1.

Die Kante zwischen zwei Schlüsseln verweist auf einen Teilbaum mit Werten die
zwischen diesen Schlüsseln liegen.

.. figure:: 234baume.png
	 :alt: 234baume

234-Bäume sind immer vollständig balanciert.

Einfügen, Löschen und Suchen ist im worst-case als :math:`O(log(n))` möglich.

Suchen und Einfügen
===================
Neue Elemente werden immer in der Blattebene eingefügt.
Diese können solange in die Knoten eingefügt werden, bis ein 4-er Konten "überfüllt"
ist. Dann muss der Knoten gesplittet werden. Dabei wird das mittlere Element des
4-er Knotens (hat 3 Elemente) eine Ebene nach oben geschoben.

.. figure:: 234split.png
	 :alt: 234split
