=========================
Räumliche Datenstrukturen
=========================

Abstandsbestimmung
==================

Unterteilung des Raumes in reguläres Gitter.

Quadtree
========

Quadtrees dienen zur hierarchischen Raumunterteilung. Rekursiv werden Zellen mit
mehr als einem Punkt weiter mit der 1-zu-4 Unterteilung aufgeteilt.

Die Traversierung ist relativ teuer durch häufige Auf- und Abbewegung in der
Hierarchie.

.. figure:: quadtreeexample.png
	 :alt: quadtreeexample
	 :scale: 40%

Quadtrees haben immer 4 oder 0 Kinder.

.. figure:: quadtree_example2.png
	 :alt: quadtree_example2

Die Punkte werden in den Blättern des Baums gespeichert. Falls die
Unterteilungstiefe beschränkt ist, kann ein Blatt auch mehrere Punkte enthalten.

.. figure:: quadtree_aufbau.png
	 :alt: quadtree_aufbau

Traversierung
-------------

Octree
======

Der Octree stellt eine Hierarchische Raumunterteilung in 3D dar. Analog zum
Quadtree wird hier eine 1-zu-8 Raumunterteilung vorgenommen. Die Gitterzellen sind
Würfel.

.. figure:: octaltree_aufbau.png
	 :alt: octaltree_aufbau

Hüllkörper
==========

Objekte in einfache Hüllkörper einschließen:

- Kugel (**bounding sphere**)
- Achsenparallele Bounding Box (**AABB**)
- orientierte Bounding Box (**OOB**)

.. figure:: huellkoerper.png
	:scale: 50 %
	:alt: huellkoerper
	:align: center

BSP Baum (Binary Space Partitioning)
====================================

BSP Baume sind graphisch Datenstrukturen.

Erweiterung von Binärbäumen in k Dimensionen

Man verwendet Ebenen, um den Raum rekursiv zu unterteilen

An Knoten muss zudem eindeutig die Trennlinie und die Orientierung (links/rechts)
gespeichert werden.

.. figure:: bsp_baum.png
	 :alt: bsp_baum

Der Baum kann rekursiv konstruiert werden.

Die Blattknoten speichern die Objekte und die inneren Knoten speichern die
Split-Ebenen.

Suche im BSP Baum
-----------------

Schritt für Schritt wird die Halbebene bestimmt, in der sich das
gesuchte Element befindet. Das Verfahren ist der Suche im Binärbaum
sehr ähnlich.

.. figure:: bsp_tree_search.png
	 :alt: bsp_tree_search

Diese Prozedur findet den Unterraum, in dem der Punkt sich befindet, aber nicht
immer das nächste Objekt.

kD-Bäume
--------

Split-Ebenen, die senkrecht zur x-, y- oder z-Achse.

Objekte, die eine Split-Ebene schneiden, werden meist in meide Kindknoten eingefügt,
können aber auch zerschnitten werden.

.. figure:: kd_tree.png
	 :alt: kd_tree

CSG-Bäume
=========

Komplexe Körper werden durch die Verknüpfung von Grundkörpern über (regularisierte)
Mengenoperationen erzeugt.

.. figure:: csg_tree_1.png
	 :alt: csg_tree_1

Die Konstruktion ist dabei nicht eindeutig. Die gleiche Szene kann durch verschiedene
CSG-Bäume beschrieben werden.

.. figure:: csg_tree_nicht_eindeutig.png
	 :alt: csg_tree_nicht_eindeutig
