=================
Rot-Schwarz-Bäume
=================

Ein Rot-Schwarz-Bum ist ein binärer Suchbaum, in dem jedem Knoten ein Farbattribut
zugeordnet ist, sodass gilt:

- Jeder Konten ist entweder ``rot`` oder ``schwarz``
- Jeder ``NIL``-Knoten (unter dem Blattknoten) ist per Definition ``schwarz``
- Die Kinder eines ``roten`` Knotens sind ``schwarz``
- Für jeden Knoten :math:`k` gilt: Jeder Pfad von :math:`k` enthält die gleiche
  Anzahl `schwarzer` Knoten. Die **Schwarztiefe** ist immer gleich.

Die Eltern- und Kindknoten eines ``roten`` Knotens sind ``schwarz``. Der Baum wird
umstrukturiert wenn diese Eigenschaften verletzt werden.

Rot Schwarz Bäume sind nur umgeformte ``2-3-4`` Bäume. Der Rot-Schwarz Baum muss
nicht perfekt balanciert sein und erreicht trotzdem :math:`O(\log n)`

Bei 3 Knoten im 2-3-4 Baum wird das linke Element rot gefärbt und das rechte Schwarz.
Beim 4 Knoten wird das mittlere Element schwarz gefärbt und die Randelemente
sind rot. Ein 2 Knoten ist schwarz.

.. figure:: struktur234baum.png
	 :alt: struktur234baum

.. figure:: beispielumformungrotschwarz.png
	 :alt: beispielumformungrotschwarz

.. figure:: redblack_equivalent_234.png
	 :alt: redblack_equivalent_234

Visualisierung: http://www.cs.usfca.edu/~galles/visualization/RedBlack.html

Die Java Klasse ``java.util.TreeMap`` ist als Rot-Schwarz Baum implementiert.

Suchen und Einfügen
===================
Beim Einfügen und Löschen bietet es sich an, den Rot-Schwarz Baum in einen 234-Baum
zu transformieren, auf dem 234-Baum die Operation durchzuführen und dann wieder
rücktransformieren.

.. figure:: insert_in_redblack_1.png
	 :alt: insert_in_redblack_1

.. figure:: insert_in_redblack_2.png
	 :alt: insert_in_redblack_2

.. figure:: insert_in_redblack_3.png
	 :alt: insert_in_redblack_3

Löschen
=======
Das Löschen eines roten Knotens ist unkritisch, sich die Schwarztiefe nicht ändert
und somit die Struktur des Rot-Schwarz Baums unverändert bleibt. 
