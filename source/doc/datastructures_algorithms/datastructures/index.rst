===============
Datenstrukturen
===============


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   introduction/*
   arrays/*
   list/*
   set/*
   trees/*
   graphs/index
   hashing/index
