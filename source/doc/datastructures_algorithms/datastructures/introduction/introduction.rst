==========
Einführung
==========

Eine Datenstruktur ist eine bestimmte Art, Daten zu verwalten und miteinander zu
verknüpfen, um in geeigneter Weise auf diese zugreifen und diese manipulieren
zu können.

Datenstrukturen sind immer mit bestimmten **Operationen** verknüpft, um Zugriff
und Manipulation zu ermöglichen.
