===============
Offenes Hashing
===============

Angenommen die gewählte :ref:`hash-funktion` :math:`h` ist nicht injektiv für :math:`S`
d.h. es gibt mehrere Elemente :math:`x,y \in S` mit :math:`h(x)=h(y)`

Wir können damit dennoch eine Hashdatenstruktur bauen, mit offenem Hashing.
Jeder Hashtafel Eintrag ist der Kopf einer verketteten Liste.

Bei Kollision wird die Position des Eintrags in der Hashtabelle nicht verändert und
in einer verketteten Liste eingefügt. Es wird gewissermaßen in der Tabellenzelle
mehr Platz geschafft. Dadurch erhöht sich die Komplexität des Suchens, Einfügens
und Löschen im Worst-Case auf :math:`O(n)`. Diese Komplexität lässt sich verbessern,
wenn man an jeden Eintrag einen Suchbaum anhängt.

Offenes Hashing benötigt dynamische Speicherverwaltung.

Platzbedarf
===========

.. math:: O(m+n) = O(n*(1+\frac{1}{\beta}))

:math:`\beta = \frac{n}{m}` ist der **Belegungsfaktur**.
Je kleiner :math:`\beta` desto platz-ineffizienter ist die Datenstruktur,
aber möglicherweise ist es dann einfacher eine "gute" Hashfunktion zu finden.

Zugriffszeiten
==============
Man nimmt an, dass :math:`h(x) \in O(1)` ausgewertet werden kann.
Sei :math:`S` die Schlüsselmenge.

- Zugriff auf :math:`x \in S` in :math:`\mathcal{O}(1+ \text{ Position von x in Liste } L_{h(x)})`
- Zugriff auf :math:`x \in U\backslash S` in :math:`\mathcal{O}(1+|L_{h(x)}|)`

Erwartete Suchzeit
==================

Man nimmt an, :math:`h` verteilt :math:`U` gleichmäßig über :math:`\{0, ..., m-1\}`,
das bedeutet:

.. math::
  \forall i \in \{0,.., m-1\} :
    |\{x \in U : h(x) = i\}| \leq \left\lceil \frac{|U|}{m} \right\rceil

Zum Beispiel :math:`h(x) = x \mod m`

Zufälliges Element außerhalb der Schlüsselmenge
-----------------------------------------------

**Satz:**
Sei :math:`x` ein zufälliges (gleichverteiltes) Element aus :math:`U \backslash S`
und :math:`n \leq \frac{|U|}{2}`.
Dann ist die erwartete Suchzeit von :math:`x`:
:math:`\mathcal{O}(1+\frac{n}{m}) = \mathcal{O}(1+\beta)`

**Beweis:**
Sei :math:`l_i` die Anzahl der Element aus :math:`S`,  die in :math:`L_i`
gespeichert werden: :math:`l_i = |L_i|`
Es gilt :math:`\sum_{i=0}^{m-1} l_i = n =|S|`.
Hierbei sind die Elemente :math:`U_i` diejenigen Elemente aus :math:`U`,
welche auf :math:`i` gehashed werden.

.. math::

  Pr(h(x)=i) &= \frac{|U_i\setminus S|}{|U\setminus S|}
             \leq \frac{|U_i|}{|\underbrace{U\setminus S|}_{\geq \frac{U}{2}}}
             \leq \frac{\left\lceil\frac{|U|}{m}\right\rceil}{\frac{|U|}{2}}
             \leq \frac{\frac{|U|}{m} + 1}{\frac{|U|}{2}} \\
            &= \frac{2}{m} + \underbrace{\frac{2}{|U|}}_{n \leq \frac{|U|}{2}}
            \leq \frac{2}{m} + \frac{1}{n}

Die Erwartete Suchzeit ist:

.. math::

  E & =\left(\sum_{i=0}^{m-1} Pr(h(i)=i) \cdot l_i\right) + 1 \\
    & \leq \left(\sum_{i=0}^{m-1}\left(\frac{2}{m} + \frac{1}{n}\right) \cdot l_i\right) + 1 \\
    & =  1 + \frac{2}{m} \underbrace{\sum_{i=0}^{m-1}l_i}_{=n}
                + \frac{1}{n} \underbrace{\sum_{i=0}^{m-1}l_i}_{=n} \\
    & =  1 + \frac{2n}{m} +1
    = 2 + \frac{2n}{m}
    = \mathcal{O}(1+\beta)

Das bedeutet, dass die erwartete Suchzeit für ein Zufälliges Element :math:`x \notin S`
ok ist.

Zufälliges Element innerhalb der Schlüsselmenge
-----------------------------------------------

**Satz:**
Sei :math:`x` ein zufälliges Element aus :math:`S`, dann ist die erwartete
Laufzeit für x:

.. math::
  \mathcal{O} \left( 1+ \frac{1}{n} \sum_{i=0}^{m-1} \frac{l_i (l_i -1)}{2}\right)

Wobei :math:`l_i = |L_i|` die Anzahl der Element aus :math:`S` ist, die auf :math:`i`
gehashet werden.

Anmerkung: Wir würden gerne eine Schranke unabhängig von den :math:`l_i` angeben.
Aber :math:`\sum_i l_i = n` sagt nichts über :math:`\sum_i l_i^2`

**Beweis**:
Wenn :math:`x` das :math:`j`-te Element seiner Liste ist, dann ist die
Suchzeit :math:`\mathcal{O}(1+j)`

.. math::

  \mathcal{O}( \sum_{i=0}^{m-1} \sum_{j=1}^{l_i} (1+j))
    = \mathcal{O} (1 + \frac{1}{n} \sum_{i=0}^{m-1} \frac{l_i * (l_i +1)}{2})

(bei einem beliebigen S)

Erwartete Suchzeit bei zufälliger Schlüsselmenge
------------------------------------------------

**Satz:**
Sei :math:`S` eine zufällig gleichverteilte Teilmenge aus :math:`U`
der Größe :math:`n` dann ist die erwartete Suchzeit nach einem zufälligen
:math:`x \in S`:

.. math:: \mathcal{O}(1+\beta*\frac{3}{2}*e^\beta)

Damit folgt für :math:`\beta` nicht deutlich größer als :math:`1` ist die Suche
in :math:`\mathcal{O}(1)` möglich.

.. hint::

  Dieser und der Vorletzte Satz besagen: Falls :math:`S \subseteq U` zufällig
  gewählt, ist erwartet alles gut. Praktisch ist das nicht der Fall.
