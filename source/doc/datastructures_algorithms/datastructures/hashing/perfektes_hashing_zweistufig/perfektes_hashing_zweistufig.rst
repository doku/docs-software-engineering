.. _zweistufiges-perfektes-hashing:

==============================
Zweistufiges Perfektes Hashing
==============================

Wir hashen zuerst mit einer Primär-Hashfunktion in eine Hashtafel, sodass wir
höchstens linear viele Kollisionen haben, also :math:`c_S(h) \in \mathcal{O}(n)`.
Damit werden maximal :math:`\sqrt{n}` viele Elemente auf den selben Hashwert
abgebildet.

Die Elemente mit Kollisionen hashen wir dann nochmal mit einer Sekundär-Hashfunktion.

Wir wählen für jeden Primärhashtafeleintrag :math:`i` mit :math:`>1` Elementen eine
Sekundärhashfunktion :math:`h_i`, die injektiv in eine Sekundärhashtafel hasht.

.. figure:: mehrstufiges_perfektes_hashing.png
	 :alt: mehrstufiges_perfektes_hashing

- :math:`m` : Größe der Primärhashtafel
- :math:`m_i`: Größe der :math:`i`-ten Sekundärhashtafel
- :math:`l_i`: Anzahl der Element, welche von Primärhashfunktion auf :math:`i`
  gemappt werden.

Für einen Primärhashtafeleintrag mit :math:`l_i` Elementen
wähle :math:`m_i > 2*c* \binom{l_i}{2}`

d.h. wir können :math:`h_i` injektiv in Zeit :math:`\mathcal{O}(\binom{l_i}{2})`
finden (erwartet)

Platzverbrauch für alle Sekundärhashtafeln ist:

.. math::
  \sum_{i=0}^{m-1} 2 * c *  \binom{l_i}{2} = 2*c \sum_{i=0}^{m-1}  \binom{l_i}{2}
  = 2*c* \text{ Anzahl Kollisionen von h }

:math:`c_S(h)` ist die Anzahl der Kollisionen unter :math:`h`

.. math:: c_S(h) = \sum_{i=0}^{m-1} \binom{l_i}{2} \approx n

Für Wahl von :math:`m = c*n`

.. math:: E(c_S(h)) \leq \frac{c}{m} = \frac{n(n-1)*c}{2 *c*n} = \frac{n-1}{2}

Geht nur für Statischen Fall (Festes zuvor bekanntes :math:`S`)


Erwartete Kollisionen der Primär-Hashfunktion
=============================================

**Korollar:**
Falls :math:`m> \frac{n-1}{2}*c`, dann existiert ein :math:`h \in \mathcal{H}`
mit :math:`c_S(h) \leq n`

**Beweis:**
Für ein zufälliges :math:`h \in \mathcal{H}` gilt:

Beweis: (Mit Satz: Für :math:`h \in \mathcal{H}` zufällig: gilt :math:`E(c_s(h)) \leq \binom{n}{2}\frac{c}{m}`)


.. math::
  E(c_S(h)) &\leq \binom{n}{2} \cdot \frac{c}{m} \\
  & = \frac{1}{2} \cdot \frac{n \cdot (n-1)}{\frac{n-1}{2}} \\
  & = n

**Korollar:**
Falls :math:`m > (n-1)*2`, dann gilt für mindestens die Hälfte aller
:math:`h \in \mathcal{H}`: :math:`c_S(h) \leq n`

**Korollar:** Für :math:`m>2c \binom{n}{2}` könen wir in erwartet :math:`\mathcal{O}(n+m)`
Zeit erzielen ein :math:`h \in \mathcal{H}` finden mit :math:`h|S` injektiv.

Setze m (Größe der Primärhashtafel) auf :math:`\approx n` und stelle sicher, dass
Primärhashfunktion :math:`\mathcal{O}(n)` Kollisionen produziert.

.. note::
  Wir können in erwartet :math:`\mathcal{O}(n+m)` eine Primärhashfunktion :math:`h`
  finden, mit :math:`c_S(h)\leq n` und Hashtafelgröße :math:`m = \mathcal{O}(n)`


Größe der Sekundär-Hashtafeln
=============================

Seien nun alle Elemente aus :math:`S`, die von der Primär-Hashfunktion :math:`h`
in den :math:`i`-ten Beutel gehasht werden folgendermaßen definiert:

.. math::
  B_i(h) = h |_S^{-1}(i) = \{x \in S | h(x)=i\}

Es gilt außerdem :math:`S_i(h) = |B_i(h)|`.

Es gilt: :math:`c_s(h) = \sum_{i=0}^{m+1} \binom{s_i(h)}{2}`

Da wir :math:`h` so gewählt haben, dass :math:`c_s(h) \leq n` gilt insbesondere auch
:math:`\sum_{i=0}^{m+1} \binom{s_i(n)}{2} \leq n`

Für jeden Hashbeutel :math:`B_i(h)` erzeugen wir eine Sekundärhashfunktion,
als auch eine Sekundärhashtafel mit der Größe :math:`m_i > 2c \binom{s_i(h)}{2}`
welche dann Inhalt von :math:`B_i(h)` injektiv hasht.

Platzverbrauch und Konstruktionszeit für eine Sekundärhashtafel :math:`B_i`
ist somit:

.. math::
  \mathcal{O} \left(2c \binom{s_i(h)}{2} \right)
  = \mathcal{O} \left(\binom{s_i(h)}{2}\right)

Die Gesamtgröße und Konstruktionszeit aller Sekundärhashtabellen und
Sekundärhashfunktionen ist somit:

.. math::

  \mathcal{O}(\sum_{i=0}^{m-1} c \binom{s_i(n)}{2}) &=
  \mathcal{O}(c \underbrace{\sum_{i=0}^{m-1} c \binom{s_i(n)}{2}}_{=c_s(h)<n}) \\
  &= \mathcal{O}(c*n)

Algorithmus: zweistufiges perfektes Hashing
===========================================

Es sind :math:`S \subseteq U`, :math:`|S| = n` gegeben. Außerdem kann man
eine Familie von c-universellen Hashfunktionen :math:`h: U \rightarrow \{0, ... m-1\}`
samplen.

#. Finde :math:`h: U \rightarrow \{0, ..., c*n\}` welches auf :math:`S` maximal
   :math:`n` Kolisionen produziert (Primäres Hashing)
#. Bestimme für :math:`i = 0,..., c*n` alle :math:`B_i(h) = \{x \in S : h(x) = i\}`
#. Für jedes :math:`i=0, ...,c*n` mit :math:`|B_i(h)|>1` finde
   :math:`h_i U \rightarrow \{0, ..., \binom{S_i(h)}{2}*c\}`
   sodass :math:`h_i` injektiv fost für :math:`B_i` (Sekundäres Hashing)

Schritt 1. und 2. sind zusammen in :math:`O(cn)` Zeit, und auch Schritt 3.
ist in :math:`O(cn)` Zeit.


Zugriff auf ein :math:`x\in U`
------------------------------

Wende :math:`h` auf :math:`x` an :math:`h(x) = i`. Falls :math:`B_i(h) \leq 1`
hat man :math:`x` gefunden.
Sonst wendet man :math:`h_i` auf :math:`x` an. Dabei ist :math:`h_i(x)` sicher ein
Hashtafeleintrag mit :math:`\leq 1` Elementen.
