====================
Universelles Hashing
====================

Wir definieren eine Klasse von Hashfunktionen, aus denen wir zufällig auswählen
können.

Sei :math:`\mathcal{H}` eine Menge von Hashfunktionen von :math:`U` nach :math:`\{0,1,..., m-1\}`
Für :math:`c>1` heißt :math:`H` :math:`c`-universell, falls
:math:`\forall x,y \in U, x \neq y` gilt:

.. math:: \frac{|\{h \in \mathcal{H} : h(x) = h(y)\}|}{|\mathcal{H}|} \leq \frac{c}{m}

Egal welches Paar :math:`x,y` man aus dem Universum nimmt: Der Anteil der
Hashfunktionen in der Menge :math:`\mathcal{H}`, die dieses Paar auf das
gleiche Element abbilden sollte nicht zu groß sein.

Die :math:`c`-Universalität ist also ein Maß für die Qualität einer Menge von
Hashfunktionen.

:math:`c`-universelles Hashing ist eine randomisierte Form von Hashing, bei der die
Wahrscheinlichkeit für Kollisionen bei einer Menge mit :math:`m` Elementen
:math:`\frac{c}{m}` beträgt. Die Hashfunktion wird zufällig aus der Menge
:math:`\mathcal{H}` an Hashfunktionen für das jeweilige Universum gewählt.

**Satz:**
Für :math:`a,b \in \{0,1,..., N-1\}` sei:

.. math:: h_{a,b} : x \mapsto ((ax+b) \mod N) \mod m

Dann ist die Klasse:

.. math:: \mathcal{H} = \{ h_{a,b}: 0 \leq a,b \leq N-1 \}

c-universell mit :math:`c = \frac{\lceil N/m \rceil}{N/m} \approx 1`

**Beweis:**
Übung

**Satz:**
Benutzt man Hashing mit Verkettung und wählt :math:`h \in \mathcal{H}` zufällig
gleichverteilt und :math:`\mathcal{H}` c-universell, dann ist die erwartete Suchzeit
:math:`\mathcal{O}(1+c*\beta)` für **beliebige Mengen** :math:`S \subseteq U, |S| = n`.

**Beweis:**
Die Zeit für Zugriff auf ein :math:`x` ist
:math:`\leq 1 + \text{Anzahl}(y \in S \text{ mit } h(x) = h(y))`

Dazu definieren wir

.. math::
  \delta_h (x,y) =
    \begin{cases}
      1 \text{ falls } h(x) = h(y), \\
      0 \text{ sonst}
    \end{cases}

Dabei berechnet :math:`\frac{1}{|\mathcal{H}|} \sum_{h \in \mathcal{H}}`
den Erwartungswert über alle möglichen Hashfunktionen.

:math:`\sum_{h \in \mathcal{H}}\delta_h(x,y)` zählt die Anzahl der Hashfunktionen
in :math:`\mathcal{H}`, die :math:`x` und :math:`y` auf den selben Hashwert abbilden.
Dabei gilt für :math:`x=y`:

.. math:: \sum_{h \in \mathcal{H}}\delta_h(x,y) = |\mathcal{H}|

und für :math:`x \neq y`, da :math:`\mathcal{H}` :math:`c`-universell ist:

.. math:: \sum_{h \in \mathcal{H}}\delta_h(x,y) \leq \frac{c}{m} * |\mathcal{H}|

Also:

.. math::

  \frac{1}{|\mathcal{H}|} \sum_{h \in \mathcal{H}} \sum_{y \in S}\delta_h(x,y)
  &= \frac{1}{|\mathcal{H}|}\sum_{y \in S} \sum_{h \in \mathcal{H}}\delta_h(x,y) \\
    & \leq \sum_{y \in S}\left[\text{if } (x=y) \text{ then } 1 \text{ else } \frac{c}{m}\right] \\
    & \leq \begin{cases}
               1 + \frac{c \cdot (n-1)}{m} & x \in S \\
               \frac{c}{m} \cdot n & x \notin
    \end{cases} \\
    & \leq 1 + c * \beta \\

Damit ist die erwartete Zugriffszeit im :math:`\mathcal{O}(1)`

Noch besser wäre, nicht nur eine erwartete Zugriffszeit von :math:`\mathcal{O}(1)`
sondern **worst-case** von :math:`\mathcal{O}(1)`
