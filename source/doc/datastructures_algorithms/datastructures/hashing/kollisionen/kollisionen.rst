===========
Kollisionen
===========

.. figure:: hash_collision.png
	 :alt: hash_collision

Veranschaulichung durch das Geburtstagsproblem:
  Sei eine Menge :math:`S` mit :math:`k` Personen. Die Geburtstage :math:`h(s)`
  sind gleichverteilt über :math:`p=365` Tage im Jahr

  Wie hoch ist die Wahrscheinlichkeit, dass mindestens zwei Personen am gleichen Tag
  Geburtstag haben?

  Wie viele Personen braucht man, damit diese Wahrscheinlichkeit bei mehr als
  50% liegt?

  Funktion Testen auf 0.5 liefert ab 23 Personen ist die Wahrscheinlichkeit >50%
  das 2 Personen an einem Tag Geburtstag haben.

.. figure:: kollisions_wahrscheinlichkeit.png
  :alt:   kollisions_wahrscheinlichkeit

Häufigkeit von Kollisionen
==========================

Man habe :math:`k` Schlüssel :math:`s_1, s_2, ... s_k` mit den Hashwerten
:math:`h(s_1), h(s_2), ..., h(s_k)` und ein Array der Größe :math:`p`.

Wegen der Gleichverteilung der Hashfunktion hat jeder Index die Wahrscheinlichkeit
:math:`\frac{1}{p}`. Bei :math:`k` Berechnungen tritt Index :math:`j` mit der
Wahrscheinlichkeit :math:`(1- \frac{1}{p})^k` nicht auf.

.. math:: (1- \frac{1}{p})^k \approx e^{-\frac{k}{p}} = e^{- \lambda}

Damit ist :math:`-\lambda = \frac{k}{p}` der **Auslastungsgrad** der Tabelle.

Mit Wahrscheinlichkeit :math:`1-(1-\frac{1}{p})^k \approx 1-e^{-\lambda}` kommt
Index :math:`j` (mindestens) einmal vor.

.. figure:: kollisions_wahrscheinlichkeit_beispiel.png
	 :alt: kollisions_wahrscheinlichkeit_beispiel


Kollisionsstrategien
====================

Geschlossenes Hashing
---------------------
Bei Kollision wird eine neue Position in der Hashtabelle gesucht.

Eine **Primärkollision** tritt auf, weil der Wert in der Tabelle einen gleichen Hashwert hat wie ein
anderer Wert.

Eine Kollision, bei einem Wert in der Tabelle der aufgrund früherer
Kollisionen hierher verschoben wurde, heißt **Sekundärkollision**.
Es können mehrere Sekundärkollisionen auftreten.

Löschen kann zu Schwierigkeiten führen.

Lineares Sondieren
^^^^^^^^^^^^^^^^^^
Beim Linearen Sondieren wird bei Kollision eine um eine konstante Position verschobene
neue Stelle bestimmt.

.. math:: G(s, i) = (h(s) + i*c) \mod p

Wobei :math:`i` sagt, die wievielte Kollision für :math:`s` vorliegt.

Dabei muss gelten :math:`\gcd(c,p) = 1`, da man sonst nicht alle Positionen
ausprobiert.

Die Verschiebung der Hashwerte muss auch beim Suchen von Elementen in der Hashtabelle
berücksichtigt werden.

Einfaches löschen eines Eintrags ist nicht möglich, da bei späterem Suchen dann
an dieser Stelle abgebrochen wird, obwohl weiter hinten noch weitere Einträge
vorhanden sind, die auf den Hash passen.

Anstelle das Element zu löschen kann man
ein ``deleted`` Boolean einführen. Alle Stellen mit ``deleted = true`` werden
wie freie Stellen behandelt. Bei der Suche werden diese Stellen wie belegte
Stellen behandelt, allerdings wird der Wert nicht für die Suchergebnisse berücksichtigt.
Dadurch verlängert sich die Suchlaufzeit.

Alternativ kann man das Element :math:`A[j]` löschen und dann durch die
Komponenten :math:`A[j+c], A[j+2c], A[j+3c], ...` iterieren bis man auf freie
Komponenten stößt. Man kopiert dann den letzten Eintrag in das Feld :math:`A[j]`


Problem des Clusterings: Durch die lineare hintereinander Reihung entstehen schnell
Cluster. Der Cluster vergrößert sich mit hoher Wahrscheinlichkeit selbst. Die
Clusterbildung beruht auf Sekundärkollisionen.

.. figure:: lineares_sondieren_cluster.png
	 :alt:     lineares_sondieren_cluster

Quadratisches Sondieren
^^^^^^^^^^^^^^^^^^^^^^^

.. math:: G(s, i) = (h(s) + i^2) \mod p

Das Quadratische Sondieren reduziert die Zahl der Sekundärkollisionen stark
und verhindert dadurch starke Clusterbildung.

- Bei Kollision wird in Quadratischen Sprüngen auf die nächsten Felder gesprungen.
- Löschen wird hier sehr kompliziert.

Man sollte die Arraygröße :math:`p` als Primzahl wählen, um kurze Zyklen zu vermeiden.

Double Hashing
^^^^^^^^^^^^^^
Bei jeder Primärkollision durchläuft jeder Schlüssel die gleiche Kette wie
frühere Schlüssel mimt dem gleichen Hash-Wert (bei linear und quadratischem
Sondieren)

Verwendung von 2 Hashfunktionen. Dadurch gibt es weniger Kollisionen.

.. math:: D(s, i) = (h(s) + i * g(s)) \mod p

Komplexität
-----------
Bei geringer Kollisionswahrscheinlichkeit ist die Komplexität von Suchen und
Einfügen :math:`O(1)`

Bei einem Auslastungsgrad :math:`\lambda` von über 80% wird des Einfüge- und
Suchverhalten schnell deutlich schlechter aufgrund der vielen auftretenden Kollisionen.

Ist die Tabelle annähernd voll bietet es sich an mit einem **Rehashing** die Tabellengröße
zu erhöhen.

Rehashing (inplace)
===================

Einfügen in die Hash-Tabelle wird immer langsamer und die Tabelle ist irgendwann
voll. Um das zu verhindern ist Rehashing nötig.

Die alte Hash-Tabelle hat die Größe :math:`p` und die neue Tabelle :math:`p'`. Die
alte Tabelle wird erweitert. Es wird die neue Position mit der neuen
Hashfunktion berechnet und an die richtige Position geschrieben. Dazu sind kleine
Puffer nötig.

.. figure:: rehashing_inplace.png
	 :alt: rehashing_inplace


Dynamisches Hashing
===================

Beim Dynamischen Hashing wird ein erweiterter Bildbereich verwendet. Bisher
waren die Hashfunktionen Abbildungen mit: :math:`h: S \rightarrow \{0,1,...  p-1\}`

.. todo:: Bilder Fix in richtigen Artikel

.. figure:: dynamisches_hashing_1.png
	 :alt: dynamisches_hashing_1

.. figure:: dynamisches_hashing_2.png
	 :alt: dynamisches_hashing_2

.. figure:: dynamisches_hashing_3.png
	 :alt: dynamisches_hashing_3

Bewertung der Hashfunktionen
============================

Die Leistungsfähigkeit jeder Hashfunktionen hängt stark von der Wertemenge ab.

Ausreichend großes Array wählen.
