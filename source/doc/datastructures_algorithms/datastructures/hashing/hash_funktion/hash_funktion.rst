.. _hash-function:

=============
Hash-Funktion
=============

Die Hashfunktion verteilt die Werte einer ungeordneten Menge auf eine Speicherdatenstruktur.

Sei :math:`S` die Menge aller Objektschlüssel, :math:`p` die Größe des Arrays.
Dann ist die Hashfunktion eine Abbildung mit :math:`h: S \rightarrow \{0, 1, ..., p-1 \}`

Eine Hashfunktion sollte **surjektiv** sein. Das heißt,
dass jedes Element der Zielmenge (hier die Array-Indizes :math:`0, 1, ..., p-1`)
wird mindestens einmal als Funktionswert angenommen. Dadurch gibt es keine leeren
Elemente im Array und es wird kein Speicherplatz verschwendet.

Die Hashfunktion sollte die zu speichernden Schlüssel gleichmäßig verteilen. Für
jedes :math:`0 \leq m < p` sollte die Menge :math:`S_m =\{ s \in S | f(s) = m \}`
ungefähr :math:`\frac{|S|}{p}` Elemente enthalten. Sonst kommt es zu mehr
Kollisionen als nötig.

Wichtig ist, dass sich die Hashfunktion sehr effizient, **in konstanter Zeit**,
berechnen lässt.

Kennt man die Semantik der Objekte, so kann man besonders kluge Hashfunktionen
entwerfen, die zu geringer Kollision führt.

Egal welche Hashfunktion gewählt wird, es gibt immer eine Eingabemenge, die
maximal schlecht ist.

Satz: Keine ideale Hashfunktion
================================

Seien :math:`U, m` und :math:`h` gegeben, :math:`|U|=k`. Dann gibt es für jedes
:math:`n` mit :math:`1 \leq n \leq \frac{k}{m}` ein :math:`S \subseteq U`, mit
:math:`|S|=n \Rightarrow S \in \binom{U}{n}`, sodass alle Elemente aus :math:`S` von :math:`h`
auf denselben Wert in :math:`\{0,1,...,m-1\}` abgebildet werden.

Nach Schubfachprinzip existiert ein :math:`i; 0 \leq i < m` sodass

.. math:: \underbrace{|h^{-1} (i)|}_{\{x \in U | h(x)=i\}} \geq \frac{|U|}{m} = \frac{k}{m}

Nach Annahme ist :math:`\frac{k}{m}\geq 1`

Wir können :math:`S \subseteq h^{-1}(i)` mit :math:`|S|=n` wählen für ein beliebiges
:math:`i` mit :math:`|h^{-1}(i)| \geq \frac{k}{m}`

Es existiert keine Super-Hashfunktion, die immer für alle S gut ist.
Um eine gute Hashfunktion zu finden, müssen also immer auch die Daten betrachtet
werden, die mit dieser Funktion gehasht werden.

.. note:: Man sollte :math:`h` in Abhängigkeit von :math:`S` bestimmen.

.. hint:: Klausuraufgabe: Bestimme Hashfunktion

Hash-Verfahren
==============

Divisionsverfahren
------------------

- Array der Größe :math:`p`
- Hashfunktion :math:`h(a) = a \mod p`

Die Länge der Hashtafel sollte eine Primzahl sein, die nicht nahe an einer
Zweierpotenz ist.

Multiplikationsverfahren
------------------------

#. Multipliziere den Objektschlüssel :math:`k` mit Zahl :math:`z` zwischen :math:`0`
   und :math:`1`. :math:`z` sollte irrational sein.
#. Man betrachtet nun nur den Nachkommateil: :math:`g(k) = k*z-\lfloor k*z \rfloor`
   sodass :math:`(0 \leq g(k) < 1)`
#. Multipliziere mit :math:`p` und runde ab: :math:`h(k) = \lfloor p*g(k) \rfloor`
   sodass :math:`(0 \leq h(k) < p)`

Fibonacci-Hashing
^^^^^^^^^^^^^^^^^

Für :math:`g(k)` wählt man :math:`z = \frac{1}{\phi_1} \approx 0,6189339887`.
Vergleiche :term:`goldener Schnitt`

Damit fällt :math:`g(k)` immer in eines der größten aktuellen Intervalle und unterteilt
es im goldenen Schnitt. Kollisionen sind selten und kommen (in den ersten :math:`p`
Elementen) durch Abrundung zustande.

Brent Hashing
-------------

Doppeltes Hashing
-----------------

FNV
---

Mid-Square Methode
--------------------
