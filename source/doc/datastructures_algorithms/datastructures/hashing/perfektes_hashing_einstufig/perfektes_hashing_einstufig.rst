=============================
Einstufiges perfektes Hashing
=============================

Man möchte einen Hashfunktion :math:`h` mit :math:`h(x) \neq h(y) \forall x \neq y \in S`
finden. Außerdem soll :math:`h` eine Hashtafel der Größte
:math:`m = \mathcal{O}(|S|) = \mathcal{O}(n)` bilden.

Die Anzahl der Kollisionen, die eine Hashfunktion :math:`h` für eine Schlüsselmenge
:math:`S` erzeugt, ist durch folgende Funktion beschreiben:

.. math:: c_S(h) &= |\{ \{ x,y \} \in \binom{S}{2} : h(x) = h(y)\}| \\

Das Ziel sind :math:`0` Kollisionen, also:

.. math:: c_S(h) = 0 \Leftrightarrow h|S \text{ injektiv}

.. note::

  Bei :math:`m > c \binom{n}{2} \in \mathcal{O}(n^2)` kann man mit WS :math:`\frac{1}{2}`
  eine injektive Hashfunktion in :math:`H` finden.

Erwartungswert der Kollisionen
==============================

**Satz:**
Für ein zufälliges :math:`h \in H` (c-universell) gilt:

.. math:: E(c_s(h))\leq \binom{n}{2} * \frac{c}{m}

**Beweis:**
Sei

.. math:: c_S(h)=\sum_{\{x,y \} \in \binom{S}{2}}^{}\delta_h(x,y)

mit

.. math::
  \delta_h (x,y) =
    \begin{cases}
      1 \text{ falls } h(x) = h(y), \\
      0 \text{ sonst}
    \end{cases}

Da :math:`\delta_h(x,y)` eine :term:`Indikatorfunktion` ist folgt für den
Erwartungswert:

.. math::

    E(c_S(h)) &=\sum_{\{x,y \} \in \binom{S}{2}}^{}E(\delta_h(x,y))=
    \sum_{\{x,y \} \in \binom{S}{2}}^{}\text{Pr}(\overbrace{\delta_h(x,y)=1}^{\leq \frac{c}{m}}) \\
    &\leq \binom{|S|}{2} \cdot \frac{c}{m} = \binom{n}{2} \cdot \frac{c}{m}

Injektive Hashfunktion
======================

**Korollar:**
Für :math:`m > c \binom{n}{2}` existiert ein :math:`h \in \mathcal{H}` mit
:math:`h|S \text{ injektiv}`

**Probebilistischer Beweis:**
Durch Einsetzen: :math:`E(c_s(h)) \leq \binom{n}{2} \frac{c}{m} <1`

Da :math:`c_S(h)` nur ganzzahlige Werte annehmen kann, muss es mindestens eine
Hashfunktion :math:`h \in \mathcal{H}` geben, mit :math:`c_S(h)=0`, also
keine Kollision hat.

Satz ist aber nicht konstruktiv, wir haben auf die Existenz von etwas geschlossen
ohne dieses konstruieren zu können. Das zufällig gewählte :math:`h` könnte mit extrem
geringer WS injektiv sein. Dann währen sehr viele Versuche nötig,
bis man ein injektives :math:`h` gefunden hat. Die Laufzeit wäre dann
:math:`\mathcal{O}(|\mathcal{H}|*n + m)`.

Zudem ist der Platzbedarf :math:`\approx n^2` schlecht.

Laufzeitschranke für das Finden der injektiven Hashfunktion
===========================================================

**Kollar:**
Falls :math:`m>2*c*\binom{n}{2}` können wir in erwartet :math:`\mathcal{O}(n+m)` Zeit
eine injektive Hashfunktion, also ein :math:`h \in \mathcal{H}` mit
:math:`h|S \text{ injektiv}` finden.

**Beweis:**

.. math::
  E(c_S(h)) \leq \binom{n}{2} *\frac{c}{m} <
  \binom{n}{2} \frac{c}{2*c* \binom{n}{2}} = \frac{1}{2}

Gemäß :ref:`markov-ungleichung` erhält man:

.. math::

  Pr(X\geq c) \leq \frac{E(X)}{c} \\
  Pr(c_S(h)\geq 1) \leq \frac{1/2}{1} = \frac{1}{2} \\
  \Rightarrow Pr(c_S(h) = 0 ) \geq \frac{1}{2} \\

Das bedeutet, dass man mit einer Wahrscheinlichkeit vom :math:`\geq \frac{1}{2}`
eine Hashfunktion :math:`h` erhält, die keine Kollisionen provoziert.

Beim einstufigen perfekten Hashing kann ein Zugriff in :math:`\mathcal{O}(1)`
erzielt werden, man muss dafür aber im :math:`|S|`-quadratisch viel Platz
aufwenden.
