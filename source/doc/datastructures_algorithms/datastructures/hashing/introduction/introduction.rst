=======
Hashing
=======

Beim Hashing sollen Objekte mittel eines eindeutigen Objektschlüssels (**keys**)
gefunden werden.

Hashing dient dazu ungeordnete Mengen zu verwalten.

Die Elemente werden auf ein Array verteilt. Dadurch erreicht man wahlfreien Zugriff
mit :math:`O(1)`

Anwendung
=========

- Closes Pair Punkte einsortieren in Gitter.
- Verwaltung des Gitters mit Hashing.
- Telefonbuch

Definition
==========

Gegeben ist ein **Universium** :math:`U` (sehr groß) und eine Teilmenge
:math:`S \subseteq U` (eher klein) wobei :math:`n=|S|`

Das Ziel ist nun ein :math:`h: U \rightarrow \{0, ... m-1\}` zu finden, sodass

.. math::
  \forall 0\leq i <m:  |\{x\in S | h(x)=i\}| \leq \left\lceil \frac{n}{2} \right\rceil

Wörterbuch-Problem
==================
Betrachte "Wörterbuch" als abstrakte Datenstruktur, die folgende Operationen
unterstützt.

- ``makeset()``: erzeutgt ein leeres Wörterbuch :math:`S`
- ``insert (x, S)``: füge Item :math:`x=(Key, value)` in :math:`S` ein,
  falls ein Item mit selben Key bereits vorhanden ist, wird es überschreiben.
- ``delete (x, S)`` löscht Item :math:`x` aus :math:`S`
- ``lookUp (Key, S)`` gibt Item :math:`x = (key, value)` aus, falls vorhanden

Bei sehr großen Datenmengen sollte man nicht Haschen.

Ziel: Datenstruktur für Wörterbuchproblem mit

  - :math:`O(1)` Zugriffszeit
  - :math:`O(n)` Platzverbrauch wobei :math:`n = |S|`

Naive Implementierung
=====================

Array/Feld für Items, Zähler für :math:`|S|`
--------------------------------------------
- ``insert(x,S)`` kostet :math:`\mathcal{O}(1)` Zeit, falls man wüsste, dass noch kein
  Item mit demselben Schlüssel bereits in :math:`S` muss man in :math:`O(n)` überprüfen,
  ob Schlüssel vorhanden.
- delete, lookup in :math:`O(n)`
- Makeset in :math:`O(1)`

Verkettete liste (einfach oder doppelt)
---------------------------------------
- verhält sich ähnlich wie Array

Direkte Adressierung
--------------------
Annahme: Schlüsseluniversum endlich und nicht zu groß.
:math:`U = \{ 0, 1,2,..., k-1\}`

Erzeuge Array der Größe :math:`k`, an Position :math:`i` steht die Information von
Schlüssel :math:`i` falls vorhanden, sonst Sonderwert ``NIL``.

- Vorteil: Alle Operationen (außer MakeSet) :math:`O(1)`,
- Nachteil: Platz :math:`\approx` Größe des **Schlüsseluniversums**
  und nicht Menge :math:`S`

Suchstruktur wie 234-Baum, AVL- und RS-Baum
-------------------------------------------
- Nachteil: Zugriff in :math:`O(\log n)`
- Vorteil: Platzbedarf :math:`O(n)`
