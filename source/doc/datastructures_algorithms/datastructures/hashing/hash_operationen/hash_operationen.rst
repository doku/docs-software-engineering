===================================
Operationen auf Hashdatenstrukturen
===================================

Bislang hatten wir immer die Annahme, dass :math:`S` fest gegeben ist.
Was ist aber, wenn sich Schlüsselmenge :math:`S` dynamisch ändert durch
``insert`` und ``remove``?

Insert
======
Falls ein ``insert`` die Anzahl der Kollisionen der Primär- oder Sekundärhashfunktion
zu sehr erhöht, muss allen komplett neu gehasht werden (Neukonstruktion).

Das ist sehr teuer, man kann jedoch zeigen, dass bei universellen Hashfunktionen
das nicht allzu oft passiert (:math:`\approx 1 \times` pro Verdopplung der Schlüsselmenge)
Damit sind die amortisierten Kosten (im mittel über alle Operationen)
pro ``insert`` :math:`O(1)`.

Remove
======
Falls ein ``remove`` den Platzverbrauch relativ zu :math:`|S|` zu groß werden
lässt, dann macht man ebenfalls eine Neukonstruktion.
z.B: wenn sich :math:`S` halbiert hat: Die amortisierten Kosten pro
``remove`` sind :math:`O(1)`.
