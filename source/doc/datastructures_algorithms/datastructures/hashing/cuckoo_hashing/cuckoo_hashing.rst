==============
Cuckoo-Hashing
==============

Das Cuckoo-Hashing ist eine einfache Alternative zu zweistufigen perfekten Hashing
(:ref:`zweistufiges-perfektes-hashing`).

Man hat eine Hashtafel der Größe :math:`m`. Sowie zwei Hashfunktionen
:math:`h_1, h_2: U \rightarrow \{0, ... m-1\}` zufällig gewählt aus einer
c-universellen Familie von Hashfunktionen.

Jeder Hashtafeleintrag hat Platz für genau ein Element.

insert
======
Versuche :math:`x` bei :math:`h_1(x)` zu speichern, falls :math:`h_1(x)` frei ist
fügt man :math:`x` ein und ist fertig.

Falls die Position :math:`h_1(x)` belegt wird von :math:`y` mit
:math:`h_i(y) = h_1(x)`, dann werfe :math:`y`
aus der Zelle :math:`h_1(x)` raus und speichere :math:`x` in Zelle :math:`h_1(x)`.

Man versucht nun :math:`y` in Zelle :math:`h_{3-i}(y)` (einfach mit der anderen
Hashfunktion hashen als zuvor verwendet) zu speichern. Falls die Zelle dort frei
ist: fertig.

Ansonsten sitzt dort :math:`z` mit :math:`h_j(z) = h_{3-i}(y)`. Werfe :math:`z`
raus, speicher :math:`y` dort und versuche :math:`z` bei
:math:`h_{3-j}(z)` unterzubringen, usw...

Die amortisierten Kosten pro ``insert`` sind :math:`\mathcal{O}(1)`.

Problem: Insert kann lange Tauschsequenzen erzeugen oder gar Tauschzyklen.
Falls das passiert, wird ein rehash gemacht.

Falls :math:`h_1, h_2` aus Pool c-universeller Hashfunktionen gewählt und man
:math:`m` hinreichend groß (:math:`m>2n`), ist Wahrscheinlichkeit dafür
sehr gering.

Ebenso rehash, falls :math:`|S|` zu groß im Vergleich zu aktuellem :math:`m`.

remove
======
Für einen ``remove(x)`` einfach Element :math:`x` bei :math:`h_1(x)` oder
:math:`h_2(x)` entfernen.

Falls :math:`|S|` zu klein wird im Vergleich zu :math:`m` macht man einen Rehash.

``remove`` ist amortisiert in :math:`\mathcal{O}(1)`.

lookup
======
Für einen ``lookup(x)`` schaut man bei :math:`h_1(x)` und :math:`h_2(x)` nach.

``lookup`` ist immer in :math:`\mathcal{O}(1)`.
