======
Theory
======

VO2 Max
=======

VO2 Max is the measurement of the maximum amount of oxygen that
an individual can utilize during intense maximal exercise.

It is measured as milliliters used in one minute per kilogramm
of body weight.
